#!/bin/bash

set -e


NUM_ARGS=$#
FILE="${!NUM_ARGS}"


# collect command line arguments
METRICS=""
CORES=""
NOCACHE=false
TIME=false

for arg in "$@"; do
    if [ "$arg" == "--metrics" ]; then
        METRICS="--print-metrics"
    fi
    if [ "$arg" == "--time" ]; then
        TIME=true
    fi
    if [ "$arg" == "--single-core" ]; then
        CORES="-j1"
    fi
    if [ "$arg" == "--no-cache" ]; then
        NOCACHE=true
    fi
done


if [[ "${FILE##*.}" == "c" ]]; then
    # delete the cache if we are supposed to not use the cache
    if $NOCACHE; then
        OUT="_build/default/$(dirname $FILE)/proofs/$(basename $FILE .c)"
        if [ -d "$OUT" ]; then
            echo "Clearing cache by deleting $OUT"
            rm -rf "$OUT"
        fi
    fi
    opam exec -- dune exec -- quiver check --no-mem-cast --no-locs --no-build $METRICS "$FILE"
    cd "$(dirname $FILE)/proofs/$(basename $FILE .c)"
    echo "Building" "$FILE"
    if $TIME; then
        opam exec -- time dune build $CORES --display short
    else
        opam exec -- dune build $CORES --display short
    fi
else if [[ "${FILE##*.}" == "v" ]]; then
    echo "Building" "${FILE}o"
    opam exec -- dune build --display short $(realpath --relative-to=. "${FILE}o")
else
    echo "Running make"
    opam exec -- make
fi fi
