#!/bin/bash

set -eo pipefail

# Wrapper for coqc that is used when running the perf script in the CI.
# Variable TIMECMD is expected to contain an absolute path to the perf script.
# If TIMECMD is not set (or empty), fallback to just calling coqc.
# we need to use opam exec -- coqc to get the coqc installed by opam, not this script

VFILE="$(pwd)/${@: -1}"

# dune calls coqc with --config and similar flags, just pass these calls through
if [[ "$VFILE" != *.v ]]; then
    opam exec -- coqc "$@"
    exit 0
fi

GLOBFILE="${VFILE%??}".glob
TMP="${VFILE%??}".tmp
if [ -z "${TIMECMD}" ]; then
  opam exec -- coqc "$@" | tee "$TMP"
else
  opam exec -- ${TIMECMD} coqc "$@" | tee "$TMP"
fi
mv "$TMP" "$GLOBFILE"
