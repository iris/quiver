import os
import collections
from tabulate import tabulate
import re



def count_existentials(lines):
    count = 0
    for line in lines:
        count += line.count('"existential instantiated x :="')
    return count

def count_solves(lines):
    count = 0
    for line in lines:
        count += line.count('"solve"')
    return count

def count_simplify(lines):
    count = 0
    for line in lines:
        count += line.count('"simplify"')
    return count

def count_size(lines):
    matches = re.findall(r'"size",\s+(\d+)', "".join(lines))
    if len(matches) == 0:
        return 0
    if len(matches) > 1:
        raise ValueError(f"Expected 1 match, got {len(matches)}")
    return int(matches[0])


def proofs_path(folder, file_base):
    return "_build/default/examples/src/casestudies/" + folder + "proofs/" + file_base

def function_path(func_name):
    return "generated_proof_" + func_name + ".glob"


def count_words_in_file(file_path):
    if not os.path.isfile(file_path):
        return None

    word_counts = collections.defaultdict(int)
    with open(file_path, 'r') as file:
        lines = file.readlines()
        word_counts["Existentials"] = count_existentials(lines)
        word_counts["Proven"] = count_solves(lines)
        word_counts["Simplified"] = count_simplify(lines)
        word_counts["Size"] = count_size(lines)
    return word_counts

def process_casestudies(casestudies):
    results = []
    for casestudy in casestudies:
        casestudy_results = {'Casestudy': casestudy['name']}
        proofs = proofs_path(folder=casestudy["folder"], file_base=casestudy["file_base"])
        for function in casestudy['functions']:
            casestudy_output = os.path.join(proofs, function_path(function))
            word_counts = count_words_in_file(casestudy_output)
            # a function is missing, so we turn the entire case study into N/A
            if word_counts is None:
                casestudy_results = { 'Casestudy': casestudy['name'], "Existentials" : "N/A", "Proven": "N/A", "Simplified": "N/A", "Size": "N/A" }
                break

            for word, count in word_counts.items():
                casestudy_results[word] = casestudy_results.get(word, 0) + count
        results.append(casestudy_results)
    return results


def print_dict_table(list_of_dicts):
    if not list_of_dicts:
        print("No data to display.")
        return
    headers = list(list_of_dicts[0].keys())
    rows = [[row[header] for header in headers] for row in list_of_dicts]
    print(tabulate(rows, headers=headers, tablefmt="grid"))




if __name__ == "__main__":
    casestudies = [
        {
            "name": "Allocators",
            "folder": "",
            "file_base": "memory",
            "functions": [ "memdup", "xmalloc", "xmemdup", "xrealloc", "xzalloc", "zalloc"]
        },
        {
            "name" : "Linked List (Mem)",
            "folder": "list/",
            "file_base": "list_no_annot",
            "functions": ["init", "is_empty", "push", "pop"]
        },
        {
            "name": "Linked List (Func)",
            "folder": "list/",
            "file_base" : "list_annotated",
            "functions": ["init", "is_empty", "push", "pop", "reverse"]
        },
        {
            "name": "Vector (Mem)",
            "folder": "arrays/",
            "file_base" : "array_no_annot",
            "functions": ["mkvec", "get_unsafe", "set_unsafe", "vec_grow", "get_checked", "set_checked", "swap", "vec_free"]
        },
        {
            "name": "Vector (Func)",
            "folder": "arrays/",
            "file_base" : "array_annotated",
            "functions": ["mkvec", "get_unsafe", "set_unsafe", "vec_grow", "get_checked", "set_checked", "swap", "vec_free"]
        },
        {
            "name": "BipBuffer (Len)",
            "folder": "memcached_bidirectional_buffers/",
            "file_base" : "bipbuffer_annotated",
            "functions": [
                "__check_for_switch_to_b",
                "bipbuf_free",
                "bipbuf_init",
                "bipbuf_is_empty",
                "bipbuf_new",
                "bipbuf_offer",
                "bipbuf_peek",
                "bipbuf_peek_all",
                "bipbuf_poll",
                "bipbuf_push",
                "bipbuf_request",
                "bipbuf_size",
                "bipbuf_sizeof",
                "bipbuf_unused",
                "bipbuf_used"
            ]
        },
        {
            "name": "OpenSSL Buffer (Mem)",
            "folder": "openssl_buffer/",
            "file_base" : "buffer",
            "functions": ["BUF_MEM_new", "BUF_MEM_new_ex", "BUF_MEM_free", "sec_alloc_realloc", "BUF_MEM_grow", "BUF_MEM_grow_clean"]
        },
        {
            "name": "OpenSSL Buffer (Len)",
            "folder": "openssl_buffer/",
            "file_base" : "buffer_shape",
            "functions": ["BUF_MEM_new", "BUF_MEM_new_ex", "BUF_MEM_free", "sec_alloc_realloc", "BUF_MEM_grow", "BUF_MEM_grow_clean"]
        },
        {
            "name": "Binary Search (Func)",
            "folder": "",
            "file_base" : "binary_search",
            "functions": ["bin_search"]
        },
        {
            "name": "Hashmap (Func)",
            "folder": "",
            "file_base" : "mutable_map",
            "functions": ["fsm_realloc_if_necessary_impl", "fsm_remove", "fsm_get", "fsm_insert", "fsm_init", "fsm_probe", "fsm_slot_for_key", "compute_min_count"]
        }

    ]

    results = process_casestudies(casestudies)
    print_dict_table(results)
