#!/bin/bash

CLANG_FORMAT=clang-format

FILE=$(mktemp).c

perl -0777 -pe 's#\[\[q::.*?\]\]##gs' "$1" | sed 's/q_assert("[^"]*");//g' | sed 's/_q_assert//g' | sed '/^[[:blank:]]*$/ d' | $CLANG_FORMAT > "$FILE"

perl -0777 -i -pe 's#\Q/* start no count */\E.*?\Q/* end no count */\E##gs' "$FILE"

cat "$FILE"
tokei "$FILE"
