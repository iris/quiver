#include <stdlib.h>

int ten () {
    int k = 0;
    while (k < 10){ k++; }
    return k;
}


int up_to_arg (int m) {
    int k = 0;
    while (k < m){ k++; }
    return k;
}

int ten_with_frame () {
    int k = 0;
    int frame = 0;
    while (k < 10) { k++; }
    return k + frame;
}

// the challenge in this example is that [a] is only discovered and
// used after the loop, but it's ownership must go into the precondition
int plus_ten(int a)
{
    int k = 0;
    while (k < 10) { k++; }
    assert(a < 200);
    return k + a;
}
