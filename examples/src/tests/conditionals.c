#include <stddef.h>

// minimum
// simplest possible function with a conditional
int min (int x, int y){
    if (x < y) {
        return x;
    } else {
        return y;
    }
}


int min3 (int x, int y, int z) {
    return min (min (x, y), z);
}

int min3_inline (int x, int y, int z) {
    if (x < y) {
        if (x < z) {
            return x;
        } else {
            return z;
        }
    } else {
        if (y < z) {
            return y;
        } else {
            return z;
        }
    }
}

void assert_positive (int x){
    assert (x > 0);
    return;
}


int min_inline (int x, int y) {
    return (x < y) ? x : y;
}

void assert_one_positive (int x, int y, int z) {
    assert (x > 0 || y > 0 || z > 0);
    return;
}

void assert_all_positive (int x, int y, int z) {
    assert (x > 0 && y > 0 && z > 0);
    return;
}

void assert_all_same_sign (int x, int y, int z) {
    assert ((x > 0 && y > 0 && z > 0) || (x < 0 && y < 0 && z < 0));
    return;
}

int arith_with_flag (int flag, int x, int y) {
    if (flag == 0)
        return x + y;
    if (flag == 1)
        return x - y;
    if (flag == 2)
        return x * y;
    if (flag == 3)
        return x / y;

    return 0;
}


int arith_with_flag_res (int flag, int x, int y) {
    int res = 0;

    if (flag == 0)
        res = x + y;
    if (flag == 1)
        res = x - y;
    if (flag == 2)
        res = x * y;
    if (flag == 3)
        res = x / y;

    return res;
}

int arith_with_flag_res2 (int flag, int x, int y) {
    int res;

    if (flag == 0)
        res = x + y;
    if (flag == 1)
        res = x - y;
    if (flag == 2)
        res = x * y;
    if (flag == 3)
        res = x / y;

    return res;
}



void conditional_set_zero (int *x, int *y, int *z) {
    if (x != NULL) {
        *x = 0;
    }

    if (y) {
        *y = 0;
    }
}


int indirect_conditional (int x, int y) {
    if (x) {
        y = 1;
    }

    return y;
}

int indirect_conditional2 (int x, int y) {
    y = y;
    if (x) {
        y = 1;
    }
    return y;
}

void indirect_conditional_ptr (int x, int *y) {
    if (x) {
        *y = 1;
    }
}

void indirect_conditional_nested_ptr (int x, int **y) {
    if (x) {
        **y = 1;
    }
}

void symmetric_conditional (int x, int *y){
    if (x) {
        *y = 1;
    } else {
        *y = 0;
    }
}



void modify_if_not_null (int *x) {
    if (x == NULL)
        return;

    *x = 5;
}

void assert_not_null (int *x){
    assert (x != NULL);
    *x = 5;
}




