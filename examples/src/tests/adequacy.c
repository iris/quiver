int inc(int x) {
    return x + 1;
}

int main() {
    int x = 41;
    int y = inc(x);
    return y;
}