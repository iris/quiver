#include <quiver.h>
#include <stddef.h>
#include <stdbool.h>

// simple swap function
// should also be typable as bool* -> bool* -> void
void swap (int *x, int *y) {
    int tmp = *x;
    *x = *y;
    *y = tmp;
}


// simple test for overwriting
void overwrite (int *x) {
    int tmp = *x;
    *x = 42;
    *x = tmp;
}

// simple pointer dereference
int deref (int *x) {
    return *x;
}

int id (int x) {
    return x;
}


// odd alternative implementation of swap
// can also be typed as bool* -> bool* -> void
// and also as even* -> even* -> void
void swap2 (int *x, int *y) {
    int tmp = *x;
    int tmp2 = *y;
    *x = 42;
    *y = 42;
    *x = tmp2;
    *y = tmp;
}

// binary operation +
// using the same variable twice
// can cause trouble with linearity
int times_two (int *x) {
    return (*x) + (*x);
}

// doubling an integer should not overflow
int double_int(int x){
    return x + x;
}


int dup(int *dst1, int *dst2, int *src) {
    *dst1 = *src;
    *dst2 = *src;
    return *src;
}

// using swap,
// example of a function call and assert
void use_swap() {
    int x = 4;
    int y = 5;
    swap(&x, &y);
    assert(x == 5 && y == 4);
}


int arith(int x, int y) {
    int z = (x + x) * y / 2;
    int a = x + y;
    int b = x - y;
    int c = x * y;
    int d = x / y;
    int e = x % y;
    int f = x << y;
    int g = x >> y;
    int h = x & y;
    int i = x | y;
    int j = x ^ y;
    int l = x == y;
    int m = x != y;
    int n = x < y;
    int o = x <= y;
    int p = x > y;
    int q = x >= y;
    int r = - x;

    return 42;
}



void cond_assign (int *x){
    if (x != NULL) {
        *x = 42;
    }
}

bool is_some_check (int *x) {
    return x != NULL;
}


bool is_none_check (int *x) {
    return x == NULL;
}

bool is_some(int *x) {
    if (x == NULL) {
        return false;
    } else {
        return true;
    }
}

bool ptr_eq (int *x, int *y) {
    return x == y;
}

bool ptr_neq (int *x, int *y) {
    return x != y;
}


int f(int x);
int g(int x);

int forwarding(int x) {
    int m = f(x);
    return g(m);
}
