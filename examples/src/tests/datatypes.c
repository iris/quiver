#include <stdint.h>
#include "alloc.h"


typedef struct triple {
    int x;
    int y;
    int z;
} triple_t;


typedef struct mixed {
    char x;
    triple_t triple;
    struct mixed *next;
    int *foo;
} *mixed_t;


typedef struct buffer {
    char contents[1024];
    struct buffer *next;
} *buffer_t;


int proj_x (triple_t t){
    return t.x;
}

int deref_x (triple_t *t){
    return t->x;
}

char get_x (mixed_t mix) {
    return mix->x;
}

void set_x (mixed_t mix, char y) {
    mix->x = y;
}

int get_foo(mixed_t mix) {
    return *(mix->foo);
}

triple_t get_triple(mixed_t mix){
    return mix->triple;
}

mixed_t get_next(mixed_t mix){
    return mix->next;
}

triple_t new_triple () {
    triple_t t = {.x = 0, .y = 0, .z = 0};
    return t;
}

mixed_t *alloc_mixed (){
    mixed_t *mixed = alloc(sizeof(mixed_t));
    return mixed;
}

char buffer_sub(buffer_t b, int i){
    return b->contents[i];
}

char array_sub(char *a, int i){
    return a[i];
}


int array_local_triple () {
    int t[3];
    t[0] = 1;
    t[1] = 2;
    t[2] = 3;
    return t[0] + t[1] + t[2];
}

int array_local_triple2 () {
    int t[3];
    t[1] = 2;
    t[0] = 1;
    t[2] = 3;
    return t[0] + t[1] + t[2];
}

int array_local_triple3 () {
    int t[3];
    t[0] = 1;
    int *p = &t[1];
    p[0] = 2;
    p[1] = 3;
    return t[0] + p[0] + p[1];
}

int array_local_triple4 () {
    int t[3];
    t[0] = 1;
    int *p = &t[1];
    p[0] = 2;
    p[1] = 3;
    return t[0] + t[1] + t[2];
}

int array_local_var (int y) {
    int x[10];

    x[1] = y;
    return x[1];
}