/**
 * Copyright (c) 2011, Willem-Hendrik Thiart
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE.bipbuffer file.
 *
 * @file
 * @author  Willem Thiart himself@willemthiart.com
 */

#include "quiver.h"
#include <stdlib.h>

/* Changes from original:
    - translated [me->data] into [__data(me)] to make the offset computation explicit, not supported by the frontend yet
    - unfolded nested assignments into multiple statements
    - named the struct [buf] (was anonymous before)
    - renamed variables
    - removed TODO comment
*/

/* for memcpy */
#include <string.h>

#include "bipbuffer_annotated.h"


static size_t bipbuf_sizeof(const unsigned int size)
{
    return sizeof(bipbuf_t) + size;
}


#define __data(me) (((unsigned char *)me) + sizeof(bipbuf_t))

// SKETCH: 2
// ANNOT: 1
[[q::args("owned ? (? @ bipbuf_t)")]]
[[q::ensures("^me ◁ᵥ owned ? (? @ bipbuf_t)")]]
[[q::join_if]]
int bipbuf_unused(const bipbuf_t* me)
{
    if (1 == me->b_inuse)
        /* distance between region B and region A */
        return me->a_start - me->b_end;
    else
        return me->size - me->a_end;
}

// SKETCH: 2
[[q::args("owned ? (? @ bipbuf_t)")]]
[[q::ensures("^me ◁ᵥ owned ? (? @ bipbuf_t)")]]
int bipbuf_size(const bipbuf_t* me)
{
    return me->size;
}


// SKETCH: 2
[[q::args("owned ? (? @ bipbuf_t)")]]
[[q::ensures("^me ◁ᵥ owned ? (? @ bipbuf_t)")]]
int bipbuf_used(const bipbuf_t* me)
{
    return (me->a_end - me->a_start) + me->b_end;
}

// SKETCH: 1
[[q::args("owned ? (any[ly_size struct_buf])", "_")]]
void bipbuf_init(bipbuf_t* me, const unsigned int size)
{
    me->b_end = 0;
    me->a_end = 0;
    me->a_start = 0;
    me->size = size;
    me->b_inuse = 0;
}

// SKETCH: 1
bipbuf_t *bipbuf_new(const unsigned int size)
{
    bipbuf_t *me = malloc(bipbuf_sizeof(size));
    if (!me)
        return NULL;
    bipbuf_init(me, size);
    q_assert("me ◁ₗ owned ? (? @ bipbuf_t)");
    return me;
}


// SKETCH: 4
[[q::parameters("l: loc")]]
[[q::parameters("data: Z * Z * Z * Z * Z")]]
[[q::args("owned l (data @ bipbuf_t)")]]
[[q::requires("block l (ly_size struct_buf + data.1.1.1.1)")]]
void bipbuf_free(bipbuf_t* me)
{
    free(me);
}

// SKETCH: 2
[[q::args("owned ? (? @ bipbuf_t)")]]
[[q::ensures("^me ◁ᵥ owned ? (? @ bipbuf_t)")]]
int bipbuf_is_empty(const bipbuf_t* me)
{
    return me->a_start == me->a_end;
}

// SKETCH: 2
// ANNOT: 1
/* find out if we should turn on region B
 * ie. is the distance from A to buffer's end less than B to A? */
[[q::args("owned ? (? @ bipbuf_t)")]]
[[q::ensures("^me ◁ᵥ owned ? (? @ bipbuf_t)")]]
[[q::join_if]]
static void __check_for_switch_to_b(bipbuf_t* me)
{
    if (me->size - me->a_end < me->a_start - me->b_end)
        me->b_inuse = 1;
}

// SKETCH: 1
[[q::ensures("^me ◁ᵥ owned ? (? @ bipbuf_t)")]]
unsigned char *bipbuf_request(bipbuf_t* me, const int size)
{
    if (bipbuf_unused(me) < size)
        return 0;
    if (1 == me->b_inuse)
    {
        return (unsigned char *)__data(me) + me->b_end;
    }
    else
    {
        return (unsigned char *)__data(me) + me->a_end;
    }
}

// SKETCH: 0
int bipbuf_push(bipbuf_t* me, const int size)
{
    if (bipbuf_unused(me) < size)
        return 0;

    if (1 == me->b_inuse)
    {
        me->b_end += size;
    }
    else
    {
        me->a_end += size;
    }

    __check_for_switch_to_b(me);
    return size;
}

// SKETCH: 0
int bipbuf_offer(bipbuf_t* me, const unsigned char *data, const int size)
{
    /* not enough space */
    if (bipbuf_unused(me) < size)
        return 0;

    if (1 == me->b_inuse)
    {
        memcpy(__data(me) + me->b_end, data, size);
        me->b_end += size;
    }
    else
    {
        memcpy(__data(me) + me->a_end, data, size);
        me->a_end += size;
    }

    __check_for_switch_to_b(me);
    return size;
}

// SKETCH: 2
[[q::args("owned ? (? @ bipbuf_t)", "_")]]
[[q::ensures("^me ◁ᵥ owned ? (? @ bipbuf_t)")]]
unsigned char *bipbuf_peek(const bipbuf_t* me, const unsigned int size)
{
    /* make sure we can actually peek at this data */
    if (me->size < me->a_start + size)
        return NULL;

    if (bipbuf_is_empty(me))
        return NULL;

    return (unsigned char *)__data(me) + me->a_start;
}

// SKETCH: 1
[[q::ensures("^me ◁ᵥ owned ? (? @ bipbuf_t)")]]
unsigned char *bipbuf_peek_all(const bipbuf_t* me, unsigned int *size)
{
    if (bipbuf_is_empty(me))
        return NULL;

    *size = me->a_end - me->a_start;
    return (unsigned char*)__data(me) + me->a_start;
}

// SKETCH: 1
[[q::ensures("^me ◁ᵥ owned ? (? @ bipbuf_t)")]]
unsigned char *bipbuf_poll(bipbuf_t* me, const unsigned int size)
{
    if (bipbuf_is_empty(me))
        return NULL;

    /* make sure we can actually poll this data */
    if (me->size < me->a_start + size)
        return NULL;

    void *end_ptr = __data(me) + me->a_start;
    me->a_start += size;

    /* we seem to be empty.. */
    if (me->a_start == me->a_end)
    {
        /* replace a with region b */
        if (1 == me->b_inuse)
        {
            me->a_start = 0;
            me->a_end = me->b_end;
            me->b_inuse = 0;
            me->b_end = 0;
        }
        else
        {
            /* safely move cursor back to the start because we are empty */
            me->a_end = 0;
            me->a_start = 0;
        }
    }

    __check_for_switch_to_b(me);
    return end_ptr;
}

// TOTAL SKETCH:  2 + 2 + 2 + 1 + 1 + 4 + 2 + 2 + 1 + 0 + 0 + 2 + 1 + 1 = 21
// TOTAL TYPE:    10 (in "bipbuffer_annotated.h")
// TOTAL INV:     0
// TOTAL ANNOT:   1 + 1 = 2


/* start no count */
[[q::args("owned ? (? @ bipbuf_t)")]]
[[q::ensures("^me ◁ᵥ owned ? (? @ bipbuf_t)")]]
unsigned long int bipbuf_size_corrected(const bipbuf_t* me)
{
    return me->size;
}


[[q::ensures("^me ◁ᵥ owned ? (ex p, p @ bipbuf_t)")]]
[[q::join_if]]
unsigned char *bipbuf_poll_joined(bipbuf_t* me, const unsigned int size)
{
    if (bipbuf_is_empty(me))
        return NULL;

    /* make sure we can actually poll this data */
    if (me->size < me->a_start + size)
        return NULL;

    void *end_ptr = __data(me) + me->a_start;
    me->a_start += size;

    /* we seem to be empty.. */
    if (me->a_start == me->a_end)
    {
        /* replace a with region b */
        if (1 == me->b_inuse)
        {
            me->a_start = 0;
            me->a_end = me->b_end;
            me->b_inuse = 0;
            me->b_end = 0;
        }
        else
        {
            /* safely move cursor back to the start because we are empty */
            me->a_end = 0;
            me->a_start = 0;
        }
    }

    __check_for_switch_to_b(me);
    return end_ptr;
}
/* end no count */
