
//@q::import swap from quiver.examples.src.casestudies.extra_proofs

// bare bones version of swap
void swap(int *a, int i, int j) {
  int tmp = a[i];
  a[i] = a[j];
  a[j] = tmp;
}


[[q::args("? @ bufR", "_", "_")]]
[[q::ensures("^a ◁ᵥ ? @ bufR")]]
void swap_buffer(int *a, int i, int j) {
  int tmp = a[i];
  a[i] = a[j];
  a[j] = tmp;
}

[[q::args("? @ int_arrayR", "_", "_")]]
[[q::ensures("^a ◁ᵥ ? @ int_arrayR")]]
void swap_int_array(int *a, int i, int j) {
  int tmp = a[i];
  a[i] = a[j];
  a[j] = tmp;
}

[[q::parameters("s: gset Z")]]
[[q::args("s @ setR", "_", "_")]]
[[q::ensures("^a ◁ᵥ s @ setR")]]
void swap_set(int *a, int i, int j) {
  int tmp = a[i];
  a[i] = a[j];
  a[j] = tmp;
}


[[q::parameters("s: Z * gset Z")]]
[[q::args("s @ setLenR", "_", "_")]]
[[q::ensures("^a ◁ᵥ s @ setLenR")]]
void swap_set_len(int *a, int i, int j) {
  int tmp = a[i];
  a[i] = a[j];
  a[j] = tmp;
}


[[q::parameters("i : Z", "j : Z")]]
[[q::args("?", "intT i32 i", "intT i32 j")]]
[[q::requires("⌜i = j⌝")]]
void swap_refl(int *a, int i, int j) {
  int tmp = a[i];
  a[i] = a[j];
  a[j] = tmp;
}
