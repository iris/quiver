#include <quiver.h>
#include "../memory.h"


// Import memory specs from other case study
//@q::import all_proofs from quiver.examples.src.casestudies.memory


typedef struct range {
    int low;
    int high;
} *range_t;


void init_range(range_t r, int s, int e) {
    r->low = s;
    r->high = e;
}

range_t mk_range(int a, int b) {
    range_t r = xmalloc(sizeof(struct range));
    init_range(r, a, b);
    return r;
}


int get_low(range_t r) {
    return r->low;
}

int get_high(range_t r) {
    return r->high;
}


int get_size(range_t r) {
    return r->high - r->low;
}

int is_empty(range_t r) {
    return r->low == r->high;
}

int contains(range_t r, int x) {
    return r->low <= x & x < r->high;
}