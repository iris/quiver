#include <quiver.h>
#include "../memory.h"


// Import memory specs from other case study
//@q::import all_proofs from quiver.examples.src.casestudies.memory


typedef struct
[[q::refined_by("s: Z", "e: Z")]]
[[q::typedef("range_t : ex q, (owned q ...) ∗∗ (block q (ly_size struct_range))")]]
[[q::constraints("⌜0 ≤ s ≤ e⌝")]]
range {
    [[q::field("int[i32] s")]]
    int low;
    [[q::field("int[i32] e")]]
    int high;
} *range_t;



void init_range(range_t r, int s, int e) {
    r->low = s;
    r->high = e;
    q_assert("^r ◁ᵥ ? @ range_t");
}

range_t mk_range(int a, int b) {
    range_t r = xmalloc(sizeof(struct range));
    init_range(r, a, b);
    return r;
}


[[q::requires("^r ◁ᵥ ? @ range_t")]]
[[q::ensures("^r ◁ᵥ ? @ range_t")]]
int get_low(range_t r) {
    return r->low;
}

[[q::requires("^r ◁ᵥ ? @ range_t")]]
[[q::ensures("^r ◁ᵥ ? @ range_t")]]
int get_high(range_t r) {
    return r->high;
}

[[q::requires("^r ◁ᵥ ? @ range_t")]]
[[q::ensures("^r ◁ᵥ ? @ range_t")]]
int get_size(range_t r) {
    return r->high - r->low;
}

[[q::requires("^r ◁ᵥ ? @ range_t")]]
[[q::ensures("^r ◁ᵥ ? @ range_t")]]
int is_empty(range_t r) {
    return r->low == r->high;
}

[[q::requires("^r ◁ᵥ ? @ range_t")]]
[[q::ensures("^r ◁ᵥ ? @ range_t")]]
int contains(range_t r, int x) {
    return r->low <= x & x < r->high;
}