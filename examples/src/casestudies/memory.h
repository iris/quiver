#include <stdint.h>
#include <stdlib.h>
#include <string.h>

//@q::import malloc from quiver.stdlib
//@q::context `{!lib_malloc Σ}


void *xmalloc(size_t size);
void *xrealloc(void *ptr, size_t size);
void *xzalloc(size_t size);
void *xmemdup(const void *ptr, size_t size);
void *zalloc(size_t size);
