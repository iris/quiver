#include <stdlib.h>

//@q::import pure_defs from quiver.examples.src.casestudies.extra_proofs.binary_search
//@q::import type_defs from quiver.examples.src.casestudies.extra_proofs.binary_search
//@q::context `{!lib_malloc Σ}

// TYPE: 7
//    should be counted as
//  sorted_array = λ xs,                                            1
//                  ex p,                                           1
//                  constraintT (sorted xs)                         1
//                  sepT (block p (length xs * ly_size i32))        1
//                  ownT p                                          1
//                  arrayT (intT i32) (ly_size i32) xs              2
//                                                                -----
//                                                                  7
//    originally counted as
//  sorted_array = λ xs,                                            0
//                  ex p,                                           1
//                  constraintT (sorted xs)                         1
//                  sepT (block p (length xs * ly_size i32))        1
//                  ownT p                                          1
//                  arrayT (intT i32) (ly_size i32) xs              1
//                                                                -----
//                                                                  5



// SKETCH: 5 = (2 params, 3 args)
// INV:   8 = (2 exists, 3 type assignments, 3 constraints)
[[q::parameters("xs: list Z", "k: Z")]]
[[q::args("xs @ sorted_arrayR", "(int[size_t] (length xs))", "(int[i32] k)")]]
size_t bin_search(int *a, size_t n, int x) {
    size_t l = 0;
    size_t r = n;

    [[q::constraints("∃ l_i r_i, l ◁ₗ (int[size_t] l_i) ∗ r ◁ₗ (int[size_t] r_i) ∗ ^a ◁ᵥ xs @ sorted_arrayR ∗ ⌜l_i ≤ r_i⌝ ∗ ⌜r_i ≤ length xs⌝ ∗ ⌜in_between xs k l_i r_i⌝")]]
    while (l < r) {
        size_t m = l + (r - l) / 2;
        if (a[m] < x) {
            l = m + 1;
        } else {
            r = m;
        }
    }
    return l;
}


// originally counted as
    // TOTAL SKETCH:  5
    // TOTAL TYPE:    5
    // TOTAL INV:     8
    // TOTAL ANNOT:   0
// but should be
    // TOTAL SKETCH:  5
    // TOTAL TYPE:    7
    // TOTAL INV:     8
    // TOTAL ANNOT:   0