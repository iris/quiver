#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "memory.h"

/* based on https://gitlab.mpi-sws.org/iris/refinedc/-/blob/master/examples/mutable_map.c

   The version from RefinedC has been adapted in the following ways:
   - Replace the use of unions with just a struct
   - Replace the loop in init with a xzalloc
   - Store integers instead of arbitrary types in the map

  Moreover, to cut the mutually recursive dependency between fsm_insert and fsm_realloc_if_necessary,
  we proceed as follows:
  1. For fsm_insert, we assume the specification fsm_realloc_if_necessary_pt below.
  2. For fsm_realloc_if_necessary, we use the inferred specification for fsm_insert.

  The assumed and inferred specifications of [fsm_realloc_if_necessary] coincide.
 */

//@q::import definitions from quiver.examples.src.casestudies.extra_proofs.mutable_map
//@q::import malloc from quiver.stdlib
//@q::import all_proofs from quiver.examples.src.casestudies.memory


// COUNTED AS 9 LINES OF COQ CODE:
//@q::inlined_final
//@  Section assume.
//@  Context `{!refinedcG Σ} `{!lib_malloc Σ}.
//@
//@  Definition fsm_realloc_if_necessary_pt (ws : list val) (ret : type Σ → iProp Σ) : iProp Σ :=
//@    (∃ m_arg, ⌜ws = [m_arg]⌝ ∗
//@      (∃ m mp items count,
//@        nfs [] [m_arg ◁ᵥ ownT m ((mp, items, count) @ fixed_size_map)]
//@          (∀ items2 (count2 : Z), nfw [count ≤ count2; 1 < count2 ≤ max_int size_t; length items ≤ length items2]
//@               [m ◁ₗ ((mp, items2, count2) @ fixed_size_map)] (ret voidT)))).
//@  End assume.
//@q::end

#define ITEM_EMPTY ((size_t)0)
#define ITEM_ENTRY ((size_t)1)
#define ITEM_TOMBSTONE ((size_t)2)


// TYPE: 9
//   counted as
//  item_t = λ x,                                   1
//    ex tag,                                       1
//    ex key,                                       1
//    ex val,                                       1
//    sepT (⌜build_item tag key val = Some x⌝)      1
//    structT [                                     1
//        int[size_t] tag;                          1
//        int[size_t] key;                          1
//        int[size_t] val                           1
//    ]                                           -----
//                                                  9
//
struct
[[q::refined_by("x : item_ref")]]
[[q::exists("tag :  Z", "key : Z", "val : Z")]]
[[q::constraints("⌜build_item tag key val = Some x⌝")]]
item
{
  [[q::field("int[size_t] tag")]]
  size_t tag;
  [[q::field("int[size_t] key")]]
  size_t key;
  [[q::field("int[size_t] val")]]
  size_t value;
};

// TYPE: 10
//   counted as
//  fixed_size_map = λ (mp, items, count),                        1
//    sepT (⌜fsm_invariant mp items count⌝)                       1
//    structT [                                                   1
//        (ex p,                                                  1
//         sepT (block p (ly_size struct_item * (length items)))  1
//         ownT p                                                 1
//         arrayT (λ x, x @ item));                               2
//        intT size_t count;                                      1
//        intT size_t (length items)                              1
//    ]                                                         -----
//                                                                10
struct
[[q::refined_by("mp : gmap Z Z", "items : list item_ref", "count : Z")]]
[[q::constraints("⌜fsm_invariant mp items count⌝")]]
fixed_size_map {
  [[q::field("ex p, (owned p (arrayT (λ x, x @ item) (ly_size struct_item) items)) ∗∗  (block p (ly_size struct_item * (length items)))")]]
  struct item (* items)[];
  [[q::field("intT size_t count")]]
  size_t count;
  [[q::field("intT size_t (length items)")]]
  size_t length;
};

// ANNOT: 1
[[q::external_pt("fsm_realloc_if_necessary_pt")]]
void fsm_realloc_if_necessary(struct fixed_size_map *m);

// SKETCH: 6
[[q::parameters("len : nat")]]
[[q::parameters("key : Z")]]
[[q::args("intT size_t len")]]
[[q::args("intT size_t key")]]
[[q::requires("⌜0 < len⌝")]]
[[q::returns("intT size_t (slot_for_key_ref key len)")]]
size_t fsm_slot_for_key(size_t len, size_t key) {
    // TODO: hash
    return key % len;
}


// SKETCH: 12 (5 parameters, 2 arguments, 1 exists, 1 returns, 3 ensures)
// ANNOT:  1
// INV:    7  (1 existential, 4 type assignments, 2 pure propositions)
[[q::parameters("ptr : loc", "mp : gmap Z Z", "items : list item_ref", "k : Z", "count : Z")]]
[[q::args("ownT ptr ((mp, items, count) @ fixed_size_map)", "intT size_t k")]]
[[q::exists("n : nat")]]
[[q::returns("intT size_t n")]]
[[q::ensures("ptr ◁ₗ (mp, items, count) @ fixed_size_map")]]
[[q::ensures("⌜n < length items⌝")]]
[[q::ensures("⌜probe_ref k items = Some (n, items !!! n )⌝")]]
[[q::exact_post]]
size_t fsm_probe(struct fixed_size_map *m, size_t key) {
    size_t slot_idx = fsm_slot_for_key(m->length, key);

    [[q::constraints("∃ (offset: nat), \
      ptr ◁ₗ ((mp, items, count) @ fixed_size_map) ∗ \
      key ◁ₗ intT size_t k ∗ \
      m ◁ₗ valueT ptr ptr_size ∗ \
      slot_idx ◁ₗ intT size_t (rotate_nat_add (slot_for_key_ref k (length items)) offset (length items)) ∗ \
      ⌜probe_ref_go k (take offset (rotate (slot_for_key_ref k (length items)) items)) = None⌝ ∗ \
      ⌜rotate_nat_add (slot_for_key_ref k (length items)) offset (length items) < length items⌝")]]
    while(true) {
      struct item *item = &(*m->items)[slot_idx];

      if(item->tag == ITEM_EMPTY)
        return slot_idx;

      if ((item->tag == ITEM_TOMBSTONE) & (item->key == key))
        return slot_idx;

      if((item->tag == ITEM_ENTRY) & (item->key == key))
        return slot_idx;

      slot_idx = (slot_idx + 1) % m->length;
    }
}

// SKETCH: 3
[[q::parameters("p : loc")]]
[[q::args("ownT p (anyT (ly_size struct_fixed_size_map))", "_")]]
[[q::ensures("^m ◁ᵥ ownT p (((∅, ?, ?) @ fixed_size_map))")]]
void fsm_init(struct fixed_size_map *m, size_t len) {
  void *storage = xzalloc(sizeof(struct item) * len);
  m->length = len;
  m->items = storage;
  m->count = len;
}

// SKETCH: 15 (6 parameters, 3 args, 2 exists, 1 returns, 3 ensures)
// ANNOT:  1
[[q::parameters("p : loc", "mp : gmap Z Z", "key : Z", "value : Z", "items: list item_ref", "count: Z")]]
[[q::args("(ownT p ((mp, items, count) @ fixed_size_map))", "intT size_t key", "intT size_t value")]]
[[q::exists("items2 : list item_ref", "count2: Z")]]
[[q::returns("intT size_t (default 0 (mp !! key))")]]
[[q::ensures("p ◁ₗ (((<[key := value]>mp, items2, count2) @ fixed_size_map))")]]
[[q::ensures("⌜length items ≤ length items2⌝", "⌜count - 1 ≤ count2⌝")]]
[[q::exact_post]]
size_t fsm_insert(struct fixed_size_map *m, size_t key, size_t value) {
    fsm_realloc_if_necessary(m);
    size_t slot_idx = fsm_probe(m, key);
    size_t replaced = 0;
    struct item *item = &(*m->items)[slot_idx];
    if (item->tag == ITEM_ENTRY) {
        replaced = item->value;
    } else if(item->tag == ITEM_EMPTY) {
      m->count -= 1;
    }

    item->tag = ITEM_ENTRY;
    item->key = key;
    item->value = value;

    return replaced;
}

// SKETCHES: 9 (5 parameters, 2 args, 1 returns, 1 ensures)
// ANNOT: 1
[[q::parameters("p : loc", "mp : gmap Z Z", "key : Z", "items: list item_ref", "count: Z")]]
[[q::args("ownT p ((mp, items, count) @ fixed_size_map)", "intT size_t key")]]
[[q::returns("intT size_t (default 0 (mp !! key))")]]
[[q::ensures("p ◁ₗ ((mp, items, count) @ fixed_size_map)")]]
[[q::exact_post]]
size_t fsm_get(struct fixed_size_map *m, size_t key) {
    size_t slot_idx = fsm_probe(m, key);
    struct item *item = &(*m->items)[slot_idx];

    if (item->tag == ITEM_ENTRY) {
        return item->value;
    } else {
        return 0;
    }
}

// SKETCHES: 9 (5 parameters, 2 args, 1 returns, 1 ensures)
// ANNOT: 1
[[q::parameters("p : loc", "mp : gmap Z Z", "key : Z", "items: list item_ref", "count: Z")]]
[[q::args("ownT p ((mp, items, count) @ fixed_size_map)", "intT size_t key")]]
[[q::returns("intT size_t (default 0 (mp !! key))")]]
[[q::ensures("p ◁ₗ (ex items, (delete key mp, items, count) @ fixed_size_map)")]]
[[q::exact_post]]
size_t fsm_remove(struct fixed_size_map *m, size_t key) {
    size_t removed;
    size_t slot_idx = fsm_probe(m, key);
    struct item *item = &(*m->items)[slot_idx];

    if (item->tag == ITEM_ENTRY) {
        removed = item->value;
        item->tag = ITEM_TOMBSTONE;
        item->key = key;
        return removed;
    } else {
        return 0;
    }
}


// SKETCHES: 6
// ANNOT: 1
// NOTE: The reason this specification is so verbose is that
// we intentionally abstract over the implementation, because
// it is so simple.
[[q::parameters("n : Z")]]
[[q::args("(int[size_t] n)")]]
[[q::exists("m : Z")]]
[[q::requires("⌜1 < n⌝")]]
[[q::returns("(int[size_t] m)")]]
[[q::ensures("⌜1 < m <= n⌝")]]
[[q::exact_post]]
size_t compute_min_count(size_t len) {
  return 2;
}


// SKETCHES: 12 (4 params, 1 args, 2 exists, 1 returns, 4 ensures)
// ANNOT: 1
// INV: 11 (3 exists, 4 assignments, 4 constraints)
[[q::parameters("ptr : loc", "mp : gmap Z Z", "items1: list item_ref", "count1: Z")]]
[[q::args("(ownT ptr ((mp, items1, count1) @ fixed_size_map))")]]
[[q::exists("items2: list item_ref", "count2: Z")]]
[[q::returns("void")]]
[[q::ensures("ptr ◁ₗ (((mp, items2, count2) @ fixed_size_map))")]]
[[q::ensures("⌜count1 ≤ count2⌝", "⌜1 < count2 ≤ max_int size_t⌝", "⌜length items1 ≤ length items2⌝")]]
[[q::exact_post]]
void fsm_realloc_if_necessary_impl(struct fixed_size_map *m) {
  if(compute_min_count(m->length) <= m->count) {
    return;
  }

  if(m->length < SIZE_MAX / 2 / sizeof(struct item) - 16) {} else { abort(); }

  struct fixed_size_map m2;
  size_t new_len = m->length * 2;
  size_t i;

  fsm_init(&m2, new_len);
  [[q::constraints("∃ (idx: nat) (items2: list item_ref) (count2 : Z),  \
    i ◁ₗ (int[size_t] idx) ∗ \
    m ◁ₗ valueT ptr ptr_size ∗ \
    ptr ◁ₗ (mp, items1, count1) @ fixed_size_map ∗ \
    m2 ◁ₗ (fsm_copy_entries items1 idx, items2, count2) @ fixed_size_map ∗ \
    ⌜count1 + length items1 - idx < count2⌝ ∗ \
    ⌜0 < count1⌝ ∗ \
    ⌜idx <= length items1⌝ ∗ \
    ⌜length items1 * 2 <= length items2⌝ \
    ")]]
  for(i = 0; i < m->length; i += 1) {
    struct item *item = &(*m->items)[i];

    if(item->tag == ITEM_ENTRY) {
      fsm_insert(&m2, item->key, item->value);
    }
  }

  free(m->items);
  m->length = m2.length;
  m->count = m2.count;
  m->items = m2.items;
}


// TOTAL SKETCH:  6 + 12 + 3 + 15 + 9 + 9 + 6 + 12 = 72
// TOTAL TYPE:    9 + 10 = 19
// TOTAL INV:     7 + 11 = 18
// TOTAL ANNOT:   7