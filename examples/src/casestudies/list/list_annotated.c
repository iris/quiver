#include <quiver.h>
#include <stddef.h>
#include <stdbool.h>
#include "../memory.h"


// Import memory specs from other case study
//@q::import all_proofs from quiver.examples.src.casestudies.memory

// make verification parametric over the list's element type
//@q::context {X : Type} (T : rtype Σ X) `{!∀ x, PtrType (x @ T)}


// TYPE: 11
//      counted as
// list X = μ list xs,                                 1
//          optional (¬ DProp (xs = []))               1
//          ex p,                                      1
//          ex x,                                      1
//          ex xr,                                     1
//          constraintT (xs = x :: xr)                 1
//          sepT (block p (ly_size struct_list))       1
//          ownT p                                     1
//          structT struct_list [                      1
//             x @ T;                                  1
//             list  xr                                1
//          ]                                       -------
//                                                     11
typedef struct [[q::refined_by("xs : list X")]]
               [[q::typedef("list_t : optional (¬ DProp (xs = [])) (ex p x xr, { ownT p ... ∗∗ block p (ly_size struct_list) | xs = x :: xr})")]]
  list {
    [[q::field("x @ T")]]
    void *head;

    [[q::field("list_t xr")]]
    struct list *tail;
} *list_t;

// SKETCH: 1
[[q::returns("? @ list_t T")]]
list_t init () {
    return NULL;
}

// SKETCH: 4
[[q::parameters("p: loc")]]
[[q::parameters("xs: list X")]]
[[q::args("ownT p (xs @ list_t T)")]]
[[q::ensures("p ◁ₗ xs @ list_t T")]]
bool is_empty (list_t *l) {
    return *l == NULL;
}

// SKETCH: 1
[[q::returns("? @ list_t T")]]
list_t push (list_t p, void *e) {
    struct list *node = xmalloc(sizeof(struct list));
    node->head = e;
    node->tail = p;
    return node;
}

// SKETCH: 3
[[q::parameters("p: loc")]]
[[q::args("ownT p (? @ list_t T)")]]
[[q::ensures("p ◁ₗ ? @ list_t T")]]
void *pop (list_t *p) {
  if (*p == NULL) {
      return NULL;
  }

  struct list *node = *p;
  void *ret = node->head;
  *p = node->tail;
  free(node);
  return ret;
}

// SKETCH: 1
[[q::parameters("xs: list X")]]
list_t reverse (list_t p) {
    list_t w, t;
    w = NULL;

    // INV: 5 (2 exists, 2 type assignments, 1 constraint)
    [[q::constraints("∃ ys zs, w ◁ₗ ys @ list_t T ∗ p ◁ₗ zs @ list_t T ∗ ⌜xs = rev ys ++ zs⌝")]]
    while (p != NULL) {
        t = p->tail;
        p->tail = w;
        w = p;
        p = t;
    }
    return w;
}

// TOTAL SKETCH:  1 + 4 + 1 + 3 + 1 = 10
// TOTAL TYPE:    11
// TOTAL INV:     5
// TOTAL ANNOT:   0

/* start no count */
[[q::parameters("p: loc")]]
[[q::args("ownT p (? @ list_t T)")]]
[[q::ensures("p ◁ₗ ? @ list_t T")]]
void *pop_difficult (list_t *p) {
  if (*p == NULL) {
      return NULL;
  }
  void *ret = (*p)->head;
  list_t q = *p;
  *p = (*p)->tail;
  free(q);
  return ret;
}
/* end no count */
