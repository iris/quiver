#include <quiver.h>
#include <stddef.h>
#include <stdbool.h>
#include "../memory.h"


// Import memory specs from other case study
//@q::import all_proofs from quiver.examples.src.casestudies.memory


//@q::inlined Definition alloced_int := lamR (λ z: Z, ex p, owned p (int[size_t] z) ∗∗ block p (ly_size size_t)).

typedef struct [[q::refined_by("xs : list Z")]]
               [[q::typedef("list_t : optional (¬ DProp (xs = [])) (ex p x xr, { ownT p ... ∗∗ block p (ly_size struct_list) | xs = x :: xr})")]]
  list {
    [[q::field("x @ alloced_int")]]
    void *head;

    [[q::field("list_t xr")]]
    struct list *tail;
} *list_t;

[[q::returns("? @ list_t")]]
list_t init () {
    return NULL;
}

[[q::parameters("p: loc", "xs: list Z")]]
[[q::args("ownT p (xs @ list_t)")]]
[[q::ensures("p ◁ₗ xs @ list_t")]]
bool is_empty (list_t *l) {
    return *l == NULL;
}

[[q::returns("? @ list_t")]]
list_t push (list_t p, void *e) {
    struct list *node = xmalloc(sizeof(struct list));
    node->head = e;
    node->tail = p;
    return node;
}

[[q::parameters("p: loc")]]
[[q::args("ownT p (? @ list_t)")]]
[[q::ensures("p ◁ₗ ? @ list_t")]]
void *pop (list_t *p) {
  if (*p == NULL) {
      return NULL;
  }

  struct list *node = *p;
  void *ret = node->head;
  *p = node->tail;
  free(node);
  return ret;
}

[[q::parameters("xs: list Z")]]
list_t reverse (list_t p) {
    list_t w, t;
    w = NULL;

    [[q::constraints("∃ ys zs, w ◁ₗ ys @ list_t ∗ p ◁ₗ zs @ list_t ∗ ⌜xs = rev ys ++ zs⌝")]]
    while (p != NULL) {
        t = p->tail;
        p->tail = w;
        w = p;
        p = t;
    }
    return w;
}


void test(){
    list_t l = init();

    assert(is_empty(&l));

    size_t *x = xmalloc(sizeof(size_t));
    size_t *y = xmalloc(sizeof(size_t));
    size_t *z = xmalloc(sizeof(size_t));
    *x = 42;
    *y = 43;
    *z = 44;
    l = push(l, x);
    l = push(l, y);
    l = push(l, z);

    l = reverse(l);

    x = pop(&l);
    assert(*x == 42);
    free(x);

    y = pop(&l);
    assert(*y == 43);
    free(y);

    z = pop(&l);
    assert(*z == 44);
    free(z);

    assert(is_empty(&l));

}