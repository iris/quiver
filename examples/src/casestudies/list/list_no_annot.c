#include <quiver.h>
#include <stddef.h>
#include <stdbool.h>
#include "../memory.h"


// Import memory specs from other case study
//@q::import all_proofs from quiver.examples.src.casestudies.memory


typedef struct list {
    void *head;

    struct list *tail;
} *list_t;

list_t init () {
    return NULL;
}

bool is_empty (list_t *l) {
    return *l == NULL;
}

list_t push (list_t p, void *e) {
    struct list *node = xmalloc(sizeof(struct list));
    node->head = e;
    node->tail = p;
    return node;
}

void *pop (list_t *p) {
  if (*p == NULL) {
      return NULL;
  }

  struct list *node = *p;
  void *ret = node->head;
  *p = node->tail;
  free(node);
  return ret;
}


// TOTAL SKETCH:  0
// TOTAL TYPE:    0
// TOTAL INV:     0
// TOTAL ANNOT:   0

/* start no count */
void *pop_difficult (list_t *p) {
  if (*p == NULL) {
      return NULL;
  }
  void *ret = (*p)->head;
  list_t q = *p;
  *p = (*p)->tail;
  free(q);
  return ret;
}
/* end no count */
