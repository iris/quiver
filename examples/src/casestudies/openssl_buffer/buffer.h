/*
 * Copyright 1995-2018 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the Apache License 2.0 (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */
// This file contains code adapted from include/openssl/types.h and include/openssl/buffer.h in the OpenSSL project (https://github.com/openssl/openssl).

// FROM include/openssl/types.h
typedef struct buf_mem_st BUF_MEM;

// FROM include/openssl/buffer.h
#include "alloc.h"
#include <quiver.h>

#include <stddef.h>

#ifndef OPENSSL_NO_DEPRECATED_3_0
# define BUF_strdup(s) OPENSSL_strdup(s)
# define BUF_strndup(s, size) OPENSSL_strndup(s, size)
# define BUF_memdup(data, size) OPENSSL_memdup(data, size)
# define BUF_strlcpy(dst, sq, size)  OPENSSL_strlcpy(dst, sq, size)
# define BUF_strlcat(dst, sq, size) OPENSSL_strlcat(dst, sq, size)
# define BUF_strnlen(str, maxlen) OPENSSL_strnlen(str, maxlen)
#endif

//@q::import malloc from quiver.stdlib
//@q::import all_buffers from quiver.examples.src.casestudies.extra_proofs.openssl_buffer
//@q::import all_proofs from quiver.examples.src.casestudies.openssl_buffer.alloc


//@q::context `{!lib_malloc Σ}

struct
buf_mem_st {
    size_t length;              // current number of bytes
    char *data;
    size_t max;                 // size of buffer
    unsigned long flags;
};


#define BUF_MEM_FLAG_SECURE  0x01

BUF_MEM *BUF_MEM_new(void);
BUF_MEM *BUF_MEM_new_ex(unsigned long flags);
void BUF_MEM_free(BUF_MEM *a);
size_t BUF_MEM_grow(BUF_MEM *str, size_t len);
size_t BUF_MEM_grow_clean(BUF_MEM *str, size_t len);
void BUF_reverse(unsigned char *out, const unsigned char *in, size_t siz);

