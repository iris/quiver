/*
 * Copyright 1995-2023 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the Apache License 2.0 (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */
// This file contains code adapted from include/openssl/macros.h and include/openssl/err.h.in in the OpenSSL project (https://github.com/openssl/openssl).


// FROM include/openssl/macros.h
#ifndef OPENSSL_FILE
# ifdef OPENSSL_NO_FILENAMES
#  define OPENSSL_FILE ""
#  define OPENSSL_LINE 0
# else
#  define OPENSSL_FILE __FILE__
#  define OPENSSL_LINE __LINE__
# endif
#endif


// NOTE: modified from the original, frontend does not know strings
#ifndef OPENSSL_FUNC
#   define OPENSSL_FUNC ""
#endif


// FROM include/openssl/err.h.in
# define ERR_LIB_OFFSET                 23L
# define ERR_LIB_MASK                   0xFF
# define ERR_RFLAGS_OFFSET              18L
# define ERR_RFLAGS_MASK                0x1F
# define ERR_REASON_MASK                0X7FFFFF

# define ERR_RFLAG_FATAL                (0x1 << ERR_RFLAGS_OFFSET)
# define ERR_RFLAG_COMMON               (0x2 << ERR_RFLAGS_OFFSET)

# define ERR_LIB_BUF             7
# define ERR_R_PASSED_INVALID_ARGUMENT           (262|ERR_RFLAG_COMMON)

// FROM include/openssl/err.h.in
// error handling
/* 12 lines and some on an 80 column terminal */
#define ERR_MAX_DATA_SIZE       1024

/* Building blocks */
void ERR_new(void);
void ERR_set_debug(const char *file, int line, const char *func);
void ERR_set_error(int lib, int reason, const char *fmt, ...);

/* Main error raising functions */
# define ERR_raise(lib, reason) ERR_raise_data((lib),(reason),NULL)
# define ERR_raise_data(lib, reason, ptr)                           \
    do {                                                            \
        ERR_new();                                                  \
        ERR_set_error(lib, reason, ptr);                            \
    } while (0);

// NOTE: removed ERR_set_debug(OPENSSL_FILE,OPENSSL_LINE,OPENSSL_FUNC);
