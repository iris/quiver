/*
 * Copyright 1995-2023 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the Apache License 2.0 (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */
// This file contains code adapted from crypto/mem_clr.c and crypto/mem.c in the OpenSSL project (https://github.com/openssl/openssl).

#include <quiver.h>
#include <stddef.h>
#include "alloc.h"
#include "../memory.h"

// Import memory specs from other case study
//@q::import all_proofs from quiver.examples.src.casestudies.memory


/* start no count */
// FROM crypto/mem_clr.c
void OPENSSL_cleanse(void *ptr, size_t len)
{
    memset(ptr, 0, len);
}

// FROM crypto/mem.c
[[q::parameters("n: Z", "m: Z", "l: loc")]]
[[q::args("(owned l (any[m]))", "(int[size_t] n)")]]
[[q::requires("⌜n ≤ m⌝ ∗ block l m" )]]
[[q::returns("void")]]
[[q::exact_post]]
void CRYPTO_clear_free(void *str, size_t num)
{
    if (str == NULL)
        return;
    if (num)
        OPENSSL_cleanse(str, num);
    CRYPTO_free(str);
}


void *CRYPTO_clear_realloc(void *str, size_t old_len, size_t num)
{
    void *ret = NULL;

    if (str == NULL)
        return CRYPTO_malloc(num);

    if (num == 0)
    {
        CRYPTO_clear_free(str, old_len);
        return NULL;
    }

    /* Can't shrink the buffer since memcpy below copies |old_len| bytes. */
    if (num < old_len)
    {
        OPENSSL_cleanse((char *)str + num, old_len - num);
        return str;
    }

    ret = CRYPTO_malloc(num);
    if (ret != NULL)
    {
        memcpy(ret, str, old_len);
        CRYPTO_clear_free(str, old_len);
    }
    return ret;
}
/* end no count */