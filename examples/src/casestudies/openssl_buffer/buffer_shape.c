/*
 * Copyright 1995-2020 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the Apache License 2.0 (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */
// This file contains code adapted from crypto/buffer/buffer.c in the OpenSSL project (https://github.com/openssl/openssl).

// #include <stdio.h>
#include "buffer_shape.h"

// memcpy functions
#include <string.h>
#include <quiver.h>



/*
 * LIMIT_BEFORE_EXPANSION is the maximum n such that (n+3)/3*4 < 2**31. That
 * function is applied in several functions in this file and this limit
 * ensures that the result fits in an int.
 */
#define LIMIT_BEFORE_EXPANSION 0x5ffffffc

// SKETCH: 1
// ANNOT:  1
[[q::join_conj]]
BUF_MEM *BUF_MEM_new_ex(unsigned long flags)
{
    BUF_MEM *ret;

    ret = BUF_MEM_new();
    if (ret != NULL) {
        ret->flags = flags;

        q_assert("ret ◁ₗ (? @ buf)");
    }
    return ret;
}

// SKETCH: 1
BUF_MEM *BUF_MEM_new(void)
{
    BUF_MEM *ret;

    ret = OPENSSL_zalloc(sizeof(*ret));
    if (ret == NULL)
        return NULL;
    [[q::type("? @ buf")]]
    return ret;
}

// SKETCH: 1
[[q::args("? @ buf")]]
void BUF_MEM_free(BUF_MEM *a)
{
    if (a == NULL)
        return;
    if (a->data != NULL) {
        if (a->flags & BUF_MEM_FLAG_SECURE)
            OPENSSL_secure_clear_free(a->data, a->max);
        else
            OPENSSL_clear_free(a->data, a->max);
    }
    OPENSSL_free(a);
}

// SKETCH: 2
// ANNOT:  1
/* Allocate a block of secure memory; copy over old data if there
 * was any, and then free it. */
[[q::args("? @ buf", "_")]]
[[q::join_conj]]
static char *sec_alloc_realloc(BUF_MEM *str, size_t len)
{
    char *ret;

    ret = OPENSSL_secure_malloc(len);
    if (str->data != NULL) {
        if (ret != NULL) {
            memcpy(ret, str->data, str->length);
            OPENSSL_secure_clear_free(str->data, str->length);
            str->data = NULL;

            q_assert("ret ◁ₗ (owned ? (any[?]))");
        }
    }
    return ret;
}


// SKETCH: 2
// ANNOT:  1
[[q::args("(?, ?, true) @ buf", "_")]]
[[q::ensures("^str ◁ᵥ ? @ buf")]]
[[q::join_if]]
size_t BUF_MEM_grow(BUF_MEM *str, size_t len)
{
    char *ret;
    size_t n;

    if (str->length >= len) {
        str->length = len;
        return len;
    }
    if (str->max >= len) {
        if (str->data != NULL)
            memset(&str->data[str->length], 0, len - str->length);
        str->length = len;

        return len;
    }
    /* This limit is sufficient to ensure (len+3)/3*4 < 2**31 */
    if (len > LIMIT_BEFORE_EXPANSION) {
        ERR_raise(ERR_LIB_BUF, ERR_R_PASSED_INVALID_ARGUMENT);
        return 0;
    }
    n = (len + 3) / 3 * 4;
    if ((str->flags & BUF_MEM_FLAG_SECURE))
        ret = sec_alloc_realloc(str, n);
    else
        ret = OPENSSL_realloc(str->data, n);
    if (ret == NULL) {
        len = 0;
    } else {
        str->data = ret;
        str->max = n;
        memset(&str->data[str->length], 0, len - str->length);
        str->length = len;
    }
    return len;
}



// SKETCH: 2
// ANNOT:  1
[[q::args("(?, ?, true) @ buf", "_")]]
[[q::ensures("^str ◁ᵥ ? @ buf")]]
[[q::join_if]]
size_t BUF_MEM_grow_clean(BUF_MEM *str, size_t len)
{
    char *ret;
    size_t n;

    if (str->length >= len) {
        if (str->data != NULL)
            memset(&str->data[len], 0, str->length - len);
        str->length = len;
        return len;
    }
    if (str->max >= len) {
        memset(&str->data[str->length], 0, len - str->length);
        str->length = len;
        return len;
    }
    /* This limit is sufficient to ensure (len+3)/3*4 < 2**31 */
    if (len > LIMIT_BEFORE_EXPANSION) {
        ERR_raise(ERR_LIB_BUF, ERR_R_PASSED_INVALID_ARGUMENT);
        return 0;
    }
    n = (len + 3) / 3 * 4;
    if ((str->flags & BUF_MEM_FLAG_SECURE))
        ret = sec_alloc_realloc(str, n);
    else
        ret = OPENSSL_clear_realloc(str->data, str->max, n);
    if (ret == NULL) {
        len = 0;
    } else {
        str->data = ret;
        str->max = n;
        memset(&str->data[str->length], 0, len - str->length);
        str->length = len;
    }
    return len;
}


// TOTAL SKETCH:  1 + 1 + 1 + 2 + 2 + 2 = 9
// TOTAL TYPE:    14 (in "buffer_shape.h")
// TOTAL INV:     0
// TOTAL ANNOT:   4