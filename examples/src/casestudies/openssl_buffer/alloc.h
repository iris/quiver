/*
 * Copyright 1995-2023 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the Apache License 2.0 (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */
// This file contains code adapted from include/openssl/macros.h and include/openssl/crypto.h.in in the OpenSSL project (https://github.com/openssl/openssl).


// header file for the allocators OpenSSL uses
#include "macros.h"
#include <stddef.h>


// FROM include/openssl/macros.h
#ifndef OSSL_CRYPTO_ALLOC
# if defined(__GNUC__)
#  define OSSL_CRYPTO_ALLOC __attribute__((malloc))
# elif defined(_MSC_VER)
#  define OSSL_CRYPTO_ALLOC __declspec(restrict)
# else
#  define OSSL_CRYPTO_ALLOC
# endif
#endif

// FROM include/openssl/crypto.h.in
#define OPENSSL_malloc(num) \
        CRYPTO_malloc(num)
#define OPENSSL_zalloc(num) \
        CRYPTO_zalloc(num)
#define OPENSSL_realloc(addr, num) \
        CRYPTO_realloc(addr, num)
#define OPENSSL_clear_realloc(addr, old_num, num) \
        CRYPTO_clear_realloc(addr, old_num, num)
#define OPENSSL_clear_free(addr, num) \
        CRYPTO_clear_free(addr, num)
#define OPENSSL_free(addr) \
        CRYPTO_free(addr)
#define OPENSSL_memdup(str, s) \
        CRYPTO_memdup((str), s)
#define OPENSSL_strdup(str) \
        CRYPTO_strdup(str)
#define OPENSSL_strndup(str, n) \
        CRYPTO_strndup(str, n)
#define OPENSSL_secure_malloc(num) \
        CRYPTO_secure_malloc(num)
#define OPENSSL_secure_zalloc(num) \
        CRYPTO_secure_zalloc(num)
#define OPENSSL_secure_free(addr) \
        CRYPTO_secure_free(addr)
#define OPENSSL_secure_clear_free(addr, num) \
        CRYPTO_secure_clear_free(addr, num)
#define OPENSSL_secure_actual_size(ptr) \
        CRYPTO_secure_actual_size(ptr)


// QUIVER: We do not verify the OpenSSL memory management and, instead,
// we use the standard library memory management functions:

//@q::import all_proofs from quiver.examples.src.casestudies.memory

[[q::external_pt("malloc_pt")]]
OSSL_CRYPTO_ALLOC void *CRYPTO_malloc(size_t num);

[[q::external_pt("zalloc_pt")]]
OSSL_CRYPTO_ALLOC void *CRYPTO_zalloc(size_t num);

[[q::external_pt("memdup_pt")]]
OSSL_CRYPTO_ALLOC void *CRYPTO_memdup(const void *str, size_t siz);

[[q::external_pt("free_pt")]]
void CRYPTO_free(void *ptr);

// implemented in alloc.c
void CRYPTO_clear_free(void *ptr, size_t num);

[[q::external_pt("realloc_pt")]]
void *CRYPTO_realloc(void *addr, size_t num);

// implemented in alloc.c
void *CRYPTO_clear_realloc(void *addr, size_t old_num, size_t num);

[[q::external_pt("malloc_pt")]]
OSSL_CRYPTO_ALLOC void *CRYPTO_secure_malloc(size_t num);

// we redefine this to use the normal clear_free from alloc.c
#define CRYPTO_secure_clear_free CRYPTO_clear_free
