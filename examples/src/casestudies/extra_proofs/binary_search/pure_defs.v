
From lithium Require Import hooks simpl_classes simpl_instances normalize.
From refinedc.typing Require Import naive_simpl.
From quiver.base Require Import classes.
From quiver.thorium.types Require Import types base_types combinators pointers arrays refinements.

Local Open Scope Z.
Notation sorted := (StronglySorted (≤)).

Section binary_search_extra.

  Definition in_between (xs: list Z) (x: Z) (l r: Z): Prop :=
    (∀ a (i: nat), i < l → xs !! i = Some a → a < x) ∧
    (∀ a (i: nat), r ≤ i → xs !! i = Some a → x ≤ a).

  Global Instance in_between_narrow_left (xs: list Z) x l m l' r :
    Solve (
      in_between xs x l r
    ∧ sorted xs
    ∧ xs !!! (Z.to_nat m) < x
    ∧ l ≤ l' ≤ m + 1
    ∧ 0 ≤ m < length xs) →
    Solve (in_between xs x l' r).
  Proof.
    rewrite /Solve /in_between.
    intros ([Hl Hr] & Hs & Hlook & Hle & Hran); split; last done.
    intros a i ??. enough (a ≤ xs !!! Z.to_nat m) by lia.
    eapply (StronglySorted_lookup_le _ _ _ _ a (xs !!! Z.to_nat m)) in Hs as [|];
    eauto using list_lookup_lookup_total_lt with lia.
  Qed.

  Global Instance in_between_narrow_right xs x l m r r':
    Solve (
      in_between xs x l r
    ∧ sorted xs
    ∧ (¬ xs !!! (Z.to_nat m) < x)
    ∧ m ≤ r' ≤ r
    ∧ 0 ≤ m < length xs) →
    Solve (in_between xs x l r').
  Proof.
    rewrite /Solve /in_between.
    intros ([Hl Hr] & Hs & Hlook & Hle & Hran); split; first done.
    intros a i ??. enough (xs !!! Z.to_nat m ≤ a) by lia.
    eapply (StronglySorted_lookup_le _ _ _ _ (xs !!! Z.to_nat m) a) in Hs as [|];
    eauto using list_lookup_lookup_total_lt with lia.
  Qed.

  Global Instance in_between_init xs x l r:
    Solve (l = 0 ∧ r = length xs)  →
    Solve (in_between xs x l r).
  Proof.
    rewrite /in_between /Solve.
    intros [??]; split; intros; solve_solve.
  Qed.

End binary_search_extra.
Global Typeclasses Opaque in_between.
