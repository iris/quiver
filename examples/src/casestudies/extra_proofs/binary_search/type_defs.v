
From lithium Require Import hooks simpl_classes simpl_instances normalize.
From refinedc.typing Require Import naive_simpl.
From quiver.base Require Import classes.
From quiver.thorium.types Require Import types base_types combinators pointers arrays refinements.
From quiver.stdlib Require Export malloc.
From quiver.examples.src.casestudies.extra_proofs.binary_search Require Import pure_defs.

Section typedefs.
  Context `{!refinedcG Σ} `{!lib_malloc Σ}.
  (* counted as type definitions *)

  Definition sorted_arrayR_body (rec: list Z → type Σ) (xs: list Z) : type Σ :=
    ex p, { owned p (arrayT (intT i32) (ly_size i32) xs) ∗∗ block p (length xs * ly_size i32) | sorted xs}.

  Global Instance sorted_arrayR_body_mono: TypeMono (sorted_arrayR_body).
  Proof. solve_proper. Qed.

  Definition sorted_arrayR := fixR (sorted_arrayR_body).
End typedefs.