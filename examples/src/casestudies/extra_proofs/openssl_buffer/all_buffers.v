From quiver.thorium.typing Require Import typing.
From quiver.inference Require Import inferPT.
From quiver.thorium.types Require Import types functions.
Set Default Proof Using "Type".


Section lib_openssl_alloc.
  Context `{!refinedcG Σ}.

  (* OpenSSL types *)
  Definition ERR_new_pt := λ (ws : list val) (ret : type Σ → iProp Σ),
    (⌜ws = []⌝ ∗ ret (void)%RT)%I.

  Definition ERR_set_error_pt := λ (ws : list val) (ret : type Σ → iProp Σ),
    (∃ a b p : val, ⌜ws = [a; b; p]⌝ ∗
      (∃ i j : Z,
        nfs []
          [a ◁ᵥ int[i32] i; b ◁ᵥ int[i32] j; p ◁ᵥ null]
          (ret void)))%I.

End lib_openssl_alloc.
