From quiver.thorium.typing Require Import typing.
From quiver.inference Require Import inferPT.


Section prelude.

  Global Instance solve_list_to_set_swap {X: Type} `{Countable X} `{!Inhabited X} (xs: list X) (y: gset X) (i j : nat):
    sequence_classes [
        Solve (i < length xs);
        Solve (j < length xs);
        Solve (list_to_set xs = y) ] →
    Solve (list_to_set
    (<[i:=xs !!! j]>
      (<[j:=xs !!! i]> xs)) = y) | 1.
  Proof.
    rewrite /sequence_classes !Forall_cons Forall_nil /=.
    rewrite /Solve. intros (? & ? & ? & ?).
    rewrite list_to_set_swap //.
  Qed.

End prelude.


Section typedefs.
  Context `{!refinedcG Σ}.

  (* buffer *)
  Definition bufR := lamR (λ n: Z, ex p, owned p (any[ly_size i32 * n])).

  (* int array *)
  Definition int_arrayR := lamR (λ xs: list Z, ex p, owned p (arrayT (intT i32) (ly_size i32) xs)).

  (* set *)
  Definition setR := lamR (λ s: gset Z, ex xs, { { (ex p, ownT p (arrayT (intT i32) (ly_size i32) xs)) | list_to_set xs = s } | size s = length xs }).

  (* set with length *)
  Definition setLenR := lamR (λ s: Z * gset Z, ex xs, { { (ex p, owned p (arrayT (intT i32) (ly_size i32) xs)) | (list_to_set xs = s.2) } | (s.1 = length xs) }).

End typedefs.