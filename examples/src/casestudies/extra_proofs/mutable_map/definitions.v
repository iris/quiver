From lithium Require Import hooks simpl_classes simpl_instances normalize.
From refinedc.typing Require Import naive_simpl.

From quiver.thorium.typing Require Export typing.
From quiver.inference Require Export inferPT.

Set Default Proof Using "Type*".

(* NOTE: these are Lithium hooks.
  We instantiate them to run the proof scripts copied from RefinedC. *)
Ltac normalize_hook ::= normalize_autorewrite.
Ltac can_solve_hook ::= naive_solve.

Section defs.
  Inductive item_ref : Type := Empty | Entry (key : Z) (v : Z) | Tombstone (key : Z).
  Global Instance item_ref_inhabited : Inhabited item_ref := populate Empty.

  Definition build_item (tag key val : Z) : option item_ref :=
    match tag with
    | 0 => Some Empty
    | 1 => Some (Entry key val)
    | 2 => Some (Tombstone key)
    | _ => None
    end.

  Global Instance item_ref_empty_dec x: Decision (x = Empty).
  Proof. by destruct x; [left| right..]. Defined.

  Definition slot_for_key_ref (key : Z) (len : nat) : nat :=
    Z.to_nat (key `mod` len).

  Definition probe_ref_found (key : Z) (x : item_ref) : Prop :=
    match x with
    | Empty       => True
    | Entry k _
    | Tombstone k => key = k
    end.

  Global Instance probe_ref_found_decision key x : Decision (probe_ref_found key x).
  Proof. destruct x; simpl; apply _. Defined.

  (* TODO: define this via maybe? *)
  Definition item_ref_to_ty (x : item_ref) : option Z := if x is Entry _ ty then Some ty else None.
  Definition item_ref_set_ty (x : item_ref) (ty : Z) : item_ref := if x is Entry key _ then Entry key ty else x.
  Definition item_ref_to_key (x : item_ref) : option Z :=
    match x with
    | Entry key _ => Some key
    | Tombstone key => Some key
    | _ => None
    end.

  Definition probe_ref_go (key : Z) (items : list item_ref) : option (nat * item_ref) :=
    list_find (probe_ref_found key) items.
  Definition probe_ref (key : Z) (items : list item_ref) : option (nat * item_ref) :=
    let hash := slot_for_key_ref key (length items) in
    prod_map (λ idx, rotate_nat_add hash idx (length items)) id <$>
             (probe_ref_go key (rotate hash items)).

  Definition probe_ref_Empty_inv (key : Z) (items : list item_ref) : Prop :=
    (∀ i ir, items !! i = Some ir → item_ref_to_key ir = Some key → probe_ref key items = Some (i, ir)).

  Definition fsm_invariant (mp : gmap Z Z) (items : list item_ref) (count : Z) : Prop :=
    (∀ key ty, mp !! key = Some ty ↔ ∃ i, probe_ref key items = Some (i, Entry key ty)) ∧
    (∀ key, probe_ref_Empty_inv key items) ∧
    (1 < length items) ∧
    (count = length (filter (λ x, x = Empty) items)) ∧
    (0 < count).
  Local Typeclasses Opaque fsm_invariant.
  Local Typeclasses Opaque probe_ref.

  Local Instance simpl_both_list_find_Some A P x (l : list A) `{∀ x, Decision (P x)}:
    SimplBothRel (=) (list_find P l) (Some x) (l !! x.1 = Some x.2 ∧ P x.2 ∧ ∀ y : A, y ∈ take x.1 l → ¬ P y).
  Proof. unfold SimplBothRel. by rewrite list_find_Some'. Qed.

  Local Instance simpl_both_list_find_None A P (l : list A) `{∀ x, Decision (P x)}:
    SimplBothRel (=) (list_find P l) (None) (∀ y, y ∈ l → ¬ P y).
  Proof. unfold SimplBothRel. by rewrite list_find_None Forall_forall. Qed.


  (*** probe_ref_go lemmata *)
  Lemma probe_ref_go_next_take key items n:
    probe_ref_go key (take n items) = None →
    (∀ i, items !! n = Some i → ¬ probe_ref_found key i) →
    probe_ref_go key (take (S n) items) = None.
  Proof.
    move => Hl Hf. destruct (items !! n) as [i|] eqn:Hi.
    - rewrite (take_S_r _ _ i) //. move/list_find_None/Forall_forall in Hl.
      apply/list_find_None/Forall_forall. set_solver.
    - move/lookup_ge_None in Hi. rewrite firstn_all2; last lia. by rewrite firstn_all2 in Hl.
  Qed.
  (*** probe_ref lemmata *)

  Lemma probe_ref_lookup key l x:
    probe_ref key l = Some x ↔
    l !! x.1 = Some x.2 ∧ probe_ref_found key x.2 ∧ (x.1 < length l)%nat ∧ ∀ y, y ∈ rotate_take (slot_for_key_ref key (length l)) x.1 l → ¬ probe_ref_found key y.
  Proof.
    rewrite /probe_ref. move: x => [??]. split; naive_simpl.
    - revert select (_ ∈ rotate_take _ _ _). rewrite rotate_take_add //. eauto.
    - eexists (rotate_nat_sub (slot_for_key_ref key (length l)) _ (length l), _) => /=.
      rewrite rotate_nat_add_sub. 1: split_and! => //. 2: done.
      naive_simpl; rewrite ?rotate_nat_add_sub //; [apply rotate_nat_sub_lt|]; naive_solver lia.
  Qed.

  Local Instance simpl_both_probe_ref_Some key x l:
    SimplBothRel (=) (probe_ref key l) (Some x) (l !! x.1 = Some x.2 ∧ probe_ref_found key x.2 ∧ (x.1 < length l)%nat ∧ ∀ y, y ∈ rotate_take (slot_for_key_ref key (length l)) x.1 l → ¬ probe_ref_found key y).
  Proof. unfold SimplBothRel. by rewrite probe_ref_lookup. Qed.

  Lemma probe_ref_take_Some key n i items:
    probe_ref_go key (take n (rotate (slot_for_key_ref key (length items)) items)) = None →
    probe_ref_found key i →
    items !! rotate_nat_add (slot_for_key_ref key (length items)) n (strings.length items) = Some i →
    probe_ref key items = Some (rotate_nat_add (slot_for_key_ref key (length items)) n (length items), i).
  Proof.
    naive_simpl. move => ?. revert select (_ ∈ rotate_take _ _ _).
    rewrite rotate_take_add; naive_simpl; try naive_solver lia.
    apply dec_stable => Hlen. revert select (∀ y, y ∈ _ → _) => Hempty.
    move: (Hempty i). rewrite take_ge ?rotate_length; [|lia].
    rewrite elem_of_rotate elem_of_list_lookup. naive_solve.
  Qed.

  Lemma probe_ref_Empty_inv_upd n key key' items ir' ir:
    probe_ref_Empty_inv key' items →
    probe_ref key items = Some (n, ir') →
    item_ref_to_key ir = Some key →
    probe_ref_Empty_inv key' (<[n:=ir]> items).
  Proof.
    move => Hinv. rewrite /probe_ref_Empty_inv.
    move => Hprobe ? i ir0 /list_lookup_insert_Some Hl ?.
    have ?: (probe_ref_found key' ir0) by destruct ir0; naive_solver.
    case: Hl.
    - move: Hprobe. naive_simpl.
      destruct (decide (key = key')); subst; last by destruct ir'; naive_solver.
      revert select (_ ∈ _). rewrite rotate_take_insert;[|naive_solve..].
      case_decide => ?; naive_solver lia.
    - move => [? Hl]. feed pose proof (Hinv i ir0) as Hp => //.
      destruct (decide (key = key')); simplify_eq. move: Hl Hprobe Hp. naive_simpl.
      revert select (_ ∈ _). rewrite rotate_take_insert;[|naive_solve..].
      case_decide; last naive_solver.
      move => /(list_elem_of_insert1 _ _ _ _)[?|?]; subst => //; last by naive_solver.
        by destruct ir; naive_solver.
  Qed.

  Lemma probe_ref_find_entry key ty items :
    probe_ref_Empty_inv key items →
    Entry key ty ∈ items → ∃ i, probe_ref key items = Some (i, Entry key ty).
  Proof. move => Hinv /elem_of_list_lookup[i ?]. eexists. by apply Hinv. Qed.

  (*** fsm_invariant *)
  Lemma fsm_invariant_lookup mp items key n ir o count:
    fsm_invariant mp items count →
    probe_ref key items = Some (n, ir) →
    o = item_ref_to_ty ir →
    mp !! key = o.
  Proof.
    rewrite /fsm_invariant. move => [Hinv ?] Hp ?; subst. apply option_eq => ty. rewrite (Hinv key ty) Hp.
    move: Hp => /probe_ref_lookup/=?. destruct ir; naive_solver.
  Qed.

  Lemma fsm_invariant_init items (count : Z):
    (1 < length items)%nat →
    (∀ i, i ∈ items → i = Empty) →
    count = length items →
    fsm_invariant ∅ items count.
  Proof.
    move => ?? ->. have <-: replicate (length items) Empty = items by apply replicate_as_elem_of.
    split_and!.
    - move => key ?. rewrite lookup_empty. setoid_rewrite probe_ref_lookup => /=.
      setoid_rewrite lookup_replicate. naive_solver.
    - move => key ? ir /lookup_replicate[??]. by destruct ir.
    - naive_solve.
    - rewrite length_filter_replicate_True; naive_solve.
    - naive_solve.
  Qed.

  Lemma fsm_invariant_partial_alter key mp n items ir ir' f count count':
    fsm_invariant mp items count →
    probe_ref key items = Some (n, ir) →
    (ir' = Empty → ir = Empty) →
    probe_ref_found key ir' →
    (f (item_ref_to_ty ir) = item_ref_to_ty ir') →
    (if bool_decide (ir = Empty ∧ ir' ≠ Empty) then 1 < count else True) →
    count' = (if bool_decide (ir = Empty ∧ ir' ≠ Empty) then count - 1 else count) →
    fsm_invariant (partial_alter f key mp) (<[n:=ir']> items) count'.
  Proof.
    move => Hinv Hprobe Hempty Hkey Hf ? ->.
    move: (Hprobe) => /probe_ref_lookup/= [Hn [?[??]]].
    move: (Hinv) => [Hinv1 [Hinv2 [Hinv3 [Hinv4 Hinv5]]]].
    efeed pose proof fsm_invariant_lookup as Hmp => //. rewrite -Hmp in Hf.
    destruct (item_ref_to_key ir') as [?|] eqn:Hkey'. 2: {
      destruct ir', ir => //; simpl in *; [|naive_solver..].
      by rewrite list_insert_id // partial_alter_self_alt' // Hf Hmp.
    }
    split_and!.
    - move => key' ?. destruct (decide (key = key')) as [<-|?].
      + rewrite lookup_partial_alter Hf.
        assert (probe_ref key (<[n:=ir']> items) = Some (n, ir')) as ->. 2: destruct ir'; naive_solver.
        naive_simpl. revert select (_ ∈ _). rewrite rotate_take_insert;[case_decide|..]; naive_solver lia.
      + rewrite lookup_partial_alter_ne // Hinv1. f_equiv => i. split; naive_simpl.
        1: { rewrite list_lookup_insert_Some. naive_solver. }
        2: { revert select ( <[ _ := _ ]> _ !! _ = Some _). rewrite list_lookup_insert_Some. naive_solver. }
        2: revert select ( <[ _ := _ ]> _ !! _ = Some _); rewrite list_lookup_insert_Some => -[?|[??]]; [ naive_solver |].
        all: revert select (_ ∈ _); revert select (∀ y, y ∈ _ → _); rewrite rotate_take_insert ?insert_length; [|lia];
        case_decide;[|naive_solver lia].
        * move => ? /(list_elem_of_insert1 _ _ _ _)[?|?]; subst; destruct ir'; naive_solver.
        * move => Hx Hin' Hx'. apply: (Hx _ _ Hx'). apply: list_elem_of_insert2' => //.
          { rewrite lookup_take // lookup_rotate_l//. }
          move => ?. subst. destruct ir; [|naive_solver..].
          efeed pose proof (Hinv2 key' i) as Hi => //. move: Hi => /probe_ref_lookup[_ [_ [_ ]]]. by apply.
    - move => key'. apply: probe_ref_Empty_inv_upd => //.
      destruct ir' => //=; simpl in *; by simplify_eq.
    - naive_solve.
    - erewrite length_filter_insert; [|done]. repeat case_bool_decide; naive_solve.
    - repeat case_bool_decide; lia.
  Qed.

  Lemma fsm_invariant_insert key ty mp n items ir count count':
    fsm_invariant mp items count →
    probe_ref key items = Some (n, ir) →
    (if bool_decide (ir = Empty) then 1 < count else True) →
    count' = (if bool_decide (ir = Empty) then count - 1 else count) →
    fsm_invariant (<[key:=ty]> mp) (<[n:=Entry key ty]> items) count'.
  Proof.
    move => ????. apply: fsm_invariant_partial_alter; [done..| |]; repeat case_bool_decide; naive_solver.
  Qed.

  Lemma fsm_invariant_delete key mp n items ir ir' count:
    fsm_invariant mp items count →
    probe_ref key items = Some (n, ir) →
    item_ref_to_ty ir' = None →
    item_ref_to_key ir' = Some key ∨ item_ref_to_key ir' = item_ref_to_key ir →
    (ir = Empty → ir' = Empty) →
    fsm_invariant (delete key mp) (<[n:=ir']> items) count.
  Proof.
    move => ? Hprobe ???. move: (Hprobe) => /probe_ref_lookup[? [?[??]]].
    apply: fsm_invariant_partial_alter => //; destruct ir, ir'; naive_solver.
  Qed.


  Definition fsm_copy_entries (items : list item_ref) (i : nat) : gmap Z Z :=
    list_to_map (im←reverse (take i items); if im is Entry key ty then [(key, ty)] else []).

  Lemma fsm_copy_entries_id mp items count i :
    fsm_invariant mp items count →
    i = length items →
    fsm_copy_entries items i = mp.
  Proof.
    unfold fsm_copy_entries. move => [Hinv1 Hinv2] ->. apply map_eq => k. apply option_eq => ty.
    rewrite Hinv1 firstn_all -elem_of_list_to_map'; [| move => ?];
      rewrite !elem_of_list_bind; setoid_rewrite elem_of_reverse. 2: {
      move => [[|??|?]] [? Hin1] [[|??|?]] [? Hin2]; set_unfold => //. destruct_or!. simplify_eq.
      move: Hin1 Hin2 => /probe_ref_find_entry [//|??]; first naive_solver.
      move => /probe_ref_find_entry[//|??]; first naive_solver.
      by simplify_eq.
    }
    split.
    - move => [[|? ?|?]]; set_unfold => -[H1 ?]//. destruct_or!. simplify_eq. apply: probe_ref_find_entry; naive_solver.
    - move => [n /probe_ref_lookup]/=[/(elem_of_list_lookup_2 _ _ _)? ?]. eexists (Entry k ty). set_solver.
  Qed.


  Lemma fsm_copy_entries_not_add items i ir:
    items !! i = Some ir →
    (if ir is Entry _ _ then False else True) →
    fsm_copy_entries items (i + 1) = fsm_copy_entries items i.
  Proof.
    unfold fsm_copy_entries. move => Hl Hir. f_equal.
    rewrite -take_take_drop reverse_app bind_app /= (drop_S _ ir) //= app_nil_r.
    by destruct ir.
  Qed.

  Lemma fsm_copy_entries_add items i key ty:
    items !! i = Some (Entry key ty) →
    fsm_copy_entries items (i + 1) = <[key:=ty]> (fsm_copy_entries items i).
  Proof.
    unfold fsm_copy_entries. move => Hl.
    rewrite -take_take_drop reverse_app bind_app /= (drop_S _ (Entry key ty)) //= app_nil_r.
  Qed.

End defs.

Global Typeclasses Opaque probe_ref_go fsm_invariant probe_ref.

Section automation.
  Global Instance simplify_prune_assumption_probe_ref key items idx itm :
    simplify_prune_assumption (probe_ref key items = Some (idx, itm)) None.
  Proof. done. Qed.

  Global Instance simplify_prune_assumption_fsm_invariant mp items count :
    simplify_prune_assumption (fsm_invariant mp items count) None.
  Proof. done. Qed.

  Lemma probe_ref_total_lookup key items i itm:
    probe_ref key items = Some (i, itm) →
    items !!! i = itm.
  Proof. move => /probe_ref_lookup[/=??]. by apply list_lookup_total_correct. Qed.

  Global Instance solve_fsm_lookup mp items key val i itm tag key' count :
    TCFastDone (fsm_invariant mp items count) →
    TCFastDone (probe_ref key items = Some (i, itm)) →
    TCFastDone (build_item tag key' val = Some (items !!! Z.to_nat (Z.of_nat i))) →
    Solve (tag = 1) →
    Solve (default 0 (mp !! key) = val).
  Proof.
    rewrite Nat2Z.id /TCFastDone /Solve.
    move => ? Hprobe He ?. simplify_eq/=.
    erewrite fsm_invariant_lookup; [|done..].
    erewrite probe_ref_total_lookup in He; [|done].
    by simplify_eq.
  Qed.

  Global Instance solve_fsm_lookup_0 mp items key val i itm tag key' count :
    TCFastDone (fsm_invariant mp items count) →
    TCFastDone (probe_ref key items = Some (i, itm)) →
    TCFastDone (build_item tag key' val = Some (items !!! Z.to_nat (Z.of_nat i))) →
    Solve (tag ≠ 1) →
    Solve (default 0 (mp !! key) = 0).
  Proof.
    rewrite Nat2Z.id /TCFastDone /Solve.
    move => ? Hprobe He ?. simplify_eq/=.
    erewrite fsm_invariant_lookup; [|done..].
    erewrite probe_ref_total_lookup in He; [|done].
    unfold build_item in *. by repeat (case_match; simplify_eq/=).
  Qed.

  Global Instance solve_fsm_invariant_insert_entry_tomb mp items key val idx itm tag' key' val' count :
    TCFastDone (fsm_invariant mp items count) →
    TCFastDone (probe_ref key items = Some (idx, itm)) →
    TCFastDone (build_item tag' key' val' = Some (items !!! Z.to_nat (Z.of_nat idx))) →
    Solve (tag' ≠ 0) →
    Solve (fsm_invariant (<[key:=val]> mp) (<[Z.to_nat (Z.of_nat idx):=Entry key val]> items) count).
  Proof.
    rewrite Nat2Z.id /TCFastDone /Solve.
    move => ?? He ?.
    erewrite probe_ref_total_lookup in He; [|done].
    apply: fsm_invariant_insert; [done..| |]; case_bool_decide => //; simplify_eq.
    all: unfold build_item in *; by repeat (case_match; simplify_eq/=).
  Qed.

  Global Instance solve_fsm_invariant_insert_empty mp items key val idx itm tag' key' val' count :
    TCFastDone (fsm_invariant mp items count) →
    TCFastDone (probe_ref key items = Some (idx, itm)) →
    TCFastDone (build_item tag' key' val' = Some (items !!! Z.to_nat (Z.of_nat idx))) →
    Solve (tag' = 0) →
    Solve (1 < count) →
    Solve (fsm_invariant (<[key:=val]> mp) (<[Z.to_nat (Z.of_nat idx):=Entry key val]> items) (count - 1)).
  Proof.
    rewrite Nat2Z.id /TCFastDone /Solve.
    move => ?? He ??.
    erewrite probe_ref_total_lookup in He; [|done]. simplify_eq/=.
    by apply: fsm_invariant_insert.
  Qed.

  Global Instance solve_fsm_invariant_remove_entry mp items key idx itm tag' key' val' count :
    TCFastDone (fsm_invariant mp items count) →
    TCFastDone (probe_ref key items = Some (idx, itm)) →
    TCFastDone (build_item tag' key' val' = Some (items !!! Z.to_nat (Z.of_nat idx))) →
    Solve (tag' = 1) →
    Solve (fsm_invariant (delete key mp) (<[Z.to_nat (Z.of_nat idx):=Tombstone key]> items) count).
  Proof.
    rewrite Nat2Z.id /TCFastDone /Solve.
    move => ?? He ?.
    erewrite probe_ref_total_lookup in He; [|done]. simplify_eq/=.
    apply: fsm_invariant_delete; [done..| |]; naive_solver.
  Qed.

  Global Instance solve_fsm_invariant_remove_non_entry mp items key idx itm tag' key' val' count :
    TCFastDone (fsm_invariant mp items count) →
    TCFastDone (probe_ref key items = Some (idx, itm)) →
    TCFastDone (build_item tag' key' val' = Some (items !!! Z.to_nat (Z.of_nat idx))) →
    Solve (tag' ≠ 1) →
    Solve (fsm_invariant (delete key mp) items count).
  Proof.
    rewrite Nat2Z.id /TCFastDone /Solve.
    move => ? Hprobe He ?.
    erewrite probe_ref_total_lookup in He; [|done].
    move: (Hprobe) => /probe_ref_lookup[/=? _].
    rewrite -(list_insert_id items idx itm); [|done].
    apply: fsm_invariant_delete; [done.. | |naive_solver|done].
    unfold build_item in *; by repeat (case_match; simplify_eq/=).
  Qed.

  Global Instance simplify_pure_fsm_invariant_init n :
    simplify_pure true (fsm_invariant ∅ (replicate (Z.to_nat n) Empty) n) (1 < n).
  Proof.
    rewrite /simplify_pure => ?. eapply fsm_invariant_init; rewrite ?replicate_length; [lia| |lia].
    move => ?. rewrite elem_of_replicate. naive_solver.
  Qed.

  Definition probe_ref_at items key n :=
    ∃ x, probe_ref key items = Some (n, x) ∧ items !! n = Some x.

  Global Typeclasses Opaque probe_ref_at.

  Global Instance simplify_pure_slot_for_key_ref_lt key n :
    simplify_pure true (slot_for_key_ref key n < n) (0 < n).
  Proof. rewrite /simplify_pure /slot_for_key_ref. lia. Qed.

  Global Instance solve_unfold_slot_for_key key (len : nat) (x: Z):
    Solve (0 ≤ key) →
    Solve (0 < len) →
    Solve (key `rem` len = x) →
    Solve (Z.of_nat (slot_for_key_ref key len) = x).
  Proof.
    rewrite /Solve /slot_for_key_ref.
    move => ??. rewrite /slot_for_key_ref Z.rem_mod_nonneg //; lia.
  Qed.


  Global Instance rotate_add_lemma (base offset len: nat):
    simplify_pure true (rotate_nat_add base offset len = base) (base < len ∧ offset = 0%nat).
  Proof. intros [? ->]. rewrite rotate_nat_add_0 //. lia. Qed.

  Global Instance simplify_pure_lt_length base offset len :
    simplify_pure true (rotate_nat_add base offset len < len) (0 < len).
  Proof.
    rewrite /simplify_pure. intros ?.
    enough (rotate_nat_add base offset len < len)%nat by lia.
    eapply rotate_nat_add_lt; lia.
  Qed.

  Global Instance probe_ref_at_found items key offset:
    TCFastDone (probe_ref_go key (take offset (rotate (slot_for_key_ref key (length items)) items)) = None) →
    TCFastDone (rotate_nat_add (slot_for_key_ref key (length items)) offset (length items) < length items) →
    simplify_pure true (probe_ref_at items key (rotate_nat_add (slot_for_key_ref key (length items)) offset (length items))) (probe_ref_found key (items !!! rotate_nat_add (slot_for_key_ref key (length items)) offset (length items))).
  Proof.
    rewrite /TCFastDone.
    intros Hgo Hrot Hfound. rewrite /Solve /probe_ref_at.
    eexists. rewrite list_lookup_lookup_total_lt; last lia.
    split; last done. eapply probe_ref_take_Some; auto.
    rewrite list_lookup_lookup_total_lt //; last lia.
  Qed.

  Global Instance simplify_probe_ref_some items key offset n :
    TCFastDone (probe_ref_go key (take offset (rotate (slot_for_key_ref key (length items)) items)) = None) →
    TCFastDone (rotate_nat_add (slot_for_key_ref key (length items)) offset (length items) < length items) →
    simplify_pure true (probe_ref key items = Some (n, items !!! n))
      (n = rotate_nat_add (slot_for_key_ref key (length items)) offset (length items) ∧ probe_ref_found key (items !!! n)).
  Proof.
    rewrite /TCFastDone.
    intros Hgo Hrot [-> Hfound].
    eapply probe_ref_take_Some; auto.
    rewrite list_lookup_lookup_total_lt //; last lia.
  Qed.

  Global Instance probe_ref_found_solve_empty tag key key' (items: list item_ref) val i:
    TCFastDone (build_item tag key val = Some (items !!! Z.to_nat (Z.of_nat i))) →
    Solve (tag = 0) →
    Solve (probe_ref_found key' (items !!! i)).
  Proof.
    rewrite /Solve /TCFastDone /probe_ref_found Nat2Z.id. intros Hbi ->.
    rewrite /build_item in Hbi. simplify_eq. rewrite -Hbi //.
  Qed.

  Global Instance probe_ref_found_solve_entry tag key key' (items: list item_ref) val i:
    TCFastDone (build_item tag key val = Some (items !!! Z.to_nat (Z.of_nat i))) →
    Solve (tag = 1) →
    Solve (key = key') →
    Solve (probe_ref_found key' (items !!! i)).
  Proof.
    rewrite /Solve /TCFastDone /probe_ref_found Nat2Z.id. intros Hbi -> ->.
    rewrite /build_item in Hbi. simplify_eq. rewrite -Hbi //.
  Qed.

  Global Instance probe_ref_found_solve_tombstone tag key key' (items: list item_ref) val i:
    TCFastDone (build_item tag key val = Some (items !!! Z.to_nat (Z.of_nat i))) →
    Solve (tag = 2) →
    Solve (key = key') →
    Solve (probe_ref_found key' (items !!! i)).
  Proof.
    rewrite /Solve /TCFastDone /probe_ref_found Nat2Z.id. intros Hbi -> ->.
    rewrite /build_item in Hbi. simplify_eq. rewrite -Hbi //.
  Qed.

  Global Instance probe_ref_go_next_take_simplify_pure key n off items:
    TCFastDone (probe_ref_go key (take n (rotate off items)) = None) →
    simplify_pure true (probe_ref_go key (take (S n) (rotate off items)) = None) (¬ probe_ref_found key (items !!! (rotate_nat_add off n (length items)))).
  Proof.
    rewrite /TCFastDone /Solve /simplify_pure. intros Href Hnf.
    eapply probe_ref_go_next_take; first done.
    intros i Hlook. eapply lookup_rotate_r_Some in Hlook as [Hlook Hi].
    eapply list_lookup_total_correct in Hlook.
    by rewrite Hlook in Hnf.
  Qed.

  Global Instance probe_ref_found_not tag key key' (items: list item_ref) val i:
    TCFastDone (build_item tag key val = Some (items !!! Z.to_nat (Z.of_nat i))) →
    Solve (¬ tag = 0) →
    Solve (¬ (tag = 1 ∧ key = key')) →
    Solve (¬ (tag = 2 ∧ key = key')) →
    Solve (¬ probe_ref_found key' (items !!! i)).
  Proof.
    rewrite /Solve /TCFastDone /probe_ref_found Nat2Z.id. intros Hbi Hne1 Hne2 Hne3 Hfound.
    rewrite /build_item /= in Hbi.
    destruct tag as [|p|n]; simplify_eq.
    destruct p; simplify_eq.
    - destruct p; simplify_eq. rewrite -Hbi in Hfound. naive_solver.
    - rewrite -Hbi in Hfound. naive_solver.
  Qed.

  Global Instance simplify_pure_rotate_slot_for_key_advance (base offset offset' len: nat):
    Solve (0 < len) →
    simplify_pure true (
      rotate_nat_add base offset' len =@{Z}
      (rotate_nat_add base offset len + 1) `rem` len
    )
    (offset' = S offset).
  Proof.
    rewrite /simplify_pure /Solve. intros Hlt ->.
    rewrite rotate_nat_add_S; last lia. rewrite Nat2Z.inj_mod.
    rewrite Z.rem_mod_nonneg //=; lia.
  Qed.

  Global Instance fsm_invariant_items_length mp items count n:
    TCFastDone (fsm_invariant mp items count) →
    Solve (n ≤ 1) →
    Solve (n < length items).
  Proof.
    rewrite /TCFastDone /Solve /fsm_invariant. lia.
  Qed.

  Global Instance fsm_invariant_items_count mp items count:
    TCFastDone (fsm_invariant mp items count) →
    Solve (0 < count).
  Proof.
    rewrite /TCFastDone /Solve /fsm_invariant. lia.
  Qed.

  Global Instance simplify_prop_fsm_invariant mp mp' items items' i count:
    TCFastDone (fsm_invariant mp items count) →
    Solve (mp = fsm_copy_entries items' i) →
    simplify_pure true (fsm_invariant mp' items count) (fsm_copy_entries items' i = mp') | 1.
  Proof.
    rewrite /simplify_pure /TCFastDone /Solve. by intros ? -> <-.
  Qed.

  Global Instance fsm_copy_entries_done mp items count i :
    TCFastDone (fsm_invariant mp items count) →
    simplify_pure true (fsm_copy_entries items i = mp) (i = length items).
  Proof.
    rewrite /simplify_pure /TCFastDone /fsm_invariant.
    intros Hinv ->. eapply fsm_copy_entries_id; eauto.
  Qed.

  Global Instance fsm_copy_entries_no_change items tag key val i:
    TCFastDone (build_item tag key val = Some (items !!! Z.to_nat (Z.of_nat i))) →
    Solve (i < length items) →
    simplify_pure true (fsm_copy_entries items (i + 1) = fsm_copy_entries items i) (tag ≠ 1).
  Proof.
    rewrite /TCFastDone /simplify_pure /Solve Nat2Z.id.
    rewrite list_lookup_total_alt.
    intros Hbi Hlen Htag. assert (i < length items)%nat as [x Hlook]%lookup_lt_is_Some_2 by lia.
    rewrite Hlook /= in Hbi. eapply fsm_copy_entries_not_add; first done. destruct x; try done.
    revert Hbi. rewrite /build_item /=.
    destruct tag as [|tag |]; try done.
    destruct tag as [|tag |]; try done.
    destruct tag; done.
  Qed.

  Global Instance fsm_copy_entries_insert items tag key val i:
    TCFastDone (build_item tag key val = Some (items !!! Z.to_nat (Z.of_nat i))) →
    Solve (i < length items) →
    simplify_pure true (fsm_copy_entries items (i + 1) = <[key:=val]> (fsm_copy_entries items i)) (tag = 1).
  Proof.
    rewrite /TCFastDone /simplify_pure /Solve Nat2Z.id.
    intros Hbi Hlen Htag. eapply fsm_copy_entries_add. subst tag.
    rewrite /build_item in Hbi. simplify_eq. rewrite Hbi.
    eapply list_lookup_lookup_total_lt. lia.
  Qed.

End automation.

