#include <quiver.h>
#include "../memory.h"

// Import memory specs from other case study
//@q::import all_proofs from quiver.examples.src.casestudies.memory

// TYPE: 11
//     counted as
// vec_t = λ xs,                                             1
//        ex q,                                              1
//        sepT (block q (ly_size struct_vector))             1
//        ownT q                                             1
//        structT struct_vector [                            1
//          (ex p,                                           1
//           sepT (block p (ly_size i32 * (length xs)))      1
//           ownT p                                          1
//           arrayT[intT[i32]] xs);                          2
//          intT[i32] (length xs)                            1
//        ]                                               -------
//                                                           11
typedef struct
  [[q::refined_by("xs : list Z")]]
  [[q::typedef("vec_t : ex q, (owned q ...) ∗∗ (block q (ly_size struct_vector))")]]
  vector {
  [[q::field("ex p, (owned p (arrayT (λ x, int[i32] x) (ly_size i32) xs)) ∗∗ (block p (ly_size i32 * (length xs)))")]]
  int *data;
  [[q::field("(int[i32] (length xs))")]]
  int len;
} *vec_t;


// SKETCH: 1
vec_t mkvec(int n) {
  size_t s = sizeof(int) * (size_t)n;
  vec_t vec = xmalloc(sizeof(*vec));
  vec->data = xzalloc(s);
  vec->len = n;

  [[q::type("? @ vec_t")]] return vec;
}

// SKETCH: 2
[[q::requires("^vec ◁ᵥ ? @ vec_t")]]
[[q::ensures("^vec ◁ᵥ ? @ vec_t")]]
void get_unsafe(vec_t vec, int i, int *x) {
  *x = vec->data[i];
}

// SKETCH: 2
[[q::requires("^vec ◁ᵥ ? @ vec_t")]]
[[q::ensures("^vec ◁ᵥ ? @ vec_t")]]
void set_unsafe(vec_t vec, int i, int *x) {
  vec->data[i] = *x;
}

// SKETCH: 2
[[q::requires("^vec ◁ᵥ ? @ vec_t")]]
[[q::ensures("^vec ◁ᵥ ? @ vec_t")]]
int vec_grow(vec_t vec, int new_size) {
  if (vec == NULL) {
    return 0;
  }
  if (new_size <= vec->len) {
    return vec->len;
  }
  int *buf = xmalloc(sizeof(int) * new_size);
  memcpy(buf, vec->data, sizeof(int) * vec->len);
  free(vec->data);
  vec->data = buf;
  memset(&(vec->data[vec->len]), 0, sizeof(int) * (new_size - vec->len));
  vec->len = new_size;
  return vec->len;
}

// SKETCH: 2
[[q::requires("^vec ◁ᵥ ? @ vec_t")]]
[[q::ensures("^vec ◁ᵥ ? @ vec_t")]]
int get_checked(vec_t vec, int i) {
  assert (vec->len >= 0);
  if (i < 0 || i >= vec->len) {
    return -1;
  }
  return vec->data[i];
}


// SKETCH: 2
[[q::requires("^vec ◁ᵥ ? @ vec_t")]]
[[q::ensures("^vec ◁ᵥ ? @ vec_t")]]
void set_checked(vec_t vec, int i, int v) {
  assert (vec->len >= 0);
  if (i < 0 || i >= vec->len) {
    return;
  }
  vec->data[i] = v;
}

// SKETCH: 2
[[q::requires("^vec ◁ᵥ ? @ vec_t")]]
[[q::ensures("^vec ◁ᵥ ? @ vec_t")]]
void swap(vec_t vec, int i, int j) {
  if (j < 0 || j >= vec->len){
    return;
  }
  if (i >= vec->len || i < 0) {
    return;
  }

  int tmp = vec->data[i];
  vec->data[i] = vec->data[j];
  vec->data[j] = tmp;
}

// SKETCH: 1
[[q::requires("^vec ◁ᵥ ? @ vec_t")]]
void vec_free(vec_t vec) {
  free(vec->data);
  free(vec);
}

// TOTAL SKETCH:  1 + 2 + 2 + 2 + 2 + 2 + 2 + 1 = 14
// TOTAL TYPE:    11
// TOTAL INV:     0
// TOTAL ANNOT:   0


// Additional variations, not counted for evaluation
/* start no count */
[[q::requires("^vec ◁ᵥ ? @ vec_t")]]
[[q::ensures("^vec ◁ᵥ ? @ vec_t")]]
int vec_grow_realloc(vec_t vec, int new_size) {
  if (vec == NULL) {
    return 0;
  }
  if (new_size <= vec->len) {
    return vec->len;
  }
  vec->data = xrealloc(vec->data, sizeof(int) * new_size);
  memset(&(vec->data[vec->len]), 0, sizeof(int) * (new_size - vec->len));
  vec->len = new_size;
  return vec->len;
}

[[q::ensures("^vec ◁ᵥ ? @ vec_t")]]
int vec_grow_no_branch(vec_t vec, int new_size) {
  [[q::exists("n : Z", "xs : list Z")]]
  [[q::constraints("^vec ◁ᵥ xs @ vec_t", "^new_size ◁ᵥ intT i32 n", "⌜n > length xs⌝")]] _q_assert
  if (vec == NULL) {
    return 0;
  }
  if (new_size <= vec->len) {
    return vec->len;
  }
  int *buf = xmalloc(sizeof(int) * new_size);
  memcpy(buf, vec->data, sizeof(int) * vec->len);
  free(vec->data);
  vec->data = buf;
  memset(&(vec->data[vec->len]), 0, sizeof(int) * (new_size - vec->len));
  vec->len = new_size;
  return vec->len;
}

[[q::requires("^vec ◁ᵥ ? @ vec_t")]]
[[q::ensures("^vec ◁ᵥ ? @ vec_t")]]
[[q::join_if]]
[[q::join_conj]]
int get_checked_joined(vec_t vec, int i) {
  assert (vec->len >= 0);
  if (i < 0 || i >= vec->len) {
    return -1;
  }
  return vec->data[i];
}
[[q::args("_", "_", "intT i32 ?")]]
[[q::requires("^vec ◁ᵥ ? @ vec_t")]]
[[q::ensures("^vec ◁ᵥ ? @ vec_t")]]
[[q::join_if]]
[[q::join_conj]]
void set_checked_joined(vec_t vec, int i, int v) {
  assert (vec->len >= 0);
  if (i < 0 || i >= vec->len) {
    return;
  }
  vec->data[i] = v;
}

// versions with inline assertions
void get_unsafe_inline_assert(vec_t vec, int i, int *x) {
  q_assert("^vec ◁ᵥ ? @ vec_t");
  *x = vec->data[i];
  q_assert("^vec ◁ᵥ ? @ vec_t");
}

void set_unsafe_inline_assert(vec_t vec, int i, int *x) {
  q_assert("^vec ◁ᵥ ? @ vec_t"); vec->data[i] = *x; q_assert("^vec ◁ᵥ ? @ vec_t");
}
/* end no count */
