#include <quiver.h>
#include "../memory.h"

// Import memory specs from other case study
//@q::import all_proofs from quiver.examples.src.casestudies.memory

typedef struct
  vector {
  int *data;
  int len;
} *vec_t;


vec_t mkvec(int n) {
  size_t s = sizeof(int) * (size_t)n;
  vec_t vec = xmalloc(sizeof(*vec));
  vec->data = xzalloc(s);
  vec->len = n;

  return vec;
}

void get_unsafe(vec_t vec, int i, int *x) {
  *x = vec->data[i];
}

void set_unsafe(vec_t vec, int i, int *x) {
  vec->data[i] = *x;
}

int vec_grow(vec_t vec, int new_size) {
  if (vec == NULL) {
    return 0;
  }
  if (new_size <= vec->len) {
    return vec->len;
  }
  int *buf = xmalloc(sizeof(int) * new_size);
  memcpy(buf, vec->data, sizeof(int) * vec->len);
  free(vec->data);
  vec->data = buf;
  memset(&(vec->data[vec->len]), 0, sizeof(int) * (new_size - vec->len));
  vec->len = new_size;
  return vec->len;
}

int get_checked(vec_t vec, int i) {
  assert (vec->len >= 0);
  if (i < 0 || i >= vec->len) {
    return -1;
  }
  return vec->data[i];
}

void set_checked(vec_t vec, int i, int v) {
  assert (vec->len >= 0);
  if (i < 0 || i >= vec->len) {
    return;
  }
  vec->data[i] = v;
}

void swap(vec_t vec, int i, int j) {
  if (j < 0 || j >= vec->len){
    return;
  }
  if (i >= vec->len || i < 0) {
    return;
  }

  int tmp = vec->data[i];
  vec->data[i] = vec->data[j];
  vec->data[j] = tmp;
}

void vec_free(vec_t vec) {
  free(vec->data);
  free(vec);
}

// TOTAL SKETCH:  0
// TOTAL TYPE:    0
// TOTAL INV:     0
// TOTAL ANNOT:   0