#include "memory.h"

// malloc wrapper
// inspired by https://github.com/cyrusimap/cyrus-imapd/blob/master/lib/xmalloc.c
// and https://github.com/twitter/twemproxy/blob/master/src/nc_util.c

void *xmalloc(size_t size) {
    void *ptr = malloc(size);
    if (ptr == NULL) {
        abort();
    }
    return ptr;
}

void *xrealloc(void *ptr, size_t size) {
    void *new_ptr = realloc(ptr, size);
    if (new_ptr == NULL) {
        abort();
    }
    return new_ptr;
}

void *xzalloc(size_t size) {
    void *ptr = xmalloc(size);
    memset(ptr, 0, size);
    return ptr;
}

void *memdup(const void *ptr, size_t size) {
    void *new_ptr = malloc(size);
    if (new_ptr == NULL) {
        return NULL;
    }
    memcpy(new_ptr, ptr, size);
    return new_ptr;
}

void *xmemdup(const void *ptr, size_t size) {
    void *new_ptr = xmalloc(size);
    memcpy(new_ptr, ptr, size);
    return new_ptr;
}


void *zalloc(size_t size) {
    void *ptr = malloc(size);
    if (ptr == NULL) {
        return NULL;
    }
    memset(ptr, 0, size);
    return ptr;
}

// TOTAL SKETCH:  0
// TOTAL TYPE:    0
// TOTAL INV:     0
// TOTAL ANNOT:   0