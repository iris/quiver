# Quiver: Guided Abductive Inference of Separation Logic Specifications in Coq

This is the Coq development of Quiver, a technique for inferring specifications of C programs in separation logic while simultaneously proving that they are correct in Coq.
To guide Quiver toward the specification, it takes hints from the user in the form of *a specification sketch* and then completes the sketch using inference.
For the inference, Quiver uses an approach that we call *abductive deductive verification*, which abductively builds up the specification while, at the same time, deductively verifying programs.
You can find out more about how it works in [our PLDI'24 paper](https://plv.mpi-sws.org/quiver/).

## Overview of the Development

To install Quiver and use it to infer specifications, please follow the [Getting Started Guide](#getting-started) below. For a closer look, please consult the following documentation about Quiver:

- [OVERVIEW.md](docs/OVERVIEW.md) contains a mapping from the paper to the Coq code
- [ANNOTATIONS.md](docs/ANNOTATIONS.md) describes the annotation syntax of Quiver
- [OUTPUT.md](docs/OUTPUT.md) explains the specification output format of Quiver
- [CASESTUDIES.md](docs/CASESTUDIES.md) explains how to build case studies
- [TUTORIAL.md](docs/TUTORIAL.md) contains a tutorial explaining how to add a new case study to Quiver
- [automation.v](theories/tests/automation.v) contains a discussion of Quiver's type-class-based automation


## Getting Started

These installation instructions will install the Coq development locally on your machine using `opam`. They will do so in a new `opam` switch that is stored in this folder, thereby ensuring that none of your existing Coq developments are affected. Below we provide step-by-step instructions for the installation.

### Dependencies
Quiver has the following dependencies: `opam`, `gmp`, and `mpfr`.

We require [Opam](https://opam.ocaml.org) (version >= 2.0).
To check whether `opam` is installed, you can run `opam --version`.
You will need at least version `2.0.0`.
If you do not already have Opam installed then follow [these instructions](https://opam.ocaml.org/doc/Install.html).
If you have not previously initialized Opam on your system, please run
```
opam init
```

The frontend of Quiver builds on [Cerberus](https://github.com/rems-project/cerberus/), which requires the GMP and MPFR libraries.
On Debian/Ubuntu, they correspond to the `libgmp-dev` and `libmpfr-dev` packages (installed via `sudo apt-get install libgmp-dev libmpfr-dev`).
On macOS, they correspond to the `gmp` and `mpfr` Homebrew packages (installed via `brew install gmp mpfr`).

When using Apple Silicon with Homebrew, it may be necessary to remap the default directory where files are installed with the following commands:
```
export GMP_PREFIX=/opt/homebrew
export MPFR_PREFIX=/opt/homebrew
```

### Installation Instructions
To build the development in a new opam switch, we first create and initialize the switch with:
```
opam switch create . ocaml-base-compiler.4.14.0 --no-install
eval $(opam env)
opam repo add coq-released "https://coq.inria.fr/opam/released"
opam repo add iris-dev "https://gitlab.mpi-sws.org/iris/opam.git"
```

Then, we install Quiver and its Coq dependencies with:
```
make builddep OPAMFLAGS="-y" # install dependencies
make generate_all            # generate case study files
make                         # build Quiver Coq files (without case studies)
```

## Building Case Studies

Once Quiver is successfully installed, you can build case studies using the `./build.sh` script.
For example, you can build the Allocators case study using the command:
```
./build.sh examples/src/casestudies/memory.c
```

Once you have inferred specifications for a case study, you can compare them against the expected version with:
```
opam exec -- dune runtest output/casestudies/memory
```

For more information on how to build case studies and the build script itself, please see [the case studies document](docs/CASESTUDIES.md). For an explanation of how to add new case studies, please see [the tutorial](docs/TUTORIAL.md).

## Reproducing the Evaluation

To reproduce the results of the evaluation, you first need to install additional dependencies (for counting lines of C code and aggregating the results of different case studies), `python3`, `clang-format`, and `pip3`.
Once these dependencies are installed, you can find concrete instructions for each case study in [the case studies document](docs/CASESTUDIES.md).

**Python.**
For aggregating the metrics of the case studies, we use Python 3.
If it is not installed on your system, please consult the [Python website](https://www.python.org) for installation instructions.
For Python 3, we require the `tabulate` package, which can usually be installed using `pip3`.

**Clang Format and Tokei.**
To count the lines of C code, we format using `clang-format` and count using `tokei`.
To install them on your machine, you can most likely use your package manager for `clang-format` (e.g., `sudo apt-get install clang-format` or `brew install clang-format`) and install `tokei` from its [Github page](https://github.com/XAMPPRocky/tokei) (see the releases).
