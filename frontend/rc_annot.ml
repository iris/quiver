open Earley_core
open Earley_str
open Earley
open Extra

(** {3 Combinators and utilities} *)

type 'a quot_elt =
  | Quot_plain of string
  | Quot_anti  of 'a

type 'a quoted = 'a quot_elt list


(** [well_bracketed c_op c_cl anti_gr] is a grammar accepting strings starting
    with character [c_op], and ending with character [c_cl]. Moreover, strings
    with non-well-bracketed occurences of characters [c_op] / [c_cl] and ['{']
    / ['}'] are rejected. A sequence of the form ["!{text}"] is interpreted as
    an antiquotation. Its contents (here, ["text"]) is parsed using [anti_gr],
    an it should itself be well-bracketed in terms of ['{'] / ['}']. Note that
    the produced semantic value is a list of elements that can be either plain
    text (using the [Quot_plain(s)] constructor) or an anti-quotation (using a
    [Quot_anti(e)] constructor). *)
let well_bracketed : char -> char -> 'a grammar -> 'a quoted grammar =
    fun c_op c_cl anti_gr ->
  let fn buf pos =
    let elts = ref [] in
    let str = Buffer.create 20 in
    let flush_plain () =
      elts := (Quot_plain(Buffer.contents str)) :: !elts;
      Buffer.clear str
    in
    let flush_anti () =
      (*Printf.eprintf "PARSING ANTIQUOTATION\n%!";*)
      let text = Buffer.contents str in
      let anti =
        let parse = Earley.parse_string anti_gr Blanks.default in
        try parse text with Earley.Parse_error(_,_) ->
          assert false (* FIXME fail correctly *)
      in
      elts := (Quot_anti(anti)) :: !elts;
      Buffer.clear str
    in
    let rec loop state buf pos =
      let (c, next_buf, next_pos) = Input.read buf pos in
      (*
      begin
        Printf.eprintf "READING [%c] IN STATE " c;
        match state with
        | `Init(i)   -> Printf.eprintf "Init(%i)\n%!" i
        | `Bang(i)   -> Printf.eprintf "Bang(%i)\n%!" i
        | `Anti(k,i) -> Printf.eprintf "Anti(%i,%i)\n%!" k i
      end;
      *)
      match (c, state) with
      | ('\255', _       )               -> (* EOF, error. *)
          Earley.give_up ()
      | ('\\'  , _       )               -> (* Escape sequence. *)
          let c = Input.get next_buf next_pos in
          if not (List.mem c ['\255'; '"'; '\\']) then Earley.give_up ();
          (* We only need to remove the [`\\`] here. *)
          loop state next_buf next_pos;
      | (_     , `Init(i)  ) when c = c_op -> (* Normal mode opening. *)
          Buffer.add_char str c; loop (`Init(i+1)) next_buf next_pos
      | (_     , `Init(1)  ) when c = c_cl -> (* Normal mode final closing. *)
          flush_plain (); (next_buf, next_pos)
      | (_     , `Init(i)  ) when c = c_cl -> (* Normal mode closing. *)
          Buffer.add_char str c; loop (`Init(i-1)) next_buf next_pos
      | ('!'   , `Init(i)  )               -> (* Potential antiquotation. *)
          loop (`Bang(i)) next_buf next_pos
      | ('{'   , `Bang(i)  )               -> (* Actual antiquotation. *)
          flush_plain (); loop (`Anti(1,i)) next_buf next_pos
      | (_     , `Bang(i)  )               -> (* No antiquot. after all. *)
          Buffer.add_char str '!'; loop (`Init(i)) buf pos
      | ('{'   , `Anti(k,i))               -> (* Antiquot. operning. *)
          Buffer.add_char str c; loop (`Anti(k+1,i)) next_buf next_pos
      | ('}'   , `Anti(1,i))               -> (* Antiquot. final closing. *)
          flush_anti (); loop (`Init(i)) next_buf next_pos
      | ('}'   , `Anti(k,i))               -> (* Antiquot. closing. *)
          Buffer.add_char str '}'; loop (`Anti(k-1,i)) next_buf next_pos
      | (_     , _         )               -> (* Normal character. *)
          Buffer.add_char str c; loop state next_buf next_pos
    in
    let (buf, pos) = loop (`Init(1)) buf (pos + 1) in
    (List.rev !elts, buf, pos)
  in
  let name = Printf.sprintf "<%cwell-bracketed%c>" c_op c_cl in
  Earley.black_box fn (Charset.singleton c_op) false name

(** {3 Annotations AST} *)

type ident   = string
(* a tuple pattern of the shape [(.., .., ..)] *)
type pattern = ident list

(* an arbitrary Coq string, which will go entirely unparsed*)
type anystring = string

(* an iProp string *)
type raw_iprop = anystring

(* a literal Coq term *)
(* embed coq_term: a quoted type_expr. TODO what exactly does the anti-quotation do? In which case would we want to go back to type_expr? *)
type coq_term  = type_expr quoted


(* a literal Iris term *)
and  iris_term = type_expr quoted

and coq_expr =
  (* just a plain identifier *)
  | Coq_ident of string
  | Coq_all   of coq_term

(* this represents stuff in constraints (requires/ensures/...) *)
and constr =
  | Constr_Iris  of iris_term
  (* special support for existential quantifiers. TODO why? *)
  | Constr_exist of string * coq_expr option * constr
  (* type assignment for location *)
  | Constr_own   of string * ptr_kind * type_expr
  (* type assignment for value *)
  | Constr_val   of string * type_expr
  (* embedded Coq expression *)
  | Constr_Coq   of coq_expr
  (* global variable assignment *)
  | Constr_glob  of string * type_expr

and ptr_kind = Own | Shr | Frac of coq_expr

and type_expr =
  (* refinement type *)
  | Ty_refine of coq_expr * type_expr
  (* placeholder in ptr_type annotation *)
  | Ty_dots
  (* existential type *)
  | Ty_exists of pattern * coq_expr option * type_expr
  (* constr type *)
  | Ty_constr of type_expr * constr
  (* identifier instantiated with a list of Coq parameters*)
  | Ty_params of ident * type_expr_arg list
  | Ty_Coq    of coq_expr

(* Terms that can appear as part of the parameters of a gineric type *)
and type_expr_arg =
  | Ty_arg_expr   of type_expr
  | Ty_arg_lambda of pattern * coq_expr option * type_expr_arg

type annot_arg = int * int * coq_expr

(* Support for [^x] value syntax *)
let mangle_value_ident x =
  "__val_" ^ x

let value_regex = Str.regexp "\\^\\([A-Za-z_][A-Za-z_0-9]*\\)"
let replace_string_value (x : string) : string =
  let rec replace_rec (pos: int) (x: string) : string =
    try
      let new_pos = Str.search_forward value_regex x pos in
      let ik = Str.matched_group 1 x in
      let next_ident = mangle_value_ident ik in
      let x = Str.replace_first value_regex next_ident x in
      replace_rec new_pos x
    with
      Not_found -> x
  in
  replace_rec 0 x

(* Make a new symbol generator for fresh identifiers *)
let make_sym_new : unit -> ((unit -> string) * (unit -> string list)) = fun _ ->
  let count = ref 0 in
  let used_symbols = ref [] in
  ((fun _ ->
    let num = !count in
    count := 1 + num;
    let sym = "__" ^ (string_of_int num) in
    used_symbols := sym :: (!used_symbols);
    sym),
  fun _ ->
    !used_symbols)

(* Replace placeholder strings [?] with fresh identifiers. *)
let placeholder_regex = Str.regexp "\\?"
let replace_string_placeholder (mk_sym : unit -> string) (x : string) : string =
  let rec replace_rec (pos: int) (x: string) : string =
    try
      let new_pos = Str.search_forward placeholder_regex x pos in
      let next_ident = mk_sym() in
      let x = Str.replace_first placeholder_regex next_ident x in
      replace_rec new_pos x
    with
      Not_found -> x
  in
  replace_rec 0 x

(* Replace a placeholder string [?] in [x].
   [mk_sym] is used to generate fresh symbols. *)
let replace_quot_elt_placeholder (mk_sym: unit -> string) (replace_anti_placeholder: 'a -> 'a) (x : 'a quot_elt) : 'a quot_elt =
  match x with
  | Quot_plain(s) -> Quot_plain(replace_string_placeholder mk_sym s)
  | Quot_anti(a) -> Quot_anti(replace_anti_placeholder a)
let replace_quoted_placeholder (mk_sym: unit -> string) (replace_anti_placeholder: 'a -> 'a) (x : 'a quoted) : 'a quoted =
  List.map (replace_quot_elt_placeholder mk_sym replace_anti_placeholder) x
let rec replace_coq_term_placeholder (mk_sym: unit -> string) (t: coq_term) : coq_term =
  replace_quoted_placeholder mk_sym (replace_type_expr_placeholder mk_sym) t
and replace_coq_expr_placeholder (mk_sym: unit -> string) (t: coq_expr) : coq_expr =
  match t with
  | Coq_ident(i) -> Coq_ident(i)
  | Coq_all(t) -> Coq_all(replace_coq_term_placeholder mk_sym t)
and replace_iris_term_placeholder (mk_sym: unit -> string) (t: iris_term) : iris_term =
  replace_quoted_placeholder mk_sym (replace_type_expr_placeholder mk_sym) t
and replace_type_expr_placeholder (mk_sym: unit -> string) (t: type_expr) : type_expr =
  match t with
  | Ty_refine(e, t) ->
      Ty_refine(replace_coq_expr_placeholder mk_sym e, t)
  | Ty_dots ->
      Ty_dots
  | Ty_exists(pat, ty, t) ->
      Ty_exists(pat, Option.map (replace_coq_expr_placeholder mk_sym) ty, replace_type_expr_placeholder mk_sym t)
  | Ty_constr(t, c) ->
      Ty_constr(replace_type_expr_placeholder mk_sym t, replace_constr_placeholder mk_sym c)
  | Ty_params(i, params) ->
      Ty_params(i, List.map (replace_type_expr_arg_placeholder mk_sym) params)
  | Ty_Coq(t) ->
      Ty_Coq(replace_coq_expr_placeholder mk_sym t)
and replace_type_expr_arg_placeholder (mk_sym: unit -> string) (t: type_expr_arg) : type_expr_arg =
  match t with
  | Ty_arg_expr(t) ->
      Ty_arg_expr(replace_type_expr_placeholder mk_sym t)
  | Ty_arg_lambda(pat, ty, t) ->
      Ty_arg_lambda(pat, Option.map (replace_coq_expr_placeholder mk_sym) ty, replace_type_expr_arg_placeholder mk_sym t)
and replace_constr_placeholder (mk_sym: unit -> string) (t: constr) : constr =
  match t with
  | Constr_Iris(t) ->
      Constr_Iris(replace_iris_term_placeholder mk_sym t)
  | Constr_exist(i, ty, t) ->
      Constr_exist(i, Option.map (replace_coq_expr_placeholder mk_sym) ty, replace_constr_placeholder mk_sym t)
  | Constr_own(i, b, t) ->
      Constr_own(i, b, replace_type_expr_placeholder mk_sym t)
  | Constr_val(i, t) ->
      Constr_val(i, replace_type_expr_placeholder mk_sym t)
  | Constr_Coq(t) ->
      Constr_Coq(replace_coq_expr_placeholder mk_sym t)
  | Constr_glob(i, t) ->
      Constr_glob(i, replace_type_expr_placeholder mk_sym t)

let replace_anystring_placeholder (mk_sym: unit -> string) (t: anystring) : anystring =
  replace_string_placeholder mk_sym t

let postprocess_anystring (mk_sym: unit -> string) (t : anystring) : anystring =
  let s = replace_string_placeholder mk_sym t in
  replace_string_value s

(** {3 Main grammar definitions} *)

(** Identifier token (regexp ["[A-Za-z_]+"]). *)
let base_ident : ident Earley.grammar =
  let cs_first = Charset.from_string "A-Za-z_" in
  let cs = Charset.from_string "A-Za-z_0-9" in
  let fn buf pos =
    let nb = ref 1 in
    while Charset.mem cs (Input.get buf (pos + !nb)) do incr nb done;
    (String.sub (Input.line buf) pos !nb, buf, pos + !nb)
  in
  Earley.black_box fn cs_first false "<ident>"

let no_star =
  let fn buf pos = ((), Input.get buf pos <> '*') in
  Earley.test Charset.full fn

let parser ident =
  | id:base_ident no_star -> id
  | "void*"               -> "void*"

let parser ty_name =
  | id:base_ident       -> id
  | '&' - id:base_ident -> "&" ^ id

(** Integer token (regexp ["[0-9]+"]). *)
let integer : int Earley.grammar =
  let cs = Charset.from_string "0-9" in
  let fn buf pos =
    let nb = ref 1 in
    while Charset.mem cs (Input.get buf (pos + !nb)) do incr nb done;
    (int_of_string (String.sub (Input.line buf) pos !nb), buf, pos + !nb)
  in
  Earley.black_box fn cs false "<integer>"

let parser pattern =
  | "(" ")"                         -> []
  | x:ident                         -> [x]
  | "(" x:ident xs:{"," ident}+ ")" -> x :: xs

(** Arbitrary ("well-bracketed") string delimited by ['{'] and ['}']. *)
let parser coq_term  = (well_bracketed '{' '}' (type_expr `Full))

(** Arbitrary ("well-bracketed") string delimited by ['['] and [']']. *)
and parser iris_term = (well_bracketed '[' ']' (type_expr `Full))

and parser coq_expr =
  | x:ident    -> Coq_ident(x)
  | s:coq_term -> Coq_all(s)

and parser constr =
  | s:iris_term                                   -> Constr_Iris(s)
  | "∃" x:ident a:{":" coq_expr}? "." c:constr    -> Constr_exist(x,a,c)
  | c:coq_expr                                    -> Constr_Coq(c)
  | "global" x:ident ':' ty:(type_expr `Full)     -> Constr_glob(x,ty)
  | k:ptr_kind x:ident ':' ty:(type_expr `Full)   -> Constr_own(x, k ,ty)
  | x:ident ':' ty:(type_expr `Full)              -> Constr_val(x, ty)

and parser ptr_kind =
  | "own"             -> Own
  | "shr"             -> Shr
  | "frac" e:coq_expr -> Frac(e)

and parser ptr_type =
  | "&own<" ty:(type_expr `Full) ">"                 -> (Own    , ty)
  | "&shr<" ty:(type_expr `Full) ">"                 -> (Shr    , ty)
  | "&frac<" e:coq_expr "," ty:(type_expr `Full) ">" -> (Frac(e), ty)

and parser type_expr @(p : [`Atom | `Cstr | `Full]) =
  | c:coq_expr ty:{"@" (type_expr `Atom)}?
      when p >= `Atom ->
        begin
          match (c, ty) with
          | (Coq_ident(x), None    ) -> Ty_params(x,[])
          | (_           , None    ) -> Ty_Coq(c)
          | (_           , Some(ty)) -> Ty_refine(c,ty)
        end
  | id:ty_name "<" tys:type_args ">"
      when p >= `Atom -> Ty_params(id,tys)
  | "..."
      when p >= `Atom -> Ty_dots
  | "∃" p:pattern a:{":" coq_expr}? "." ty:(type_expr `Full)
      when p >= `Full -> Ty_exists(p,a,ty)
  | ty:(type_expr `Cstr) "&" c:constr
      when p >= `Cstr -> Ty_constr(ty,c)
  | "(" ty:(type_expr `Full) ")"
      when p >= `Atom -> ty

and parser type_expr_arg =
  | ty:(type_expr `Full)
      -> Ty_arg_expr(ty)
  | "λ" p:pattern a:{":" coq_expr}? "." tya:type_expr_arg
      -> Ty_arg_lambda(p,a,tya)

and parser type_args =
  | EMPTY                                   -> []
  | e:type_expr_arg es:{"," type_expr_arg}* -> e::es

let type_expr = type_expr `Full

let parser anystring =
  s:''.*'' ->
    s
    (*String.of_seq (List.to_seq s)*)

let make_pure_quotation s =
  let els = List.map (fun a -> match a with Quot_plain(s) -> s | Quot_anti(s) -> s) s in
  match els with
    | [el] -> "⌜" ^ el ^ "⌝"


let parser raw_iprop =
  | s:(well_bracketed '{' '}' anystring) ->
      make_pure_quotation s
  | s:anystring ->
    s

(** {3 Entry points} *)

(** {4 Annotations on type definitions} *)

let parser annot_parameter : (ident * anystring) Earley.grammar =
  | id:ident ":" s:anystring

let parser annot_refine : (ident * anystring) Earley.grammar =
  | id:ident ":" s:anystring

let parser annot_ptr_type : (ident * anystring) Earley.grammar =
  | id:ident ":" ty:anystring

(*let parser annot_type : ident Earley.grammar =*)
  (*| id:ident*)

(** {4 Annotations on structs} *)

let parser annot_size : coq_expr Earley.grammar =
  | c:coq_expr

let parser annot_exist : (ident * anystring) Earley.grammar =
  | id:ident ":" s:anystring

(* TODO: should have special syntax for pure escape at least *)
let parser annot_constr : anystring Earley.grammar =
  | c:raw_iprop

let parser annot_let : (ident * anystring option * anystring) Earley.grammar =
  | id:ident ty:{":" anystring}? "=" def:anystring

let parser annot_unfold_prio : int Earley.grammar =
  | i:integer

(** {4 Annotations on tagged unions} *)

type tag_spec = string * (string * coq_expr) list

let tagged_union : coq_expr Earley.grammar = coq_expr

let parser union_tag : tag_spec Earley.grammar =
  | c:ident l:{"(" ident ":" coq_expr ")"}*

(** {4 Annotations on fields} *)

let parser annot_field : anystring Earley.grammar =
  | ty:anystring

(** {4 Annotations on global variables} *)

let parser annot_global : type_expr Earley.grammar =
  | ty:type_expr

(** {4 Annotations on functions} *)

let parser annot_arg : anystring option Earley.grammar =
  | '_' -> None
  | ty:anystring -> Some(ty)

(* TODO should have special syntax for pure at least *)
let parser annot_requires : anystring Earley.grammar =
  | c:anystring

let parser annot_returns : anystring Earley.grammar =
  | ty:anystring

(* TODO syntax for pure *)
let parser annot_ensures : anystring Earley.grammar =
  | c:anystring

let parser annot_args : annot_arg Earley.grammar =
  | integer ":" integer coq_expr

let parser annot_external_pt : string Earley.grammar =
  | ident

type manual_proof = string * string * string (* Load path, module, lemma. *)

let parser annot_manual : manual_proof Earley.grammar =
  | f:ident fs:{"." ident}* ":" file:ident "," thm:ident ->
      (String.concat "." (f :: fs), file, thm)

(** {4 Annotations on statement expressions (ExprS)} *)

let parser annot_type : anystring Earley.grammar =
  | ty:anystring

(*
let parser annot : ... Earley.grammar =
*)

(** {4 Annotations on blocks} *)

let parser annot_inv_var : (ident * anystring) Earley.grammar =
  | id:ident ":" ty:anystring

(** {4 Type definition (in comments)} *)

type typedef = string * (string * coq_expr option) list * type_expr

let parser typedef_arg = ident {":" coq_expr}?

let parser typedef_args =
  | EMPTY                                   -> []
  | arg:typedef_arg args:{"," typedef_arg}* -> arg :: args

let parser typedef : typedef Earley.grammar =
  | id:ident args:{"<" typedef_args ">"}?[[]] "≔" ty:type_expr

(** {3 Parsing of attributes} *)

type annot =
  | Annot_parameters   of (ident * anystring) list
  | Annot_refined_by   of (ident * anystring) list
  | Annot_ptr_type     of (ident * anystring)
  | Annot_size         of coq_expr
  | Annot_exist        of (ident * anystring) list
  | Annot_lets         of (ident * anystring option * anystring) list
  | Annot_constraint   of anystring list
  | Annot_immovable
  | Annot_tagged_union of coq_expr
  | Annot_union_tag    of tag_spec
  | Annot_field        of anystring
  | Annot_global       of type_expr
  | Annot_args         of (anystring option) list
  | Annot_requires     of anystring list
  | Annot_returns      of anystring
  | Annot_type         of anystring
  | Annot_ensures      of anystring list
  | Annot_annot        of string
  | Annot_inv_vars     of (ident * anystring) list
  | Annot_annot_args   of annot_arg list
  | Annot_tactics      of string list
  | Annot_trust_me
  | Annot_skip
  | Annot_manual       of manual_proof
  | Annot_block
  | Annot_full_block
  | Annot_inlined
  | Annot_unfold_prio  of int
  (* annotation to remove this expression-statement because it's only there to attach annotations to it *)
  | Annot_remove
  | Annot_join_conj
  | Annot_join_if
  | Annot_exact_post
  | Annot_external_pt    of string

let annot_lemmas : string list -> string list =
  List.map (Printf.sprintf "all: try by apply: %s; solve_goal.")

let rc_locs : Location.Pool.t = Location.Pool.make ()

exception Invalid_annot of Location.t * string

let invalid_annot : type a. Location.t -> string -> a = fun loc msg ->
  raise (Invalid_annot(loc, msg))

let invalid_annot_no_pos : type a. string -> a = fun msg ->
  invalid_annot (Location.none rc_locs) msg

type rc_attr_arg =
  { rc_attr_arg_value  : string Location.located
  ; rc_attr_arg_pieces : string Location.located list }

let loc_of_pos : rc_attr_arg -> int -> Location.t = fun arg pos ->
  let open Location in
  let rec find pos pieces =
    match pieces with
    | []          -> assert false
    | p :: pieces ->
        if pos < String.length p.elt then (pos, p.loc)
        else find (pos - String.length p.elt) pieces
  in
  let (i, loc) = find pos arg.rc_attr_arg_pieces in
  match Location.get loc with
  | None    -> Location.none rc_locs
  | Some(d) ->
  let file = d.loc_file in
  let line = d.loc_line1 in
  let col = d.loc_col1 in
  (* FIXME unicode offset. *)
  Location.make file (line - 1) (col + i) (line - 1) (col + i) rc_locs

type rc_attr =
  { rc_attr_id   : string Location.located
  ; rc_attr_args : rc_attr_arg list }

let parse_attr : rc_attr -> annot = fun attr ->
  let {rc_attr_id = id; rc_attr_args = args} = attr in
  let error msg =
    invalid_annot id.loc (Printf.sprintf "Annotation [%s] %s." id.elt msg)
  in

  let parse : type a.a grammar -> rc_attr_arg -> a = fun gr arg ->
    let s = arg.rc_attr_arg_value in
    let parse_string = Earley.parse_string gr Blanks.default in
    try parse_string s.elt with Earley.Parse_error(buf,pos) ->
      let loc = loc_of_pos arg pos in
      invalid_annot loc "No parse in annotation."
  in

  let single_arg : type a.a grammar -> (a -> annot) -> annot = fun gr c ->
    match args with
    | [s] -> c (parse gr s)
    | _   -> error "should have exactly one argument"
  in

  let many_args : type a.a grammar -> (a list -> annot) -> annot = fun gr c ->
    match args with
    | [] -> error "should have at least one argument"
    | _  -> c (List.map (parse gr) args)
  in

  let raw_single_arg : (string -> annot) -> annot = fun c ->
    match args with
    | [a] -> c a.rc_attr_arg_value.elt
    | _   -> error "should have exactly one argument"
  in

  let raw_many_args : (string list -> annot) -> annot = fun c ->
    match args with
    | [] -> error "should have at least one argument"
    | _  -> c (List.map (fun a -> Location.(a.rc_attr_arg_value.elt)) args)
  in

  let no_args : annot -> annot = fun c ->
    match args with
    | [] -> c
    | _  -> error "should not have arguments"
  in

  match id.elt with
  | "parameters"   -> many_args annot_parameter (fun l -> Annot_parameters(l))
  | "refined_by"   -> many_args annot_refine (fun l -> Annot_refined_by(l))
  | "typedef"     -> single_arg annot_ptr_type (fun e -> Annot_ptr_type(e))
  | "size"         -> single_arg annot_size (fun e -> Annot_size(e))
  | "exists"       -> many_args annot_exist (fun l -> Annot_exist(l))
  | "let"          -> many_args annot_let (fun l -> Annot_lets(l))
  | "constraints"  -> many_args annot_constr (fun l -> Annot_constraint(l))
  | "immovable"    -> no_args Annot_immovable
  | "tagged_union" -> single_arg tagged_union (fun e -> Annot_tagged_union(e))
  | "union_tag"    -> single_arg union_tag (fun t -> Annot_union_tag(t))
  | "field"        -> single_arg annot_field (fun e -> Annot_field(e))
  | "global"       -> single_arg annot_global (fun e -> Annot_global(e))
  | "args"         -> many_args annot_arg (fun l -> Annot_args(l))
  | "requires"     -> many_args annot_requires (fun l -> Annot_requires(l))
  | "returns"      -> single_arg annot_returns (fun e -> Annot_returns(e))
  | "type"         -> single_arg annot_type (fun e -> Annot_type(e))
  | "ensures"      -> many_args annot_ensures (fun l -> Annot_ensures(l))
  | "annot"        -> raw_single_arg (fun e -> Annot_annot(e))
  | "inv_vars"     -> many_args annot_inv_var (fun l -> Annot_inv_vars(l))
  | "annot_args"   -> many_args annot_args (fun l -> Annot_annot_args(l))
  | "tactics"      -> raw_many_args (fun l -> Annot_tactics(l))
  | "lemmas"       -> raw_many_args (fun l -> Annot_tactics(annot_lemmas l))
  | "trust_me"     -> no_args Annot_trust_me
  | "skip"         -> no_args Annot_skip
  | "manual_proof" -> single_arg annot_manual (fun e -> Annot_manual(e))
  | "block"        -> no_args Annot_block
  | "full_block"   -> no_args Annot_full_block
  | "inlined"      -> no_args Annot_inlined
  | "unfold_prio"  -> single_arg annot_unfold_prio (fun i -> Annot_unfold_prio(i))
  | "remove"       -> no_args Annot_remove
  | "join_if"      -> no_args Annot_join_if
  | "join_conj"    -> no_args Annot_join_conj
  | "exact_post"   -> no_args Annot_exact_post
  | "external_pt"    -> single_arg annot_external_pt (fun e -> Annot_external_pt(e))
  | _              -> error "undefined"

(** {3 High level parsing of attributes} *)

type proof_kind =
  | Proof_normal
  | Proof_skipped (* Not even a spec is generated. *)
  | Proof_trusted
  | Proof_manual of manual_proof
  | Proof_inlined

type function_annot_pre =
{ fa_parameters : (ident * anystring) list
  ; fa_anon_parameters : (ident) list
  ; fa_args       : anystring option list
  ; fa_requires   : anystring list }

type function_annot_post =
{ fa_returns    : anystring option
  ; fa_exists     : (ident * anystring) list
  ; fa_anon_exists : (ident) list
  ; fa_ensures    : anystring list }

type function_spec = 
  | Spec_annot of (function_annot_pre option) * (function_annot_post option)
  | Spec_external of string
  | Spec_none

type function_annot =
  { fa_spec       : function_spec
  ; fa_tactics    : string list
  ; fa_proof_kind : proof_kind
  ; fa_join_conj  : bool
  ; fa_join_if    : bool
  ; fa_exact_post : bool
  }

let function_annot : rc_attr list -> function_annot = fun attrs ->
  let parameters = ref [] in
  let args = ref [] in
  let exists = ref [] in
  let returns = ref None in
  let requires = ref [] in
  let ensures = ref [] in
  let tactics = ref [] in
  let proof = ref Proof_normal in
  let inlined = ref false in
  let join_conj = ref false in
  let join_if = ref false in
  let exact_post = ref false in
  let external_spec = ref None in

  let has_pre = ref false in
  let has_post = ref false in
  let has_external_spec = ref false in

  let (univ_symbol_gen, univ_get_symbols) = make_sym_new () in
  let (ex_symbol_gen, ex_get_symbols) = make_sym_new () in

  let nb_attrs = ref 0 in
  let handle_attr ({rc_attr_id = id; _} as attr) =
    let error msg =
      invalid_annot id.loc (Printf.sprintf "Annotation [%s] %s." id.elt msg)
    in
    if !inlined then error "should be the only attribute";
    incr nb_attrs;
    match (parse_attr attr, !returns) with
    | (_                  , _  ) when !proof = Proof_skipped ->
        error "a skipped function should not have other annotations";
    | (Annot_skip         , _   ) ->
        if !proof <> Proof_normal then error "proof mode already specified";
        if !nb_attrs <> 1 then error "other annotations are given";
        proof := Proof_skipped
    | (Annot_trust_me     , _   ) ->
        if !proof <> Proof_normal then error "proof mode already specified";
        proof := Proof_trusted
    | (Annot_external_pt(s) , _   ) when (!has_pre = false && !has_post = false) ->
        if !has_external_spec then
            error "a function cannot have multiple external specs"
        else
          has_external_spec := true;
          external_spec := Some(s)
    | (Annot_external_pt(s) , _   ) ->
        error "a function with external specification should not have other spec annotations";
    | (Annot_manual(cfg)  , _   ) ->
        if !proof <> Proof_normal then error "proof mode already specified";
        proof := Proof_manual(cfg)
    | (Annot_parameters(l), _   ) when !has_external_spec = false ->
        has_pre := true;
        parameters := !parameters @ l
    | (Annot_args(l)      , _   ) when !has_external_spec = false ->
        let l = List.map (Option.map (postprocess_anystring univ_symbol_gen)) l in
        has_pre := true;
        args := !args @ l
    | (Annot_returns(ty)  , None) when !has_external_spec = false ->
        let ty = postprocess_anystring ex_symbol_gen ty in
        has_post := true;
        returns := Some(ty)
    | (Annot_returns(_)   , _   ) -> error "already specified"
    | (Annot_requires(l)  , _   ) when !has_external_spec = false ->
        let l = List.map (postprocess_anystring univ_symbol_gen) l in
        has_pre := true;
        requires := !requires @ l
    | (Annot_ensures(l)   , _   ) when !has_external_spec = false ->
        let l = List.map (postprocess_anystring ex_symbol_gen) l in
        has_post := true;
        ensures := !ensures @ l
    | (Annot_exist(l)     , _   ) when !has_external_spec = false ->
        has_post := true;
        exists := !exists @ l
    | (Annot_annot_args(_), _   ) -> () (* Handled separately. *)
    | (Annot_tactics(l)   , _   ) -> tactics := !tactics @ l
    | (Annot_inlined      , _   ) ->
        if !nb_attrs <> 1 then error "should be the only attribute";
        proof := Proof_inlined;
        inlined := true
    | (Annot_join_if      , _   ) ->
        join_if := true
    | (Annot_join_conj    , _   ) ->
        join_conj := true
    | (Annot_exact_post   , _   ) ->
        exact_post := true
    | (_                  , _   ) -> error "is invalid for a function"
  in
  List.iter handle_attr attrs;

  (* anonymous params become universally quantified *)
  let anon_params = univ_get_symbols () in

  (* anonymous exists become existentially quantified *)
  let anon_exists = ex_get_symbols () in

  let annot_pre =
    {  fa_parameters = !parameters
    ; fa_anon_parameters = anon_params
    ; fa_args       = !args
    ; fa_requires   = !requires }
  in
  let annot_post =
    { fa_returns    = !returns
    ; fa_exists     = !exists
    ; fa_anon_exists = anon_exists
    ; fa_ensures    = !ensures }
  in

  let spec = 
    match !external_spec with
    | Some(s) -> Spec_external(s)
    | None ->
        if !has_pre || !has_post then
          Spec_annot((if !has_pre then Some(annot_pre) else None), (if !has_post then Some(annot_post) else None))
        else Spec_none
  in

  { fa_spec = spec
  ; fa_tactics    = !tactics
  ; fa_proof_kind = !proof
  ; fa_join_if = !join_if
  ; fa_join_conj = !join_conj
  ; fa_exact_post = !exact_post }

let function_annot_args : rc_attr list -> annot_arg list = fun attrs ->
  let annot_args = ref [] in

  let handle_attr ({rc_attr_id = id; _} as attr) =
    if id.elt <> "annot_args" then () else
    match parse_attr attr with
    | Annot_annot_args(l) -> annot_args := !annot_args @ l
    | _                   -> assert false (* Unreachable. *)
  in
  List.iter handle_attr attrs;

  !annot_args

type member_annot =
  | MA_none
  | MA_field of anystring
  | MA_utag  of tag_spec

let member_annot : rc_attr list -> member_annot = fun attrs ->
  let annot = ref MA_none in

  let handle_attr ({rc_attr_id = id; _} as attr) =
    let error msg =
      invalid_annot id.loc (Printf.sprintf "Annotation [%s] %s." id.elt msg)
    in
    match (parse_attr attr, !annot) with
    | (Annot_field(ty)   , MA_none) -> annot := MA_field(ty)
    | (Annot_field(_)    , _      ) -> error "already specified"
    | (Annot_union_tag(s), MA_none) -> annot := MA_utag(s)
    | (Annot_union_tag(_), _      ) -> error "already specified"
    | (_                 , _      ) -> error "is invalid for a field"
  in
  List.iter handle_attr attrs; !annot

(* Raw expression annotations *)
type expr_annot = string option

let expr_annot : rc_attr list -> expr_annot = fun attrs ->
  let error msg =
    invalid_annot_no_pos (Printf.sprintf "Expression annotation %s." msg)
  in
  match attrs with
  | []      -> None
  | _::_::_ -> error "carries more than one attributes"
  | [attr]  ->
  match parse_attr attr with
  | Annot_annot(s) -> Some(s)
  | _              -> error "is invalid (wrong kind)"

(* Type expression annotations *)
type expr_type_annot =
  { et_exists   : (ident * anystring) list
  ; et_anon_exists : ident list
  ; et_constrs  : anystring list
  ; et_type     : anystring}

let expr_type_annot : rc_attr list -> expr_type_annot = fun attrs ->
  let exists = ref [] in
  let constrs = ref [] in
  let ty = ref None in

  let (symbol_gen, get_symbols) = make_sym_new () in

  let handle_attr ({rc_attr_id = id; _} as attr) =
    let error msg =
      invalid_annot id.loc (Printf.sprintf "Annotation [%s] %s." id.elt msg)
    in
    match parse_attr attr with
    | Annot_exist(l)      ->
        let l: (ident * anystring) list = List.map (fun (i, e) -> (i, postprocess_anystring symbol_gen e)) l in
        exists := !exists @ l
    | Annot_constraint(l) ->
        let l: anystring list = List.map (postprocess_anystring symbol_gen) l in
        constrs := !constrs @ l
    | Annot_type(t) ->
        if !ty = None then
          let t = postprocess_anystring symbol_gen t in
          ty := Some(t) else error "multiple types specified"
    | _                   -> error "is invalid (wrong kind)"
  in
  List.iter handle_attr attrs;

  (* get all the fresh symbols we introduced *)
  let anon_exists = get_symbols () in
  let ty =
    match !ty with
    | None -> invalid_annot_no_pos (Printf.sprintf "No type specified in expression type annotation.fa_returns")
    | Some ty -> ty
  in

  { et_exists = !exists; et_constrs = !constrs; et_anon_exists = anon_exists; et_type = ty }


type basic_struct_annot =
  { st_parameters  : (ident * anystring) list
  ; st_refined_by  : (ident * anystring) list
  ; st_pre_exists  : (ident * anystring) list
  ; st_exists      : (ident * anystring) list
  ; st_lets        : (ident * anystring option * anystring) list
  ; st_pre_constrs : anystring list
  ; st_constrs     : anystring list
  ; st_size        : coq_expr option
  ; st_ptr_type    : (ident * anystring) option
  ; st_immovable   : bool
  ; st_unfold_prio : int }

let default_basic_struct_annot : basic_struct_annot =
  { st_parameters  = []
  ; st_refined_by  = []
  ; st_pre_exists  = []
  ; st_exists      = []
  ; st_lets        = []
  ; st_pre_constrs = []
  ; st_constrs     = []
  ; st_size        = None
  ; st_ptr_type    = None
  ; st_immovable   = false
  ; st_unfold_prio = 100 }

(* Decides whether the annotation on the structure should lead to the
   definition of a RefinedC type. *)
let basic_struct_annot_defines_type : basic_struct_annot -> bool = fun annot ->
  annot.st_refined_by <> [] || annot.st_ptr_type <> None

type struct_annot =
  | SA_union
  | SA_basic    of basic_struct_annot
  | SA_tagged_u of coq_expr

let struct_annot : rc_attr list -> struct_annot = fun attrs ->
  let parameters = ref [] in
  let refined_by = ref [] in
  let pre_exists = ref [] in
  let exists = ref [] in
  let lets = ref [] in
  let pre_constrs = ref [] in
  let constrs = ref [] in
  let size = ref None in
  let ptr = ref None in
  let immovable = ref false in
  let tagged_union = ref None in
  let unfold_prio = ref None in

  let after_ptr = ref false in

  let handle_attr ({rc_attr_id = id; _} as attr) =
    let error msg =
      invalid_annot id.loc (Printf.sprintf "Annotation [%s] %s." id.elt msg)
    in
    let check_and_set r v =
      if !r <> None then error "already specified";
      r := Some(v)
    in
    match (parse_attr attr, !tagged_union) with
    (* Tagged union stuff. *)
    | (Annot_tagged_union(e), None   ) -> tagged_union := Some(e)
    | (Annot_tagged_union(e), Some(_)) -> error "already specified"
    (* Normal struct stuff. *)
    | (Annot_parameters(l)  , None   ) ->
        parameters := !parameters @ l
    | (Annot_refined_by(l)  , None   ) -> refined_by := !refined_by @ l
    | (Annot_exist(l)       , None   ) ->
        if !after_ptr then exists := !exists @ l else pre_exists := !pre_exists @ l
    | (Annot_lets(l)        , None   ) -> lets := !lets @ l
    | (Annot_constraint(l)  , None   ) ->
        if !after_ptr then constrs := !constrs @ l else pre_constrs := !pre_constrs @ l
    | (Annot_size(s)        , None   ) -> check_and_set size s
    | (Annot_ptr_type(e)    , None   ) ->
        after_ptr := true;
        check_and_set ptr e
    | (Annot_immovable      , None   ) ->
        if !immovable then error "already specified";
        immovable := true
    | (Annot_unfold_prio(i) , None   ) ->
         begin
           match !unfold_prio with
           | Some _ ->  error "already specified"
           | None -> unfold_prio := Some(i)
         end
    | (Annot_parameters(_)  , _      )
    | (Annot_refined_by(_)  , _      )
    | (Annot_exist(_)       , _      )
    | (Annot_constraint(_)  , _      )
    | (Annot_size(_)        , _      )
    | (Annot_ptr_type(_)    , _      )
    | (Annot_immovable      , _      ) ->
        error "is invalid for tagged unions"
    | (_                    , _      ) ->
        error "is invalid for a struct or a tagged union"
  in
  List.iter handle_attr attrs;

  match !tagged_union with
  | Some(e) -> SA_tagged_u(e)
  | None    ->
  let basic_annot =
    { st_parameters  = !parameters
    ; st_refined_by  = !refined_by
    ; st_pre_exists  = !pre_exists
    ; st_exists      = !exists
    ; st_lets        = !lets
    ; st_pre_constrs = !pre_constrs
    ; st_constrs     = !constrs
    ; st_size        = !size
    ; st_ptr_type    = !ptr
    ; st_immovable   = !immovable
    ; st_unfold_prio = Option.get 100 !unfold_prio }
  in
  SA_basic(basic_annot)

(** Parsing statement annotations *)
(* We accept the following clauses:
    [exists], describing existentially quantified variables with explicit type annotations;
    [vars], describing type assertions (potentially with holes) for local variables/arguments;
    [constrs], describing constraints (potentially with holes). *)
type stmt_annot =
  { sa_exists   : (ident * anystring) list
  ; sa_anon_exists : ident list
  ; sa_constrs  : anystring list }

let stmt_annot : rc_attr list -> stmt_annot = fun attrs ->
  let exists = ref [] in
  let constrs = ref [] in

  let (symbol_gen, get_symbols) = make_sym_new () in

  let handle_attr ({rc_attr_id = id; _} as attr) =
    let error msg =
      invalid_annot id.loc (Printf.sprintf "Annotation [%s] %s." id.elt msg)
    in
    match parse_attr attr with
    | Annot_exist(l)      ->
        let l: (ident * anystring) list = List.map (fun (i, e) -> (i, postprocess_anystring symbol_gen e)) l in
        exists := !exists @ l
    | Annot_constraint(l) ->
        let l: anystring list = List.map (postprocess_anystring symbol_gen) l in
        constrs := !constrs @ l
    | Annot_remove ->
        ()
    | _                   -> error "is invalid (wrong kind)"
  in
  List.iter handle_attr attrs;

  (* get all the fresh symbols we introduced *)
  let anon_exists = get_symbols () in

  { sa_exists = !exists; sa_constrs = !constrs; sa_anon_exists = anon_exists }

type global_annot =
  { ga_parameters : (ident * anystring) list
  ; ga_type       : type_expr }

let global_annot : rc_attr list -> global_annot option = fun attrs ->
  let typ = ref None in
  let parameters = ref [] in

  let handle_attr ({rc_attr_id = id; _} as attr) =
    let error msg =
      invalid_annot id.loc (Printf.sprintf "Annotation [%s] %s." id.elt msg)
    in
    match (parse_attr attr, !typ) with
    | (Annot_global(e)    , None) -> typ := Some e
    | (Annot_parameters(l), _   ) -> parameters := !parameters @ l
    | (Annot_global(_)    , _   ) -> error "already specified"
    | (_                  , _   ) -> error "is invalid for a global"
  in
  List.iter handle_attr attrs;

  match !typ with
  | Some(ty) -> Some {ga_parameters = !parameters; ga_type = ty}
  | None -> None
