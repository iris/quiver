open Format
open Extra
open Panic
open Coq_ast
open Rc_annot
open Comment_annot

(* Flags set by CLI. *)
let print_expr_locs = ref true
let print_stmt_locs = ref true
let no_mem_cast = ref false

let pp_str = pp_print_string

let pp_anystring : anystring pp =
  fun ff s -> Format.fprintf ff "%s" s

let pp_enclosed (l : string) (r : string) (pp : 'a pp) : 'a pp =
  fun ff s ->
    fprintf ff "%s%a%s" l pp s r

let pp_as_tuple : 'a pp -> 'a list pp = fun pp ff xs ->
  match xs with
  | []      -> pp_str ff "()"
  | [x]     -> pp ff x
  | x :: xs -> fprintf ff "(%a" pp x;
               List.iter (fprintf ff ", %a" pp) xs;
               pp_str ff ")"

let pp_encoded_patt_name : bool -> string list pp = fun used ff xs ->
  match xs with
  | []  -> pp_str ff (if used then "unit__" else "_")
  | [x] -> pp_str ff x
  | _   -> pp_str ff "patt__"

(* Print projection to get the [i]-th element of a tuple with [n] elements. *)
let rec pp_projection : int -> int pp = fun n ff i ->
  match n with
  | 1                -> ()
  | _ when i = n - 1 -> fprintf ff ".2"
  | _                -> fprintf ff ".1%a" (pp_projection (n - 1)) i

let pp_encoded_patt_bindings : string list pp = fun ff xs ->
  let nb = List.length xs in
  if nb <= 1 then () else
  let pp_let i x =
    fprintf ff "let %s := patt__%a in@;" x (pp_projection nb) i
  in
  List.iteri pp_let xs

let pp_sep : string -> 'a pp -> 'a list pp = fun sep pp ff xs ->
  match xs with
  | []      -> ()
  | x :: xs -> pp ff x; List.iter (fprintf ff "%s%a" sep pp) xs

let pp_as_prod : 'a pp -> 'a list pp = fun pp ff xs ->
  match xs with
  | [] -> pp_str ff "()"
  | _  -> pp_sep " * " pp ff xs

let pp_id_args : bool -> string -> string list pp = fun need_paren id ff xs ->
  if xs <> [] && need_paren then pp_str ff "(";
  pp_str ff id; List.iter (fprintf ff " %s") xs;
  if xs <> [] && need_paren then pp_str ff ")"

let pp_simple_coq_expr wrap ff coq_e =
  match coq_e with
  | Coq_ident(x)             -> pp_str ff x
  | Coq_all([Quot_plain(s)]) -> fprintf ff (if wrap then "(%s)" else "%s") s
  | _                        ->
  Panic.panic_no_pos "Antiquotation forbidden here." (* FIXME location *)

(** Print annotations *)
type rec_mode =
  | Rec_none
  | Rec_in_def of string
  | Rec_in_lem of string

let rec pp_quoted : type_expr pp -> type_expr quoted pp = fun pp_ty ff l ->
  let pp_quoted_elt ff e =
    match e with
    | Quot_plain(s) -> pp_str ff s
    | Quot_anti(ty) -> fprintf ff "(%a)" pp_ty ty
  in
  match l with
  | []     -> assert false (* Unreachable. *)
  | [e]    -> pp_quoted_elt ff e
  | e :: l -> fprintf ff "%a " pp_quoted_elt e; pp_quoted pp_ty ff l

and pp_coq_expr : bool -> type_expr pp -> coq_expr pp = fun wrap pp_ty ff e ->
  match e with
  | Coq_ident(x) -> pp_str ff x
  | Coq_all(l)   ->
      fprintf ff (if wrap then "(%a)" else "%a") (pp_quoted pp_ty) l

and pp_type_annot : type_expr pp -> coq_expr option pp = fun pp_ty ff eo ->
  Option.iter (fprintf ff " : %a" (pp_coq_expr false pp_ty)) eo

and pp_constr_rec : unit pp option -> rec_mode -> bool -> constr pp =
    fun pp_dots r wrap ff c ->
  let pp_ty = pp_type_expr_rec pp_dots r in
  let pp_coq_expr wrap = pp_coq_expr wrap pp_ty in
  let pp_constr = pp_constr_rec pp_dots r in
  let pp_kind ff k =
    match k with
    | Own     -> pp_str ff "◁ₗ"
    | Shr     -> pp_str ff "◁ₗ{Shr}"
    | Frac(e) -> fprintf ff "◁ₗ{%a}" (pp_coq_expr false) e
  in
  match c with
  (* Needs no wrapping. *)
  | Constr_Coq(e)       ->
      fprintf ff "⌜%a⌝" (pp_coq_expr false) e
  (* Apply wrapping. *)
  | _ when wrap         ->
      fprintf ff "(%a)" (pp_constr false) c
  (* No need for wrappin now. *)
  | Constr_Iris(l)      ->
      pp_quoted pp_ty ff l
  | Constr_exist(x,a,c) ->
      fprintf ff "∃ %s%a, %a" x (pp_type_annot pp_ty) a (pp_constr false) c
  | Constr_own(x,k,ty)  ->
      fprintf ff "%s %a %a" x pp_kind k pp_ty ty
  | Constr_val(x, ty)   ->
      fprintf ff "%s ◁ᵥ %a" x pp_ty ty
  | Constr_glob(x,ty)   ->
      fprintf ff "global_with_type %S Own %a" x pp_ty ty

and pp_type_expr_rec : unit pp option -> rec_mode -> type_expr pp =
    fun pp_dots r ff ty ->
  let pp_constr = pp_constr_rec pp_dots r in
  let rec pp_ty_annot ff a =
    pp_type_annot (pp false false) ff a
  and pp wrap rfnd ff ty =
    let pp_coq_expr wrap = pp_coq_expr wrap (pp false rfnd) in
    match ty with
    (* Don't need explicit wrapping. *)
    | Ty_Coq(e)          -> (pp_coq_expr wrap) ff e
    (* Remaining constructors (no need for explicit wrapping). *)
    | Ty_dots            ->
        begin
          match pp_dots with
          | None     -> Panic.panic_no_pos "Unexpected ellipsis."
          | Some(pp) ->
          fprintf ff (if wrap then "(@;  %a@;)" else "%a") pp ()
        end
    (* Insert wrapping if needed. *)
    | _ when wrap        -> fprintf ff "(%a)" (pp false rfnd) ty
    | Ty_refine(e,ty)    ->
        begin
          match (r, ty) with
          | (Rec_in_def(s), Ty_params(c,tys)) when c = s ->
              fprintf ff "self (%a" (pp_coq_expr true) e;
              List.iter (fprintf ff ", %a" (pp_arg true)) tys;
              fprintf ff ")"
          | (Rec_in_lem(s), Ty_params(c,tys)) when c = s ->
              fprintf ff "%a @@ " (pp_coq_expr true) e;
              if tys <> [] then pp_str ff "(";
              pp_str ff c;
              List.iter (fprintf ff " %a" (pp_arg true)) tys;
              if tys <> [] then pp_str ff ")"
          | (_              , _               )            ->
              fprintf ff "%a @@ %a" (pp_coq_expr true) e
                (pp true true) ty
        end
    | Ty_exists(xs,a,ty) ->
        fprintf ff "∃ₜ %a%a, %a%a" (pp_encoded_patt_name false) xs
          pp_ty_annot a pp_encoded_patt_bindings xs
          (pp false false) ty
    | Ty_constr(ty,c)    ->
        fprintf ff "constrained %a %a" (pp true false) ty
          (pp_constr true) c
    | Ty_params(id,tyas) ->
        let default () =
          pp_str ff id;
          List.iter (fprintf ff " %a" (pp_arg true)) tyas
        in
        match r with
        | Rec_in_def(s) when id = s ->
           (* We cannot use the ∃ₜ notation here as it hard-codes a
              rtype-to-type conversion.*)
            fprintf ff "tyexists (λ rfmt__, ";
            fprintf ff "self (rfmt__";
            List.iter (fprintf ff ", %a" (pp_arg true)) tyas;
            fprintf ff "))"
        | Rec_in_lem(s) when id = s ->
            fprintf ff "tyexists (λ rfmt__, ";
            fprintf ff "rfmt__ @@ ";
            default (); fprintf ff ")"
        | _                           ->
        match id with
        | "&frac"                  ->
            let (beta, ty) =
              match tyas with
              | [tya1; tya2] -> (tya1, tya2)
              | _            ->
                 Panic.panic_no_pos "[%s] expects two arguments." id
            in
            fprintf ff "&frac{%a} %a"
              (pp_arg false) beta (pp_arg true) ty
        | "optional" when not rfnd ->
            let (tya1, tya2) =
              match tyas with
              | [tya]        -> (tya, Ty_arg_expr(Ty_Coq(Coq_ident("null"))))
              | [tya1; tya2] -> (tya1, tya2)
              | _            ->
                 Panic.panic_no_pos "[%s] expects one or two arguments." id
            in
            let tya1 =
              Ty_arg_lambda([], Some(Coq_ident("unit")), tya1)
            in
            fprintf ff "optionalO %a %a" (pp_arg true) tya1
              (pp_arg true) tya2
        | "optional" | "optionalO" ->
           (match tyas with
           | [tya]        ->
               fprintf ff "%s %a null" id (pp_arg true) tya
           | [tya1; tya2] ->
               fprintf ff "%s %a %a" id (pp_arg true) tya1
                 (pp_arg true) tya2
           | _            ->
               Panic.panic_no_pos "[%s] expects one or two arguments." id)
        | "struct"                 ->
            let (tya, tyas) =
              match tyas with tya :: tyas -> (tya, tyas) | [] ->
              Panic.panic_no_pos "[%s] expects at least one argument." id
            in
            fprintf ff "struct %a [@@{type} %a ]"
              (pp_arg true) tya
              (pp_sep " ; " (pp_arg false)) tyas
        | _                        ->
            default ()
  and pp_arg wrap ff tya =
    match tya with
    | Ty_arg_expr(ty)         ->
        pp wrap false ff ty
    | Ty_arg_lambda(xs,a,tya) ->
        fprintf ff "(λ %a%a,@;  @[<v 0>%a%a@]@;)"
          (pp_encoded_patt_name false) xs
          pp_ty_annot a pp_encoded_patt_bindings xs
          (pp_arg false) tya
  in
  pp true false ff ty

let pp_type_expr = pp_type_expr_rec None Rec_none
let pp_constr = pp_constr_rec None Rec_none true

let pp_constrs : anystring list pp = fun ff cs ->
  match cs with
  | []      -> pp_str ff "True"
  | c :: cs -> pp_anystring ff c; List.iter (fprintf ff " ∗ %a" pp_anystring) cs

let rec pp_stmt_hint_prop_ex : stmt_hint_prop_ex pp = fun ff h ->
  let pp fmt = Format.fprintf ff fmt in
  match h with
  | SHintEx(i, ty, h) ->
      pp "∃{{ \"%s\" }} %s : %a, %a" i i pp_anystring ty pp_stmt_hint_prop_ex h
  | SHintExAnon(i, h) ->
      pp "∃? %s, %a" i pp_stmt_hint_prop_ex h
  | SHintBase(c) ->
      pp "%a : iProp Σ" pp_constrs c

let rec pp_stmt_hint_prop : stmt_hint_prop pp = fun ff h ->
  let pp fmt = Format.fprintf ff fmt in
  match h with
  | SHintAll(i, ty, h) ->
      pp "∀{{ \"%s\" }} %s : %a, %a" i i pp_anystring ty pp_stmt_hint_prop h
  | SHintLocal(i, h) ->
      pp "L{{ \"%s\" }} %s : loc, %a" i i pp_stmt_hint_prop h
  | SHintValue(i, h) ->
      pp "V{{ \"%s\" }} %s : val, %a" i (mangle_value_ident i) pp_stmt_hint_prop h
  | SHintAllBase(h) ->
      pp "%a" pp_stmt_hint_prop_ex h

let rec pp_expr_hint_prop_ex : expr_hint_prop_ex pp = fun ff h ->
  let pp fmt = Format.fprintf ff fmt in
  match h with
  | EHintEx(i, ty, h) ->
      pp "∃[{ \"%s\" }] %s : %a, %a" i i pp_anystring ty pp_expr_hint_prop_ex h
  | EHintExAnon(i, h) ->
      pp "∃?e %s, %a" i pp_expr_hint_prop_ex h
  | EHintBase(h, c) ->
      pp "EHintBase (%a) (%a : iProp Σ)" (pp_anystring) h pp_constrs c

let rec pp_expr_hint_prop : expr_hint_prop pp = fun ff h ->
  let pp fmt = Format.fprintf ff fmt in
  match h with
  | EHintAll(i, ty, h) ->
      pp "∀[{ \"%s\" }] %s : %a, %a" i i pp_anystring ty pp_expr_hint_prop h
  | EHintLocal(i, h) ->
      pp "L[{ \"%s\" }] %s : loc, %a" i i pp_expr_hint_prop h
  | EHintValue(i, h) ->
      pp "V[{ \"%s\" }] %s : val, %a" i (mangle_value_ident i) pp_expr_hint_prop h
  | EHintAllBase(h) ->
      pp "%a" pp_expr_hint_prop_ex h

let pp_assert_hint : assert_hint pp = fun ff h ->
  let pp fmt = Format.fprintf ff fmt in
  match h with
  | AssertHint(h) ->
      pp "CtxHintAnnot \"%s\"" h

let pp_expr_annot : Coq_ast.expr_annot pp = fun ff h ->
  let pp fmt = Format.fprintf ff fmt in
  match h with
  | ExprAnnotLit(h) ->
      pp "%a" (pp_simple_coq_expr false) h
  | ExprAnnotAssert(h) ->
      pp "%a" pp_assert_hint h


(** Print code *)
let pp_int_type : Coq_ast.int_type pp = fun ff it ->
  let pp fmt = Format.fprintf ff fmt in
  match it with
  | ItSize_t(true)      -> pp "ssize_t"
  | ItSize_t(false)     -> pp "size_t"
  | ItIntptr_t(true)    -> pp "intptr_t"
  | ItIntptr_t(false)   -> pp "uintptr_t"
  | ItPtrdiff_t         -> pp "ptrdiff_t"
  | ItI8(true)          -> pp "i8"
  | ItI8(false)         -> pp "u8"
  | ItI16(true)         -> pp "i16"
  | ItI16(false)        -> pp "u16"
  | ItI32(true)         -> pp "i32"
  | ItI32(false)        -> pp "u32"
  | ItI64(true)         -> pp "i64"
  | ItI64(false)        -> pp "u64"

let rec pp_layout : bool -> Coq_ast.layout pp = fun wrap ff layout ->
  let pp fmt = Format.fprintf ff fmt in
  match layout with
  | LVoid              -> pp "void_layout"
  | LBool              -> pp "bool_layout"
  | LPtr               -> pp "void*"
  | _ when wrap        -> pp "(%a)" (pp_layout false) layout
  | LStruct(id, false) -> pp "layout_of struct_%s" id
  | LStruct(id, true ) -> pp "ul_layout union_%s" id
  | LInt(i)            -> pp "it_layout %a" pp_int_type i
  | LArray(layout, n)  -> pp "mk_array_layout %a %s"
                            (pp_layout true) layout n

let rec pp_op_type : Coq_ast.op_type pp = fun ff ty ->
  let pp fmt = Format.fprintf ff fmt in
  match ty with
  | OpBool           -> pp "BoolOp"
  | OpInt(i)         -> pp "IntOp %a" pp_int_type i
  | OpPtr(_)         -> pp "PtrOp" (* FIXME *)
  | OpStruct(id, os) -> pp "StructOp struct_%s ([ %a ])" id (pp_sep " ; " pp_op_type) os
  | OpUntyped(ly)    -> pp "UntypedOp (%a)" (pp_layout false) ly

let pp_un_op : Coq_ast.un_op pp = fun ff op ->
  let pp fmt = Format.fprintf ff fmt in
  match op with
  | NotBoolOp  -> pp "NotBoolOp"
  | NotIntOp   -> pp "NotIntOp"
  | NegOp      -> pp "NegOp"
  | CastOp(ty) -> pp "(CastOp $ %a)" pp_op_type ty

let pp_bin_op : Coq_ast.bin_op pp = fun ff op ->
  pp_str ff @@
  match op with
  | AddOp     -> "+"
  | SubOp     -> "-"
  | MulOp     -> "×"
  | DivOp     -> "/"
  | ModOp     -> "%"
  | AndOp     -> "&"
  | OrOp      -> "|"
  | XorOp     -> "^"
  | ShlOp     -> "<<"
  | ShrOp     -> ">>"
  | EqOp      -> "="
  | NeOp      -> "!="
  | LtOp      -> "<"
  | GtOp      -> ">"
  | LeOp      -> "≤"
  | GeOp      -> "≥"
  | CommaOp   -> ","
  | LazyAndOp -> "&&"
  | LazyOrOp  -> "||"

let is_bool_result_op = fun op ->
  match op with
  | EqOp | NeOp | LtOp | GtOp | LeOp | GeOp -> true
  | LazyAndOp | LazyOrOp -> true
  | _ -> false

(* pretty-print annotations *)
let pp_annot : Coq_ast.stmt_annot pp = fun ff annot ->
  let pp fmt = Format.fprintf ff fmt in
  match annot with
  | StmtAnnotAssert(h) ->
      pp "%a" pp_assert_hint h


let rec pp_expr : Coq_ast.expr pp = fun ff e ->
  let pp fmt = Format.fprintf ff fmt in
  let pp_expr_body ff e =
    match Location.(e.elt) with
    | Var(None   ,_)                ->
        pp "\"_\""
    | Var(Some(x),g)                ->
        if g then fprintf ff "global_%s" x else fprintf ff "\"%s\"" x
    | Val(Null)                     ->
        pp "NULL"
    | Val(Void)                     ->
        pp "VOID"
    | Val(Int(s,it))                ->
        pp "i2v %s %a" s pp_int_type it
    | Val(SizeOf(ly))               ->
        pp "i2v (%a).(ly_size) %a" (pp_layout false) ly
          pp_int_type (ItSize_t false)
    | UnOp(op,ty,e)                 ->
        pp "UnOp %a (%a) (%a)" pp_un_op op pp_op_type ty pp_expr e
    | BinOp(op,ty1,ty2,e1,e2)       ->
        begin
          match (ty1, ty2, op) with
          (* Comma operator. *)
          | (_       , _       , CommaOp) ->
              pp "(%a) %a{%a, %a} (%a)" pp_expr e1 pp_bin_op op
                pp_op_type ty1 pp_op_type ty2 pp_expr e2
          (* Pointer offset operations. *)
          | (OpPtr(l), OpInt(_), AddOp  ) ->
              pp "(%a) at_offset{%a, PtrOp, %a} (%a)" pp_expr e1
                (pp_layout false) l pp_op_type ty2 pp_expr e2
          | (OpPtr(l), OpInt(_), SubOp  ) ->
              pp "(%a) at_neg_offset{%a, PtrOp, %a} (%a)" pp_expr e1
                (pp_layout false) l pp_op_type ty2 pp_expr e2
          (* Pointer difference. *)
          | (OpPtr(l1), OpPtr(l2), SubOp) ->
              pp "(%a) sub_ptr{%a, PtrOp, PtrOp} (%a)" pp_expr e1
                (pp_layout false) l1 pp_expr e2
          (* Pointer compared to 0 (Cerberus rejects non-0 integer values). *)
          | (OpInt(_) , OpPtr(l) , (EqOp | NeOp)) ->
              let e1 = {e1 with elt = UnOp(CastOp(ty2), ty1, e1)} in
              pp "(%a) %a{PtrOp, PtrOp, i32} (%a)" pp_expr e1
                pp_bin_op op pp_expr e2
          | (OpPtr(l) , OpInt(_) , (EqOp | NeOp)) ->
              let e2 = {e2 with elt = UnOp(CastOp(ty1), ty2, e2)} in
              pp "(%a) %a{PtrOp, PtrOp, i32} (%a)" pp_expr e1
                pp_bin_op op pp_expr e2
          (* Invalid operations mixing an integer and a pointer. *)
          | (OpPtr(_), OpInt(_), _      )
          | (OpInt(_), OpPtr(_), _      ) ->
              let loc = Location.to_cerb_loc e.loc in
              panic loc "Invalid use of binary operation [%a]." pp_bin_op op
          (* All other operations are defined. *)
          | _                             ->
              if is_bool_result_op op then
                pp "(%a) %a{%a, %a, i32} (%a)" pp_expr e1 pp_bin_op op
                  pp_op_type ty1 pp_op_type ty2 pp_expr e2
              else
                pp "(%a) %a{%a, %a} (%a)" pp_expr e1 pp_bin_op op
                  pp_op_type ty1 pp_op_type ty2 pp_expr e2
        end
    | Deref(atomic,ty,e)            ->
       if !no_mem_cast then
         if atomic then
           pp "!{%a, ScOrd, false} (%a)" pp_op_type ty pp_expr e
         else
           pp "!{%a, Na1Ord, false} (%a)" pp_op_type ty pp_expr e
        else
         if atomic then
           pp "!{%a, ScOrd} (%a)" pp_op_type ty pp_expr e
         else
           pp "!{%a} (%a)" pp_op_type ty pp_expr e
    | CAS(ty,e1,e2,e3)              ->
        pp "CAS@ (%a)@ (%a)@ (%a)@ (%a)" pp_op_type ty
          pp_expr e1 pp_expr e2 pp_expr e3
    | Call(e,es)        ->
      let pp_args _ es =
        let n = List.length es in
        let fn i e =
          pp (if i = n - 1 then "%a" else "%a ;@;") pp_expr e
        in
        List.iteri fn es
      in
      pp "Call (%a) [@@{expr} %a ]" pp_expr e pp_args es
    | IfE(ty,e1,e2,e3)              ->
        pp "IfE@ (%a)@ (%a)@ (%a)@ (%a)" pp_op_type ty
          pp_expr e1 pp_expr e2 pp_expr e3
    | SkipE(e)                      ->
        pp "SkipE (%a)" pp_expr e
    | Use(atomic,ty,e)              ->
       if !no_mem_cast then
         if atomic then
           pp "use{%a, ScOrd, false} (%a)" pp_op_type ty pp_expr e
         else
           pp "use{%a, Na1Ord, false} (%a)" pp_op_type ty pp_expr e
       else
         if atomic then
           pp "use{%a, ScOrd} (%a)" pp_op_type ty pp_expr e
         else
           pp "use{%a} (%a)" pp_op_type ty pp_expr e
    | AddrOf(e)                     ->
        pp "&(%a)" pp_expr e
    | LValue(e)                     ->
        pp "LValue (%a)" pp_expr e
    | GetMember(e,name,false,field) ->
        pp "(%a) at{struct_%s} %S" pp_expr e name field
    | GetMember(e,name,true ,field) ->
        pp "(%a) at_union{union_%s} %S" pp_expr e name field
    | OffsetOf(name,false,field)    ->
        pp "(OffsetOf (struct_%s) (%S))" name field
    | OffsetOf(name,true ,field)    ->
        pp "(OffsetOfUnion (union_%s) (%S))" name field
    | AnnotExpr(i,coq_e,e)          ->
        pp "AnnotExpr %i%%nat (%a) (%a)" i
          (pp_expr_annot) coq_e pp_expr e
    | Struct(id, fs)                ->
        pp "@[@[<hov 2>StructInit struct_%s [" id;
        let fn i (id, e) =
          let s = if i = List.length fs - 1 then "" else " ;" in
          pp "@;(%S, %a : expr)%s" id pp_expr e s
        in
        List.iteri fn fs;
        pp "@]@;]@]"
    | Macro(name, args, es, e)      ->
        pp "@[@[<hov 2>CheckedMacroE (%s %s) [" name (String.concat " " args);
        let fn i e =
          let s = if i = List.length es - 1 then "" else " ;" in
          pp "@;(%a : expr)%s" pp_expr e s
        in
        List.iteri fn es;
        pp "@]@;] (%a : expr)@]" pp_expr e
    | CopyAID(ot2, e1, e2)          ->
        pp "CopyAllocId (%a) (%a) (%a)" pp_op_type ot2 pp_expr e1 pp_expr e2
  in
  match Location.get e.loc with
  | Some(d) when !print_expr_locs ->
      pp "LocInfoE loc_%i (%a)" d.loc_key pp_expr_body e
  | _                             ->
      pp "%a" pp_expr_body e



let pp_if_cont : (string option) pp =
  fun ff opt ->
  let pp fmt = Format.fprintf ff fmt in
  begin
    match opt with
    | None -> pp "None"
    | Some lb -> pp "Some \"%s\"" lb
  end


let rec pp_stmt : Coq_ast.stmt pp = fun ff stmt ->
  let pp fmt = Format.fprintf ff fmt in
  if !print_stmt_locs then
    begin
      match Location.get stmt.loc with
      | None    -> ()
      | Some(d) -> pp "locinfo: loc_%i ;@;" d.loc_key
    end;
  match stmt.elt with
  | Goto(id)                      ->
      pp "Goto %S" id
  | GotoLoop(cond_id, cont_id, hint) ->
      pp "Loop %S %S %S" cond_id cont_id hint
  | Return(e)                     ->
      pp "Return @[<hov 0>(%a)@]" pp_expr e
  | Switch(it,e,map,bs,def)       ->
      pp "@[<v 2>Switch %a@;" pp_int_type it;
      pp "(%a)@;" pp_expr e;
      begin
        match map with
        | []         -> pp "∅@;"
        | (k,v)::map ->
        pp "@[<v 2>(@;<[ %s := %i%%nat ]> " k v;
        List.iter (fun (k,v) -> pp "$@;<[ %s := %i%%nat ]> " k v) map;
        pp "∅@]@;)@;"
      end;
      begin
        match bs with
        | []    -> pp "[]@;"
        | b::bs ->
        pp "@[<v 2>(@;(%a)" pp_stmt b;
        List.iter (pp " ::@;(%a)" pp_stmt) bs;
        pp " :: []@]@;)@;"
      end;
      pp "(%a)@]" pp_stmt def
  | Assign(atomic,ot,e1,e2,stmt) ->
      let order = if atomic then ", ScOrd" else "" in
      pp "@[<hov 2>%a <-{ %a%s }@ %a ;@]@;%a"
        pp_expr e1 pp_op_type ot order pp_expr e2 pp_stmt stmt
  | SkipS(stmt)                   ->
      pp_stmt ff stmt
  | If(lb_opt,ot,e,stmt1,stmt2)          ->
      pp "if{%a, %a}: @[<hov 0>%a@]@;then@;@[<v 2>%a@]@;else@;@[<v 2>%a@]"
            pp_op_type ot pp_if_cont lb_opt  pp_expr e pp_stmt stmt1 pp_stmt stmt2
  | Assert(ot,e,stmt)             ->
      pp "assert{%a}: (%a) ;@;%a" pp_op_type ot pp_expr e pp_stmt stmt
  | ExprS(annot, e, stmt)         ->
      Option.iter (Option.iter (pp "annot: (%s) ;@;")) annot;
      pp "expr: (%a) ;@;%a" pp_expr e pp_stmt stmt
  | AnnotS(annot, stmt)              ->
      pp "annot: (%a) ;@;%a" pp_annot annot pp_stmt stmt

type import = string * string

let pp_import ff (from, mod_name) =
  Format.fprintf ff "From %s Require Import %s.@;" from mod_name

let pp_code : string -> import list -> Coq_ast.t pp =
    fun root_dir imports ff ast ->
  (* Formatting utilities. *)
  let pp fmt = Format.fprintf ff fmt in

  (* Printing some header. *)
  pp "@[<v 0>From caesium Require Export notation.@;";
  pp "From caesium Require Import tactics.@;";
  pp "From quiver.base Require Import annotations.@;";
  List.iter (pp_import ff) imports;
  pp "Set Default Proof Using \"Type*\".@;@;";

  (* Printing generation data in a comment. *)
  pp "(* Generated from [%s]. *)@;" ast.source_file;

  (* Opening the section. *)
  pp "@[<v 2>Section code.";

  (* Printing of location data. *)
  if !print_expr_locs || !print_stmt_locs then
    begin
      let (all_locations, all_files) =
        let open Location in
        let locs = ref [] in
        let files = ref [] in
        let fn ({loc_file = file; _} as d) =
          locs := d :: !locs;
          if not (List.mem file !files) then files := file :: !files
        in
        Location.Pool.iter fn coq_locs;
        let locs = List.sort (fun d1 d2 -> d1.loc_key - d2.loc_key) !locs in
        let files = List.mapi (fun i s -> (s, i)) !files in
        (locs, files)
      in
      let pp_file_def (file, key) =
        let file =
          try Filename.relative_path root_dir file
          with Invalid_argument(_) -> file
        in
        fprintf ff "@;Definition file_%i : string := \"%s\"." key file
      in
      List.iter pp_file_def all_files;
      let pp_loc_def d =
        let open Location in
        pp "@;Definition loc_%i : location_info := " d.loc_key;
        pp "LocationInfo file_%i %i %i %i %i."
          (List.assoc d.loc_file all_files)
          d.loc_line1 d.loc_col1 d.loc_line2 d.loc_col2
      in
      List.iter pp_loc_def all_locations;
    end;

  (* Printing for struct/union members. *)
  let pp_members members is_struct =
    let nb_bytes = ref 0 in
    let n = List.length members in
    let fn i (id, (attrs, (align, size), layout)) =
      (* Insert padding for field alignment (for structs). *)
      if is_struct && !nb_bytes mod align <> 0 then
        begin
          let pad = align - !nb_bytes mod align in
          pp "@;(None, Layout %i%%nat 0%%nat);" pad;
          nb_bytes := !nb_bytes + pad;
        end;
      let sc = if i = n - 1 then "" else ";" in
      let some = if is_struct then "Some " else "" in
      pp "@;(%s%S, %a)%s" some id (pp_layout false) layout sc;
      nb_bytes := !nb_bytes + size
    in
    List.iteri fn members;
    (* Insert final padding if necessary. *)
    if is_struct then
      begin
        let max_align =
          let fn acc (_,(_,(align,_),_)) = max acc align in
          List.fold_left fn 1 members
        in
        let r = !nb_bytes mod max_align in
        if r <> 0 then pp ";@;(None, Layout %i%%nat 0%%nat)" (max_align - r)
      end
  in

  (* Definition of structs/unions. *)
  let pp_struct (id, decl) =
    pp "\n@;(* Definition of struct [%s]. *)@;" id;
    pp "@[<v 2>Program Definition struct_%s := {|@;" id;

    pp "@[<v 2>sl_members := [";
    pp_members decl.struct_members true;
    pp "@]@;];@]@;|}.@;";
    pp "Solve Obligations with solve_struct_obligations."
  in
  let pp_union (id, decl) =
    pp "\n@;(* Definition of union [%s]. *)@;" id;
    pp "@[<v 2>Program Definition union_%s := {|@;" id;

    pp "@[<v 2>ul_members := [";
    pp_members decl.struct_members false;
    pp "@]@;];@]@;|}.@;";
    pp "Solve Obligations with solve_struct_obligations."
  in
  let rec sort_structs found strs =
    match strs with
    | []                     -> []
    | (id, s) as str :: strs ->
    if List.for_all (fun id -> List.mem id found) s.struct_deps then
      str :: sort_structs (id :: found) strs
    else
      sort_structs found (strs @ [str])
  in
  let pp_struct_union ((_, {struct_is_union; _}) as s) =
    if struct_is_union then pp_union s else pp_struct s
  in
  List.iter pp_struct_union (sort_structs [] ast.structs);

  (* Definition of functions. *)
  let pp_function_def (id, def) =
    let deps = fst def.func_deps @ snd def.func_deps in
    pp "\n@;(* Definition of function [%s]. *)@;" id;
    pp "@[<v 2>Definition impl_%s " id;
    if deps <> [] then begin
      pp "(";
      List.iter (pp "global_%s ") deps;
      pp ": loc)";
    end;
    pp ": function := {|@;";

    pp "@[<v 2>f_args := [";
    begin
      let n = List.length def.func_args in
      let fn i (id, layout) =
        let sc = if i = n - 1 then "" else ";" in
        pp "@;(%S, %a)%s" id (pp_layout false) layout sc
      in
      List.iteri fn def.func_args
    end;
    pp "@]@;];@;";

    pp "@[<v 2>f_local_vars := [";
    begin
      let n = List.length def.func_vars in
      let fn i (id, layout) =
        let sc = if i = n - 1 then "" else ";" in
        pp "@;(%S, %a)%s" id (pp_layout false) layout sc
      in
      List.iteri fn def.func_vars
    end;
    pp "@]@;];@;";

    pp "f_init := \"#0\";@;";

    pp "@[<v 2>f_code := (";
    begin
      let fn id stmt =
        pp "@;@[<v 2><[ \"%s\" :=@;" id;

        pp_stmt ff stmt;
        pp "@]@;]> $";
      in
      SMap.iter fn def.func_blocks;
      pp "∅"
    end;
    pp "@]@;)%%E";
    pp "@]@;|}.";
  in
  let pp_function (id, def_or_decl) =
    match def_or_decl with
    | FDef(def) -> pp_function_def (id, def)
    | _         -> ()
  in
  List.iter pp_function ast.functions;

  (* Closing the section. *)
  pp "@]@;End code.@]"

let (reset_nroot_counter, with_uid) : (unit -> unit) * string pp =
  let counter = ref (-1) in
  let with_uid ff s = incr counter; fprintf ff "\"%s_%i\"" s !counter in
  let reset _ = counter := -1 in
  (reset, with_uid)

let gather_struct_fields id s =
  let fn (x, (ty_opt, _, layout)) =
    match ty_opt with
    | Some(MA_field(ty)) -> (x, ty, layout)
    | Some(MA_utag(_))
    | Some(MA_none)      ->
        Panic.panic_no_pos "Bad annotation on field [%s] of struct [%s]." x id
    | None           ->
        Panic.panic_no_pos "No annotation on field [%s] of struct [%s]." x id
  in
  List.map fn s.struct_members

let rec pp_struct_def_np structs annot fields ff id =
  let pp fmt = fprintf ff fmt in
  let pp_exists ff ex =
    let pp fmt = fprintf ff fmt in
    if ex <> [] then
      begin
        pp "ex";
        let pp_exist (x, e) =
          pp " (%s : %s)" x e
        in
        List.iter pp_exist ex;
        pp ",@;"
      end;
  in
  let pp_let ff (id, ty, def) =
    let pp fmt = fprintf ff fmt in
    match ty with
    | None    -> pp "let %s := %s in@;" id def;
    | Some ty -> pp "let %s : %s := %s in@;" id ty def;
  in
  let pp_constrs ff constrs =
    let pp fmt = fprintf ff fmt in
    if constrs <> [] then pp "∗∗ %a" (pp_sep " ∗∗ " pp_anystring) constrs;
  in
  (* Print the part that may stand for dots in case of "ptr_type". *)
  let pp_dots ff () =
    let pp fmt = fprintf ff fmt in
    (* Printing of the "exists". *)
    pp "@[<v 0>";
    pp_exists ff annot.st_exists;
    (* Printing the let-bindings. *)
    List.iter (pp_let ff) annot.st_lets;
    pp "@[<v 2>"; (* Open box for struct fields. *)

    let pp fmt = fprintf ff fmt in
    (* Printing the struct fields. *)
    pp "structT struct_%s [" id;
    let pp_field ff (_, ty, layout) =
      match layout with
      | LStruct(s_id, false) ->
          let (s, structs) =
            try (List.assoc s_id structs, List.remove_assoc s_id structs)
            with Not_found -> Panic.panic_no_pos "Unknown struct [%s]." s_id
          in
          let annot =
            match s.struct_annot with
            | Some(annot) -> annot
            | None        ->
            Panic.panic_no_pos "Annotations on struct [%s] are invalid." s_id
          in
          begin
            match annot with
            | SA_union        ->
                Panic.panic_no_pos "Annotations on struct [%s] are invalid \
                  since it is not a union." s_id
            | SA_tagged_u(_)  ->
                Panic.panic_no_pos "Annotations on struct [%s] are invalid \
                  since it is not a tagged union." s_id
            | SA_basic(annot) ->
            if annot = default_basic_struct_annot || basic_struct_annot_defines_type annot then
              (* No annotation on struct, fall back to normal printing. *)
              pp "%s" ty
            else
            let annot =
              match annot.st_ptr_type with
              | None    -> {annot with st_ptr_type = Some((s_id,ty))}
              | Some(_) ->
              Panic.panic_no_pos "[rc::ptr_type] in nested struct [%s]." s_id
            in
            let fields = gather_struct_fields s_id s in
            pp "(%a)" (pp_struct_def_np structs annot fields) s_id
          end
      | LStruct(_   , true ) -> assert false (* TODO *)
      | _                    -> pp "%s" ty
    in
    begin
      match fields with
      | []              -> ()
      | field :: fields ->
      reset_nroot_counter ();
      pp "@;%a" pp_field field;
      List.iter (pp " ;@;%a" pp_field) fields
    end;
    pp "@]@;] "; (* Close box for struct fields. *)
    (* Printing of constraints. *)
    pp_constrs ff annot.st_constrs;
    pp "@]"
  in
  reset_nroot_counter ();
  (* print the exists before the ptr_type *)
  pp_exists ff annot.st_pre_exists;
  (* print the pointer type *)
  (match annot.st_ptr_type with
  | None        -> pp_dots ff ()
  | Some(_, ty) ->
      (* first print the core definition into a string *)
      let core = Format.asprintf "(%a)" pp_dots () in
      let dots_regex = Str.regexp "\\.\\.\\." in
      let ty2 =
        try
          let new_pos = Str.search_forward dots_regex ty 0 in
          Str.replace_first dots_regex core ty
        with
          Not_found -> ty
      in
      pp "%s" ty2
  );
  pp " ";
  pp_constrs ff annot.st_pre_constrs

      (*pp_type_expr_rec (Some(pp_dots)) r ff ty*)

let pp_spec : Coq_path.t -> import list -> inlined_code ->
      typedef list -> string list -> Coq_ast.t pp =
    fun coq_path imports inlined typedefs ctxt ff ast ->

  (* Formatting utilities. *)
  let pp fmt = Format.fprintf ff fmt in

  (* Print inlined code (starts with an empty line) *)
  let pp_inlined extra_line_after descr ls =
    if ls <> [] then pp "\n";
    if ls <> [] then
      begin
        match descr with
        | None        -> pp "@;(* Inlined code. *)\n"
        | Some(descr) -> pp "@;(* Inlined code (%s). *)\n" descr
      end;
    List.iter (fun s -> if s = "" then pp "\n" else pp "@;%s" s) ls;
    if extra_line_after && ls <> [] then pp "\n";
  in

  (* Printing some header. *)
  pp "@[<v 0>From quiver.thorium.typing Require Import typing.@;";
  pp "@[<v 0>From quiver.inference Require Import inferPT.@;";
  pp "@[<v 0>From quiver.thorium.types Require Import types functions.@;";
  pp "From %a Require Import generated_code.@;" Coq_path.pp coq_path;
  List.iter (pp_import ff) imports;
  pp "Set Default Proof Using \"Type*\".\n";

  (* Printing generation data in a comment. *)
  pp "@;(* Generated from [%s]. *)" ast.source_file;

  (* Printing inlined code (from comments). *)
  pp_inlined true (Some "prelude") inlined.ic_prelude;

  (* Opening the section. *)
  pp "@;@[<v 2>Section spec.@;";
  pp "Context `{!refinedcG Σ}.";
  List.iter (pp "@;%s.") ctxt;

  (* Printing inlined code (from comments). *)
  pp_inlined false None inlined.ic_section;

  (* [Notation] data for printing sugar. *)
  let sugar = ref [] in

  (* [Typeclass Opaque] stuff that needs to be repeated after the section. *)
  let opaque = ref [] in

  (* Definition of types. *)
  let pp_struct struct_id annot s =
    (* Check if a type must be generated. *)
    if not (basic_struct_annot_defines_type annot) then () else
    (* Gather the field annotations. *)
    let fields = gather_struct_fields struct_id s in
    let (ref_names, ref_types) = List.split annot.st_refined_by in
    let (par_names, par_types) = List.split annot.st_parameters in
    let ref_and_par_types = ref_types @ par_types in
    let ref_and_par_names = ref_names @ par_names in
    let pp_params ff =
      let fn (x,e) = fprintf ff "(%s : %a) " x (pp_anystring) e in
      List.iter fn
    in
    let params = annot.st_parameters in
    let id =
      match annot.st_ptr_type with
      | None       -> struct_id
      | Some(id,_) -> id
    in
    pp "\n@;(* Definition of type [%s]. *)@;" id;
    let pp_prod = pp_as_prod (pp_enclosed "(" ")" pp_anystring) in
    pp "@[<v 2>Definition %s_rec : (%a → type Σ) → (%a → type Σ) := " id
      pp_prod ref_and_par_types pp_prod ref_and_par_types;

    let rec_name = id in
    pp "(λ %s %a,@;" rec_name (pp_encoded_patt_name false) ref_and_par_names;
    pp_encoded_patt_bindings ff ref_and_par_names;
    (* print the main body of the type *)
    pp_struct_def_np ast.structs annot fields ff struct_id;

    pp "@]@;)%%RT.@;";
    if par_names <> [] then sugar := !sugar @ [(id, par_names)];
    (*opaque := !opaque @ [id ^ "_rec"];*)

    pp "@;Global Instance %s_rec_mono : TypeMono %s_rec." id id;
    pp "@;Proof. solve_proper. Qed.\n@;";

    pp "@[<v 2>Definition %s %a: rtype Σ _ := fixR %s_rec." id pp_params params id
  in
  (*
  let pp_tagged_union id tag_type_e s =
    if s.struct_is_union then
      Panic.panic_no_pos "Tagged union annotations used on [%s] should \
        rather be placed on a struct definition." id;
    (* Extract the two fields of the wrapping structure (tag and union). *)
    let (tag_field, union_field) =
      match s.struct_members with
      | [tag_field ; union_field] -> (tag_field, union_field)
      | _                         ->
      Panic.panic_no_pos "Tagged union [%s] is ill-formed: it should have \
        exactly two fields (tag and union)." id
    in
    (* Obtain the name of the tag field and check its type. *)
    let tag_field =
      let (tag_field, (annot, _, layout)) = tag_field in
      if annot <> Some(MA_none) then
        Panic.wrn None "Annotation ignored on the tag field [%s] of \
          the tagged union [%s]." tag_field id;
      if layout <> LInt(ItSize_t(false)) then
        Panic.panic_no_pos "The tag field [%s] of tagged union [%s] does \
          not have the expected [size_t] type." tag_field id;
      tag_field
    in
    (* Obtain the name of the union field and the name of the actual union. *)
    let (union_field, union_name) =
      let (union_field, (annot, _, layout)) = union_field in
      if annot <> Some(MA_none) then
        Panic.wrn None "Annotation ignored on the union field [%s] of \
          the tagged union [%s]." union_field id;
      match layout with
      | LStruct(union_name, true) -> (union_field, union_name)
      | _                         ->
      Panic.panic_no_pos "The union field [%s] of tagged union [%s] is \
        expected to be a union." union_field id
    in
    (* Find the union and extract its fields and corresponding annotations. *)
    let union_cases =
      let union =
        try List.assoc union_name ast.structs
        with Not_found -> assert false (* Unreachable thanks to Cerberus. *)
      in
      (* Some sanity checks. *)
      if not union.struct_is_union then
        Panic.panic_no_pos "[%s] was expected to be a union." union_name;
      assert (union.struct_annot = Some(SA_union));
      (* Extracting data from the fields. *)
      let fn (name, (annot, _, layout)) =
        match annot with
        | Some(MA_utag(ts)) ->
            let id_struct =
              match layout with
              | LStruct(id, false) -> id
              | _                  ->
              Panic.panic_no_pos "Field [%s] of union [%s] is not a struct."
                name union_name
            in
            (name, ts, id_struct)
        | Some(MA_none    ) ->
            Panic.panic_no_pos "Union tag annotation expected on field [%s] \
              of union [%s]." name union_name
        | Some(MA_field(_)) ->
            Panic.panic_no_pos "Unexpected field annotation on [%s] in the \
              union [%s]." name union_name
        | None              ->
            Panic.panic_no_pos "Invalid annotation on field [%s] in the \
              union [%s]." name union_name
      in
      List.map fn union.struct_members
    in
    (* Starting to do the printing. *)
    pp "\n@;(* Definition of type [%s] (tagged union). *)@;" id;
    (* Definition of the tag function. *)
    pp "@[<v 2>Definition %s_tag (c : %a) : nat :=@;"
      id (pp_simple_coq_expr false) tag_type_e;
    pp "match c with@;";
    let pp_tag_case i (_, (c, args), _) =
      pp "| %s" c; List.iter (fun _ -> pp " _") args; pp " => %i%%nat@;" i
    in
    List.iteri pp_tag_case union_cases;
    pp "end.@]\n@;";
    (* Simplifications hints for inversing the tag function. *)
    let pp_inversion_hint i (_, (c, args), _) =
      pp "Global Instance simpl_%s_tag_%s c :@;" id c;
      pp "  SimplBothRel (=) (%s_tag c) %i%%nat (" id i;
      if args <> [] then pp "∃";
      let fn (x,e) = pp " (%s : %a)" x (pp_simple_coq_expr false) e in
      List.iter fn args;
      if args <> [] then pp ", ";
      pp "c = %s" c; List.iter (fun (x,_) -> pp " %s" x) args; pp ").@;";
      pp "Proof. split; destruct c; naive_solver. Qed.\n@;";
    in
    List.iteri pp_inversion_hint union_cases;
    (* Definition for the tagged union info. *)
    pp "@[<v 2>Program Definition %s_tunion_info : tunion_info := {|@;" id;
    pp "ti_rtype := %a;@;" (pp_simple_coq_expr false) tag_type_e;
    pp "ti_base_layout := struct_%s;@;" id;
    pp "ti_tag_field_name := \"%s\";@;" tag_field;
    pp "ti_union_field_name := \"%s\";@;" union_field;
    pp "ti_union_layout := union_%s;@;" union_name;
    pp "ti_tag := %s_tag;@;" id;
    pp "ti_type c :=@;";
    pp "  match c with@;";
    let fn (name, (c, args), struct_id) =
      pp "  | %s" c; List.iter (fun (x,_) -> pp " %s" x) args;
      pp " => struct struct_%s [@@{type} " name;
      begin
        let s =
          try List.assoc struct_id ast.structs
          with Not_found -> assert false (* Unreachable thanks to Cerberus. *)
        in
        let fields = gather_struct_fields struct_id s in
        let pp_field ff (_, ty, _) = fprintf ff "%a" pp_type_expr ty in
        match fields with
        | []      -> ()
        | f :: fs -> pp "%a" pp_field f; List.iter (pp "; %a" pp_field) fs
      end;
      pp "]%%I@;"
    in
    List.iter fn union_cases;
    pp "  end;@]@;";
    pp "|}.@;";
    pp "Next Obligation. done. Qed.@;";
    pp "Next Obligation. by case; eauto. Qed.\n@;";
    (* Actual definition of the type. *)
    pp "Program Definition %s : rtype := tunion %s_tunion_info." id id
  in
  *)
  let pp_struct_or_tagged_union (id, s) =
    match s.struct_annot with
    | Some(SA_basic(annot)) -> pp_struct id annot s
    | Some(SA_tagged_u(e))  ->
        Panic.panic_no_pos "Quiver does not currently support tagged unions"
        (*pp_tagged_union id e s*)
    | Some(SA_union)        -> ()
    | None                  ->
        Panic.panic_no_pos "Annotations on struct [%s] are invalid." id
  in
  List.iter pp_struct_or_tagged_union ast.structs;

  (* Type definitions (from comments). *)
  pp "\n@;(* Type definitions. *)";
  let pp_typedef (id, args, ty) =
    pp "\n@;Definition %s" id;
    let pp_arg (id, eo) =
      let pp_coq_expr = pp_coq_expr false pp_type_expr in
      match eo with
      | None    -> pp " %s" id
      | Some(e) -> pp " (%s : %a)" id pp_coq_expr e
    in
    List.iter pp_arg args;
    pp " := %a." pp_type_expr ty
  in
  List.iter pp_typedef typedefs;

  (* Closing the section. *)
  pp "@]@;End spec.";

  (* [Notation] stuff (printing sugar). *)
  if !sugar <> [] then pp "@;";
  let pp_sugar (id, params) =
    pp "@;Notation \"%s< %a >\"" id (pp_sep " , " pp_print_string) params;
    pp " := (%s %a)@;  " id (pp_sep " " pp_print_string) params;
    pp "(only printing, format \"'%s<' %a '>'\") : printing_sugar." id
      (pp_sep " ,  " pp_print_string) params
  in
  List.iter pp_sugar !sugar;

  (* [Typeclass Opaque] stuff. *)
  if !opaque <> [] then pp "@;";
  let pp_opaque = pp "@;Global Typeclasses Opaque %s." in
  List.iter pp_opaque !opaque;

  (* Printing inlined code (from comments). *)
  pp_inlined false (Some "final") inlined.ic_final;
  pp "@]"

exception Unsupported_proof of string * string

let pp_spec_of_decl : func_decl -> Coq_ast.t pp = fun decl ff ast ->
  (* Formatting utilities. *)
  let pp fmt = Format.fprintf ff fmt in

  pp "\n@;(* Type declaration for [%s]. *)@;" decl.decl_name;

  (* Only print a comment if the function is trusted. *)
  match decl.decl_annot with
  | None ->
    pp "(* You didn't even provide a type for this declared function. *)"
  | Some(annot) ->
      match annot.fa_spec with
      | Spec_none
      | Spec_annot(_, _) ->
          (* TODO: we could try to still print the predicate transformer in this case,
              if the annotations provide a complete spec *)
        pp "(* You didn't even provide a type for this declared function. *)"
      | Spec_external(s) ->
        pp "Definition %s_pt := %s.@;@;" decl.decl_name s


let pp_spec_and_proof : func_def -> proof_kind -> Coq_ast.t pp =
    fun def proof_kind ff ast ->
  (* Formatting utilities. *)
  let pp fmt = Format.fprintf ff fmt in

  pp "\n@;(* Typing inference for [%s]. *)@;" def.func_name;

  (* Only print a comment if the function is trusted. *)
  match proof_kind with
  | Proof_trusted ->
      (match def.func_annot with
      | None ->
        pp "(* Let's skip that, you seem to have some faith. *)"
      | Some(annot) ->
          match annot.fa_spec with
          | Spec_none
          | Spec_annot(_, _) ->
              (* TODO: we could try to still print the predicate transformer in this case,
                  if the annotations provide a complete spec *)
            pp "(* Let's skip that, you seem to have some faith. *)"
          | Spec_external(s) ->
            pp "Definition %s_pt := %s.@;@;" def.func_name s)
  | Proof_skipped ->
      pp "(* You were too lazy to even write a spec for this function. *)"
  | _             ->


  (* Statement of the typing proof. *)
  let func_annot =
    match def.func_annot with
    | Some(annot) -> annot
    | None        -> assert false (* Unreachable. *)
  in
  (*if List.length def.func_args <> List.length func_annot.fa_args then*)
    (*Panic.panic_no_pos "Argument number missmatch between code and spec.";*)
  (* Get all globals, including those needed for inlined functions. *)
  let (used_globals, used_functions) =
    let merge (g1, f1) (g2, f2) =
      let dedup = List.dedup String.compare in
      (dedup (g1 @ g2), dedup (f1 @ f2))
    in
    let fn acc f =
      match List.assoc_opt f ast.functions with
      | Some(FDef(def)) when is_inlined def ->
          merge acc def.func_deps
      | _                                   -> acc
    in
    List.fold_left fn def.func_deps (snd def.func_deps)
  in
  let deps = used_globals @ used_functions in
  let global_deps = List.map (fun s -> "global_" ^ s) deps in
  let pp_args ff xs =
    match xs with
    | [] -> ()
    | _  -> fprintf ff " (%a : loc)" (pp_sep " " pp_str) xs
  in
  let pp_apps ff xs =
    match xs with
    | [] -> ()
    | _ -> fprintf ff " %a" (pp_sep " " pp_str) xs
  in

  (* check if we are doing inference or proving a given predicate transformer *)
  let generate_inference =
    match def.func_annot with
    | None ->
      true
    | Some(annot) ->
        match annot.fa_spec with
        | Spec_annot(_, _)
        | Spec_none -> true
        | _ -> false
  in
  if not generate_inference then
    raise (Unsupported_proof (def.func_name, "proving an external spec is not supported yet. Provide the trust_me attribute."))
  else
  (* print the inference statement and proof *)
  pp "@[<v 2>Lemma type_%s%a :@;" def.func_name pp_args global_deps;
  let pp_head ppty =
    (* print dependencies (global variables and other functions) *)
    let pp_dep ff f =
      let pp fmt = Format.fprintf ff fmt in
      pp "global_%s ◁ᵥ " f;
      begin
      match List.assoc_opt f ast.functions with
      | Some(FDef(def)) ->
        let layouts = List.map snd def.func_args in
        pp "fnT [%a] %s_pt" (pp_sep "; " (pp_layout true)) layouts f
      | Some(FDec(decl)) ->
          match decl.decl_args with
          | Some(layouts) ->
            pp "fnT [%a] %s_pt" (pp_sep "; " (pp_layout true)) layouts f
      end
    in
    let pp_qenvs () =
      match used_functions with
      | [] -> pp "qenvs_empty"
      | _ ->
          pp "QE nil nil [";
          pp "%a" (pp_sep "; " pp_dep) used_functions;
          pp "] nil"
    in
    pp_qenvs ();
    pp " ⊨ impl_%s %a : %a.@]@;" def.func_name pp_apps global_deps ppty ()
  in
  pp_head (fun _ _ -> pp "?");

  (* We have a manual proof. *)
  match proof_kind with
  | Proof_manual(_,_,thm) ->
      pp "Proof. refine %s. Qed." thm;
  | _                     ->


  (* We output a normal proof. *)
  pp "@[<v 2>Proof.@;";

  (* declare special flags *)
  (match def.func_annot with
  | Some(annot) ->
      if annot.fa_join_if then
        pp "pose FancyIf.@;";
      if annot.fa_join_conj then
        pp "pose FancyConj.@;";
      if annot.fa_exact_post then
        pp "pose ExactPost.@;";
  | None ->
      ());

  (* declare hints *)
  List.iter (fun (i, h) -> pp "pose_hint \"%s\" (%a)%%I.@;" i pp_stmt_hint_prop h) def.func_stmt_hints;
  List.iter (fun (i, h) -> pp "pose_expr_hint \"%s\" (%a)%%I.@;" i pp_expr_hint_prop h) def.func_expr_hints;

  pp "inferF.@;";
  pp "- abduct.@;";
  pp "- simplify_passes.@;";
  pp "- idtac \"%s: \"; print_loud.@;@]" def.func_name;
  pp "Defined.@;@;";

  (* Also output a definition for the inferred predicate transformer *)
  let fndeps = List.map (fun x -> "global_" ^x) deps in
  (* the predicate transformer should not depend on the locations of globals *)
  pp "Definition %s_pt := pt_of (type_%s %a).@;@;" def.func_name def.func_name pp_apps (List.map (fun _ -> "inhabitant") fndeps);

  pp "@[<v 2>Lemma impl_%s_has_type_%s_pt%a :@;" def.func_name def.func_name pp_args global_deps;
  pp_head (fun _ _ -> pp "%s_pt" def.func_name);
  pp "@[<v 2>Proof.@;";
  pp "proof_from_well_typed (type_%s %a).@;@]" def.func_name pp_apps global_deps;
  pp "Qed.@;@;"

let pp_proof_file : Coq_path.t -> (func_def_or_decl * proof_kind) -> import list -> string list -> bool -> Coq_ast.t pp =
  fun coq_path defs imports ctxt print_metrics ff ast ->
    (* Formatting utilities. *)
    let pp fmt = Format.fprintf ff fmt in

    (* Printing some header. *)
    pp "@[<v 0>From quiver.thorium.typing Require Import typing.@;";
    pp "@[<v 0>From quiver.inference Require Import inferPT.@;";
    pp "@[<v 0>From quiver.thorium.types Require Import types functions.@;";
    pp "From %a Require Import generated_code.@;" Coq_path.pp coq_path;
    pp "From %a Require Import generated_spec.@;" Coq_path.pp coq_path;
    List.iter (pp_import ff) imports;
    pp "Set Printing Depth 1000000.@;";
    pp "Set Printing Width 400.@;";
    pp "Set Default Proof Using \"Type*\".@;@;";
    pp "Import AbductNotations.@;";
    if print_metrics then
      pp "From quiver.inference Require Import metrics.@;";

    (* Printing generation data in a comment. *)
    pp "(* Generated from [%s]. *)@;" ast.source_file;

    (* Opening the section. *)
    pp "@[<v 2>Section inference.@;";
    pp "Context `{!refinedcG Σ}.";
    List.iter (pp "@;%s.") ctxt;

    (match fst defs with
    | FDef(def) ->
      pp_spec_and_proof def (snd defs) ff ast;
    | FDec(decl) ->
      pp_spec_of_decl decl ff ast;
    );

    (* Closing the section. *)
    pp "@]@;End inference.@]"


type mode =
  | Code of string * import list
  | Spec of Coq_path.t * import list * inlined_code * typedef list * string list
  | Fprf of Coq_path.t * (func_def_or_decl * proof_kind) * import list * string list * bool

let write : mode -> string -> Coq_ast.t -> unit = fun mode fname ast ->
  let pp =
    match mode with
    | Code(root_dir,imports)                 ->
        pp_code root_dir imports
    | Spec(coq_path,imports,inlined,tydefs,ctxt) ->
        pp_spec coq_path imports inlined tydefs ctxt
    | Fprf(coq_path,def,imports,ctxt,print_metrics)       ->
        pp_proof_file coq_path def imports ctxt print_metrics
  in
  (* We write to a buffer. *)
  let buffer = Buffer.create 4096 in
  Format.fprintf (Format.formatter_of_buffer buffer) "%a@." pp ast;
  (* Check if we should write the file (inexistent / contents different). *)
  let must_write =
    try Buffer.contents (Buffer.from_file fname) <> Buffer.contents buffer
    with Sys_error(_) -> true
  in
  if must_write then Buffer.to_file fname buffer
