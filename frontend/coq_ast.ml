open Extra
open Rc_annot

type int_type =
  | ItSize_t    of bool (* signed *)
  | ItIntptr_t  of bool (* signed *)
  | ItPtrdiff_t
  | ItI8        of bool (* signed *)
  | ItI16       of bool (* signed *)
  | ItI32       of bool (* signed *)
  | ItI64       of bool (* signed *)

type layout =
  | LVoid
  | LBool
  | LPtr
  | LStruct of string * bool (* Union? *)
  | LInt of int_type
  | LArray of layout * string (* size *)

type op_type =
  | OpBool
  | OpInt of int_type
  | OpPtr of layout
  | OpStruct of string * op_type list
  | OpUntyped of layout

type un_op =
  | NotBoolOp
  | NotIntOp
  | NegOp
  | CastOp of op_type

type bin_op =
  | AddOp | SubOp | MulOp | DivOp | ModOp | AndOp | OrOp | XorOp | ShlOp
  | ShrOp | EqOp | NeOp | LtOp | GtOp | LeOp | GeOp | CommaOp
  | LazyAndOp | LazyOrOp

type value =
  | Null
  | Void
  | Int of string * int_type
  | SizeOf of layout

let coq_locs : Location.Pool.t = Location.Pool.make ()

type expr_hint_prop_ex =
  | EHintEx      of string * anystring * expr_hint_prop_ex
  | EHintExAnon  of string * expr_hint_prop_ex
  | EHintBase    of anystring * (anystring list)
type expr_hint_prop =
  | EHintLocal   of ident * expr_hint_prop
  | EHintValue   of ident * expr_hint_prop
  | EHintAll     of ident * anystring * expr_hint_prop
  | EHintAllBase of expr_hint_prop_ex

type assert_hint =
  | AssertHint of ident
type expr_annot =
  | ExprAnnotLit of coq_expr
  | ExprAnnotAssert of assert_hint

type expr = expr_aux Location.located
and expr_aux =
  | Var       of string option * bool (* Global? *)
  | Val       of value
  | UnOp      of un_op * op_type * expr
  | BinOp     of bin_op * op_type * op_type * expr * expr
  | Deref     of bool (* Atomic? *) * op_type * expr
  | CAS       of op_type * expr * expr * expr
  | Call      of expr * expr list
  | IfE       of op_type * expr * expr * expr
  | SkipE     of expr
  | Use       of bool (* Atomic? *) * op_type * expr
  | AddrOf    of expr
  | LValue    of expr
  | GetMember of expr * string * bool (* From_union? *) * string
  | OffsetOf  of string * bool (* From_union? *) * string
  | AnnotExpr of int * expr_annot * expr
  | Struct    of string * (string * expr) list
  | Macro     of string * string list * expr list * expr
  | CopyAID   of op_type * expr * expr

type stmt_hint_prop_ex =
  | SHintEx      of string * anystring * stmt_hint_prop_ex
  | SHintExAnon  of string * stmt_hint_prop_ex
  | SHintBase    of anystring list
type stmt_hint_prop =
  | SHintLocal   of ident * stmt_hint_prop
  | SHintValue   of ident * stmt_hint_prop
  | SHintAll     of ident * anystring * stmt_hint_prop
  | SHintAllBase of stmt_hint_prop_ex


type stmt_annot =
  | StmtAnnotAssert of assert_hint

type stmt = stmt_aux Location.located
and stmt_aux =
  | Goto   of string (* Block index in the [IMap.t]. *)
  (* Block index of loop head, loop continuation, and hint name *)
  | GotoLoop of string * string * string
  | Return of expr
  | Switch of int_type * expr * (string * int) list * stmt list * stmt
  | Assign of bool (* Atomic? *) * op_type * expr * expr * stmt
  | SkipS  of stmt
  | If     of string option (* join label *) * op_type * expr * stmt * stmt
  | Assert of op_type * expr * stmt
  | ExprS  of Rc_annot.expr_annot option * expr * stmt
  | AnnotS of stmt_annot * stmt

(* The integers are respecively the alignment and the size. *)
type field_data = member_annot option * (int * int) * layout

type struct_decl =
  { struct_name     : string
  ; struct_annot    : struct_annot option
  ; struct_deps     : string list
  ; struct_is_union : bool
  ; struct_members  : (string * field_data) list }

type func_def =
  { func_name   : string
  ; func_annot  : function_annot option
  ; func_args   : (string * layout) list
  ; func_vars   : (string * layout) list
  ; func_init   : string
  ; func_deps   : string list * string list (* global vars/functions used. *)
  ; func_stmt_hints  : (string * stmt_hint_prop) list
  ; func_expr_hints  : (string * expr_hint_prop) list
  ; func_blocks : stmt SMap.t }

type func_decl = 
  { decl_name   : string
  ; decl_annot  : function_annot option
  ; decl_args   : layout list option }

type func_def_or_decl =
  | FDef of func_def
  | FDec of func_decl

type t =
  { source_file : string
  ; entry_point : string option
  ; global_vars : (string * global_annot option) list
  ; structs     : (string * struct_decl) list
  ; functions   : (string * func_def_or_decl) list }

let proof_kind : func_def -> proof_kind = fun def ->
  match def.func_annot with
  | None        -> Proof_normal
  | Some(annot) -> annot.fa_proof_kind

let is_inlined : func_def -> bool = fun def ->
  proof_kind def = Proof_inlined


(* Scope of binders declared by hints. *)
type hint_scope = (string * anystring) list
type all_stmt_hints = (string * stmt_hint_prop) list ref
type all_expr_hints = (string * expr_hint_prop) list ref
type hint_ctx = hint_scope * all_stmt_hints * all_expr_hints * int ref
let insert_hint_scope (h: hint_scope) (x: string) (ty: anystring) =
  match List.assoc_opt x h with
  | None -> (x, ty) :: h
  | Some _ ->
      (* TODO maybe should warn? *)
      (x, ty) :: List.remove_assoc x h
let insert_all_stmt_hints (ah: all_stmt_hints) (x: string) (h: stmt_hint_prop) =
  let all = !ah in
  let all = match List.assoc_opt x all with
  | None -> (x, h) :: all
  | Some _ ->
      (* TODO maybe should warn? *)
      (x, h) :: List.remove_assoc x all
  in ah := all
let insert_all_expr_hints (ah: all_expr_hints) (x: string) (h: expr_hint_prop) =
  let all = !ah in
  let all = match List.assoc_opt x all with
  | None -> (x, h) :: all
  | Some _ ->
      (* TODO maybe should warn? *)
      (x, h) :: List.remove_assoc x all
  in ah := all

(* Build a hint annotation, updating the [hint_scope] with new existential binders introduced by the hint annotation *)
let build_hint_annot: ?prefix:string -> string list -> string list -> hint_ctx -> string option -> Rc_annot.stmt_annot -> assert_hint * hint_ctx = fun ?(prefix = "__hint") local_scope value_scope (scope, stmt_hints, expr_hints, next_hint) name annot ->
  (* get a name for this annotation *)
  let name =
    match name with
    | Some name -> name
    | _ ->
      let name = prefix ^ "_" ^ (string_of_int !next_hint) in
      next_hint := 1 + !next_hint;
      name
  in
  (* first build the base constr *)
  let c = SHintBase (annot.sa_constrs) in
  (* add anonymous existential quantifiers *)
  let c = List.fold_left (fun c (q: ident) -> SHintExAnon (q, c)) c annot.sa_anon_exists in
  (* add named existential quantifiers *)
  let (c, scope2) = List.fold_left (fun (c, scope) (q, ty) -> (SHintEx (q, ty,  c), insert_hint_scope scope q ty)) (c, scope) annot.sa_exists in
  (* finish existential quantifiers *)
  let c = SHintAllBase(c) in
  (* add universal quantifiers that are in scope. Important: we are using the old scope! *)
  let c = List.fold_left (fun c (q, ty) -> SHintAll (q, ty, c)) c scope in
  (* add local variables *)
  let c = List.fold_left (fun c i -> SHintLocal (i, c)) c local_scope in
  (* add values *)
  let c = List.fold_left (fun c i -> SHintValue (i, c)) c value_scope in
  insert_all_stmt_hints stmt_hints name c;
  (AssertHint name, (scope2, stmt_hints, expr_hints, next_hint))

let build_empty_hint_annot: ?prefix:string -> hint_ctx -> assert_hint * hint_ctx = fun ?(prefix = "__hint") (scope, stmt_hints, expr_hints, next_hint) ->
  let name = prefix ^ "_" ^ (string_of_int !next_hint) in
  next_hint := 1 + !next_hint;
  (AssertHint name, (scope, stmt_hints, expr_hints, next_hint))

(* Build an expression hint annotation, updating the [hint_scope] with new existential binders introduced by the hint annotation *)
let build_expr_type_annot: string list -> string list -> hint_ctx -> string option -> expr_type_annot -> assert_hint * hint_ctx = fun local_scope value_scope (scope, stmt_hints, expr_hints, next_hint) name annot ->
  (* get a name for this annotation *)
  let name =
    match name with
    | Some name -> name
    | _ ->
      let name = "__hint_" ^ (string_of_int !next_hint) in
      next_hint := 1 + !next_hint;
      name
  in
  (* first build the base constr *)
  let c = EHintBase (annot.et_type, annot.et_constrs) in
  (* add anonymous existential quantifiers *)
  let c = List.fold_left (fun c (q: ident) -> EHintExAnon (q, c)) c annot.et_anon_exists in
  (* add named existential quantifiers *)
  let (c, scope2) = List.fold_left (fun (c, scope) (q, ty) -> (EHintEx (q, ty,  c), insert_hint_scope scope q ty)) (c, scope) annot.et_exists in
  (* finish existential quantifiers *)
  let c = EHintAllBase(c) in
  (* add universal quantifiers that are in scope. Important: we are using the old scope! *)
  let c = List.fold_left (fun c (q, ty) -> EHintAll (q, ty, c)) c scope in
  (* add local variables *)
  let c = List.fold_left (fun c i -> EHintLocal (i, c)) c local_scope in
  (* add values *)
  let c = List.fold_left (fun c i -> EHintValue (i, c)) c value_scope in
  insert_all_expr_hints expr_hints name c;
  (AssertHint name, (scope2, stmt_hints, expr_hints, next_hint))

let init_hint_ctx () =
  ([], ref [], ref [], ref 0)
