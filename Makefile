QUIVER_FLAGS := --no-mem-cast --no-locs --no-build

all:
	@dune build _build/default/coq-iris-quiver.install --display short
.PHONY: all

all_with_examples: generate_all
	@dune build --display short
.PHONY: all_with_examples

evaluate: QUIVER_FLAGS+=--print-metrics
evaluate: generate_all
	@dune build --display short
.PHONY: all_with_examples

test: generate_all
	@dune build @runtest --display short
.PHONY: test

# We want to run @default and @runtest in the same dune call such that they can be parallelized
all_with_examples_and_test: generate_all
	@dune build @default @runtest --display short
.PHONY: all_with_examples_and_test

install:
	@dune install
.PHONY: install

uninstall:
	@dune uninstall
.PHONY: uninstall

clean_generated:

clean: clean_generated
	@dune clean
.PHONY: clean

# We cannot use builddep-pins as a dependency of builddep-opamfiles because the CI removes all pins.
builddep-pins:
	@opam pin add -n -y cerberus-lib "git+https://github.com/rems-project/cerberus.git#57c0e80af140651aad72e3514133229425aeb102"
	@opam pin add -n -y cerberus "git+https://github.com/rems-project/cerberus.git#57c0e80af140651aad72e3514133229425aeb102"
.PHONY: builddep-pins


builddep-opamfiles: builddep/coq-iris-quiver-builddep.opam
	@true
.PHONY: builddep-opamfiles

# see https://stackoverflow.com/a/649462 for defining multiline strings in Makefiles
define BUILDDEP_OPAM_BODY
opam-version: "2.0"
name: "coq-iris-quiver-builddep"
synopsis: "---"
description: """
---
"""
license: "BSD-3-Clause"
maintainer: "Authors"
authors: "Authors"
depends: [
endef
export BUILDDEP_OPAM_BODY

# Create a virtual Opam package with the same deps as Quiver, but no
# build.
builddep/coq-iris-quiver-builddep.opam: coq-iris-quiver.opam Makefile
	@echo "# Creating builddep package."
	@mkdir -p builddep
	@echo "$$BUILDDEP_OPAM_BODY" > $@
	@opam show -f depends: ./coq-iris-quiver.opam >> $@
	@echo "]" >> $@

# Install the virtual Opam package to ensure that:
#  1) dependencies of RefinedC are installed,
#  2) they will remain satisfied even if other packages are updated/installed,
#  3) we do not have to pin the RefinedC package itself (which takes time).
# Note: We also need to install cerberus to make sure that new pins are propagated correctly.
builddep: builddep/coq-iris-quiver-builddep.opam builddep-pins
	@echo "# Installing package $<."
	@opam install $(OPAMFLAGS) $< cerberus coq-lithium refinedc
.PHONY: builddep

DUNE_FILES = $(shell find theories/ -type f -name 'dune')

C_SRC = examples/src/casestudies/arrays/array_annotated.c \
		examples/src/casestudies/arrays/array_no_annot.c  \
		examples/src/casestudies/openssl_buffer/alloc.c \
		examples/src/casestudies/openssl_buffer/buffer.c  \
		examples/src/casestudies/openssl_buffer/buffer_shape.c \
		examples/src/casestudies/swap.c \
		examples/src/casestudies/list/list_no_annot.c \
		examples/src/casestudies/list/list_annotated.c \
		examples/src/casestudies/list/list_concrete.c \
		examples/src/casestudies/range/range_annotated.c \
		examples/src/casestudies/range/range.c \
		examples/src/casestudies/memory.c \
		examples/src/casestudies/swap.c \
		examples/src/casestudies/memcached_bidirectional_buffers/bipbuffer_annotated.c \
		examples/src/casestudies/binary_search.c \
		examples/src/casestudies/mutable_map.c

%.c.gen: %.c phony
	@dune exec -- quiver check $(QUIVER_FLAGS) $<
.PHONY: phony

generate_all: $(addsuffix .gen, $(C_SRC))
.PHONY: generate_all

