From quiver.thorium.typing Require Import typing.

Class lib_malloc (Σ: gFunctors) := {
  block: loc → Z → iProp Σ;
}.


Section malloc_library.
  Context `{!refinedcG Σ} `{!lib_malloc Σ}.

  (* preliminaries *)
  Global Instance predicate_block : Predicate loc Z block := {}.

  (* the function types *)
  Definition malloc_pt : pt Σ := λ vs ret,
    (∃ v, ⌜vs = [v]⌝ ∗ ∃ n, (v ◁ᵥ int[size_t] n) ∗
         (ret null ∧ ∀ p, block p n -∗ ret (owned p (any[n]))))%I.

  Definition free_pt : pt Σ := λ vs ret,
    (∃ v, ⌜vs = [v]⌝ ∗ ∃ p w n, v ◁ᵥ owned p (value[n] w) ∗ block p n ∗ ret void)%I.

  Definition memcpy_pt : pt Σ := λ vs ret,
    (∃ dst_val src_val sz_val, ⌜vs = [dst_val; src_val; sz_val]⌝ ∗
      ∃ p q n v, dst_val ◁ᵥ owned p (any[n]) ∗ src_val ◁ᵥ owned q (value[n] v) ∗ (sz_val ◁ᵥ int[size_t] n) ∗
      ((p ◁ₗ value[n] v) -∗ (q ◁ₗ value[n] v) -∗ ret (value[ptr_size] p)))%I.

  Definition memset_pt : pt Σ := λ vs ret,
    (∃ dst_val zero_val sz_val, ⌜vs = [dst_val; zero_val; sz_val]⌝ ∗
      ∃ p n, dst_val ◁ᵥ ownT p (anyT n) ∗ zero_val ◁ᵥ intT i32 0 ∗ sz_val ◁ᵥ intT size_t n ∗
      (p ◁ₗ zerosT n -∗ ret (valueT p ptr_size)))%I.

  Import AbductNotations.
  Definition realloc_pt : pt Σ := λ vs ret,
    (∃ dst_val sz_val, ⌜vs = [dst_val; sz_val]⌝ ∗
      ∃ o n, dst_val ◁ᵥ opt o ∗ (sz_val ◁ᵥ int[size_t] n) ∗
        IF (¬ not_null o)
        THEN (ret null ∧ ∀ p, block p n -∗ ret (owned p (any[n])))%I
        ELSE
          ∃ p m v, o ◁ᵥ owned p (value[m] v) ∗ block p m ∗ ⌜m ≤ n⌝ ∗
            (((p ◁ₗ value[m] v) -∗ block p m -∗ ret null) ∧
            (∀ q, q ◁ₗ slicesT n [(0, m, value[m] v); (m, n - m, any[n - m])] -∗ block q n -∗ ret (value[ptr_size] q))))%I.


  (* OTHER STANDARD LIBRARY FUNCTIONS *)
  Definition abort_pt : pt Σ := λ vs ret,
    (⌜vs = []⌝ ∗ (False -∗ ret void))%I.

End malloc_library.
