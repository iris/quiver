From quiver.thorium.types Require Import types base_types functions.
From quiver.thorium.logic Require Export syntax.
From quiver.thorium.typing Require Export classes.
From quiver.argon.simplification Require Export simplification.
From quiver.argon.abduction Require Export abduct proofmode pure.
From quiver.thorium.typing Require Export typing.
From iris.proofmode Require Import string_ident.

From quiver.inference Require Export flexhints final_simplification.



Section infer.
  Context {Σ: gFunctors} `{!refinedcG Σ}.

  Definition type_function (fn: function) (ws: list val) (Φ: type Σ → iProp Σ) : iProp Σ :=
    ∀ lsa lsv, ([∗ list] l; p  ∈ lsa; zip (fn.(f_args).*2) ws, l ◁ₗ valueT p.2 (ly_size p.1)) -∗
                ([∗ list] l; ly ∈ lsv; (fn.(f_local_vars).*2), l ◁ₗ anyT (ly_size ly)) -∗
                ⌜length ws = length fn.(f_args)⌝ -∗
                swp (Goto fn.(f_init)) (fn_cfg fn lsa lsv) (λ v A, ([∗ list] l;ly ∈ lsa;(f_args fn).*2, l ◁ₗ (any[ly_size ly])) ∗ ([∗ list] l;ly ∈ lsv;(f_local_vars fn).*2, l ◁ₗ (any[ly_size ly])) ∗ type_unwind v A Φ).

  Definition type_function_body fn (vs: list val) lsa lsv Φ : iProp Σ :=
    ⌜length lsa = length (f_args fn)⌝ -∗ ⌜length lsv = length (f_local_vars fn)⌝ -∗ swp (Goto fn.(f_init)) (fn_cfg fn lsa lsv) (λ v A, ([∗ list] l;ly ∈ lsa;(f_args fn).*2, l ◁ₗ (any[ly_size ly])) ∗ ([∗ list] l;ly ∈ lsv;(f_local_vars fn).*2, l ◁ₗ (any[ly_size ly])) ∗ type_unwind v A Φ)%I.

  Definition type_function_frame (args: list (var_name * layout)) (locals: list (var_name * layout)) (ws: list val) (Φ: list val → list loc → list loc → iProp Σ) : iProp Σ :=
    ∀ lsa lsv, ⌜length lsa = length args⌝ -∗ ⌜length lsv = length locals⌝ -∗
    ∃ vs, ⌜length vs = length lsa⌝ ∗ ⌜ws = vs⌝ ∗
      (([∗ list] l; p  ∈ lsa; zip (args.*2) vs, l ◁ₗ valueT p.2 (ly_size p.1)) -∗
      ([∗ list] l; ly ∈ lsv; (locals.*2), l ◁ₗ anyT (ly_size ly)) -∗
      Φ vs lsa lsv).

  Definition type_function_connect (vs ws: list val) (P: iProp Σ) : iProp Σ :=
    ⌜vs = ws⌝ ∗ P.

  Definition type_function_start (P: iProp Σ) : iProp Σ := P.

  Definition type_subst (σ: list (string * val)) (cfg: gmap string stmt) (Φ: gmap string stmt → iProp Σ) :=
    Φ (subst_stmt σ <$> cfg).

  Definition type_function_return fn lsa lsv Φ : val → type Σ → iProp Σ :=
    λ v A, (⌜length lsa = length (f_args fn)⌝ -∗ ⌜length lsv = length (f_local_vars fn)⌝ -∗ ([∗ list] l;ly ∈ lsa;(f_args fn).*2, l ◁ₗ (any[ly_size ly])) ∗ ([∗ list] l;ly ∈ lsv;(f_local_vars fn).*2, l ◁ₗ (any[ly_size ly])) ∗ type_unwind v A Φ)%I.


  Global Typeclasses Opaque
    type_function
    type_function_frame
    type_function_connect
    type_function_body
    type_function_start
    type_subst
    type_function_return.

  Lemma type_function_intro (fn: function) (ws: list val) (Φ: type Σ → iProp Σ):
    (abd_simpl (fn.(f_args))
    $ λ args, abd_simpl (fn.(f_local_vars)) $
    λ locals, type_function_frame args locals ws
              $ λ vs lsa lsv, type_function_body fn vs lsa lsv Φ)
    ⊢ type_function fn ws Φ.
  Proof.
    rewrite /abd_simpl /type_function_frame /type_function_connect /type_function_body /type_function. iIntros "Hcont".
    iIntros (lsa lsv) "Hargs Hlocs %Hlen0".
    iDestruct (big_sepL2_length with "Hlocs") as "%Hlen1".
    iDestruct (big_sepL2_length with "Hargs") as "%Hlen2".
    iDestruct ("Hcont" with "[] []") as "(%vs & %Hlen3 & -> & Hbody)"; last first.
    - iApply ("Hbody" with "Hargs Hlocs").
      + rewrite -Hlen0 Hlen3 //.
      + rewrite Hlen1 fmap_length //.
    - iPureIntro. rewrite Hlen1 fmap_length //.
    - iPureIntro. rewrite Hlen2 zip_length fmap_length //.
  Qed.


  Lemma abduct_type_function_intro Δ fn ws Φ R:
    abduct Δ (
      (abd_simpl (fn.(f_args))
      $ λ args, abd_simpl (fn.(f_local_vars)) $
      λ locals, type_function_frame args locals ws
                $ λ vs lsa lsv, type_function_body fn vs lsa lsv Φ)
    )%I R →
    abduct Δ (type_function fn ws Φ) R.
  Proof. rewrite -type_function_intro //. Qed.

  Global Instance abuct_step_type_function_intro Δ fn ws Φ R:
    AbductStep (
      abduct Δ (abd_simpl (fn.(f_args))
      $ λ args, abd_simpl (fn.(f_local_vars)) $
      λ locals, type_function_frame args locals ws
                $ λ vs lsa lsv, type_function_body fn vs lsa lsv Φ) R
      )
      Δ (type_function fn ws Φ) R.
  Proof. intros ?. by eapply abduct_type_function_intro. Qed.


  Lemma type_function_frame_intro args locs ws Φ :
    (abd_simpl (map (λ x, append x "_arg") (args.*1))
    $ λ sargs, abd_simpl (map (λ x, append x "_val") (args.*1))
    $ λ svals, abd_simpl (map (λ x, append x "_var") (locs.*1))
    $ λ slocs, abd_simpl ((args.*2))
    $ λ lysa,  abd_simpl ((locs.*2))
    $ λ lysv,
      forallL sargs
      $ λ lsa, forallL slocs
      $ λ lsv, existsL svals
      $ λ vs,  type_function_connect ws vs
                $ abd_assume CTX ([∗ list] l; p ∈ lsa; zip lysa vs, l ◁ₗ valueT p.2 (ly_size p.1))
                $ abd_assume CTX ([∗ list] l; ly ∈ lsv; lysv, l ◁ₗ anyT (ly_size ly))
                $ Φ vs lsa lsv)
    ⊢ type_function_frame args locs ws Φ.
  Proof.
    rewrite /type_function_frame /type_function_connect /type_function_body /type_function.
    rewrite /abd_simpl /forallL /existsL.
    iIntros "Hcont". iIntros (lsa lsv) "%Hlen1 %Hlen2".
    iSpecialize ("Hcont" $! lsa with "[]").
    { iPureIntro. rewrite Hlen1 !fmap_length //. }
    iSpecialize ("Hcont" $! lsv with "[]").
    { iPureIntro. rewrite Hlen2 !fmap_length //. }
    iDestruct ("Hcont") as "(%vs & %Hlen & %Heq & Hcont)".
    iExists ws. rewrite !fmap_length in Hlen. iSplit.
    { iPureIntro. subst. rewrite -Hlen Hlen1 //. }
    iSplit; first done.
    iIntros "Hargs Hlocs". rewrite /abd_assume.
    subst. iApply ("Hcont" with "Hargs Hlocs").
  Qed.

  Lemma abduct_type_function_frame Δ args locs ws Φ R:
    abduct Δ (
    abd_simpl (map (λ x, append x "_arg") (args.*1))
    $ λ sargs, abd_simpl (map (λ x, append x "_val") (args.*1))
    $ λ svals, abd_simpl (map (λ x, append x "_var") (locs.*1))
    $ λ slocs, abd_simpl ((args.*2))
    $ λ lysa,  abd_simpl ((locs.*2))
    $ λ lysv,
      forallL sargs
      $ λ lsa, forallL slocs
      $ λ lsv, existsL svals
      $ λ vs,  type_function_connect ws vs
               $ abd_assume CTX ([∗ list] l; p ∈ lsa; zip lysa vs, l ◁ₗ valueT p.2 (ly_size p.1))
               $ abd_assume CTX ([∗ list] l; ly ∈ lsv; lysv, l ◁ₗ anyT (ly_size ly))
               $ Φ vs lsa lsv
    )%I R →
    abduct Δ (type_function_frame args locs ws Φ) R.
  Proof. rewrite -type_function_frame_intro //. Qed.

  Global Instance abduct_step_type_function_frame Δ args locs ws Φ R :
    AbductStep (abduct Δ (
        abd_simpl (map (λ x, append x "_arg") (args.*1))
      $ λ sargs, abd_simpl (map (λ x, append x "_val") (args.*1))
      $ λ svals, abd_simpl (map (λ x, append x "_var") (locs.*1))
      $ λ slocs, abd_simpl ((args.*2))
      $ λ lysa,  abd_simpl ((locs.*2))
      $ λ lysv,
            forallL sargs
        $ λ lsa, forallL slocs
        $ λ lsv, existsL svals
        $ λ vs,  type_function_connect ws vs
               $ abd_assume CTX ([∗ list] l; p ∈ lsa; zip lysa vs, l ◁ₗ valueT p.2 (ly_size p.1))
               $ abd_assume CTX ([∗ list] l; ly ∈ lsv; lysv, l ◁ₗ anyT (ly_size ly))
               $ Φ vs lsa lsv
      ) R)%I
    Δ (type_function_frame args locs ws Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_function_frame. Qed.

  Lemma type_function_body_intro fn vs lsa lsv Φ :
    (abd_simpl ((fn.(f_args) ++ fn.(f_local_vars)).*1)
     $ λ xs, abd_simpl (val_of_loc <$> lsa ++ lsv)
     $ λ ls, abd_cbn (list_to_map (zip xs (lsa ++ lsv)))
     $ λ subst, abd_cbn (list_to_map (zip (fn.(f_args).*1) vs))
     $ λ av, abd_simpl (zip xs ls)
     $ λ σ, abd_simpl (fn.(f_init))
     $ λ lb, type_subst σ (fn.(f_code))
            $ λ Q, abd_pose "cfg" Q
            $ λ cfg, abd_pose_proof (Some "subst") (local_map subst)
            $ abd_pose_proof (Some "arg_vals") (val_map av)
              $ abd_stop
              $ type_function_start
              $ type_annot (CtxHintAnnot "pre")
              $ swp (Goto lb) cfg (type_function_return fn lsa lsv Φ))
    ⊢ type_function_body fn vs lsa lsv Φ.
  Proof.
    rewrite /type_function_body /abd_simpl /abd_cbn /type_subst /abd_pose /abd_stop /abd_pose_proof.
    rewrite /type_function_return /fn_cfg. iIntros "Hcont %Hlen1 %Hlen2".
    iApply (swp_wand with "(Hcont [] [])").
    { by iPureIntro; apply LocalMap. }
    { by iPureIntro; apply ArgVals. }
    iIntros (v A) "Hcont". iApply ("Hcont" with "[//] [//]").
  Qed.

  Lemma abduct_type_function_body_intro Δ fn vs lsa lsv Φ R :
    abduct Δ (abd_simpl ((fn.(f_args) ++ fn.(f_local_vars)).*1)
    $ λ xs, abd_simpl (val_of_loc <$> lsa ++ lsv)
    $ λ ls, abd_cbn (list_to_map (zip xs (lsa ++ lsv)))
    $ λ subst, abd_cbn (list_to_map (zip (fn.(f_args).*1) vs))
    $ λ av, abd_simpl (zip xs ls)
    $ λ σ, abd_simpl (fn.(f_init))
    $ λ lb, type_subst σ (fn.(f_code))
           $ λ Q, abd_pose "cfg" Q
           $ λ cfg, abd_pose_proof (Some "subst") (local_map subst)
             $ abd_pose_proof (Some "arg_vals") (val_map av)
             $ abd_stop
             $ type_function_start
             $ type_annot (CtxHintAnnot "pre")
             $ swp (Goto lb) cfg (type_function_return fn lsa lsv Φ)) R →
    abduct Δ (type_function_body fn vs lsa lsv Φ) R.
  Proof. rewrite -type_function_body_intro //. Qed.

  Global Instance abduct_step_type_function_body_init Δ fn vs lsa lsv Φ R :
    AbductStep
      (abduct Δ (abd_simpl ((fn.(f_args) ++ fn.(f_local_vars)).*1)
        $ λ xs, abd_simpl (val_of_loc <$> lsa ++ lsv)
        $ λ ls, abd_cbn (list_to_map (zip xs (lsa ++ lsv)))
        $ λ subst, abd_cbn (list_to_map (zip (fn.(f_args).*1) vs))
        $ λ av, abd_simpl (zip xs ls)
        $ λ σ, abd_simpl (fn.(f_init))
        $ λ lb, type_subst σ (fn.(f_code))
              $ λ Q, abd_pose "cfg" Q
              $ λ cfg, abd_pose_proof (Some "subst") (local_map subst)
                $ abd_pose_proof (Some "arg_vals") (val_map av)
                $ abd_stop
                $ type_function_start
                $ type_annot (CtxHintAnnot "pre")
                $ swp (Goto lb) cfg (type_function_return fn lsa lsv Φ)) R)
    Δ (type_function_body fn vs lsa lsv Φ) R.
  Proof. intros ?. by eapply abduct_type_function_body_intro. Qed.


  Lemma abduct_type_function_start Δ P Q R:
    clear_universals (abduct Δ P Q) →
    simplify Q R →
    print R →
    abduct Δ (type_function_start P) R.
  Proof.
    rewrite /clear_universals /type_function_start. intros ???. by eapply abduct_simplify.
  Qed.


  Lemma abduct_type_subst_intro Δ σ cfg cfg' Φ R:
    ComputeSubst σ cfg cfg' →
    abduct Δ (Φ cfg') R →
    abduct Δ (type_subst σ cfg Φ) R.
  Proof.
    rewrite /ComputeSubst. intros <-.
    rewrite /type_subst. done.
  Qed.

  Global Instance abduct_step_type_subst_intro Δ σ cfg cfg' Φ R:
    ComputeSubst σ cfg cfg' →
    AbductStep (abduct Δ (Φ cfg') R)
      Δ (type_subst σ cfg Φ) R.
  Proof. intros ??. by eapply abduct_type_subst_intro. Qed.

  Lemma type_function_return_intro fn lsa lsv Φ v A:
    (abd_simpl (zip lsa (fn.(f_args).*2))
    $ λ argl, abd_simpl (zip lsv (fn.(f_local_vars).*2))
    $ λ locl, type_dealloc_all (argl ++ locl)
      $ type_annot (CtxHintAnnot "post")
      $ type_annot_val (CtxHintAnnot "post_val") v A
      $ λ B, type_unwind v B Φ)
    ⊢ type_function_return fn lsa lsv Φ v A.
  Proof.
    rewrite /abd_simpl /abd_clear /type_function_return /type_annot /type_annot_val /type_dealloc_all.
    iIntros "([Hargs Hlocs] & Hw) %Hlen1 %Hlen2".
    rewrite !big_sepL2_alt. iFrame. rewrite /type_post /type_cont /type_unwind.
    rewrite !fmap_length. iSplit; first done. iSplit; first done.
    iIntros "Hv". iDestruct ("Hw" with "Hv") as "(%B & Hv & Hcont)".
    by iApply "Hcont".
  Qed.

  Lemma type_function_return_intro_no_unwind fn lsa lsv Φ v A:
    (abd_simpl (zip lsa (fn.(f_args).*2))
    $ λ argl, abd_simpl (zip lsv (fn.(f_local_vars).*2))
    $ λ locl, type_dealloc_all (argl ++ locl)
      $ type_annot (CtxHintAnnot "post")
      $ type_annot_val (CtxHintAnnot "post_val") v A Φ
    )
    ⊢ type_function_return fn lsa lsv Φ v A.
  Proof.
    rewrite /abd_simpl /abd_clear /type_function_return /type_annot /type_annot_val /type_dealloc_all.
    iIntros "([Hargs Hlocs] & Hw) %Hlen1 %Hlen2".
    rewrite !big_sepL2_alt. iFrame. rewrite /type_post /type_cont /type_unwind.
    rewrite !fmap_length. iSplit; first done. iSplit; first done.
    iIntros "Hv". iDestruct ("Hw" with "Hv") as "(%B & Hv & Hcont)".
    iExists _. iFrame.
  Qed.

  Lemma abduct_type_function_return_intro Δ fn lsa lsv Φ v A R:
    abduct Δ (
      abd_simpl (zip lsa (fn.(f_args).*2))
      $ λ argl, abd_simpl (zip lsv (fn.(f_local_vars).*2))
      $ λ locl, type_dealloc_all (argl ++ locl)
        $ type_annot (CtxHintAnnot "post")
        $ type_annot_val (CtxHintAnnot "post_val") v A
        $ λ B, type_unwind v B Φ
    ) R →
    abduct Δ (type_function_return fn lsa lsv Φ v A) R.
  Proof. rewrite -type_function_return_intro //. Qed.

  Global Instance abduct_step_type_function_return_intro Δ fn lsa lsv Φ v A R:
    AbductStep
      (abduct Δ (
        abd_simpl (zip lsa (fn.(f_args).*2))
        $ λ argl, abd_simpl (zip lsv (fn.(f_local_vars).*2))
        $ λ locl, type_dealloc_all (argl ++ locl)
          $ type_annot (CtxHintAnnot "post")
          $ type_annot_val (CtxHintAnnot "post_val") v A
          $ λ B, type_unwind v B Φ
      ) R)
    Δ (type_function_return fn lsa lsv Φ v A) R | 2.
  Proof. intros ?. by eapply abduct_type_function_return_intro. Qed.


  Lemma abduct_type_function_return_intro_exact_hint Δ fn lsa lsv Φ v A R:
    exact_hint "post_val" →
    abduct Δ (
      abd_simpl (zip lsa (fn.(f_args).*2))
      $ λ argl, abd_simpl (zip lsv (fn.(f_local_vars).*2))
      $ λ locl, type_dealloc_all (argl ++ locl)
        $ type_annot (CtxHintAnnot "post")
        $ type_annot_val (CtxHintAnnot "post_val") v A Φ
    ) R →
    abduct Δ (type_function_return fn lsa lsv Φ v A) R.
  Proof. rewrite -type_function_return_intro_no_unwind //. Qed.

  Global Instance abduct_step_type_function_return_intro_exact_hint Δ fn lsa lsv Φ v A R:
    exact_hint "post_val" →
    AbductStep
      (abduct Δ (
        abd_simpl (zip lsa (fn.(f_args).*2))
        $ λ argl, abd_simpl (zip lsv (fn.(f_local_vars).*2))
        $ λ locl, type_dealloc_all (argl ++ locl)
          $ type_annot (CtxHintAnnot "post")
          $ type_annot_val (CtxHintAnnot "post_val") v A Φ
      ) R)
    Δ (type_function_return fn lsa lsv Φ v A) R | 1.
  Proof. intros ??. by eapply abduct_type_function_return_intro_exact_hint. Qed.


  Lemma abduct_type_function_connect Δ vs ws P R:
    abduct Δ P R →
    abduct Δ (type_function_connect vs ws P) (⌜vs = ws⌝ ∗ R).
  Proof.
    iIntros (Habd) "[HΔ [-> HR]]".
    rewrite /type_function_connect. iSplit; first done.
    iApply Habd. iFrame.
  Qed.

  Global Instance abduct_step_type_function_connect Δ vs ws P R:
    AbductStep (abduct Δ P R) Δ (type_function_connect vs ws P) (⌜vs = ws⌝ ∗ R).
  Proof. intros ?. by eapply abduct_type_function_connect. Qed.


  Lemma fn_implements_pt_intro Δ `{!PersistentCtx Δ} fn T:
    (∀ ws Φ, abduct Δ (type_function fn ws Φ)%I (T ws Φ)) →
    qenvs_ctx_prop Δ ⊢ fn_implements_pt fn T.
  Proof.
    intros Hcont. iIntros "#Hctx". iModIntro. iIntros (Φ lsa lsv) "Hpre".
    rewrite /fn_implements_pt. iDestruct ("Hpre") as "(%vs & %Ψ & %Hlen & Hargs & Hlocs & Hpost & Hcont)".
    iDestruct (Hcont with "[$Hctx $Hpost]") as "Htrans".
    rewrite /type_function.
    rewrite /abd_assume. iSpecialize ("Htrans" $! lsa lsv with "Hargs Hlocs []").
    { iPureIntro. rewrite Hlen fmap_length //. }
    iApply (wps_wand with "Htrans"). iIntros (v) "(%A & Hv & Hargs & Hlocs & Hpost)".
    rewrite /type_unwind. iDestruct ("Hpost" with "Hv") as "(%B & Hv & HΨ)".
    iApply ("Hcont" with "Hargs Hlocs Hv HΨ").
  Qed.

End infer.

Definition fn_typed `{!refinedcG Σ} (Δ: qenvs Σ) (fn: function) (T: pt Σ) : Prop :=
  qenvs_ctx_prop Δ ⊢ fn_implements_pt fn T.

Notation "Δ ⊨ fn : T" := (fn_typed Δ fn T) (at level 20, fn at next level).

Record fn_well_typed `{!refinedcG Σ} (Δ : qenvs Σ) (fn: function) : Type :=
  {
    well_typed_pt: pt Σ; well_typed_pt_typed : Δ ⊨ fn : well_typed_pt;
  }.

Notation "Δ ⊨ fn : ?" := (fn_well_typed Δ fn) (at level 20, fn at next level).

Ltac inferF :=
  match goal with
  | |- fn_well_typed ?Δ ?fn =>
    eexists _; apply: fn_implements_pt_intro; intros ws ret;
    (* abduct [type_function] until [type_function_start], then introduce *)
    abduct; skip_stop; eapply abduct_type_function_start; [abduct_step| |];
    [(* abduct*) | (* simplify *) | (* print*) ]
  end.

Ltac pt_from_well_typed inf :=
  let k := eval simpl in (well_typed_pt _ _ inf) in exact k.

Ltac proof_from_well_typed inf :=
  exact (well_typed_pt_typed _ _ inf).

Notation pt_of inf := (ltac:(pt_from_well_typed inf)) (only parsing).









