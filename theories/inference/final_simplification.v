From quiver.argon.simplification Require Import simplify normalize simplification.
From quiver.argon.base Require Import judgments.
From quiver.base Require Import classes.
From quiver.thorium.logic Require Import syntax.

(* UNFOLD POST *)
Section unfold_type_post.
  Context {Σ: gFunctors}.

  Definition unfold_type_post (P: iProp Σ) (Q: iProp Σ) : Prop := simplify P Q.

  Lemma unfold_type_post_exists {X: Type} (P Q : X → iProp Σ) :
    (∀! x, unfold_type_post (P x) (Q x)) →
    unfold_type_post (∃ x, P x) (∃ x, Q x).
  Proof. rewrite /unfold_type_post /evar_forall. simplify. Qed.

  Lemma unfold_type_post_forall {X: Type} (P Q : X → iProp Σ) :
    (∀! x, unfold_type_post (P x) (Q x)) →
    unfold_type_post (∀ x, P x) (∀ x, Q x).
  Proof. rewrite /unfold_type_post /evar_forall. simplify. Qed.

  Lemma unfold_type_post_nfs Γ Ω P Q :
    unfold_type_post P Q →
    unfold_type_post (nfs Γ Ω P) (nfs Γ Ω Q).
  Proof. rewrite /unfold_type_post. simplify. Qed.

  Lemma unfold_type_post_nfw Γ Ω P Q :
    unfold_type_post P Q →
    unfold_type_post (nfw Γ Ω P) (nfw Γ Ω Q).
  Proof. rewrite /unfold_type_post. simplify. Qed.

  Lemma unfold_type_post_conj P1 P2 Q1 Q2 :
    unfold_type_post P1 Q1 →
    unfold_type_post P2 Q2 →
    unfold_type_post (P1 ∧ P2) (Q1 ∧ Q2).
  Proof. rewrite /unfold_type_post. intros H1 H2. simplify. Qed.

  Lemma unfold_type_post_case (φ: dProp) P1 P2 Q1 Q2 :
    unfold_type_post P1 Q1 →
    unfold_type_post P2 Q2 →
    unfold_type_post (case φ P1 P2) (case φ Q1 Q2).
  Proof. rewrite /unfold_type_post. intros H1 H2. simplify. Qed.

  (* use this for wand, nfw, forall, and postconditions *)
  Lemma unfold_type_post_unfold `{!refinedcG Σ} Φ A :
    unfold_type_post (type_post Φ A) (Φ A).
  Proof.
    rewrite /unfold_type_post. eapply simplify_ent.
    rewrite -type_post_intro type_cont_intro //.
  Qed.

  Lemma unfold_abd_exact_post_unfold P :
    unfold_type_post (abd_exact_post P) P.
  Proof.
    rewrite /unfold_type_post /abd_exact_post. by eapply simplify_ent.
  Qed.

  Lemma unfold_type_post_abort P :
    print ("unfold_post_aborted:", P) →
    unfold_type_post P P.
  Proof. rewrite /unfold_type_post. simplify. Qed.

  Lemma unfold_type_post_simplify P Q :
    unfold_type_post P Q →
    simplify P Q.
  Proof. rewrite /unfold_type_post. simplify. Qed.

End unfold_type_post.

Ltac unfold_type_post := eapply simplify_trans; [apply: unfold_type_post_simplify|].

Existing Class unfold_type_post.
Global Hint Mode unfold_type_post - ! - : typeclass_instances.

Global Existing Instance unfold_type_post_exists | 1.
Global Existing Instance unfold_type_post_forall | 1.
Global Existing Instance unfold_type_post_nfs | 1.
Global Existing Instance unfold_type_post_nfw | 1.
Global Existing Instance unfold_type_post_conj | 1.
Global Existing Instance unfold_type_post_case | 1.
Global Existing Instance unfold_type_post_unfold | 1.
Global Existing Instance unfold_abd_exact_post_unfold | 1.
Global Existing Instance unfold_type_post_abort | 100.


(* SIMPLIFY PASSES *)
Tactic Notation "simplify_passes" :=
  simplify_iter 3;
  prune_exists;
  unfold_type_post;
  normalize;
  cbn;
  simplify_done.
