From quiver.base Require Import classes.
From quiver.argon.abduction Require Import abduct proofmode.
From quiver.thorium.typing Require Import controlflow.
From quiver.base Require Export annotations.
From quiver.thorium.typing Require Import classes.
From quiver.argon.base Require Import judgments.
From quiver.thorium.logic Require Import syntax controlflow.

(** * Flexible hints at any point in the code by [annot: CtxHintAnnot h; s] statements *)

Section ctx.
  Context `{!refinedcG Σ}.

  (* Find a [have_ident] instantiation *)
  Existing Class have_ident.
  Global Hint Mode have_ident + - - : typeclass_instances.

  Definition find_instantiation {A} (ident : string) (Φ : A → iProp Σ) : iProp Σ :=
    ∃ a, Φ a.
  Global Typeclasses Opaque find_instantiation.

  Lemma abduct_find_instantiation Δ {A} (Φ : A → iProp Σ) x a R :
    Δ ⊨ ctx_hyp (have_ident x a) →
    abduct Δ (Φ a) R →
    abduct Δ (find_instantiation x Φ) R.
  Proof.
    iIntros (_ Ha) "Ha".
    iExists a. by iApply Ha.
  Qed.
  Global Instance abduct_step_find_instantiation Δ {A} (Φ : A → iProp Σ) a x R :
    Δ ⊨ ctx_hyp (have_ident x a) →
    AbductStep (abduct Δ (Φ a) R) Δ (find_instantiation x Φ) R.
  Proof. intros ??. eapply abduct_find_instantiation; eassumption. Qed.

  (* Find the location for a local variable / argument *)
  Definition find_local (ident : string) (Φ : loc → iProp Σ) : iProp Σ :=
    ∃ a, Φ a.
  Global Typeclasses Opaque find_local.

  Lemma abduct_find_local Δ (Φ : loc → iProp Σ) x a M R :
    local_map M →
    ComputeMapLookup M x a →
    abduct Δ (Φ a) R →
    abduct Δ (find_local x Φ) R.
  Proof.
    iIntros (_ _ Ha) "Ha".
    iExists a. by iApply Ha.
  Qed.
  Global Instance abduct_step_find_local Δ (Φ : loc → iProp Σ) a x M R :
    local_map M →
    ComputeMapLookup M x a →
    AbductStep (abduct Δ (Φ a) R) Δ (find_local x Φ) R.
  Proof. intros ???. eapply abduct_find_local; eassumption. Qed.

  (* find the value of a argument *)
  Definition find_arg_val (ident : string) (Φ : val → iProp Σ) : iProp Σ :=
    ∃ a, Φ a.
  Global Typeclasses Opaque find_arg_val.

  Lemma abduct_find_arg_val Δ (Φ : val → iProp Σ) x a M R :
    val_map M →
    ComputeMapLookup M x a →
    abduct Δ (Φ a) R →
    abduct Δ (find_arg_val x Φ) R.
  Proof.
    iIntros (_ _ Ha) "Ha".
    iExists a. by iApply Ha.
  Qed.
  Global Instance abduct_step_find_arg_val Δ (Φ : val → iProp Σ) a x M R :
    val_map M →
    ComputeMapLookup M x a →
    AbductStep (abduct Δ (Φ a) R) Δ (find_arg_val x Φ) R.
  Proof. intros ???. eapply abduct_find_arg_val; eassumption. Qed.


End ctx.

(** Statement hints *)
Section stmts.
  Context `{!refinedcG Σ}.

  Inductive hint_prop_ex : Type :=
  (** Bind [ident] to the instantiation found for an existential quantifier *)
  | HintEx {A} (ident : string) (Φ : A → hint_prop_ex)
  (** Anonymous existential quantifier *)
  | HintExAnon {A} (Φ : A → hint_prop_ex)
  (** Base proposition to prove *)
  | HintBase (P : iProp Σ).

  Inductive hint_prop : Type :=
  (** Lookup a [have_ident] *)
  | HintAll {A} (ident : string) (Φ : A → hint_prop)
  (** Lookup the name of a local variable / argument *)
  | HintLocal (ident : string) (Φ : loc → hint_prop)
  | HintArgVal (ident : string) (Φ : val → hint_prop)
  | HintAllBase (h : hint_prop_ex).

  Coercion HintAllBase : hint_prop_ex >-> hint_prop.
  Coercion HintBase : iProp >-> hint_prop_ex.

  (* Put hints in the context with a name *)
  Inductive have_hint (i : string) (h : hint_prop) :=
    | HaveHint.
  Existing Class have_hint.
  Global Hint Mode have_hint + - : typeclass_instances.
  Global Arguments have_hint _ _%E.


  Inductive exact_hint (i: string) :=
    | ExactHint.
  Existing Class exact_hint.
  Global Hint Mode exact_hint + : typeclass_instances.
  Global Arguments exact_hint _%E.

  Global Instance exact_hint_post:
    exact_post →
    exact_hint "post_val".
  Proof. intros ?. apply ExactHint. Qed.


  (** Interpretation of hints *)
  Fixpoint interpret_hint_prop_ex (h : hint_prop_ex) (T : iProp Σ) : iProp Σ :=
    match h with
    | HintEx x Φ => ∃ x', interpret_hint_prop_ex (Φ x') (
        abd_assume ASSUME (⌜have_ident x x'⌝) T)
    | HintExAnon Φ => ∃ x', interpret_hint_prop_ex (Φ x') T
    | HintBase P => P ∗ type_precond_yield (abd_assume ASSERT P T)
    end.
  Fixpoint interpret_hint_prop (h : hint_prop) (T : iProp Σ) : iProp Σ :=
    match h with
    | HintAll x Φ => find_instantiation x (λ x', interpret_hint_prop (Φ x') T)
    | HintLocal x Φ => find_local x (λ x', interpret_hint_prop (Φ x') T)
    | HintArgVal x Φ => find_arg_val x (λ x', interpret_hint_prop (Φ x') T)
    | HintAllBase h => type_precond unit RECOVER (λ _, interpret_hint_prop_ex h T) (λ _, []) (λ _, []) (λ _, []) (λ _, None)
    end.

  Lemma interpret_hint_prop_ex_elim h T :
    interpret_hint_prop_ex h T -∗ T.
  Proof.
    iIntros "Ha".
    iInduction h as [ | | ] "IH" forall (T); simpl.
    - iDestruct "Ha" as (x') "Ha".
      iPoseProof ("IH" with "Ha") as "Ha".
      rewrite /abd_assume. iApply "Ha". done.
    - iDestruct "Ha" as (x') "Ha".
      iPoseProof ("IH" with "Ha") as "Ha". done.
    - iDestruct "Ha" as "(HP & HT)".
      rewrite /type_precond_yield /abd_assume. by iApply "HT".
  Qed.
  Lemma interpret_hint_prop_elim h T :
    interpret_hint_prop h T -∗ T.
  Proof.
    iIntros "Ha". iInduction h as [ | | |] "IH" forall (T); simpl.
    - rewrite /find_instantiation. iDestruct "Ha" as (a) "Ha".
      by iApply "IH".
    - rewrite /find_local. iDestruct "Ha" as (a) "Ha".
      by iApply "IH".
    - rewrite /find_arg_val. iDestruct "Ha" as (a) "Ha".
      by iApply "IH".
    - rewrite /type_precond. iDestruct "Ha" as "(% & Ha & _)".
      iApply interpret_hint_prop_ex_elim. done.
  Qed.


  (** Invariant interpretation of hints *)
  Fixpoint hint_to_invariant_ex (h : hint_prop_ex) : iProp Σ :=
    match h with
    | HintEx x Φ => ∃ x, hint_to_invariant_ex (Φ x)
    | HintExAnon Φ => ∃ x', hint_to_invariant_ex (Φ x')
    | HintBase P => P
    end.

  Fixpoint hint_to_invariant (h : hint_prop) (F: iProp Σ → iProp Σ) : iProp Σ :=
    match h with
    | HintAll x Φ => find_instantiation x (λ x', hint_to_invariant (Φ x') F)
    | HintLocal x Φ => find_local x (λ x', hint_to_invariant (Φ x') F)
    | HintArgVal x Φ => find_arg_val x (λ x', hint_to_invariant (Φ x') F)
    | HintAllBase h => F (hint_to_invariant_ex h)
    end.

  Lemma hint_to_invariant_unchanged h P:
    hint_to_invariant h (λ _, P) ⊢ P.
  Proof.
    induction h as [A s Φ IH| A Φ IH | A Φ IH  |]; simpl.
    - rewrite /find_instantiation. iDestruct 1 as "[%a Hx]".
      by iApply IH.
    - rewrite /find_local. iDestruct 1 as "[%a Hx]".
      by iApply IH.
    - rewrite /find_arg_val. iDestruct 1 as "[%a Hx]".
      by iApply IH.
    - done.
  Qed.

  Definition type_annot (a: context_hint_annot) (P: iProp Σ) : iProp Σ := P.
  Global Typeclasses Opaque type_annot.

  Definition type_hint (a: hint_prop) (P: iProp Σ) : iProp Σ := P.
  Global Typeclasses Opaque type_hint.


  Lemma abduct_type_annot_stmt_annot Δ a s Q Φ R :
    abduct Δ (type_annot a (swp s Q Φ)) R →
    abduct Δ (type_annot_stmt a s Q Φ) R.
  Proof.
    rewrite /type_annot /type_annot_stmt.
    iIntros (Habd) "Hctx". iApply swp_annot_stmt.
    by iApply Habd.
  Qed.

  Global Instance abduct_step_type_annot_stmt_annot Δ a s Q Φ R :
    AbductStep (abduct Δ (type_annot a (swp s Q Φ)) R) Δ (type_annot_stmt a s Q Φ) R.
  Proof. intros ?. by apply abduct_type_annot_stmt_annot. Qed.

  Lemma abduct_type_annot_type_hint Δ i h P R :
    have_hint i h →
    abduct Δ (type_hint h P) R →
    abduct Δ (type_annot (CtxHintAnnot i) P) R.
  Proof. rewrite /type_hint /type_annot //. Qed.

  Global Instance abduct_step_type_annot_type_hint Δ i h P R :
    have_hint i h →
    AbductStep (abduct Δ (type_hint h P) R) Δ (type_annot (CtxHintAnnot i) P) R | 1.
  Proof. intros ??. by eapply abduct_type_annot_type_hint. Qed.

  (* lower-priority hint in case we cannot find the hint in the context *)
  Lemma abduct_type_annot_no_hint a Δ P R :
    abduct Δ P R →
    abduct Δ (type_annot a P) R.
  Proof. rewrite /type_annot //. Qed.

  Global Instance abduct_step_type_annot_no_hint a Δ P R :
    AbductStep (abduct Δ P R) Δ (type_annot a P) R | 2.
  Proof. intros ?. by eapply abduct_type_annot_no_hint. Qed.


  Lemma abduct_type_hint Δ h P R :
    abduct Δ (abd_breakpoint (λ Q, interpret_hint_prop h Q) (λ Pre, abd_evars Pre (λ Pre', Pre' P))) R →
    abduct Δ (type_hint h P) R.
  Proof.
    rewrite /type_hint /abd_breakpoint /abd_evars. iIntros (Habd) "Hctx".
    iPoseProof (Habd with "Hctx") as "(%Pre & Hw & %Pre' & Hw2 & Hcont)".
    iDestruct ("Hw2" with "Hcont") as "Hcont". iDestruct ("Hw" with "Hcont") as "Hcont".
    by iApply interpret_hint_prop_elim.
  Qed.

  Global Instance abduct_step_type_hint Δ h P R :
    AbductStep (abduct Δ (abd_breakpoint (λ Q, interpret_hint_prop h Q) (λ Pre, abd_evars Pre (λ Pre', Pre' P))) R) Δ (type_hint h P) R.
  Proof. intros ?. by apply abduct_type_hint. Qed.


  Lemma abduct_type_loop_breakpoint Δ lb lb_exit i h P cfg Φ R:
    have_hint i h →
    Simpl (hint_to_invariant h (λ I, type_loop_core lb I cfg Φ)%I) P →
    abduct Δ (type_break_at lb_exit cfg Φ P) R →
    abduct Δ (type_loop lb lb_exit i cfg Φ) R.
  Proof.
    intros _ <-. rewrite /type_break_at.
    rewrite /type_loop hint_to_invariant_unchanged //.
  Qed.

  Global Instance abduct_step_type_loop_breakpoint  Δ lb lb_exit i h P cfg Φ R:
    have_hint i h →
    Simpl (hint_to_invariant h (λ I, type_loop_core lb I cfg Φ)%I) P →
    AbductStep (abduct Δ (type_break_at lb_exit cfg Φ P) R) Δ (type_loop lb lb_exit i cfg Φ) R | 30.
  Proof. intros ???. by eapply abduct_type_loop_breakpoint. Qed.

  Lemma abduct_type_loop_breakpoint_no_hint Δ lb lb_exit i P cfg Φ R:
    Simpl (type_loop_core lb True%I cfg Φ) P →
    abduct Δ (type_break_at lb_exit cfg Φ P) R →
    abduct Δ (type_loop lb lb_exit i cfg Φ) R.
  Proof.
    intros <-. rewrite /type_break_at.
    rewrite /type_loop //.
  Qed.

  Global Instance abduct_step_type_loop_breakpoint_no_hint Δ lb lb_exit i P cfg Φ R:
    Simpl (type_loop_core lb True%I cfg Φ) P →
    AbductStep (abduct Δ (type_break_at lb_exit cfg Φ P) R) Δ (type_loop lb lb_exit i cfg Φ) R | 50.
  Proof. intros ??. by eapply abduct_type_loop_breakpoint_no_hint. Qed.

End stmts.

Notation "'∃{{' s '}}' x , P" := (@HintEx _ _ s (λ x, P))
  (at level 200, x binder, right associativity,
   format "'[hv' '∃{{' s '}}' '/ '  x  , '/ '  P ']'") : bi_scope.
Notation "'∀{{' s '}}' x , P" := (@HintAll _ _ s (λ x, P))
  (at level 200, x binder, right associativity,
  format "'[hv' '∀{{' s '}}' '/ '  x  , '/ '  P ']'") : bi_scope.
Notation "'L{{' s '}}' x , P" := (@HintLocal _ s (λ x, P))
  (at level 200, x binder, right associativity,
  format "'[hv' 'L{{' s '}}' '/ '  x  , '/ '  P ']'") : bi_scope.
Notation "'V{{' s '}}' x , P" := (@HintArgVal _ s (λ x, P))
  (at level 200, x binder, right associativity,
  format "'[hv' 'V{{' s '}}' '/ '  x  , '/ '  P ']'") : bi_scope.
Notation "'∃?' x ',' P" := (@HintExAnon _ _ (λ x, P))
  (at level 200, x binder, right associativity,
  format "'[hv' '∃?' '/ '  x  , '/ '  P ']'") : bi_scope.

Ltac pose_hint i h :=
  assert (have_hint i h%I) as ?; first apply HaveHint.


(** Expression hints *)
(* basically, want to get the value and maybe an inferred type and then link up to an annotated type *)
Section expr_hints.
  Context `{!refinedcG Σ}.

  Inductive ehint_prop_ex : Type :=
  (** Bind [ident] to the instantiation found for an existential quantifier *)
  | EHintEx {A} (ident : string) (Φ : A → ehint_prop_ex)
  (** Anonymous existential quantifier *)
  | EHintExAnon {A} (Φ : A → ehint_prop_ex)
  (** Base proposition to prove: a type assignment for the value, and a proposition *)
  | EHintBase (τ : type Σ) (P : iProp Σ).

  Inductive ehint_prop : Type :=
  (** Lookup a [have_ident] *)
  | EHintAll {A} (ident : string) (Φ : A → ehint_prop)
  (** Lookup the name of a local variable / argument *)
  | EHintLocal (ident : string) (Φ : loc → ehint_prop)
  | EHintArgVal (ident : string) (Φ : val → ehint_prop)
  | EHintAllBase (h : ehint_prop_ex).


  Coercion EHintAllBase : ehint_prop_ex >-> ehint_prop.

  (* Put hints in the context with a name *)
  Inductive have_expr_hint (i : string) (h : ehint_prop) :=
    | EHaveHint.
  Existing Class have_expr_hint.
  Global Hint Mode have_expr_hint + - : typeclass_instances.
  Global Arguments have_expr_hint _ _%E.


  (** Interpretation of hints *)
  Fixpoint interpret_ehint_prop_ex (h : ehint_prop_ex) (T : type Σ → iProp Σ) (f : val) : iProp Σ :=
    match h with
    | EHintEx x Φ => ∃ x', interpret_ehint_prop_ex (Φ x') (
        λ A, abd_assume ASSUME (⌜have_ident x x'⌝) (T A)) f
    | EHintExAnon Φ => ∃ x', interpret_ehint_prop_ex (Φ x') T f
    | EHintBase F' P =>
        f ◁ᵥ F' ∗ P ∗
          type_precond_yield (abd_assume ASSERT P (T F'))
    end.

  Fixpoint ehint_to_post (h: ehint_prop_ex) (Ψ: type Σ → iProp Σ) : iProp Σ :=
    match h with
    | EHintEx x Φ => ∀ x', ehint_to_post (Φ x') Ψ
    | EHintExAnon Φ => ∀ x', ehint_to_post (Φ x') Ψ
    | EHintBase F' P => P -∗ Ψ F'
    end.

  Fixpoint interpret_ehint_prop_ex_cont (h : ehint_prop_ex) (P: iProp Σ) (f : val) : iProp Σ :=
    match h with
    | EHintEx x Φ => ∃ x', interpret_ehint_prop_ex_cont (Φ x') P f
    | EHintExAnon Φ => ∃ x', interpret_ehint_prop_ex_cont (Φ x') P f
    | EHintBase F' Q => f ◁ᵥ F' ∗ Q ∗ type_precond_yield P
    end.

  Lemma interpret_ehint_prop_ex_cont_mono h P Q f:
    (P ⊢ Q) →
    interpret_ehint_prop_ex_cont h P f ⊢ interpret_ehint_prop_ex_cont h Q f.
  Proof.
    intros Hent; induction h as [X s Φ IH | X Φ IH | ]; simpl.
    - iIntros "[%x Hx]". iExists x. by iApply IH.
    - iIntros "[%x Hx]". iExists x. by iApply IH.
    - rewrite /type_precond_yield Hent //.
  Qed.

  Lemma interpret_ehint_prop_ex_mono h Φ Ψ f:
    (∀ A, Φ A ⊢ Ψ A) →
    interpret_ehint_prop_ex h Φ f ⊢ interpret_ehint_prop_ex h Ψ f.
  Proof.
    induction h as [X s Ξ IH | X Ξ IH | ] in Φ, Ψ |-*; simpl; intros Hent.
    - iIntros "[%x Hx]". iExists x. iApply IH; last done.
      intros A; rewrite /abd_assume Hent //.
    - iIntros "[%x Hx]". iExists x. iApply IH; done.
    - rewrite /type_precond_yield /abd_assume.
      rewrite Hent //.
  Qed.

  Lemma interpret_ehint_prop_agree (h: ehint_prop_ex) (Ψ: type Σ → iProp Σ) f:
    interpret_ehint_prop_ex_cont h (abd_exact_post (ehint_to_post h Ψ)) f ⊢ interpret_ehint_prop_ex h Ψ f.
  Proof.
    rewrite /abd_exact_post. induction h as [X s Φ IH | X Φ IH| ]; simpl.
    - iIntros "[%x Hx]". iExists x.
      iDestruct (interpret_ehint_prop_ex_cont_mono with "Hx") as "Hx"; last first.
      + iDestruct (IH with "Hx") as "Hx".
        rewrite interpret_ehint_prop_ex_mono; first done.
        intros A. rewrite /abd_assume. by iIntros "$".
      + iIntros "Hx". by iApply "Hx".
    - iIntros "[%x Hx]". iExists x.
      iDestruct (interpret_ehint_prop_ex_cont_mono with "Hx") as "Hx"; last first.
      + iDestruct (IH with "Hx") as "Hx". done.
      + iIntros "Hx". by iApply "Hx".
    - rewrite /type_precond_yield /abd_assume //.
  Qed.

  Fixpoint interpret_ehint_prop (h : ehint_prop) (T : type Σ → iProp Σ) (f : val) : iProp Σ :=
    match h with
    | EHintAll x Φ => find_instantiation x (λ x', interpret_ehint_prop (Φ x') T f)
    | EHintLocal x Φ => find_local x (λ x', interpret_ehint_prop (Φ x') T f)
    | EHintArgVal x Φ => find_arg_val x (λ x', interpret_ehint_prop (Φ x') T f)
    | EHintAllBase h => type_precond unit RECOVER (λ _, interpret_ehint_prop_ex h T f) (λ _, []) (λ _, []) (λ _, []) (λ _, None)
    end.


  Fixpoint interpret_ehint_exact_prop (h : ehint_prop) (T : type Σ → iProp Σ) (f : val) : iProp Σ :=
    match h with
    | EHintAll x Φ => find_instantiation x (λ x', interpret_ehint_exact_prop (Φ x') T f)
    | EHintLocal x Φ => find_local x (λ x', interpret_ehint_exact_prop (Φ x') T f)
    | EHintArgVal x Φ => find_arg_val x (λ x', interpret_ehint_exact_prop (Φ x') T f)
    | EHintAllBase h => type_precond unit RECOVER (λ _, interpret_ehint_prop_ex_cont h (abd_exact_post (ehint_to_post h T)) f) (λ _, []) (λ _, []) (λ _, []) (λ _, None)
    end.


  Lemma interpret_ehint_prop_ex_elim h T f :
    interpret_ehint_prop_ex h T f -∗ ∃ F', f ◁ᵥ F' ∗ T F'.
  Proof.
    iIntros "Ha".
    iInduction h as [ | | ] "IH" forall (T); simpl.
    - iDestruct "Ha" as (x') "Ha".
      iPoseProof ("IH" with "Ha") as "(%F' & Hf & Hcont)".
      rewrite /abd_assume. iExists _. iFrame.
      iApply "Hcont". done.
    - iDestruct "Ha" as (x') "Ha".
      iPoseProof ("IH" with "Ha") as "Ha". done.
    - rewrite /abd_assume.
      iDestruct ("Ha") as "(Hf & HP & HT)".
      rewrite /type_precond_yield.
      iExists _. iFrame.
      by iApply ("HT" with "HP").
  Qed.


  Lemma interpret_ehint_prop_elim h T f :
    interpret_ehint_prop h T f -∗ ∃ F', f ◁ᵥ F' ∗ T F'.
  Proof.
    iIntros "Ha". iInduction h as [ | | |] "IH" forall (T); simpl.
    - rewrite /find_instantiation. iDestruct "Ha" as (a) "Ha".
      by iApply ("IH").
    - rewrite /find_local. iDestruct "Ha" as (a) "Ha".
      by iApply ("IH").
    - rewrite /find_arg_val. iDestruct "Ha" as (a) "Ha".
      by iApply ("IH").
    - rewrite /type_precond. iDestruct "Ha" as "(% & Ha & _)".
      iApply (interpret_ehint_prop_ex_elim). done.
  Qed.

  Lemma interpret_ehint_exact_prop_elim h T f :
    interpret_ehint_exact_prop h T f -∗ ∃ F', f ◁ᵥ F' ∗ T F'.
  Proof.
    iIntros "Ha". iInduction h as [ | | |] "IH" forall (T); simpl.
    - rewrite /find_instantiation. iDestruct "Ha" as (a) "Ha".
      by iApply ("IH").
    - rewrite /find_local. iDestruct "Ha" as (a) "Ha".
      by iApply ("IH").
    - rewrite /find_arg_val. iDestruct "Ha" as (a) "Ha".
      by iApply ("IH").
    - rewrite /type_precond. iDestruct "Ha" as "(% & Ha & _)".
      iApply (interpret_ehint_prop_ex_elim).
      iApply interpret_ehint_prop_agree.
      done.
  Qed.


  Definition type_hint_val (a: ehint_prop) (f: val) (F: type Σ) (P: type Σ → iProp Σ) : iProp Σ :=
    f ◁ᵥ F -∗ ∃ F', f ◁ᵥ F' ∗ P F'.
  Global Typeclasses Opaque type_hint_val.

  Definition type_hint_val_exact (a: ehint_prop) (f: val) (F: type Σ) (P: type Σ → iProp Σ) : iProp Σ :=
    f ◁ᵥ F -∗ ∃ F', f ◁ᵥ F' ∗ P F'.
  Global Typeclasses Opaque type_hint_val_exact.

  Definition type_annot_val (a: context_hint_annot) (f: val) (F: type Σ) (P: type Σ →iProp Σ) : iProp Σ :=
    f ◁ᵥ F -∗ ∃ F', f ◁ᵥ F' ∗ P F'.
  Global Typeclasses Opaque type_annot_val.

  (* TODO move *)
  Lemma wp_annot_expr_intro (e : expr) n {B} (b : B) Φ :
    ⊢ WP e {{ v, Φ v } } -∗ WP (AnnotExpr n b e) {{ v, Φ v } }.
  Proof.
    iInduction n as [ | n] "IH".
    { simpl; eauto. }
    rewrite annot_expr_S.
    iIntros "Ha".
    iPoseProof ("IH" with "Ha") as "Ha".
    iApply (wp_bind (λ e, lang_fill_item (ExprCtx SkipECtx) e)).
    fold to_rtexpr. iApply (wp_wand with "Ha").
    iIntros (v) "Hv". by iApply wp_skip.
  Qed.

  Lemma type_annot_expr_type_annot_val n a f F Φ :
    type_annot_val a f F (Φ f) ⊢ type_annot_expr a n f F Φ.
  Proof.
    rewrite /type_annot_val /type_annot_expr.
    iIntros "Hcont Hf". iApply wp_annot_expr_intro.
    iApply wp_value. iApply ("Hcont" with "Hf").
  Qed.

  Lemma abduct_type_expr_annot_type_annot_expr Δ a n f F Φ R :
    abduct Δ (type_annot_val a f F (Φ f)) R →
    abduct Δ (type_annot_expr a n f F Φ) R.
  Proof.
    rewrite -type_annot_expr_type_annot_val //.
  Qed.

  Global Instance abduct_step_type_expr_annot_type_annot_expr Δ a n R f F Φ:
    AbductStep (abduct Δ (type_annot_val a f F (Φ f)) R) Δ (type_annot_expr a n f F Φ) R.
  Proof. intros ?. by eapply abduct_type_expr_annot_type_annot_expr. Qed.

  Lemma abduct_type_expr_annot_type_expr_hint_exact Δ i h f F Φ R :
    sequence_classes [
      exact_hint i;
      have_expr_hint i h
    ] →
    abduct Δ (type_hint_val_exact h f F Φ) R →
    abduct Δ (type_annot_val (CtxHintAnnot i) f F Φ) R.
  Proof.
    rewrite /type_hint_val_exact /type_annot_val.
    iIntros (_ Habd) "Hctx Hf". iPoseProof (Habd with "Hctx Hf") as "(%F' & Hf & HT)".
    iExists F'. iFrame.
  Qed.

  Global Instance abduct_step_type_expr_annot_type_expr_hint_exact Δ i h R f F Φ:
    sequence_classes [
      exact_hint i;
      have_expr_hint i h
    ] →
    AbductStep (abduct Δ (type_hint_val_exact h f F Φ) R) Δ (type_annot_val (CtxHintAnnot i) f F Φ) R | 1.
  Proof. intros ??. by eapply abduct_type_expr_annot_type_expr_hint_exact. Qed.

  Lemma abduct_type_expr_annot_type_expr_hint Δ i h f F Φ R :
    have_expr_hint i h →
    abduct Δ (type_hint_val h f F Φ) R →
    abduct Δ (type_annot_val (CtxHintAnnot i) f F Φ) R.
  Proof.
    rewrite /type_hint_val /type_annot_val.
    iIntros (_ Habd) "Hctx Hf". iPoseProof (Habd with "Hctx Hf") as "(%F' & Hf & HT)".
    iExists F'. iFrame.
  Qed.

  Global Instance abduct_step_type_expr_annot_type_expr_hint Δ i h R f F Φ:
    have_expr_hint i h →
    AbductStep (abduct Δ (type_hint_val h f F Φ) R) Δ (type_annot_val (CtxHintAnnot i) f F Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_expr_annot_type_expr_hint. Qed.

  (* lower-priority hint in case we cannot find the hint in the context *)
  Lemma abduct_type_expr_annot_no_hint a Δ R f F Φ :
    abduct Δ (Φ F) R →
    abduct Δ (type_annot_val a f F Φ) R.
  Proof.
    rewrite /type_annot_val.
    iIntros (Habd) "Hctx Hf". iExists _. iFrame.
    by iApply Habd.
  Qed.

  Global Instance abduct_step_type_expr_annot_no_hint a Δ R f F Φ :
    AbductStep (abduct Δ (Φ F) R) Δ (type_annot_val a f F Φ) R | 3.
  Proof. intros ?. by eapply abduct_type_expr_annot_no_hint. Qed.

  Lemma abduct_type_expr_hint Δ h R f F Φ:
    abduct Δ (
      abd_assume ASSUME (f ◁ᵥ F) (
      abd_breakpoint (λ Q, interpret_ehint_prop h Q f)
      (λ Pre, abd_evars Pre (λ Pre', Pre' Φ)))) R →
    abduct Δ (type_hint_val h f F Φ) R.
  Proof.
    rewrite /type_hint_val /abd_breakpoint /abd_evars. iIntros (Habd) "Hctx Hf".
    rewrite /abd_assume in Habd.
    iPoseProof (Habd with "Hctx Hf") as "(%Pre & Hw & %Pre' & Hw2 & Hcont)".
    iDestruct ("Hw2" with "Hcont") as "Hcont". iDestruct ("Hw" with "Hcont") as "Hcont".
    by iApply interpret_ehint_prop_elim.
  Qed.

  Global Instance abduct_step_type_expr_hint Δ h R f F Φ:
    AbductStep (abduct Δ (
      abd_assume ASSUME (f ◁ᵥ F) (
      abd_breakpoint (λ Q, interpret_ehint_prop h Q f)
      (λ Pre, abd_evars Pre (λ Pre', Pre' Φ)))) R)
  Δ (type_hint_val h f F Φ) R.
  Proof. intros ?. by apply abduct_type_expr_hint. Qed.

  Lemma abduct_type_expr_exact_hint Δ h R f F Φ:
    abduct Δ (
      abd_assume ASSUME (f ◁ᵥ F) (interpret_ehint_exact_prop h Φ f)) R →
    abduct Δ (type_hint_val_exact h f F Φ) R.
  Proof.
    rewrite /type_hint_val_exact. iIntros (Habd) "Hctx Hf".
    rewrite /abd_assume in Habd.
    iPoseProof (Habd with "Hctx Hf") as "Hcont".
    by iApply interpret_ehint_exact_prop_elim.
  Qed.

  Global Instance abduct_step_type_hint_val_exact Δ h R f F Φ:
      AbductStep (abduct Δ (
        abd_assume ASSUME (f ◁ᵥ F) (
        interpret_ehint_exact_prop h Φ f)) R)
    Δ (type_hint_val_exact h f F Φ) R.
  Proof.
    intros ?. by eapply abduct_type_expr_exact_hint.
  Qed.

End expr_hints.

Notation "'∃[{' s '}]' x , P" := (@EHintEx _ _ _ s (λ x, P))
  (at level 200, x binder, right associativity,
   format "'[hv' '∃[{' s '}]' '/ '  x  , '/ '  P ']'") : bi_scope.
Notation "'∀[{' s '}]' x , P" := (@EHintAll _ _ _ s (λ x, P))
  (at level 200, x binder, right associativity,
  format "'[hv' '∀[{' s '}]' '/ '  x  , '/ '  P ']'") : bi_scope.
Notation "'L[{' s '}]' x , P" := (@EHintLocal _ _ s (λ x, P))
  (at level 200, x binder, right associativity,
  format "'[hv' 'L[{' s '}]' '/ '  x  , '/ '  P ']'") : bi_scope.
Notation "'V[{' s '}]' x , P" := (@EHintArgVal _ _ s (λ x, P))
  (at level 200, x binder, right associativity,
  format "'[hv' 'V[{' s '}]' '/ '  x  , '/ '  P ']'") : bi_scope.
Notation "'∃?e' x ',' P" := (@EHintExAnon _ _ _ (λ x, P))
  (at level 200, x binder, right associativity,
  format "'[hv' '∃?e' '/ '  x  , '/ '  P ']'") : bi_scope.

Ltac pose_expr_hint i h :=
  assert (have_expr_hint i h%I) as ?; first apply EHaveHint.

Ltac is_exact_hint i :=
  assert (exact_hint i) as ?; first apply ExactHint.