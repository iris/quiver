From quiver.base Require Import dprop facts classes.
From quiver.argon.base Require Import classes.
From quiver.argon.simplification Require Import simplify normalize.
From quiver.inference Require Import inferPT.



(*
Inductive diagnostics : Prop := Diagnostics.
Existing Class diagnostics.

Global Instance debug_print_no_diagnostics `{!TCUnless diagnostics} {X: Type} (x: X) : debug_print x | 1.
Proof. rewrite /debug_print. rewrite sealed_eq //. Qed. *)

Class debug_print {X: Type} (x: X) :=
  debug_print_core: sealed True.
Global Hint Mode debug_print - + : typeclass_instances.


Global Instance debug_print_diagnostics {X: Type} (x y: X) :
  sequence_classes [Simpl x y; print y] →
  debug_print x | 2.
Proof. rewrite /debug_print. rewrite sealed_eq //. Qed.



(* debug printing for simplify, solve, and missing *)
Record debug_print_simplify_pure (star: bool) (φ ψ: Prop) :=
  { debug_print_simplfiy_pure_inst: sealed True }.

Record debug_print_solve (φ: Prop) :=
  { debug_print_solve_inst: sealed True }.

Record debug_print_missing (φ: Prop) :=
    { debug_print_missing_inst: sealed True }.

Record debug_print_decided (φ: Prop) :=
  { debug_print_decided_inst: sealed True }.


Lemma debug_print_simplify_pure_false (φ ψ: Prop):
  debug_print ("cleanup", φ → ψ) →
  debug_print_simplify_pure false φ ψ.
Proof. split; rewrite sealed_eq //. Qed.

Lemma debug_print_simplify_pure_true (φ: Prop):
  debug_print ("solve", φ) →
  debug_print_simplify_pure true φ True.
Proof. split; rewrite sealed_eq //. Qed.

Lemma debug_print_simplify_pure_simpl (φ ψ: Prop):
  debug_print ("simplify", ψ → φ) →
  debug_print_simplify_pure true φ ψ.
Proof. split; rewrite sealed_eq //. Qed.

Lemma debug_print_solve_true :
  debug_print_solve True.
Proof. split. rewrite sealed_eq //. Qed.

Lemma debug_print_solve_otherwise φ:
  debug_print ("solve", φ) →
  debug_print_solve φ.
Proof. split. rewrite sealed_eq //. Qed.

Lemma debug_print_missing_print φ:
  debug_print ("missing", φ) →
  debug_print_missing φ.
Proof. split. rewrite sealed_eq //. Qed.

Lemma debug_print_decided_print φ:
  debug_print ("decided", φ) →
  debug_print_decided φ.
Proof. split. rewrite sealed_eq //. Qed.


Existing Class debug_print_simplify_pure.
Global Hint Mode debug_print_simplify_pure + ! - : typeclass_instances.
Global Existing Instance debug_print_simplify_pure_false | 1.
Global Existing Instance debug_print_simplify_pure_true | 1.
Global Existing Instance debug_print_simplify_pure_simpl | 2.

Existing Class debug_print_solve.
Global Hint Mode debug_print_solve + : typeclass_instances.
Global Existing Instance debug_print_solve_true | 1.
Global Existing Instance debug_print_solve_otherwise | 2.

Existing Class debug_print_missing.
Global Hint Mode debug_print_missing + : typeclass_instances.
Global Existing Instance debug_print_missing_print | 1.

Existing Class debug_print_decided.
Global Hint Mode debug_print_decided + : typeclass_instances.
Global Existing Instance debug_print_decided_print | 1.






Section calculate_prop_size.
  Context `{!refinedcG Σ}.

  Inductive prop_size : iProp Σ → nat → Prop :=
  | prop_size_return {X} (A: X) Φ:
      debug_print ("post", Φ A) →
      prop_size (Φ A) 1
  | prop_size_exists A (P: A → iProp Σ) n:
      (∀ x, prop_size (P x) n) →
      prop_size (∃ x, P x) (S n)
  | prop_size_forall A (P: A → iProp Σ) n:
      (∀ x, prop_size (P x) n) →
      prop_size (∀ x, P x) (S n)
  | prop_size_nfs_cons_pure φ Γ Δ P n:
      prop_size (nfs Γ Δ P) n →
      prop_size (nfs (φ :: Γ) Δ P) (S n)
  | prop_size_nfs_cons_atom A Γ Δ P n:
      prop_size (nfs Γ Δ P) n →
      prop_size (nfs Γ (A :: Δ) P) (S n)
  | prop_size_nfs_nil_nil P n:
      prop_size P n →
      prop_size (nfs nil nil P) n
  | prop_size_nfw_cons_pure φ Γ Δ P n:
      prop_size (nfw Γ Δ P) n →
      prop_size (nfw (φ :: Γ) Δ P) (S n)
  | prop_size_nfw_cons_atom A Γ Δ P n:
      prop_size (nfw Γ Δ P) n →
      prop_size (nfw Γ (A :: Δ) P) (S n)
  | prop_size_nfw_nil_nil P n:
      prop_size P n →
      prop_size (nfw nil nil P) n
  | prop_size_case φ P1 P2 n m:
      prop_size P1 n →
      prop_size P2 m →
      prop_size (case φ P1 P2) (S (n + m))
  | prop_size_conj P1 P2 n m:
      prop_size P1 n →
      prop_size P2 m →
      prop_size (P1 ∧ P2) (S (n + m)).

End calculate_prop_size.

Existing Class prop_size.
Global Hint Mode prop_size - ! - : typeclass_instances.

Global Existing Instance prop_size_return | 100.
Global Existing Instance prop_size_exists | 1.
Global Existing Instance prop_size_forall | 1.
Global Existing Instance prop_size_nfs_cons_pure | 1.
Global Existing Instance prop_size_nfs_cons_atom | 1.
Global Existing Instance prop_size_nfs_nil_nil | 1.
Global Existing Instance prop_size_nfw_cons_pure | 1.
Global Existing Instance prop_size_nfw_cons_atom | 1.
Global Existing Instance prop_size_nfw_nil_nil | 1.
Global Existing Instance prop_size_case | 1.
Global Existing Instance prop_size_conj | 1.



(* remove the rules without debug information *)
Global Remove Hints
  (* pure *)
  abduct_step_pure_prove
  abduct_step_pure_missing
  abduct_step_pure_simplify_pure
  abduct_step_class_pure_inst
  (* precond *)
  abduct_step_type_precond_simplify_pure
  (* proofmode *)
  abduct_step_case_left
  abduct_step_case_right
  (* simplification *)
  simplify_prop_simplify_pure
  (* existentials *)
  elim_this_universal_inst
  elim_this_existental_inst
  (* types *)
  has_size_arrayT
  has_size_slicesT
  abduct_step_type_cast_loc_val_no_own
  abduct_step_type_cast_slices_project_slice'
  abduct_step_type_cast_slices_array
  : typeclass_instances.




(* print-augmented abduction lemmas *)
Section abduction_lemmas.
  Context {Σ: gFunctors}.
  Implicit Types (Δ: qenvs Σ).

  (* pure *)
  Lemma abduct_pure_prove Δ P R φ:
    Δ ⊨ Solve φ →
    debug_print_solve φ →
    abduct Δ P R →
    abduct Δ (abduct_pure φ P) R.
  Proof.
    rewrite /abduct_pure /Solve.
    intros ??. by eapply abduct_prove_pure.
  Qed.

  Lemma abduct_pure_missing Δ Δ' P R (φ: Prop):
    qenvs_add_pure_assertion Δ φ Δ' →
    debug_print_missing φ →
    abduct Δ' P R →
    abduct Δ (abduct_pure φ P) (⌜φ⌝ ∗ R).
  Proof.
    rewrite /abduct_pure. intros ??. by eapply abduct_assert_pure.
  Qed.

  Lemma abduct_pure_simplify_pure Δ (φ ψ: Prop) (P: iProp Σ) R:
    Δ ⊨ simplify_pure true φ ψ →
    debug_print_simplify_pure true φ ψ →
    abduct Δ (abduct_pure ψ P) R →
    abduct Δ (abduct_pure φ P) R.
  Proof.
    rewrite /abduct_pure.
    intros Hent ? Habd. apply: abduct_qenvs_entails=> Hsimpl.
    iIntros "Hctx". iDestruct (Habd with "Hctx") as "[%Hφ Hx]".
    iFrame. iPureIntro. by eapply Hsimpl.
  Qed.

  Lemma abduct_class_prove Δ (φ: Prop) P R:
    Δ ⊨ φ →
    abduct Δ P R →
    debug_print_solve φ →
    abduct Δ (abduct_class φ P) R.
  Proof. intros ???. by eapply abduct_pure_prove. Qed.

  Global Instance abduct_step_pure_prove φ Δ P R :
    Δ ⊨ Solve φ →
    debug_print_solve φ →
    AbductStep (abduct Δ P R)
      Δ (abduct_pure φ P) R | 2.
  Proof. intros ???. by apply abduct_pure_prove. Qed.

  Global Instance abduct_step_pure_missing (φ: Prop) Δ Δ' P R :
    qenvs_add_pure_assertion Δ φ Δ' →
    debug_print_missing φ →
    AbductStep (abduct Δ' P R)
      Δ (abduct_pure φ P) (⌜φ⌝ ∗ R) | 10.
  Proof. intros ???. by eapply abduct_pure_missing. Qed.

  Global Instance abduct_step_pure_simplify_pure Δ (φ ψ: Prop) (P: iProp Σ) R :
    _ → _ → AbductStep _ _ _ _ | 90 := abduct_pure_simplify_pure Δ φ ψ  P R.

  Global Instance abduct_step_class_pure_inst Δ R (φ: Prop) P:
    Δ ⊨ φ →
    debug_print_solve φ →
    AbductStep (abduct Δ P R) Δ (abduct_class φ P) R | 1.
  Proof. rewrite /abduct_class. intros ???. eapply abduct_step_pure_prove; done. Qed.

  (* proofmode *)
  Global Instance abduct_step_case_left Δ P1 P2 (φ: dProp) (R: iProp Σ):
    Δ ⊨ Solve φ →
    debug_print_solve φ →
    debug_print_decided φ →
    AbductStep (abduct Δ P1 R) Δ (case φ P1 P2) R | 1.
  Proof. rewrite /Solve. intros ????. eapply abduct_case_true; eauto. Qed.

  Global Instance abduct_step_case_right Δ P1 P2 (φ: dProp) (R: iProp Σ):
    Δ ⊨ Solve (¬ φ) →
    debug_print_solve (¬ φ) →
    debug_print_decided φ →
    AbductStep (abduct Δ P2 R) Δ (case φ P1 P2) R | 2.
  Proof. rewrite /Solve. intros ????. eapply abduct_case_false; eauto. Qed.

  (* precond *)
  Lemma abduct_type_precond_simplify_pure Δ X pm φ ψ ξ (P: X → iProp Σ) E M B C R:
    Δ ⊨ (∀! x, simplify_pure true (φ x) (ψ x)) →
    (∀ x, debug_print_simplify_pure true (φ x) (ψ x)) →
    (∀! x, Simpl (ψ x) (ξ x)) →
    abduct Δ (type_precond X pm (λ x, ⌜ξ x⌝ ∗ P x)%I E M B C) R →
    abduct Δ (type_precond X pm (λ x, ⌜φ x⌝ ∗ P x)%I E M B C) R.
  Proof.
    rewrite /evar_forall. intros Hif ? Heq ?.
    apply: abduct_qenvs_entails => {}Hif.
    assert (∀ x, simplify_pure true (φ x) (ξ x)).
    { intros x. rewrite /simplify_pure. rewrite -Heq. apply Hif. }
    rewrite -type_precond_impl //.
  Qed.

  Global Instance abduct_step_type_precond_simplify_pure Δ X pm φ ψ ξ (P: X → iProp Σ) E M B C R:
    Δ ⊨ (∀! x, simplify_pure true (φ x) (ψ x)) →
    (∀ x, debug_print_simplify_pure true (φ x) (ψ x)) →
    (∀! x, Simpl (ψ x) (ξ x)) →
    AbductStep (abduct Δ (type_precond X pm (λ x, ⌜ξ x⌝ ∗ P x)%I E M B C) R)
      Δ (type_precond X pm (λ x, ⌜φ x⌝ ∗ P x)%I E M B C) R | 2.
  Proof. intros ????; by eapply abduct_type_precond_simplify_pure. Qed.

  (* simplification *)
  Global Instance simplify_prop_simplify_pure b φ ψ:
    simplify_pure b φ ψ →
    debug_print_simplify_pure b φ ψ →
    simplify_prop b (⌜φ⌝: iProp Σ)%I ⌜ψ⌝ | 2.
  Proof.
    rewrite /simplify_pure /once_tc /simplify_prop; destruct b; rewrite -!simplify_ent_iff.
    all: iIntros (Himpl) "%Hpure"; iPureIntro; naive_solver.
  Qed.

  (* existentials *)
  Global Instance elim_this_existental_inst {X: Type} (x: X) (G: X → iProp Σ) R:
    resolve_this_existential x (G x) R →
    debug_print ("existential instantiated x :=", x) →
    elim_this_existential (∃ x: X, G x) R | 100.
  Proof.
    rewrite /resolve_this_existential /elim_this_existential.
    rewrite simplify_eq /simplify_def. iIntros (Hent _) "R".
    iExists _. by iApply Hent.
  Qed.

  Global Instance elim_this_universal_inst {X: Type} y (G H: X → iProp Σ) :
    (∀! x, resolve_this_universal x y (G x) (H x)) →
    debug_print ("universal instantiated x :=", y) →
    elim_this_universal (∀ x: X, G x) (H y) | 100.
  Proof.
    rewrite /resolve_this_universal /elim_this_universal.
    rewrite simplify_eq /simplify_def. iIntros (Hent _) "H".
    iIntros (x). iApply Hent. iIntros "->". done.
  Qed.


  (* typing  *)
  Context `{!refinedcG Σ}.

  Global Instance has_size_arrayT {X} (T: X → type Σ) (sz: Z) (xs: list X) n:
    sequence_classes [
      (∀ x, T x `has_size` n);
      Solve (n = sz ∧ 0 ≤ sz);
      debug_print_solve ( n = sz ∧ 0 ≤ sz)
      ] →
    (arrayT T sz xs `has_size` (n * length xs)).
  Proof.
    rewrite /sequence_classes !Forall_cons /= /Solve.
    intros (Hsz & [Hle Heq] & _). subst.
    rewrite Z.mul_comm. eapply arrayT_has_size; eauto.
  Qed.

  Global Instance has_size_slicesT len Ts n:
    Trivial (len = n) →
    TCForall (λ s, TCAnd (ty_has_size s.2 s.1.2) (Solve (0 ≤ s.1.2))) Ts →
    Solve (0 ≤ n) →
    debug_print_solve (0 ≤ n) →
    slicesT len Ts `has_size` n.
  Proof.
    rewrite /Trivial/Solve => -> /TCForall_Forall Hall ??.
    apply slicesT_has_size; [|done].
    apply: Forall_impl; [done|]. by move => ? [??].
  Qed.


  Global Instance abduct_step_type_cast_loc_val_no_own Δ X lv pm v n1 n2 Φ R:
    Δ ⊨ Solve (n1 = n2) →
    debug_print_solve (n1 = n2) →
    AbductStep (abduct Δ (Φ (λ x, emp)%I) R)
      Δ (type_cast X pm lv (valueT v n1) (λ x: X, valueT v n2) Φ) R | 1.
  Proof. intros ???. by eapply abduct_type_cast_loc_val_no_own. Qed.

  Lemma abduct_type_cast_slices_project_slice' Δ X pm l len Ls n m A B Φ R:
    (∀ x, B x `has_size` n) →
    Solve (n = m) →
    debug_print_solve (n = m) →
    abduct Δ (type_project_slice l 0 n ((0, m, A) :: Ls) (λ i li A L R, ⌜li = n⌝ ∗
      abd_assume CTX (l ◁ₗ slicesT len (L ++ (0, n, placeT (l +ₗ 0)) :: R) ∗ (l +ₗ 0) ◁ₗ A) (Φ (λ x, (l +ₗ 0) ◁ₗ B x))))%I R →
    abduct Δ (type_cast X pm (LOC l) (slicesT len ((0, m, A) :: Ls)) B Φ) R.
  Proof. intros ??. rewrite -type_cast_slices_project_slice'//. Qed.

  Global Instance abduct_step_type_cast_slices_project_slice' Δ X pm l len Ls n m A B Φ R H1 H2 H3:
    AbductStep _ _ _ _ | 5 := abduct_type_cast_slices_project_slice' Δ X pm l len Ls n m A B Φ R H1 H2 H3.

  Lemma abduct_type_cast_slices_array Δ {X Y} pm l n m k As Φ (xs: X → list Y) P sz B R:
    sequence_classes [
        (∀ x, B x `has_size` k);
        Δ ⊨ Solve (0 ≤ k);
        Simpl (length As) m;
        Simpl ([∗ list] p ∈ As, (l +ₗ p.1.1) ◁ₗ p.2)%I P;
        debug_print_solve (0 ≤ k)
    ] →
    abduct Δ (abd_assume CTX P $ Φ (λ x, existsN m (λ Xs, abd_simpl (concat Xs) (λ ys, ⌜xs x = ys⌝ ∗ ⌜sz x = k⌝ ∗ ⌜Forall2 (λ zs p, p.1.2 = (length zs * k)%Z) Xs As⌝ ∗ abd_simpl ([∗ list] p;ys ∈ As;Xs, (l +ₗ p.1.1) ◁ₗ arrayT B (sz x) ys) (λ P, P))))%I) R →
    abduct Δ (type_cast X pm (LOC l) (slicesT n As) (λ x, arrayT B (sz x) (xs x))%RT Φ) R.
  Proof.
    rewrite /sequence_classes !Forall_cons /id Forall_nil. intros (?& ?& <- & <- & _) Habd.
    apply: abduct_qenvs_entails. rewrite /Solve. intros Hle.
    rewrite -type_cast_slices_array //.
  Qed.

  Global Instance abduct_step_type_cast_slices_array Δ {X Y} pm l n m k As Φ (xs: X → list Y) P sz B R:
    sequence_classes [
        (∀ x, B x `has_size` k);
        Δ ⊨ Solve (0 ≤ k);
        Simpl (length As) m;
        Simpl ([∗ list] p ∈ As, (l +ₗ p.1.1) ◁ₗ p.2)%I P;
        debug_print_solve (0 ≤ k)
    ] →
    AbductStep (abduct Δ (abd_assume CTX P $ Φ (λ x, existsN m (λ Xs, abd_simpl (concat Xs) (λ ys, ⌜xs x = ys⌝ ∗ ⌜sz x = k⌝ ∗ ⌜Forall2 (λ zs p, p.1.2 = (length zs * k)%Z) Xs As⌝ ∗ abd_simpl ([∗ list] p;ys ∈ As;Xs, (l +ₗ p.1.1) ◁ₗ arrayT B (sz x) ys) (λ P, P))))%I) R)
      Δ (type_cast X pm (LOC l) (slicesT n As) (λ x, arrayT B (sz x) (xs x))%RT Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_cast_slices_array. Qed.


End abduction_lemmas.




(* inference *)
Section inference_lemmas.
  Context `{!refinedcG Σ}.

  Lemma abduct_type_function_start_count Δ (P: iProp Σ) Q R n m:
    clear_universals (abduct Δ P Q) →
    simplify Q R →
    sequence_classes [print R; prop_size R n; Simpl n m; print ("size", m)] →
    abduct Δ (type_function_start P) R.
  Proof.
    rewrite /clear_universals /type_function_start. intros ???. by eapply abduct_simplify.
  Qed.

End inference_lemmas.

(* inference *)
Ltac inferF ::=
  match goal with
  | |- fn_well_typed ?Δ ?fn =>
    eexists _; apply: fn_implements_pt_intro; intros ws ret;
    (* abduct [type_function] until [type_function_start], then introduce *)
    abduct; skip_stop; eapply abduct_type_function_start_count; [abduct_step| |];
    [(* abduct*) | (* simplify *) | (* print*) ]
  end.

Ltac print_loud ::=
  match goal with
  | |- print ?x => idtac x; apply do_print
  | |- sequence_classes _ => apply _
  end.
