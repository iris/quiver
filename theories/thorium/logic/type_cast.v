
From quiver.thorium.types Require Export types base_types pointers combinators functions structs refinements bytes.
From quiver.argon.base Require Import classes judgments.
From quiver.thorium.logic Require Export syntax assume.
From quiver.argon.base Require Export precond.

Section type_cast.
  Context `{!refinedcG Σ}.

  (* type casting *)
  Definition type_progress_type (X: Type) (lv: loc_or_val) (A: type Σ) (B: X → type Σ) (Φ : progress → iProp Σ) : iProp Σ :=
    ∃ p, Φ p.

  Lemma type_progress_type_intro (X: Type) (lv: loc_or_val) (A: type Σ) (B: X → type Σ) (Φ : progress → iProp Σ) p :
    Φ p ⊢ type_progress_type X lv A B Φ.
  Proof. iIntros "HΦ". iExists p. iFrame. Qed.

  Lemma type_progress_type_guaranteed (X: Type) (lv: loc_or_val) (A: type Σ) (B: X → type Σ) (Φ : progress → iProp Σ) :
    Φ GUARANTEED ⊢ type_progress_type X lv A B Φ.
  Proof. eapply type_progress_type_intro. Qed.

  Lemma type_progress_type_blocked (X: Type) (lv: loc_or_val) (A: type Σ) (B: X → type Σ) (Φ : progress → iProp Σ) :
    Φ BLOCKED ⊢ type_progress_type X lv A B Φ.
  Proof. eapply type_progress_type_intro. Qed.

  Lemma type_progress_type_maybe (X: Type) (lv: loc_or_val) n (A: type Σ) (B: X → type Σ) (Φ : progress → iProp Σ) :
    Φ (MAYBE n) ⊢ type_progress_type X lv A B Φ.
  Proof. eapply type_progress_type_intro. Qed.

  Lemma type_progress_type_impossible (X: Type) (lv: loc_or_val) (A: type Σ) (B: X → type Σ) (Φ : progress → iProp Σ) :
    Φ IMPOSSIBLE ⊢ type_progress_type X lv A B Φ.
  Proof. eapply type_progress_type_intro. Qed.



  (** In [type_cast], the continuation [Φ] takes the remanining
     part of the precondition that needs to be true for the type
     cast to succeed (e.g., [it1 = it2]). *)
  Definition type_cast (X: Type) (pm: precond_mode) (lv: loc_or_val) (A: type Σ) (B: X → type Σ) (Φ: (X → iProp Σ) → iProp Σ) : iProp Σ :=
    lv ◁ₓ A -∗ ∃ Q, (∀ x, Q x -∗ lv ◁ₓ B x) ∗ Φ Q.

  Definition type_extract_prefix (l: loc) (A: type Σ) (n: Z) (Φ: loc → val → iProp Σ) : iProp Σ :=
    l ◁ₗ A -∗ ∃ v, l ◁ₗ valueT v n ∗ Φ l v.

  Definition type_extract_range (l: loc) (A: type Σ) (i li: Z) (Φ: loc → val → iProp Σ) : iProp Σ :=
    l ◁ₗ A -∗ ∃ v, (l +ₗ i) ◁ₗ valueT v li ∗ Φ (l +ₗ i) v.

  Definition type_pre_type (x: loc_or_val) pm (T: Type) (A: T → type Σ) (Φ: T → iProp Σ) : iProp Σ :=
    type_pre_post T pm (λ t, x ◁ₓ A t) Φ.

  Definition type_unify (x: loc_or_val) pm (A: type Σ) (T: Type) (B: T → type Σ) (Φ: T → iProp Σ) : iProp Σ :=
    x ◁ₓ A -∗ type_pre_type x pm T B Φ.


  Lemma type_cast_moved_subtype X pm lv A A' B Φ:
    A ⊑ A' →
    type_cast X pm lv A' B Φ ⊢
    type_cast X pm lv A B Φ.
  Proof.
    intros Hsub. rewrite /type_cast.
    destruct lv; simpl; rewrite Hsub //.
  Qed.

  Definition type_recover (pm: precond_mode) (lv: loc_or_val) (A: type Σ) (P: iProp Σ) : iProp Σ :=
    match lv with
    | VAL v => match pm with RECOVER => v ◁ᵥ A -∗ P | CONSUME => P end
    | LOC l => P
    end.

  Definition type_from_zeros (X : Type) (n : Z) (A : X → type Σ) (Φ : (X → iProp Σ) → iProp Σ)
    : iProp Σ :=
    ∃ P, ⌜∀ x, Persistent (P x)⌝ ∗ □ (∀ lv x, P x -∗ lv ◁ₓ zerosT n -∗ lv ◁ₓ A x) ∗ Φ P.

  Definition type_from_zeros_all (X : Type) (ns : list Z)
    (As : X → list (type Σ)) (Φ : (X → iProp Σ) → iProp Σ)
    : iProp Σ :=
    ∃ Ps Q, ⌜∀ x, length ns = length (As x)⌝ ∗ ⌜∀ x, Persistent (Q x)⌝ ∗
       □ ([∗ list] i↦n;P∈ns;Ps, (∀ lv x, P x -∗ lv ◁ₓ zerosT n -∗ lv ◁ₓ (As x !!! i))) ∗
       □ (∀ x, Q x -∗ [∗ list] P∈Ps, P x) ∗ Φ Q.


  (* type recover *)
  Lemma type_recover_copy pm (lv: loc_or_val) (A: type Σ) (P: iProp Σ) :
    Copy A →
    type_recover pm lv A P -∗ lv ◁ₓ A -∗ P ∗ lv ◁ₓ A.
  Proof.
    rewrite /type_recover. iIntros (Hcp) "Hcont Hlv".
    destruct lv; simpl; first by iFrame.
    iDestruct "Hlv" as "#Hlv".
    destruct pm; simpl.
    - by iFrame.
    - iFrame "Hlv". by iApply "Hcont".
  Qed.

  Lemma type_recover_moved pm (lv: loc_or_val) (A: type Σ) (P: iProp Σ) :
    type_recover pm lv (movedT A) P -∗ lv ◁ₓ A -∗ P ∗ lv ◁ₓ A.
  Proof.
    rewrite /type_recover. iIntros "Hcont Hlv".
    destruct lv; simpl; first by iFrame.
    destruct pm; simpl.
    - by iFrame.
    - iDestruct (vtr_move with "Hlv") as "#Hmv". iFrame. by iApply "Hcont".
  Qed.

  (* type casting *)

  (* void casting *)
  Lemma type_cast_void pm X lv Φ:
    type_recover pm lv voidT (Φ (λ x, True)) ⊢ type_cast X pm lv voidT (λ x, voidT) Φ.
  Proof.
    rewrite /type_cast. iIntros "HΦ Hx".
    iDestruct (type_recover_copy with "HΦ Hx") as "[HΦ Hx]".
    iExists _. iFrame.
    iIntros (x) "Hx"; simpl. iFrame.
  Qed.

  (* boolean casting *)
  Lemma type_cast_bool X pm lv it1 it2 (b: bool) (b': X → bool) Φ :
    type_recover pm lv (boolT it1 b) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜b' x = b⌝)) ⊢ type_cast X pm lv (boolT it1 b) (λ x, boolT (it2 x) (b' x)) Φ.
  Proof.
    rewrite /type_cast. iIntros "HΦ Hx".
    iDestruct (type_recover_copy with "HΦ Hx") as "[HΦ Hx]".
    iExists _. iFrame.
    iIntros (x) "[<- <-]". iFrame.
  Qed.

  (* integer casting *)
  Lemma type_cast_int (X: Type) pm lv it1 it2 n1 n2 Φ:
    type_recover pm lv (intT it1 n1) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜n2 x = n1⌝)) ⊢ type_cast X pm lv (intT it1 n1) (λ x, intT (it2 x) (n2 x)) Φ.
  Proof.
    rewrite /type_cast. iIntros "HΦ Hx".
    iDestruct (type_recover_copy with "HΦ Hx") as "[HΦ Hx]".
    iExists _. iFrame.
    iIntros (x) "[<- <-]". iFrame.
  Qed.

  Lemma type_cast_bool_int (X: Type) pm lv it1 it2 (b: bool) n Φ:
    type_recover pm lv (boolT it1 b) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜n x = bool_to_Z b⌝)) ⊢ type_cast X pm lv (boolT it1 b) (λ x, intT (it2 x) (n x)) Φ.
  Proof.
    rewrite /type_cast. iIntros "Hcont Hlv".
    iDestruct (type_recover_copy with "Hcont Hlv") as "[HΦ Hx]".
    iExists _. iFrame.
    iIntros (x) "[%Heq1 %Heq2]". rewrite Heq2 Heq1.
    rewrite boolT_eq /boolT_def //.
  Qed.

  Definition Z_to_bool (z : Z) : option bool :=
    match z with
    | Z0 => Some false
    | Zpos xH => Some true
    | _ => None
    end.
  Lemma Z_to_of_bool b : Z_to_bool (bool_to_Z b) = Some b.
  Proof. destruct b; done. Qed.
  Lemma Z_of_to_bool b z : Z_to_bool z = Some b → z = bool_to_Z b.
  Proof.
    destruct z as [ | [] | ]; simpl; try done.
    all: injection 1 as [= <-]; done.
  Qed.

  Lemma type_cast_int_bool (X: Type) pm lv it1 it2 b n Φ:
    type_recover pm lv (intT it1 n) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜Some (b x) = Z_to_bool n⌝)) ⊢ type_cast X pm lv (intT it1 n) (λ x, boolT (it2 x) (b x)) Φ.
  Proof.
    rewrite /type_cast. iIntros "Hcont Hlv".
    iDestruct (type_recover_copy with "Hcont Hlv") as "[HΦ Hx]".
    iExists _. iFrame.
    iIntros (x) "[%Heq1 %Heq2]". rewrite Heq1.
    rewrite boolT_eq /boolT_def //.
    erewrite <-Z_of_to_bool; done.
  Qed.

  (* ZEROS *)
  Lemma type_cast_from_zeros X pm lv A Φ n:
    (* TODO: can we get rid of the manual unfolding of abd_evars here? *)
    type_recover pm lv (zerosT n) (type_from_zeros X n A (λ P, ∃ Q, □ (∀ x, Q x -∗ P x) ∗ Φ Q)) ⊢
    type_cast X pm lv (zerosT n) A Φ.
  Proof.
    rewrite /type_from_zeros/type_cast. iIntros "Hr Hz".
    iDestruct (type_recover_copy with "Hr Hz") as "[[%P [% [#HA [% [HQ HΦ]]]]] Hz]".
    iExists _. iFrame. iIntros (?) "HP".
    iDestruct ("HQ" with "[$]") as "HP". iApply ("HA" with "HP Hz").
  Qed.

  Lemma type_from_zeros_all_cons X Ts Φ n ns T :
    type_from_zeros X n T (λ P, type_from_zeros_all X ns Ts (λ Ps, Φ (λ x, P x ∗ Ps x)))
    ⊢ type_from_zeros_all X (n::ns) (λ x, (T x :: Ts x)) Φ.
  Proof.
    rewrite /type_from_zeros/type_from_zeros_all/=.
    iIntros "(%P&%&#?&%Ps&%Q&%Hlen&%&#HPs&#HQ&?)".
    iExists (P::Ps), _. iFrame. repeat iSplit; try iPureIntro.
    - move => ?. by rewrite -Hlen.
    - apply _.
    - iModIntro. rewrite big_sepL2_cons /=. iFrame "#".
    - iIntros "!>" (?) "[??]". simpl. iFrame. by iApply "HQ".
  Qed.

  Lemma type_from_zeros_all_nil X Φ :
    Φ (λ x, True)
    ⊢ type_from_zeros_all X [] (λ x, []) Φ.
  Proof.
    rewrite /type_from_zeros_all/=.
    iIntros "HΦ". iExists [], _. iFrame.
    repeat iSplit; try iPureIntro.
    - done.
    - apply _.
    - iModIntro. done.
    - iIntros "!>" (?) "?". done.
  Qed.

  (* ANY *)
  Lemma type_cast_any_tgt pm lv X A (n: X → Z) Φ:
    (lv ◁ₓ A -∗ Φ (λ x, ∃ v, lv ◁ₓ valueT v (n x)))
  ⊢ type_cast X pm lv A (λ x, anyT (n x)) Φ.
  Proof.
    rewrite /type_cast. iIntros "Hcont Hl".
    iDestruct ("Hcont" with "Hl") as "Hcont".
    iExists _. iFrame. iIntros (x) "[%v Hl]".
    iApply xtr_cast_to_anyT; last done.
    by apply valueT_has_size.
  Qed.

  (* owned pointers and optionals *)
  Lemma type_cast_own (X: Type) pm lv (p: loc) (q: X → loc) (A : type Σ) (B: X → type Σ) Φ:
    (p ◁ₗ A -∗ type_recover pm lv (valueT p ptr_size) (Φ (λ x, p ◁ₗ B x ∗ ⌜p = q x⌝))) ⊢ type_cast X pm lv (ownT p A) (λ x, ownT (q x) (B x)) Φ.
  Proof.
    rewrite /type_cast. iIntros "Hcont Hlv". rewrite -value_own_xtr.
    iDestruct "Hlv" as "[Hlv Hp]". iDestruct ("Hcont" with "Hp") as "HΦ".
    iDestruct (type_recover_copy with "HΦ Hlv") as "[HΦ Hx]".
    iExists _. iFrame. iIntros (x) "[Hq ->]". rewrite -value_own_xtr. iFrame.
  Qed.

  Lemma type_cast_null X pm lv Φ:
    type_recover pm lv nullT (Φ (λ _, emp)) ⊢ type_cast X pm lv nullT (λ x, nullT) Φ.
  Proof.
    rewrite /type_cast. iIntros "Hcont Hlv".
    iDestruct (type_recover_copy with "Hcont Hlv") as "[Hcont Hx]".
    iExists _. iFrame. by iIntros (x) "_".
  Qed.

  Lemma type_cast_own_optional (X: Type) pm lv (p: loc) (A: type Σ) (φ: X → dProp) (B: X → type Σ) Φ :
    (lv ◁ₓ ownT p A -∗ Φ (λ x, ⌜φ x⌝ ∗ lv ◁ₓ B x))%I
  ⊢ type_cast X pm lv (ownT p A) (λ x, optionalT (φ x) (B x)) Φ.
  Proof.
    rewrite /type_cast.
    iIntros "Hcont Hlv" . iExists _. iDestruct ("Hcont" with "Hlv") as "$".
    iIntros (x) "[%Hpure Hv]". rewrite /optionalT /case.
    destruct decide; by naive_solver.
  Qed.


  Lemma type_cast_optional_own (X: Type) pm lv (φ: dProp) (B: type Σ) (p: X → loc) (A: X → type Σ) Φ :
    (⌜φ⌝ ∗ (abd_assume CTX (lv ◁ₓ B) (Φ (λ x, lv ◁ₓ ownT (p x) (A x)))))%I
  ⊢ type_cast X pm lv (optionalT φ B) (λ x, ownT (p x) (A x)) Φ.
  Proof.
    rewrite /type_cast /abd_assume.
    iIntros "[%Hφ Hcont] Hlv". rewrite /optionalT /case. destruct decide; last by naive_solver.
    iExists _. iDestruct ("Hcont" with "Hlv") as "$".
    iIntros (x) "Hv". iFrame.
  Qed.


  Lemma type_cast_null_optional X pm lv (φ: X → dProp) (B: X → type Σ) Φ :
    type_recover pm lv nullT (Φ (λ x, ⌜¬ φ x⌝)) ⊢ type_cast X pm lv nullT (λ x, optionalT (φ x) (B x)) Φ.
  Proof.
    rewrite /type_cast. iIntros "Hcont Hlv".
    iDestruct (type_recover_copy with "Hcont Hlv") as "[Hcont Hx]".
    iExists _. iFrame. iIntros (x) "%Hpure".
    rewrite /optionalT /case. destruct decide; naive_solver.
  Qed.

  Lemma type_cast_optional_null X pm lv (φ: dProp) (B: type Σ) Φ :
    ⌜¬ φ⌝ ∗ type_recover pm lv nullT (Φ (λ _, emp)) ⊢ type_cast X pm lv (optionalT φ B) (λ x, nullT) Φ.
  Proof.
    rewrite /type_cast. iIntros "[%Hφ Hcont] Hlv".
    rewrite /optionalT /case. destruct decide; first naive_solver.
    iDestruct (type_recover_copy with "Hcont Hlv") as "[Hcont Hx]".
    iExists _. iFrame. by iIntros (x) "Hv".
  Qed.

  Lemma type_cast_optional X pm lv φ A (ψ: X → dProp) (B: X → type Σ) Φ :
    type_recover pm lv (movedT (optionalT φ A)) (Φ (λ x, ⌜dprop_iff φ (ψ x)⌝ ∗ ⌜A `is_ty` (B x)⌝)) ⊢ type_cast X pm lv (optionalT φ A) (λ x, optionalT (ψ x) (B x)) Φ.
  Proof.
    rewrite /type_cast. iIntros "Hcont Hlv".
    iDestruct (type_recover_moved with "Hcont Hlv") as "[Hcont Hx]".
    iExists _.
    iFrame. iIntros (x) "[%Hpure %Hty]".
    setoid_rewrite Hpure. by setoid_rewrite Hty.
  Qed.

  Lemma type_cast_optional_case pm (lv: loc_or_val) X (A: type Σ) φ ψ (B: X → type Σ) Φ :
    case φ (type_cast X pm lv A (λ x, optionalT (ψ x) (B x)) Φ) (type_cast X pm lv nullT (λ x, optionalT (ψ x) (B x)) Φ) ⊢
    type_cast X pm lv (optionalT φ A) (λ x, optionalT (ψ x) (B x)) Φ.
  Proof.
    rewrite {3}/optionalT /case. destruct decide; done.
  Qed.

  Lemma type_cast_own_value pm (lv: loc_or_val) X (p: loc) (A: type Σ) (q: X → loc) Φ:
    (abd_assume CTX (p ◁ₗ A) (type_recover pm lv (valueT p ptr_size) (Φ (λ x, ⌜p = q x⌝)))) ⊢ type_cast X pm lv (ownT p A) (λ x, valueT (q x) ptr_size) Φ.
  Proof.
    rewrite /type_cast /abd_assume. iIntros "Hcont Hlv". rewrite -value_own_xtr.
    iDestruct "Hlv" as "[Hlv Hp]".
    iDestruct ("Hcont" with "Hp") as "HΦ".
    iDestruct (type_recover_copy with "HΦ Hlv") as "[HΦ Hlv]".
    iExists _. iFrame. iIntros (x) "->". iFrame.
  Qed.


  Lemma type_cast_opt_lookup X pm lv w A Φ:
    type_recover pm lv (optT w) (Φ (λ x, w ◁ᵥ A x)) ⊢ type_cast X pm lv (optT w) A Φ.
  Proof.
    rewrite /type_cast. iIntros "HΦ Hlv".
    iDestruct (type_recover_copy with "HΦ Hlv") as "[HΦ Hlv]".
    iExists _. iFrame. iIntros (x) "Hw".
    rewrite /optT xtr_sepT_iff. iDestruct "Hlv" as "[Hlv _]".
    iApply (valueT_elim_xtr with "Hlv Hw").
  Qed.

  Lemma type_cast_opt_opt X pm lv v w Φ:
    type_recover pm lv (optT w) (Φ (λ x, ⌜v x = w⌝)) ⊢ type_cast X pm lv (optT w) (λ x, optT (v x)) Φ.
  Proof.
    rewrite /type_cast. iIntros "HΦ Hlv".
    iDestruct (type_recover_copy with "HΦ Hlv") as "[HΦ Hlv]".
    iExists _. iFrame. iIntros (x) "->". done.
  Qed.

  Lemma type_cast_loc_opt X pm l v A Φ:
    type_move l A ptr_size (λ w, Φ (λ x, w ◁ᵥ optT (v x))) ⊢ type_cast X pm (LOC l) A (λ x, optT (v x)) Φ.
  Proof.
    rewrite /type_move /type_cast. iIntros "Hcont Hl".
    iDestruct ("Hcont" with "Hl") as "(%w & Hl & HΦ)".
    iExists _. iFrame. iIntros (x) "Hw".
    iApply (valueT_elim_xtr with "[Hl] Hw"); simpl; iFrame.
  Qed.

  Lemma type_cast_null_opt X pm v w Φ:
    abd_assume CTX (v ◁ᵥ nullT) (Φ (λ x, ⌜w x = v⌝)) ⊢ type_cast X pm (VAL v) nullT (λ x, optT (w x)) Φ.
  Proof.
    rewrite /type_cast /abd_assume. iIntros "HΦ #Hl".
    iSpecialize ("HΦ" with "Hl").
    iExists _. iFrame. iIntros (x) "->".
    by iApply null_opt.
  Qed.

  Lemma type_cast_own_opt X pm v w p A Φ:
    ⌜NonNull A⌝ ∗ abd_assume CTX (v ◁ᵥ ownT p A) (Φ (λ x, ⌜w x = v⌝)) ⊢ type_cast X pm (VAL v) (ownT p A) (λ x, optT (w x)) Φ.
  Proof.
    rewrite /type_cast. iIntros "[%Hnonnull HΦ] Hl".
    iDestruct (non_null_opt with "[$Hl]") as "#Hv".
    { iPureIntro. apply _. }
    rewrite /abd_assume. iSpecialize ("HΦ" with "Hl").
    iExists _. iFrame. by iIntros (x) "->".
  Qed.

  Lemma type_cast_optional_opt X pm v w φ A Φ:
    ⌜NonNullVal A⌝ ∗ abd_assume CTX (v ◁ᵥ optionalT φ A) (Φ (λ x, ⌜w x = v⌝)) ⊢ type_cast X pm (VAL v) (optionalT φ A) (λ x, optT (w x)) Φ.
  Proof.
    rewrite /optionalT /case. destruct decide; last first.
    - rewrite -type_cast_null_opt. iIntros "[_ $]".
    - rewrite /type_cast /abd_assume.
      iIntros "[%Hnonnull HΦ] Hl".
      iDestruct (non_null_opt with "[$Hl]") as "#Hv".
      { iPureIntro. apply _. }
      iSpecialize ("HΦ" with "Hl").
      iExists _. iFrame. by iIntros (x) "->".
  Qed.


  (* type casts *)
  Lemma type_cast_value_val X pm (v: val) A (w: X → val) (n: X → Z) Φ:
    type_size A (λ m, abd_assume CTX (v ◁ᵥ A) (Φ (λ x, ⌜w x = v⌝ ∗ ⌜n x = m⌝)))
  ⊢ type_cast X pm (VAL v) A (λ x, valueT (w x) (n x)) Φ.
  Proof.
    rewrite /type_size /type_cast. iIntros "[%m [%Hsz Hcont]] Hv".
    rewrite /abd_assume.
    iDestruct (ty_size_eq with "Hv") as "%Hlen"; first done.
    iDestruct (vtr_valueT_intro with "[//]") as "Hw".
    iDestruct ("Hcont" with "Hv") as "HΦ". iExists _. iFrame.
    iIntros (x) "[-> ->]". by iApply "Hw".
  Qed.

  Lemma type_cast_value_loc_sized (X: Type) pm (l: loc) A (w: X → val) n m Φ:
    ⌜A `has_size` n⌝ ∗ ⌜n = m⌝ ∗ (∀ v, abd_assume CTX (v ◁ᵥ A) (Φ (λ x, ⌜w x = v⌝))) ⊢ type_cast X pm (LOC l) A (λ x, valueT (w x) m) Φ.
  Proof.
    rewrite /type_cast /abd_assume. iIntros "[%Hsz [%Heq Hcont]] Hlv"; subst.
    iDestruct (ty_move with "Hlv") as "[%v [Hl Hv]]"; first done.
    iDestruct (ty_size_eq with "Hv") as "%Hlen"; first done.
    iDestruct (valueT_intro with "Hl") as "Hl"; first done.
    iDestruct ("Hcont" with "Hv") as "Hcont".
    iExists _. iFrame. iIntros (x) "%Heq". subst. by iFrame.
  Qed.

  Lemma type_cast_value_loc X pm l A (v: X → val) n Φ:
    type_extract_prefix l A n (λ r w, Φ (λ x, ⌜v x = w⌝)) ⊢ type_cast X pm (LOC l) A (λ x, valueT (v x) n) Φ.
  Proof.
    rewrite /type_extract_prefix /type_cast. iIntros "HΦ Hl".
    iDestruct ("HΦ" with "Hl") as "(%w & Hw & HΦ)". iExists _. iFrame.
    by iIntros (x) "->".
  Qed.

  Lemma type_cast_value_loc_maybe X pm (l: loc) A (w: X → val) n m Φ:
    A `has_size` n →
    (∀ v, abd_assume CTX (v ◁ᵥ A) (Φ (λ x, ⌜w x = v⌝ ∗ ⌜m x = n⌝)))
    ⊢ type_cast X pm (LOC l) A (λ x, valueT (w x) (m x)) Φ.
  Proof.
    intros Hsz. rewrite /type_cast /abd_assume /=. iIntros "Hcont Hl".
    iDestruct (ty_move with "Hl") as "(%v & Hl & Hv)"; first done.
    iDestruct (ty_size_eq with "Hv") as "%Hsz2"; first done.
    iDestruct ("Hcont" with "Hv") as "Hcont". iExists _. iFrame.
    iIntros (x) "[-> ->]". by iApply (valueT_intro with "Hl").
  Qed.

  Lemma type_cast_value_loc_maybe_bounds X pm (l: loc) A (w: X → val) n m Φ:
    A `has_bounds` n →
    type_move l A n (λ v, Φ (λ x, ⌜m x = n⌝ ∗ ⌜w x = v⌝))
    ⊢ type_cast X pm (LOC l) A (λ x, valueT (w x) (m x)) Φ.
  Proof.
    intros Hsz. rewrite /type_cast /type_move /=. iIntros "Hcont Hl".
    iDestruct ("Hcont" with "Hl") as "(%v & Hl & Hp)".
    iExists _. iFrame.
    iIntros (x) "[-> ->]".
    done.
  Qed.

  Lemma type_cast_value_src X pm lv v n B Φ:
    type_recover pm lv (valueT v n) (Φ (λ x, v ◁ᵥ B x)) ⊢ type_cast X pm lv (valueT v n) B Φ.
  Proof.
    rewrite /type_cast /=. iIntros "Hcont Hlv". destruct lv; simpl in *.
    - iExists _. iFrame. iIntros (x) "Hv". iApply (valueT_elim_loc with "Hlv Hv").
    - iDestruct "Hlv" as "#Hv". iDestruct (vtr_valueT_elim with "Hv") as "[%Heq _]". subst.
      destruct pm.
      + iExists _. iFrame. iIntros (x) "$".
      + iDestruct ("Hcont" with "Hv") as "?".
        iExists _. iFrame. iIntros (x) "$".
  Qed.

  Lemma type_cast_value_any X pm lv v n m Φ:
    type_recover pm lv (valueT v n) (Φ (λ x, ⌜n = m x⌝)) ⊢ type_cast X pm lv (valueT v n) (λ x, anyT (m x)) Φ.
  Proof.
    rewrite /type_cast /=. iIntros "Hcont Hlv". destruct lv; simpl in *.
    - iExists _. iFrame. iIntros (x ->). iApply (cast_to_any_ltr with "Hlv"). by apply valueT_has_size.
    - iDestruct "Hlv" as "#Hv". destruct pm.
      + iExists _. iFrame. iIntros (x ->). iApply (xtr_cast_to_anyT _ (VAL _) with "Hv"). by apply valueT_has_size.
      + iDestruct ("Hcont" with "Hv") as "?". iExists _. iFrame.
        iIntros (x ->). iApply (xtr_cast_to_anyT _ (VAL _) with "Hv"). by apply valueT_has_size.
  Qed.

  Lemma type_cast_value_value X pm lv v n L V Φ:
    (Φ (λ x, ⌜n = L x⌝ ∗ ⌜V x = v⌝)) ⊢ type_cast X pm lv (valueT v n) (λ x, valueT (V x) (L x)) Φ.
  Proof.
    rewrite /type_cast /=. iIntros "Hcont Hlv". destruct lv; simpl in *.
    - iExists _. iFrame. iIntros (x) "(-> & ->)". done.
    - iDestruct "Hlv" as "#Hv". iExists _. iFrame.
      iIntros (x) "(-> & ->)". done.
  Qed.

  Lemma type_cast_loc_val_no_own X lv pm v n1 n2 Φ:
    ⌜n1 = n2⌝ ∗ Φ (λ x, emp) ⊢ type_cast X pm lv (valueT v n1) (λ x: X, valueT v n2) Φ.
  Proof.
    rewrite /type_cast /=.
    iIntros "[-> HΦ] Hl". iExists _. by iFrame.
  Qed.

  (* PLACES *)
  Lemma type_cast_place_src X l pm r A Φ:
    abd_assume CTX (l ◁ₗ placeT r) (Φ (λ x, r ◁ₗ A x)) ⊢ type_cast X pm (LOC l) (placeT r) A Φ.
  Proof.
    rewrite /type_cast /abd_assume /=.
    iIntros "Hcont #Hl". iExists _. iDestruct ("Hcont" with "Hl") as "$".
    iIntros (x) "Hr". by iDestruct (placeT_is_eq with "Hl") as "->".
  Qed.

  Lemma type_cast_place_tgt X pm l r A Φ:
    abd_assume CTX (l ◁ₗ A) (Φ (λ x, ⌜r x = l⌝)) ⊢ type_cast X pm (LOC l) A (λ x, placeT (r x)) Φ.
  Proof.
    rewrite /type_cast /abd_assume /=.
    iIntros "Hcont Hl". iExists _. iDestruct ("Hcont" with "Hl") as "$".
    iIntros (x) "->". iApply placeT_create.
  Qed.

  (* VIRTUALS *)
  Lemma type_cast_constraint_tgt (lv: loc_or_val) pm X (A : type Σ) (φ: X → Prop) (B: X → type Σ) Φ :
    (abd_assume CTX (lv ◁ₓ A) (Φ (λ x, ⌜φ x⌝ ∗ lv ◁ₓ B x))) ⊢ type_cast X pm lv A (λ x, constraintT (B x) (φ x)) Φ.
  Proof.
    rewrite /type_cast /abd_assume. iIntros "Hcont Hv". iDestruct ("Hcont" with "Hv") as "Hcont".
    iExists _. iFrame. iIntros (x) "[%Hφ Hv]". rewrite xtr_constraintT_iff. by iFrame.
  Qed.

  Lemma type_cast_sep_tgt (lv: loc_or_val) pm X (A : type Σ) (P: X → iProp Σ) (B: X → type Σ) Φ :
    (abd_assume CTX (lv ◁ₓ A) (Φ (λ x, P x ∗ lv ◁ₓ B x))) ⊢ type_cast X pm lv A (λ x, sepT (B x) (P x)) Φ.
  Proof.
    rewrite /type_cast /abd_assume. iIntros "Hcont Hv". iDestruct ("Hcont" with "Hv") as "Hcont".
    iExists _. iFrame. iIntros (x) "[Hv Hp]". rewrite xtr_sepT_iff. by iFrame.
  Qed.

  Lemma type_cast_constraint_src (lv: loc_or_val) pm X (A : type Σ) (φ: Prop) (B: X → type Σ) Φ :
    (abd_assume CTX (lv ◁ₓ A) (abd_assume CTX ⌜φ⌝ (Φ (λ x, lv ◁ₓ B x)))) ⊢ type_cast X pm lv (constraintT A φ) B Φ.
  Proof.
    rewrite /type_cast /abd_assume. rewrite xtr_constraintT_iff.
    iIntros "Hcont [Hv %Hφ]". iDestruct ("Hcont" with "Hv [//]") as "Hcont".
    iExists _. iFrame. iIntros (x) "Hv". by iFrame.
  Qed.

  Lemma type_cast_sep_src (lv: loc_or_val) pm X (A : type Σ) (P: iProp Σ) (B: X → type Σ) Φ :
    (abd_assume CTX (lv ◁ₓ A) (abd_assume CTX P (Φ (λ x, lv ◁ₓ B x)))) ⊢ type_cast X pm lv (sepT A P) B Φ.
  Proof.
    rewrite /type_cast /abd_assume. rewrite xtr_sepT_iff.
    iIntros "Hcont [Hv Hp]". iDestruct ("Hcont" with "Hv Hp") as "Hcont".
    iExists _. iFrame. iIntros (x) "Hv". by iFrame.
  Qed.

  Lemma type_cast_exists_tgt (lv: loc_or_val) pm X (Y: X → Type) (A : type Σ) (B: ∀ x: X, Y x → type Σ) Φ :
    (abd_assume CTX (lv ◁ₓ A) (Φ (λ x, ∃ y, lv ◁ₓ B x y))) ⊢ type_cast X pm lv A (λ x, existsT (B x)) Φ.
  Proof.
    rewrite /type_cast /abd_assume. iIntros "Hcont Hv". iDestruct ("Hcont" with "Hv") as "Hcont".
    iExists _. iFrame. iIntros (x) "Hv". rewrite xtr_existsT_iff. by iFrame.
  Qed.

  Lemma type_cast_exists_src (lv: loc_or_val) pm X Y (A : Y → type Σ) (B: X→ type Σ) Φ :
    (∀ y, abd_assume CTX (lv ◁ₓ A y) (Φ (λ x, lv ◁ₓ B x))) ⊢ type_cast X pm lv (existsT A) B Φ.
  Proof.
    rewrite /type_cast /abd_assume. rewrite xtr_existsT_iff.
    iIntros "Hcont Hv". iDestruct "Hv" as (y) "Hv".
    iDestruct ("Hcont" with "Hv") as "Hcont".
    iExists _. iFrame. iIntros (x) "Hv". by iFrame.
  Qed.


  (** structs *)
  Lemma type_cast_struct X pm l sl1 sl2 As Bs Φ:
    ⌜sl1 = sl2⌝ ∗ (abd_assume CTX ([∗ list] l; A ∈ member_locs l sl1; As, l ◁ₗ A) (type_mono Φ (λ x, ty_own_all (Bs x) (member_locs l sl2))))
    ⊢ type_cast X pm (LOC l) (structT sl1 As) (λ x, structT sl2 (Bs x)) Φ.
  Proof.
    rewrite /type_cast /abd_assume /=. iIntros "[-> HΦ] Hl".
    iDestruct (struct_focus_ty_own_all with "Hl") as "[Hl Hconts]".
    iDestruct ("HΦ" with "Hl") as "Hmono". rewrite /type_mono.
    iDestruct "Hmono" as (Q) "[Hw HΦ]". iExists _. iFrame.
    iIntros (x) "HQ". iApply "Hconts". by iApply "Hw".
  Qed.

  Lemma type_cast_any_fixed_size X pm l m (sl: struct_layout) Us Φ:
    type_extract_prefix l (anyT m) (ly_size sl) (λ l v, l ◁ₗ structT sl (map anyT (member_sizes sl)) -∗ Φ (λ x, l ◁ₗ structT sl (Us x))) ⊢ type_cast X pm (LOC l) (anyT m) (λ x, structT sl (Us x)) Φ.
  Proof.
    rewrite /type_extract_prefix /type_cast /=. iIntros "Hcont Hl".
    iDestruct ("Hcont" with "Hl") as (v) "[Hl Hcont]".
    rewrite cast_to_any_ltr; last by apply valueT_has_size.
    rewrite anyT_structT_equiv_member_layouts.
    iDestruct ("Hcont" with "Hl") as "Hcont".
    iExists _. iFrame. iIntros (x) "$".
  Qed.

  (* slices *)
  Lemma type_cast_slices X pm l len len' As Bs Φ :
     abd_assume CTX ([∗ list] a ∈ As, (l +ₗ a.1.1) ◁ₗ a.2)
       (type_mono Φ (λ x, ⌜len = len' x⌝ ∗ ⌜Forall2 eq As.*1 (Bs x).*1⌝ ∗ [∗ list] b ∈ Bs x, (l +ₗ b.1.1) ◁ₗ b.2)%I) ⊢
    type_cast X pm (LOC l) (slicesT len As) (λ x, slicesT (len' x) (Bs x)) Φ.
  Proof.
    rewrite /abd_assume/type_cast/type_mono.
    iIntros "HΦ Hl /=".
    iDestruct (slicesT_borrow_all with "Hl") as "[Hl Hs]".
    rewrite /ty_own_all/slices_locs !big_sepL2_fmap_l !big_sepL2_fmap_r -big_sepL_sepL2_diag_iff.
    iDestruct ("HΦ" with "Hl") as (?) "[Hc ?]".
    iExists _. iFrame. iIntros (x) "HQ".
    iDestruct ("Hc" with "HQ") as (? Hall) "HBs". subst.
    have -> : (As.*1 = (Bs x).*1) by elim: Hall; naive_solver.
    iSpecialize ("Hs" $! (Bs x).*2).
    iEval (rewrite -(zip_fst_snd (Bs x))). iApply "Hs".
    by rewrite  !big_sepL2_fmap_l !big_sepL2_fmap_r -big_sepL_sepL2_diag_iff.
  Qed.

  Lemma type_cast_slices_project_slice X pm l len Ls n B Φ:
    (∀ x, B x `has_size` n) →
    type_project_slice l 0 n Ls (λ i li A L R, ⌜li = n⌝ ∗
      abd_assume CTX (l ◁ₗ slicesT len (L ++ (0, n, placeT (l +ₗ 0)) :: R) ∗ (l +ₗ 0) ◁ₗ A) (Φ (λ x, (l +ₗ 0) ◁ₗ B x)))
    ⊢ type_cast X pm (LOC l) (slicesT len Ls) B Φ.
  Proof.
    rewrite /type_project_slice /type_cast /=.
    iIntros (Hsz) "Hcont Hl". iSpecialize ("Hcont" $! len nil nil).
    rewrite app_nil_l app_nil_r.
    iDestruct ("Hcont" with "Hl") as "(%i & %li & %A & %L & %R & % & % & % & Hl & Ha & -> & Hcont)".
    have -> : i = 0 by lia.
    rewrite app_nil_l app_nil_r.
    rewrite /abd_assume.
    iSpecialize ("Hcont" with "[$Hl $Ha]"). iExists _. iFrame.
    iIntros (x) "Hl". by rewrite shift_loc_0.
  Qed.

  (* The automation uses the following version *)
  Lemma type_cast_slices_project_slice' X pm l len Ls n m A B Φ:
    (∀ x, B x `has_size` n) →
    Solve (n = m) →
    type_project_slice l 0 n ((0, m, A) :: Ls) (λ i li A L R, ⌜li = n⌝ ∗
      abd_assume CTX (l ◁ₗ slicesT len (L ++ (0, n, placeT (l +ₗ 0)) :: R) ∗ (l +ₗ 0) ◁ₗ A) (Φ (λ x, (l +ₗ 0) ◁ₗ B x)))
    ⊢ type_cast X pm (LOC l) (slicesT len ((0, m, A) :: Ls)) B Φ.
  Proof.
    intros ? ->.  by apply type_cast_slices_project_slice.
  Qed.

  Lemma type_cast_slices_array {X Y} pm l (n m: Z) (As: list (Z * Z * type Σ)) Φ (xs: X → list Y) sz B:
    (∀ x, B x `has_size` m) →
    0 ≤ m →
    abd_assume CTX ([∗ list] p ∈ As, (l +ₗ p.1.1) ◁ₗ p.2) $ Φ (λ x, existsN (length As) (λ Xs, ⌜xs x = concat Xs⌝ ∗ ⌜sz x = m⌝ ∗ ⌜Forall2 (λ xs p, (p.1.2: Z) = length xs * m)%Z Xs As⌝ ∗ [∗ list] p;ys ∈ As;Xs, (l +ₗ p.1.1) ◁ₗ arrayT B (sz x) ys))%I
    ⊢ type_cast X pm (LOC l) (slicesT n As) (λ x, arrayT B (sz x) (xs x))%RT Φ.
  Proof.
    intros HB Hm. rewrite /abd_assume /type_cast. iIntros "Hcont Hl".
    iDestruct (slicesT_covers (LOC l) with "Hl") as "%Hcov".
    iDestruct (slicesT_borrow_all with "Hl") as "[Hall Hslices]".
    rewrite {1}/slices_locs {1}/ty_own_all.
    rewrite !big_sepL2_fmap_l !big_sepL2_fmap_r.
    rewrite big_sepL2_alt zip_diag big_sepL_fmap /=.
    iDestruct "Hall" as "[_ Hall]". iDestruct ("Hcont" with "Hall") as "Hcont".
    iExists _. iFrame. rewrite /existsN.
    iIntros (x) "(%ys & %Hlen & -> & -> & %Hsz & Hall)".
    iDestruct ("Hslices" $! (map (arrayT B m) ys) with "[Hall]") as "Hl".
    { rewrite /ty_own_all /slices_locs.
      rewrite !big_sepL2_fmap_l !big_sepL2_fmap_r. iFrame. }
    rewrite /slicesT ltr_constraintT_iff. iDestruct "Hl" as "[Hl _]".
    rewrite fst_zip; last rewrite !fmap_length Hlen //.
    rewrite snd_zip; last rewrite !fmap_length Hlen //.
    enough ((Z.to_nat n) = (length (concat ys) * Z.to_nat m)%nat) as Heq.
    - rewrite arrayT_concat //.
      + rewrite Heq. iFrame.
      + destruct Hcov as (Hcov & ? & ?). rewrite Heq in Hcov. done.
      + rewrite !Forall2_fmap_r. clear -Hsz.
        induction Hsz as [|xs [[i li] q]]; first rewrite Forall2_nil //.
        rewrite Forall2_cons. simpl in *. subst. split; last done. lia.
    - destruct Hcov as (Hcov & Hn & Hall). clear Hlen.
      revert Hcov Hall Hsz. induction As as [|[[i li] A] As IH] using rev_ind in ys, n, Hn |-*; simpl.
      + rewrite covers_nil_iff Forall_nil. intros ? ? ?%Forall2_nil_inv_r. subst; simpl. lia.
      + rewrite !fmap_app map_app /=. rewrite covers_app_singleton_iff.
        rewrite Forall_app !Forall_cons Forall_nil.
        intros [Hcov Heq] (Hall & Hle & _) (l1 & l2 & Hall1 & Hall2 & ->)%Forall2_app_inv_r.
        eapply Forall2_cons_inv_r in Hall2 as (? & ? & ? & Hall2 & ?). eapply Forall2_nil_inv_r in Hall2.
        simpl in *. subst. rewrite concat_app /= app_nil_r app_length.
        rewrite Nat.mul_add_distr_r.
        erewrite <-IH; [| | done|done|done]; lia.
  Qed.

  Lemma type_cast_slices_any {X} pm l (n m: Z) (As: list (Z * Z * type Σ)) Φ sz:
    abd_assume CTX ([∗ list] p ∈ As, (l +ₗ p.1.1) ◁ₗ p.2) $ Φ (λ x,
      ⌜sz x = n⌝ ∗ [∗ list] p ∈ As, (l +ₗ p.1.1) ◁ₗ anyT p.1.2)%I
    ⊢ type_cast X pm (LOC l) (slicesT n As) (λ x, anyT (sz x))%RT Φ.
  Proof.
    rewrite /abd_assume /type_cast. iIntros "Hcont Hl".
    iPoseProof (slices_has_bounds with "Hl") as "(#Hlb & %Hn)".
    iDestruct (slicesT_covers (LOC l) with "Hl") as "%Hcov".
    iDestruct (slicesT_borrow_all with "Hl") as "[Hall Hslices]".
    rewrite {1}/slices_locs {1}/ty_own_all.
    rewrite !big_sepL2_fmap_l !big_sepL2_fmap_r.
    rewrite big_sepL2_alt zip_diag big_sepL_fmap /=.
    iDestruct "Hall" as "[_ Hall]". iDestruct ("Hcont" with "Hall") as "Hcont".
    iExists _. iFrame.
    iIntros (x) "(%Hsz & Hall)".
    - destruct Hcov as (Hcov & _ & Hall).
      rewrite Hsz. clear Hsz sz.
      iClear "Hslices".
      iInduction As as [|[[i li] A] As] "IH" using rev_ind forall (n Hn Hcov Hall) "Hlb"; simpl.
      + move: Hcov Hall. rewrite covers_nil_iff Forall_nil. intros ? ?. subst; simpl.
        assert (n = 0) as -> by lia.
        rewrite anyT_eq /anyT_def. rewrite ltr_existsT_iff. iExists [].
        iApply valueT_intro; first done.
        rewrite heap_mapsto_nil. done.
      + move: Hcov Hall. rewrite !fmap_app map_app /=. rewrite covers_app_singleton_iff.
        rewrite Forall_app !Forall_cons Forall_nil.
        intros [Hcov Heq] (Hf & (? & ?) & _).
        rewrite -Z2Nat.inj_add in Heq; [ | lia..].
        apply Z2Nat.inj in Heq; [ | lia..].
        subst.
        rewrite big_sepL_app; simpl. iDestruct "Hall" as "(Hall & Ha & _)".
        rewrite anyT_split; [ | lia..].
        rewrite ltr_slices_iff.
        simpl.
        iSplitL; first last. { iPureIntro. split; first lia. repeat first [econstructor | lia]. }
        rewrite seqT_eq /seqT_def/=.
        iSplitR. { iPureIntro.
          rewrite Z2Nat.inj_add; [ | lia.. ].
          eapply (covers_cons _ _ [_]). apply (covers_cons' _ _ []); first econstructor. lia.
        }
        iFrame "Hlb".
        iSplitR "Ha"; first last. { iSplitL; last done.
          rewrite Z2Nat.id; last lia. done. }
        rewrite Z2Nat.inj_0/=.
        rewrite shift_loc_0_nat. iApply ("IH" with "[] [//] [//] Hall []"); first done.
        iModIntro. iApply (loc_in_bounds_shorten with "Hlb"). lia.
  Qed.

  (* arrays *)
  Lemma type_cast_array_copy pm X Y lv (T: Y → type Σ) sz sz' xs ys  Φ:
    (∀ x, Copy (T x)) →
    type_recover pm lv (arrayT T sz xs) (Φ (λ x, ⌜sz = sz' x⌝ ∗ ⌜xs = ys x⌝))
  ⊢ type_cast X pm lv (arrayT T sz xs) (λ x, arrayT T (sz' x) (ys x)) Φ.
  Proof.
    intros ?. rewrite /type_cast. iIntros "Hcont Hx".
    iDestruct (type_recover_copy with "Hcont Hx") as "[Hcont Hx]".
    iExists _. iFrame. iIntros (x) "[<- <-]". iFrame.
  Qed.

  Lemma type_cast_array X Y pm lv (T: Y → type Σ) sz sz' xs ys  Φ:
    Φ (λ x, ⌜sz = sz' x⌝ ∗ ⌜xs = ys x⌝)
  ⊢ type_cast X pm lv (arrayT T sz xs) (λ x, arrayT T (sz' x) (ys x)) Φ.
  Proof.
    rewrite /type_cast. iIntros "Hcont Hx".
    iExists _. iFrame. iIntros (x) "[<- <-]". iFrame.
  Qed.

  (* MOVED *)
  Lemma type_cast_virtual X pm lv A B Φ:
    type_virt lv A (λ A', lv ◁ₓ A' -∗ Φ (λ x, lv ◁ₓ B x))
  ⊢ type_cast X pm lv A B Φ.
  Proof.
    rewrite /type_virt /type_cast. iIntros "Hcont Hl".
    iDestruct ("Hcont" with "Hl") as (v) "[Hl Hcont]".
    iDestruct ("Hcont" with "Hl") as "Hcont". iExists _. iFrame.
    iIntros (x) "$".
  Qed.

  (* WAND *)
  Lemma type_cast_wandT X Y pm l A (P : Y → iProp Σ) T Φ:
    type_precond Y pm P (λ _, []) (λ _, []) (λ _, []) (λ y, Some (abd_assume CTX (l ◁ₗ T y) $ Φ (λ x, l ◁ₗ A x)))  ⊢
    type_cast X pm (LOC l) (wandT P T) A Φ.
  Proof.
    rewrite /abd_assume /type_precond /type_cast /=.
    iIntros "(%x&?&_&_&_&Hcont) Hwand". iExists _.
    iSplitR; last first.
    - iApply "Hcont". by iApply (wandT_elim with "Hwand").
    - iIntros (?) "$".
  Qed.



  (* PRECOND EXTENSION *)
  Lemma type_precond_existential_type_var X Y pm A B (y: X → Y) C sub (P: X → iProp Σ) E M B' C':
    (∀ x, TypeParams (B x) Y (y x) C sub) →
    (∃ z: Y, ⌜A `is_ty` C z⌝ ∗ type_precond X pm (λ x, ⌜sub (y x) z⌝ ∗ P x) E M B' C')
  ⊢ type_precond X pm (λ x, ⌜A `is_ty` B x⌝ ∗ P x) E M B' C'.
  Proof.
    rewrite /type_precond. iIntros (Hparam) "(%z & %Hty & (%x & [%Hsub HP] & HE & HM & HB & HC))".
    iExists _. iFrame. iPureIntro. setoid_rewrite Hty. constructor.
    eapply Hparam. done.
  Qed.

  Definition type_precond_xtr X pm (lv: X → loc_or_val) (A: X → type Σ) P E M B C : iProp Σ :=
    type_precond X pm (λ x, lv x ◁ₓ A x ∗ P x)%I E M B C.

  Lemma type_precond_xtr_non_dep (X: Type) (lv: loc_or_val) pm (A2: X → type Σ) (P: X → iProp Σ) E M M' B C:
    (∀ x n, [∗] (M' n x).*2 ⊣⊢ lv ◁ₓ A2 x ∗ [∗] ((M x).*2)) →
    type_loc_or_val lv (λ lv' A1,
      type_progress_type X lv' A1 A2 (λ p,
        type_progress_match p
          (type_cast X pm lv' A1 A2 (λ Q, type_precond X pm (λ x, Q x ∗ P x) E M B C))
          (abd_assume CTX (lv' ◁ₓ A1) (type_precond X pm P E M (λ x, (lv' ◁ₓ A2 x) :: B x) C))
          (λ n, abd_assume CTX (lv' ◁ₓ A1) (type_precond X pm P E (M' n) B C))
          (abd_fail "type cast failed")
      )) ⊢ type_precond_xtr X pm (λ _, lv) A2 P E M B C.
  Proof.
    rewrite /type_precond_xtr.
    rewrite /type_loc_or_val /abd_assume /type_progress_type /type_progress_match /type_precond.
    iIntros (Heq) "[%A1 [Hv Hcont]]". iDestruct "Hcont" as "[%p Hcont]".
    destruct p; simpl.
    - iDestruct ("Hcont" with "Hv") as (Q) "[Hcast Hcont]".
      iDestruct "Hcont" as (x) "([HQ HP] & HB & HM & HC)".
      iExists x. iFrame. by iApply "Hcast".
    - iDestruct ("Hcont" with "Hv") as (x) "(HP & HE & HM & [Hv HB] & HC)".
      iExists x. iFrame.
    - iDestruct ("Hcont" with "Hv") as (x) "(HP & HE & HM & HB & HC)".
      rewrite Heq. iDestruct "HM" as "[Hv HM]".
      iExists x. iFrame.
    - rewrite /abd_fail. iDestruct "Hcont" as %Hfalse. done.
  Qed.

  Lemma type_precond_xtr_dep (X: Type) pm (lv: X → loc_or_val) (A: X → type Σ) (P: X → iProp Σ) E M B C:
    type_precond X pm P E M (λ x, (lv x ◁ₓ A x) :: B x) C ⊢ type_precond_xtr X pm lv A P E M B C.
  Proof.
    rewrite /type_precond.
    iIntros "Hprecond". iDestruct "Hprecond" as (x) "(HP & HE & HM & [Hlv HB] & HC)".
    iExists x. iFrame.
  Qed.

  Lemma type_precond_sep_xtr (X: Type)  pm lv (A: X → type Σ) (P: X → iProp Σ) E M B C:
    type_precond_xtr X pm lv A P E M B C ⊢ type_precond X pm (λ x, lv x ◁ₓ A x ∗ P x) E M B C.
  Proof. done. Qed.

  Lemma type_precond_sep_vtr (X: Type) pm (v: X → val) (A: X → type Σ) (P: X → iProp Σ) E M B C:
    type_precond_xtr X pm (λ x, VAL (v x)) A P E M B C ⊢ type_precond X pm (λ x, v x ◁ᵥ A x ∗ P x) E M B C.
  Proof. done. Qed.

  Lemma type_precond_sep_ltr (X: Type) pm (l: X → loc) (A: X → type Σ) (P: X → iProp Σ) E M B C:
    type_precond_xtr X pm (λ x, LOC (l x)) A P E M B C ⊢ type_precond X pm (λ x, l x ◁ₗ A x ∗ P x) E M B C.
  Proof. done. Qed.

  (* MAYBE *)
  Lemma type_maybe_cast (X: Type) pm (lv: loc_or_val) A1 A2 Φ:
    lv ◁ₓ A1 ∗ type_cast X pm lv A1 A2 Φ ⊢ type_maybe X pm (λ x, lv ◁ₓ A2 x) Φ.
  Proof.
    rewrite /type_cast /type_maybe.
    iIntros "[Hv Hcont]". by iApply "Hcont".
  Qed.

  Lemma type_maybe_has_size_unknown (X: Type) pm (A: X → type Σ) (n: X → Z) Φ:
    Φ (λ x, ∃ v, ⌜A x = valueT v (n x)⌝)%I ⊢ type_maybe X pm (λ x, A x `has_size` n x: iProp Σ) Φ.
  Proof.
    rewrite /type_maybe.
    iIntros "Hcont". iExists _. iFrame.
    iIntros (x) "H". iDestruct "H" as (v) "%Hty".
    rewrite Hty. iPureIntro. by eapply valueT_has_size.
  Qed.

  Lemma type_maybe_has_size_known (X: Type) pm (A: type Σ) m (n: X → Z) Φ:
    ⌜A `has_size` m⌝ ∗ Φ (λ x, ⌜n x = m⌝)%I ⊢ type_maybe X pm (λ x, A `has_size` n x: iProp Σ) Φ.
  Proof.
    rewrite /type_maybe.
    iIntros "[%Hty Hcont]". iExists _. iFrame.
    iIntros (x) "%Heq". iPureIntro. by subst.
  Qed.

End type_cast.



Global Typeclasses Opaque
  type_precond_xtr
  type_unify
  type_progress_type
  type_cast
  type_recover.

Global Typeclasses Opaque
  type_from_zeros
  type_from_zeros_all.

Global Typeclasses Transparent type_pre_type.


Global Typeclasses Opaque
  type_extract_prefix type_extract_range.
