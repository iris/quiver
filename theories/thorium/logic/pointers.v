From quiver.thorium.types Require Import types base_types pointers refinements structs.
From quiver.argon.base Require Import classes.
From quiver.thorium.logic Require Import  syntax assume type_cast.


Section weakest_pre_rules.
  Context `{!refinedcG Σ}.

  Implicit Types
    (l: loc) (v: val) (e: expr) (s: stmt)
    (ot: op_type) (ly: layout) (A B: type Σ).


  Lemma wp_deref_typed ot l A Ψ :
    A `has_size` ly_size (ot_layout ot) →
    (∀ v,
      l ↦ v -∗
      v ◁ᵥ A -∗
      ⌜v `has_size_val` ly_size (ot_layout ot)⌝ -∗
      Ψ v)%I ∗ l ◁ₗ A
    ⊢ WP (!{ot, Na1Ord, false} l) {{ Ψ }}.
  Proof.
    iIntros (Hsz) "[Hpost Hl]".
    iPoseProof (ty_move with "Hl") as "[%v [Hl Hv]]"; first done.
    iPoseProof (ty_size_eq with "Hv") as "%Hsz2"; eauto.
    iApply (wp_deref_aligned with "Hl"); eauto with quiver_logic.
    { by eapply has_size_val_has_layout. }
    iNext. iIntros (st) "Hl".
    iApply ("Hpost" with "Hl Hv [//]").
  Qed.

  Lemma wps_assign_typed (l: loc) ot (v: val) B s Q Φ:
    B `has_size` ly_size (ot_layout ot) →
    v ◁ᵥ B ∗
    l ↦|ot_layout ot| ∗
    (l ↦ v -∗ v ◁ᵥ B -∗ WPs s {{Q, Φ}}) ⊢ WPs (l <-{ot} v; s) {{Q, Φ}}.
  Proof.
    iIntros (Hsz) "(Hv & Hl & Hpost)"; iApply (wps_assign with "[-]"); simpl; eauto with quiver_logic.
    iModIntro.
    iPoseProof (ty_size_eq with "Hv") as "%Hsz2"; eauto.
    iFrame. iSplit; first by iPureIntro; eapply has_size_val_has_layout.
    iNext. iIntros "Hl".
    iModIntro. iApply ("Hpost" with "Hl Hv").
  Qed.

End weakest_pre_rules.


Section pointer_rules.
  Context `{!refinedcG Σ}.

  Implicit Types
    (l: loc) (v: val) (e: expr) (s: stmt)
    (ot: op_type) (A B: type Σ).


  Definition type_move_all (ls: list loc) (As: list (type Σ)) (ns: list Z) (Φ: list val → iProp Σ) : iProp Σ :=
    ty_own_all As ls -∗ ∃ vs, ⌜length vs = length ns⌝ ∗ ty_own_all ((λ '(v, n), valueT v n) <$> (zip vs ns)) ls ∗ Φ vs.


  (* VAL *)
  Lemma type_val_own (l: loc) Φ:
    type_loc l (λ l A, Φ (l: val) (ownT l A)) ⊢ type_val l Φ.
  Proof.
    rewrite /type_val /type_loc. iIntros "[%A [Hl HΦ]]".
    iExists _. iFrame. rewrite ownT_ty_own_val_own. by iFrame.
  Qed.

  (* MOVE *)
  Lemma type_move_sized l A n Φ:
    ⌜A `has_size` n⌝ ∗ (∀ v, v ◁ᵥ A -∗ Φ v) ⊢ type_move l A n Φ.
  Proof.
    rewrite /type_move. iIntros "[%Hsz Hcont] Hl".
    iDestruct (ty_move with "Hl") as (v) "[Hl Hv]"; first done.
    iDestruct (ty_size_eq with "Hv") as "%Hv"; first done.
    iDestruct (valueT_intro with "Hl") as "Hl"; first done.
    iExists _. iFrame. by iApply "Hcont".
  Qed.

  Lemma type_move_value l v n m Φ:
    ⌜n = m⌝ ∗ Φ v ⊢ type_move l (valueT v n) m Φ.
  Proof.
    rewrite /type_move. iIntros "[-> HΦ] Hl".
    iExists _. iFrame.
  Qed.

  Lemma type_move_opt l v n Φ:
    ⌜n = ptr_size⌝ ∗ Φ v ⊢ type_move l (optT v) n Φ.
  Proof.
    rewrite /type_move. iIntros "[-> HΦ] Hl".
    iExists _. rewrite /optT ltr_sepT_iff.
    by iDestruct "Hl" as "[$ _]".
  Qed.

  Lemma type_move_own (l: loc) p n A (Φ: val → iProp Σ):
    ⌜n = ptr_size⌝ ∗ abd_assume CTX (p ◁ₗ A) (Φ p) ⊢ type_move l (ownT p A) n Φ.
  Proof.
    rewrite /type_move /abd_assume. iIntros "[-> HΦ] Hl".
    rewrite -valueT_ownT. iDestruct "Hl" as "[Hl Hp]".
    iSpecialize ("HΦ" with "Hp"). iExists _. iFrame.
  Qed.

  Lemma type_move_place (l: loc) (r: loc) n Φ:
    type_loc r (λ r' A, abd_assume CTX (l ◁ₗ placeT r') (type_move r' A n Φ)) ⊢ type_move l (placeT r) n Φ.
  Proof.
    rewrite /type_move /abd_assume /type_loc. iIntros "Hr Hl".
    iDestruct "Hr" as (A) "[Hr HΦ]".
    iDestruct (placeT_is_eq with "Hl") as "->".
    by iApply ("HΦ" with "Hl Hr").
  Qed.

  Lemma type_move_virtual l A n Φ :
    type_virt (LOC l) A (λ A', type_move l A' n Φ) ⊢ type_move l A n Φ.
  Proof.
    rewrite /type_move /type_virt. iIntros "Hcont Hl".
    iDestruct ("Hcont" with "Hl") as (A') "[Hl HΨ]".
    iApply ("HΨ" with "Hl").
  Qed.

  Lemma type_move_struct_move_all (n: Z) (sl : struct_layout) l As Φ:
    ⌜n = ly_size sl⌝ ∗ type_move_all (member_locs l sl) As (member_sizes sl) (λ vs, ∀ v, (v ◁ᵥ struct[sl] ((λ '(w, n), value[n] w) <$> zip vs (member_sizes sl))) -∗ Φ v) ⊢
    type_move l (struct[sl] As) n Φ.
  Proof.
    rewrite /type_move /type_move_all.
    iIntros "[-> Hm] Hl".
    iDestruct (struct_focus_ty_own_all with "Hl") as "[Hmems Hcont]".
    iDestruct ("Hm" with "Hmems") as (vs) "(% & Hmems & HΦ)".
    iDestruct ("Hcont" with "Hmems") as "Hl".
    erewrite (valueT_loc_val l); last first.
    { rewrite structT_eq /structT_def /=.
      eapply is_struct_size_values; eauto. }
    iDestruct "Hl" as "(%v & Hl & Hv)". iExists v. iFrame.
    by iApply "HΦ".
  Qed.

  Lemma type_move_slicesT_move_all Ts l Φ len n :
    (⌜n = len⌝ ∗ type_move_all (slices_locs l Ts) (Ts.*2) (Ts.*1.*2) (λ vs,
      ∀ v, v ◁ᵥ slicesT len (map (λ '(v, T), (T.1.1, T.1.2, valueT v T.1.2)) (zip vs Ts)) -∗ Φ v)) ⊢
    type_move l (slicesT len Ts) n Φ.
  Proof.
    rewrite /type_move/type_move_all.
    iIntros "[-> HΦ] Hl".
    iDestruct (slicesT_borrow_all with "Hl") as "[Hl Hs]".
    iDestruct ("HΦ" with "Hl") as (? Hlen) "[Hall HΦ]".
    iDestruct ("Hs" with "Hall") as "Hs".
    iDestruct (slicesT_covers (LOC _) with "Hs") as %[?[? Hall]].
    iDestruct (valueT_loc_val with "Hs") as (?) "[??]".
    - apply slicesT_has_size; [|done].
      rewrite Forall_fmap Forall_forall in Hall.
      apply Forall_forall. move => [[??]?]/= Hin. move: (Hall _ Hin) => /=[??].
      split; [|done].
      move: Hin => /(elem_of_list_lookup_1 _ _)[? ].
      move => /lookup_zip_with_Some[[??][?[?[]]]].
      move => /list_lookup_fmap_Some[[[??]?][??]].
      move => /list_lookup_fmap_Some[[??][/lookup_zip_with_Some[?[?[?[? Ht]]]]?]].
      move: Ht => /list_lookup_fmap_Some[[??][/list_lookup_fmap_Some[[[??]?][??]]?]].
      simplify_eq/=. by apply valueT_has_size.
    - iExists _. iFrame. iApply "HΦ".
      have -> : (zip Ts.*1 ((λ '(v, n), valueT v n) <$> zip vs Ts.*1.*2)) =
               (map (λ '(v0, T), (T.1.1, T.1.2, valueT v0 T.1.2)) (zip vs Ts)); last done.
      move: Hlen. rewrite !fmap_length. clear. elim: Ts vs => [ | T Ts IH ] [ ^vs ] //; csimpl.
      move => ?. rewrite IH; [|lia]. f_equal. by destruct (T.1).
  Qed.

  (* TYPE MOVE ALL *)
  Lemma type_move_all_nil Φ:
    Φ nil ⊢ type_move_all [] [] [] Φ.
  Proof.
    rewrite /type_move_all. iIntros "HΦ _".
    iExists nil. iFrame. iSplit; first done.
    rewrite /ty_own_all /= //.
  Qed.

  Lemma type_move_all_cons l ls A As n ns Φ :
    type_move l A n (λ v, type_move_all ls As ns (λ vs, Φ (v :: vs))) ⊢
    type_move_all (l :: ls) (A :: As) (n :: ns) Φ.
  Proof.
    rewrite /type_move_all /type_move. iIntros "HΦ Hl".
    rewrite ty_own_all_cons. iDestruct "Hl" as "[Hl Hall]".
    iDestruct ("HΦ" with "Hl") as "(%v & Hl & Hcont)".
    iDestruct ("Hcont" with "Hall") as "(%vs & %Hlen & Hall & Hcont)".
    iExists _. iFrame. iPureIntro. by rewrite /= Hlen.
  Qed.

  (* ADDROF *)
  Lemma type_addr_of_own l A (Ψ: val → type Σ → iProp Σ):
    Ψ l (ownT l A) ⊢ type_addr_of l A Ψ.
  Proof.
    rewrite /type_addr_of /AddrOf.
    iIntros "HΨ Hl".
    iDestruct (ownT_ty_own_val_own l l A) as "[_ Hown]".
    iSpecialize ("Hown" with "[$Hl //]").
    iApply rwp_val. iApply (type_val_intro with "Hown HΨ").
  Qed.

  (* USE *)
  Lemma type_use_move ot l A Ψ :
    A `has_size` ly_size (ot_layout ot) →
    (∀ v, abd_assume CTX (l ◁ₗ valueT v (ly_size (ot_layout ot))) (abd_assume CTX (v ◁ᵥ A) (Ψ v (valueT v (ly_size (ot_layout ot)))))) ⊢ type_use l A ot Ψ.
  Proof.
    rewrite /type_use /abd_assume. iIntros (Hop) "Hpost Hl".
    iApply (wp_deref_typed with "[Hpost $Hl]"); first done.
    iIntros (v) "Hl Hv %".
    iPoseProof ("Hpost" with "[Hl] Hv") as "Hpost".
    { by iApply (valueT_intro with "Hl"). }
    iExists _. iFrame. iApply vtr_valueT_intro. iSplit; done.
  Qed.

  Lemma type_use_copy ot l A `{!Copy A} Ψ :
    A `has_size` ly_size (ot_layout ot) →
    (∀ v, abd_assume CTX (l ◁ₗ A) (abd_assume CTX (v ◁ᵥ A) (Ψ v A))) ⊢ type_use l A ot Ψ.
  Proof.
    rewrite /type_use /abd_assume. iIntros (Hop) "Hpost Hl".
    iApply (wp_deref_typed with "[Hpost $Hl]"); eauto.
    iIntros (v) "Hl #Hv %". iExists _. iFrame "Hv".
    iApply ("Hpost" with "[Hl] Hv").
    iApply (ty_move_back with "Hl"); eauto.
  Qed.

  Lemma type_use_value (n: Z) ot l v Φ :
    n = ly_size (ot_layout ot) →
    (abd_assume CTX (l ◁ₗ valueT v n) (Φ v (valueT v n))) ⊢ type_use l (valueT v n) ot Φ.
  Proof.
    iIntros (->) "Hcont". iApply type_use_copy; first by apply valueT_has_size.
    rewrite /abd_assume. iIntros (w) "Hl Hv". iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
    by iApply "Hcont".
  Qed.

  Lemma type_use_place l r ot Φ:
    type_loc r (λ r' B, abd_assume CTX (l ◁ₗ placeT r') (type_use r' B ot Φ)) ⊢ type_use l (placeT r) ot Φ.
  Proof.
    rewrite /type_loc /abd_assume. iIntros "[%B [Hr Hass]]".
    rewrite /type_use. iIntros "#Hl". iDestruct (placeT_is_eq with "Hl") as "->".
    by iApply ("Hass" with "Hl Hr").
  Qed.

  Lemma type_use_virtual l A ot Ψ:
    type_virt (LOC l) A (λ A', type_use l A' ot Ψ) ⊢ type_use l A ot Ψ.
  Proof.
    rewrite /type_use /type_virt. iIntros "Hv Hl".
    iDestruct ("Hv" with "Hl") as (A') "[Hl HΨ]".
    iApply ("HΨ" with "Hl").
  Qed.

  (* LOAD *)
  Lemma type_load_use l A Φ :
    type_use l A PtrOp (λ v B, type_val_to_loc v B Φ) ⊢ type_load l A Φ.
  Proof.
    rewrite /type_load /type_use /type_val_to_loc /lwp /rwp. iIntros "Hpost Hl".
    iApply wp_mono; last by iApply ("Hpost" with "Hl").
    iIntros (v) "[%R [Hv Hcont]]".
    iDestruct ("Hcont" with "Hv") as "(%r & %L & ?)". iExists _, _. iFrame.
  Qed.

  (* ASSIGN *)
  Lemma type_assign_move l A v B ot s Q Ψ :
    A `has_size` ly_size (ot_layout ot) →
    B `has_size` ly_size (ot_layout ot) →
    (abd_assume CTX (l ◁ₗ valueT v (ly_size (ot_layout ot))) ((abd_assume CTX (v ◁ᵥ B)) (∀ w, abd_assume CTX (w ◁ᵥ A) (swp s Q Ψ)))) ⊢ type_assign l A v B ot s Q Ψ.
  Proof.
    rewrite /type_assign /abd_assume. iIntros (HlyA HlyB) "Hpost Hl Hv".
    iApply (wps_assign_typed with "[$Hv Hl Hpost]"); first done.
    iPoseProof (ty_move with "Hl") as "[%w [Hl Hty]]"; first done.
    iPoseProof (ty_size_eq with "Hty") as "%Hsz"; first done.
    iSplitL "Hl".
    { iApply heap_mapsto_layout_iff. iExists _; iFrame. iPureIntro. by eapply has_size_val_has_layout. }
    iIntros "Hl Hv". iPoseProof (ty_size_eq with "Hv") as "%HszB"; first done.
    iApply ("Hpost" with "[Hl] Hv Hty").
    iApply valueT_intro; auto.
  Qed.

  Lemma type_assign_copy l A v B ot s Q Ψ :
    Copy B →
    A `has_size` ly_size (ot_layout ot) →
    B `has_size` ly_size (ot_layout ot) →
    (l ◁ₗ B -∗ abd_assume CTX (v ◁ᵥ B) (∀ w, abd_assume CTX (w ◁ᵥ A) (swp s Q Ψ))) ⊢ type_assign l A v B ot s Q Ψ.
  Proof.
    rewrite /type_assign /abd_assume. iIntros (Hcp HlyA HlyB) "Hpost Hl #Hv".
    iApply wps_assign_typed; eauto. iFrame.
    iPoseProof (ty_move with "Hl") as "[%w [Hl Hw]]"; first done.
    iPoseProof (ty_size_eq with "Hw") as "%Hsz"; first done.
    iSplit; first by iFrame "Hv".
    iSplitL "Hl".
    { iApply heap_mapsto_layout_iff. iExists _; iFrame. iPureIntro. by eapply has_size_val_has_layout. }
    iIntros "Hl Hv2". iApply ("Hpost" with "[Hl Hv2] Hv Hw").
    iApply (ty_move_back with "Hl Hv2").
  Qed.

  Lemma type_assign_place l r v A ot s Q Φ:
    type_loc r (λ r' B, abd_assume CTX (l ◁ₗ placeT r') (type_assign r' B v A ot s Q Φ)) ⊢ type_assign l (placeT r) v A ot s Q Φ.
  Proof.
    rewrite /type_loc /abd_assume. iIntros "[%B [Hr Hass]]".
    rewrite /type_assign. iIntros "#Hl Hv". iDestruct (placeT_is_eq with "Hl") as "->".
    iApply ("Hass" with "Hl Hr Hv").
  Qed.

  Lemma type_assign_type_virt l A v B ot s Q Φ:
    type_virt (LOC l) A (λ A', type_assign l A' v B ot s Q Φ) ⊢ type_assign l A v B ot s Q Φ.
  Proof.
    rewrite /type_assign /type_virt. iIntros "Hcont Hl Hv".
    iDestruct ("Hcont" with "Hl") as "(%A' & Hl & Hcont)".
    iApply ("Hcont" with "Hl Hv").
  Qed.


  (* VAL-TO-LOC *)
  Lemma type_val_to_loc_own v (p: loc) A Φ:
    (abd_assume CTX (v ◁ᵥ valueT p ptr_size) (Φ p A)) ⊢ type_val_to_loc v (ownT p A) Φ.
  Proof.
    rewrite /type_val_to_loc /abd_assume. rewrite ownT_ty_own_val_own.
    iIntros "HΦ [-> Hp]". iExists _, _. iFrame. iSplit; first done.
    iApply "HΦ". by iApply vtr_valueT_intro.
  Qed.

  Lemma type_val_to_loc_value_loc v (p: loc) Φ:
    (abd_assume CTX (v ◁ᵥ valueT p ptr_size) (Φ p (placeT p))) ⊢ type_val_to_loc v (valueT p ptr_size) Φ.
  Proof.
    rewrite /type_val_to_loc /abd_assume. iIntros "HΦ #Hv".
    iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
    iExists p, _. iSplit; first done.
    iSplitR; first iApply placeT_create.
    by iApply "HΦ".
  Qed.

  (* we update the value value to the final destination
    (similar to search in a union find) *)
  Lemma type_val_to_loc_value_val v w Φ:
    type_val w (λ u A, type_val_to_loc u A (λ p B, abd_assume CTX (v ◁ᵥ valueT p ptr_size) (Φ p B))) ⊢ type_val_to_loc v (valueT w ptr_size) Φ.
  Proof.
    rewrite /type_val_to_loc /type_val /abd_assume. iIntros "HΦ Hv".
    iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
    iDestruct "HΦ" as "[%A [Hw Hcont]]".
    iPoseProof ("Hcont" with "Hw") as "(%l & %B & -> & Hl & Hcont)".
    iExists _, _. iFrame. iSplit; first done.
    iApply "Hcont". by iApply vtr_valueT_intro.
  Qed.

  Lemma type_val_to_loc_optional v A Φ:
    type_unify (VAL v) RECOVER A (dProp * type Σ) (λ x, optionalT x.1 x.2) (λ x, ⌜x.1⌝ ∗ type_val_to_loc v x.2 Φ)%I ⊢
    type_val_to_loc v A Φ.
  Proof.
    rewrite /type_unify /type_val_to_loc.
    iIntros "Hctx Hv". iSpecialize ("Hctx" with "Hv"). rewrite /type_pre_type /type_pre_post.
    iDestruct "Hctx" as "(%t & Hv & %Hp & Hcast)". destruct t as [φ B]; simpl in *.
    rewrite /optionalT /case. destruct decide; last naive_solver.
    by iApply "Hcast".
  Qed.

  Lemma type_val_to_loc_own_cast v A Φ:
    type_unify (VAL v) RECOVER A (loc * type Σ) (λ x, ownT x.1 x.2) (λ x, type_val_to_loc v (ownT x.1 x.2) Φ)%I ⊢
    type_val_to_loc v A Φ.
  Proof.
    rewrite /type_unify /type_val_to_loc.
    iIntros "Hctx Hv". iSpecialize ("Hctx" with "Hv"). rewrite /type_pre_type /type_pre_post.
    iDestruct "Hctx" as "(%t & Hv & Hcast)". destruct t as [p B]; simpl in *.
    by iApply "Hcast".
  Qed.

  Lemma type_val_to_loc_virt v A Φ:
    type_virt (VAL v) A (λ B, type_val_to_loc v B Φ) ⊢ type_val_to_loc v A Φ.
  Proof.
    rewrite /type_virt /type_val_to_loc.
    iIntros "Hcont Hv". iDestruct ("Hcont" with "Hv") as "(%B & Hv & Hcont)".
    by iApply "Hcont".
  Qed.

  Lemma type_val_to_loc_opt v w Φ:
    abd_assume CTX (v ◁ᵥ optT w) (type_val w (λ u A, type_val_to_loc u A Φ)) ⊢ type_val_to_loc v (optT w) Φ.
  Proof.
    rewrite /abd_assume /type_val_to_loc /type_val.
    iIntros "Hcont #Hv". iDestruct ("Hcont" with "Hv") as "(%A & Hw & HΦ)".
    iDestruct ("HΦ" with "Hw") as "HΦ".
    rewrite /optT vtr_sepT_iff. iDestruct "Hv" as "[Hv _]".
    by iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
  Qed.

  (* POINTER ARITHMETIC *)
  Lemma type_bin_op_ptr_offset_op ly v1 v2 A1 A2 ot1 ot2 Φ:
    type_val_to_loc v2 A2 (λ l A2', type_at_offset l v1 A2' A1 ly ot2 ot1 (λ r A, Φ (r: val) (ownT r A)))
    ⊢ type_bin_op (PtrOffsetOp ly) ot1 ot2 v1 A1 v2 A2 Φ.
  Proof.
    rewrite /type_val_to_loc /type_bin_op /type_at_offset.
    iIntros "Hcont Hv1 Hv2". iDestruct ("Hcont" with "Hv2") as "(%l & %C & -> & Hl & Hcont)".
    iDestruct ("Hcont" with "Hl Hv1") as "Hwp". rewrite /lwp /rwp.
    iApply (wp_wand with "Hwp"). iIntros (v) "(%L & %r & -> & Hr & HΦ)".
    iExists _. iFrame. rewrite ownT_ty_own_val_own. iFrame. by iPureIntro.
  Qed.

  (* POINTER CASTS *)
  Lemma type_un_op_cast_ptr v A Φ:
    PtrType A →
    Φ v A ⊢ type_un_op (CastOp PtrOp) PtrOp v A Φ.
  Proof.
    rewrite /type_un_op /rwp.
    iIntros (Hptr) "HΦ Hv". iDestruct (ptr_type_val_loc with "Hv") as "%HL".
    destruct HL as [l Hcast]. iApply wp_cast_loc; first done.
    iNext. iExists _. iFrame.
  Qed.

  Lemma type_un_op_cast_ptr_loc (l: loc) A (Φ: val → type Σ → iProp Σ):
    Φ l A ⊢ type_un_op (CastOp PtrOp) PtrOp l A Φ.
  Proof.
    rewrite /type_un_op /rwp.
    iIntros "HΦ Hv". iApply wp_cast_loc; first eauto with quiver_logic.
    iNext. iExists _. iFrame.
  Qed.

  Lemma type_un_op_cast_null_ptr v it Φ:
    (∀ w, abd_assume CTX (v ◁ᵥ intT it 0) (Φ w nullT)) ⊢ type_un_op (CastOp PtrOp) (IntOp it) v (intT it 0) Φ.
  Proof.
    rewrite /type_un_op /abd_assume /rwp. iIntros "Hcont Hv".
    iDestruct (vtr_int_inversion with "Hv") as "[-> %Hin]".
    iApply wp_cast_int_null; first eauto with quiver_logic.
    iDestruct ("Hcont" with "Hv") as "?".
    iNext. iExists _. iFrame. iApply vtr_null.
  Qed.


  (* POINTER EQUALITY CHECKS *)
  Definition type_null_check (v: val) (A: type Σ) (Φ: dProp → type Σ → iProp Σ) : iProp Σ :=
    ∃ (φ: dProp) (B: type Σ), (v ◁ᵥ A -∗ v ◁ᵥ B ∗ if decide φ then ∃ l: loc, ⌜v = l⌝ ∗ loc_in_bounds l 0 else ⌜val_to_loc v = Some NULL_loc⌝) ∗ Φ φ B.

  Global Typeclasses Opaque type_null_check.

  Lemma type_null_check_non_null_val v A Φ:
    ⌜NonNullVal A⌝ ∗ Φ True%DP A ⊢ type_null_check v A Φ.
  Proof.
    iIntros "[%Hnnv HΦ]". iExists _, _. iFrame.
    iIntros "Hv"; simpl.
    iDestruct (Hnnv with "Hv") as "#H". by iFrame.
  Qed.

  Lemma type_null_check_owned v p A Φ:
    (⌜NonNull A⌝ ∗ Φ True%DP (ownT p A)) ⊢ type_null_check v (ownT p A) Φ.
  Proof.
    iIntros "[%Hn HΦ]". iApply type_null_check_non_null_val. iFrame "HΦ".
    iPureIntro. by apply non_null_val_own.
  Qed.

  Lemma type_null_check_null v Φ:
    (Φ False%DP nullT) ⊢ type_null_check v nullT Φ.
  Proof.
    iIntros "HΦ". rewrite /type_null_check. iExists _, _. iFrame; simpl.
    iIntros "Hv". iDestruct (nullT_inv with "Hv") as "%Hn". iFrame.
    by iPureIntro.
  Qed.

  Lemma type_null_check_option_non_null_val v A φ Φ:
    ⌜NonNullVal A⌝ ∗ (Φ φ (optionalT φ A)) ⊢ type_null_check v (optionalT φ A) Φ.
  Proof.
    iIntros "[%Hnnv HΦ]". iExists _, _. iFrame.
    iIntros "Hv". rewrite /optionalT /case.
    destruct decide.
    + iDestruct (Hnnv with "Hv") as "#H". by iFrame.
    + iDestruct (nullT_inv with "Hv") as "%"; subst.
      iFrame. by iPureIntro.
  Qed.

  Lemma type_null_check_option v A φ Φ:
    type_null_check v A (λ ψ C, Φ (φ ∧ ψ)%DP (optionalT φ C)) ⊢ type_null_check v (optionalT φ A) Φ.
  Proof.
    rewrite /type_null_check.
    iIntros "[%ψ [%B [Hw HΦ]]]". iExists _, _. iFrame.
    iIntros "Hv". rewrite /optionalT /case.
    destruct (decide φ); last first.
    + simpl; destruct (decide (φ ∧ ψ)); first naive_solver.
      iDestruct (nullT_inv with "Hv") as "%"; subst.
      iFrame. by iPureIntro.
    + iSpecialize ("Hw" with "Hv").
      destruct decide, decide; naive_solver.
  Qed.


  Lemma type_null_check_opt v w Φ:
    Φ (not_null w) (optT w) ⊢ type_null_check v (optT w) Φ.
  Proof.
    iIntros "HΦ". rewrite /type_null_check. iExists _, _. iFrame.
    iIntros "#Hv". iFrame "Hv". iDestruct (optT_not_null with "Hv") as "Hx".
    rewrite /case. destruct decide; last done.
    iDestruct ("Hx") as (l) "(%Heq & Hb)".
    iExists l. iFrame "Hb". iDestruct (loc_in_bounds_null_loc with "Hb") as "%Hne".
    eapply val_of_to_loc in Heq as Hn. iPureIntro. destruct Hn as [|[??]]; first done.
    subst v. rewrite val_of_to_loc_null_bytes in Heq. naive_solver.
  Qed.

  (* for refinements and fixpoints *)
  Lemma type_null_check_eq v A B Φ:
    B ≡ A →
    type_null_check v A Φ ⊢ type_null_check v B Φ.
  Proof.
    rewrite /type_null_check.
    iIntros (Heq) "[%ψ [%C [Hw HΦ]]]".
    iExists _, _. iFrame. rewrite Heq //.
  Qed.

  Lemma type_null_check_type_binop_ne_null v w A it Φ :
    type_null_check v A (λ φ B, abd_assume CTX (v ◁ᵥ B) (abd_assume CTX (w ◁ᵥ nullT) (Φ (b2v (bool_decide φ) it) (φ @ boolR it)%RT))) ⊢ type_bin_op (NeOp it) PtrOp PtrOp v A w nullT Φ.
  Proof.
    rewrite /type_bin_op /type_null_check /abd_assume /rwp. iIntros "HΦ Hv #Hw".
    iPoseProof (nullT_inv with "Hw") as "%Heq".
    iDestruct "HΦ" as "[%φ [%B [Hcont Hc]]]".
    rewrite /with_refinement /boolR /rty.
    rewrite bool_decide_decide; simpl.
    destruct decide; last first.
    - iDestruct ("Hcont" with "Hv") as "[Hv %Hr]".
      iSpecialize ("Hc" with "Hv Hw").
      iApply wp_binop_det_pure; last first.
      { iNext. iExists _. iFrame. iApply vtr_bool. }
      intros σ u. rewrite eval_bin_op_val_to_loc /NULL; eauto with quiver_logic.
      rewrite eval_bin_op_ptr_cmp //; eauto using heap_loc_eq_NULL_NULL.
      rewrite val_of_Z_bool. naive_solver.
    - iDestruct ("Hcont" with "Hv") as "[Hv #Hloc]".
      iSpecialize ("Hc" with "Hv Hw").
      iDestruct "Hloc" as "[%l [-> Hloc]]".
      iApply wp_binop_det; iIntros (σ) "SC".
      iPoseProof (loc_in_bounds_to_heap_loc_in_bounds with "Hloc SC") as "%".
      iMod (fupd_mask_subseteq ∅) as "Hcont"; first set_solver.
      iModIntro. iSplit; last first.
      { iNext. iMod "Hcont". iModIntro. iFrame.
        iExists _. iFrame. iApply vtr_bool. }
      iPureIntro.
      intros v.
      rewrite eval_bin_op_val_to_loc /NULL; eauto with quiver_logic.
      rewrite eval_bin_op_ptr_cmp //; eauto using heap_loc_eq_alloc_NULL.
      rewrite val_of_Z_bool; naive_solver.
  Qed.

  Lemma type_null_check_type_binop_eq_null v w A it Φ :
    type_null_check v A (λ φ B, abd_assume CTX (v ◁ᵥ B) (abd_assume CTX (w ◁ᵥ nullT) (Φ (b2v (bool_decide (¬ φ)) it) ((¬ φ)%DP @ boolR it)%RT))) ⊢ type_bin_op (EqOp it) PtrOp PtrOp v A w nullT Φ.
  Proof.
    rewrite /type_bin_op /type_null_check /abd_assume /rwp. iIntros "HΦ Hv #Hw".
    iPoseProof (nullT_inv with "Hw") as "%Heq".
    iDestruct "HΦ" as "[%φ [%B [Hcont Hc]]]".
    rewrite /with_refinement /boolR /rty.
    rewrite bool_decide_decide; simpl.
    destruct decide; last first.
    - iDestruct ("Hcont" with "Hv") as "[Hv %Hr]".
      iSpecialize ("Hc" with "Hv Hw").
      iApply wp_binop_det_pure; last first.
      { iNext. iExists _. iFrame. iApply vtr_bool. }
      intros σ u. rewrite eval_bin_op_val_to_loc /NULL; eauto with quiver_logic.
      rewrite eval_bin_op_ptr_cmp //; eauto using heap_loc_eq_NULL_NULL.
      rewrite val_of_Z_bool. destruct decide; naive_solver.
    - iDestruct ("Hcont" with "Hv") as "[Hv #Hloc]".
      iSpecialize ("Hc" with "Hv Hw").
      iDestruct "Hloc" as "[%l [-> Hloc]]".
      iApply wp_binop_det; iIntros (σ) "SC".
      iPoseProof (loc_in_bounds_to_heap_loc_in_bounds with "Hloc SC") as "%".
      iMod (fupd_mask_subseteq ∅) as "Hcont"; first set_solver.
      iModIntro. iSplit; last first.
      { iNext. iMod "Hcont". iModIntro. iFrame.
        iExists _. iFrame. iApply vtr_bool. }
      iPureIntro.
      intros v. rewrite eval_bin_op_val_to_loc /NULL; eauto with quiver_logic.
      rewrite eval_bin_op_ptr_cmp //; eauto using heap_loc_eq_alloc_NULL.
      rewrite val_of_Z_bool; destruct decide; naive_solver.
  Qed.

  Lemma type_null_check_type_binop_null_ne v w A it Φ :
    type_null_check w A (λ φ B, abd_assume CTX (v ◁ᵥ nullT) (abd_assume CTX (w ◁ᵥ B) (Φ (b2v (bool_decide φ) it) (φ @ boolR it)%RT))) ⊢ type_bin_op (NeOp it) PtrOp PtrOp v nullT w A Φ.
  Proof.
    rewrite /type_bin_op /type_null_check /abd_assume /rwp. iIntros "HΦ #Hv Hw".
    iPoseProof (nullT_inv with "Hv") as "%Heq".
    iDestruct "HΦ" as "[%φ [%B [Hcont Hc]]]".
    rewrite /with_refinement /boolR /rty.
    rewrite bool_decide_decide; simpl.
    destruct decide; last first.
    - iDestruct ("Hcont" with "Hw") as "[Hw %Hr]".
      iSpecialize ("Hc" with "Hv Hw").
      iApply wp_binop_det_pure; last first.
      { iNext. iExists _. iFrame. iApply vtr_bool. }
      intros σ u. rewrite eval_bin_op_val_to_loc /NULL; eauto with quiver_logic.
      rewrite eval_bin_op_ptr_cmp //; eauto using heap_loc_eq_NULL_NULL.
      rewrite val_of_Z_bool. naive_solver.
    - iDestruct ("Hcont" with "Hw") as "[Hw #Hloc]".
      iSpecialize ("Hc" with "Hv Hw").
      iDestruct "Hloc" as "[%l [-> Hloc]]".
      iApply wp_binop_det; iIntros (σ) "SC".
      iPoseProof (loc_in_bounds_to_heap_loc_in_bounds with "Hloc SC") as "%".
      iMod (fupd_mask_subseteq ∅) as "Hcont"; first set_solver.
      iModIntro. iSplit; last first.
      { iNext. iMod "Hcont". iModIntro. iFrame.
        iExists _. iFrame. iApply vtr_bool. }
      iPureIntro.
      intros w. rewrite eval_bin_op_val_to_loc /NULL; eauto with quiver_logic.
      rewrite eval_bin_op_ptr_cmp //; last rewrite heap_loc_eq_symmetric; eauto using heap_loc_eq_alloc_NULL.
      rewrite val_of_Z_bool; naive_solver.
  Qed.

  Lemma type_null_check_type_binop_null_eq v w A it Φ :
    type_null_check w A (λ φ B, abd_assume CTX (v ◁ᵥ nullT) (abd_assume CTX (w ◁ᵥ B) (Φ (b2v (bool_decide (¬ φ)) it) ((¬ φ)%DP @ boolR it)%RT))) ⊢ type_bin_op (EqOp it) PtrOp PtrOp v nullT w A Φ.
  Proof.
    rewrite /type_bin_op /type_null_check /abd_assume /rwp. iIntros "HΦ #Hv Hw".
    iPoseProof (nullT_inv with "Hv") as "%Heq".
    iDestruct "HΦ" as "[%φ [%B [Hcont Hc]]]".
    rewrite /with_refinement /boolR /rty.
    rewrite bool_decide_decide; simpl.
    destruct decide; last first.
    - iDestruct ("Hcont" with "Hw") as "[Hw %Hr]".
      iSpecialize ("Hc" with "Hv Hw").
      iApply wp_binop_det_pure; last first.
      { iNext. iExists _. iFrame. iApply vtr_bool. }
      intros σ u. rewrite eval_bin_op_val_to_loc /NULL; eauto with quiver_logic.
      rewrite eval_bin_op_ptr_cmp //; eauto using heap_loc_eq_NULL_NULL.
      rewrite val_of_Z_bool. destruct decide; naive_solver.
    - iDestruct ("Hcont" with "Hw") as "[Hw #Hloc]".
      iSpecialize ("Hc" with "Hv Hw").
      iDestruct "Hloc" as "[%l [-> Hloc]]".
      iApply wp_binop_det; iIntros (σ) "SC".
      iPoseProof (loc_in_bounds_to_heap_loc_in_bounds with "Hloc SC") as "%".
      iMod (fupd_mask_subseteq ∅) as "Hcont"; first set_solver.
      iModIntro. iSplit; last first.
      { iNext. iMod "Hcont". iModIntro. iFrame.
        iExists _. iFrame. iApply vtr_bool. }
      iPureIntro.
      intros w. rewrite eval_bin_op_val_to_loc /NULL; eauto with quiver_logic.
      rewrite eval_bin_op_ptr_cmp //; last rewrite heap_loc_eq_symmetric; eauto using heap_loc_eq_alloc_NULL.
      rewrite val_of_Z_bool; destruct decide; naive_solver.
  Qed.


End pointer_rules.
Global Typeclasses Opaque type_null_check type_move_all.




Section fill_placeholder_type.
  Context `{!refinedcG Σ}.

  Inductive fill_mode : Type := AlwaysCopy | NoCopy | CopyIntBool.

  Definition fill_placeholder_type (fm: fill_mode) x A Δ B Ξ : Prop :=
    (x ◁ₓ A ∗ [∗] Δ) ⊢ (x ◁ₓ B ∗ [∗] Ξ).

  Lemma fill_placeholder_type_value_val_copy x ot v A Δ Δ' :
    FindAtom DEEP Δ (v ◁ᵥ A) Δ' →
    Copy A →
    fill_placeholder_type AlwaysCopy x (valueT v ot) Δ A Δ.
  Proof.
    rewrite /fill_placeholder_type. intros Hfind Hcp.
    rewrite Hfind. iIntros "(Hx & #Hv & $)".
    by iDestruct (valueT_elim_xtr A with "Hx Hv") as "$".
  Qed.

  Lemma fill_placeholder_type_value_val_copy_int x (it: int_type) v A Δ Δ' :
    FindAtom DEEP Δ (v ◁ᵥ A) Δ' →
    Copy A →
    fill_placeholder_type CopyIntBool x (valueT v (ly_size it)) Δ A Δ.
  Proof.
    apply fill_placeholder_type_value_val_copy.
  Qed.

  Lemma fill_placeholder_type_value_val_no_copy fm x ot v A Δ Δ' :
    FindAtom DEEP Δ (v ◁ᵥ A) Δ' →
    fill_placeholder_type fm x (valueT v ot) Δ A Δ'.
  Proof.
    rewrite /fill_placeholder_type. intros Hfind.
    rewrite Hfind. iIntros "(Hx & Hv & $)".
    by iDestruct (valueT_elim_xtr A with "Hx Hv") as "$".
  Qed.

  Lemma fill_placeholder_type_value_loc fm x (l: loc) A Δ Δ' :
    FindAtom DEEP Δ (l ◁ₗ A) Δ' →
    fill_placeholder_type fm x (valueT l ptr_size) Δ (ownT l A) Δ'.
  Proof.
    rewrite /fill_placeholder_type. intros Hfind.
    rewrite Hfind. iIntros "(Hx & Hv & $)".
    rewrite -value_own_xtr; iFrame.
  Qed.

  Lemma fill_placeholder_type_place fm (l r: loc) A Δ Δ' :
    FindAtom DEEP Δ (l ◁ₗ A) Δ' →
    fill_placeholder_type fm (LOC r) (placeT l) Δ A Δ'.
  Proof.
    rewrite /fill_placeholder_type. intros Hfind.
    rewrite Hfind. iIntros "(Hx & Hv & $)".
    iApply placeT_merge. iFrame.
  Qed.

End fill_placeholder_type.

Existing Class fill_placeholder_type.
Global Hint Mode fill_placeholder_type - - + ! ! - - - : typeclass_instances.

Global Existing Instance fill_placeholder_type_value_val_copy | 1.
Global Existing Instance fill_placeholder_type_value_val_copy_int | 1.
Global Existing Instance fill_placeholder_type_value_val_no_copy | 2.
Global Existing Instance fill_placeholder_type_value_loc | 2.
Global Existing Instance fill_placeholder_type_place | 1.
