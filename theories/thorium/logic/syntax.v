From quiver.base Require Import annotations.
From quiver.thorium.types Require Export types base_types pointers functions arrays refinements.
From quiver.argon.base Require Export judgments.

Section syntax_predicates.
  Context `{!refinedcG Σ}.

  (** * The Syntax Predicates *)

  (** ** Weakest Preconditions *)
  (** r-expression weakest pre *)
  Definition rwp e Ψ : iProp Σ :=
    WP e {{ v, (∃ R, v ◁ᵥ R ∗ Ψ v R)%I }}.

  (** l-expression weakest pre *)
  Definition lwp p Φ : iProp Σ :=
    WP p {{ v, (∃ (L: type Σ) (l: loc), ⌜(v: val) = l⌝ ∗ l ◁ₗ L ∗ Φ l L)%I }}.

  (** statement weakest pre *)
  Definition swp (s: stmt) (Q: gmap string stmt) (Φ: val → type Σ → iProp Σ) : iProp Σ :=
    WPs s {{ Q, λ v, (∃ (A: type Σ), v ◁ᵥ A ∗ Φ v A)%I }}.

  (* a label with a precondition attached to it *)
  Definition btr (lb: label) (Q: gmap label stmt) P Φ : iProp Σ :=
    □ (P -∗ swp (Goto lb) Q Φ).


  (* monotonicity of the weakest preconditions *)
  Lemma lwp_wand Φ Φ' p:
     lwp p Φ -∗ (∀ l L, Φ l L -∗ Φ' l L) -∗ lwp p Φ'.
  Proof.
    rewrite /lwp. iIntros "Hswp Hmon". iApply (wp_wand with "Hswp").
    iIntros (v) "[%L [%l [Heq [Hl HΦ]]]]". iExists _, _. iFrame.
    iApply "Hmon". done.
  Qed.

  Lemma rwp_wand Ψ Ψ' e:
     rwp e Ψ -∗ (∀ v R, Ψ v R -∗ Ψ' v R) -∗ rwp e Ψ'.
  Proof.
    rewrite /rwp. iIntros "Hswp Hmon". iApply (wp_wand with "Hswp").
    iIntros (v) "[%R  [Hl HΦ]]". iExists _. iFrame.
    iApply "Hmon". done.
  Qed.

  Lemma swp_wand Φ Ψ s Q:
     swp s Q Φ -∗ (∀ v A, Φ v A -∗ Ψ v A) -∗ swp s Q Ψ.
  Proof.
    rewrite /swp. iIntros "Hswp Hmon". iApply (wps_wand with "Hswp").
    iIntros (v) "[%A [Hv HΦ]]". iExists _. iFrame.
    iApply "Hmon". done.
  Qed.

  (** Derived Expressions *)
  Definition bool_eta ot e it : expr := (IfE ot e (i2v 1 it) (i2v 0 it)).

  (** ** Syntax Predicates *)
  (** For each syntactic construct, we have a different syntax predicate.
      These syntax predicates correspond to typing assignments for the
      respective syntactic construct ones its arguments have been evaluated
      to values. *)

  (* values *)
  Definition type_loc (l: loc) (Φ: loc → type Σ → iProp Σ) : iProp Σ :=
    ∃ A, l ◁ₗ A ∗ Φ l A.

  Definition type_val (v: val) (Φ: val → type Σ → iProp Σ) : iProp Σ :=
    ∃ A, v ◁ᵥ A ∗ Φ v A.

  Definition type_loc_or_val (lv: loc_or_val) (Φ: loc_or_val → type Σ → iProp Σ) : iProp Σ :=
    ∃ A, lv ◁ₓ A ∗ Φ lv A.

  (* we can use the following to cast a value to a type *)
  Definition type_val_to_loc (v: val) (A: type Σ) (P: loc → type Σ → iProp Σ) : iProp Σ :=
    v ◁ᵥ A -∗ ∃ (l: loc) (C: type Σ), ⌜v = l⌝ ∗ l ◁ₗ C ∗ P l C.

  (* move a location to a value *)
  Definition type_move (l: loc) (A: type Σ) (n: Z) (Φ: val → iProp Σ) : iProp Σ :=
    l ◁ₗ A -∗ ∃ v, l ◁ₗ valueT v n ∗ Φ v.

  (* size of a type *)
  Definition type_size (A: type Σ) (Φ: Z → iProp Σ) : iProp Σ :=
    ∃ n, ⌜A `has_size` n⌝ ∗ Φ n.

  (* bounds of a type *)
  Definition type_bounds (A: type Σ) (Φ: Z → iProp Σ) : iProp Σ :=
    ∃ n, ⌜A `has_bounds` n⌝ ∗ Φ n.

  (* instruction to prune the context of redundant assumptions *)
  Definition type_garbage_collect (P: iProp Σ) := P.


  Definition type_project_slice (l: loc) (i: Z) (li: Z) (As: list (Z * Z * type Σ))
    (Φ: Z → Z → type Σ → list (Z * Z * type Σ) → list (Z * Z * type Σ) → iProp Σ) : iProp Σ :=
    ∀ len L R, l ◁ₗ slicesT len (L ++ As ++ R) -∗
    ∃ i' li' A L' R', ⌜0 ≤ i' ≤ i⌝ ∗ ⌜0 ≤ li'⌝ ∗ ⌜i + li ≤ i' + li'⌝ ∗
      l ◁ₗ slicesT len (L ++ L' ++ (i', li', placeT (l +ₗ i')) :: R' ++ R) ∗
      (l +ₗ i') ◁ₗ A ∗ Φ i' li' A L' R'.


  (* a wrapper for closing a predicate transformer with wands *)
  Definition type_mono {X: Type} (Φ: (X → iProp Σ) → iProp Σ) : ((X → iProp Σ) → iProp Σ) :=
    λ P, (∃ Q, (∀ x, Q x -∗ P x) ∗ Φ Q)%I.

  Definition type_if_cond (v: val) (A: type Σ) (ot: op_type) (Φ: dProp → iProp Σ) : iProp Σ :=
    v ◁ᵥ A -∗
    match ot with
    | PtrOp => ∃ (l: loc) (φ: dProp), ⌜val_to_loc v = Some l⌝ ∗ ⌜(φ ↔ l ≠ NULL_loc)⌝ ∗ wp_if_precond l ∗ Φ φ
    | IntOp it => ∃ (z: Z) (φ: dProp), ⌜val_to_Z v it = Some z⌝ ∗ ⌜(φ ↔ z ≠ 0)⌝ ∗ Φ φ
    | BoolOp => ∃ (b: bool) (φ: dProp), ⌜val_to_bool v = Some b⌝ ∗ ⌜(φ ↔ b = true)⌝ ∗ Φ φ
    | _ => False
    end.

  (* r-expressions *)
  Definition type_use l L ot Ψ : iProp Σ :=
    l ◁ₗ L -∗ rwp (use{ot, Na1Ord, false} l)%E Ψ.

  Definition type_addr_of l L Ψ : iProp Σ :=
    l ◁ₗ L -∗ rwp (& l)%E Ψ.

  Definition type_bin_op op ot1 ot2 v1 R1 v2 R2 Ψ : iProp Σ :=
    v1 ◁ᵥ R1 -∗ v2 ◁ᵥ R2 -∗ rwp (BinOp op ot1 ot2 v1 v2) Ψ.

  Definition type_un_op op ot v R Ψ : iProp Σ :=
    v ◁ᵥ R -∗ rwp (UnOp op ot v) Ψ.

  Definition type_if_expr ot v A e1 e2 Ψ : iProp Σ :=
    v ◁ᵥ A -∗ rwp (IfE ot v e1 e2) Ψ.

  Definition type_logical_and ot1 ot2 rit v A e Ψ : iProp Σ :=
    v ◁ᵥ A -∗ rwp (v &&{ot1, ot2, rit} e)%E Ψ.

  Definition type_logical_or ot1 ot2 rit v A e Ψ : iProp Σ :=
    v ◁ᵥ A -∗ rwp (v ||{ot1, ot2, rit} e)%E Ψ.

  Definition type_call (f: val) (F: type Σ) (vs: list (val * type Σ)) (es: list expr) Ψ : iProp Σ :=
    ty_own_val_all (vs.*2) (vs.*1) -∗ f ◁ᵥ F -∗ rwp (Call f ((Val <$> vs.*1) ++ es)) Ψ.

  Definition type_annot_expr {B} (b : B) (n : nat) v A Ψ : iProp Σ :=
    v ◁ᵥ A -∗ rwp (AnnotExpr n b v) Ψ.

  Definition type_bool_eta (ot: op_type) v A (it: int_type) Ψ : iProp Σ :=
    v ◁ᵥ A -∗ rwp (bool_eta ot v it) Ψ.

  (* l-expressions *)
  Definition type_load l L Φ : iProp Σ :=
    l ◁ₗ L -∗ lwp (!{PtrOp, Na1Ord, false} l)%E Φ.

  Definition type_get_member (l: loc) (L: type Σ) (s : struct_layout) (m : var_name) Φ : iProp Σ :=
    l ◁ₗ L -∗ lwp (l at{s} m)%E Φ.

  Definition type_at_offset (l: loc) (v: val) (A B: type Σ) (ly : layout) (ot1 ot2: op_type) Φ : iProp Σ :=
    l ◁ₗ A -∗ v ◁ᵥ B -∗ lwp (l at_offset{ly, ot1, ot2} v)%E Φ.

  (* statements *)
  Definition type_goto lb Q Φ : iProp Σ :=
    swp (Goto lb) Q Φ.

  Definition type_loop (lb lb_exit: label) (i: string) Q Φ : iProp Σ :=
    swp (Loop lb lb_exit i) Q Φ.

  Definition type_return v A Q Φ : iProp Σ :=
    v ◁ᵥ A -∗ swp (Return v) Q Φ.

  Definition type_assign l L v R ot s Q Φ: iProp Σ :=
    l ◁ₗ L -∗ v ◁ᵥ R -∗ swp (l <-{ot} v; s)%E Q Φ.

  Definition type_if_stmt v R ot join s1 s2 Q Φ: iProp Σ :=
    v ◁ᵥ R -∗ swp (if{ot, join}: v then s1 else s2)%E Q Φ.

  Definition type_assert v R ot s Q Φ: iProp Σ :=
    v ◁ᵥ R -∗ swp (assert{ot}: v; s)%E Q Φ.

  Definition type_annot_stmt {A} (a : A) s Q Φ: iProp Σ :=
    swp (annot: a; s)%E Q Φ.

  (* freeing locations *)
  Definition type_free_locs (L: list (loc * Z)) (P: iProp Σ) : iProp Σ :=
    ([∗ list] p ∈ L, p.1 ◁ₗ anyT p.2) ∗ P.

  Definition type_dealloc_all (L: list (loc * layout)) (P: iProp Σ) : iProp Σ :=
    ([∗ list] p ∈ L, p.1 ◁ₗ anyT (ly_size p.2)) ∗ P.

  Definition type_unwind v A Φ : iProp Σ :=
    v ◁ᵥ A -∗ ∃ B, v ◁ᵥ B ∗ Φ B.

  (* basic goal for continutations *)
  Definition type_cont (Φ: type Σ → iProp Σ) (A: type Σ) : iProp Σ :=
    (∀ v, v ◁ᵥ A -∗ ∃ B, v ◁ᵥ B ∗ Φ B)%I.

  (* basic goal for continuations that are the function post *)
  Definition type_post (Φ: type Σ → iProp Σ) (A: type Σ) :=
    type_cont Φ A.

  (** ** The Syntax Rules *)
  (** the constant rules *)
  Lemma lwp_loc (l: loc) Φ:
    type_loc l Φ ⊢ lwp l Φ.
  Proof.
    rewrite /lwp /type_loc. iIntros "[%A [Hl HΦ]]".
    iApply wp_value.
    iExists _, _. iFrame. done.
  Qed.

  Lemma type_loc_intro l Φ A :
    l ◁ₗ A -∗ Φ l A -∗ type_loc l Φ.
  Proof.
    iIntros "Hl HΦ". iExists _. iFrame.
  Qed.

  Lemma type_loc_missing l Φ :
    (∃ A, l ◁ₗ A ∗ Φ l A) ⊢ type_loc l Φ.
  Proof. done. Qed.

  Lemma rwp_val v Ψ:
    type_val v Ψ ⊢ rwp v Ψ.
  Proof.
    rewrite /rwp /type_val. iIntros "[%A [Hv HΦ]]".
    iApply wp_value. iExists _. iFrame.
  Qed.

  Lemma type_val_intro v Φ A :
    v ◁ᵥ A -∗ Φ v A -∗ type_val v Φ.
  Proof.
    iIntros "Hl HΦ". iExists _. iFrame.
  Qed.

  Lemma type_val_b2v (b: bool) it Φ:
    (∀ v, Φ v ((if b then True%DP else False%DP) @ boolR it)%RT)%I ⊢ type_val (b2v b it) Φ.
  Proof.
    iIntros "Hcont". rewrite /type_val.
    iExists _. iSplitR; last iApply "Hcont".
    destruct b; iApply vtr_bool.
  Qed.

  Lemma type_loc_find_val (l: loc) A Φ:
    (l ◁ᵥ A ∗ type_val_to_loc l A Φ) ⊢
    type_loc l Φ.
  Proof.
    rewrite /type_loc /type_val_to_loc.
    iIntros "[Hv Hw]".
    iDestruct ("Hw" with "Hv") as "(%r & %D & %Heq & Hl & HΦ)".
    simplify_eq. iExists _. iFrame.
  Qed.

  Lemma type_val_missing v Φ :
    (∃ A, v ◁ᵥ A ∗ Φ v A) ⊢ type_val v Φ.
  Proof. done. Qed.

  (* TYPE SIZE *)
  Lemma type_size_missing A Φ:
    (∃ n, ⌜A `has_size` n⌝ ∗ Φ n) ⊢ type_size A Φ.
  Proof. done. Qed.

  Lemma type_size_intro A n Φ:
    ⌜A `has_size` n⌝ ∗ Φ n ⊢ type_size A Φ.
  Proof. iIntros "H". iExists _. iFrame. Qed.

  (* TYPE bounds *)
  Lemma type_bounds_missing A Φ:
    (∃ n, ⌜A `has_bounds` n⌝ ∗ Φ n) ⊢ type_bounds A Φ.
  Proof. done. Qed.

  Lemma type_bounds_intro A n Φ:
    ⌜A `has_bounds` n⌝ ∗ Φ n ⊢ type_bounds A Φ.
  Proof. iIntros "H". iExists _. iFrame. Qed.

  (* TYPE CONT *)
  Lemma type_cont_intro Φ A :
    Φ A ⊢ type_cont Φ A.
  Proof.
    rewrite /type_cont. iIntros "Φ".
    iIntros (v) "Hv". iExists A. iFrame.
  Qed.

  (* TYPE POST *)
  Lemma type_post_intro Φ A :
    type_cont Φ A ⊢ type_post Φ A.
  Proof.
    rewrite /type_post //.
  Qed.

  (** the bind rules *)
  (* l-expressions *)
  Lemma lwp_load (p: expr) Φ:
    lwp p (λ l L, type_load l L Φ) ⊢ lwp (!{PtrOp, Na1Ord, false} p)%E Φ.
  Proof.
    rewrite /lwp. iIntros "Hp".
    wp_bind. iApply (wp_wand with "Hp").
    iIntros (v) "Hv". iDestruct "Hv" as (A l ->) "[Hl Hr]".
    by iApply "Hr".
  Qed.

  Lemma lwp_get_member (p: expr) s m Φ:
    lwp p (λ l L, type_get_member l L s m Φ) ⊢ lwp (p at{s} m)%E Φ.
  Proof.
    rewrite /lwp. iIntros "Hp".
    wp_bind. iApply (wp_wand with "Hp").
    iIntros (v) "Hv". iDestruct "Hv" as (A l ->) "[Hl Hr]".
    by iApply "Hr".
  Qed.

  Lemma lwp_at_offset (p e: expr) (ly: layout) (ot1 ot2: op_type) Φ:
    rwp e (λ v B, lwp p (λ l A, type_at_offset l v A B ly ot1 ot2 Φ)) ⊢ lwp (p at_offset{ly, ot1, ot2} e)%E Φ.
  Proof.
    rewrite /lwp /rwp. iIntros "He".
    wp_bind. iApply (wp_wand with "He").
    iIntros (v) "Hv". iDestruct "Hv" as (A) "[Hv Hp]".
    wp_bind. iApply (wp_wand with "Hp").
    iIntros (w) "[%B [%l [-> [Hl Hoff]]]]".
    iApply ("Hoff" with "Hl Hv").
  Qed.

  (* r-expressions *)
  Lemma rwp_addr_of p Ψ:
    lwp p (λ l L, type_addr_of l L Ψ) ⊢ rwp (&p)%E Ψ.
  Proof.
    rewrite /lwp /rwp. iIntros "Hlwp". wp_bind.
    rewrite /AddrOf. iApply (wp_wand with "Hlwp").
    iIntros (v). iDestruct 1 as (L l ->) "[Hl Hpost]".
    by iApply ("Hpost" with "Hl").
  Qed.

  Lemma rwp_use p Ψ ot:
    lwp p (λ l L, type_use l L ot Ψ) ⊢ rwp (use{ot, Na1Ord, false} p)%E Ψ.
  Proof.
    rewrite /lwp /rwp. iIntros "Hlwp".
    rewrite /Use. wp_bind.
    iApply (wp_wand with "Hlwp").
    iIntros (v). iDestruct 1 as (L l ->) "[Hl Hpost]".
    iApply ("Hpost" with "Hl").
  Qed.

  Lemma rwp_binop Ψ op ot1 ot2 e1 e2:
    rwp e1 (λ v1 R1, rwp e2 (λ v2 R2, type_bin_op op ot1 ot2 v1 R1 v2 R2 Ψ)) ⊢
    rwp (BinOp op ot1 ot2 e1 e2)%E Ψ.
  Proof.
    rewrite /rwp. iIntros "Hwp".
    wp_bind. iApply (wp_wand with "Hwp").
    iIntros (v1). iDestruct 1 as (R1) "[Hv1 Hwp]".
    wp_bind. iApply (wp_wand with "Hwp").
    iIntros (v2). iDestruct 1 as (R2) "[Hv2 Hwp]".
    iApply ("Hwp" with "Hv1 Hv2").
  Qed.

  Lemma rwp_unop Ψ op ot e:
    rwp e (λ v R, type_un_op op ot v R Ψ) ⊢
    rwp (UnOp op ot e)%E Ψ.
  Proof.
    rewrite /rwp. iIntros "Hwp".
    wp_bind. iApply (wp_wand with "Hwp").
    iIntros (v). iDestruct 1 as (R) "[Hv1 Hwp]".
    by iApply "Hwp".
  Qed.

  Lemma rwp_if_expr Ψ ot e e1 e2:
    rwp e (λ v R, type_if_expr ot v R e1 e2 Ψ) ⊢
    rwp (IfE ot e e1 e2)%E Ψ.
  Proof.
    rewrite /rwp. iIntros "Hwp".
    wp_bind. iApply (wp_wand with "Hwp").
    iIntros (v). iDestruct 1 as (R) "[Hv1 Hwp]".
    by iApply "Hwp".
  Qed.

  Lemma rwp_logical_and Ψ ot1 ot2 rit e1 e2:
    rwp e1 (λ v R, type_logical_and ot1 ot2 rit v R e2 Ψ) ⊢
    rwp (e1 &&{ot1, ot2, rit} e2)%E Ψ.
  Proof.
    rewrite /rwp /LogicalAnd. iIntros "Hwp".
    wp_bind. iApply (wp_wand with "Hwp").
    iIntros (v). iDestruct 1 as (R) "[Hv1 Hwp]".
    rewrite /type_logical_and /rwp /LogicalAnd.
    by iApply "Hwp".
  Qed.

  Lemma rwp_logical_or Ψ ot1 ot2 rit e1 e2:
    rwp e1 (λ v R, type_logical_or ot1 ot2 rit v R e2 Ψ) ⊢
    rwp (e1 ||{ot1, ot2, rit} e2)%E Ψ.
  Proof.
    rewrite /rwp /LogicalOr. iIntros "Hwp".
    wp_bind. iApply (wp_wand with "Hwp").
    iIntros (v). iDestruct 1 as (R) "[Hv1 Hwp]".
    rewrite /type_logical_or /rwp /LogicalOr.
    by iApply "Hwp".
  Qed.

  Lemma rwp_call e es Ψ :
    rwp e (λ f F, type_call f F nil es Ψ) ⊢ rwp (Call e es) Ψ.
  Proof.
    iIntros "Hl". rewrite /rwp /wp /wp_expr_wp.
    assert (coerce_rtexpr (Call e es) = fill [ExprCtx $ CallLCtx (coerce_rtexpr <$> es)] (coerce_rtexpr e)) as -> by done.
    iApply wp_bind. iApply (wp_wand with "Hl").
    iIntros (f) "(%R & Hf & Hcall)". simpl.
    rewrite /type_call /ty_own_val_all /=.
    iSpecialize ("Hcall" with "[//] Hf").
    simpl. iApply "Hcall".
  Qed.

  Lemma wp_bind_iter n e Φ :
    WP e {{v, WP (Nat.iter n SkipE (Val v)) {{ Φ }} }} -∗
    WP (Nat.iter n SkipE e) {{ Φ }}.
  Proof.
    iInduction n as [ | n ] "IH" forall (Φ); simpl.
    - iIntros "Hw". iApply (wp_bind id). done.
    - iIntros "Hw".
      wp_bind.
      iApply "IH". iApply (wp_wand with "Hw").
      iIntros (v) "Hw".
      iApply (wp_bind_inv (λ e, lang_fill_item (ExprCtx SkipECtx) e)).
      done.
  Qed.

  Lemma rwp_annot_expr n {B} (b : B) e Ψ :
    rwp e (λ v R, type_annot_expr b n v R Ψ) ⊢ rwp (AnnotExpr n b e) Ψ.
  Proof.
    rewrite /rwp /type_annot_expr /AnnotExpr. iIntros "Hwp".
    iApply wp_bind_iter.
    iApply (wp_wand with "Hwp"). iIntros (v) "(%R & Hv & Ha)".
    iApply ("Ha" with "Hv").
  Qed.

  Lemma rwp_bool_eta ot e it Φ :
    rwp e (λ v A, type_bool_eta ot v A it Φ) ⊢
    rwp (bool_eta ot e it) Φ.
  Proof.
    rewrite /bool_eta.
    rewrite -rwp_if_expr.
    rewrite /type_if_expr /type_bool_eta /bool_eta //.
  Qed.

  (* statements *)
  Lemma swp_return e Q Φ:
    rwp e (λ v A, type_return v A Q Φ) ⊢ swp (Return e)%E Q Φ.
  Proof.
    rewrite /rwp /swp /type_return. iIntros "Hwp".
    wps_bind. iApply (wp_wand with "Hwp").
    iIntros (v) "[%A [Hv Hret]]". iApply "Hret".
    iFrame.
  Qed.

  Lemma swp_goto lb Q Φ:
    type_goto lb Q Φ ⊢ swp (Goto lb)%E Q Φ.
  Proof.
    rewrite /swp /type_goto //.
  Qed.

  Lemma swp_loop lb lb_exit i Q Φ:
    type_loop lb lb_exit i Q Φ ⊢ swp (Loop lb lb_exit i) Q Φ.
  Proof.
    rewrite /swp /type_goto //.
  Qed.

  Lemma swp_expr (e: expr) s Q Φ:
    rwp e (λ v A, abd_assume CTX (v ◁ᵥ A) (swp s Q Φ)) ⊢ swp (expr: e; s)%E Q Φ.
  Proof.
    rewrite /swp /rwp /abd_assume.
    iIntros "He". wps_bind.
    iApply (wp_wand with "He").
    iIntros (v) "[%A [Hv Hs]]".
    iApply wps_exprs.
    iApply fupd_mask_intro; first set_solver.
    iIntros "Hfupd". iNext. iMod "Hfupd". iModIntro.
    iApply ("Hs" with "Hv").
  Qed.

  Lemma swp_assign (p e: expr) s Q Φ ot:
    rwp e (λ v A, lwp p (λ l B, type_assign l B v A ot s Q Φ)) ⊢ swp (p <-{ot} e; s)%E Q Φ.
  Proof.
    rewrite /swp /rwp /lwp.
    iIntros "He". wps_bind.
    iApply (wp_wand with "He").
    iIntros (v) "[%A [Hv Hp]]".
    wps_bind. iApply (wp_wand with "Hp").
    iIntros (w) "[%B [%l [-> [Hl Hcont]]]]".
    iApply ("Hcont" with "Hl Hv").
  Qed.

  Lemma swp_if (e: expr) join s1 s2 Q Φ ot:
    rwp e (λ v A, type_if_stmt v A ot join s1 s2 Q Φ) ⊢ swp (if{ot, join}: e then s1 else s2)%E Q Φ.
  Proof.
    rewrite /type_if_stmt /swp /rwp /lwp.
    iIntros "He". wps_bind.
    iApply (wp_wand with "He").
    iIntros (v) "[%A [Hv Hp]]".
    by iApply "Hp".
  Qed.

  Lemma swp_skip s Q Φ:
    swp s Q Φ ⊢ swp (SkipS s)%E Q Φ.
  Proof.
    rewrite /swp. iIntros "Hs".
    iApply wps_skip.
    iApply fupd_mask_intro; first set_solver.
    iIntros "Hclose". iNext. iMod "Hclose". iModIntro.
    done.
  Qed.

  Lemma swp_assert (e: expr) s Q Φ ot:
    rwp e (λ v A, type_assert v A ot s Q Φ) ⊢ swp (assert{ot}: e; s)%E Q Φ.
  Proof.
    rewrite /swp /rwp.
    iIntros "He". wps_bind.
    iApply (wp_wand with "He").
    iIntros (v) "[%A [Hv Hp]]".
    by iApply "Hp".
  Qed.

  Lemma swp_annot_stmt {A} (a : A) s Q Φ:
    swp s Q Φ ⊢ swp (annot: a; s)%E Q Φ.
  Proof.
    rewrite /swp. iIntros "HP".
    iApply wps_annot. iApply step_fupdN_intro; done.
  Qed.
End syntax_predicates.


Notation "lb '◁ᵦ' P ; Q ; Φ" := (btr lb Q P Φ) (at level 60, Q at next level, P at next level, Φ at next level).

(* values and locations *)
Global Typeclasses Opaque type_loc type_val type_if_cond type_bool_eta type_loc_or_val type_move type_size type_bounds type_val_to_loc type_garbage_collect type_mono.
Global Typeclasses Opaque type_free_locs type_dealloc_all type_project_slice.
(* l-expressions *)
Global Typeclasses Opaque type_get_member type_at_offset type_load.
(* r-expressions *)
Global Typeclasses Opaque type_use type_addr_of type_bin_op type_un_op type_if_expr type_logical_and type_logical_or type_call.
(* statements *)
Global Typeclasses Opaque type_return type_goto type_loop type_assign type_if_stmt type_assert type_annot_stmt.
(* auxiliary judgments *)
Global Typeclasses Opaque btr type_unwind type_post type_cont.

Global Typeclasses Opaque bool_eta.
Global Arguments bool_eta ot e : simpl never.
