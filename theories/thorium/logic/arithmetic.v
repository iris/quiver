From caesium Require Import lang lifting.
From quiver.thorium.types Require Import types base_types refinements.
From quiver.thorium.logic Require Import syntax.


Section arithmetic_rules.
  Context {Σ: gFunctors} `{!refinedcG Σ}.

  (* Once we have resolved the typing constraints,
     we move to [type_bin_op_force] and [type_un_op_force],
     which are the actual rules for the binary and unary operations. *)

  Definition type_bin_op_force op ot1 ot2 v1 A1 v2 A2 Φ : iProp Σ :=
    type_bin_op op ot1 ot2 v1 A1 v2 A2 Φ.

  Definition type_un_op_force op ot v A Φ : iProp Σ :=
    type_un_op op ot v A Φ.


  (* integer arithmetic rules *)
  Lemma type_bin_op_int_arith op it n1 n2 n3 v1 v2 Ψ:
    int_arithop_result it n1 n2 op = Some n3 →
    int_arithop_sidecond it n1 n2 n3 op →
    Ψ (i2v n3 it) (intT it n3) ⊢ type_bin_op_force op (IntOp it) (IntOp it) v1 (intT it n1) v2 (intT it n2) Ψ.
  Proof.
    intros Hcomp Hside. rewrite /type_bin_op_force /type_bin_op /rwp.
    iIntros "HΨ #Hv1 #Hv2".
    iPoseProof (vtr_int_inversion with "Hv1") as "[-> %Hin1]".
    iPoseProof (vtr_int_inversion with "Hv2") as "[-> %Hin2]".
    iApply wp_int_arithop; eauto using i2v_val_to_Z.
    iIntros (v Hval). iNext.
    eapply int_arithop_result_in_range in Hside; eauto.
    rewrite val_of_Z_i2v in Hval; last done.
    injection Hval as <-.
    iExists _. iFrame.
    iApply vtr_int. done.
  Qed.

  Lemma type_bin_op_int_comp op it1 it2 n1 n2 b v1 v2 Ψ :
    eval_bin_op_relII op n1 n2 = Some (b, it2) →
    Ψ (b2v b it2) (boolT it2 b) ⊢ type_bin_op_force op (IntOp it1) (IntOp it1) v1 (intT it1 n1) v2 (intT it1 n2) Ψ.
  Proof.
    intros Hcomp. rewrite /type_bin_op_force /type_bin_op /rwp.
    iIntros "HΨ #Hv1 #Hv2".
    iPoseProof (vtr_int_inversion with "Hv1") as "[-> %Hin1]".
    iPoseProof (vtr_int_inversion with "Hv2") as "[-> %Hin2]".
    iApply wp_binop_det_pure.
    { intros ??. by eapply eval_bin_op_relII_iff. }
    iNext.
    iExists _. iFrame.
    iApply vtr_bool.
  Qed.

  Lemma type_un_op_cast_int_int it1 it2 w n Ψ :
    (n ∈ it1 → n ∈ it2) →
    Ψ (i2v n it2) (intT it2 n) ⊢ type_un_op_force (CastOp (IntOp it2)) (IntOp it1) w (intT it1 n) Ψ.
  Proof.
    rewrite /type_un_op_force /type_un_op /rwp.
    iIntros (Hran) "HΨ #Hv".
    iPoseProof (vtr_int_inversion with "Hv") as "[-> %Hin]".
    eapply Hran in Hin as Hpr.
    eapply (val_of_Z_is_Some (val_to_byte_prov _)) in Hpr as [v Hpr].
    iApply wp_cast_int; eauto with quiver_logic.
    rewrite val_to_byte_prov_i2v in Hpr.
    i2v_simpl.
    iNext. iExists _. iFrame.
    by iApply vtr_int.
  Qed.

  Lemma type_un_op_cast_bool_int it w n Ψ :
    Ψ (b2v (bool_decide (n ≠ 0)%DP) u8) ((n ≠ 0)%DP @ boolR u8)%RT
    ⊢ type_un_op_force (CastOp BoolOp) (IntOp it) w (intT it n) Ψ.
  Proof.
    rewrite /type_un_op_force /type_un_op /rwp.
    iIntros "HΨ #Hv".
    iPoseProof (vtr_int_inversion with "Hv") as "[-> %Hin]".
    iApply wp_cast_int_bool; eauto with quiver_logic.
    iNext. iExists _. rewrite val_of_bool_b2v.
    iFrame. iApply vtr_bool.
  Qed.

  Lemma type_un_op_cast_bool_bool it w b Ψ :
    Ψ (b2v b u8) (boolT u8 b)%RT
    ⊢ type_un_op_force (CastOp BoolOp) (IntOp it) w (boolT it b) Ψ.
  Proof.
    rewrite /type_un_op_force /type_un_op /rwp.
    rewrite boolT_eq /boolT_def.
    iIntros "HΨ #Hv".
    iPoseProof (vtr_int_inversion with "Hv") as "[-> %Hin]".
    iApply wp_cast_int_bool; eauto with quiver_logic.
    iNext. rewrite val_of_bool_b2v. destruct b; simpl.
    - iExists _. iFrame. iApply vtr_int. done.
    - iExists _. iFrame. iApply vtr_int. done.
  Qed.

  Lemma type_un_op_neg_int it w n Ψ :
    (n ∈ it → -n ∈ it) →
    Ψ (i2v (-n) it) (intT it (-n)) ⊢ type_un_op_force NegOp (IntOp it) w (intT it n) Ψ.
  Proof.
    rewrite /type_un_op_force /type_un_op /rwp.
    iIntros (Hran) "HΨ #Hv".
    iPoseProof (vtr_int_inversion with "Hv") as "[-> %Hin]".
    iApply wp_neg_int; eauto using val_of_Z_i2v with quiver_logic.
    iNext. iExists _. iFrame. iApply vtr_int. eauto.
  Qed.


  (* logic on booleans *)
  Definition bool_log_op op (φ ψ: dProp) : option dProp :=
    match op with
    | AndOp => Some  ((φ ∧ ψ))%DP
    | OrOp => Some  ((φ ∨ ψ))%DP
    | XorOp => Some  (((φ ∧ ¬ ψ) ∨ (¬ φ ∧ ψ))%DP)
    | _ => None
    end.

  Lemma bool_log_op_int_arithop_result op it φ ψ ξ:
    bool_log_op op φ ψ = Some ξ →
    int_arithop_result it (bool_to_Z (bool_decide φ)) (bool_to_Z (bool_decide ψ)) op = Some (bool_to_Z (bool_decide ξ)).
  Proof.
    destruct op; (discriminate || injection 1).
    all: intros <-; simpl.
    - f_equal. rewrite bool_decide_and.
      destruct (bool_decide φ), (bool_decide ψ); done.
    - f_equal. rewrite bool_decide_or.
      destruct (bool_decide φ), (bool_decide ψ); done.
    - f_equal. rewrite bool_decide_or !bool_decide_and !bool_decide_not.
      destruct (bool_decide φ), (bool_decide ψ); done.
  Qed.

  Lemma bool_log_op_int_arithop_sidecond op it φ ψ ξ:
    bool_log_op op φ ψ = Some ξ →
    int_arithop_sidecond it (bool_to_Z (bool_decide φ)) (bool_to_Z (bool_decide ψ)) (bool_to_Z (bool_decide ξ)) op.
  Proof.
    destruct op; done.
  Qed.

  Lemma type_bin_op_bool_log it op φ ψ ξ v1 v2 Ψ:
    bool_log_op op φ ψ = Some ξ →
    Ψ (b2v (bool_decide ξ) it) (boolT it (bool_decide ξ))
    ⊢ type_bin_op_force op (IntOp it) (IntOp it) v1 (boolT it (bool_decide φ)) v2 (boolT it (bool_decide ψ)) Ψ.
  Proof.
    intros Hcomp. rewrite /type_bin_op_force /type_bin_op /rwp.
    iIntros "HΨ #Hv1 #Hv2".
    iPoseProof (vtr_bool_inversion with "Hv1") as "->".
    iPoseProof (vtr_bool_inversion with "Hv2") as "->".
    rewrite /b2v.
    iApply wp_int_arithop; eauto using bool_log_op_int_arithop_result, bool_log_op_int_arithop_sidecond with quiver_logic.
    iIntros (v Hval). iNext.
    iPoseProof (vtr_bool (bool_decide ξ) it) as "Hb".
    rewrite /b2v /i2v Hval /=.
    iExists _. by iFrame.
  Qed.

  Definition bool_log_op_raw op (φ ψ: bool) : option bool :=
    match op with
    | AndOp => Some  ((φ && ψ))%DP
    | OrOp => Some  ((φ || ψ))%DP
    | XorOp => Some  (((φ && negb ψ)|| (negb φ && ψ))%DP)
    | _ => None
    end.

  Lemma bool_log_op_raw_int_arithop_result op it b1 b2 b:
    bool_log_op_raw op b1 b2 = Some b →
    int_arithop_result it (bool_to_Z b1) (bool_to_Z b2) op = Some (bool_to_Z b).
  Proof.
    destruct op; (discriminate || injection 1).
    all: intros <-; simpl.
    all: f_equal; destruct b1, b2; done.
  Qed.

  Lemma bool_log_op_raw_int_arithop_sidecond op it b1 b2 b:
    bool_log_op_raw op b1 b2 = Some b →
    int_arithop_sidecond it (bool_to_Z b1) (bool_to_Z b2) (bool_to_Z b) op.
  Proof.
    destruct op; done.
  Qed.

  Lemma type_bin_op_bool_log_raw it op b1 b2 b v1 v2 Ψ:
    bool_log_op_raw op b1 b2 = Some b →
    Ψ (b2v b it) (boolT it b)
    ⊢ type_bin_op_force op (IntOp it) (IntOp it) v1 (boolT it b1) v2 (boolT it b2) Ψ.
  Proof.
    intros Hcomp. rewrite /type_bin_op_force /type_bin_op /rwp.
    iIntros "HΨ #Hv1 #Hv2".
    iPoseProof (vtr_bool_inversion with "Hv1") as "->".
    iPoseProof (vtr_bool_inversion with "Hv2") as "->".
    rewrite /b2v.
    iApply wp_int_arithop; eauto using bool_log_op_raw_int_arithop_result, bool_log_op_raw_int_arithop_sidecond with quiver_logic.
    iIntros (v Hval). iNext.
    iPoseProof (vtr_bool b it) as "Hb".
    rewrite /b2v /i2v Hval /=.
    iExists _. by iFrame.
  Qed.

End arithmetic_rules.


Global Typeclasses Opaque type_bin_op_force type_un_op_force.
