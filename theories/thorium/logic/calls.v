From quiver.thorium.types Require Import types base_types functions.
From quiver.thorium.logic Require Import syntax assume.
From quiver.argon.base Require Import precond.



Section calling_rules.
  Context `{!refinedcG Σ}.

  Lemma rwp_call_pred_transformer f fn vs Ψ T :
    Forall2 has_layout_val vs (f_args fn).*2 →
    fntbl_entry f fn ∗ ▷ fn_implements_pt fn T ∗ T vs (λ A, ∀ v, Ψ v A) ⊢
    rwp (Call f (Val <$> vs)) Ψ.
  Proof.
    iIntros (Hly) "(#He & #Hp & HT)". iApply wp_call; [eauto with quiver_logic..|].
    iNext. iIntros (lsa lsv) "%Hly2 Hargs Hlocs".
    rewrite -fmap_app. rewrite /fn_implements_pt.
    iDestruct "Hp" as "#Hp". iExists _. iSplitR ""; last first.
    { iIntros (w) "Hw". iExact "Hw". }
    rewrite /fn_cfg. iApply "Hp". iClear "Hp". rewrite /fn_pre.
    eapply Forall2_length in Hly2 as Hlen. iDestruct (big_sepL2_length with "Hargs") as "%Hlen2".
    iExists _, _. iFrame "HT". iSplit; first by rewrite -Hlen.
    iSplitL "Hargs"; last iSplitL "Hlocs".
    - clear Hlen Hlen2. revert Hly Hly2. generalize ((f_args fn).*2) as lys.
      intros lys Hly Hly2. iInduction vs as [|v vs] "IH" forall (lsa lys Hly Hly2).
      + eapply Forall2_nil_inv_l in Hly. revert Hly2. rewrite Hly => Hly2.
        eapply Forall2_nil_inv_r in Hly2. subst. done.
      + eapply Forall2_cons_inv_l in Hly as (ly & lys' & Hv & Hvs & Hly).
        revert Hly2. rewrite Hly => Hly2.
        eapply Forall2_cons_inv_r in Hly2 as (l & lsa' & Hl & Hlsa' & Hly2).
        subst; simpl. iDestruct "Hargs" as "(Hl & Hargs)".
        iSplitL "Hl". { iApply valueT_intro; last done. by eapply has_size_val_has_layout. }
        iApply ("IH" with "[//] [//] Hargs").
    - rewrite ltr_anyT_pts_ly_list big_sepL2_fmap_r //.
    - iIntros (v A) "Hargs Hlocs Hv Hcont".
      rewrite !ltr_anyT_pts_ly_list !big_sepL2_fmap_r //. iFrame.
      iExists _. iFrame. by iApply "Hcont".
  Qed.


  Definition fn_type_pre (ft: fn_type) (vs: list val) (Ψ: val → type Σ → iProp Σ) : iProp Σ :=
    ∃ x: ft.(f_all), ty_own_val_all (ft x).(fp_atys) vs ∗ (ft x).(fp_Pa)
      ∗ (∀ (y : (ft x).(fp_rtype )) v, ((ft x).(fp_fr) y).(fr_R) -∗ Ψ v ((ft x).(fp_fr) y).(fr_rty)).

  Lemma rwp_call_function_type f fn ft vs Ψ :
    ft_wf fn ft →
    fntbl_entry f fn ∗ ▷ typed_function fn ft ∗ fn_type_pre ft vs Ψ ⊢ rwp (Call f (Val <$> vs)) Ψ.
  Proof.
    iIntros (Hwf) "(Hentr & #Hty & Hpre)". rewrite /fn_type_pre.
    iDestruct "Hpre" as "(%x & Hargs & Hpre & HΨ)".
    iPoseProof (ft_wf_layout with "Hargs") as "%Hly"; first done.
    iApply (wp_call with "Hentr"); eauto using val_to_of_loc.
    iNext. iIntros (lsa lsv) "%Hloc Hargs' Hvars".
    rewrite /typed_function. iDestruct "Hty" as "#Hty".
    iSpecialize ("Hty" $! x lsa lsv with "[Hargs Hargs' Hvars $Hpre]").
    - iSplitR "Hvars"; last first.
      { rewrite big_sepL2_fmap_r. iApply (big_sepL2_mono with "Hvars").
        intros ?????; simpl. by rewrite ltr_anyT_pts_ly. }
      iPoseProof (big_sepL2_trans with "Hargs' Hargs") as "Hargs".
      iApply (big_sepL2_mono with "Hargs"); simpl.
      iIntros (k l A Hlook1 Hlook2). iIntros "(%v & Hl & Hv)".
      eapply Forall2_lookup_l in Hloc as (y & Hlook3 & Hlyl); last done.
      specialize (Hwf x).
      eapply Forall2_lookup_r in Hwf as (B & Hlook4 & Hot); last done.
      simplify_eq.
      iApply (ty_move_back with "Hl Hv"); eauto.
    - iExists _. iFrame "Hty".
      rewrite /fn_ret_prop. iIntros (v) "(%y & Hv & Hpost & Hargs & Hlocs)".
      iSplitL "Hargs".
      { rewrite big_sepL2_fmap_r. iApply (big_sepL2_mono with "Hargs").
        intros ?????; simpl. by rewrite ltr_anyT_pts_ly. }
      iSplitL "Hlocs".
      { rewrite big_sepL2_fmap_r. iApply (big_sepL2_mono with "Hlocs").
        intros ?????; simpl. by rewrite ltr_anyT_pts_ly. }
      iExists _. iFrame. by iApply "HΨ".
  Qed.

End calling_rules.





Section function_call_rules.
  Context `{!refinedcG Σ}.

  Implicit Types
    (l: loc) (v: val) (e: expr) (s: stmt)
    (ot: op_type) (A B: type Σ).

  (* evaluating function arguments *)
  Lemma type_call_arg f F vs e es Ψ:
    rwp e (λ v A, type_call f F (vs ++ [(v, A)]) es Ψ) ⊢ type_call f F vs (e :: es) Ψ.
  Proof.
    rewrite /type_call. iIntros "He Hargs Hl". rewrite {3}/rwp /wp /wp_expr_wp.
    assert (coerce_rtexpr (Call f ((Val <$> vs.*1) ++ e :: es)) = fill [ExprCtx $ CallRCtx f vs.*1 (coerce_rtexpr <$> es)] (coerce_rtexpr e)) as ->.
    { simpl. rewrite /coerce_rtexpr //= fmap_app //=. do 3 f_equal. rewrite -!list_fmap_compose //. }
    iApply wp_bind. iApply (wp_wand with "He").
    iIntros (v) "(%R & Hv & Hcall)".
    rewrite !fmap_app /=. rewrite ty_own_val_all_snoc.
    iSpecialize ("Hcall" with "[$Hv $Hargs] Hl").
    rewrite -app_assoc //= -!list_fmap_compose.
    rewrite /rwp {1}/wp /wp_expr_wp //=.
    enough (coerce_rtexpr (Call f ((Val ∘ fst <$> vs) ++ (Val v :: es))) = Expr (RTCall f ((Expr ∘ RTVal ∘ fst <$> vs) ++ of_val v :: (coerce_rtexpr <$> es)))) as -> by done.
    rewrite /coerce_rtexpr ; simpl. f_equal. f_equal.
    rewrite fmap_app //= -list_fmap_compose //=.
  Qed.

  (* traditional function calls *)
  Definition type_call_ft (vs: list (val * type Σ)) ft Ψ : iProp Σ :=
    ty_own_val_all (vs.*2) (vs.*1) -∗ fn_type_pre ft (vs.*1) Ψ.

  Definition type_fn_ret (B: Type) (Post: B → iProp Σ) (Ψ: B → val → iProp Σ): iProp Σ :=
    ∀ (b: B) (v: val), Post b -∗ Ψ b v.

  Lemma type_call_ft_type_call v ft vs Ψ:
    type_call_ft vs ft Ψ ⊢ type_call v (functionT ft) vs nil Ψ.
  Proof.
    rewrite /type_call_ft /type_call.
    iIntros "Hcall Hargs Hv". rewrite functionT_eq /functionT_def //= app_nil_r.
    iDestruct "Hv" as "(%f & %fn & -> & %Hwf & Hentr & #Hty)".
    iApply rwp_call_function_type; first done.  iFrame "Hentr Hty".
    iDestruct ("Hcall" with "Hargs") as "(%x & Hpre & Hargs & HΨ)".
    iExists x. iFrame.
  Qed.

  Lemma type_pre_call vs Φ X ft:
    (abd_assume ASSERT (ty_own_val_all vs.*2 vs.*1)
      (type_pre_post X RECOVER (λ a, ty_own_val_all (ft a).(fp_atys) (vs.*1) ∗ (ft a).(fp_Pa)) (λ a, (∀ (v: val) (y: ((ft a).(fp_rtype))), (abd_assume ASSUME ((ft a).(fp_fr) y).(fr_R)) (Φ v ((ft a).(fp_fr) y).(fr_rty))))))
    ⊢ type_call_ft vs (FT X ft) Φ.
  Proof.
    rewrite /type_pre_post /abd_assume /type_call_ft /=.
    iIntros "Hcont Hall". iSpecialize ("Hcont" with "Hall").
    iDestruct "Hcont" as "[%x Hx]". iDestruct "Hx" as "[Pre Post]".
    iDestruct "Pre" as "[Hty Hpre]".
    iExists x. iFrame. iIntros (v y) "Hr". iApply "Post". iFrame.
  Qed.

  Lemma type_fn_ret_intro (A: Type) Pre Ξ:
    (∀ (a: A) (v: val), Pre a -∗ Ξ a v) ⊢ type_fn_ret A Pre Ξ.
  Proof. reflexivity. Qed.


  (* predicate transformer function calls *)
  Definition type_call_pt (vs: list (val * type Σ)) (lys: list layout) (T: pt Σ) (Ψ: val → type Σ → iProp Σ) : iProp Σ :=
    ⌜Forall2 (λ A ly, A `has_size` (ly_size ly))%type vs.*2 lys⌝ ∗ (ty_own_val_all (vs.*2) (vs.*1) -∗ T vs.*1 (λ A, ∀ v, Ψ v A)%I).

  Lemma type_call_predicate_transformer v vs lys T Ψ:
    type_call_pt vs lys T Ψ
  ⊢ type_call v (fnT lys T) vs nil Ψ.
  Proof.
    rewrite /abd_assume /type_call /type_call_pt. iIntros "[%Hall Hcont] Hty #Hv".
    iDestruct (has_size_has_layout_val with "Hty") as "%Hvals"; first done.
    iDestruct ("Hcont" with "Hty") as "Hcont". rewrite app_nil_r.
    rewrite /fnT. rewrite vtr_existsT_iff. iDestruct "Hv" as "(%fn & Hv)".
    rewrite vtr_existsT_iff. iDestruct "Hv" as "(%f & Hv)".
    rewrite vtr_sepT_iff. iDestruct "Hv" as "(Hv & %Heq & He & #Hp)".
    iDestruct (vtr_valueT_elim with "Hv") as "[-> _]". subst.
    iApply rwp_call_pred_transformer; first done. by iFrame "#".
  Qed.

  Lemma fn_type_call f vs lys Ψ T :
    Forall2 has_layout_val vs lys →
    f ◁ᵥ fnT lys T -∗
    T vs (λ A, ∀ v, Ψ v A) -∗
    rwp (Call f (Val <$> vs)) Ψ.
  Proof.
    iIntros (Hly) "#Hf HT".
    rewrite fnT_elim_strong. iDestruct "Hf" as "(%l & %fn & -> & -> & #Hf & #Himpl)".
    iApply rwp_call_pred_transformer; first done.
    iFrame "#". iFrame.
  Qed.

  (* we need to make sure the argument types have the right size *)
  Definition type_has_sizes (As: list (type Σ)) (lys: list layout) (P: iProp Σ) : iProp Σ :=
    ⌜Forall2 (λ A ly, A `has_size` (ly_size ly)) As lys⌝ ∗ P.


  Lemma type_has_sizes_nil P : P ⊢ type_has_sizes [] [] P.
  Proof.
    iIntros "$". iPureIntro. constructor.
  Qed.

  Lemma type_has_sizes_cons A As ly lys P :
    type_size A (λ n, ⌜n = ly_size ly⌝ ∗ type_has_sizes As lys P) ⊢ type_has_sizes (A :: As) (ly :: lys) P.
  Proof.
    rewrite /type_has_sizes /type_size. iIntros "(%n & %Hsz & -> & %Hall & $)".
    iPureIntro. eauto.
  Qed.

  Definition type_pred_transform {A: Type} (P: A → iProp Σ) (Φ: A) : iProp Σ := P Φ.

  Lemma type_call_pt_intro vs lys T Ψ:
      type_has_sizes vs.*2 lys
    $ abd_assume ASSERT (ty_own_val_all vs.*2 vs.*1)
    $ type_pred_transform (T vs.*1) (λ A, ∀ v, Ψ v A)
  ⊢ type_call_pt vs lys T Ψ.
  Proof.
    rewrite /type_call_pt /type_has_sizes /abd_assume. iIntros "($ & ?)".
    rewrite /type_pred_transform //.
  Qed.

End function_call_rules.

Global Typeclasses Opaque type_has_sizes type_call_ft type_fn_ret type_pred_transform.
