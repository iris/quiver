

From quiver.argon.base Require Import judgments.
From quiver.thorium.types Require Export types base_types pointers combinators functions structs refinements arrays bytes.


Section assume.

  Context `{!refinedcG Σ}.

  Definition type_resolve_duplicate (l: loc) (A B: type Σ) (P: iProp Σ) : iProp Σ :=
    l ◁ₗ A -∗ l ◁ₗ B -∗ P.


  Lemma abd_assume_ltr_typing m l A Q:
    (l ◁ₗ A -∗ Q) ⊢ abd_assume m (l ◁ₗ A) Q.
  Proof. done. Qed.

  Lemma abd_assume_vtr_typing m v A Q :
    (v ◁ᵥ A -∗ Q) ⊢ abd_assume m (v ◁ᵥ A) Q.
  Proof. done. Qed.

  Lemma abd_assume_vtr_own_loc_id m l A Q:
    abd_assume m (l ◁ₗ A) Q
  ⊢ abd_assume m (l ◁ᵥ ownT l A) Q.
  Proof.
    rewrite /abd_assume. iIntros "Hcont Hl".
    iApply "Hcont". rewrite ownT_ty_own_val_own.
    iDestruct "Hl" as "[_ $]".
  Qed.


  (* RESOLVING DUPLICATES *)
  Lemma type_resolve_duplicate_left_offset l A B P:
    (abd_assume CTX (l ◁ₗ A) (abd_assume CTX ((l +ₗ 0) ◁ₗ B) P))
  ⊢ type_resolve_duplicate l A B P.
  Proof.
    rewrite /abd_assume /type_resolve_duplicate.
    rewrite shift_loc_0. iIntros "Hcont Hl1 Hl2".
    by iApply ("Hcont" with "[$] [$]").
  Qed.


  Lemma type_resolve_duplicate_right_offset l A B P:
    (abd_assume CTX ((l +ₗ 0) ◁ₗ A) (abd_assume CTX (l ◁ₗ B) P))
  ⊢ type_resolve_duplicate l A B P.
  Proof.
    rewrite /abd_assume /type_resolve_duplicate.
    rewrite shift_loc_0. iIntros "Hcont Hl1 Hl2".
    by iApply ("Hcont" with "[$] [$]").
  Qed.

  Lemma type_resolve_duplicate_left_drop l A B P:
    (abd_assume CTX (l ◁ₗ B) P)
  ⊢ type_resolve_duplicate l A B P.
  Proof.
    rewrite /abd_assume /type_resolve_duplicate.
    iIntros "Hcont Hl1 Hl2".
    by iApply ("Hcont" with "[$]").
  Qed.


  Lemma type_resolve_duplicate_right_drop l A B P:
    (abd_assume CTX (l ◁ₗ A) P)
  ⊢ type_resolve_duplicate l A B P.
  Proof.
    rewrite /abd_assume /type_resolve_duplicate.
    iIntros "Hcont Hl1 Hl2".
    by iApply ("Hcont" with "[$]").
  Qed.


  Lemma type_resolve_duplicate_left_place l r B P:
    (abd_assume CTX (l ◁ₗ placeT r) (abd_assume CTX (r ◁ₗ B) P))
  ⊢ type_resolve_duplicate l (placeT r) B P.
  Proof.
    rewrite /abd_assume /type_resolve_duplicate.
    iIntros "Hcont Hl1 Hl2".
    iDestruct (placeT_is_eq with "Hl1") as "->".
    by iApply ("Hcont" with "[$] [$]").
  Qed.

  Lemma type_resolve_duplicate_right_place l r A P:
    (abd_assume CTX (l ◁ₗ placeT r) (abd_assume CTX (r ◁ₗ A) P))
  ⊢ type_resolve_duplicate l A (placeT r) P.
  Proof.
    rewrite /abd_assume /type_resolve_duplicate.
    iIntros "Hcont Hl1 Hl2".
    iDestruct (placeT_is_eq with "Hl2") as "->".
    by iApply ("Hcont" with "[$] [$]").
  Qed.



  (* RECYCLING *)
  Definition type_recycle (v: val) (A: type Σ) (Q: iProp Σ) : iProp Σ :=
    v ◁ᵥ A -∗ Q.

  Lemma abd_assume_vtr_recycle m (v: val) (A: type Σ) Q :
    type_recycle v A Q ⊢ abd_assume m (v ◁ᵥ A) Q.
  Proof. done. Qed.

  Lemma type_recycle_exists lv {X: Type} (A: X → type Σ) Q :
    (∀ x, type_recycle lv (A x) Q) ⊢ type_recycle lv (ex x, A x) Q.
  Proof.
    rewrite /type_recycle. rewrite vtr_existsT_iff.
    iIntros "Hcont [%x Hlv]". by iApply "Hcont".
  Qed.

  Lemma type_recycle_sep lv A P Q :
    abd_assume RECYCLE P (type_recycle lv A Q) ⊢ type_recycle lv (A ∗∗ P) Q.
  Proof.
    rewrite /type_recycle /abd_assume. iIntros "Hcont Hlv".
    rewrite vtr_sepT_iff. iDestruct "Hlv" as "[Hlv HP]".
    by iApply ("Hcont" with "HP Hlv").
  Qed.

  Lemma type_recycle_constraint lv A φ Q :
    abd_assume RECYCLE ⌜φ⌝ (type_recycle lv A Q) ⊢ type_recycle lv {A | φ} Q.
  Proof.
    rewrite /type_recycle /abd_assume. iIntros "Hcont Hlv".
    rewrite vtr_constraintT_iff. iDestruct "Hlv" as "[Hlv Hφ]".
    by iApply ("Hcont" with "Hφ Hlv").
  Qed.

  Lemma type_recycle_int v it (n: Z) Q :
    abd_assume RECYCLE ⌜(n ∈ it)%Z⌝ Q ⊢ type_recycle v (int[it] n) Q.
  Proof.
    rewrite /type_recycle /abd_assume. iIntros "Hcont Hlv".
    iDestruct (vtr_int_inversion with "Hlv") as "[_ %Hin]".
    by iApply "Hcont".
  Qed.

  Lemma type_recycle_value v w n Q:
    abd_assume RECYCLE ⌜0 ≤ n⌝ Q ⊢ type_recycle v (value[n] w) Q.
  Proof.
    rewrite /type_recycle /abd_assume.
    iIntros "Hcont Hlv". iDestruct (vtr_valueT_elim with "Hlv") as "[_ %Heq]".
    rewrite Heq. iApply "Hcont". iPureIntro. lia.
  Qed.

  Lemma type_recycle_opt v w Q:
    Q ⊢ type_recycle v (opt w) Q.
  Proof.
    rewrite /type_recycle /abd_assume.
    by iIntros "$ Hlv".
  Qed.

  Lemma type_recycle_zeros v n Q:
    abd_assume RECYCLE ⌜0 ≤ n⌝ Q ⊢ type_recycle v (zerosT n) Q.
  Proof.
    rewrite zerosT_eq /zerosT_def -type_recycle_value //.
  Qed.

  Lemma type_recycle_any v n Q:
    abd_assume RECYCLE ⌜0 ≤ n⌝ Q ⊢ type_recycle v (any[n]) Q.
  Proof.
    rewrite anyT_eq /anyT_def. rewrite -type_recycle_exists.
    iIntros "Hass" (x). rewrite -type_recycle_value //.
  Qed.

  Lemma type_recycle_array v {X} (T: X → type Σ) xs n Q :
    abd_assume RECYCLE ⌜0 ≤ n⌝ Q
  ⊢ type_recycle v (arrayT T n xs) Q.
  Proof.
    rewrite /abd_assume /type_recycle.
    iIntros "Hcont Hv". iApply "Hcont".
    iDestruct (xtr_array_size (VAL v) with "Hv") as "%Hsz".
    iPureIntro. lia.
  Qed.

  Lemma type_recycle_drop v A Q:
    Q ⊢ type_recycle v A Q.
  Proof.
    rewrite /type_recycle. by iIntros "$ _".
  Qed.

  Lemma type_recycle_own v p A Q:
    abd_assume RECYCLE (p ◁ₗ A) Q ⊢ type_recycle v (ownT p A) Q.
  Proof.
    rewrite /type_recycle /abd_assume /= ownT_ty_own_val_own.
    iIntros "Hcont [_ Hp]". by iApply "Hcont".
  Qed.

  Lemma type_recycle_struct v sl As Q:
    abd_assume RECYCLE (ty_own_val_all As (member_vals v sl)) Q ⊢ type_recycle v (structT sl As) Q.
  Proof.
    rewrite /abd_assume /type_recycle /=. iIntros "Hv".
    rewrite struct_focus_ty_own_all_val. iIntros "[Hx _]".
    by iApply "Hv".
  Qed.

  Lemma type_recycle_fallthrough v A P:
    (vty A -∗ P) ⊢ type_recycle v A P.
  Proof.
    rewrite /type_recycle. iIntros "Hcont Hv".
    iApply "Hcont". iExists _. iFrame "Hv".
  Qed.

End assume.

Global Typeclasses Opaque type_recycle type_resolve_duplicate.



(* instead of going for full unification,
   we can sometimes get by by simply "destructing"
   virtual types such as ∃ x. A x, A * P, {A | φ}, ... *)
Section virtual_types.
  Context `{!refinedcG Σ}.

  Definition type_virt (lv: loc_or_val) (A: type Σ) (Φ: type Σ → iProp Σ) : iProp Σ :=
    lv ◁ₓ A -∗ ∃ B, lv ◁ₓ B ∗ Φ B.

  Lemma type_virt_refl lv A Φ:
    Φ A ⊢ type_virt lv A Φ.
  Proof.
    rewrite /type_virt. iIntros "HΦ Hlv". iExists A. iFrame.
  Qed.

  Lemma type_virt_equiv lv A B Φ:
    A ≡ B →
    type_virt lv A Φ ⊢ type_virt lv B Φ.
  Proof.
    rewrite /type_virt. intros Heq. by setoid_rewrite Heq.
  Qed.

  Lemma type_virt_sep lv A P Φ :
    abd_assume CTX P (type_virt lv A Φ) ⊢ type_virt lv (sepT A P) Φ.
  Proof.
    rewrite /type_virt /abd_assume.
    rewrite xtr_sepT_iff. iIntros "HΦ [Hlv HP]".
    iApply ("HΦ" with "HP Hlv").
  Qed.

  Lemma type_virt_constraint lv A φ Φ :
    abd_assume CTX (⌜φ⌝) (type_virt lv A Φ) ⊢ type_virt lv (constraintT A φ) Φ.
  Proof.
    rewrite constraintT_eq /constraintT_def. eapply type_virt_sep.
  Qed.

  Lemma type_virt_exists lv X (A: X → type Σ) Φ :
    (∀ x, type_virt lv (A x) Φ) ⊢ type_virt lv (ex x, A x) Φ.
  Proof.
    rewrite /type_virt. iIntros "HΦ Hlv".
    rewrite xtr_existsT_iff.
    iDestruct "Hlv" as (x) "Hlv".
    iDestruct ("HΦ" $! x with "Hlv") as (B) "[Hlv HΦ]".
    iExists _. iFrame.
  Qed.

  Lemma type_virt_is_ty lv A B Φ:
    A `is_ty` B →
    type_virt lv B Φ ⊢ type_virt lv A Φ.
  Proof.
    rewrite /type_virt. iIntros ([HAB]) "Hcont Hlv".
    destruct lv; simpl; rewrite HAB; by iApply "Hcont".
  Qed.

  Lemma type_virt_moved_copy lv A Φ:
    Copy A →
    type_virt lv A Φ ⊢ type_virt lv (movedT A) Φ.
  Proof.
    rewrite /type_virt. iIntros (Hcopy) "Hcont Hlv".
    rewrite xtr_moved_copy. by iApply "Hcont".
  Qed.

  Lemma type_virt_moved_own lv (p: loc) A Φ:
    type_virt lv (valueT p ptr_size) Φ ⊢ type_virt lv (movedT (ownT p A)) Φ.
  Proof.
    rewrite /type_virt. iIntros "Hcont Hlv".
    rewrite xtr_moved_own. by iApply "Hcont".
  Qed.

  Lemma type_virt_moved_is_ty lv A B Φ:
    A `is_ty` B →
    type_virt lv (movedT B) Φ ⊢ type_virt lv (movedT A) Φ.
  Proof.
    rewrite /type_virt. iIntros ([HAB]) "Hcont Hlv".
    destruct lv; simpl; rewrite HAB; by iApply "Hcont".
  Qed.

End virtual_types.

Global Typeclasses Opaque type_virt.
