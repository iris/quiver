From quiver.base Require Export dprop facts.
From quiver.thorium.types Require Import types base_types refinements structs.
From quiver.thorium.logic Require Import syntax assume pointers.



(* basic controlflow *)
Section controlflow_rules.
Context `{!refinedcG Σ}.

  Definition type_if_cond_force v A ot Φ : iProp Σ :=
    type_if_cond v A ot Φ.

  Definition type_if_expr_prop (φ: dProp) (e1 e2: expr) (Φ: val → type Σ → iProp Σ) :=
    if decide φ then rwp e1 Φ else rwp e2 Φ.

  Definition type_if_prop (φ: dProp) (join: option string) (s1 s2: stmt) (Q: gmap string stmt) (Φ: val → type Σ → iProp Σ) :=
    if decide φ then swp s1 Q Φ else swp s2 Q Φ.

  Definition type_to_bool (v: val) (A: type Σ) (Φ: type Σ → iProp Σ) : iProp Σ :=
    v ◁ᵥ A -∗ ∃ B, v ◁ᵥ B ∗ Φ B.


  (* Loop auxillary definitions *)
  Definition type_break_at (lb: label) (cfg: gmap string stmt) (Φ: val → type Σ → iProp Σ) (P: iProp Σ) : iProp Σ := P.

  Definition type_loop_core (lb: label) (I: iProp Σ) (Q: gmap string stmt) (Φ: val → type Σ → iProp Σ) : iProp Σ :=
    type_goto lb Q Φ.

  Definition type_loop_core_assuming_inv  (lb: label) (I: iProp Σ) (Q: gmap string stmt) (Φ: val → type Σ → iProp Σ) : iProp Σ :=
    I -∗ type_goto lb Q Φ.

  Definition type_loop_core_assuming_inv_ctx  (lb: label) (I: iProp Σ) (Δ: list (iProp Σ)) (Q: gmap string stmt) (Φ: val → type Σ → iProp Σ) : iProp Σ :=
    ([∗] Δ) -∗ I -∗ type_goto lb Q Φ.

  Definition type_loop_fixp (F: iProp Σ → iProp Σ) : iProp Σ :=
    ∃ P, P ∗ □ (P -∗ F P).




  Lemma type_if_cond_wand v A ot (Φ Ψ: dProp → iProp Σ):
    (∀ φ, Ψ φ -∗ Φ φ) ∗ type_if_cond v A ot Ψ ⊢ type_if_cond v A ot Φ.
  Proof.
    rewrite /type_if_cond. iIntros "[Hw Hcond] Hv".
    iSpecialize ("Hcond" with "Hv"). destruct ot; try done.
    - iDestruct "Hcond" as (l φ) "(? & ? & Hφ)". iExists l, φ. iFrame. by iApply "Hw".
    - iDestruct "Hcond" as (z φ) "(? & ? & Hφ)". iExists z, φ. iFrame. by iApply "Hw".
    - iDestruct "Hcond" as (b φ) "(? & ? & ? & Hφ)". iExists b, φ. iFrame. by iApply "Hw".
  Qed.



  (* LABEL PRECONDITIONS *)
  Lemma btr_type_goto lb Q Φ:
    ⊢ lb ◁ᵦ (type_goto lb Q Φ); Q; Φ.
  Proof.
    rewrite /btr /type_goto. iModIntro. iIntros "$".
  Qed.

  Lemma type_goto_btr lb Q P Φ:
    lb ◁ᵦ P; Q; Φ ∗ P ⊢ type_goto lb Q Φ.
  Proof.
    rewrite /btr /type_goto. iIntros "[#HP P]".
    by iApply "HP".
  Qed.


  (* GOTOs *)
  Lemma type_goto_jump Q lb s Φ:
    Q !! lb = Some s →
    swp s Q Φ ⊢ type_goto lb Q Φ.
  Proof.
    rewrite /type_goto /swp. iIntros (Hlook) "Hpost".
    iApply wps_goto; first done. iNext. done.
  Qed.

  Lemma type_goto_checkpoint F s lb Q  Φ:
    Q !! lb = Some s →
    (∀ P, lb ◁ᵦ P; Q; Φ ∗ F P ⊢ swp s Q Φ) →
    (∃ P, P ∗ □ (P -∗ F P)) ⊢ type_goto lb Q Φ.
  Proof.
    rewrite /type_goto /btr.
    iIntros (Hlb Hcont) "Hrec".
    iDestruct "Hrec" as "[%P [P #Hwand]]".
    iLöb as "IH".
    iApply wps_goto; first done. iNext.
    iApply Hcont. iFrame "IH". by iApply "Hwand".
  Qed.

  Lemma type_goto_checkpoint_with_inv I F s lb Q  Φ:
    Q !! lb = Some s →
    (∀ P, lb ◁ᵦ I ∗ P; Q; Φ ∗ F P ⊢ (I -∗ swp s Q Φ)) →
    I ∗ (∃ P, P ∗ □ (P -∗ F P)) ⊢ type_goto lb Q Φ.
  Proof.
    rewrite /type_goto /btr.
    iIntros (Hlb Hcont) "Hrec".
    iDestruct "Hrec" as "[I [%P [P #Hwand]]]".
    iLöb as "IH".
    iApply wps_goto; first done. iNext.
    iApply (Hcont with "[P] I"). iSplit; last first.
    - by iApply "Hwand".
    - iModIntro. iIntros "[P I]". iApply ("IH" with "P I").
  Qed.

  (* FREEING LOCATIONS *)
  Definition type_free_loc_typed (l: loc) (A: type Σ) (n: Z) (P: iProp Σ) : iProp Σ :=
    l ◁ₗ A -∗ l ◁ₗ anyT n ∗ P.

  Definition type_free_locs_typed (L: list loc) (T: list (type Σ)) (LY: list Z) (P: iProp Σ) : iProp Σ :=
    ty_own_all T L -∗ type_free_locs (zip L LY) P.

  Lemma type_free_locs_nil P:
    P ⊢ type_free_locs nil P.
  Proof.
    rewrite /type_free_locs //=. by iIntros "$".
  Qed.

  Lemma type_free_locs_cons l n L P :
    type_loc l (λ r A, type_free_loc_typed r A n (type_free_locs L P)) ⊢ type_free_locs ((l, n)::L) P.
  Proof.
    rewrite /type_free_locs /type_free_loc_typed /type_loc.
    iIntros "[%A [Hl HΦ]]". iDestruct ("HΦ" with "Hl") as "[Hl HΦ]".
    iFrame. iFrame.
  Qed.

  Lemma type_dealloc_all_nil P:
    P ⊢ type_dealloc_all nil P.
  Proof.
    rewrite /type_dealloc_all //=. by iIntros "$".
  Qed.

  Lemma type_dealloc_all_cons l ly L P :
    type_loc l (λ r A, type_free_loc_typed r A (ly_size ly) (type_dealloc_all L P)) ⊢ type_dealloc_all ((l, ly)::L) P.
  Proof.
    rewrite /type_dealloc_all /type_free_loc_typed /type_loc.
    iIntros "[%A [Hl HΦ]]". iDestruct ("HΦ" with "Hl") as "[Hl HΦ]".
    iFrame. iFrame.
  Qed.

  Lemma type_free_loc_typed_has_layout l n A P :
    A `has_size` n →
    (∀ v, abd_assume CTX (v ◁ᵥ A) P) ⊢ type_free_loc_typed l A n P.
  Proof.
    rewrite /type_free_loc_typed /type_loc /abd_assume.
    iIntros (Hlt) "HΦ Hl".
    iPoseProof (xtr_to_any (LOC l) with "Hl") as "[Hv $]"; first done.
    rewrite /vty. iDestruct "Hv" as "[%v Hv]". by iApply "HΦ".
  Qed.

  Lemma type_free_loc_typed_place l r ly P :
    type_loc r (λ r' A, type_free_loc_typed r' A ly (abd_assume CTX (l ◁ₗ placeT r') P)) ⊢
    type_free_loc_typed l (placeT r) ly P.
  Proof.
    rewrite /type_free_loc_typed /type_loc /abd_assume.
    iIntros "[%A [Hr HΦ]] Hl". iDestruct ("HΦ" with "Hr") as "[Hr HΦ]".
    iDestruct (placeT_is_eq with "Hl") as "%Heq". subst.
    iFrame. by iApply "HΦ".
  Qed.

  Lemma type_free_loc_typed_is_ty l A n P :
    ⌜A `has_size` n⌝ ∗ P ⊢ type_free_loc_typed l A n P.
  Proof.
    rewrite /type_free_loc_typed. iIntros "[%Hty HP] Hl". iFrame.
    by iApply cast_to_any_ltr.
  Qed.

  Lemma type_free_loc_typed_virtual l A ly P :
    type_virt (LOC l) A (λ A', type_free_loc_typed l A' ly P) ⊢
    type_free_loc_typed l A ly P.
  Proof.
    rewrite /type_virt/type_free_loc_typed.
    iIntros "HΦ Hl". iDestruct ("HΦ" with "Hl") as (?) "[? HΦ]".
    by iApply "HΦ".
  Qed.


  Lemma member_locs_layouts_zip l sl:
    zip (member_locs l sl) (member_sizes sl) =
    omap (λ '(o, ly), (λ n, (l at{sl}ₗ n, Z.of_nat (ly_size ly))) <$> o) (sl_members sl).
  Proof.
    unfold member_locs, member_sizes, member_layouts.
    induction (sl_members sl) as [| [[n|] ly] Ms IH]; simpl; first done.
    - f_equal. done.
    - done.
  Qed.

  Lemma type_free_loc_typed_struct l T sl P:
    type_free_locs_typed (member_locs l sl) T (member_sizes sl) P ⊢ type_free_loc_typed l (structT sl T) (ly_size sl) P .
  Proof.
    rewrite /type_free_loc_typed. rewrite anyT_structT_equiv_member_layouts.
    rewrite struct_focus. iIntros "Hcont [Hfields Hrest]".
    rewrite /type_free_locs_typed.
    rewrite /ty_own_all. rewrite big_sepL2_fmap_l.
    iSpecialize ("Hcont" with "Hfields"). rewrite /type_free_locs.
    iDestruct "Hcont" as "[Hany $]". iApply "Hrest".
    rewrite member_locs_layouts_zip. rewrite /member_sizes /member_layouts.
    rewrite big_sepL2_fmap_r. clear.
    iInduction (sl_members sl) as [| [[n|] ly] Ls] "IH"; simpl.
    - done.
    - iDestruct "Hany" as "[$ ?]". by iApply "IH".
    - by iApply "IH".
  Qed.

  Lemma type_free_locs_typed_nil P:
    P ⊢ type_free_locs_typed nil nil nil P.
  Proof.
    rewrite /type_free_locs_typed /ty_own_all /type_free_locs. iIntros "Hcont".
    rewrite /=. by iIntros "$".
  Qed.

  Lemma type_free_locs_typed_cons l L ly LY A T P :
    type_free_loc_typed l A ly (type_free_locs_typed L T LY P) ⊢ type_free_locs_typed (l :: L) (A :: T) (ly :: LY) P.
  Proof.
    rewrite /type_free_locs_typed /type_free_locs /type_free_loc_typed /=.
    rewrite ty_own_all_cons.
    iIntros "Hcont [Hl Hown]". iDestruct ("Hcont" with "Hl") as "[$ Hcont]".
    by iApply "Hcont".
  Qed.


  (* RETURN *)
  Lemma type_return_resolve v A Q Φ:
    Φ v A ⊢ type_return v A Q Φ.
  Proof.
    rewrite /type_return. iIntros "HΦ Hv".
    iApply wps_return. iExists _. iFrame.
  Qed.

  (* IF-EXPRESSIONS *)
  Lemma type_if_cond_type_if_expr (v: val) (A: type Σ) e1 e2 ot Φ :
    type_if_cond v A ot (λ φ, type_if_expr_prop φ e1 e2 Φ) ⊢ type_if_expr ot v A e1 e2 Φ.
  Proof.
    rewrite /type_if_expr /type_if_cond. iIntros "Hcont Hv".
    iPoseProof ("Hcont" with "Hv") as "Hcont".
    rewrite /type_if_expr_prop.
    destruct ot; try done.
    - iDestruct "Hcont" as (b φ Hval Hiff) "Hcont".
      iApply wp_if_bool; first done.
      destruct decide, b; naive_solver.
    - iDestruct "Hcont" as (z φ Hval Hiff) "Hcont".
      iApply wp_if_int; first done.
      rewrite bool_decide_decide.
      destruct decide, decide; naive_solver.
    - iDestruct "Hcont" as (l φ Hval Hiff) "[Hw Hcont]".
      iApply (wp_if_ptr with "Hw"); first eauto with quiver_logic.
      rewrite bool_decide_decide.
      destruct decide, decide; naive_solver.
  Qed.

  (* LOGICAL AND and LOGICAL OR *)
  Lemma type_logical_and_type_if_expr v ot1 ot2 rit A e Φ:
    type_if_expr ot1 v A (IfE ot2 e (i2v 1 rit) (i2v 0 rit)) (i2v 0 rit) Φ ⊢ type_logical_and ot1 ot2 rit v A e Φ.
  Proof.
    rewrite /type_if_expr /type_logical_and /LogicalAnd //.
  Qed.

  Lemma type_logical_or_type_if_expr v ot1 ot2 rit A e Φ:
    type_if_expr ot1 v A (i2v 1 rit) (IfE ot2 e (i2v 1 rit) (i2v 0 rit)) Φ ⊢ type_logical_or ot1 ot2 rit v A e Φ.
  Proof.
    rewrite /type_if_expr /type_logical_or /LogicalOr //.
  Qed.

  (* BOOL ETA *)
  Lemma type_bool_eta_type_if_cond ot v A it Φ:
    type_if_cond v A ot (λ φ, ∀ w, Φ w (φ @ boolR it)%RT)%I
  ⊢ type_bool_eta ot v A it Φ.
  Proof.
    rewrite /type_bool_eta /bool_eta.
    iIntros "Hif".
    iPoseProof (type_if_cond_type_if_expr) as "Hent".
    rewrite /type_if_expr. iApply ("Hent" with "[Hif]").
    iApply type_if_cond_wand. iFrame.
    iIntros (φ) "Hcont". rewrite /type_if_expr_prop.
    rewrite /with_refinement /boolR /=. rewrite bool_decide_decide.
    destruct decide.
    - iApply rwp_val. iExists _. iSplitR; last iApply "Hcont".
      iApply vtr_bool.
    - iApply rwp_val. iExists _. iSplitR; last iApply "Hcont".
      iApply vtr_bool.
  Qed.


  (* TYPE BOOL *)
  Lemma type_to_bool_intro v φ it Φ:
    Φ (φ @ boolR it)%RT ⊢ type_to_bool v (φ @ boolR it)%RT Φ.
  Proof.
    rewrite /type_to_bool.
    iIntros "Hcont Hv". iExists _. iFrame.
  Qed.

  Lemma type_to_bool_if_true_l v (φ ψ: dProp) it Φ:
    type_to_bool v ((φ ∨ ψ)%DP @ boolR it)%RT Φ ⊢ type_to_bool v (case φ True%DP ψ @ boolR it)%RT Φ.
  Proof.
    rewrite /type_to_bool /type_if_expr_prop.
    iIntros "Hcont Hv". iApply "Hcont". rewrite boolR_iff_eq //.
    rewrite /case. destruct (decide φ), (decide ψ); naive_solver.
  Qed.

  Lemma type_to_bool_if_true_r v (φ ψ: dProp) it Φ:
    type_to_bool v ((φ → ψ)%DP @ boolR it)%RT Φ ⊢ type_to_bool v (case φ ψ True%DP @ boolR it)%RT Φ.
  Proof.
    rewrite /type_to_bool /type_if_expr_prop.
    iIntros "Hcont Hv". iApply "Hcont". rewrite boolR_iff_eq //.
    rewrite /case. destruct (decide φ), (decide ψ); naive_solver.
  Qed.

  Lemma type_to_bool_if_false_l v (φ ψ: dProp) it Φ:
    type_to_bool v ((¬ φ ∧ ψ)%DP @ boolR it)%RT Φ ⊢ type_to_bool v (case φ False%DP ψ @ boolR it)%RT Φ.
  Proof.
    rewrite /type_to_bool /type_if_expr_prop.
    iIntros "Hcont Hv". iApply "Hcont". rewrite boolR_iff_eq //.
    rewrite /case. destruct (decide φ), (decide ψ); naive_solver.
  Qed.

  Lemma type_to_bool_if_false_r v (φ ψ: dProp) it Φ:
    type_to_bool v ((φ ∧ ψ)%DP @ boolR it)%RT Φ ⊢ type_to_bool v (case φ ψ False%DP @ boolR it)%RT Φ.
  Proof.
    rewrite /type_to_bool /type_if_expr_prop.
    iIntros "Hcont Hv". iApply "Hcont". rewrite boolR_iff_eq //.
    rewrite /case. destruct (decide φ), (decide ψ); naive_solver.
  Qed.

  Lemma type_to_bool_eta v (φ: dProp) it Φ:
    type_to_bool v (φ @ boolR it)%RT Φ ⊢ type_to_bool v (case φ True%DP False%DP @ boolR it)%RT Φ.
  Proof.
    rewrite /type_to_bool /type_if_expr_prop.
    iIntros "Hcont Hv". iApply "Hcont". rewrite boolR_iff_eq //.
    rewrite /case. destruct (decide φ); naive_solver.
  Qed.


  (* IF-STATEMENTS *)
  Lemma type_if_cond_type_if_stmt (v: val) (A: type Σ) s1 s2 join ot Q Φ :
    type_if_cond v A ot (λ φ, type_if_prop φ join s1 s2 Q Φ) ⊢ type_if_stmt v A ot join s1 s2 Q Φ.
  Proof.
    rewrite /type_if_stmt /type_if_cond. iIntros "Hcont Hv".
    iPoseProof ("Hcont" with "Hv") as "Hcont".
    rewrite /type_if_prop.
    destruct ot; try done.
    - iDestruct "Hcont" as (b φ Hval Hiff) "Hcont".
      iApply wps_if_bool; first done.
      destruct decide, b; naive_solver.
    - iDestruct "Hcont" as (z φ Hval Hiff) "Hcont".
      iApply wps_if; first done.
      rewrite bool_decide_decide.
      destruct decide, decide; naive_solver.
    - iDestruct "Hcont" as (l φ Hval Hiff) "[Hw Hcont]".
      iApply (wps_if_ptr with "Hw"); first eauto with quiver_logic.
      rewrite bool_decide_decide.
      destruct decide, decide; naive_solver.
  Qed.

  (* ASSERT *)
  Lemma type_assert_type_if_cond v A ot s Q Φ:
    type_if_cond v A ot (λ φ, ⌜φ⌝ ∗ swp s Q Φ) ⊢ type_assert v A ot s Q Φ.
  Proof.
    rewrite /type_assert /Assert. iIntros "Hcont Hv".
    iPoseProof (type_if_cond_type_if_stmt _ _ _ _ None) as "Hif".
    rewrite /type_if_stmt. iApply ("Hif" with "[Hcont] Hv").
    iApply type_if_cond_wand. iFrame.
    iIntros (φ) "[%Hφ Hswp]".
    rewrite /type_if_prop. destruct decide; naive_solver.
  Qed.

  (* IF CONDITIONS *)
  Lemma type_if_cond_value v w ot1 ot2 Φ:
    type_val w (λ u A, type_if_cond u A ot2 (λ φ, abd_assume CTX (v ◁ᵥ valueT u ot1) (Φ φ))) ⊢ type_if_cond v (valueT w ot1) ot2 Φ.
  Proof.
    rewrite /type_val. iIntros "[%A [Hv Hcont]]".
    rewrite /type_if_cond /abd_assume. iIntros "#Hvw".
    iDestruct (vtr_valueT_elim with "Hvw") as "[-> _]".
    iSpecialize ("Hcont" with "Hv"). destruct ot2.
    - iDestruct "Hcont" as (b φ Hval Hiff) "Hcont". iExists b, φ.
      iSpecialize ("Hcont" with "Hvw"). by iFrame.
    - iDestruct "Hcont" as (z φ Hval Hiff) "Hcont". iExists z, φ.
      iSpecialize ("Hcont" with "Hvw"). by iFrame.
    - iDestruct "Hcont" as (l φ Hval Hiff) "[Hw Hcont]".
      iExists l, φ. iSpecialize ("Hcont" with "Hvw"). by iFrame.
    - done.
    - done.
  Qed.

  Lemma type_if_cond_force_type_if_cond v A ot Φ:
    type_if_cond_force v A ot Φ ⊢ type_if_cond v A ot Φ.
  Proof. rewrite /type_if_cond_force /type_if_cond. done. Qed.

  Lemma type_if_cond_force_bool_boolT (v: val) φ (Φ: dProp → iProp Σ) :
    abd_assume CTX (v ◁ᵥ φ @ boolR u8) (Φ φ) ⊢ type_if_cond_force v (φ @ boolR u8)%RT BoolOp Φ.
  Proof.
    rewrite /type_if_cond_force /type_if_cond. iIntros "Hcont #Hv".
    rewrite /abd_assume. iDestruct ("Hcont" with "Hv") as "Hcont".
    rewrite /with_refinement /boolR /=.
    iDestruct (vtr_bool_inversion with "Hv") as "->".
    iExists (bool_decide φ), φ. iFrame. iPureIntro.
    split; last first.
    { rewrite bool_decide_decide. destruct decide; naive_solver. }
    destruct (bool_decide φ); done.
  Qed.

  Lemma type_if_cond_force_int_boolT (v: val) it φ (Φ: dProp → iProp Σ) :
    abd_assume CTX (v ◁ᵥ φ @ boolR it) (Φ φ) ⊢ type_if_cond_force v (φ @ boolR it)%RT (IntOp it) Φ.
  Proof.
    rewrite /type_if_cond_force /type_if_cond. iIntros "Hcont #Hv".
    rewrite /abd_assume. iDestruct ("Hcont" with "Hv") as "Hcont".
    rewrite /with_refinement /boolR /=.
    iDestruct (vtr_bool_inversion with "Hv") as "->".
    iExists (bool_to_Z (bool_decide φ)), φ. iFrame. iPureIntro.
    split; first by eapply i2v_bool_Some.
    rewrite bool_decide_decide. destruct decide; naive_solver.
  Qed.

  Lemma type_if_cond_force_int_intT (v: val) n it (Φ: dProp → iProp Σ) :
    abd_assume CTX (v ◁ᵥ n @ intR it) (Φ (n ≠ 0)%DP) ⊢ type_if_cond_force v (n @ intR it)%RT (IntOp it) Φ.
  Proof.
    rewrite /type_if_cond_force /type_if_cond. iIntros "Hcont #Hv".
    rewrite /abd_assume. iDestruct ("Hcont" with "Hv") as "Hcont".
    rewrite /with_refinement /intR /=.
    iDestruct (vtr_int_inversion with "Hv") as "[%Heq %Hint]".
    iExists n, (n ≠ 0)%DP. iFrame. iPureIntro.
    split; last done.
    subst v. eauto with quiver_logic.
  Qed.

  Lemma type_if_cond_force_ptr (v: val) (A: type Σ) (Φ: dProp → iProp Σ) :
    type_null_check v A (λ φ B, abd_assume CTX (v ◁ᵥ B) (Φ φ)) ⊢ type_if_cond_force v A PtrOp Φ.
  Proof.
    rewrite /type_null_check /abd_assume /type_if_cond_force /type_if_cond.
    iIntros "Hcont Hv". iDestruct "Hcont" as (φ B) "[Hcast Hcont]".
    destruct decide; iDestruct ("Hcast" with "Hv") as "[Hv #Hdec]".
    - iDestruct ("Hcont" with "Hv") as "Hcont".
      iDestruct "Hdec" as (l) "[-> Hloc]".
      iExists l, φ. iFrame. iDestruct (wp_if_precond_alloc with "Hloc") as "$".
      iDestruct (loc_in_bounds_null_loc with "Hloc") as %Hneq.
      iSplit; iPureIntro; eauto with quiver_logic.
    - iDestruct "Hdec" as "%Heq". iDestruct ("Hcont" with "Hv") as "Hcont".
      iExists _, _. iFrame. iSplit; first done.
      iSplit; first by naive_solver.
      by iApply wp_if_precond_null.
  Qed.

End controlflow_rules.

Global Typeclasses Opaque
  type_if_cond_force
  type_if_expr_prop
  type_if_prop
  type_free_loc_typed
  type_free_locs_typed
  type_to_bool.

Global Typeclasses Opaque type_break_at.

Global Typeclasses Opaque type_loop_core.
Global Typeclasses Opaque type_loop_core_assuming_inv.
Global Typeclasses Opaque type_loop_core_assuming_inv_ctx.
Global Typeclasses Opaque type_loop_fixp.
