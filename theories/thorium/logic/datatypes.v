
From caesium Require Import lang.
From quiver.base Require Import classes.
From quiver.thorium.types Require Import types base_types.
From quiver.thorium.types Require Export structs arrays bytes.
From quiver.argon.base Require Import judgments.
From quiver.thorium.logic Require Import syntax pointers controlflow type_cast.

Section structs.
  Context `{!refinedcG Σ}.

  (* STRUCT-MEMBER *)
  Lemma type_get_member_struct_type l s m (i: nat) T A Φ:
    T  !! i = Some A →
    field_index_of (sl_members s) m = Some i →
    (l ◁ₗ structT s (<[i:=placeT (l at{s}ₗ m)]> T) -∗ Φ (l at{s}ₗ m) A) ⊢ type_get_member l (structT s T) s m Φ.
  Proof.
    rewrite /type_get_member. iIntros (Hty Hmem) "Hpost Ht".
    rewrite /lwp.
    iPoseProof (struct_mem_in_bounds with "Ht") as "#Hb".
    iApply wp_get_member; [by eapply val_to_of_loc | by eapply field_index_of_to_index_of | done |].
    iNext.
    iPoseProof (struct_focus with "Ht") as "[Hmems Hclose]".
    iPoseProof (big_sepL2_insert_acc with "Hmems") as "[Hm Hrest]";
      eauto using field_index_of_lookup.
    iPoseProof (placeT_create (l at{s}ₗ m)) as "Hl".
    iSpecialize ("Hrest" with "Hl").
    rewrite (list_insert_id _  i m); last by eapply field_index_of_lookup.
    iSpecialize ("Hclose" with "Hrest").
    iExists _, _. iSplit; first done. iFrame.
    by iApply "Hpost".
  Qed.

  Lemma type_get_member_place x l sl m Φ :
    type_loc l (λ r A, type_get_member r A sl m Φ) ⊢ type_get_member x (placeT l) sl m Φ.
  Proof.
    rewrite /type_get_member /type_loc. iIntros "[%A [Hl Hlwp]] Hx".
    iPoseProof (placeT_is_eq with "Hx") as "->".
    iApply "Hlwp". iApply (placeT_merge with "[$Hx $Hl]").
  Qed.

  Lemma type_get_member_any l (sl: struct_layout) m Φ :
    type_get_member l (structT sl (anyT <$> member_sizes sl)) sl m Φ ⊢ type_get_member l (anyT (ly_size sl)) sl m Φ.
  Proof.
    rewrite /type_get_member. iIntros "Hcont Hl". rewrite anyT_structT_equiv_member_layouts //.
    by iApply "Hcont".
  Qed.

  Lemma type_get_member_zeros l (sl: struct_layout) m Φ :
    type_get_member l (structT sl (zerosT <$> member_sizes sl)) sl m Φ ⊢ type_get_member l (zerosT (ly_size sl)) sl m Φ.
  Proof.
    rewrite /type_get_member. iIntros "Hcont Hl".
    iDestruct ((zerosT_to_structT_member_layouts (LOC l)) with "Hl") as "?".
    by iApply "Hcont".
  Qed.

  Lemma type_get_member_virtual l A sl m Φ :
    type_virt (LOC l) A (λ A', type_get_member l A' sl m Φ) ⊢ type_get_member l A sl m Φ.
  Proof.
    rewrite /type_virt /type_get_member. iIntros "Hcont Hl".
    iDestruct ("Hcont" with "Hl") as (A') "[Hl Hcont]".
    by iApply "Hcont".
  Qed.

  Lemma type_get_member_slices l len Ls (sl: struct_layout) m Φ:
    type_project_slice l 0 (ly_size sl) Ls (λ i li A L R, ⌜li = (ly_size sl)⌝ ∗
      (l ◁ₗ slicesT len (L ++ (0, ly_size sl: Z, placeT (l +ₗ 0)) :: R) -∗
      type_get_member (l +ₗ 0) A sl m Φ)
    )
    ⊢ type_get_member l (slicesT len Ls) sl m Φ.
  Proof.
    rewrite /type_project_slice /type_get_member.
    iIntros "Hcont Hl". iSpecialize ("Hcont" $! len nil nil).
    rewrite app_nil_l app_nil_r.
    iDestruct ("Hcont" with "Hl") as "(%i & %li & %A & %L & %R & % & % & % & Hl & Ha & -> & Hcont)".
    have -> : i = 0 by lia.
    rewrite app_nil_l app_nil_r. rewrite shift_loc_0.
    iApply ("Hcont" with "Hl Ha").
  Qed.

End structs.



Section arrays.
  Context `{!refinedcG Σ}.

  Definition type_ptr_offset l A (i: Z) Φ : iProp Σ :=
    l ◁ₗ A -∗ ∃ r B, ⌜r = (l +ₗ i)⌝ ∗ r ◁ₗ B ∗ loc_in_bounds r 0 ∗ Φ r B.

  Definition type_offset_to_idx (i sz : Z) (Φ : Z → iProp Σ) : iProp Σ :=
    ∃ j, ⌜i = (sz * j)%Z⌝ ∗ Φ j.

  (* RAW POINTER ACCESSES *)
  Definition type_use_at_idx r A n i Φ : iProp Σ :=
    r ◁ₗ A -∗ ∃ v A, (r +ₗ i) ◁ₗ valueT v n ∗ v ◁ᵥ A ∗ ((r +ₗ i) ◁ₗ valueT v n -∗ Φ v A).

  Definition type_assign_at_idx r A v B n i P : iProp Σ :=
    r ◁ₗ A -∗ v ◁ᵥ B -∗ ∃ C, (r +ₗ i) ◁ₗ anyT n ∗ v ◁ᵥ C ∗ ((r +ₗ i) ◁ₗ valueT v n -∗ v ◁ᵥ C -∗ P).

  Definition type_borrow_at_idx r A (n : Z) i Φ : iProp Σ :=
    r ◁ₗ A -∗ ∃ B, (r +ₗ i) ◁ₗ B ∗ Φ (r +ₗ i) B.


  (* AT-OFFSET *)
  Lemma type_at_offset_value l v A ot w ly ot1 ot2 Φ :
    type_val w (λ u B, abd_assume CTX (v ◁ᵥ valueT u ot) (type_at_offset l u A B ly ot1 ot2 Φ)) ⊢ type_at_offset l v A (valueT w ot) ly ot1 ot2 Φ.
  Proof.
    rewrite /type_val /type_at_offset /abd_assume.
    iIntros "[%B [Hw Hrec]] Hl #Hv".
    iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
    iApply ("Hrec" with "Hv Hl Hw").
  Qed.

  Lemma type_at_offset_place l r v ly ot1 ot2 B Φ :
    type_loc r (λ r' A, abd_assume CTX (l ◁ₗ placeT r') (type_at_offset r' v A B ly ot1 ot2 Φ)) ⊢ type_at_offset l v (placeT r) B ly ot1 ot2 Φ.
  Proof.
    rewrite /type_at_offset /type_loc /abd_assume. iIntros "[%A [Hl Hlwp]] Hx".
    iDestruct (placeT_is_eq with "Hx") as "%Heq"; subst l.
    by iApply ("Hlwp" with "Hx Hl").
  Qed.

  Lemma type_at_offset_type_var l v A B ly it Φ:
    (∃ n, ⌜B `is_ty` intT it n⌝ ∗ type_at_offset l v A (intT it n) ly PtrOp (IntOp it) Φ) ⊢ type_at_offset l v A B ly PtrOp (IntOp it) Φ.
  Proof.
    iIntros "(%n & %Hty & Hpost)". destruct Hty as [Hty].
    rewrite /type_at_offset. rewrite Hty //.
  Qed.

  (* PTR-OFFSET *)
  Lemma type_at_offset_raw_pointer l v ly it i A Φ :
    type_ptr_offset l A (ly_size ly * i) (λ r B, abd_assume CTX (v ◁ᵥ intT it i) (Φ r B)) ⊢ type_at_offset l v A (intT it i) ly PtrOp (IntOp it) Φ.
  Proof.
    rewrite /type_at_offset /type_ptr_offset /abd_assume. iIntros "Hpost Hl #Hv".
    iDestruct (vtr_int_inversion with "Hv") as "[-> %Hel]".
    iDestruct ("Hpost" with "Hl") as (r B) "(-> & Hr & #Hbounds & Hpost)".
    iDestruct ("Hpost" with "Hv") as "Hpost".
    rewrite /lwp. iApply (wp_ptr_offset with "[] [-]"); eauto with quiver_logic.
    iNext. iExists _, _. iFrame. done.
  Qed.

  Lemma type_ptr_offset_bounds l A i Φ :
    (type_bounds A (λ n, ⌜0 ≤ i ≤ n⌝ ∗ (l ◁ₗ A -∗ Φ (l +ₗ i) (ptrT l n i))))
  ⊢ type_ptr_offset l A i Φ.
  Proof.
    rewrite /type_ptr_offset /type_bounds. iIntros "H Hl". iDestruct "H" as (n) "(%Hb & %Hi & HΦ)".
    iDestruct (Hb with "Hl") as "[#Hb %Hn]".
    iDestruct ("HΦ" with "Hl") as "HΦ".
    iExists _, _. iFrame "HΦ". iSplit; first done.
    iSplit; last first.
    { iApply (loc_in_bounds_shift_loc with "Hb"). lia. }
    rewrite ltr_ptrT_iff. iSplit; first done. iFrame "Hb".
    by iPureIntro.
  Qed.

  Lemma type_ptr_offset_shift_raw_pointer l r i j n Φ:
    ⌜0 ≤ i + j ≤ n⌝ ∗ Φ (r +ₗ (i + j)) (ptrT r n (i + j)) ⊢ type_ptr_offset l (ptrT r n i) j Φ.
  Proof.
    rewrite /type_ptr_offset. iIntros "[%Hbounds Hcont] Hl".
    iDestruct (raw_pointer_shift j with "Hl") as "#Hj"; first lia.
    iExists _, _. iFrame "Hcont". iDestruct (ltr_ptrT_inv with "Hl") as "->".
    rewrite shift_loc_assoc.
    iFrame "Hj". iSplit; first done.
    by iApply raw_pointer_loc_in_bounds.
  Qed.

  Lemma type_ptr_offset_exi l i A Φ :
    ⌜A `is_ty` empT⌝ ∗ (abd_assume CTX (l ◁ₗ empT) $ type_loc (l +ₗ i) (λ r B, ⌜NonNull B⌝ ∗ (Φ (l +ₗ i) B))) ⊢
      type_ptr_offset l A i Φ.
  Proof.
    rewrite /abd_assume/type_loc/type_ptr_offset.
    iIntros "[-> Hcont] Hl". iDestruct ("Hcont" with "Hl") as (?) "[Hl [% HΦ]]".
    iDestruct (is_not_null with "Hl") as "#?".
    iExists _, _. by iFrame "∗#".
  Qed.

  Lemma type_ptr_offset_empT l i Φ :
    (abd_assume CTX (l ◁ₗ empT) $ type_loc (l +ₗ i) (λ r B, ⌜NonNull B⌝ ∗ (Φ r B))) ⊢
      type_ptr_offset l empT i Φ.
  Proof.
    rewrite /abd_assume/type_loc/type_ptr_offset.
    iIntros "Hcont Hl". iDestruct ("Hcont" with "Hl") as (?) "[Hl [% HΦ]]".
    iDestruct (is_not_null with "Hl") as "#?".
    iExists _, _. by iFrame "∗#".
  Qed.

  (* OFFSET-TO-IDX *)
  Lemma type_offset_to_idx_mul_l i sz Φ :
    Φ i ⊢ type_offset_to_idx (sz * i) sz Φ.
  Proof. iIntros "?". iExists _. by iFrame. Qed.

  (* RAW POINTER USE/STORE *)
  Lemma type_use_raw_ptr r i ot l n Φ :
    type_loc r (λ r' A, type_use_at_idx r' A (ly_size (ot_layout ot)) i Φ) ⊢ type_use l (ptrT r n i) ot Φ.
  Proof.
    rewrite /type_loc /type_use_at_idx /abd_assume /type_use.
    iDestruct 1 as "[%A [He Hcont]]". iIntros "#Hl".
    iDestruct ("Hcont" with "He") as "(%v & %B & Hoff & Hv & Hcont)".
    iDestruct (ltr_ptrT_inv with "Hl") as "->".
    rewrite /rwp. iApply (wp_deref_typed with "[Hoff Hv Hcont Hl]").
    { by apply valueT_has_size. }
    iFrame. iIntros (w) "Hpts Hval %Hsz".
    iDestruct (vtr_valueT_elim with "Hval") as "[-> _]".
    iExists _. iFrame. iApply "Hcont".
    iApply valueT_intro; first done. iFrame.
  Qed.

  Lemma type_assign_raw_pointer r n l i v B ot s cfg Φ :
    ⌜B `has_size` ly_size (ot_layout ot)⌝ ∗ type_loc r (λ r' A,
      type_assign_at_idx r' A v B (ly_size (ot_layout ot)) i (swp s cfg Φ)
    )%I ⊢ type_assign l (ptrT r n i) v B ot s cfg Φ.
  Proof.
    rewrite /type_loc /type_assign /abd_assume. iDestruct 1 as "[%Hsz [%A [Hr Hcont]]]".
    iIntros "#Hl Hv". iDestruct (ltr_ptrT_inv with "Hl") as "->".
    rewrite /type_assign_at_idx. iDestruct (ty_size_eq with "Hv") as "%Hval"; first done.
    iDestruct ("Hcont" with "Hr Hv") as (C) "(Hoff & Hv & Hcont)".
    rewrite /swp. iApply (wps_assign_typed); first by apply valueT_has_size.
    iSplitL ""; last iSplitL "Hoff".
    { iApply vtr_valueT_intro. iPureIntro. done. }
    { rewrite ltr_anyT_pts_ly //. }
    iIntros "Hpts Hw". iApply ("Hcont" with "[Hpts] Hv").
    iApply valueT_intro; done.
  Qed.

  Lemma type_get_member_raw_ptr r i (sl : struct_layout) l n m Φ :
    type_loc r (λ r' A, type_borrow_at_idx r' A (ly_size sl) i (λ r'' B, type_get_member r'' B sl m Φ))
    ⊢ type_get_member l (ptrT r n i) sl m Φ.
  Proof.
    rewrite /type_loc /type_borrow_at_idx /type_get_member.
    iDestruct 1 as "[%A [He Hcont]]". iIntros "#Hl".
    iDestruct ("Hcont" with "He") as "(%B & HB & Hcont)".
    iDestruct (ltr_ptrT_inv with "Hl") as "->".
    by iApply "Hcont".
  Qed.

  (* RAW POINTER ACCESS *)
  Lemma type_assign_at_idx_value r A v w n m i P:
    type_val w (λ u B, abd_assume CTX (v ◁ᵥ valueT w n) $ (type_assign_at_idx r A u B m i P)) ⊢ type_assign_at_idx r A v (valueT w n) m i P.
  Proof.
    rewrite /type_assign_at_idx /type_val /abd_assume.
    iIntros "(%B & Hw & Hcont) Hr #Hv".
    iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
    iApply ("Hcont" with "Hv Hr Hw").
  Qed.

  Lemma type_use_at_idx_any l n m i Φ:
    (⌜0 ≤ i⌝ ∗ ⌜0 ≤ m⌝ ∗ ⌜i + m ≤ n⌝ ∗ abd_assume CTX (l ◁ₗ anyT n) $ ∀ v, Φ v (anyT m))%I ⊢ type_use_at_idx l (anyT n) m i Φ.
  Proof.
    rewrite /abd_assume/type_use_at_idx.
    iDestruct 1 as (???) "HΦ". iIntros "Hl".
    iDestruct (ltr_anyT_extract_range i m with "Hl") as "[Hl Hwand]"; [lia..|].
    rewrite {1}(valueT_loc_val (l +ₗ i)); [|apply anyT_has_size].
    iDestruct "Hl" as (?) "[??]".
    iExists _, _. iFrame. iIntros "Hl".
    iApply ("HΦ" with "[-]"). iApply "Hwand".
    iApply (cast_to_any_ltr with "Hl").
    by apply valueT_has_size.
  Qed.

  Lemma type_assign_at_idx_any l n m v A i Φ:
  (⌜0 ≤ i⌝ ∗ ⌜0 ≤ m⌝ ∗ ⌜i + m ≤ n⌝ ∗ ⌜A `has_size` m⌝ ∗ abd_assume CTX (l ◁ₗ anyT n) $ Φ)%I ⊢ type_assign_at_idx l (anyT n) v A m i Φ.
  Proof.
    rewrite /abd_assume/type_assign_at_idx.
    iDestruct 1 as (????) "HΦ". iIntros "Hl Hv".
    iDestruct (ltr_anyT_extract_range i m with "Hl") as "[Hl Hwand]"; [lia..|].
    iExists _. iFrame. iIntros "Hl Hv".
    iApply ("HΦ" with "[-]"). iApply "Hwand".
    iApply (cast_to_any_ltr with "Hl").
    by apply valueT_has_size.
  Qed.

  Lemma type_assign_at_idx_array_copy X (T: X → type Σ) r xs sz v x m i P:
    (∀ x, Copy (T x)) →
    type_offset_to_idx i sz (λ j, ⌜m = sz⌝ ∗ ⌜0 ≤ j < length xs⌝ ∗ (abd_assume CTX (r ◁ₗ (arrayT T sz (<[Z.to_nat j := x]> xs))) $ abd_assume CTX (v ◁ᵥ (T x)) $ P)%RT)
    ⊢ type_assign_at_idx r (arrayT T sz xs)%RT v (T x)%RT m i P.
  Proof.
    intros Hcp.
    rewrite /type_offset_to_idx /abd_assume /type_assign_at_idx.
    iIntros "(%j & -> & -> & %Hle & Hty) Hr Hv".
    iExists _. iFrame "Hv".
    assert (Z.to_nat j < length xs)%nat as Hlt by lia.
    eapply lookup_lt_is_Some_2 in Hlt as [y Hy].
    iDestruct (xtr_array_size (LOC r) with "Hr") as "%Hsz".
    destruct Hsz as [Hle2 Hsz].
    iDestruct (arrayT_get_type with "Hr") as "Hx"; first done.
    replace (Z.to_nat j * sz) with (sz * j) by lia.
    iDestruct "Hx" as "[Hx Hr]".
    iSplitL "Hx".
    { iApply cast_to_any_ltr; last iFrame. done. }
    iIntros "Hj #Hv". iApply ("Hty" with "[-Hv] Hv").
    iDestruct (valueT_elim_loc with "Hj Hv") as "Hj".
    iApply "Hr". iFrame.
  Qed.

  Lemma type_assign_at_idx_array_exi_arg X (T: X → type Σ) sz xs r v B m i P:
    (∃ x: X, ⌜B `is_ty` T x⌝ ∗ type_assign_at_idx r (arrayT T sz xs) v (T x) m i P)
  ⊢ type_assign_at_idx r (arrayT T sz xs) v B m i P.
  Proof.
    iIntros "H". iDestruct "H" as (x) "[%Hty H]". destruct Hty as [Hty].
    rewrite /type_assign_at_idx. rewrite Hty //.
  Qed.

  Lemma type_use_at_idx_array_copy X `{!Inhabited X} (T: X → type Σ) xs r sz m i Φ:
    (∀ x, Copy (T x)) →
    type_offset_to_idx i sz (λ j, ⌜m = sz⌝ ∗ ⌜0 ≤ j < length xs⌝ ∗ (abd_assume CTX (r ◁ₗ arrayT T sz xs) $ ∀ v, Φ v (T (xs !!! (Z.to_nat j)))%RT)) ⊢ type_use_at_idx r (arrayT T sz xs) m i Φ.
  Proof.
    intros Hcp.
    rewrite /type_offset_to_idx /abd_assume /type_use_at_idx.
    iIntros "(%j & -> & -> & %Hle & Hty) Hr".
    assert (Z.to_nat j < length xs)%nat as Hlt by lia.
    eapply lookup_lt_is_Some_2 in Hlt as [y Hy].
    iDestruct (xtr_array_size (LOC r) with "Hr") as "%Hsz".
    destruct Hsz as [Hle2 Hsz].
    iDestruct (arrayT_get_type with "Hr") as "Hx"; first done.
    iDestruct "Hx" as "[Hx Hr]".
    replace (Z.to_nat j * sz: Z) with (sz * j) by lia.
    rewrite (valueT_loc_val (r +ₗ _)); last done.
    iDestruct "Hx" as "(%v & Hj & #Hv)".
    replace (Z.to_nat j * sz) with (sz * j) by lia.
    iExists _, _. iFrame "Hj". iFrame "Hv". iIntros "Hj".
    iDestruct (valueT_elim_loc with "Hj Hv") as "Hj".
    iSpecialize ("Hr" with "Hj"). rewrite list_insert_id //.
    iSpecialize ("Hty" with "Hr").
    erewrite list_lookup_total_correct; last done.
    iApply "Hty".
  Qed.

  Lemma type_borrow_at_idx_array X `{!Inhabited X} (T: X → type Σ) xs r sz m i Φ:
    type_offset_to_idx i sz (λ j, (⌜m = sz⌝ ∗ ⌜0 ≤ j < length xs⌝ ∗
         (abd_assume CTX (r ◁ₗ wandT (λ x, (r +ₗ i) ◁ₗ T x) (λ x, arrayT T sz (<[Z.to_nat j := x]> xs)))
            $ Φ (r +ₗ i) (T (xs !!! (Z.to_nat j)))%RT)))
      ⊢ type_borrow_at_idx r (arrayT T sz xs) m i Φ.
  Proof.
    rewrite /type_offset_to_idx /abd_assume /type_use_at_idx.
    iIntros "(%j & -> & -> & %Hle & Hty) Hr".
    assert (Z.to_nat j < length xs)%nat as Hlt by lia.
    eapply lookup_lt_is_Some_2 in Hlt as [y Hy].
    iDestruct (arrayT_get_type with "Hr") as "[Hx Hr]"; first done.
    rewrite Z2Nat.id; [|lia]. rewrite (Z.mul_comm j sz).
    iExists _. iFrame. erewrite list_lookup_total_correct; last done.
    iApply "Hty". by iApply wandT_intro.
  Qed.

  (* EXTRACTING PREFIXES AND RANGES *)
  Lemma type_extract_prefix_raw_pointer r l i li n Φ:
    type_loc r (λ r' A, abd_assume CTX (l ◁ₗ ptrT r' n i) (type_extract_range r' A i li Φ)) ⊢ type_extract_prefix l (ptrT r n i) li Φ.
  Proof.
    rewrite /type_loc /type_extract_range /type_extract_prefix /abd_assume.
    iIntros "[%A [Hr HΦ]] #Hl". iDestruct (ltr_ptrT_inv with "Hl") as "->".
    iApply ("HΦ" with "Hl Hr").
  Qed.


  (* matching size *)
  Lemma type_move_extract_prefix l A n Φ:
    type_move l A n (λ v, Φ l v) ⊢ type_extract_prefix l A n Φ.
  Proof.
    rewrite /type_move /type_extract_prefix.
    iIntros "Hcont Hl". by iApply "Hcont".
  Qed.

  Lemma type_extract_prefix_start l A n Φ :
    type_extract_range l A 0 n Φ ⊢ type_extract_prefix l A n Φ.
  Proof.
    rewrite /type_extract_prefix /type_extract_range. iIntros "Hl".
    setoid_rewrite shift_loc_0. iApply "Hl".
  Qed.

  Lemma type_extract_prefix_any l n m Φ :
    ⌜0 ≤ n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, n, placeT (l +ₗ 0)); (n, m - n, anyT (m - n))] -∗ type_move (l +ₗ 0) (anyT n) n (λ v, Φ (l +ₗ 0) v))  ⊢ type_extract_prefix l (anyT m) n Φ.
  Proof.
    rewrite /type_extract_prefix. iIntros "[%Hran Hcont] Hl".
    assert (m = n + (m - n)) as Heq by lia. rewrite {4}Heq.
    rewrite anyT_split; [|lia..]. pose proof (sliced_borrow_iff l nil (n + (m - n)) 0 n) as Hiff.
    simpl in Hiff. rewrite Hiff. iDestruct "Hl" as "[Hl Hi]".
    rewrite -Heq. iDestruct ("Hcont" with "Hl") as "Hcont". rewrite /type_move.
    rewrite shift_loc_0. by iApply "Hcont".
  Qed.

  Lemma type_extract_prefix_zeros l n m Φ :
    ⌜0 ≤ n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, n, placeT (l +ₗ 0)); (n, m - n, zerosT (m - n))] -∗ type_move (l +ₗ 0) (zerosT n) n (λ v, Φ (l +ₗ 0) v))  ⊢ type_extract_prefix l (zerosT m) n Φ.
  Proof.
    rewrite /type_extract_prefix. iIntros "[%Hran Hcont] Hl".
    assert (m = n + (m - n)) as Heq by lia. rewrite {4}Heq.
    rewrite zerosT_split; [|lia..]. pose proof (sliced_borrow_iff l nil (n + (m - n)) 0 n) as Hiff.
    simpl in Hiff. rewrite Hiff. iDestruct "Hl" as "[Hl Hi]".
    rewrite -Heq. iDestruct ("Hcont" with "Hl") as "Hcont". rewrite /type_move.
    rewrite shift_loc_0. by iApply "Hcont".
  Qed.

  (* EXTRACT RANGE *)
  Lemma type_extract_range_ptr l r j i n li Φ :
    type_loc r (λ r' A, abd_assume CTX (l ◁ₗ ptrT r' n j) (type_extract_range r' A (i + j) li Φ))
  ⊢ type_extract_range l (ptrT r n j) i li Φ.
  Proof.
    rewrite /type_extract_range /type_loc /abd_assume.
    iDestruct 1 as (A) "(Hr & HΦ)". iIntros "Hl".
    iDestruct (ltr_ptrT_inv with "Hl") as "->".
    rewrite shift_loc_assoc. rewrite Z.add_comm.
    iApply ("HΦ" with "Hl Hr").
  Qed.


  Lemma type_extract_range_any l i n m Φ :
    ⌜0 ≤ i⌝ ∗ ⌜0 ≤ n⌝ ∗ ⌜i + n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, i, anyT i); (i, n, placeT (l +ₗ i)); (i + n, m - n - i, anyT (m - n - i))] -∗ type_move (l +ₗ i) (anyT n) n (λ v, Φ (l +ₗ i) v))
  ⊢ type_extract_range l (anyT m) i n Φ.
  Proof.
    rewrite /type_extract_range. iIntros "(%Hran1 & %Hran2 & %Hran3 & Hcont) Hl".
    iDestruct (anyT_non_negative (LOC l) with "Hl") as "%Hz".
    assert (m = i + n + (m - n - i)) as Heq by lia. rewrite {4}Heq.
    rewrite anyT_split; [|lia..].
    specialize (slices_split_slice 0 i n m (anyT (i + n)) (anyT i) (anyT n) nil) as Hiff.
    simpl in Hiff. rewrite -Heq. rewrite Hiff; last first.
    { eapply anyT_split; lia. }
    rewrite Z.add_0_l.
    pose proof (sliced_borrow_iff l [(0, i, anyT i)] m i n (anyT n)) as Hbor. rewrite Hbor.
    iDestruct "Hl" as "[Hl Hi]".
    iDestruct ("Hcont" with "Hl") as "Hcont".
    rewrite /type_move. by iApply "Hcont".
  Qed.

  Lemma type_extract_range_zeros l i n m Φ :
    ⌜0 ≤ i⌝ ∗ ⌜0 ≤ n⌝ ∗ ⌜i + n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, i, zerosT i); (i, n, placeT (l +ₗ i)); (i + n, m - n - i, zerosT (m - n - i))] -∗ type_move (l +ₗ i) (zerosT n) n (λ v, Φ (l +ₗ i) v))
  ⊢ type_extract_range l (zerosT m) i n Φ.
  Proof.
    rewrite /type_extract_range. iIntros "(%Hran1 & %Hran2 & %Hran3 & Hcont) Hl".
    iDestruct (zerosT_non_negative (LOC l) with "Hl") as "%Hz".
    assert (m = i + n + (m - n - i)) as Heq by lia. rewrite {4}Heq.
    rewrite zerosT_split; [|lia..].
    specialize (slices_split_slice 0 i n m (zerosT (i + n)) (zerosT i) (zerosT n) nil) as Hiff.
    simpl in Hiff. rewrite -Heq. rewrite Hiff; last first.
    { eapply zerosT_split; lia. }
    rewrite Z.add_0_l.
    pose proof (sliced_borrow_iff l [(0, i, zerosT i)] m i n (zerosT n)) as Hbor. rewrite Hbor.
    iDestruct "Hl" as "[Hl Hi]".
    iDestruct ("Hcont" with "Hl") as "Hcont".
    rewrite /type_move. by iApply "Hcont".
  Qed.

  Lemma type_extract_range_slices l len Ts i li Φ:
    type_project_slice l i li Ts (λ i' li' A L R,
        l ◁ₗ slicesT len (L ++ (i', li', placeT (l +ₗ i')) :: R) -∗
        type_extract_range (l +ₗ i') A (i - i') li Φ)
  ⊢ type_extract_range l (slicesT len Ts) i li Φ.
  Proof.
    rewrite /type_extract_range /type_project_slice.
    iIntros "Hpr". iIntros "Hl".
    iDestruct ("Hpr" $! len [] []) as "Hpr". rewrite app_nil_l app_nil_r.
    iDestruct ("Hpr" with "Hl") as "(%i' & %li' & %A & %L' & %R' & % & % & % & Hl & HA & Hcont)".
    rewrite app_nil_l app_nil_r.
    have -> : (l +ₗ i' +ₗ (i - i')) = (l +ₗ i) by rewrite shift_loc_assoc; f_equal; lia.
    iApply ("Hcont" with "Hl HA").
  Qed.

  Lemma type_extract_range_var_middle l A i li Φ:
    type_bounds A (λ n,
    ∃ A1 A2 A3,
      ⌜A `is_ty` slicesT n [(0, i, A1); (i, li, A2); (i + li, n - i - li, A3)]%Z⌝ ∗
      (abd_assume CTX (l ◁ₗ slicesT n [(0, i, A1); (i, li, placeT (l +ₗ i)); (i + li, n - i - li, A3)]) $ type_move (l +ₗ i) A2 li (λ v, Φ (l +ₗ i) v))
    )%I ⊢ type_extract_range l A i li Φ.
  Proof.
    rewrite /type_extract_range /type_bounds /abd_assume /type_move.
    iIntros "(%n & %Hb & Hcont) Hl".
    iDestruct ("Hcont") as "(%A1 & %A2 & %A3 & %Hty & Hcont)".
    destruct Hty as [Hty]. rewrite Hty.
    rewrite (sliced_borrow_iff _ [(0, i, A1)] n i li A2) /=.
    iDestruct "Hl" as "[Hl Hi]". iApply ("Hcont" with "Hl Hi").
  Qed.

  Lemma type_extract_range_default l A m n Φ:
    ⌜n = 0⌝ ∗ type_move l A m (Φ l) ⊢ type_extract_range l A n m Φ.
  Proof. rewrite /type_extract_range/type_move. iDestruct 1 as (->) "?". by rewrite shift_loc_0. Qed.

  (* PROJECT SLICE  *)
  Lemma type_project_slice_cons_skip l i li j lj A As Φ :
    type_project_slice l i li As (λ i' li' v L' R', Φ i' li' v ((j, lj, A) :: L') R')
    ⊢ type_project_slice l i li ((j, lj, A) :: As) Φ.
  Proof.
    rewrite /type_project_slice. iIntros "Hpr". iIntros (len L R) "Hl".
    iDestruct ("Hpr" $! len (L ++ [(j, lj, A)]) R) as "Hpr".
    rewrite -app_assoc /=. iDestruct ("Hpr" with "Hl") as "(%i' & %li' & %v & %L' & %R' & % & % & % & Hl & Hi & HΦ)".
    iExists _, _, _, _, _. iFrame "Hi HΦ". repeat (iSplit; [done|]). rewrite -app_assoc //.
  Qed.

  Lemma type_project_slice_cons_match l i li j lj A As Φ :
    ⌜j ≤ i⌝ ∗ ⌜i + li ≤ j + lj⌝ ∗ Φ j lj A [] As ⊢ type_project_slice l i li ((j, lj, A) :: As) Φ.
  Proof.
    iIntros "(% & % & Hm)". rewrite /type_project_slice.
    iIntros (len L R) "Hl". simpl.
    iDestruct (slicesT_covers (LOC _) with "Hl") as %[_ [? Hall]].
    move: Hall. rewrite fmap_app fmap_cons/=. move => /Forall_app[_ /Forall_cons[??]].
    rewrite sliced_borrow_iff.
    iDestruct "Hl" as "[Hl Hj]". iExists _, _, _, _, _. iFrame. simpl. iFrame.
    iPureIntro. lia.
  Qed.


  Lemma type_project_slice_empty l i li Φ :
    abd_fail "extracting a slice from an empty slices type" ⊢
    type_project_slice l i li nil Φ.
  Proof.
    eapply abd_fail_anything.
  Qed.

  Lemma type_project_slice_default l i li j lj A As Φ :
    case (j ≤ i ∧ i + li ≤ j + lj)
      (Φ j lj A [] As)
      (type_project_slice l i li As (λ i' li' v L R, Φ i' li' v ((j, lj, A) :: L) R)) ⊢
    type_project_slice l i li ((j, lj, A) :: As) Φ.
  Proof.
    rewrite /case; destruct decide.
    - rewrite -type_project_slice_cons_match. iIntros "$". simpl in *. iPureIntro; lia.
    - rewrite -type_project_slice_cons_skip //.
  Qed.


End arrays.

Section zeros.
  Context `{!refinedcG Σ}.

  Lemma type_from_zeros_unfold_refinement A X (R : rtype Σ A) F Φ n r :
    (∀ x, unfold_refinement R (r x) (F x)) →
    type_from_zeros X n (λ x, F x) Φ
    ⊢ type_from_zeros X n (λ x, (r x) @ R)%RT Φ.
  Proof.
    rewrite /unfold_refinement/type_from_zeros.
    iIntros (Heq) "(%P&%&#HP&?)". iExists _. iFrame.
    iSplit; [done|]. iIntros "!>" (??) "??".
    rewrite Heq. by iApply ("HP" with "[$]").
  Qed.

  Lemma type_from_zeros_existsT X Y (T : X → Y → type Σ) Φ n :
    type_from_zeros (X * Y) n (λ x, T x.1 x.2) (λ P, (Φ (λ x, ∃ y, P (x, y))))
    ⊢ type_from_zeros X n (λ x, existsT (T x)) Φ.
  Proof.
    rewrite /type_from_zeros.
    iIntros "(%P&%Hpers&#Hwand&Hcont)". iExists _. iFrame.
    iSplit; [iPureIntro; apply _|].
    iIntros "!>" (??) "[% HP] Hz". rewrite xtr_existsT_iff. iExists _.
    iApply ("Hwand" $! _ (_, _) with "HP Hz").
  Qed.

  Lemma type_from_zeros_constraintT X (T : X → type Σ) Q Φ n :
    type_from_zeros X n (λ x, T x) (λ P, (Φ (λ x, P x ∗ ⌜Q x⌝)))
    ⊢ type_from_zeros X n (λ x, constraintT (T x) (Q x)) Φ.
  Proof.
    rewrite /type_from_zeros.
    iIntros "(%P&%Hpers&#Hwand&Hcont)". iExists _. iFrame.
    iSplit; [iPureIntro; apply _|].
    iIntros "!>" (??) "[HP %] Hz". rewrite xtr_constraintT_iff. iSplit; [|done].
    iApply ("Hwand" with "HP Hz").
  Qed.

  Lemma type_from_zeros_sepT X (T : X → type Σ) Q Φ n `{!∀ x, Persistent (Q x)} :
    type_from_zeros X n (λ x, T x) (λ P, (Φ (λ x, P x ∗ Q x)))
    ⊢ type_from_zeros X n (λ x, sepT (T x) (Q x)) Φ.
  Proof.
    rewrite /type_from_zeros.
    iIntros "(%P&%Hpers&#Hwand&Hcont)". iExists _. iFrame.
    iSplit; [iPureIntro; apply _|].
    iIntros "!>" (??) "[HP HQ] Hz". rewrite xtr_sepT_iff. iSplit; [|done].
    iApply ("Hwand" with "HP Hz").
  Qed.

  Lemma type_from_zeros_intT X (it : int_type) Φ n z :
    Φ (λ x, ⌜n = int_size it⌝ ∗ ⌜z x = 0⌝)
    ⊢ type_from_zeros X n (λ x, intT it (z x)) Φ.
  Proof.
    rewrite /type_from_zeros.
    iIntros "Hcont". iExists _. iFrame.
    iSplit; [iPureIntro; apply _|].
    iIntros "!>" (??) "[-> ->] Hz".
    by rewrite zerosT_intT.
  Qed.

  Lemma type_from_zeros_boolT X (it : int_type) Φ n z :
    Φ (λ x, ⌜n = int_size it⌝ ∗ ⌜z x = false⌝)
    ⊢ type_from_zeros X n (λ x, boolT it (z x)) Φ.
  Proof.
    rewrite /type_from_zeros.
    iIntros "Hcont". iExists _. iFrame.
    iSplit; [iPureIntro; apply _|].
    iIntros "!>" (??) "[-> ->] Hz".
    by rewrite zerosT_boolT.
  Qed.

  Lemma type_from_zeros_nullT X Φ n :
    Φ (λ x, ⌜n = ptr_size⌝)
    ⊢ type_from_zeros X n (λ x, nullT) Φ.
  Proof.
    rewrite /type_from_zeros.
    iIntros "Hcont". iExists _. iFrame.
    iSplit; [iPureIntro; apply _|].
    iIntros "!>" (??) "-> Hz". by iApply zerosT_nullT.
  Qed.

  Lemma type_from_zeros_optionalT X Φ n (φ : dProp) A :
    (if decide φ then type_from_zeros X n (λ x, (A x)) Φ else Φ (λ x, ⌜n = ptr_size⌝))
    ⊢ type_from_zeros X n (λ x, optionalT φ (A x)) Φ.
  Proof.
    rewrite /type_from_zeros.
    iIntros "Hcont".
    case_decide.
    - iDestruct "Hcont" as (P) "(? & #Ha & Hb)". iExists _. iFrame.
      iIntros "!>" (??) "HP Hv".
      rewrite /optionalT/case. rewrite decide_True; last done.
      iApply ("Ha" with "HP Hv").
    - iExists _. iFrame.
      iSplit; [iPureIntro; apply _|].
      iIntros "!>" (??) "-> Hz".
      rewrite /optionalT/case.
      rewrite decide_False; last done.
      by iApply zerosT_nullT.
  Qed.

  Lemma type_from_zeros_optionalT_force_null X Φ n (φ : X → dProp) A :
    Φ (λ x, ⌜¬ φ x⌝ ∗ ⌜n = ptr_size⌝)
    ⊢ type_from_zeros X n (λ x, optionalT (φ x) (A x)) Φ.
  Proof.
    rewrite /type_from_zeros.
    iIntros "Hcont".
    iExists _. iFrame.
    iSplit; [iPureIntro; apply _|].
    iIntros "!>" (??) "[%Hn ->] Hz".
    rewrite /optionalT/case.
    rewrite decide_False; last done.
    by iApply zerosT_nullT.
  Qed.

  Lemma type_from_zeros_structT X (sl : struct_layout) Ts Φ n (ns : list Z) :
    Simpl (member_sizes sl) ns →
    type_from_zeros_all X ns Ts (λ P, Φ (λ x, P x ∗ ⌜n = ly_size sl⌝))
    ⊢ type_from_zeros X n (λ x, structT sl (Ts x)) Φ.
  Proof.
    rewrite /Simpl/type_from_zeros_all/type_from_zeros => <-.
    iIntros "(%Ps&%&%Heq&%Hpers&#HPs&#HQ&?)".
    iExists _. iFrame.
    iSplit; [iPureIntro; apply _|].
    iIntros "!>" (??) "[#? %] Hz". subst.
    iDestruct ("HQ" with "[$]") as "Hl".
    rewrite zerosT_to_structT_member_layouts.
    iApply (struct_mono with "Hz").
    iDestruct "HPs" as "-#HPs".
    iAssert ([∗ list] i↦n;P ∈ member_sizes sl;Ps, ∀ lv0, P x -∗
        lv0 ◁ₓ (zeros[n]) -∗ lv0 ◁ₓ (Ts x !!! i))%I with "[HPs]" as "HPs".
    { iApply (big_sepL2_impl with "HPs"). iIntros "!>" (?????) "HP".
      iIntros (?) "?". by iApply "HP". }
    move: (Heq x). move: (Ts x) => T {}Heq. iClear "HQ".
    iInduction (member_sizes sl) as [] "IH" forall (T Ps Heq); destruct T => //; csimpl.
    iDestruct (big_sepL2_cons_inv_l with "HPs") as (?? ->) "[Hw ?]" => /=. simpl in Heq.
    iDestruct "Hl" as "[??]".
    iDestruct ("IH" with "[] [$] [$]") as "$". { eauto with lia. }
    iIntros (??). by iApply "Hw".
  Qed.

  Lemma type_from_zeros_array A X (T : A → type Σ) Φ n m xs :
    type_offset_to_idx n m (λ l, type_from_zeros A m T (λ P,
        Φ (λ x, ∃ y, P y ∗ ⌜0 ≤ m⌝ ∗
            ⌜(∀ a : A, T a `has_size` m)⌝ ∗ ⌜xs x = replicate (Z.to_nat l) y⌝)))
    ⊢ type_from_zeros X n (λ x, arrayT T m (xs x)) Φ.
  Proof.
    rewrite /type_offset_to_idx/type_from_zeros.
    iIntros "(%j&%&%P&%Hpers&#Hwand&Hcont)". iExists _. iFrame. simplify_eq.
    iSplit; [iPureIntro; apply _|].
    iIntros "!>" (lv x) "[%y [#HP [% [% ->]]]] Hz".
    rewrite -arrayT_type_arrayT; [|done..].
    iDestruct (zerosT_non_negative with "Hz") as %?.
    replace (m * j) with (Z.of_nat (Z.to_nat j) * Z.of_nat (Z.to_nat m)) by lia.
    rewrite -array_replicate_zeros_equiv.
    iApply (type_arrayT_mono with "Hz").
    rewrite big_sepL2_fmap_r big_sepL2_replicate_l ?replicate_length //.
    iApply big_sepL_intro. iIntros "!>" (?? [-> ?]%lookup_replicate ??). rewrite Z2Nat.id; [|lia].
    by iApply "Hwand".
  Qed.

End zeros.

Global Typeclasses Opaque
  type_ptr_offset
  type_project_slice
  type_offset_to_idx.

Global Typeclasses Opaque
  type_use_at_idx
  type_assign_at_idx
  type_borrow_at_idx.
