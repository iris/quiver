
From quiver.thorium.types Require Export types base_types combinators.


Section pointer_types.
  Context {Σ} `{!refinedcG Σ}.


  (* the null type *)
  Definition nullT_def := ex v, { value[ptr_size] v | val_to_loc v = Some NULL_loc }.
  Definition nullT_aux : nullT_def.(seal).
  Proof. by eexists. Qed.
  Definition nullT := nullT_aux.(unseal).
  Definition nullT_eq : nullT = nullT_def :=
  seal_eq nullT_aux.

  Lemma vtr_null:
    ⊢ NULL ◁ᵥ nullT.
  Proof.
    rewrite nullT_eq /nullT_def //=.
    iApply vtr_existsT_iff. iExists NULL_loc.
    iApply vtr_constraintT_iff. iSplit; first by iApply vtr_valueT_intro.
    iPureIntro. by apply val_to_of_loc.
  Qed.

  Lemma val_of_to_loc_null_bytes :
    val_to_loc NULL_bytes = Some NULL_loc.
  Proof.
    rewrite /val_to_loc /NULL_bytes /ptr_size /=.
    rewrite bool_decide_decide; destruct decide; naive_solver.
  Qed.

  Lemma vtr_null_bytes:
    ⊢ NULL_bytes ◁ᵥ nullT.
  Proof.
    rewrite nullT_eq /nullT_def //=.
    iApply vtr_existsT_iff. iExists NULL_bytes.
    iApply vtr_constraintT_iff. iSplit; first by iApply vtr_valueT_intro.
    iPureIntro. by apply  val_of_to_loc_null_bytes.
  Qed.

  Lemma vtr_nullT_iff v :
    v ◁ᵥ nullT ⊣⊢ ⌜val_to_loc v = Some NULL_loc⌝.
  Proof.
    rewrite nullT_eq/nullT_def.
    rewrite vtr_existsT_iff. setoid_rewrite vtr_constraintT_iff.
    iSplit.
    - iIntros "(%w & Hw & %Ha)".
      rewrite vtr_valueT_elim. iDestruct "Hw" as "(-> & _)". done.
    - iIntros (Ha). iExists v. iSplitL; last done.
      iApply vtr_valueT_intro. iSplitR; first done.
      rewrite /has_size_val.
      rewrite val_to_loc_length; first done. eauto.
  Qed.

  Lemma nullT_inv w:
    w ◁ᵥ nullT ⊢ ⌜val_to_loc w = Some NULL_loc⌝.
  Proof. rewrite vtr_nullT_iff//. Qed.

  Global Instance nullT_ptr_type : PtrType nullT.
  Proof.
    rewrite nullT_eq. eapply existsT_ptr_type => w. split.
    - eapply constraintT_has_size, valueT_has_size => //.
    - intros v. rewrite vtr_constraintT_iff vtr_valueT_elim.
      iIntros "Hv". iDestruct "Hv" as "[[-> _] %Heq]". iExists NULL_loc.
      by iPureIntro.
  Qed.

  Global Instance nullT_copy : Copy nullT.
  Proof.
    split. rewrite nullT_eq /nullT_def; eapply existsT_copy. intros x.
    eapply constraintT_copy. eapply valueT_copy.
  Qed.

  Global Instance type_params_null : TypeParams nullT unit () (λ _, nullT) (λ _ _, True) | 1.
  Proof.
    intros n' ?; simpl in *; by subst.
  Qed.

  (* The own type *)
  Program Definition ownT_def (p: loc) (A: type Σ): type Σ :=
    {|
      ty_own l := (l ↦ p ∗ p ◁ₗ A)%I;
      ty_own_val v := (⌜v = p⌝ ∗ p ◁ₗ A)%I;
      ty_has_size n := n = ptr_size |}.
  Next Obligation.
    iIntros (p A ot v Hop) "(-> & Hp)"; simpl; done.
  Qed.
  Next Obligation.
    iIntros (p A ot l Hot) "(Hl & Hp)"; iExists _; iFrame; done.
  Qed.
  Next Obligation.
    iIntros (p A l v) "Hl [-> Hp]"; simpl; by iFrame.
  Qed.

  Definition ownT_aux : ownT_def.(seal).
  Proof. by eexists. Qed.
  Definition ownT := ownT_aux.(unseal).
  Definition ownT_eq : ownT = ownT_def :=
    seal_eq ownT_aux.

  Global Instance ownT_ptr_type A p : PtrType (ownT p A).
  Proof.
    rewrite ownT_eq /ownT_def; split; simpl.
    - naive_solver.
    - iIntros (v) "[-> _]". iExists _. iPureIntro.
      eapply val_to_of_loc.
  Qed.

  Lemma ownT_has_size p A n:
    (ownT p A) `has_size` n ↔ n = ptr_size.
  Proof.
    rewrite ownT_eq /ownT_def /=. lia.
  Qed.

  Lemma ownT_ty_own_val_own v p A:
    v ◁ᵥ (ownT p A) ⊣⊢ (⌜v = p⌝ ∗ (p ◁ₗ A))%I.
  Proof.
    rewrite ownT_eq //=.
  Qed.

  Global Instance non_null_val_own p A:
    NonNull A →
    NonNullVal (ownT p A).
  Proof.
    rewrite /NonNull /NonNullVal.
    iIntros (Hent v) "Hv". rewrite ownT_ty_own_val_own.
    iDestruct "Hv" as "[-> Hp]". iExists _. iSplit; first done.
    by iApply Hent.
  Qed.


  Global Instance ownT_le p :
    Proper ((⊑) ==> (⊑)) (ownT p).
  Proof. rewrite ownT_eq /ownT_def. solve_type_proper. Qed.

  Global Instance ownT_proper p :
    Proper ((≡) ==> (≡)) (ownT p).
  Proof. rewrite ownT_eq /ownT_def. solve_type_proper. Qed.

  Lemma ownT_is_ty p A B:
    A `is_ty` B →
    ownT p A `is_ty` ownT p B.
  Proof.
    rewrite !is_ty_ty_le. eapply ownT_le.
  Qed.


  Lemma valueT_ownT (l r: loc) B:
    l ◁ₗ valueT r ptr_size ∗ r ◁ₗ B ⊣⊢ l ◁ₗ ownT r B.
  Proof.
    rewrite valueT_eq /valueT_def //=.
    rewrite ownT_eq /ownT_def //=.
    iSplit.
    - iIntros "[(Hl & Hly) Hr]". iFrame.
    - iIntros "(Hl & Hr)". iFrame. done.
  Qed.

  Lemma ownT_placeT_valueT l:
    ownT l (placeT l) ≡ valueT l ptr_size.
  Proof.
    split; intros ?.
    - rewrite -valueT_ownT. iSplit.
      + iIntros "[$ _]".
      + iIntros "$". by iApply placeT_create.
    - rewrite ownT_ty_own_val_own. iSplit.
      + iIntros "[-> _]". iApply vtr_valueT_intro.
        iPureIntro; split_and!; eauto with quiver_logic.
      + iIntros "Hv". rewrite vtr_valueT_elim.
        iDestruct "Hv" as "[$ _]". iApply placeT_create.
  Qed.

  Lemma value_own_xtr x (l: loc) A:
    x ◁ₓ (valueT l ptr_size) ∗ l ◁ₗ A ⊣⊢ x ◁ₓ ownT l A.
  Proof.
    destruct x as [r|v]; simpl.
    - rewrite -valueT_ownT //.
    - rewrite ownT_ty_own_val_own. iSplit.
      + iIntros "[Hv $]". iDestruct (vtr_valueT_elim with "Hv") as "[$ _]".
      + iIntros "[-> $]".
        iApply vtr_valueT_intro.
        iPureIntro; split_and!; eauto with quiver_logic.
  Qed.


  Global Instance type_params_own p A :
    TypeParams (ownT p A) (loc * type Σ) (p, A) (λ p, ownT p.1 p.2) (prod_relation eq (flip is_ty)) | 1.
  Proof.
    intros [p' A'] [? Hty]; simpl in *. subst.
    destruct Hty as [Hty]. split. by setoid_rewrite Hty.
  Qed.


  Lemma xtr_moved_own x p A:
    x ◁ₓ movedT (ownT p A) ⊢ x ◁ₓ valueT p ptr_size.
  Proof.
    rewrite movedT_eq /movedT_def.
    rewrite xtr_existsT_iff. setoid_rewrite xtr_existsT_iff.
    setoid_rewrite xtr_sepT_iff. iDestruct 1 as "(%n & %w & Hv & HP)".
    iDestruct ("HP" $! (w ◁ᵥ valueT p ptr_size) with "[]") as "#Hw".
    { iPureIntro. rewrite /vty_persist. pose proof (value_own_xtr (VAL w)) as Heq.
      simpl in Heq. rewrite -Heq. iIntros "[#Hw _]". by iModIntro. }
    by iApply (valueT_elim_xtr with "Hv Hw").
  Qed.

  (* the optional type *)
  Definition optionalT (φ: dProp) (A: type Σ) :=
    case φ A nullT.

  Global Typeclasses Opaque optionalT.

  Global Instance optionalT_ptr_type {A} `{!PtrType A} φ : PtrType  (optionalT φ A).
  Proof. rewrite /optionalT /case; destruct decide; apply _. Qed.


  Global Instance optionalT_le φ:
    Proper ((⊑) ==> (⊑)) (optionalT φ).
  Proof.
    rewrite /optionalT /case; destruct decide; solve_type_proper.
  Qed.

  Global Instance optionalT_proper :
    Proper (dprop_iff ==> (≡) ==> (≡)) (optionalT).
  Proof.
    rewrite /optionalT. intros φ1 φ2 Hφ A1 A2 HA.
    rewrite Hφ HA //.
  Qed.

  Global Instance optionalT_is_ty:
    Proper (dprop_iff ==> (is_ty) ==> (is_ty)) (optionalT).
  Proof.
    rewrite /optionalT. intros φ1 φ2 Hφ A1 A2 HA.
    rewrite Hφ. rewrite /case; destruct decide; auto.
    reflexivity.
  Qed.

  Lemma optionalT_iff_eq (φ ψ: dProp) A:
    (φ ↔ ψ) → optionalT φ A ≡ optionalT ψ A.
  Proof.
    intros [H1 H2]. rewrite /optionalT /case.
    destruct decide, decide; eauto; naive_solver.
  Qed.

  Global Instance type_params_optional φ A :
    TypeParams (optionalT φ A) (dProp * type Σ) (φ, A) (λ p, optionalT p.1 p.2) (prod_relation dprop_iff (flip is_ty)) | 1.
  Proof.
    intros [φ' A'] [Hiff Hty]; simpl in *. subst.
    destruct Hty as [Hty]. split. setoid_rewrite Hty.
    by setoid_rewrite Hiff.
  Qed.


  (* the raw pointer type *)
  Definition ptrT_def (l: loc) (len: Z) (i: Z) :=
    constraintT
      (sepT
        (placeT (l +ₗ i))
        (loc_in_bounds l (Z.to_nat len)))
      (0 ≤ i ≤ len).
  Definition ptrT_aux : ptrT_def.(seal).
  Proof. by eexists. Qed.
  Definition ptrT := ptrT_aux.(unseal).
  Definition ptrT_eq : ptrT = ptrT_def := seal_eq ptrT_aux.


  Global Instance ptr_persistent l r len i : Persistent (l ◁ₗ ptrT r len i).
  Proof.
    rewrite ptrT_eq /ptrT_def /=. apply _.
  Qed.

  Lemma ltr_ptrT_iff l r len i :
    l ◁ₗ ptrT r len i ⊣⊢ ⌜l = r +ₗ i⌝ ∗ loc_in_bounds r (Z.to_nat len) ∗ ⌜0 ≤ i ≤ len⌝.
  Proof.
    rewrite ptrT_eq /ptrT_def.
    rewrite ltr_constraintT_iff ltr_sepT_iff.
    rewrite bi.sep_assoc. f_equiv. f_equiv.
    rewrite placeT_eq /placeT_def /=.
    iSplit; by iIntros "->".
  Qed.

  Lemma ltr_ptrT_inv l r len i :
    l ◁ₗ ptrT r len i -∗ ⌜l = r +ₗ i⌝.
  Proof.
    rewrite ltr_ptrT_iff. iDestruct 1 as "(%H & _ & _)". done.
  Qed.

  Lemma raw_pointer_intro l A len:
    A `has_bounds` len →
    l ◁ₗ A ⊢ l ◁ₗ ptrT l len 0.
  Proof.
    rewrite ltr_ptrT_iff shift_loc_0. intros Hb.
    iIntros "Hl". iDestruct (Hb with "Hl") as "[Hb %Hle]". iFrame.
    iSplit; first done. iPureIntro. lia.
  Qed.

  Lemma raw_pointer_shift j l r n i:
    0 ≤ i + j ≤ n →
    l ◁ₗ ptrT r n i ⊢ (l +ₗ j) ◁ₗ ptrT r n (i + j).
  Proof.
    iIntros (Hbounds) "Hl". rewrite !ltr_ptrT_iff.
    iDestruct "Hl" as "(-> & Hb & %Hbounds')". iFrame. iSplit.
    - iPureIntro. rewrite shift_loc_assoc //.
    - iPureIntro. lia.
  Qed.

  Lemma raw_pointer_loc_in_bounds l r len i:
    l ◁ₗ ptrT r len i ⊢ loc_in_bounds l 0.
  Proof.
    rewrite ltr_ptrT_iff. iIntros "Hl". iDestruct "Hl" as "(-> & Hb & %Hbounds')".
    iApply loc_in_bounds_shift_loc; last iFrame. lia.
  Qed.

  Lemma raw_pointer_has_bounds r len i :
    (ptrT r len i) `has_bounds` (len - i).
  Proof.
    iIntros (l) "Ha".
    rewrite ltr_ptrT_iff. iDestruct "Ha" as "(-> & Hlb & %Ha)".
    iSplitL; last by iPureIntro; lia.
    iApply (loc_in_bounds_shift_loc); last done. lia.
  Qed.

  (* mini-optionals *)
  Definition optT (v: val) : type Σ :=
    sepT (valueT v ptr_size) (∃ l: loc, ⌜val_to_loc v = Some l⌝ ∗ (⌜l = NULL_loc⌝ ∨ loc_in_bounds l 0)).
  Global Typeclasses Opaque optT.

  Global Instance type_params_opt v:
    TypeParams (optT v) val v (λ w, optT w) eq | 2.
  Proof.
    intros w ->. done.
  Qed.

  Global Instance optT_copy v: Copy (optT v).
  Proof.
    rewrite /optT. apply _.
  Qed.

  Lemma optT_has_size v n:
    optT v `has_size` n ↔ n = ptr_size.
  Proof.
    rewrite /optT sepT_has_size valueT_has_size //.
  Qed.

  Global Instance optT_ptr_type v : PtrType (optT v).
  Proof.
    split.
    - by eapply optT_has_size.
    - iIntros (w) "Hw".
      rewrite /optT vtr_sepT_iff. iDestruct ("Hw") as "[Hw Hb]".
      iDestruct (vtr_valueT_elim with "Hw") as "[-> _]".
      iDestruct ("Hb") as "(%l & ? & ?)". iExists _. iFrame.
  Qed.

  Global Instance optT_non_null v: NonNull (optT v).
  Proof.
    rewrite /optT. apply _.
  Qed.

  Definition not_null (v: val) : dProp := @DProp (val_to_loc v ≠ Some NULL_loc) _.

  Lemma non_null_not_null v A:
    ⌜NonNullVal A⌝ ∗ v ◁ᵥ A ⊢ ⌜not_null v⌝.
  Proof.
    iIntros "[%Hnn Hv]". iDestruct (Hnn with "Hv") as "(%l & -> & Hb)".
    rewrite /not_null /=. iDestruct (loc_in_bounds_null_loc with "Hb") as "%Hneq".
    iPureIntro. intros Heq. eapply Hneq. rewrite val_to_of_loc in Heq. naive_solver.
  Qed.

  Lemma null_not_not_null v:
    v ◁ᵥ nullT ⊢ ⌜¬ not_null v⌝.
  Proof.
    iIntros "Hv". iDestruct (nullT_inv with "Hv") as "%Hl".
    iPureIntro. rewrite /not_null /=. rewrite Hl. naive_solver.
  Qed.

  Lemma optional_not_null v A φ:
    ⌜NonNullVal A⌝ ∗ v ◁ᵥ optionalT φ A ⊢ ⌜φ ↔ not_null v⌝.
  Proof.
    rewrite /optionalT /case. destruct decide.
    - rewrite  non_null_not_null. iIntros "%Hn".
      iPureIntro. naive_solver.
    - rewrite null_not_not_null. iIntros "%Hn".
      iPureIntro. naive_solver.
  Qed.

  Lemma optT_not_null v w:
    v ◁ᵥ optT w ⊢ case (not_null w) (∃ l: loc, ⌜val_to_loc v = Some l⌝ ∗ loc_in_bounds l 0) (⌜val_to_loc v = Some NULL_loc⌝)%I.
  Proof.
    rewrite /optT vtr_sepT_iff. iIntros "[Hv Hl]".
    iDestruct "Hl" as "(%l & %Heq & Hor)".
    iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
    rewrite /case /not_null /=. destruct decide.
    - iDestruct "Hor" as "[%Hn|Hb]"; first by naive_solver.
      iExists _. by iFrame.
    - destruct (decide (val_to_loc w = Some NULL_loc)); last naive_solver.
      simplify_eq. by iPureIntro.
  Qed.


  Lemma null_opt v:
    v ◁ᵥ nullT ⊢ v ◁ᵥ optT v.
  Proof.
    iIntros "Hv". rewrite /optT.
    rewrite vtr_sepT_iff.
    iDestruct (nullT_inv with "Hv") as "%Hvl".
    iSplit; last by naive_solver. iApply vtr_valueT_intro.
    iSplit; first done. iPureIntro. eapply val_of_to_loc in Hvl as [|[??]]; subst.
    + eauto with quiver_logic.
    + done.
  Qed.

  Lemma non_null_opt v A:
    ⌜NonNullVal A⌝ ∗ v ◁ᵥ A ⊢ v ◁ᵥ optT v.
  Proof.
    rewrite /NonNullVal. iIntros "[%Hnn Hv]". rewrite /optT.
    rewrite vtr_sepT_iff.
    iDestruct (Hnn with "Hv") as "(%l & -> & Hb)".
    iSplit; last first.
    { iExists l. iFrame. iPureIntro. eauto with quiver_logic. }
    iApply vtr_valueT_intro.
    iSplit; first done. iPureIntro. eauto with quiver_logic.
  Qed.

  Lemma optional_opt v φ A:
    ⌜NonNullVal A⌝ ∗ v ◁ᵥ optionalT φ A ⊢ v ◁ᵥ optT v.
  Proof.
    rewrite /optionalT /case. destruct decide.
    - rewrite non_null_opt //.
    - rewrite null_opt. iIntros "[_ $]".
  Qed.

  Lemma opt_elim v w A:
    v ◁ᵥ optT w ∗ w ◁ᵥ A ⊢ v ◁ᵥ A.
  Proof.
    rewrite /optT. iIntros "[Hv Hw]".
    rewrite vtr_sepT_iff. iDestruct "Hv" as "[Hv _]".
    iApply (valueT_elim_val with "Hv Hw").
  Qed.

End pointer_types.

Global Typeclasses Opaque not_null.
Global Opaque not_null.

Notation owned := (ownT).
Notation null := (nullT).
Notation opt := (optT).
Notation optional := (optionalT).
Notation "'ptr[' len ']' r i" := (ptrT r len i) (at level 60, format "ptr[ len ]  r  i") : ref_type_scope.
