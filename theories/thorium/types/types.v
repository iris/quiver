From caesium Require Export lifting proofmode tactics notation.
From quiver.base Require Export facts.
Set Default Proof Using "Type".

Section types.
  Context {Σ} `{!refinedcG Σ}.

  Definition has_size_val (v: val) (n: Z) : Prop :=
    n = length v.

  Record type : Type := {
    ty_has_size : Z → Prop;
    ty_own : loc → iProp Σ;
    ty_own_val : val → iProp Σ;
    ty_size_eq n v :
      ty_has_size n →
      ty_own_val v -∗ ⌜has_size_val v n⌝;
    ty_move n l:
      ty_has_size n →
      ty_own l -∗ ∃ v, l ↦ v ∗ ty_own_val v;
    ty_move_back l v:
      l ↦ v -∗
      ty_own_val v -∗
      ty_own l;
  }.

  Class PtrType (A: type) :=
    {
      ptr_type_size : ty_has_size A ptr_size;
      ptr_type_val_loc v : A.(ty_own_val) v ⊢ ∃ l: loc, ⌜val_to_loc v = Some l⌝
    }.

  Class NonNull (A: type) :=
    is_not_null l : ty_own A l ⊢ loc_in_bounds l 0.

  Class NonNullVal (A: type) :=
    non_null_val v : ty_own_val A v ⊢ ∃ l: loc, ⌜v = l⌝ ∗ loc_in_bounds l 0.


  (* [Copy] types do not require their ownership
     to be moved out of memory upon read.
  *)
  Class Copy (A: type) := {
    copy_rtype_persistent v: Persistent (A.(ty_own_val) v)
  }.
  Global Existing Instance copy_rtype_persistent.


  (* the empty type used to show type is inhabited *)
  Local Program Definition empty_type : type :=
    {| ty_has_size n := False; ty_own l := False%I; ty_own_val v := False%I |}.
  Next Obligation. done. Qed.
  Next Obligation. done. Qed.
  Next Obligation. iIntros (l v) "Hl %HF". done. Qed.

  Global Instance type_inhabited : Inhabited type.
  Proof. split. exact empty_type. Qed.

  Lemma ty_own_loc_in_bounds l n A:
    ty_has_size A n →
    A.(ty_own) l ⊢ loc_in_bounds l (Z.to_nat n).
  Proof.
    iIntros (Hly) "Hl".
    iDestruct (ty_move with "Hl") as "[%v [Hl Hv]]"; first done.
    iDestruct (heap_mapsto_loc_in_bounds with "Hl") as "Hb".
    iDestruct (ty_size_eq with "Hv") as %Hsz; first done.
    rewrite /has_size_val in Hsz.
    rewrite Hsz Nat2Z.id //.
  Qed.

  Lemma ty_own_ty_val_ent l A B n1 n2 :
    A.(ty_has_size) n1 →
    B.(ty_has_size) n2 →
    (∀ v, A.(ty_own_val) v ⊢ B.(ty_own_val) v) →
    A.(ty_own) l ⊢ B.(ty_own) l.
  Proof.
    iIntros (Hty1 Hty2 Hval) "Hl".
    iDestruct (ty_move with "Hl") as "[%v [Hl Hv]]"; first done.
    rewrite Hval.
    iDestruct (ty_move_back with "Hl Hv") as "Hl"; eauto.
  Qed.

  Lemma ty_own_ty_val_equiv l A B n1 n2 :
    A.(ty_has_size) n1 →
    B.(ty_has_size) n2 →
    (∀ v, A.(ty_own_val) v ⊣⊢ B.(ty_own_val) v) →
    A.(ty_own) l ⊣⊢ B.(ty_own) l.
  Proof.
    iIntros (Hty1 Hty2 Hval).
    iSplit; iApply ty_own_ty_val_ent; eauto.
    - iIntros (v); rewrite Hval; done.
    - iIntros (v); rewrite Hval; done.
  Qed.

  Definition ty_has_bounds (A: type) (n: Z) : Prop :=
    ∀ l, A.(ty_own) l ⊢ loc_in_bounds l (Z.to_nat n) ∗ ⌜0 ≤ n⌝.


End types.

Global Arguments type _ {_}.

Declare Scope ref_type_scope.
Delimit Scope ref_type_scope with RT.
Bind Scope ref_type_scope with type.
Global Open Scope ref_type_scope.

Global Arguments ty_own {_ _} _%RT _.
Global Arguments ty_own_val {_ _} _%RT _.

Notation "v ◁ᵥ A" := (ty_own_val A%RT v) (at level 15).
Notation "l ◁ₗ A" := (ty_own A%RT l) (at level 15).
Notation "A `has_size` n" := (ty_has_size A%RT n) (at level 15) : type_scope.
Notation "A `has_size` n" := (⌜A `has_size` n⌝)%I (at level 15) : stdpp_scope.
Notation "A `has_bounds` n" := (ty_has_bounds A%RT n) (at level 15) : type_scope.
Notation "A `has_bounds` n" := (⌜A `has_bounds` n⌝)%I (at level 15) : stdpp_scope.
Notation "v `has_size_val` n" := (has_size_val v n) (at level 15) : type_scope.
Notation "v `has_size_val` n" := (⌜has_size_val v n⌝)%I (at level 15) : stdpp_scope.


Section mono.
  Context {Σ: gFunctors} `{!refinedcG Σ}.

  Inductive type_le' (ty1 ty2 : type Σ) : Prop :=
    Type_le :
      (* We omit [ty_has_op_type] on purpose as it is not preserved by fixpoints. *)
      (∀ l, ty1.(ty_own) l ⊢ ty2.(ty_own) l) →
      (∀ v, ty1.(ty_own_val) v ⊢ ty2.(ty_own_val) v) →
      type_le' ty1 ty2.
  Global Instance type_le : SqSubsetEq (type Σ) := type_le'.

  Inductive type_equiv' (ty1 ty2 : type Σ) : Prop :=
    Type_equiv :
      (* We omit [ty_has_op_type] on purpose as it is not preserved by fixpoints. *)
      (∀ l, ty1.(ty_own) l ≡ ty2.(ty_own) l) →
      (∀ v, ty1.(ty_own_val) v ≡ ty2.(ty_own_val) v) →
      type_equiv' ty1 ty2.
  Global Instance type_equiv : Equiv (type Σ) := type_equiv'.

  Global Instance type_equiv_antisym :
    AntiSymm (≡@{type Σ}) (⊑).
  Proof. move => ?? [??] [??]. split; intros; by apply (anti_symm (⊢)). Qed.

  Global Instance type_le_preorder : PreOrder (⊑@{type Σ}).
  Proof.
    constructor.
    - done.
    - move => ??? [??] [??].
      constructor => *; (etrans; [match goal with | H : _ |- _ => apply H end|]; done).
  Qed.

  Global Instance type_equivalence : Equivalence (≡@{type Σ}).
  Proof.
    constructor.
    - done.
    - move => ?? [??]. constructor => *; by symmetry.
    - move => ??? [??] [??].
      constructor => *; (etrans; [match goal with | H : _ |- _ => apply H end|]; done).
  Qed.

  Global Instance ty_le_proper : Proper ((≡) ==> (≡) ==> iff) (⊑@{type Σ}).
  Proof.
    move => ?? [Hl1 Hv1] ?? [Hl2 Hv2].
    split; move => [??]; constructor; intros.
    - by rewrite -Hl1 -Hl2.
    - by rewrite -Hv1 -Hv2.
    - by rewrite Hl1 Hl2.
    - by rewrite Hv1 Hv2.
  Qed.

  Lemma type_le_equiv_list (f : list (type Σ) → type Σ) :
    Proper (Forall2 (⊑) ==> (⊑)) f →
    Proper (Forall2 (≡) ==> (≡)) f.
  Proof.
    move => HP ?? Heq. apply (anti_symm (⊑)); apply HP.
    2: symmetry in Heq.
    all: by apply: Forall2_impl; [done|] => ?? ->.
  Qed.

  Global Instance ty_own_le : Proper ((⊑) ==> eq ==> (⊢)) ty_own.
  Proof. intros ?? EQ ??->. apply EQ. Qed.
  Global Instance ty_own_proper : Proper ((≡) ==> eq ==> (≡)) ty_own.
  Proof. intros ?? EQ ??->. apply EQ. Qed.
  Lemma ty_own_entails `{!typeG Σ} ty1 ty2 l:
    ty1 ≡@{type Σ} ty2 →
    ty_own ty1 l ⊢ ty_own ty2 l.
  Proof. by move => [-> ?]. Qed.

  Global Instance ty_own_val_le : Proper ((⊑) ==> eq ==> (⊢)) ty_own_val.
  Proof. intros ?? EQ ??->. apply EQ. Qed.
  Global Instance ty_own_val_proper : Proper ((≡) ==> eq ==> (≡)) ty_own_val.
  Proof. intros ?? EQ ??->. apply EQ. Qed.

End mono.
Notation TypeMono T := (Proper (pointwise_relation _ (⊑) ==> pointwise_relation _ (⊑)) T).

(* tactics for simplifying types *)
Ltac simpl_type_core_refinement := fail.

Ltac simpl_type_core :=
  match goal with
  | |- context C [ty_own {| ty_own := ?f |}] => let G := context C [f] in change G
  | |- context C [ty_own_val {| ty_own_val := ?f |}] => let G := context C [f] in change G
  | |- _ => simpl_type_core_refinement
  end.

Ltac simpl_type :=  simpl; repeat simpl_type_core; simpl.


Ltac unfold_type_equiv_refinement := fail.

Ltac unfold_type_equiv :=
  lazymatch goal with
  | |- {| ty_own := _ |} ⊑ {| ty_own := _ |} =>
      constructor => *; simpl_type
  | |- {| ty_own := _ |} ≡ {| ty_own := _ |} =>
      constructor => *; simpl_type
  | |- context [let '_ := ?x in _] => destruct x
  end.

Ltac solve_type_proper :=
  solve_proper_core ltac:(fun _ => first [ fast_reflexivity | f_contractive | unfold_type_equiv | f_equiv | reflexivity ]).



Section types_derived.
  Context `{!refinedcG Σ}.

  Lemma has_size_val_has_layout v ly:
    v `has_size_val` (ly_size ly) ↔ v `has_layout_val` ly.
  Proof.
    rewrite /has_size_val /has_layout_val //. lia.
  Qed.

  Lemma vtr_size_non_zero v A n:
    A `has_size` n →
    v ◁ᵥ A ⊢ ⌜0 ≤ n⌝.
  Proof.
    iIntros (Hsz) "Hv". iDestruct (ty_size_eq with "Hv") as "%Hsz'"; first done.
    rewrite /has_size_val in Hsz'. subst n. iPureIntro. lia.
  Qed.

  Lemma ltr_size_non_zero l A n:
    A `has_size` n →
    l ◁ₗ A ⊢ ⌜0 ≤ n⌝.
  Proof.
    iIntros (Hsz) "Hl". iDestruct (ty_move with "Hl") as "[%v [Hl Hv]]"; first done.
    by iApply (vtr_size_non_zero with "Hv").
  Qed.

  Lemma has_bounds_le (A B: type Σ) n :
    A `has_bounds` n →
    B ⊑ A →
    B `has_bounds` n.
  Proof.
    rewrite /ty_has_bounds. intros Hb Hsub.
    iIntros (l) "Hl". iApply Hb. rewrite Hsub //.
  Qed.

  Lemma has_size_has_bounds A n:
    A `has_size` n → A `has_bounds`  n.
  Proof.
    rewrite /ty_has_bounds. iIntros (Hs l) "Hl".
    iDestruct (ltr_size_non_zero with "Hl") as "#$"; first done.
    by iApply ty_own_loc_in_bounds.
  Qed.

End types_derived.




(* Location or Value Types *)
(* a type of locations or values to avoid duplication *)
Inductive loc_or_val_type := LOC_TYPE | VAL_TYPE.
Inductive loc_or_val : Type := LOC (l: loc) | VAL (v: val).
Inductive locs_or_vals : Type := LOCS (L: list loc) | VALS (V: list val).

Definition xtr {Σ} `{!refinedcG Σ} (x: loc_or_val) (A: type Σ) : iProp Σ :=
  match x with
  | LOC l => (A.(ty_own) l)
  | VAL v => (A.(ty_own_val) v)
  end.

Infix "◁ₓ" := xtr (at level 15).


Global Instance xtr_proper {Σ} `{!refinedcG Σ}:
  Proper ((=) ==> (≡) ==> (≡)) (@xtr Σ _).
Proof.
  intros ?? Heq ??. destruct x, y; simplify_eq; simpl; by intros ->.
Qed.


Definition loc_or_val_to_type (L: loc_or_val_type) : Type :=
  match L with
  | LOC_TYPE => loc
  | VAL_TYPE => val
  end.

Coercion loc_or_val_to_type : loc_or_val_type >-> Sortclass.

Definition lov {T: loc_or_val_type} : T → loc_or_val :=
  match T with
  | LOC_TYPE => λ l, LOC l
  | VAL_TYPE => λ v, VAL v
  end.

Definition lov_type (x: loc_or_val) : loc_or_val_type :=
  match x with
  | LOC _ => LOC_TYPE
  | VAL _ => VAL_TYPE
  end.

Section xtr_lemmas.
  Context {Σ} `{!refinedcG Σ}.

  Lemma type_le_xtr_intro (A B: type Σ) :
    (∀ x, x ◁ₓ A ⊢ x ◁ₓ B) → A ⊑ B.
  Proof.
    intros Hent. split.
    - intros l. eapply (Hent (LOC l)).
    - intros v. eapply (Hent (VAL v)).
  Qed.

  Lemma type_equiv_xtr_intro (A B: type Σ) :
    (∀ x, x ◁ₓ A ⊣⊢ x ◁ₓ B) → A ≡ B.
  Proof.
    intros Hent. split.
    - intros l. eapply (Hent (LOC l)).
    - intros v. eapply (Hent (VAL v)).
  Qed.

  Lemma xtr_size_non_zero x A n:
    A `has_size` n →
    x ◁ₓ A ⊢ ⌜0 ≤ n⌝.
  Proof.
    destruct x; simpl; eauto using vtr_size_non_zero, ltr_size_non_zero.
  Qed.

End xtr_lemmas.



(* Sometimes we have ownership left over from, for example, an assignment.
   The value will be irrelevant, since it cannot be accessed in the rest of the
   program, but it might still contain useful ownership. *)
Definition vty `{!refinedcG Σ} (A: type Σ) : iProp Σ := ∃ v, v ◁ᵥ A.
Global Typeclasses Opaque vty.



Section is_ty.
  Context `{!refinedcG Σ}.

  Record is_ty (A B: type Σ) : Prop := { is_ty_subtype: A ⊑ B }.

  Definition is_ty_all := Forall2 is_ty.

  Global Instance is_ty_preorder : PreOrder (is_ty).
  Proof.
    split.
    - intros x. by constructor.
    - intros x y z [Hxy] [Hzy]; split; by etrans.
  Qed.

  Global Instance is_ty_all_preorder : PreOrder (is_ty_all).
  Proof.
    unfold is_ty_all; apply _.
  Qed.

  Lemma is_ty_ty_le A B :
    is_ty A B ↔ A ⊑ B.
  Proof.
    split; by [intros [Hle]| econstructor].
  Qed.

  Lemma is_ty_trans A B C:
    is_ty A B → is_ty B C → is_ty A C.
  Proof.
    intros ??; by etrans.
  Qed.

  Global Instance ty_own_proper_is_ty : Proper (is_ty ==> (=) ==> (⊢)) (@ty_own Σ _) | 10.
  Proof. intros ?? EQ ??->. apply EQ. Qed.

  Global Instance ty_own_val_proper_is_ty : Proper (is_ty ==> (=) ==> (⊢)) (@ty_own_val Σ _) | 10.
  Proof. intros ?? EQ ?? ->. apply EQ. Qed.

  Global Instance xtr_own_proper_is_ty : Proper ((=) ==> is_ty ==> (⊢)) (@xtr Σ _) | 10.
  Proof. intros ?[|] -> ?? [EQ];  apply EQ. Qed.

  Lemma equiv_is_ty_all (As Bs: list (type Σ)) :
    As ≡ Bs → is_ty_all As Bs.
  Proof.
    intros EQ. induction EQ as [|A B As Bs Heq _ IH]; constructor; auto.
    split. rewrite Heq //.
  Qed.

  Lemma is_ty_all_le As Bs:
    is_ty_all As Bs ↔ Forall2 (⊑) As Bs.
  Proof.
    rewrite  /is_ty_all.
    split; induction 1; eauto using is_ty_subtype, Build_is_ty.
  Qed.

  Definition TypeParams (A: type Σ) (X: Type) (x: X) (B: X → type Σ) (sub: relation X) :=
    (∀ y, sub x y → is_ty (B y) A).

End is_ty.

Existing Class TypeParams.
Global Hint Mode TypeParams - - ! - - - - : typeclass_instances.

Notation "A `is_ty` B" := (is_ty A B) (at level 15) : type_scope.
Notation "A `is_ty_all` B" := (is_ty_all A B) (at level 15) : type_scope.





Section list_versions.
  Context `{!refinedcG Σ}.

  Definition ty_own_all (As: list (type Σ)) (Ls: list loc) : iProp Σ :=
    ([∗ list] l; A ∈ Ls; As, A.(ty_own) l)%I.

  Definition ty_own_val_all (As: list (type Σ)) (Vs: list val) : iProp Σ :=
    ([∗ list] v; A ∈ Vs; As, A.(ty_own_val) v)%I.

  Definition xtr_all (x: list loc_or_val) (As: list (type Σ)) : iProp Σ :=
    ([∗ list] x; A ∈ x; As, x ◁ₓ A)%I.


  Lemma ty_own_all_nil : ty_own_all [] [] ⊣⊢ True.
  Proof. rewrite /ty_own_all. by rewrite big_sepL2_nil. Qed.

  Lemma ty_own_all_cons (A: type Σ) (L: loc) (As: list (type Σ)) (Ls: list loc) :
    ty_own_all (A :: As) (L :: Ls) ⊣⊢ A.(ty_own) L ∗ ty_own_all As Ls.
  Proof. rewrite /ty_own_all. by rewrite big_sepL2_cons. Qed.

  Lemma ty_own_all_snoc (A: type Σ) (L: loc) (As: list (type Σ)) (Ls: list loc) :
    ty_own_all (As ++ [A]) (Ls ++ [L]) ⊣⊢ ty_own_all As Ls ∗ A.(ty_own) L.
  Proof. rewrite /ty_own_all big_sepL2_snoc //. Qed.

  Lemma ty_own_val_all_nil : ty_own_val_all [] [] ⊣⊢ True.
  Proof. rewrite /ty_own_val_all. by rewrite big_sepL2_nil. Qed.

  Lemma ty_own_val_all_cons (A: type Σ) (V: val) (As: list (type Σ)) (Vs: list val) :
    ty_own_val_all (A :: As) (V :: Vs) ⊣⊢ A.(ty_own_val) V ∗ ty_own_val_all As Vs.
  Proof. rewrite /ty_own_val_all. by rewrite big_sepL2_cons. Qed.

  Lemma ty_own_val_all_snoc (A: type Σ) (V: val) (As: list (type Σ)) (Vs: list val) :
    ty_own_val_all (As ++ [A]) (Vs ++ [V]) ⊣⊢ ty_own_val_all As Vs ∗ A.(ty_own_val) V.
  Proof. rewrite /ty_own_val_all big_sepL2_snoc //. Qed.

  Lemma xtr_all_nil : xtr_all [] [] ⊣⊢ True.
  Proof. rewrite /xtr_all. by rewrite big_sepL2_nil. Qed.

  Lemma xtr_all_cons (x: loc_or_val) (A: type Σ) (xs: list loc_or_val) (As: list (type Σ)) :
    xtr_all (x :: xs) (A :: As) ⊣⊢ x ◁ₓ A ∗ xtr_all xs As.
  Proof. rewrite /xtr_all. by rewrite big_sepL2_cons. Qed.

  Lemma xtr_all_snoc (x: loc_or_val) (A: type Σ) (xs: list loc_or_val) (As: list (type Σ)) :
    xtr_all (xs ++ [x]) (As ++ [A]) ⊣⊢ xtr_all xs As ∗ x ◁ₓ A.
  Proof. rewrite /xtr_all big_sepL2_snoc //. Qed.

  Lemma xtr_all_fmap_val Vs As: xtr_all (VAL <$> Vs) As ⊣⊢ ty_own_val_all As Vs.
  Proof.
    rewrite /xtr_all /ty_own_val_all.
    rewrite big_sepL2_fmap_l /= //.
  Qed.

  Lemma xtr_all_fmap_loc Ls As: xtr_all (LOC <$> Ls) As ⊣⊢ ty_own_all As Ls.
  Proof.
    rewrite /xtr_all /ty_own_all.
    rewrite big_sepL2_fmap_l /= //.
  Qed.


  Lemma ty_own_all_is_ty_all (As Bs: list (type Σ)) (Ls: list loc) :
    As `is_ty_all` Bs → ty_own_all As Ls ⊢ ty_own_all Bs Ls.
  Proof.
    induction 1 as [|A B As Bs Hty _ IH] in Ls |-*; first done.
    destruct Ls as [|L Ls]; first done.
    rewrite !ty_own_all_cons.
    iIntros "[HA HB]".
    rewrite IH. setoid_rewrite Hty. iFrame.
  Qed.

  Lemma ty_own_val_all_is_ty_all (As Bs: list (type Σ)) (Vs: list val) :
    As `is_ty_all` Bs → ty_own_val_all As Vs ⊢ ty_own_val_all Bs Vs.
  Proof.
    induction 1 as [|A B As Bs Hty _ IH] in Vs |-*; first done.
    destruct Vs as [|V Vs]; first done.
    rewrite !ty_own_val_all_cons.
    iIntros "[HA HB]".
    rewrite IH. setoid_rewrite Hty. iFrame.
  Qed.

  Lemma xtr_all_is_ty_all (xs: list loc_or_val) (As Bs: list (type Σ)) :
    As `is_ty_all` Bs → xtr_all xs As ⊢ xtr_all xs Bs.
  Proof.
    induction 1 as [|A B As Bs Hty _ IH] in xs |-*; first done.
    destruct xs as [|x xs]; first done.
    rewrite !xtr_all_cons.
    iIntros "[HA HB]".
    rewrite IH. setoid_rewrite Hty. iFrame.
  Qed.

  Lemma ty_own_all_equiv (As Bs: list (type Σ)) (Ls: list loc) :
    As ≡ Bs → ty_own_all As Ls ⊣⊢ ty_own_all Bs Ls.
  Proof.
    intros Heq. iSplit; iApply ty_own_all_is_ty_all; eauto using equiv_is_ty_all.
  Qed.

  Lemma ty_own_val_all_equiv (As Bs: list (type Σ)) (Vs: list val) :
    As ≡ Bs → ty_own_val_all As Vs ⊣⊢ ty_own_val_all Bs Vs.
  Proof.
    intros Heq. iSplit; iApply ty_own_val_all_is_ty_all; eauto using equiv_is_ty_all.
  Qed.

  Lemma xtr_all_equiv (xs: list loc_or_val) (As Bs: list (type Σ)) :
    As ≡ Bs → xtr_all xs As ⊣⊢ xtr_all xs Bs.
  Proof.
    intros Heq. iSplit; iApply xtr_all_is_ty_all; eauto using equiv_is_ty_all.
  Qed.

  Lemma has_size_has_layout_val As vs lys :
    Forall2 (λ A ly, A `has_size` (ly_size ly))%type As lys →
    ty_own_val_all As vs ⊢ ⌜Forall2 has_layout_val vs lys⌝.
  Proof.
    rewrite /ty_own_val_all.
    iIntros (Hly) "Hown". iInduction As as [|A As] "IH" forall (vs lys Hly).
    - destruct vs; last done. eapply Forall2_nil_inv_l in Hly. subst. by iPureIntro.
    - destruct vs; first done. eapply Forall2_cons_inv_l in Hly as (? & ? & ? & ? & ?).
      subst. iDestruct "Hown" as "[Hown Hown']".  iDestruct (ty_size_eq with "Hown") as "%Hsz"; first done.
      iDestruct ("IH" with "[//] Hown'") as "%Hly". iPureIntro. econstructor; eauto.
      by eapply has_size_val_has_layout.
  Qed.


End list_versions.
