From iris.algebra Require Import list.
From quiver.thorium.types Require Import types combinators base_types.
Set Default Proof Using "Type".


Section struct.
  Context `{!refinedcG Σ}.


  (* We state the sidecondition using foldr instead of Forall since this is faster to solve for the automation. *)
  Definition is_struct_size (sl : struct_layout) (tys : list (type Σ)) (n : Z) :=
    n = ly_size sl ∧
    Forall2 (λ A mem, A `has_size` ly_size (mem.2))%type tys (field_members sl.(sl_members)).


  Lemma is_struct_size_size sl tys n:
    is_struct_size sl tys n → n = ly_size sl.
  Proof. move => [?]. naive_solver. Qed.

  Lemma is_struct_size_tys_len sl tys n:
    is_struct_size sl tys n → length tys = length (field_names (sl_members sl)).
  Proof.
    unfold is_struct_size; intros [? ?].
    rewrite -field_members_length. by eapply Forall2_length.
  Qed.

  Lemma is_struct_size_forall sl tys n:
    is_struct_size sl tys n →
    n = ly_size sl ∧
    Forall2 (λ m ty, ty `has_size` (ly_size m.2))%type sl.(sl_members) (pad_struct sl.(sl_members) tys (λ ly, anyT (ly_size ly))).
  Proof.
    intros (Heq & Hall). split; auto. subst. induction (sl_members sl) as [|[[n|] ly]] in tys, Hall |-*; simpl.
    - by eapply Forall2_nil.
    - simpl in Hall. eapply Forall2_cons_inv_r in Hall as (ty & tys' & Hot & Hall & ->).
      eapply Forall2_cons; simpl. split; auto.
    - simpl in Hall. eapply Forall2_cons; split; eauto.
      apply anyT_has_size.
  Qed.

  Program Definition structT_def (sl : struct_layout) (tys : list (type Σ)) : type Σ := {|
    ty_has_size := is_struct_size sl tys;
    ty_own l :=
      ⌜length (field_names sl.(sl_members)) = length tys⌝ ∗
      loc_in_bounds l (sum_list (ly_size <$> (sl_members sl).*2)) ∗
      [∗ list] i↦ty∈pad_struct sl.(sl_members) tys (λ ly, anyT (ly_size ly)),
        (l +ₗ Z.of_nat (offset_of_idx sl.(sl_members) i)) ◁ₗ ty;
    ty_own_val v :=
      (⌜v `has_layout_val` sl⌝ ∗ ⌜length (field_names sl.(sl_members)) = length tys⌝ ∗
       [∗ list] v';ty∈reshape (ly_size <$> sl.(sl_members).*2) v;pad_struct sl.(sl_members) tys (λ ly, anyT (ly_size ly)), (v' ◁ᵥ ty))%I;
  |}%I.
  Next Obligation.
    iIntros (sl tys n v Heq%is_struct_size_size) "(%Hv&_)".
    rewrite /has_size_val Hv. iPureIntro. lia.
  Qed.
  Next Obligation.
    iIntros (sl tys n l Hop) "(_ & #Hb & Htys)".
    eapply is_struct_size_tys_len in Hop as Hlen.
    eapply is_struct_size_forall in Hop as (_ & Hall).
    rewrite Hlen. clear n Hlen.
    rewrite /=/layout_of{1}/has_layout_val {3}/ly_size.
    iInduction (sl_members sl) as [|[n ly] ms] "IH" forall (tys l Hall); csimpl.
    { iExists []. iSplitR; first by iApply heap_mapsto_nil. iSplit => //. }
    move: Hall => /=. intros (ty & tys' & ? &?&[=])%Forall2_cons_inv_l.
    rewrite shift_loc_0. iDestruct "Htys" as "[Hty Htys]".  cbn.
    iDestruct (loc_in_bounds_split with "Hb") as "[Hb1 Hb2]".
    setoid_rewrite <-shift_loc_assoc_nat.
    destruct n => /=.
    1: destruct tys => //; simplify_eq/=.
    all: iDestruct (ty_move with "Hty") as (v') "[Hl Hty]"; [subst; done|].
    all: iDestruct (ty_size_eq with "Hty") as %Hszv; [subst; done|].
    all: iDestruct ("IH" $! _ with "[] Hb2 Htys") as (vs') "(Hl' & Hsz & Hf & Htys)";
      try iPureIntro; simplify_eq/= => //.
    all: iDestruct "Hsz" as %Hsz; iDestruct "Hf" as %Hf.
    all: iExists (v' ++ vs').
    all: assert (length v' = ly_size ly) by (rewrite /has_size_val in Hszv; lia).
    all: rewrite heap_mapsto_app -Hsz take_app_alt // drop_app_alt // app_length; iFrame.
    all: rewrite Hszv; iFrame "Hl'".
    all: iPureIntro; eauto with lia.
  Qed.
  Next Obligation.
    move => sl tys l v. iIntros "Hl".
    rewrite /layout_of/has_layout_val{1}/ly_size /=.
    iDestruct 1 as (Hv Hcount) "Htys". do 1 iSplitR => //.
    iSplit. { rewrite -Hv. by iApply heap_mapsto_loc_in_bounds. }
    iInduction (sl_members sl) as [|[n ly] ms] "IH" forall (tys l v Hv Hcount); csimpl in * => //.
    iDestruct "Htys" as "[Hty Htys]".
    rewrite -(take_drop (ly_size ly) v).
    rewrite shift_loc_0 heap_mapsto_app take_app_alt ?take_length_le // ?Hv; try by cbn; lia.
    iDestruct "Hl" as "[Hl Hl']". cbn. simplify_eq/=.
    setoid_rewrite <-shift_loc_assoc_nat.
    iSplitR "Htys Hl'".
    - iClear "IH".
      destruct n; [destruct tys => //|] => /=; iDestruct (ty_move_back with "Hl Hty") as "$" => //.
    - destruct n => /=; rewrite -?fmap_tail; iApply ("IH" with "[] [] Hl' [Htys]") => //;
        iClear "IH"; try iPureIntro; rewrite ?drop_length; try lia.
      all: try by rewrite Hv /struct_size/offset_of_idx; csimpl; lia.
      1: destruct tys; naive_solver.
      all: rewrite drop_app_alt ?take_length// Hv; cbn; lia.
  Qed.


  Definition structT_aux : structT_def.(seal).
  Proof. by eexists. Qed.
  Definition structT := structT_aux.(unseal).
  Definition structT_eq : structT = structT_def :=
    seal_eq structT_aux.

  Lemma default_proper {A} (R : relation A) :
    Proper (R ==> option_Forall2 R ==> R) default.
  Proof. move => ?? ? [?|] [?|] //= Hopt; by inversion Hopt. Qed.

  Global Instance head_proper {A} (R : relation A): Proper (Forall2 R ==> option_Forall2 R) head.
  Proof. move => ?? [] * /=; by constructor. Qed.


  Global Instance structT_le : Proper ((=) ==> Forall2 (⊑) ==> (⊑)) (structT).
  Proof.
    rewrite structT_eq. move => ? sl -> tys1 tys2 Htys.
    have Hlen : length tys1 = length tys2 by apply: Forall2_length.
    constructor.
    - move => l; rewrite/ty_own/=/offset_of_idx.
      f_equiv. 1: f_equiv; by move: Htys => /Forall2_length->.
      f_equiv. clear Hlen.
      elim: (sl_members sl) tys1 tys2 Htys l => // -[m ?] s IH tys1 tys2 Htys l. csimpl.
      f_equiv.
      + do 2 f_equiv. apply default_proper; [done|]. by f_equiv.
      + setoid_rewrite <-shift_loc_assoc_nat; apply IH => //.
        destruct m, Htys => //. by f_equiv.
    - move => v. rewrite/ty_own_val/=. f_equiv. rewrite Hlen. f_equiv. clear Hlen.
      elim: (sl_members sl) v tys1 tys2 Htys => // -[m ?] s IH v tys1 tys2 Htys. csimpl.
      f_equiv.
      + do 2 f_equiv. apply default_proper; [done|]. by f_equiv.
      + apply IH. destruct m, Htys => //. by f_equiv.
  Qed.

  Global Instance struct_proper : Proper ((=) ==> Forall2 (≡) ==> (≡)) structT.
  Proof. move => ??-> ?? Heq. apply type_le_equiv_list; [by apply structT_le|done]. Qed.


  Lemma struct_focus l sl tys:
    l ◁ₗ structT sl tys ⊢
      ([∗ list] n;ty∈field_names sl.(sl_members);tys, l at{sl}ₗ n ◁ₗ ty) ∗
      (∀ tys', ([∗ list] n;ty∈field_names sl.(sl_members);tys', l at{sl}ₗ n ◁ₗ ty) -∗ l ◁ₗ structT sl tys').
  Proof.
    rewrite structT_eq {1 4}/ty_own/=. iIntros "Hs". iDestruct "Hs" as (Hcount) "[#Hb Hs]".
    rewrite /GetMemberLoc/offset_of_idx.
    have HND : (NoDup (field_names (sl_members sl))) by eapply bool_decide_unpack, sl_nodup.
    iInduction (sl_members sl) as [|[n ly] ms] "IH" forall (l tys Hcount HND). {
      destruct tys => //. iSplit => //. iIntros (tys') "Htys".
      iDestruct (big_sepL2_nil_inv_l with "Htys") as %->. iFrame. by iSplit.
    }
    csimpl. iDestruct "Hs" as "[Hl Hs]".
    iDestruct (loc_in_bounds_split with "Hb") as "[Hb1 Hb2]".
    setoid_rewrite <-shift_loc_assoc_nat.
    iDestruct ("IH" with "[] [] Hb2 Hs") as "[Hl1 Hs]"; try iPureIntro.
    { by destruct n, tys; naive_solver. }
    { destruct n => //. apply: NoDup_cons_1_2. naive_solver. }
    iClear "IH". destruct n; csimpl.
    - destruct tys => //=. rewrite offset_of_cons; eauto. case_decide => //=. iFrame.
      iSplitL "Hl1". {
        iApply (big_sepL2_impl with "Hl1"). iIntros "!#" (k n ty Hm ?) "Hl".
        move: Hm => /(elem_of_list_lookup_2 _ _ _) ?.
        rewrite offset_of_cons; eauto. case_decide; last by rewrite shift_loc_assoc_nat.
        move: HND => /= /(NoDup_cons_1_1 _ _). set_solver.
      }
      iIntros (tys') "Htys".
      iDestruct (big_sepL2_cons_inv_l with "Htys") as (?? ->)"[H1 Htys]".
      rewrite offset_of_cons; eauto. case_decide => //=. iFrame.
      iDestruct (big_sepL2_length with "Htys") as %<-. iSplitR => //.
      iSplit. { iApply loc_in_bounds_split. eauto. }
      iDestruct ("Hs" with "[Htys]") as (?) "[_ $]".
      iApply (big_sepL2_impl with "Htys"). iIntros "!#" (k n ty Hm ?) "Hl".
      move: Hm => /(elem_of_list_lookup_2 _ _ _) ?.
      rewrite offset_of_cons; eauto. case_decide; last by rewrite shift_loc_assoc_nat.
      move: HND => /= /(NoDup_cons_1_1 _ _). set_solver.
    - iFrame. iSplitL "Hl1". {
        iApply (big_sepL2_impl with "Hl1"). iIntros "!#" (k n ty Hm ?) "Hl".
        move: Hm => /(elem_of_list_lookup_2 _ _ _) ?.
        rewrite offset_of_cons; eauto. case_decide => //. by rewrite shift_loc_assoc_nat.
      }
      iIntros (tys') "Htys".
      iDestruct ("Hs" with "[Htys]") as (?) "[_ $]" => //; last by iSplit.
      iApply (big_sepL2_impl with "Htys"). iIntros "!#" (k n ty Hm ?) "Hl".
      move: Hm => /(elem_of_list_lookup_2 _ _ _) ?.
      rewrite offset_of_cons; eauto. case_decide => //. by rewrite shift_loc_assoc_nat.
  Qed.


  Lemma struct_mem_in_bounds l s T :
    (l ◁ₗ structT s T) ⊢ loc_in_bounds l (ly_size s).
  Proof.
    rewrite structT_eq. by iIntros "(%Hlen & #Hbounds & _)".
  Qed.

  Lemma struct_has_bounds s T:
    structT s T `has_bounds` (ly_size s).
  Proof.
    intros ?. rewrite struct_mem_in_bounds. iIntros "Hb".
    replace (Z.to_nat (ly_size s)) with (ly_size s) by lia.
    iSplit; first done. iPureIntro. lia.
  Qed.

  Global Instance structT_non_null sl L:
    NonNull (structT sl L).
  Proof.
    rewrite /NonNull.
    iIntros (l) "Hl".
    iApply loc_in_bounds_shorten; last first.
    { by iApply struct_mem_in_bounds. }
    lia.
  Qed.

  Lemma field_index_of_lookup L m i:
    field_index_of L m = Some i → field_names L !! i = Some m.
  Proof.
    induction L as [|[[l1|] l2] L] in i |-*; simpl.
    - naive_solver.
    - rewrite bool_decide_decide. destruct decide.
      + injection 1 as <-. done.
      + destruct i; first by destruct field_index_of; naive_solver.
        intros Hlook. simpl. apply IHL.
        destruct field_index_of; naive_solver.
    - rewrite option_fmap_id; eauto.
  Qed.



  Lemma anyT_structT_equiv l (s : struct_layout) :
    (l ◁ₗ anyT (ly_size s)) ⊣⊢ (l ◁ₗ structT s (anyT <$> omap (λ '(n, ly), const (ly_size ly: Z) <$> n) s.(sl_members))).
  Proof.
    rewrite ltr_anyT_pts_ly /layout_of structT_eq /structT_def {1}/ty_own/offset_of_idx/=.
    iSplit.
    - iDestruct 1 as (v Hv Hl) "Hl". iSplit.
      { iPureIntro. rewrite fmap_length. by apply omap_length_eq => i [[?|]?]. }
      have {}Hl := check_fields_aligned_alt_correct _ _ Hl.
      rewrite /has_layout_val{1}/ly_size in Hv.
      iSplit. { iApply loc_in_bounds_shorten; last by iApply heap_mapsto_loc_in_bounds. lia. }
      iInduction (sl_members s) as [|[n ly] ms] "IH" forall (v l Hl Hv) => //; csimpl in *.
      rewrite shift_loc_0. setoid_rewrite <-shift_loc_assoc_nat. move: Hl => [??].
      have Hlen: (length (take (ly_size ly) v) = ly_size ly) by rewrite take_length_le ?Hv//; cbn; lia.
      rewrite -(take_drop ly.(ly_size) v).
      iDestruct (heap_mapsto_app with "Hl") as "[Hl Hr]". rewrite Hlen.
      iSplitL "Hl"; destruct n; simpl.
      + iApply ltr_anyT_pts_ly. by [iExists _; iFrame].
      + iApply ltr_anyT_pts_ly. by [iExists _; iFrame].
      + iApply "IH" => //. rewrite drop_length; try iPureIntro; lia.
      + iApply "IH" => //. rewrite drop_length; try iPureIntro; lia.
    - iIntros "Hl". iDestruct "Hl" as (_) "[#Hb Hl]".
      rewrite /has_layout_val{2}/ly_size.
      iInduction (sl_members s) as [|[n ly] ms] "IH" forall (l) => //; csimpl in *.
      { iApply heap_mapsto_layout_iff. iExists []. repeat iSplit => //; last by rewrite heap_mapsto_nil. }
      rewrite shift_loc_0. setoid_rewrite <-shift_loc_assoc_nat.
      iDestruct "Hl" as "[Hl Hs]".
      iDestruct (loc_in_bounds_split with "Hb") as "[Hb1 Hb2]".
      destruct n; csimpl.
      all: rewrite ltr_anyT_pts_ly.
      all: rewrite /ty_own/=; iDestruct "Hl" as (v1 Hv1 Hl) "Hl".
      all: iDestruct ("IH" with "Hb2 Hs") as (v2 Hv2 Hly) "Hv".
      all: iApply heap_mapsto_layout_iff.
      all: iExists (v1 ++ v2).
      all: rewrite heap_mapsto_app /has_layout_val app_length Hv1 Hv2; iFrame.
      all: by iPureIntro.
  Qed.


  Lemma anyT_structT_equiv_member_layouts l (sl: struct_layout):
    l ◁ₗ anyT (ly_size sl) ⊣⊢ l ◁ₗ structT sl (anyT <$> member_sizes sl).
  Proof.
    rewrite anyT_structT_equiv.
    rewrite /member_sizes /member_layouts //.
    rewrite -(list_fmap_compose _ Z.of_nat).
    rewrite !list_fmap_omap /=. f_equiv. f_equiv.
    induction (sl_members sl) as [|[[n|] ly]]; simpl.
    - by eapply Forall2_nil.
    - eapply Forall2_cons. split; done.
    - done.
  Qed.

  Lemma struct_focus_ty_own_all l sl As:
    l ◁ₗ structT sl As ⊢ ty_own_all As (member_locs l sl) ∗ ∀ Bs, ty_own_all Bs (member_locs l sl) -∗ l ◁ₗ structT sl Bs.
  Proof.
    rewrite struct_focus. iIntros "[Hl Hcont]". iSplitL "Hl".
    - rewrite /ty_own_all /member_locs. rewrite big_sepL2_fmap_l //.
    - iIntros (Bs) "Hown". iApply "Hcont".
      rewrite /ty_own_all /member_locs. rewrite big_sepL2_fmap_l //.
  Qed.


  Definition member_vals (v: val) (sl: struct_layout) : list val :=
    (λ n, v at{sl}ᵥ n) <$> field_names (sl_members sl).

  Lemma reshape_borrow v sl tys:
    length tys = length (field_names (sl_members sl)) →
    ([∗ list] w;ty ∈ reshape (ly_size <$> (sl_members sl).*2) v; pad_struct (sl_members sl) tys (λ ly, (anyT (ly_size ly))), w ◁ᵥ ty) ⊢
    ([∗ list] n;ty ∈ field_names (sl_members sl);tys, v at{sl}ᵥ n ◁ᵥ ty) ∗
    (∀ tys', ([∗ list] n;ty ∈ field_names (sl_members sl);tys', v at{sl}ᵥ n ◁ᵥ ty) -∗ ([∗ list] w;ty ∈ reshape (ly_size <$> (sl_members sl).*2) v; pad_struct (sl_members sl) tys' (λ ly, (anyT (ly_size ly))), w ◁ᵥ ty)).
  Proof.
    rewrite /GetMemberVal. intros Hlen.
    pose proof (sl_nodup sl) as Hnd%bool_decide_unpack.
    induction (sl_members sl) as [|[[n|] ly] fs IH] in v, tys, Hlen, Hnd |-*; csimpl.
    - simpl; destruct tys; simpl; last naive_solver.
      iIntros "$". iIntros (tys') "H". done.
    - simpl in Hlen. destruct tys; first naive_solver. simpl in Hlen. simplify_eq.
      eapply NoDup_cons in Hnd as [Hni Hnd].
      simpl head. simpl default. simpl tail. iIntros "[Ht Hd]".
      simpl. destruct decide; last naive_solver. rewrite -bi.sep_assoc. iSplitL "Ht".
      { rewrite offset_of_cons; last auto. destruct decide; last naive_solver.
        rewrite drop_0 //. }
      rewrite IH //. iDestruct "Hd" as "[Hd Hcont]".
      iSplitL "Hd".
      + iApply (big_sepL2_mono with "Hd").
        intros k m B Hlook Htys. destruct decide.
        * simplify_eq. exfalso. eapply Hni.
          eapply elem_of_list_lookup_2 with (i:=k). done.
        * destruct list_find as [[? [? ?]]|]; simpl; last done.
          rewrite offset_of_cons; last first.
          { right. eapply elem_of_list_lookup_2 with (i:=k). done. }
          destruct decide; first naive_solver.
          rewrite drop_drop //.
      + iIntros (tys') "Hd". destruct tys' as [|B tys']; first by naive_solver.
        simpl. destruct decide; last naive_solver. iDestruct "Hd" as "[Ht Hd]".
        iSplitL "Ht".
        { rewrite offset_of_cons; last auto. destruct decide; last naive_solver.
          rewrite drop_0 //. }
        iApply "Hcont". iApply (big_sepL2_mono with "Hd").
        intros k m C Hlook Htys. destruct decide.
        * simplify_eq. exfalso. eapply Hni.
          eapply elem_of_list_lookup_2 with (i:=k). done.
        * destruct list_find as [[? [? ?]]|]; simpl; last done.
          rewrite offset_of_cons; last first.
          { right. eapply elem_of_list_lookup_2 with (i:=k). done. }
          destruct decide; first naive_solver.
          rewrite drop_drop //.
    - iIntros "[Ht Hd]". simpl in Hlen. simpl in Hnd.
      rewrite IH //.  iDestruct "Hd" as "[Hd Hcont]".
      iSplitL "Hd".
      + iApply (big_sepL2_mono with "Hd").
        intros k m B Hlook Htys. destruct list_find as [[? [? ?]]|]; simpl; last done.
        rewrite offset_of_cons; last first.
        { right. eapply elem_of_list_lookup_2 with (i:=k). done. }
        destruct decide; first naive_solver.
        rewrite drop_drop //.
      + iIntros (tys') "Hd". iFrame. iApply "Hcont".
        iApply (big_sepL2_mono with "Hd").
        intros k m C Hlook Htys. destruct list_find as [[? [? ?]]|]; simpl; last done.
        rewrite offset_of_cons; last first.
        { right. eapply elem_of_list_lookup_2 with (i:=k). done. }
        destruct decide; first naive_solver.
        rewrite drop_drop //.
  Qed.


  Lemma struct_focus_vtr v sl tys:
    v ◁ᵥ structT sl tys ⊢
      ([∗ list] n;ty∈field_names sl.(sl_members);tys, v at{sl}ᵥ n ◁ᵥ ty) ∗
      (∀ tys', ([∗ list] n;ty∈field_names sl.(sl_members);tys', v at{sl}ᵥ n ◁ᵥ ty) -∗ v ◁ᵥ structT sl tys').
  Proof.
    rewrite structT_eq /structT_def /=. iIntros "(%Hly & %Hlen & Hall)".
    rewrite reshape_borrow //. iDestruct "Hall" as "[$ Hcont]".
    iIntros (tys') "Hall". iSplit; first done.
    iDestruct (big_sepL2_length with "Hall") as "#$".
    iApply "Hcont". iFrame.
  Qed.

  Lemma struct_focus_ty_own_all_val v sl As:
    v ◁ᵥ structT sl As ⊢ ty_own_val_all As (member_vals v sl) ∗ ∀ Bs, ty_own_val_all Bs (member_vals v sl) -∗ v ◁ᵥ structT sl Bs.
  Proof.
    rewrite struct_focus_vtr. iIntros "[Hl Hcont]". iSplitL "Hl".
    - rewrite /ty_own_val_all /member_locs. rewrite big_sepL2_fmap_l //.
    - iIntros (Bs) "Hown". iApply "Hcont".
      rewrite /ty_own_val_all /member_locs. rewrite big_sepL2_fmap_l //.
  Qed.

  Lemma struct_mono lv sl tys tys' :
    lv ◁ₓ structT sl tys -∗
    ([∗ list] ty;ty'∈tys;tys', ∀ lv', ⌜lov_type lv = lov_type lv'⌝ -∗ lv' ◁ₓ ty -∗ lv' ◁ₓ ty') -∗
    lv ◁ₓ structT sl tys'.
  Proof.
    iIntros "Hs Hwand".
    destruct lv => /=.
    - iDestruct (struct_focus with "Hs") as "[Htys Hs]". iApply "Hs".
      iInduction (field_names (sl_members sl)) as [] "IH" forall (tys tys').
      { iDestruct (big_sepL2_nil_inv_l with "Htys") as %->.
        by iDestruct (big_sepL2_nil_inv_l with "Hwand") as %->. }
      iDestruct (big_sepL2_cons_inv_l with "Htys") as (?? ->) "[Hw ?]".
      iDestruct (big_sepL2_cons_inv_l with "Hwand") as (?? ->) "[Hty ?]". simpl.
      iDestruct ("Hty" $! (LOC _) with "[//] Hw") as "$". iApply ("IH" with "[$] [$]").
    - iDestruct (struct_focus_vtr with "Hs") as "[Htys Hs]". iApply "Hs".
      iInduction (field_names (sl_members sl)) as [] "IH" forall (tys tys').
      { iDestruct (big_sepL2_nil_inv_l with "Htys") as %->.
        by iDestruct (big_sepL2_nil_inv_l with "Hwand") as %->. }
      iDestruct (big_sepL2_cons_inv_l with "Htys") as (?? ->) "[Hw ?]".
      iDestruct (big_sepL2_cons_inv_l with "Hwand") as (?? ->) "[Hty ?]". simpl.
      iDestruct ("Hty" $! (VAL _) with "[//] Hw") as "$". iApply ("IH" with "[$] [$]").
  Qed.

  Lemma is_struct_size_values sl vs:
    length vs = length (member_sizes sl) → is_struct_size sl ((λ '(v, n), value[n] v) <$> zip vs (member_sizes sl)) (ly_size sl).
  Proof.
    rewrite /member_sizes /member_layouts.
    intros Hlen. split; first done.
    induction (sl_members sl) as [| [[n|] mnm] sl' IH] in vs, Hlen |-*; simpl.
    - destruct vs; last naive_solver. by eapply Forall2_nil.
    - destruct vs; first naive_solver. simpl.
      eapply Forall2_cons; split; first by apply valueT_has_size.
      eapply IH; eauto.
    - eapply IH. simpl in Hlen. done.
  Qed.

  Global Instance type_params_struct sl As:
    TypeParams (structT sl As) (struct_layout * list (type Σ)) (sl, As) (λ p, structT p.1 p.2) (prod_relation eq (flip is_ty_all)) | 1.
  Proof.
    rewrite /TypeParams. intros [sl' As'] [Heq Htys].
    simpl in *. split. eapply structT_le; first done.
    by eapply is_ty_all_le.
  Qed.



End struct.
Global Typeclasses Opaque structT.

Global Notation "'struct[' sl ']' T" := (structT sl T) (at level 60, format "struct[ sl ]  T") : ref_type_scope.