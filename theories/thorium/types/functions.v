From quiver.thorium.types Require Export types base_types combinators.

Section function_type.
  Context {Σ} `{!refinedcG Σ}.

  (* function types *)
  Record fn_ret := FR {
    (* return type (rc::returns) *)
    fr_rty : type Σ;
    (* postcondition (rc::ensures) *)
    fr_R : iProp Σ;
  }.

  Record fn_params := FP {
    (* types of arguments (rc::args) *)
    fp_atys : list (type Σ);
    (* precondition (rc::requires) *)
    fp_Pa : iProp Σ;
    (* type of the existential quantifier (rc::exists) *)
    fp_rtype : Type;
    (* return type and postcondition (rc::returns and rc::ensures) *)
    fp_fr: fp_rtype → fn_ret;
  }.

  Record fn_type := FT {
    f_all: Type;
    f_params:> f_all → fn_params
  }.


  (* a function type [ft] is well-formed for a function [fn], if
     the op types of all arguments have the layout dictated by the function
  *)
  Definition ft_wf (fn: function) (ft: fn_type) : Prop :=
    ∀ x: f_all ft, Forall2 (λ A ly, A `has_size` ly_size ly)%type (fp_atys (ft x)) (f_args fn).*2.


  Lemma ft_wf_layout ft fn x vs:
    ft_wf fn ft →
    ([∗ list] v;A ∈ vs;fp_atys (ft x), v ◁ᵥ A) ⊢ ⌜Forall2 has_layout_val vs (f_args fn).*2⌝.
  Proof.
    intros Hwf. induction (Hwf x) as [|A ly args As]in vs |-*; simpl.
    - destruct vs; simpl; eauto.
    - destruct vs; simpl; first by eauto.
      iIntros "[Hv Hrest]".
      iPoseProof (IHf with "Hrest") as "%IH".
      iPoseProof (ty_size_eq with "Hv") as "%Hsz"; first done.
      iPureIntro. econstructor; eauto. by eapply has_size_val_has_layout.
  Qed.

  Definition fn_ret_prop {B} (fr : B → fn_ret) (fn: function) (lsa lsv: list loc) : val → iProp Σ :=
    (λ v, ∃ x, v ◁ᵥ (fr x).(fr_rty) ∗
              (fr x).(fr_R) ∗
              ([∗ list] l; p ∈ lsa; fn.(f_args).*2, l ◁ₗ anyT (ly_size p)) ∗
              ([∗ list] l; p ∈ lsv; fn.(f_local_vars).*2, l ◁ₗ anyT (ly_size p)))%I.

  Definition typed_function (fn : function) (ft : fn_type) : iProp Σ :=
    (□ ∀ x: ft.(f_all), ∀ (lsa : list loc) (lsv : list loc),
          let Qinit := ([∗list] l;t∈lsa;(ft x).(fp_atys), l ◁ₗ t) ∗
                        ([∗list] l;p∈lsv;fn.(f_local_vars).*2, l ◁ₗ anyT (ly_size p)) ∗
                        (ft x).(fp_Pa) in
          let Q := (subst_stmt (zip (fn.(f_args).*1 ++ fn.(f_local_vars).*1)
                  (val_of_loc <$> (lsa ++ lsv)))) <$> fn.(f_code) in
          Qinit -∗
          WPs (Goto fn.(f_init)) {{ Q, fn_ret_prop (ft x).(fp_fr) fn lsa lsv}}%I
    )%I.


  Global Program Definition functionT_def (ft: fn_type) : type Σ :=
    {|
      ty_own l := (∃ (f: loc) (fn: function), l ↦ f ∗ ⌜ft_wf fn ft⌝ ∗ fntbl_entry f fn ∗ ▷ typed_function fn ft)%I%I;
      ty_own_val v := (∃ (f: loc) (fn: function), ⌜v = f⌝ ∗ ⌜ft_wf fn ft⌝ ∗ fntbl_entry f fn ∗ ▷ typed_function fn ft)%I;
      ty_has_size n := n = ptr_size
    |}.
  Next Obligation.
    iIntros (ft ot v Hop) "(%f & %fn & -> & % & Hentr & Hty)".
    iPureIntro. rewrite /has_layout_val Hop //.
  Qed.
  Next Obligation.
    iIntros (ft ot l Hop) "(%f & %fn & Hl & % & Hentr & Hty)".
    iExists f. iFrame. iExists f, fn. iFrame. done.
  Qed.
  Next Obligation.
    iIntros (fp l v) "Hl (%f & %fn & -> & % & Hentr & Hty)".
    iExists f, fn. iFrame. done.
  Qed.

  Definition functionT_aux : functionT_def.(seal).
  Proof. by eexists. Qed.
  Definition functionT := functionT_aux.(unseal).
  Definition functionT_eq : functionT = functionT_def :=
    seal_eq functionT_aux.

  Global Instance functionT_ptr_type ft : PtrType (functionT ft).
  Proof.
    rewrite functionT_eq /functionT_def; split; simpl.
    - naive_solver.
    - iIntros (v)  "(%f & %fn & -> & % & Hentr & Hty)". iExists _.
      iPureIntro. by eapply val_to_of_loc.
  Qed.

  Lemma functionT_has_size fp n:
    (functionT fp) `has_size` n ↔ n = ptr_size.
  Proof. rewrite functionT_eq //. Qed.

  Global Instance functionT_copy fp: Copy (functionT fp).
  Proof.
    split. rewrite functionT_eq /functionT_def //=. apply _.
  Qed.

End function_type.


(* pretty printing of function types *)
Declare Scope fun_type_scope.
Delimit Scope fun_type_scope with FT.
Bind Scope fun_type_scope with fn_type.

Notation "'fn(' P )" := (FT unit (λ _, P)) (format "'fn(' P )", only printing) : fun_type_scope.
Notation "'fn(∀' x : X , P )" := (FT X (λ x, P)) (x binder, only printing) : fun_type_scope.
Notation "A → R" := (FP A True unit (λ _, R)) (only printing).
Notation "A ; P → R" := (FP A P unit (λ _, R)) (only printing, at level 99).
Notation "A → ∃ y : Y , R" := (FP A True Y (λ y, R)) (only printing, at level 99, y binder).
Notation "A ; P → ∃ y : Y , R" := (FP A P Y (λ y, R)) (only printing, at level 99, y binder).
Notation "A" := (FR A True) (only printing, at level 99) : fun_type_scope.
Notation "A ; P" := (FR A P) (only printing, at level 99) : fun_type_scope.






Section quiver_functions.

  Context {Σ} `{!refinedcG Σ}.

  Inductive qfn_ret : Type :=
    | QFR (A: type Σ) (Q: iProp Σ)
    | QFRE {X: Type} (A: X → qfn_ret).

  Inductive qfn_type : Type :=
    | QFT (As: list (type Σ)) (P: iProp Σ) (R: qfn_ret)
    | QFTA {X: Type} (A: X → qfn_type).

  Fixpoint qfn_ret_exists (R: qfn_ret) : Type :=
    match R with
    | QFR A Q => unit
    | @QFRE X A => sigT (λ x, qfn_ret_exists (A x))
    end.

  Fixpoint qfn_ret_ret_type (R: qfn_ret) : qfn_ret_exists R → fn_ret :=
    match R with
    | QFR A Q => λ _, FR A Q
    | @QFRE X A => λ y, qfn_ret_ret_type (A (projT1 y)) (projT2 y)
    end.

  Fixpoint qfn_type_all (R: qfn_type) : Type :=
    match R with
    | QFT As P R => unit
    | @QFTA X A => sigT (λ x, qfn_type_all (A x))
    end.

  Fixpoint qfn_type_fn_params (R: qfn_type) : qfn_type_all R → fn_params :=
    match R with
    | QFT As P R => λ _, FP As P (qfn_ret_exists R) (qfn_ret_ret_type R)
    | @QFTA X A => λ y, qfn_type_fn_params (A (projT1 y)) (projT2 y)
    end.

  Definition qfn_type_fn_type (R: qfn_type) : fn_type :=
    FT (qfn_type_all R) (qfn_type_fn_params R).

  Definition fn_type_qfn_type (ft: fn_type) : qfn_type :=
    @QFTA (ft.(f_all)) (λ x,
      let fp := ft.(f_params) x in
      QFT fp.(fp_atys) fp.(fp_Pa)
        (@QFRE (fp.(fp_rtype)) (λ y,
          let fr := fp.(fp_fr) y in
          QFR (fr.(fr_rty)) (fr.(fr_R))))).

  Definition qfnT (qft: qfn_type) : type Σ :=
    functionT (qfn_type_fn_type qft).

  Lemma qfn_to_function (qft: qfn_type)  :
    qfnT qft ≡ functionT (qfn_type_fn_type qft).
  Proof. done. Qed.

  Lemma ft_wf_qft fn ft:
    ft_wf fn ft ↔ ft_wf fn (qfn_type_fn_type (fn_type_qfn_type ft)).
  Proof.
    rewrite /ft_wf /=.
    split; intros Hent.
    - intros [x ?]; specialize (Hent x). done.
    - intros x; specialize (Hent (existT x tt)). done.
  Qed.

  Lemma typed_function_qft fn ft:
    typed_function fn ft ⊣⊢ typed_function fn (qfn_type_fn_type (fn_type_qfn_type ft)).
  Proof.
    rewrite /typed_function /=.
    f_equiv. iSplit; iIntros "Hcont".
    - iIntros ([x ?]); iSpecialize ("Hcont" $! x). simpl.
      iIntros (lsa lsv) "Hpre". iSpecialize ("Hcont" $! lsa lsv with "Hpre").
      iApply (wps_wand with "Hcont"). iIntros (v) "Hret". rewrite /fn_ret_prop.
      iDestruct "Hret" as "[%y Hret]". iExists (existT y tt). iFrame.
    - iIntros (x). iSpecialize ("Hcont" $! (existT x tt)).
      iIntros (lsa lsv) "Hpre". iSpecialize ("Hcont" $! lsa lsv with "Hpre").
      iApply (wps_wand with "Hcont"). iIntros (v) "Hret". rewrite /fn_ret_prop.
      iDestruct "Hret" as "[%y Hret]". iExists (projT1 y). iFrame.
  Qed.


  Lemma function_to_qft ft :
    functionT ft ≡ qfnT (fn_type_qfn_type ft).
  Proof.
    rewrite /qfnT.
    rewrite functionT_eq /functionT_def /=. split; simpl.
    - intros l. f_equiv=>r. f_equiv=>fn. f_equiv.
      f_equiv; f_equiv; first apply ft_wf_qft.
      f_equiv. iApply typed_function_qft.
    - intros v. f_equiv=>r. f_equiv=>fn. f_equiv. f_equiv.
      1: f_equiv; first apply ft_wf_qft.
      f_equiv. f_equiv. iApply typed_function_qft.
  Qed.

End quiver_functions.


Notation "∀ x .. y , P" := (QFTA (λ x , .. (QFTA (λ y , P)) ..)) (at level 200, x binder, y binder, right associativity, format "'[ ' '[ ' ∀ x .. y ']' , '/' P ']'", only printing) : fun_type_scope.
Notation "A → R" := (QFT A%RT True R%FT) (only printing, format "'[' '[' A ']'  →  '/' R ']'").
Notation "A ; 'Pre(' P ')' → R" := (QFT A%RT P%I R%FT) (only printing, at level 99, format "'[' '[' A ']' ;  '/' '[' 'Pre(' P ')' ']'  →  '/' R ']'") : fun_type_scope.
Notation "∃ x .. y , P" := (QFRE (λ x , .. (QFRE (λ y , P)) ..)) (at level 200, x binder, y binder, right associativity, format "'[ ' '[ ' ∃ x .. y ']' , '/' P ']'", only printing) : fun_type_scope.
Notation "A" := (QFR A%RT True) (only printing, at level 99, format "'[' A ']'") : fun_type_scope.
Notation "A ; 'Post(' P ')'" := (QFR A%RT P%I) (only printing, at level 99, format "'[' A ']' ;  '/' '[' 'Post(' P ')' ']'") : fun_type_scope.




Section predicate_transformers.
  Context {Σ: gFunctors} `{!refinedcG Σ}.

  Definition pt : Type := list val → (type Σ → iProp Σ) → iProp Σ.

  Definition fn_cfg (fn: function) (lsa lsv: list loc) :=
    let Q := (subst_stmt (zip ((fn.(f_args) ++ fn.(f_local_vars)).*1)
    (val_of_loc <$> (lsa ++ lsv)))) <$> fn.(f_code) in Q.

  Definition fn_pre (T: pt) (lsa lsv: list loc) (fn: function) (Φ: val → iProp Σ) : iProp Σ :=
    ∃ vs Ψ, ⌜length vs = length (fn.(f_args).*2)⌝ ∗
          ([∗ list] l; p ∈ lsa; zip (fn.(f_args).*2) vs, l ◁ₗ valueT p.2 (ly_size p.1)) ∗
          ([∗ list] l; ly ∈ lsv; (fn.(f_local_vars).*2), l ◁ₗ anyT (ly_size ly)) ∗
          T vs Ψ ∗
          (∀ v A,
            ([∗ list] l; ly ∈ lsa; fn.(f_args).*2, l ◁ₗ anyT (ly_size ly)) -∗
            ([∗ list] l; ly ∈ lsv; (fn.(f_local_vars).*2), l ◁ₗ anyT (ly_size ly)) -∗
            v ◁ᵥ A -∗ Ψ A -∗ Φ v).

  Definition fn_implements_pt (fn: function) (T: pt) : iProp Σ :=
    □ (∀ Φ lsa lsv, fn_pre T lsa lsv fn Φ -∗ WPs (Goto fn.(f_init)) {{ fn_cfg fn lsa lsv, Φ }}).


  (* the predicate transformer type *)
  Definition fnT (args: list layout) (fp: pt) : type Σ :=
    (ex (f: loc) (fn: function), value[ptr_size] f ∗∗ (⌜args = fn.(f_args).*2⌝ ∗ fntbl_entry f fn ∗ ▷ fn_implements_pt fn fp))%RT.

  Lemma fnT_intro f fn T :
    fntbl_entry f fn ∗ ▷ fn_implements_pt fn T ⊢ f ◁ᵥ fnT (fn.(f_args).*2) T.
  Proof.
    iIntros "(#Hf & #Hfp)". rewrite /fnT.
    rewrite vtr_existsT_iff. iExists _.
    rewrite vtr_existsT_iff. iExists _.
    rewrite vtr_sepT_iff. iFrame "Hf Hfp".
    iSplit; last done. iApply vtr_valueT_intro. iSplit; first done.
    iPureIntro. eauto with quiver_logic.
  Qed.

  Lemma fnT_elim v lys T :
    v ◁ᵥ fnT lys T ⊢ ∃ (f: loc) (fn: function), ⌜v = f⌝ ∗ fntbl_entry f fn ∗ ▷ fn_implements_pt fn T.
  Proof.
    rewrite /fnT. rewrite vtr_existsT_iff. iDestruct 1 as (f) "Hf".
    rewrite vtr_existsT_iff. iDestruct "Hf" as (fn) "Hf".
    rewrite vtr_sepT_iff. iDestruct "Hf" as "(Hf & -> & He & Hfp)".
    iExists _, _. iFrame. by iDestruct (vtr_valueT_elim with "Hf") as "[-> _]".
  Qed.


  Lemma fnT_elim_strong v lys T :
    v ◁ᵥ fnT lys T ⊢ ∃ (f: loc) (fn: function), ⌜v = f⌝ ∗ ⌜lys = fn.(f_args).*2⌝ ∗ fntbl_entry f fn ∗ ▷ fn_implements_pt fn T.
  Proof.
    rewrite /fnT. rewrite vtr_existsT_iff. iDestruct 1 as (f) "Hf".
    rewrite vtr_existsT_iff. iDestruct "Hf" as (fn) "Hf".
    rewrite vtr_sepT_iff. iDestruct "Hf" as "(Hf & -> & He & Hfp)".
    iExists f, _. iFrame. by iDestruct (vtr_valueT_elim with "Hf") as "[-> _]".
  Qed.

End predicate_transformers.

Global Arguments pt _ {_}.

Notation "fn `implements` pt" := (fn_implements_pt fn pt) (at level 20).
