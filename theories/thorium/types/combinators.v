From quiver.thorium.types Require Export types.
From quiver.base Require Import classes.


Section combinators.
  Context {Σ} `{!refinedcG Σ}.

  Global Program Definition sepT_def (A: type Σ) (P: iProp Σ): type Σ :=
    {| ty_own l := (l ◁ₗ A ∗ P)%I;
       ty_own_val v := (v ◁ᵥ A ∗ P)%I;
       ty_has_size := A.(ty_has_size) |}.
  Next Obligation.
    intros A P ot v Hop. iIntros "[Hv _]". by iApply ty_size_eq.
  Qed.
  Next Obligation.
    intros A P ot l Hop. iIntros "[Hl P]".
    iPoseProof (ty_move with "Hl") as "[%v [Hl Hv]]"; first done.
    iExists _. by iFrame.
  Qed.
  Next Obligation.
    intros A P l v.
    iIntros "Hl [Hv P]". iPoseProof (ty_move_back with "Hl Hv") as "Hl"; eauto with iFrame.
  Qed.

  Definition sepT_aux : sepT_def.(seal).
  Proof. by eexists. Qed.
  Definition sepT := sepT_aux.(unseal).
  Definition sepT_eq : sepT = sepT_def :=
  seal_eq sepT_aux.

  Lemma sepT_has_size A P n:
    sepT A P `has_size` n ↔ A `has_size` n.
  Proof.
    rewrite sepT_eq /sepT_def //=.
  Qed.

  Lemma sepT_has_bounds A P n:
    A `has_bounds` n → (sepT A P) `has_bounds` n.
  Proof.
    rewrite sepT_eq /sepT_def /ty_has_bounds /=.
    iIntros (Hb l) "[Hl P]". by iApply Hb.
  Qed.


  Global Instance sepT_copy (A: type Σ) `{!Copy A} P `{!Persistent P}: Copy (sepT A P).
  Proof. econstructor. rewrite sepT_eq //=. apply _. Qed.

  Global Instance sepT_persistent l A P:
    Persistent (l ◁ₗ A) →
    Persistent P →
    Persistent (l ◁ₗ sepT A P).
  Proof. rewrite sepT_eq /sepT_def /=. apply _. Qed.

  (* the constraint type *)
  Lemma vtr_sepT_iff v (A: type Σ) P : v ◁ᵥ sepT A P ⊣⊢ (v ◁ᵥ A ∗ P).
  Proof.
    rewrite sepT_eq /sepT_def //=.
  Qed.

  Lemma ltr_sepT_iff l (A: type Σ) P : l ◁ₗ sepT A P ⊣⊢ (l ◁ₗ A ∗ P).
  Proof.
    rewrite sepT_eq /sepT_def //=.
  Qed.

  Lemma xtr_sepT_iff x (A: type Σ) P : x ◁ₓ sepT A P ⊣⊢ (x ◁ₓ A ∗ P).
  Proof.
    destruct x; rewrite sepT_eq /sepT_def //=.
  Qed.

  Global Instance sepT_ptr_type A P:
    PtrType A →
    PtrType (sepT A P).
  Proof.
    intros Hpt; split; rewrite sepT_eq /sepT_def /=.
    - apply ptr_type_size.
    - iIntros (v) "[Hv _]". by iApply ptr_type_val_loc.
  Qed.

  Global Instance non_null_sep P (A: type Σ):
    NonNull A →
    NonNull (sepT A P).
  Proof.
    rewrite /NonNull. iIntros (Hent l) "Hl".
    rewrite sepT_eq /sepT_def /=.
    iDestruct "Hl" as "[Hl HP]".
    by iApply Hent.
  Qed.

  Global Instance non_null_val_sep P (A: type Σ):
    NonNullVal A →
    NonNullVal (sepT A P).
  Proof.
    rewrite /NonNullVal. iIntros (Hent l) "Hl".
    rewrite sepT_eq /sepT_def /=.
    iDestruct "Hl" as "[Hl HP]".
    by iApply Hent.
  Qed.

  Global Instance sepT_le :
    Proper ((⊑) ==> (⊢) ==> (⊑)) sepT.
  Proof. rewrite sepT_eq /sepT_def. solve_type_proper. Qed.

  Global Instance sepT_proper :
    Proper ((≡) ==> (≡) ==> (≡)) sepT.
  Proof. rewrite sepT_eq /sepT_def. solve_type_proper. Qed.


  (* The constraint type *)
  Definition constraintT_def (A: type Σ) (φ: Prop): type Σ := sepT A ⌜φ⌝.
  Definition constraintT_aux : constraintT_def.(seal).
  Proof. by eexists. Qed.
  Definition constraintT := constraintT_aux.(unseal).
  Definition constraintT_eq : constraintT = constraintT_def :=
  seal_eq constraintT_aux.

  Lemma constraintT_has_size A φ n:
    (constraintT A φ) `has_size` n ↔ A `has_size` n.
  Proof.
    rewrite constraintT_eq /constraintT_def. apply sepT_has_size.
  Qed.

  Lemma constraintT_has_bounds A φ n:
    A `has_bounds` n → constraintT A φ `has_bounds` n.
  Proof.
    rewrite constraintT_eq /constraintT_def. apply sepT_has_bounds.
  Qed.


  Global Instance constraintT_copy (A: type Σ) `{!Copy A} φ: Copy (constraintT A φ).
  Proof.
    rewrite constraintT_eq /constraintT_def /=.
    apply _.
  Qed.

  Global Instance constrainT_persistent l A φ:
    Persistent (l ◁ₗ A) →
    Persistent (l ◁ₗ constraintT A φ).
  Proof. rewrite constraintT_eq /constraintT_def /=. apply _. Qed.

  (* the constraint type *)
  Lemma vtr_constraintT_iff v (A: type Σ) φ : v ◁ᵥ constraintT A φ ⊣⊢ (v ◁ᵥ A ∗ ⌜φ⌝).
  Proof.
    rewrite constraintT_eq /constraintT_def /=.
    rewrite vtr_sepT_iff //.
  Qed.

  Lemma ltr_constraintT_iff l (A: type Σ) φ : l ◁ₗ constraintT A φ ⊣⊢ (l ◁ₗ A ∗ ⌜φ⌝).
  Proof.
    rewrite constraintT_eq /constraintT_def //=.
    rewrite ltr_sepT_iff //.
  Qed.

  Lemma xtr_constraintT_iff x (A: type Σ) φ : x ◁ₓ constraintT A φ ⊣⊢ (x ◁ₓ A ∗ ⌜φ⌝).
  Proof.
    rewrite constraintT_eq /constraintT_def //=.
    rewrite xtr_sepT_iff //.
  Qed.

  Global Instance constraintT_ptr_type A φ:
    PtrType A →
    PtrType (constraintT A φ).
  Proof.
    rewrite constraintT_eq /constraintT_def /=.
    apply _.
  Qed.

  Global Instance non_null_constraint φ (A: type Σ):
    NonNull A →
    NonNull (constraintT A φ).
  Proof.
    rewrite constraintT_eq /constraintT_def /=.
    apply _.
  Qed.

  Global Instance non_null_val_constraint φ (A: type Σ):
    NonNullVal A →
    NonNullVal (constraintT A φ).
  Proof.
    rewrite constraintT_eq /constraintT_def /=.
    apply _.
  Qed.

  Global Instance constraintT_le :
    Proper ((⊑) ==> impl ==> (⊑)) constraintT.
  Proof. rewrite constraintT_eq /constraintT_def. solve_type_proper. Qed.

  Global Instance constraintT_proper :
    Proper ((≡) ==> iff ==> (≡)) constraintT.
  Proof. rewrite constraintT_eq /constraintT_def. solve_type_proper. Qed.


  (* The wand type *)
  Global Program Definition wandT_def {X} (P: X → iProp Σ) (T: X → type Σ) : type Σ :=
    {| ty_own l := (∀ x, P x -∗ l ◁ₗ T x)%I;
       ty_own_val v := False%I;
       ty_has_size _ := False |}.
  Next Obligation. done. Qed.
  Next Obligation. done. Qed.
  Next Obligation. move => *. iIntros "? %". done. Qed.

  Definition wandT_aux : @wandT_def.(seal).
  Proof. by eexists. Qed.
  Definition wandT {A} := wandT_aux.(unseal) A.
  Lemma wandT_eq : @wandT = @wandT_def.
  Proof. by rewrite /wandT (seal_eq wandT_aux). Qed.
  Global Typeclasses Opaque wandT.

  Lemma wandT_intro {A} P (T: A → type Σ) l:
    (∀ x, P x -∗ l ◁ₗ T x) -∗ l ◁ₗ wandT P T.
  Proof. iIntros "Hwand". by rewrite wandT_eq /wandT_def /ty_own. Qed.

  Lemma wandT_elim {A} P (T: A → type Σ) l x:
    l ◁ₗ wandT P T -∗ P x -∗ l ◁ₗ T x.
  Proof. iIntros "Hwand HP". rewrite wandT_eq /wandT_def /ty_own. by iApply "Hwand". Qed.

  (* The existential type *)
  Global Program Definition existsT_def {X: Type} (A: X → type Σ): type Σ :=
    {|
      ty_own l := (∃ x: X, l ◁ₗ (A x))%I;
      ty_own_val v := (∃ x: X, v ◁ᵥ (A x))%I;
      ty_has_size n := ∀ x, (A x) `has_size` n;
    |}.
  Next Obligation.
    simpl; iIntros (X A ot v Hop) "[%x Hv]". by iApply ty_size_eq.
  Qed.
  Next Obligation.
    simpl; iIntros (X A ot l Hop) "[%x Hl]".
    iPoseProof (ty_move with "Hl") as (v) "[Hv Hl]"; first done.
    iExists _. iFrame. by iExists _.
  Qed.
  Next Obligation.
    simpl; iIntros (X A l v) "Hl [%x Hv]".
    iExists x. by iApply (ty_move_back with "Hl Hv").
  Qed.

  Definition existsT_aux (X: Type) : (@existsT_def X).(seal).
  Proof. by eexists. Qed.
  Definition existsT {X: Type} := (existsT_aux X).(unseal).
  Definition existsT_eq X : @existsT X = @existsT_def X :=
    seal_eq (existsT_aux X).

  Lemma existsT_has_size {X} (A: X → type Σ) n:
    (existsT A) `has_size` n ↔ ∀ x, (A x) `has_size` n.
  Proof.
    rewrite existsT_eq /existsT_def //=.
  Qed.

  Lemma existsT_has_bounds {X} (A: X → type Σ) n:
    (∀ x, (A x) `has_bounds` n) → (existsT A) `has_bounds` n.
  Proof.
    rewrite existsT_eq /existsT_def /ty_has_bounds //=.
    iIntros (Hle l). iIntros "[%x Hl]". by iApply Hle.
  Qed.

  Global Instance existsT_copy {X: Type} (A: X → type Σ) `{!∀ x, Copy (A x)}: Copy (existsT A).
  Proof.
    rewrite existsT_eq /existsT_def //=.
    split; simpl. apply _.
  Qed.


  Global Instance existsT_persistent l {X: Type} (A: X → type Σ):
    (∀ x, Persistent (l ◁ₗ A x)) →
    Persistent (l ◁ₗ existsT A).
  Proof. rewrite existsT_eq /existsT_def /=. apply _. Qed.

  Lemma vtr_existsT_iff {A} v (P: A → type Σ):
    v ◁ᵥ (existsT P) ⊣⊢ ∃ a, v ◁ᵥ P a.
  Proof. rewrite existsT_eq /existsT_def /= //. Qed.

  Lemma ltr_existsT_iff {A} l (P: A → type Σ):
    l ◁ₗ (existsT P) ⊣⊢ ∃ a, l ◁ₗ P a.
  Proof. rewrite existsT_eq /existsT_def /= //. Qed.

  Lemma xtr_existsT_iff {A} x (P: A → type Σ):
    x ◁ₓ (existsT P) ⊣⊢ ∃ a, x ◁ₓ P a.
  Proof. destruct x; rewrite existsT_eq /existsT_def /= //. Qed.


  Global Instance existsT_ptr_type {X: Type} (A: X → type Σ):
    (∀ x, PtrType (A x)) →
    PtrType (existsT A).
  Proof.
    intros Hpt; split; rewrite existsT_eq /existsT_def /=.
    - intros ?; apply ptr_type_size.
    - iIntros (v) "[%x Hv]". by iApply ptr_type_val_loc.
  Qed.

  Global Instance non_null_exists {X: Type} (A: X → type Σ):
    (∀ x, NonNull (A x)) →
    NonNull (existsT A).
  Proof.
    rewrite /NonNull. iIntros (Hent l) "Hl".
    rewrite existsT_eq /existsT_def /=. iDestruct "Hl" as "[%x Hl]".
    by iApply Hent.
  Qed.

  Global Instance non_null_val_exists {X: Type} (A: X → type Σ):
    (∀ x, NonNullVal (A x)) →
    NonNullVal (existsT A).
  Proof.
    rewrite /NonNull. iIntros (Hent l) "Hl".
    rewrite existsT_eq /existsT_def /=. iDestruct "Hl" as "[%x Hl]".
    by iApply Hent.
  Qed.


  Global Instance existsT_le {X: Type} :
    Proper (pointwise_relation X (⊑) ==> (⊑)) existsT.
  Proof. rewrite existsT_eq /existsT_def; solve_type_proper. Qed.

  Global Instance existsT_equiv {X: Type} :
    Proper (pointwise_relation X (≡) ==> (≡)) existsT.
  Proof. rewrite existsT_eq /existsT_def; solve_type_proper. Qed.

  Lemma is_ty_exists {X: Type} (y: X) A B :
    A `is_ty` (B y) →
    A `is_ty` (existsT B).
  Proof.
    intros Histy. etrans; first apply Histy.
    split. eapply type_le_xtr_intro.
    iIntros (x) "Hx". rewrite xtr_existsT_iff.
    iExists y. iFrame.
  Qed.


  (* the fixpoint combinator *)
  (* fixpoints only exist as refinement types,
     not as ordinary types *)
  Definition type_fixpoint_def {A} : ((A -> type Σ) → (A -> type Σ)) → (A → type Σ) :=
    λ T x, existsT (λ ty: A → type Σ, constraintT (ty x) (∀ x, ty x ⊑ T ty x)).
  Definition type_fixpoint_aux : seal (@type_fixpoint_def). Proof. by eexists. Qed.
  Definition type_fixpoint := type_fixpoint_aux.(unseal).
  Global Arguments type_fixpoint {A} _ _.
  Lemma type_fixpoint_unseal {A} : type_fixpoint = @type_fixpoint_def A.
  Proof. rewrite -type_fixpoint_aux.(seal_eq) //. Qed.



  Section fixpoint.
    Context {A : Type}.
    Implicit Types (T : (A -> type Σ) → (A -> type Σ)).

    Local Lemma type_fixpoint_own_eq T x l :
      l ◁ₗ type_fixpoint T x ⊣⊢ ∃ ty, ⌜∀ x, ty x ⊑ T ty x⌝ ∗ l ◁ₗ ty x.
    Proof.
      rewrite type_fixpoint_unseal /type_fixpoint_def {1}/ty_own/=.
      rewrite existsT_eq /existsT_def constraintT_eq /constraintT_def sepT_eq /sepT_def /=.
      f_equiv => ?. iSplit; by iIntros "[$ ?]".
    Qed.

    Local Lemma type_fixpoint_own_val_eq T x v :
      v ◁ᵥ type_fixpoint T x ⊣⊢ ∃ ty, ⌜∀ x, ty x ⊑ T ty x⌝ ∗ v ◁ᵥ ty x.
    Proof.
      rewrite type_fixpoint_unseal /type_fixpoint_def {1}/ty_own_val/=.
      rewrite existsT_eq /existsT_def constraintT_eq /constraintT_def sepT_eq /sepT_def /=.
      f_equiv => ?. iSplit; by iIntros "[$ ?]".
    Qed.

    Lemma type_fixpoint_greatest T ty :
      (∀ x, ty x ⊑ T ty x) →
      ∀ x, ty x ⊑ type_fixpoint T x.
    Proof.
      move => Hle. constructor.
      - iIntros (l) "Hl". rewrite type_fixpoint_own_eq. iExists _. by iFrame.
      - iIntros (v) "Hv". rewrite type_fixpoint_own_val_eq. iExists _. by iFrame.
    Qed.

    Lemma type_fixpoint_unfold_1 T `{TM: !TypeMono T}:
      ∀ x, type_fixpoint T x ⊑ T (type_fixpoint T) x.
    Proof.
      intros x. constructor => *.
      - rewrite type_fixpoint_own_eq. iIntros "[%ty [%Hle HA]]".
        destruct (Hle x) as [-> ?].
        edestruct (TM ty (type_fixpoint T)) as [Hown2 ?]; [|by iApply Hown2].
        intros ?. by apply type_fixpoint_greatest.
      - rewrite type_fixpoint_own_val_eq. iIntros "[%ty [%Hle HA]]".
        destruct (Hle x) as [? ->].
        edestruct (TM ty (type_fixpoint T)) as [? Hown2]; [|by iApply Hown2].
        intros ?. by apply type_fixpoint_greatest.
    Qed.

    Lemma type_fixpoint_unfold_2 T `{TM: !TypeMono T} :
      ∀ x, T (type_fixpoint T) x ⊑ type_fixpoint T x.
    Proof.
      intros x. constructor => *.
      - rewrite type_fixpoint_own_eq. iIntros "?". iExists _. iSplit; [|done].
        iPureIntro. intros. apply TM. intros ?. by apply type_fixpoint_unfold_1.
      - rewrite type_fixpoint_own_val_eq. iIntros "?". iExists _. iSplit; [|done].
        iPureIntro. intros. apply TM. intros ?. by apply type_fixpoint_unfold_1.
    Qed.

    Lemma type_fixpoint_unfold T x `{!TypeMono T} :
      type_fixpoint T x ≡ T (type_fixpoint T) x.
    Proof. apply (anti_symm (⊑)); [by apply type_fixpoint_unfold_1 | by apply type_fixpoint_unfold_2]. Qed.

    Lemma type_fixpoint_unfold2 T x `{!TypeMono T}:
      T (type_fixpoint T) x ≡ T (T (type_fixpoint T)) x.
    Proof.
      apply (anti_symm (⊑)); apply TypeMono0;
        intros ?; [by apply type_fixpoint_unfold_1 | by apply type_fixpoint_unfold_2].
    Qed.

    Lemma type_fixpoint_proper x1 x2 (T1 T2 : (A → type Σ) → (A → type Σ)) :
      x1 = x2 →
      (∀ f x, T1 f x ≡ T2 f x) →
      type_fixpoint T1 x1 ≡ type_fixpoint T2 x2.
    Proof.
      move => -> HT.
      constructor => *.
      - rewrite !type_fixpoint_own_eq. by setoid_rewrite HT.
      - rewrite !type_fixpoint_own_val_eq. by setoid_rewrite HT.
    Qed.
  End fixpoint.

  Section fixp.

    (* fixT *)
    Definition fixT_def {A} : ((A -> type Σ) → (A -> type Σ)) → (A → type Σ) :=
      λ T, T (type_fixpoint T).
    Definition fixT_aux : seal (@fixT_def). Proof. by eexists. Qed.
    Definition fixT := fixT_aux.(unseal).
    Global Arguments fixT {A} _ _.
    Lemma fixT_unseal {A} : fixT = @fixT_def A.
    Proof. rewrite -fixT_aux.(seal_eq) //. Qed.


    Lemma fixT_equiv {A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a:
      fixT T a ≡ T (fixT T) a.
    Proof.
      rewrite fixT_unseal /fixT_def /= type_fixpoint_unfold2 //.
    Qed.

    Lemma fixT_has_size {A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a n:
      (∀ R a, T R a `has_size` n) →
      ((fixT T a)%I `has_size` n).
    Proof. rewrite fixT_unseal /fixT_def //. Qed.

    Lemma fixT_has_bounds {A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a n:
      (∀ R a, T R a `has_bounds` n) →
      ((fixT T a)%I `has_bounds` n).
    Proof. rewrite fixT_unseal /fixT_def //. Qed.

    Global Instance fixT_ptr_type {A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a:
      (∀ R a, PtrType (T R a)) →
      (PtrType (fixT T a)).
    Proof. rewrite fixT_unseal /fixT_def //. Qed.

    Global Instance fixT_copy {A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a:
      (∀ R a, Copy (T R a)) →
      (Copy (fixT T a)).
    Proof. rewrite fixT_unseal /fixT_def //. Qed.

    Global Instance fixT_non_null {A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a:
      (∀ R a, NonNull (T R a)) →
      (NonNull (fixT T a)).
    Proof. rewrite fixT_unseal /fixT_def //. Qed.

    Global Instance fixT_non_null_val {A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a:
      (∀ R a, NonNullVal (T R a)) →
      (NonNullVal (fixT T a)).
    Proof. rewrite fixT_unseal /fixT_def //. Qed.

  End fixp.
End combinators.


Global Notation "'ex' x .. y , P" := (existsT (λ x , .. (existsT (λ y , P)%RT) ..)%RT) (at level 200, x binder, y binder, right associativity, format "'[ ' '[ ' 'ex'  x  ..  y , ']'  '/' P ']'") : ref_type_scope.
Global Notation "{ A | φ }" := (constraintT A φ) (at level 0, A at level 99) : ref_type_scope.
Global Notation "A '∗∗' P" := (sepT A%RT P%I) (at level 61, left associativity): ref_type_scope.
