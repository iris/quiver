From quiver.base Require Import classes.
From quiver.thorium.types Require Import types combinators base_types pointers arrays.


(* Refinement Types *)
Record rtype `{!refinedcG Σ} {X: Type} : Type := RType {
  rty : X → type Σ;
  rty_inhabited : Inhabited X
}.

Global Arguments rtype _ {_} _.
Add Printing Constructor rtype.
Global Arguments RType {_ _ _} _%RT {_}.
Global Arguments rty {_ _ _} _%RT _.
Global Existing Instance rty_inhabited.


(* refinement notation *)
Definition with_refinement `{!refinedcG Σ} {X} (T : rtype Σ X) (x : X) : type Σ := T.(rty) x.
Global Arguments with_refinement {_ _ _} _%RT _ : simpl never.
Notation "x @ r" := (with_refinement r x) (at level 14) : ref_type_scope.


(* checking for refinements *)
Definition is_refinement `{!refinedcG Σ} (A: type Σ) {X} (R: rtype Σ X) (x: X) :=
  A ≡ (x @ R)%RT.

Existing Class is_refinement.
Global Hint Mode is_refinement - - ! - - - : typeclass_instances.

Ltac is_refinement :=
  match goal with
  |- is_refinement (with_refinement ?R1 ?x1) ?R2 ?x2 =>
      unify R1 R2; unify x1 x2; rewrite /is_refinement; reflexivity
  end.

Global Hint Extern 1 (is_refinement _ _ _) => is_refinement : typeclass_instances.

Global Instance type_params_refinement `{!refinedcG Σ} (A: type Σ) {X} (R: rtype Σ X) (x: X) :
  is_refinement A R x →
  TypeParams A X x (λ y, y @ R)%RT eq | 10.
Proof.
  intros Href ? ?; simpl in *. subst. split.
  by rewrite Href.
Qed.


(* tactics for simplifying refinements *)
Ltac simpl_type_core_refinement ::=
  match goal with
  | |- context C [ty_own (?x @ {| rty := ?f |})%RT] =>
      let G := context C [let '({| ty_own := y |}) := (f x) in y ] in
      change G
  | |- context C [ty_own_val (?x @ {| rty := ?f |})%RT] =>
      let G := context C [let '({| ty_own_val := y |}) := (f x) in y ] in
      change G
  end.


Ltac unfold_type_equiv_refinement ::=
  match goal with
  | |- (?a @ ?ty1)%RT ⊑ (?b @ ?ty2)%RT => change (rty ty1 a ⊑ rty ty2 b); simpl
  | |- (?a @ ?ty1)%RT ≡ (?b @ ?ty2)%RT => change (rty ty1 a ≡ rty ty2 b); simpl
  end.








Section fixpoints.
  Context {Σ} `{!refinedcG Σ}.


  Definition fixR {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) : rtype Σ A := RType (fixT T).

  Lemma fixR_equiv {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a:
    (a @ fixR T)%RT ≡ T (rty (fixR T)) a.
  Proof.
    rewrite /fixR /with_refinement /rty fixT_equiv //.
  Qed.

  Lemma fixR_unfold_loc {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} l a:
    l ◁ₗ (a @ fixR T) ⊣⊢ l ◁ₗ T (rty (fixR T)) a.
  Proof.
    rewrite fixR_equiv //.
  Qed.

  Lemma fixR_unfold_val {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} v a:
    v ◁ᵥ a @ fixR T ⊣⊢ v ◁ᵥ T (rty (fixR T)) a.
  Proof.
    rewrite fixR_equiv //.
  Qed.

  Lemma fixR_has_size {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a n:
    (∀ R a, T R a `has_size` n) →
    ((a @ fixR T) `has_size` n).
  Proof. rewrite /with_refinement /rty /fixR. by eapply fixT_has_size. Qed.

  Lemma fixR_has_bounds {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a n:
    (∀ R a, T R a `has_bounds` n) →
    ((a @ fixR T) `has_bounds` n).
  Proof. rewrite /with_refinement /rty /fixR. by eapply fixT_has_bounds. Qed.

  Global Instance fixR_ptr_type {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a:
    (∀ R a, PtrType (T R a)) →
    (PtrType (a @ fixR T)).
  Proof. rewrite /with_refinement /rty /fixR; by eapply fixT_ptr_type. Qed.

  Global Instance fixR_copy {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a:
    (∀ R a, Copy (T R a)) →
    (Copy (a @ fixR T)).
  Proof. rewrite /with_refinement /rty /fixR. by eapply fixT_copy. Qed.

  Global Instance fixR_non_null {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a:
    (∀ R a, NonNull (T R a)) →
    (NonNull (a @ fixR T)).
  Proof. rewrite /with_refinement /rty /fixR. by eapply fixT_non_null. Qed.

  Global Instance fixR_non_null_val {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a:
    (∀ R a, NonNullVal (T R a)) →
    (NonNullVal (a @ fixR T)).
  Proof. rewrite /with_refinement /rty /fixR. by eapply fixT_non_null_val. Qed.


End fixpoints.

Global Typeclasses Opaque fixR.
Notation "'μ' A x '.'  L" := (fixR (λ A x, L)) (at level 60, A binder, x binder) : ref_type_scope.


Section lambdas.
  Context `{!refinedcG Σ}.

  Definition lamT_def {A: Type} (T: A → type Σ) : A → type Σ := T.
  Definition lamT_aux : seal (@lamT_def). by eexists. Qed.
  Definition lamT {A} := lamT_aux.(unseal) A.
  Definition lamT_eq : @lamT = @lamT_def := lamT_aux.(seal_eq).

  Definition lamR {A} `{!Inhabited A} (T: A → type Σ) : rtype Σ A := RType (lamT T).

  Lemma lamR_equiv {A} `{!Inhabited A} (T: A → type Σ) a:
    (a @ lamR T)%RT ≡ T a.
  Proof.
    rewrite /lamR /with_refinement /rty lamT_eq //.
  Qed.

  Lemma lamR_unfold_loc {A} `{!Inhabited A} (T: A → type Σ) l a:
    l ◁ₗ (a @ lamR T) ⊣⊢ l ◁ₗ T a.
  Proof. rewrite lamR_equiv //. Qed.

  Lemma lamR_unfold_val {A} `{!Inhabited A} (T: A → type Σ) v a:
    v ◁ᵥ a @ lamR T ⊣⊢ v ◁ᵥ T a.
  Proof. rewrite lamR_equiv //. Qed.

  Lemma lamR_has_size {A} `{!Inhabited A} (T: A → type Σ) a n:
    (T a `has_size` n) →
    ((a @ lamR T) `has_size` n).
  Proof.
    rewrite /with_refinement /rty /lamR lamT_eq /lamT_def //.
  Qed.

  Lemma lamR_has_bounds {A} `{!Inhabited A} (T: A → type Σ) a n:
    (T a `has_bounds` n) →
    ((a @ lamR T) `has_bounds` n).
  Proof.
    rewrite /with_refinement /rty /lamR lamT_eq /lamT_def //.
  Qed.

  Global Instance lamR_ptr_type {A} `{!Inhabited A} (T: A → type Σ) a:
    (PtrType (T a)) →
    (PtrType (a @ lamR T)).
  Proof.
    rewrite /with_refinement /rty /lamR lamT_eq /lamT_def //.
  Qed.

  Global Instance lamR_copy {A} `{!Inhabited A} (T: A → type Σ) a:
    (Copy (T a)) →
    (Copy (a @ lamR T)).
  Proof.
    rewrite /with_refinement /rty /lamR lamT_eq /lamT_def //.
  Qed.

  Global Instance lamR_non_null {A} `{!Inhabited A} (T: A → type Σ) a:
    (NonNull (T a)) →
    (NonNull (a @ lamR T)).
  Proof.
    rewrite /with_refinement /rty /lamR lamT_eq /lamT_def //.
  Qed.

  Global Instance lamR_non_null_val {A} `{!Inhabited A} (T: A → type Σ) a:
    (NonNullVal (T a)) →
    (NonNullVal (a @ lamR T)).
  Proof.
    rewrite /with_refinement /rty /lamR lamT_eq /lamT_def //.
  Qed.

End lambdas.

Global Typeclasses Opaque lamR.
Notation "'η' x '.'  L" := (lamR (λ x, L)) (at level 60, x binder) : ref_type_scope.



(* TYPECLASSES FOR REFINEMENT TYPES *)

Definition unfold_refinement `{!refinedcG Σ} {X} (R: rtype Σ X) (x: X) (A: type Σ) : Prop :=
  (x @ R)%RT ≡ A.

Existing Class unfold_refinement.
Global Hint Mode unfold_refinement - - - ! - - : typeclass_instances.


Definition unfold_fix `{!refinedcG Σ} {X} (R: rtype Σ X) (x: X) {B: Type} (F: (B → type Σ) → (B → type Σ)) (A: type Σ) : Prop :=
  (x @ R)%RT ≡ A.

Ltac unfold_fix :=
  match goal with
  |- unfold_fix ?R ?x ?F ?A =>
    rewrite /unfold_fix; rewrite fixR_equiv;
    change (rty (fixR F)) with (with_refinement R);
    rewrite {1}/F; reflexivity
  end.

Existing Class unfold_fix.
Global Hint Mode unfold_fix - - - ! - - - - : typeclass_instances.
Global Hint Extern 1 (unfold_fix _ _ _ _) => unfold_fix : typeclass_instances.


Global Instance unfold_refinement_fixR `{!refinedcG Σ} {A} `{!Inhabited A} (R: rtype Σ A) (x: A) (F: (A → type Σ) → A → type Σ) `{!TypeMono F} B :
  unify R (fixR F) →
  unfold_fix R x F B →
  unfold_refinement R x B | 1.
Proof.
  intros ->. rewrite /unfold_refinement /unfold_fix.
  rewrite fixR_equiv //.
Qed.

Global Instance unfold_refinement_lamR `{!refinedcG Σ} {A} `{!Inhabited A} (R: rtype Σ A) (x: A) (F: A → type Σ) :
  unify R (lamR F) →
  unfold_refinement R x (F x) | 1.
Proof.
  intros ->. rewrite /unfold_refinement /unfold_fix.
  rewrite lamR_equiv //.
Qed.


Global Instance unfold_refinement_rtype `{!refinedcG Σ} {A: Type} `{!Inhabited A} (F: A → type Σ) (x: A) B :
  Simpl (F x) B →
  unfold_refinement (RType F) x B | 2.
Proof. rewrite /unfold_refinement /with_refinement /rty. by intros ->. Qed.



Section basic_refinement_types.
  Context {Σ} `{!refinedcG Σ}.

  Definition intR (it: int_type) : rtype Σ Z := RType (intT it).
  Definition boolR (it: int_type) : rtype Σ dProp := RType (λ φ: dProp, boolT it (bool_decide φ)).


  Global Instance intR_copy it n: Copy (n @ intR it).
  Proof.
    rewrite /with_refinement; apply _.
  Qed.

  Global Instance boolR_copy it φ: Copy (φ @ boolR it).
  Proof.
    rewrite /with_refinement; apply _.
  Qed.

  Lemma boolR_iff_eq (φ ψ: dProp) it:
    (φ ↔ ψ) → (φ @ boolR it)%RT ≡ (ψ @ boolR it)%RT.
  Proof.
    intros Hiff. rewrite /boolR /with_refinement /=.
    rewrite bool_decide_decide.
    setoid_rewrite (decide_ext φ); eauto.
    rewrite -bool_decide_decide //.
  Qed.

  Global Instance type_params_bool it φ:
    TypeParams (φ @ boolR it)%RT (dProp * int_type) (φ, it) (λ x, x.1 @ boolR x.2)%RT (prod_relation dprop_iff eq) | 1.
  Proof.
    rewrite /TypeParams. intros [? ?] [? ?]; simpl in *. subst.
    split. rewrite boolR_iff_eq //.
  Qed.

End basic_refinement_types.