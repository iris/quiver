From quiver.base Require Export facts dprop.
From quiver.thorium.types Require Export types.
From quiver.thorium.types Require Import pointers arrays structs base_types combinators .



Section byte_types.
  Context `{!refinedcG Σ}.

  (** The [zerosT] type. *)

  Definition zerosT_def (n: Z) : type Σ :=
    valueT (zero_bytes (Z.to_nat n)) n.
  Definition zerosT_aux : zerosT_def.(seal).
  Proof. by eexists. Qed.
  Definition zerosT := zerosT_aux.(unseal).
  Definition zerosT_eq : zerosT = zerosT_def :=
    seal_eq zerosT_aux.

  Lemma valueT_has_size_val x v n:
    x ◁ₓ valueT v n -∗ ⌜v `has_size_val` n⌝.
  Proof.
    destruct x as [l|w]; rewrite valueT_eq /valueT_def /=.
    all: iIntros "[_ $]".
  Qed.

  Lemma xtr_zerosT_iff x n:
    x ◁ₓ zerosT n ⊣⊢ x ◁ₓ valueT (zero_bytes (Z.to_nat n)) n.
  Proof.
    rewrite zerosT_eq /zerosT_def //.
  Qed.

  Lemma nat_pow_gt_zero n m:
    (0 < n)%nat → (0 < n ^ m)%nat.
  Proof.
    intros Hn. induction m as [|m IH]; simpl; lia.
  Qed.

  Lemma ly_size_int_type_non_zero (it: int_type):
    ly_size it > 0.
  Proof.
    destruct it as [sz sn]; rewrite /ly_size /= /bytes_per_int /=.
    feed pose proof (nat_pow_gt_zero 2 sz) as Hsz; lia.
  Qed.

  Lemma zerosT_intT (it: int_type) :
    zerosT (int_size it) ≡ intT it 0.
  Proof.
    eapply type_equiv_xtr_intro; intros x.
    rewrite zerosT_eq /zerosT_def intT_eq /intT_def.
    rewrite xtr_constraintT_iff. rewrite zero_bytes_i2v.
    rewrite int_size_ly_size.
    replace (Z.to_nat (ly_size it)) with (ly_size it) by lia.
    iSplit.
    - iIntros "$". iPureIntro. by eapply zero_in_int_type.
    - iIntros "[$ _]".
  Qed.

  Lemma zerosT_boolT (it: int_type) :
    zerosT (int_size it) ≡ boolT it false.
  Proof.
    eapply type_equiv_xtr_intro; intros x.
    rewrite boolT_eq /boolT_def.
    rewrite zerosT_intT. done.
  Qed.

  Lemma zerosT_nullT lv :
    lv ◁ₓ zerosT ptr_size -∗ lv ◁ₓ nullT.
  Proof.
    iIntros "Ha".
    rewrite zerosT_eq /zerosT_def nullT_eq /nullT_def.
    rewrite xtr_existsT_iff. setoid_rewrite xtr_constraintT_iff.
    iExists _. iFrame. by rewrite val_to_loc_null_bytes.
  Qed.

  (* copy *)
  Global Instance zeros_copy n :
    Copy (zerosT n).
  Proof.
    rewrite zerosT_eq /zerosT_def. apply _.
  Qed.

  Global Instance non_null_zeros n:
    NonNull (zerosT n).
  Proof.
    rewrite zerosT_eq /zerosT_def. apply _.
  Qed.


  (* zerosT_has_size *)
  Lemma zerosT_has_size n m:
    zerosT n `has_size` m ↔ n = m.
  Proof.
    rewrite zerosT_eq /zerosT_def. apply valueT_has_size.
  Qed.

  Lemma repeat_replicate {A: Type} n (x: A):
    repeat x n = replicate n x.
  Proof.
    induction n; simpl; congruence.
  Qed.


  Lemma seq_val_zeros_proj (i li n: nat):
    (i + li ≤ n)%nat → seq_val i li (zero_bytes n) = zero_bytes li.
  Proof.
    intros Hle. rewrite /seq_val /zero_bytes.
    rewrite !repeat_replicate. rewrite drop_replicate.
    rewrite take_replicate. f_equal. lia.
  Qed.

  Lemma seq_val_zeros_comb v (i li lj: nat):
    seq_val i li v = zero_bytes li →
    seq_val (i + li) lj v = zero_bytes lj →
    seq_val i (li + lj) v = zero_bytes (li + lj).
  Proof.
    rewrite /seq_val /zero_bytes. rewrite !repeat_replicate.
    rewrite replicate_add. intros <- <-.
    rewrite -take_take_drop drop_drop //.
  Qed.

  Lemma zerosT_split_seq_val v (i j: nat):
    v ◁ᵥ zerosT (i + j) ⊣⊢ v ◁ᵥ seqT (i + j) [(0%nat, i); (i, j)] [zerosT i; zerosT j].
  Proof.
    rewrite zerosT_eq /zerosT_def seqT_eq /seqT_def /=.
    rewrite valueT_eq /valueT_def /=.
    iSplit; iIntros "%Hx"; iPureIntro.
    - destruct Hx as [-> _].
      replace (Z.to_nat (i + j)) with (i + j)%nat by lia.
      replace (Z.to_nat i) with i by lia.
      replace (Z.to_nat j) with j by lia.
      rewrite /has_size_val. split_and!; eauto using covers_pair, zero_bytes_length.
      + rewrite seq_val_zeros_proj //. lia.
      + rewrite seq_val_zeros_proj //.
    - destruct Hx as (Hsz & Hcov & [Hz1 _] & [Hz2 _] & _).
      replace (Z.to_nat (i + j)) with (i + j)%nat in * by lia.
      rewrite /has_size_val. rewrite zero_bytes_length. split; last lia.
      feed pose proof (seq_val_zeros_comb v 0 i j) as Hij.
      + rewrite Hz1. f_equal. lia.
      + rewrite Hz2. f_equal. lia.
      + rewrite -Hij. rewrite seq_val_zero //.
        rewrite /has_size_val in Hsz. lia.
  Qed.

  (* splitting *)
  Lemma zerosT_split_seq (i j: nat):
    zerosT (i + j) ≡ seqT (i + j) [(0%nat, i); (i, j)] [zerosT i; zerosT j].
  Proof.
    eapply type_equiv_xtr_intro=> x. destruct x; simpl.
    - eapply ty_own_ty_val_equiv.
      + eapply zerosT_has_size. done.
      + eapply seqT_has_size. rewrite /elements_well_sized.
        rewrite !Forall2_cons Forall2_nil. rewrite !zerosT_has_size //.
      + intros ?. rewrite zerosT_split_seq_val //.
    - rewrite zerosT_split_seq_val //.
  Qed.

  Lemma zerosT_split i j:
    0 ≤ i →
    0 ≤ j →
    zerosT (i + j) ≡ slicesT (i + j) [(0, i, zerosT i); (i, j, zerosT j)].
  Proof.
    intros ??. eapply type_equiv_xtr_intro=> x.
    rewrite /slicesT xtr_constraintT_iff /=.
    replace i with (Z.to_nat i : Z) by lia.
    replace j with (Z.to_nat j : Z) by lia.
    rewrite !Nat2Z.id //.
    replace ((Z.to_nat (Z.to_nat i + Z.to_nat j))) with ((Z.to_nat i + Z.to_nat j))%nat by lia.
    rewrite -zerosT_split_seq. iSplit.
    - iIntros "$". iPureIntro. rewrite !Forall_cons !Forall_nil /=. lia.
    - iIntros "[$ _]".
  Qed.

  Lemma zeros_to_null :
    zerosT ptr_size ⊑ nullT.
  Proof.
    eapply type_le_xtr_intro; intros x.
    rewrite zerosT_eq /zerosT_def nullT_eq /nullT_def //=.
    rewrite xtr_existsT_iff. setoid_rewrite xtr_constraintT_iff.
    iIntros "Hx". iExists _. iFrame. replace (Z.to_nat ptr_size) with ptr_size by lia.
    rewrite val_to_loc_null_bytes //.
  Qed.


  Lemma zerosT_to_structT l (s : struct_layout) :
    (l ◁ₗ zerosT (ly_size s)) ⊢ (l ◁ₗ structT s (zerosT <$> omap (λ '(n, ly), const (ly_size ly: Z) <$> n) s.(sl_members))).
  Proof.
    rewrite /list_fmap /list_omap {1}zerosT_eq /zerosT_def valueT_eq /valueT_def /=.
    rewrite structT_eq /structT_def /=. iIntros "[Hl _]". iSplit; last iSplit.
    { iPureIntro. rewrite fmap_length. by apply omap_length_eq => i [[?|]?]. }
    { iApply loc_in_bounds_shorten; last by iApply heap_mapsto_loc_in_bounds. rewrite zero_bytes_length.
      rewrite /layout_of {2}/ly_size. lia. }
    rewrite /layout_of {1}/ly_size. rewrite Nat2Z.id.
    rewrite zero_bytes_sum_list.
    iInduction (sl_members s) as [|[n ly] ms] "IH" forall (l) => //=.
    iDestruct (heap_mapsto_app with "Hl") as "[Hl Hr]".
    rewrite shift_loc_0.
    rewrite {2}/offset_of_idx /=. setoid_rewrite <-shift_loc_assoc_nat.
    iSplitL "Hl"; destruct n; simpl.
    + setoid_rewrite (xtr_zerosT_iff (LOC l)); simpl.
      rewrite Nat2Z.id. iApply valueT_intro; last done.
      rewrite /has_size_val. rewrite zero_bytes_length //.
    + rewrite ltr_anyT_pts_ly. iApply heap_mapsto_layout_iff. iExists _. iFrame.
      iPureIntro. rewrite /has_layout_val. rewrite zero_bytes_length //.
    + iApply "IH" => //. rewrite zero_bytes_length //.
    + iApply "IH" => //. rewrite zero_bytes_length //.
  Qed.

  Lemma vtr_zerosT_to_structT v (s : struct_layout) :
    (v ◁ᵥ zerosT (ly_size s)) ⊢ (v ◁ᵥ structT s (zerosT <$> omap (λ '(n, ly), const (ly_size ly: Z) <$> n) s.(sl_members))).
  Proof.
    rewrite /list_fmap /list_omap {1}zerosT_eq /zerosT_def valueT_eq /valueT_def /=.
    rewrite structT_eq /structT_def /=. iIntros "[-> %]". iSplit; last iSplit.
    { iPureIntro. by rewrite -has_size_val_has_layout. }
    { iPureIntro. rewrite fmap_length. apply omap_length_eq => ? [[?|]?]/=; done. }
    rewrite /layout_of {2}/ly_size. rewrite Nat2Z.id.
    rewrite zero_bytes_sum_list.
    iInduction (sl_members s) as [|[n ly] ms] "IH" => //.
    destruct n; cbn.
    all: rewrite take_app_alt ?drop_app_alt ?zero_bytes_length //; iFrame "IH".
    - setoid_rewrite (xtr_zerosT_iff (VAL _)); simpl.
      rewrite Nat2Z.id. iApply vtr_valueT_intro. iPureIntro. split; [done|].
      rewrite /has_size_val. by rewrite zero_bytes_length.
    - iApply vtr_anyT_intro. rewrite /has_size_val. by rewrite zero_bytes_length.
  Qed.

  Lemma xtr_zerosT_to_structT lv (s : struct_layout) :
    (lv ◁ₓ zerosT (ly_size s)) ⊢ (lv ◁ₓ structT s (zerosT <$> omap (λ '(n, ly), const (ly_size ly: Z) <$> n) s.(sl_members))).
  Proof. destruct lv; [apply zerosT_to_structT|apply vtr_zerosT_to_structT]. Qed.

  Lemma zerosT_to_structT_member_layouts lv (sl: struct_layout):
    lv ◁ₓ zerosT (ly_size sl) ⊢ lv ◁ₓ structT sl (zerosT <$> member_sizes sl).
  Proof.
    rewrite xtr_zerosT_to_structT.
    rewrite /member_sizes /member_layouts //.
    rewrite -(list_fmap_compose _ Z.of_nat).
    rewrite !list_fmap_omap /=. f_equiv. f_equiv.
    apply list_omap_ext, Forall_Forall2_diag, Forall_forall.
    by move => [[?|]?] /=.
  Qed.


  Lemma zerosT_non_negative x n:
    x ◁ₓ zerosT n ⊢ ⌜0 ≤ n⌝.
  Proof.
    eapply xtr_size_non_zero. by apply zerosT_has_size.
  Qed.

  Global Instance type_params_zeros n :
    TypeParams (zerosT n) Z n zerosT eq | 1.
  Proof.
    intros n' ?; simpl in *; by subst.
  Qed.


  Lemma zeros_any:
    zerosT 0 ≡ anyT 0.
  Proof.
    eapply type_equiv_xtr_intro=> x.
    iSplit.
    - iIntros "Hz". iApply xtr_cast_to_anyT; last done.
      eapply zerosT_has_size. done.
    - iIntros "Hany".
      rewrite anyT_eq /anyT_def  zerosT_eq /zerosT_def.
      rewrite xtr_existsT_iff. iDestruct "Hany" as "[%v Hv]".
      iDestruct (valueT_has_size_val with "Hv") as "%Hsz".
      destruct v; last done. done.
  Qed.

  Lemma array_replicate_zeros_equiv len n:
    type_arrayT len (replicate n (zerosT len)) ≡ zerosT (n * len).
  Proof.
    rewrite type_arrayT_eq /type_arrayT_def. rewrite replicate_length.
    induction n.
    - simpl; rewrite Z.mul_0_l. rewrite /type_arrayT_slices /=.
      rewrite seqT_empty_is_any_zero. rewrite zeros_any //.
    - replace (S n) with (n + 1)%nat at 3 by lia. rewrite replicate_add /=.
      replace ((S n * len)%Z : Z) with ((n * len)%nat + len)%Z by lia.
      rewrite zerosT_split_seq.
      symmetry. etrans.
      { eapply (sliced_compat_equiv _ _ _ nil); symmetry.
        replace ((n * len)%nat: Z) with (n * len)%Z by lia. eapply IHn. }
      simpl; etrans.
      { eapply (sliced_compat_equiv _ _ _ [_]). symmetry. by eapply seqT_singleton, zerosT_has_size. }
      simpl. etrans.
      { symmetry. replace (n * len)%nat with (len * n)%nat by lia.
        eapply seqT_split; rewrite ?replicate_length ?type_array_slices_length ?type_array_slices_slices_length /=; lia. }
      simpl. replace (S n) with (n + 1)%nat at 1 by lia.
      rewrite type_array_slices_add /=. replace (len * n + 0)%nat with (n * len + 0)%nat by lia.
      by replace (len * n + len)%nat with (len + n * len)%nat by lia.
  Qed.

  Lemma zeros_type_arrayT (it: int_type) (m: nat):
    zerosT (ly_size it * m) ≡ (type_arrayT (ly_size it) (replicate m (intT it 0))).
  Proof.
    rewrite Z.mul_comm.
    rewrite -array_replicate_zeros_equiv.
    rewrite -zerosT_intT.
    rewrite int_size_ly_size //.
  Qed.

  Lemma zeros_arrayT (it: int_type) (m: nat):
    zerosT (ly_size it * m) ≡ (arrayT (intT it) (ly_size it) (replicate m 0)).
  Proof.
    rewrite arrayT_eq /arrayT_def. eapply type_equiv_xtr_intro=>x.
    rewrite xtr_constraintT_iff. rewrite fmap_replicate.
    rewrite zeros_type_arrayT.
    replace (Z.to_nat (ly_size it)) with (ly_size it) by lia.
    iSplit.
    - iIntros "$". iPureIntro. split; first lia.
      intros z. rewrite -int_size_ly_size. eapply intT_has_size.
    - iIntros "[$ _]".
  Qed.


End byte_types.


Notation "'zeros[' len ']'" := (zerosT len) (at level 60, format "zeros[ len ]") : ref_type_scope.
