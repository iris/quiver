From iris.algebra Require Import list.
From quiver.thorium.types Require Export types.
From quiver.thorium.types Require Import base_types combinators pointers.
From Coq Require Import Nat.
Set Default Proof Using "Type".




Section seq.
  Context `{!refinedcG Σ}.

  Implicit Types
    (i li len: nat)
    (Ls: list (nat * nat))
    (vs: val).

  Open Scope nat.

  Inductive covers : list (nat * nat) → nat → Prop :=
  | covers_nil : covers [] 0
  | covers_cons i li Ls :
     covers Ls i →
     covers (Ls ++ [(i, li)]) (i + li).
  Lemma covers_cons' (i li : nat) Ls (n : nat) :
    covers Ls i → n = (i + li)%nat → covers (Ls ++ [(i, li)]) n.
  Proof.
    intros ? ->. by econstructor.
  Qed.

  Definition seq_val i li vs : val := take li (drop i vs).

  Definition seq_vals Ls vs : list val :=
    map (λ '(i, li), seq_val i li vs) Ls.

  Definition val_contains_slice i li vs : Prop :=
    ((i + li) ≤ length vs).

  Definition val_contains_slices Ls vs :=
    Forall (λ '(i, li), val_contains_slice i li vs) Ls.

  Definition shift_slices (i : nat) (Ls : list (nat * nat)) : list (nat * nat) :=
    map (λ '(i', li), ((i + i'), li)) Ls.

  Lemma shift_slices_app i As Bs :
    shift_slices i (As ++ Bs) = shift_slices i As ++ shift_slices i Bs.
  Proof.
    rewrite /shift_slices. by rewrite map_app.
  Qed.

  Lemma val_contains_slice_extend i li vs vs' :
    length vs ≤ length vs' →
    val_contains_slice i li vs →
    val_contains_slice i li vs'.
  Proof.
    rewrite /val_contains_slice. intros ??. lia.
  Qed.

  Lemma val_contains_slices_extend Ls vs vs' :
    length vs ≤ length vs' →
    val_contains_slices Ls vs →
    val_contains_slices Ls vs'.
  Proof.
    rewrite /val_contains_slices; induction 2 as [|[i len]]; first done.
    apply Forall_cons; eauto using val_contains_slice_extend.
  Qed.

  Lemma covers_contains_slice Ls len vs:
    covers Ls len →
    length vs = len →
    val_contains_slices Ls vs.
  Proof.
    induction 1 as [|] in vs |-*; simpl.
    - intros ?; by apply Forall_nil.
    - intros Hlen. eapply Forall_app. split.
      + feed pose proof (IHcovers (take i vs)).
        { rewrite take_length. lia. }
        eapply Forall_impl; first done. intros [i' len'].
        eapply val_contains_slice_extend. rewrite take_length. lia.
      + rewrite Forall_cons Forall_nil. split; last done.
        rewrite /val_contains_slice Hlen. done.
  Qed.

  Lemma covers_contains_slice_lookup Ls vs k i li len :
    covers Ls len →
    Ls !! k = Some (i, li) →
    length vs = len →
    val_contains_slice i li vs.
  Proof.
    intros Hcov ? ?. eapply covers_contains_slice in Hcov; eauto.
    eapply Forall_lookup in Hcov; eauto. simpl in Hcov. done.
  Qed.

  Lemma covers_iff Ls len :
    covers Ls len ↔
    (Ls = nil ∧ len = 0) ∨ (∃ i li Ls', Ls = Ls' ++ [(i, li)] ∧ covers Ls' i ∧ (len = i + li)).
  Proof.
    split.
    - destruct 1 as [|i li Ls' Hcov]; eauto 10.
    - destruct 1 as [[? ?]|(?&?&?&?&?&?)]; subst; eauto using covers_nil, covers_cons.
  Qed.

  Lemma covers_nil_iff len :
    covers nil len ↔ len = 0.
  Proof.
    rewrite covers_iff. split.
    - destruct 1 as [|(i & li & Ls' & Heq1 & _)]; first naive_solver.
      destruct Ls'; naive_solver.
    - naive_solver.
  Qed.

  Lemma covers_app_singleton_iff Ls len i li :
    covers (Ls ++ [(i, li)]) len ↔
    covers Ls i ∧ len = (i + li).
  Proof.
    rewrite covers_iff. split.
    - destruct 1 as [|(i' & li' & Ls' & Heq1 & Hcov & Heq2)]; subst; eauto.
      + destruct Ls; naive_solver.
      + eapply app_inj_tail in Heq1 as [? Ht]. by simplify_eq.
    - intros [Hcov ->]. eauto 10.
  Qed.

  Lemma covers_app_shift Ls Ls' i li:
    covers Ls i →
    covers Ls' li →
    covers (Ls ++ shift_slices i Ls') (i + li).
  Proof.
    intros Hcov1 Hcov2. induction Hcov2.
    - rewrite app_nil_r Nat.add_0_r. done.
    - rewrite shift_slices_app app_assoc.
      rewrite covers_app_singleton_iff. split; first done. lia.
  Qed.

  Lemma covers_app_shift_inv Ls1 Ls2 len :
    covers (Ls1 ++ Ls2) len →
    ∃ len1 len2 Ls2', covers Ls1 len1 ∧ Ls2 = shift_slices len1 Ls2' ∧ covers Ls2' len2 ∧ (len = len1 + len2).
  Proof.
    revert len; induction Ls2 as [|[i li] Ls2 IH] using list_snoc_ind; intros len.
    - rewrite app_nil_r. intros Hcov. exists len, 0, nil. split_and!; eauto with lia.
      constructor.
    - rewrite app_assoc. rewrite covers_app_singleton_iff.
      intros [Hcov1 Hlen]. subst len. eapply IH in Hcov1.
      destruct Hcov1 as (len1 & len2 & Ls2' & Hcov1 & Hlen & Hcov2 & Hlen2). subst.
      exists len1, (len2 + li), (Ls2' ++ [(len2, li)]).
      rewrite covers_app_singleton_iff shift_slices_app {3}/shift_slices /=.
      split_and!; eauto with lia.
  Qed.

  Lemma covers_app_iff As Bs len :
    covers (As ++ Bs) len ↔
    ∃ lA lB Cs, covers As lA ∧ Bs = shift_slices lA Cs ∧ covers Cs lB ∧ (len = lA + lB).
  Proof.
    split.
    - eapply covers_app_shift_inv.
    - intros (lA & lB & Cs & Hcov1 & Hlen & Hcov2 & Hlen2). subst.
      eapply covers_app_shift; eauto.
  Qed.

  Lemma covers_injective As len1 len2 :
    covers As len1 →
    covers As len2 →
    len1 = len2.
  Proof.
    induction 1 in len2 |-*.
    - rewrite covers_nil_iff. lia.
    - rewrite covers_app_singleton_iff. lia.
  Qed.

  Lemma covers_middle_iff As i li Bs len :
    covers (As ++ (i, li) :: Bs) len ↔
    (∃ Cs lj, covers As i ∧ covers Cs lj ∧ (len = i + li + lj) ∧ Bs = shift_slices (i + li) Cs).
  Proof.
    rewrite cons_middle app_assoc. rewrite covers_app_iff. split.
    - intros (lA & lB & Cs & Hcov1 & Hlen & Hcov2 & Hlen2).
      eapply covers_app_iff in Hcov1.
      destruct Hcov1 as (lA' & lB' & Cs' & Hcov1 & Hlen' & Hcov2' & Hlen2').
      destruct Cs' as [|[j lj] Cs']; first discriminate.
      destruct Cs'; last discriminate. injection Hlen'; intros. clear Hlen'.
      eapply (covers_app_singleton_iff nil ) in Hcov2' as [Hcov2' Heq].
      eapply covers_nil_iff in Hcov2'. subst. simpl. rewrite Nat.add_0_r.
      eexists _, _; split_and!; eauto.
    - intros (Cs & lj & Hcov1 & Hcov2 & Hlen & Hlen2). subst.
      eexists _, _, _; split_and!; eauto.
      eapply covers_app_singleton_iff; split; eauto.
  Qed.

  Lemma covers_shift_unique As Bs len1 len2 i j:
    covers As len1 →
    covers Bs len2 →
    shift_slices i As = shift_slices j Bs →
    len1 = len2 ∧ As = Bs.
  Proof.
    induction 1 as [|i' len As Hcov IH] in len2, i, j, Bs |-*.
    - destruct Bs; last discriminate.
      rewrite covers_nil_iff. intros -> _. split_and!; eauto.
    - destruct (list_inv_snoc Bs) as [->|([k lk] & Bs' & ->)]; first (by destruct As; discriminate).
      rewrite covers_app_singleton_iff. intros [Hcov2 ->].
      rewrite !shift_slices_app. intros [Heq Heq']%app_inj_tail.
      injection Heq'; intros; subst; clear Heq'.
      eapply IH in Hcov2 as [-> ->]; done.
  Qed.

  Lemma shift_slices_injective As i j :
    shift_slices i As = shift_slices j As →
    As ≠ nil →
    i = j.
  Proof.
    destruct As as [|[z lz] As]; first naive_solver.
    injection 1 as ??. lia.
  Qed.

  Lemma covers_contains As len:
    covers As len →
    Forall (λ '(i, li), i + li ≤ len) As.
  Proof.
    induction 1; simpl.
    - done.
    - rewrite Forall_app; split.
      { eapply Forall_impl; first done. intros [j lj]. lia. }
      rewrite Forall_cons Forall_nil. lia.
  Qed.

  Lemma covers_succ As i li j lj Bs len:
    covers (As ++ (i, li) :: (j, lj) :: Bs) len → (j = i + li)%nat.
  Proof.
    rewrite cons_middle app_assoc.
    rewrite covers_middle_iff.
    setoid_rewrite covers_app_singleton_iff.
    naive_solver.
  Qed.


  Lemma seq_val_slices' As len vs ws :
    covers As len →
    length vs = len →
    vs = concat (seq_vals As (vs ++ ws)).
  Proof.
    rewrite /seq_vals.
    induction 1 as [| ] in vs, ws |-*; simpl.
    - destruct vs; first done. simpl; lia.
    - intros Hlen. rewrite map_app concat_app.
      feed pose proof (IHcovers (take i vs) (drop i vs ++ ws)) as Hx.
      { rewrite take_length. lia. }
      rewrite app_assoc in Hx. rewrite take_drop in Hx.
      rewrite -Hx. clear Hx. simpl. rewrite app_nil_r.
      rewrite /seq_val. rewrite take_drop_commute.
      rewrite -Hlen. rewrite take_app.
      rewrite take_drop //.
  Qed.


  Lemma seq_val_slices As len vs :
    covers As len →
    length vs = len →
    vs = concat (seq_vals As vs).
  Proof.
    intros Hcov ?; eapply seq_val_slices' with (ws := nil) in Hcov; eauto.
    rewrite app_nil_r in Hcov. done.
  Qed.

  Lemma seq_vals_app As Bs vs:
    seq_vals (As ++ Bs) vs = seq_vals As vs ++ seq_vals Bs vs.
  Proof. apply map_app. Qed.

  Lemma seq_vals_cons As i li vs:
    seq_vals ((i, li) :: As) vs = seq_val i li vs :: seq_vals As vs.
  Proof. done. Qed.


  Lemma seq_val_app i li vs ws  :
    val_contains_slice i li vs →
    seq_val i li (vs ++ ws) = seq_val i li vs.
  Proof.
    rewrite /seq_val /val_contains_slice. intros ?. rewrite !take_drop_commute.
    f_equal. rewrite take_app_le //.
  Qed.

  Lemma seq_val_skip i li vs ws :
    length vs = i →
    length ws = li →
    seq_val i li (vs ++ ws) = ws.
  Proof.
    rewrite /seq_val. intros <- <-.
    rewrite drop_app take_ge //.
  Qed.

  Lemma seq_val_sized i li vs ws :
    vs `has_size_val` i →
    ws `has_size_val` li →
    seq_val i li (vs ++ ws) = ws.
  Proof.
    rewrite /has_size_val.
    intros ??; rewrite seq_val_skip //; lia.
  Qed.

  Lemma seq_val_middle i li vs u ws:
    length vs = i →
    length u = li →
    seq_val i li (vs ++ u ++ ws) = u.
  Proof.
    intros Hi Hu.
    rewrite app_assoc seq_val_app; last first.
    { rewrite /val_contains_slice app_length. lia. }
    rewrite seq_val_skip //.
  Qed.


  Lemma has_size_val_app (n m : Z) vs ws :
    vs `has_size_val` n →
    ws `has_size_val` m →
    (0 ≤ n)%Z →
    (0 ≤ m)%Z →
    (vs ++ ws) `has_size_val` (n + m)%Z.
  Proof.
    rewrite /has_size_val. intros Hlen1 Hlen2 ? ?.
    rewrite app_length Hlen1 Hlen2. lia.
  Qed.

  Lemma has_size_val_app_nat (n m: nat) vs ws :
    vs `has_size_val` n →
    ws `has_size_val` m →
    (vs ++ ws) `has_size_val` (n + m)%nat.
  Proof.
    rewrite /has_size_val. intros Hlen1 Hlen2.
    rewrite app_length. lia.
  Qed.


  Definition elements_well_sized (slices: list (nat * nat)) (elements: list (type Σ)) : Prop :=
    Forall2 (λ '(i, li) A, A `has_size` (li: nat))%type slices elements.

  Lemma elements_well_sized_snoc_inv_l slices elements i li :
    elements_well_sized (slices ++ [(i, li)]) (elements) →
    ∃ A elements', elements = elements' ++ [A] ∧ elements_well_sized slices elements' ∧ A `has_size` li.
  Proof.
    rewrite /elements_well_sized. rewrite Forall2_app_inv_l.
    intros (k1 & k2 & Hf1 & Hf2 & Heq).
    eapply Forall2_cons_inv_l in Hf2 as (A & As & ? & Hx & ?). subst.
    eapply Forall2_nil_inv_l in Hx as ->. eauto.
  Qed.


  Definition seqT_size (len: nat) (slices: list (nat * nat)) (elements: list (type Σ)) : Z → Prop :=
    λ n, n = len ∧ elements_well_sized slices elements.


  Lemma seq_vals_app_val Ls vs ws  :
    val_contains_slices Ls vs →
    seq_vals Ls (vs ++ ws) = seq_vals Ls vs.
  Proof.
    induction 1 as [|[i len] As ? _ IH]; simpl; first done.
    rewrite seq_val_app //. rewrite IH //.
  Qed.

  Lemma covers_size_contains_slices Ls len vs :
    covers Ls len →
    vs `has_size_val` len →
    val_contains_slices Ls vs.
  Proof.
    intros Hc Hlen. eapply covers_contains_slice; eauto.
    rewrite /has_size_val in Hlen. lia.
  Qed.


  Lemma slices_move l slices elements (len: nat):
    covers slices len →
    elements_well_sized slices elements →
    ([∗ list] p; A ∈ slices; elements, (l +ₗ (p.1: nat)) ◁ₗ A) ∗ loc_in_bounds l len ⊢
    ∃ vs, l ↦ vs ∗ ⌜vs `has_size_val` len⌝ ∗ ([∗ list] v; A ∈ seq_vals slices vs; elements, v ◁ᵥ A).
  Proof.
    induction 1 as [|i li As Hcov IH] in elements |-*; simpl; intros Hsz.
    - iIntros "[Ht Hl]". iExists []. destruct elements; simpl; last naive_solver.
      rewrite heap_mapsto_nil. iFrame. iPureIntro. done.
    - iIntros "[Hs #Hb]". eapply elements_well_sized_snoc_inv_l in Hsz as (A & elements' & -> & Hsz & HA).
      rewrite big_sepL2_snoc /=. iDestruct "Hs" as "[HAs Hi]"; simpl.
      iPoseProof (IH with "[$HAs Hb]") as "Hs"; first done.
      { iApply loc_in_bounds_shorten; last done. lia. }
      iDestruct "Hs" as "[%v [Hl [%Hv Hs]]]".
      iDestruct (ty_move with "Hi") as "[%vi [Hi Hvi]]"; first eapply HA.
      iDestruct (ty_size_eq with "Hvi") as "%Hvi"; first eapply HA.
      iExists (v ++ vi). rewrite heap_mapsto_app. iFrame.
      rewrite Hv. iFrame.
      iSplit; first by iPureIntro; eapply has_size_val_app_nat; eauto.
      rewrite seq_vals_app /=.
      rewrite big_sepL2_snoc /=. rewrite seq_val_sized //. iFrame.
      rewrite seq_vals_app_val //. by eapply covers_size_contains_slices.
  Qed.

  Lemma val_has_size_add_inv vs (n m: nat):
    vs `has_size_val` (n + m) →
    ∃ vs1 vs2, vs = vs1 ++ vs2 ∧ vs1 `has_size_val` n ∧ vs2 `has_size_val` m.
  Proof.
    rewrite /has_size_val. intros Hlen. exists (take n vs), (drop n vs).
    rewrite take_length drop_length take_drop. split_and!; eauto with lia.
  Qed.

  Lemma shift_loc_sized i l v:
    v `has_size_val` i →
    (l +ₗ length v) = (l +ₗ i).
  Proof.
    rewrite /has_size_val. intros Hlen; destruct l as [p a]; rewrite /has_size_val /shift_loc /=.
    f_equal. lia.
  Qed.


  Lemma slices_move_back vs l (slices: list (nat * nat)) (elements: list (type Σ)) (len: nat):
    covers slices len →
    vs `has_size_val` len  →
    loc_in_bounds l len ∗
    l ↦ vs ∗
    ([∗ list] v; A ∈ seq_vals slices vs; elements, v ◁ᵥ A) ⊢
    [∗ list] p; A ∈ slices; elements, (l +ₗ (p.1: nat)) ◁ₗ A.
  Proof.
    intros Hcov Hlen.
    induction Hcov as [|i sz As Hcov IH] in vs, elements, Hlen |-*; simpl.
    - destruct elements; simpl.
      + iIntros "[_ [_ _]]". done.
      + iIntros "[_ [_ %Hf]]". done.
    - iIntros "[#Hb [Hl Hs]]". iDestruct (big_sepL2_length with "Hs") as "%Hlen2".
      rewrite map_length app_length /= in Hlen2.
      destruct (list_inv_snoc elements) as [->|(A & elements' & ->)]; first by simpl in *; lia.
      rewrite seq_vals_app /=. rewrite !big_sepL2_snoc.
      replace ((i + sz)%nat : Z) with (i + sz)%Z in Hlen by lia.
      eapply val_has_size_add_inv in Hlen as (vs' & v & -> & Hlen & Hv).
      rewrite seq_val_sized //. rewrite heap_mapsto_app.
      rewrite seq_vals_app_val; last first.
      { eapply covers_contains_slice; eauto. rewrite /has_size_val in Hlen. lia. }
      iDestruct "Hl" as "[Hl Hi]".
      iDestruct "Hs" as "[Hs Hv]".
      iDestruct (IH with "[$Hl $Hs]") as "Hs"; first done.
      { iApply loc_in_bounds_shorten; last by iFrame. lia. }
      iFrame; simpl. rewrite (shift_loc_sized i) //.
      iApply (ty_move_back with "Hi Hv").
  Qed.


  Program Definition seqT_def (len: nat) (slices: list (nat * nat)) (elements: list (type Σ)) : type Σ := {|
    ty_has_size := seqT_size len slices elements;
    ty_own l := (
      ⌜covers slices len⌝ ∗ loc_in_bounds l len ∗
      ([∗ list] p; A ∈ slices; elements, (l +ₗ (p.1: nat)) ◁ₗ A)
    )%I;
    ty_own_val vs := (
      ⌜vs `has_size_val` len⌝ ∗ ⌜covers slices len⌝ ∗
      ([∗ list] v; A ∈ seq_vals slices vs; elements, v ◁ᵥ A)
    )%I
    |}.
  Next Obligation.
    intros len slices elements ot v [Heq Hot]; simpl.
    rewrite /has_size_val. iIntros "(% & % & Hv)". iPureIntro. lia.
  Qed.
  Next Obligation.
    intros len slices elements ot l [Heq Hot]; simpl.
    iIntros "(% & #Hb & Hs)".
    iDestruct (slices_move with "[$Hs $Hb]") as "[%v (Hl & Hly & Hs)]"; auto.
    iExists v. iFrame. iPureIntro. done.
  Qed.
  Next Obligation.
    intros len slices elements l v; simpl.
    iIntros "Hl (%Hly & %Hcov & Hs)".
    iSplit; first done. rewrite /has_size_val in Hly. assert (len = length v) as -> by lia.
    iDestruct (heap_mapsto_loc_in_bounds with "Hl") as "#Hb".
    iFrame "Hb". iApply (slices_move_back with "[$Hb $Hl $Hs]"); eauto.
  Qed.

  Definition seqT_aux : seqT_def.(seal).
  Proof. by eexists. Qed.
  Definition seqT := seqT_aux.(unseal).
  Definition seqT_eq : seqT = seqT_def :=
    seal_eq seqT_aux.


  Lemma seqT_has_size len slices elements:
    elements_well_sized slices elements →
    seqT len slices elements `has_size` len.
  Proof.
    intros Hsz. rewrite seqT_eq /seqT_def /=. split; done.
  Qed.


  (* singleton sequences  *)
  Lemma seq_val_zero v len:
    length v = len →
    seq_val 0 len v = v.
  Proof.
    intros Hlen. rewrite -{1}(app_nil_l v).
    rewrite seq_val_skip //.
  Qed.

  Lemma seqT_singleton_vtr v len A:
    A `has_size` len →
    v ◁ᵥ seqT len [(0, len)] [A] ⊣⊢ v ◁ᵥ A.
  Proof.
    rewrite seqT_eq /=. intros Hsz. iSplit.
    - iIntros "(%Hly & % & ? & _)".
      rewrite seq_val_zero; first done.
      revert Hly; rewrite /has_size_val; lia.
    - iIntros "Hv".
      iDestruct (ty_size_eq with "Hv") as %Hsz2; first apply Hsz.
      iSplit; first done.
      iSplit.
      { iPureIntro. eapply (covers_app_singleton_iff nil). split; eauto using covers_nil. }
      rewrite seq_val_zero; first iFrame.
      revert Hsz2; rewrite /has_size_val; lia.
  Qed.

  Lemma seqT_singleton_ltr l len A:
    A `has_size` len →
    l ◁ₗ seqT len [(0, len)] [A] ⊣⊢ l ◁ₗ A.
  Proof.
    intros Hsz. eapply ty_own_ty_val_equiv; eauto.
    - eapply seqT_has_size. rewrite /elements_well_sized.
      rewrite !Forall2_cons Forall2_nil. eauto.
    - intros ?. by eapply seqT_singleton_vtr.
  Qed.

  Lemma seqT_singleton len A:
    A `has_size` len →
    seqT len [(0, len)] [A] ≡ A.
  Proof.
    split; eauto using seqT_singleton_ltr, seqT_singleton_vtr.
  Qed.

  Lemma sepT_loc_in_bounds l len Ls As:
    l ◁ₗ seqT len Ls As ⊢ loc_in_bounds l len.
  Proof.
    rewrite seqT_eq /seqT_def /=.
    iIntros "Hl". iDestruct "Hl" as "(% & #Hb & _)". done.
  Qed.


  (* element borrowing, returning, and type changing *)
  Lemma big_sepL2_middle {A B} (Φ: nat → A → B → iProp Σ) (L1 L2: list A) (R1 R2: list B) l r:
    length L1 = length R1 ∨ length L2 = length R2 →
    ([∗ list] k ↦ a; b ∈ L1 ++ l :: L2; R1 ++ r :: R2, Φ k a b) ⊣⊢
    ([∗ list] k ↦ a; b ∈ L1; R1, Φ k a b) ∗ Φ (length L1) l r ∗ ([∗ list] k ↦ a; b ∈ L2; R2, Φ (S (length L1) + k) a b).
  Proof.
    intros Hlen. iSplit.
    - iIntros "Hs". rewrite big_sepL2_app_inv; last first.
      { simpl. destruct Hlen as [-> | ->]; eauto. }
      iDestruct "Hs" as "(Hs1 & Hs2 & Hs3)".
      rewrite Nat.add_0_r. setoid_rewrite Nat.add_succ_r. iFrame.
    - iIntros "(Hs1 & Hs2 & Hs3)". iApply (big_sepL2_app with "Hs1").
      rewrite /= Nat.add_0_r. setoid_rewrite Nat.add_succ_r. iFrame.
  Qed.

  (* borrowing sequence elements *)
  Lemma seq_borrow_element l len i li Ls Ls' A As As' :
    length As = length Ls ∨ length Ls' = length As' →
    l ◁ₗ seqT len (Ls ++ (i, li) :: Ls') (As ++ A :: As') ⊢
    l ◁ₗ seqT len (Ls ++ (i, li) :: Ls') (As ++ placeT (l +ₗ i) :: As') ∗ (l +ₗ i) ◁ₗ A.
  Proof.
    intros Hlen. rewrite seqT_eq /seqT_def /=. iIntros "Hl".
    iDestruct "Hl" as "(%Hcov & #Hb & Hs)". rewrite -bi.sep_assoc.
    iSplit; first by iPureIntro. iFrame "Hb".
    rewrite big_sepL2_middle /=; last by naive_solver.
    iDestruct "Hs" as "(Hs1 & Hs2 & Hs3)". iFrame.
    iApply placeT_create.
  Qed.

  Lemma list_middle_inv {A B} (l1 l2: list A) l (rs: list B) :
    length rs = length (l1 ++ l :: l2) →
    ∃ r1 r2 r, rs = r1 ++ r :: r2 ∧ length l1 = length r1 ∧ length l2 = length r2.
  Proof.
    rewrite app_length /=. intros Hlen.
    destruct (rs !! (length l1)) as [r|] eqn: Heq; last first.
    { eapply lookup_ge_None_1 in Heq. lia. }
    exists (take (length l1) rs), (drop (S (length l1)) rs), r.
    rewrite take_length drop_length. split_and!; eauto with lia.
    eapply take_drop_middle in Heq. done.
  Qed.

  Lemma seq_return_element l r len Ls A As As' :
    l ◁ₗ seqT len Ls (As ++ placeT r :: As') ∗ r ◁ₗ A ⊢
    l ◁ₗ seqT len Ls (As ++ A :: As').
  Proof.
    rewrite seqT_eq /seqT_def /=. iIntros "[Hl Hr]".
    iDestruct "Hl" as "(%Hcov & #Hb & Hs)".
    iSplit; first by iPureIntro. iFrame "Hb".
    iDestruct (big_sepL2_length with "Hs") as "%Hlen".
    eapply list_middle_inv in Hlen as (Ls1 & Ls2 & p & -> & Hlen1 & Hlen2).
    rewrite big_sepL2_middle /=; last by naive_solver.
    iDestruct "Hs" as "(Has & Ha & Hbs)"; simpl. iFrame.
    iApply (placeT_merge with "[$Ha $Hr]").
  Qed.

  Lemma sliced_compat_le len Ls A As B Bs :
    A ⊑ B →
    seqT len Ls (As ++ A :: Bs) ⊑ seqT len Ls (As ++ B :: Bs).
  Proof.
    intros Hsub. split; rewrite seqT_eq /seqT_def /=.
    - intros l. iIntros "(%Hcov & $ & Hs)". iSplit; first done.
      iDestruct (big_sepL2_length with "Hs") as "%Hlen".
      eapply list_middle_inv in Hlen as (Ls1 & Ls2 & p & -> & Hlen1 & Hlen2).
      rewrite !big_sepL2_middle /=; try by naive_solver.
      iDestruct "Hs" as "(Has & Ha & Hbs)"; simpl. iFrame. by rewrite Hsub.
    - intros v. iIntros "(%Hsz & %Hcov & Hs)". iSplit; first done. iSplit; first done.
      iDestruct (big_sepL2_length with "Hs") as "%Hlen".
      eapply list_middle_inv in Hlen as (Ls1 & Ls2 & p & -> & Hlen1 & Hlen2).
      rewrite !big_sepL2_middle /=; try by naive_solver.
      iDestruct "Hs" as "(Has & Ha & Hbs)"; simpl. iFrame. by rewrite Hsub.
  Qed.

  Lemma sliced_compat_equiv len Ls A As B Bs :
    A ≡ B →
    seqT len Ls (As ++ A :: Bs) ≡ seqT len Ls (As ++ B :: Bs).
  Proof.
    intros Heq. eapply type_equiv_antisym; eapply sliced_compat_le; rewrite Heq //.
  Qed.

  Lemma seqT_covers x Ls As len :
    x ◁ₓ seqT len Ls As ⊢ ⌜covers Ls len⌝.
  Proof.
    destruct x; rewrite seqT_eq /=.
    - iIntros "(%Hcov & #Hb & Hs)". done.
    - iIntros "(%Hly & %Hcov & Hs)". done.
  Qed.

  Lemma seq_val_nested (i j li lj: nat) v:
    (i + j + lj ≤ i + li) →
    seq_val j lj (seq_val i li v) = seq_val (i + j) lj v.
  Proof.
    intros Hle. rewrite /seq_val. rewrite !take_drop_commute.
    rewrite drop_drop. rewrite take_take.
    f_equal. f_equal. lia.
  Qed.

  Lemma seq_vals_seq_val Ls i li vs:
    covers Ls li →
    seq_vals Ls (seq_val i li vs) = seq_vals (shift_slices i Ls) vs.
  Proof.
    rewrite /seq_vals /shift_slices map_map. intros Hcov.
    eapply map_ext_Forall, Forall_lookup_2.
    intros k [j lj] Hlook; simpl. rewrite seq_val_nested //.
    eapply covers_contains in Hcov. eapply Forall_lookup_1 in Hcov; last done.
    simpl in Hcov. lia.
  Qed.


  Lemma shift_slices_nested i j As :
    shift_slices i (shift_slices j As) = shift_slices (i + j) As.
  Proof.
    rewrite /shift_slices map_map. eapply map_ext.
    intros [k lk]; simpl. f_equal. lia.
  Qed.

  Lemma covers_lookup_range Ls (li k j lj: nat) :
    covers Ls li →
    Ls !! k = Some (j, lj) →
    j + lj ≤ li.
  Proof.
    intros Hcov Hlook. eapply covers_contains in Hcov.
    eapply Forall_lookup_1 in Hcov; last done.
    simpl in Hcov. lia.
  Qed.

  Lemma covers_inline Ls Ls' Ls'' i li len:
    covers Ls' li →
    covers (Ls ++ (i, li) :: Ls'') len →
    covers (Ls ++ shift_slices i Ls' ++ Ls'') len.
  Proof.
    rewrite cons_middle app_assoc. intros Hcov Hcov'.
    eapply covers_app_shift_inv in Hcov' as (l1 & l2 & Ls''' & Hcov2 & Heq & Hcov3 & Hlen).
    eapply covers_app_singleton_iff in Hcov2 as [Hcov2 ->]. subst.
    rewrite app_assoc. eauto using covers_app_shift.
  Qed.


  Lemma sliced_flatten As len i li Ls Ls' Ls'' Bs Cs :
    length As = length Ls ∨ length Cs = length Ls'' →
    seqT len (Ls ++ (i, li) :: Ls'') (As ++ (seqT li Ls' Bs) :: Cs) ⊑
    seqT len (Ls ++ shift_slices i Ls' ++ Ls'') (As ++ Bs ++ Cs).
  Proof.
    intros Hlen; split.
    - intros l. rewrite {1 3}seqT_eq /=.
      iIntros "(%Hcov & #Hb & Hs)". iFrame "Hb".
      rewrite big_sepL2_app_inv; last by simpl; naive_solver.
      iDestruct "Hs" as "[$ [Hi $]]"; simpl.
      iDestruct (seqT_covers (LOC (l +ₗ i)) with "Hi") as %Hcov'.
      iSplit; first by iPureIntro; eapply covers_inline.
      rewrite seqT_eq /=. iDestruct "Hi" as "( _ & _ & ?)".
      rewrite /shift_slices. rewrite big_sepL2_fmap_l. iApply (big_sepL2_mono with "[$]").
      iIntros (k [j lj] B ? ?) "H"; simpl. rewrite shift_loc_assoc.
      by replace ((i + j)%Z : Z) with ((i + j) : Z) by lia.
    - intros v. rewrite {1 3}seqT_eq /=.
      iIntros "(%Hsz & %Hcov & Hs)".
      iSplit; first done. rewrite !seq_vals_app !seq_vals_cons.
      rewrite big_sepL2_app_inv; last by rewrite /= /seq_vals !map_length; naive_solver.
      iDestruct "Hs" as "[? [Hi ?]]"; simpl. iFrame.
      iDestruct (seqT_covers (VAL _) with "Hi") as %Hcov'.
      iSplit; first by iPureIntro; eapply covers_inline.
      rewrite seqT_eq /=. iDestruct "Hi" as "( _ & _ & ?)".
      rewrite seq_vals_seq_val //.
  Qed.


  Lemma seq_has_bounds n Ls As:
    seqT n Ls As `has_bounds` n.
  Proof.
    rewrite /ty_has_bounds. iIntros (l) "Hl".
    replace (Z.to_nat n) with n by lia.
    iDestruct (sepT_loc_in_bounds with "Hl") as "$".
    iPureIntro. lia.
  Qed.


  Lemma shift_slices_zero As: shift_slices 0 As = As.
  Proof.
    induction As as [|[i li] As IH]; simpl; eauto with congruence.
  Qed.


  Fixpoint slices_length (Ls: list (nat * nat)) : nat :=
    match Ls with
    | [] => 0
    | [(i, li)] => i + li
    | _ :: xr => slices_length xr
    end.

  Lemma slices_length_last Ls i li:
    slices_length (Ls ++ [(i, li)]) = (i + li).
  Proof.
    induction Ls as [|[j lj] Ls IH] in i, li |-*; simpl; eauto.
    rewrite IH. destruct Ls; simpl; done.
  Qed.

  Lemma covers_slices_length Ls len:
    covers Ls len → len = slices_length Ls.
  Proof.
    destruct (list_inv_snoc Ls) as [|([i li] & Ls' & ->)].
    - subst. rewrite covers_nil_iff /=. lia.
    - rewrite covers_app_singleton_iff. intros [??]. subst.
      rewrite slices_length_last. done.
  Qed.

  Lemma shift_slices_injective_slices i Ls1 Ls2 :
    shift_slices i Ls1 = shift_slices i Ls2 → Ls1 = Ls2.
  Proof.
    induction Ls1 as [|[j lj] Ls1 IH] in Ls2 |-*; destruct Ls2 as [|[k lk]]; simpl; eauto; try congruence.
    intros ?. simplify_eq. assert (j = k) as -> by lia.
    erewrite IH; eauto.
  Qed.

  Lemma slices_length_covers_shift Ls Ls' Ls'' i li len :
    covers (Ls ++ shift_slices i Ls' ++ Ls'') len →
    li = slices_length Ls' →
    i = slices_length Ls →
    covers Ls' li.
  Proof.
    intros Hcov ??. subst. rewrite app_assoc in Hcov.
    eapply covers_app_iff in Hcov as (l1 & l2 & Ls2' & Hcov & Hshft & _ & Hlen). subst.
    eapply covers_app_iff in Hcov as (l3 & l4 & Ls3' & Hcov & Hshft' & Hcov2 & Hlen'). subst.
    destruct (list_inv_snoc Ls) as [|([j lj] & Ls'' & ?)]; subst.
    - eapply covers_nil_iff in Hcov. subst.
      simpl in Hshft'. rewrite !shift_slices_zero in Hshft'. subst.
      eapply covers_slices_length in Hcov2 as Heq. subst. done.
    - eapply covers_app_singleton_iff in Hcov as [? ?]. subst.
      rewrite slices_length_last in Hshft'.
      eapply shift_slices_injective_slices in Hshft'. subst.
      eapply covers_slices_length in Hcov2 as Heq. subst. done.
  Qed.


  Lemma covers_nest i li Ls Ls' Ls'' len:
    i = slices_length Ls →
    li = slices_length Ls' →
    covers (Ls ++ shift_slices i Ls' ++ Ls'') len →
    covers (Ls ++ (i, li) :: Ls'') len.
  Proof.
    rewrite cons_middle !app_assoc. intros Hcov Hlen Hcov'. subst.
    eapply covers_app_shift_inv in Hcov' as (l1 & l2 & Ls2' & Hcov1 & Hshft1 & Hcov2 & ->); subst.
    eapply covers_app_shift; last done.
    eapply covers_app_shift_inv in Hcov1 as (l3 & l4 & Ls3' & Hcov3 & Hshft2 & Hcov4 & ->); subst.
    eapply covers_app_singleton_iff.
    eapply covers_slices_length in Hcov3 as Heq. subst.
    eapply covers_slices_length in Hcov4 as Heq. subst.
    eapply shift_slices_injective_slices in Hshft2; subst. done.
  Qed.


  Lemma seq_val_has_size i li v:
    val_contains_slice i li v →
    seq_val i li v `has_size_val` li.
  Proof.
    rewrite /seq_val /has_size_val /val_contains_slice.
    intros ?. rewrite take_length drop_length. lia.
  Qed.

  Lemma val_contains_slice_has_size v i li len:
    v `has_size_val` len →
    i + li ≤ len →
    val_contains_slice i li v.
  Proof.
    rewrite /has_size_val /val_contains_slice. lia.
  Qed.


  Lemma slices_nest_length i li Ls Ls' Ls'' len:
    i = slices_length Ls →
    li = slices_length Ls' →
    covers (Ls ++ shift_slices i Ls' ++ Ls'') len →
    i + li ≤ len.
  Proof.
    rewrite app_assoc. intros Hcov Hlen Hcov'. subst.
    eapply covers_app_shift_inv in Hcov' as (l1 & l2 & Ls2' & Hcov1 & Hshft1 & Hcov2 & ->); subst.
    eapply covers_app_shift_inv in Hcov1 as (l3 & l4 & Ls3' & Hcov3 & Hshft2 & Hcov4 & ->); subst.
    eapply covers_slices_length in Hcov3 as Heq. subst.
    eapply covers_slices_length in Hcov4 as Heq. subst.
    eapply shift_slices_injective_slices in Hshft2; subst.
    lia.
  Qed.


  Lemma seqT_nest As len i li Ls Ls' Ls'' Bs Cs :
    length As = length Ls →
    length Cs = length Ls'' →
    i = slices_length Ls →
    li = slices_length Ls' →
    seqT len (Ls ++ shift_slices i Ls' ++ Ls'') (As ++ Bs ++ Cs) ⊑
    seqT len (Ls ++ (i, li) :: Ls'') (As ++ (seqT li Ls' Bs) :: Cs).
  Proof.
    intros Hne Hlen. rewrite cons_middle. split.
    - iIntros (l) "Hl". rewrite {1 2}seqT_eq /seqT_def /=.
      iDestruct "Hl" as "(%Hcov' & #Hb & Hs)". iFrame "Hb".
      rewrite big_sepL2_app_inv; last by naive_solver.
      rewrite big_sepL2_app_inv; last by naive_solver.
      iDestruct "Hs" as "[$ [Hi $]]"; simpl.
      iSplit; first by iPureIntro; eapply covers_nest; eauto.
      rewrite seqT_eq /seqT_def /=. iSplit; first by iPureIntro; eapply slices_length_covers_shift.
      eapply slices_nest_length in Hcov' as Hle; eauto.
      iSplit. { iApply loc_in_bounds_shift_loc; last by iFrame. lia. }
      rewrite /shift_slices big_sepL2_fmap_l. iApply (big_sepL2_mono with "Hi").
      iIntros (?[]???) "H"; simpl. rewrite shift_loc_assoc.
      replace (i + n)%Z with ((i + n)%nat : Z) by lia. by iFrame.
    - iIntros (v) "Hv". rewrite {1 2}seqT_eq /seqT_def /=.
      iDestruct "Hv" as "(%Hsz & %Hcov' & Hs)". iSplit; first done.
      rewrite !seq_vals_app.
      rewrite big_sepL2_app_inv; last by rewrite /seq_vals map_length; naive_solver.
      rewrite big_sepL2_app_inv; last by rewrite /seq_vals /shift_slices !map_length; naive_solver.
      iDestruct "Hs" as "[$ [Hi $]]"; simpl.
      iSplit; first by iPureIntro; eapply covers_nest; eauto.
      eapply slices_nest_length in Hcov' as Hle; eauto.
      rewrite seqT_eq /seqT_def /=.
      iSplit. { iPureIntro. eapply seq_val_has_size, val_contains_slice_has_size; eauto. }
      iSplit; first by iPureIntro; eapply slices_length_covers_shift.
      rewrite seq_vals_seq_val //.
      by eapply slices_length_covers_shift.
  Qed.


  Lemma seqT_subtype_covers len Ls As B:
    (covers Ls len → seqT len Ls As ⊑ B) →
    seqT len Ls As ⊑ B.
  Proof.
    intros Hsub. eapply type_le_xtr_intro. intros x.
    iIntros "Hx". iDestruct (seqT_covers with "Hx") as "%Hcov".
    specialize (Hsub Hcov). destruct x; simpl; rewrite Hsub //.
  Qed.

  Lemma covers_cons_zero i li Ls len:
    covers ((i, li) :: Ls) len → i = 0.
  Proof.
    rewrite (covers_middle_iff nil); simpl.
    setoid_rewrite covers_nil_iff. naive_solver.
  Qed.

  Lemma seqT_nest' As len i li Ls Ls' Ls'' Bs Cs :
    length As = length Ls →
    length Cs = length Ls'' →
    Ls' ≠ nil →
    covers Ls' li →
    seqT len (Ls ++ shift_slices i Ls' ++ Ls'') (As ++ Bs ++ Cs) ⊑
    seqT len (Ls ++ (i, li) :: Ls'') (As ++ (seqT li Ls' Bs) :: Cs).
  Proof.
    intros Heq1 Heq2 Hsplit Heq3. eapply seqT_subtype_covers. intros Hcov.
    rewrite app_assoc in Hcov.
    eapply covers_app_shift_inv in Hcov as (l1 & l2 & Ls2' & Hcov1 & Hshft1 & Hcov2 & ->); subst.
    eapply covers_app_shift_inv in Hcov1 as (l3 & l4 & Ls3' & Hcov3 & Hshft2 & Hcov4 & ->); subst.
    eapply covers_slices_length in Hcov3 as Heq. subst.
    eapply covers_slices_length in Hcov4 as Heq. subst.
    enough (i = slices_length Ls) as -> by eauto using seqT_nest, covers_slices_length.
    destruct Ls' as [|[j lj] ?]; first naive_solver.
    destruct Ls3' as [|[k lk] ?]; first naive_solver.
    simpl in Hshft2. simplify_eq.
    eapply covers_cons_zero in Heq3.
    eapply covers_cons_zero in Hcov4. subst. lia.
  Qed.

  Lemma covers_singleton i j:
    covers [(0, i)] j ↔ i = j.
  Proof.
    rewrite (covers_app_singleton_iff nil).
    rewrite covers_nil_iff. lia.
  Qed.


  Lemma covers_pair i j:
    covers [(0, i); (i, j)] (i + j).
  Proof.
    rewrite (covers_app_singleton_iff [(0, i)]).
    rewrite covers_singleton //.
  Qed.


  Lemma sliced_split_slice (i li lj len: nat) A A1 A2 Ls Ls' As Bs:
    length As = length Ls →
    length Bs = length Ls' →
    A ≡ seqT (li + lj) [(0, li); (li, lj)] [A1; A2] →
    seqT len (Ls ++ (i, (li + lj)%nat) :: Ls') (As ++ A :: Bs) ≡
    seqT len (Ls ++ (i, li) ::((i + li)%nat, lj) :: Ls') (As ++ A1 :: A2 :: Bs).
  Proof.
    intros Hlen1 Hlen2 Heq. simpl. etrans; first by eapply sliced_compat_equiv.
    eapply type_equiv_antisym.
    - rewrite sliced_flatten //=; last by naive_solver. rewrite Nat.add_0_r //.
    - etrans; last eapply seqT_nest'; eauto using covers_pair.
      rewrite /= Nat.add_0_r //.
  Qed.


  Lemma loc_in_bounds_has_size_val l v (k: nat):
    v `has_size_val` k →
    loc_in_bounds l (length v) ⊢ loc_in_bounds l k.
  Proof.
    intros Hsz. iIntros "Hl". iApply loc_in_bounds_shorten; [|done].
    rewrite /has_size_val in Hsz. lia.
  Qed.


  Lemma has_size_val_add_split v (i j: nat):
    v `has_size_val` (i + j) →
    ∃ vs us, v = vs ++ us ∧ vs `has_size_val` i ∧ us `has_size_val` j.
  Proof.
    intros Hsz. exists (take i v), (drop i v).
    rewrite take_drop //. split; first done.
    revert Hsz. rewrite /has_size_val.
    rewrite take_length drop_length. lia.
  Qed.


  Lemma vtr_anyT_split_into_slices v (i j : nat):
    v ◁ᵥ anyT (i + j) ⊣⊢ v ◁ᵥ seqT (i + j) [(0, i); (i, j)] [anyT i; anyT j].
  Proof.
    rewrite seqT_eq /seqT_def /=. iSplit.
    - iIntros "Hv". iDestruct (ty_size_eq with "Hv") as "%Hsz"; first by apply (anyT_has_size_iff _ (i+j)%nat); lia.
      iSplit; first done. iSplit; first by iPureIntro; eapply covers_pair.
      rewrite right_id. iSplit.
      + iApply vtr_anyT_intro. iPureIntro.
        eapply seq_val_has_size, val_contains_slice_has_size; eauto with lia.
      + iApply vtr_anyT_intro. iPureIntro.
        eapply seq_val_has_size, val_contains_slice_has_size; eauto with lia.
    - iIntros "[%Hsz _]". iApply vtr_anyT_intro. iPureIntro.
      revert Hsz. rewrite /has_size_val. lia.
  Qed.


  Lemma ltr_anyT_split_into_slices l (i j : nat) :
    l ◁ₗ anyT (i + j) ⊣⊢ l ◁ₗ seqT (i + j) [(0, i); (i, j)] [anyT i; anyT j].
  Proof.
    eapply ty_own_ty_val_equiv; last by eauto using vtr_anyT_split_into_slices.
    { eapply anyT_has_size_iff. done. }
    eapply seqT_has_size. rewrite /elements_well_sized !Forall2_cons Forall2_nil.
    split_and!; eauto using anyT_has_size.
  Qed.

  Lemma anyT_split_into_sliced (i j : nat):
    anyT (i + j) ≡ seqT (i + j) [(0, i); (i, j)] [anyT i; anyT j].
  Proof.
    split; eauto using ltr_anyT_split_into_slices, vtr_anyT_split_into_slices.
  Qed.

  Lemma covers_empty_slice Ls Ls' i len :
    covers (Ls ++ (i, 0) :: Ls') len → covers (Ls ++ Ls') len.
  Proof.
    rewrite cons_middle app_assoc. rewrite covers_app_iff.
    intros (lA & lB & Ls'' & Hcov & -> & Hcov2 & ->).
    eapply covers_app_iff. exists lA, lB, Ls''. split_and!; eauto.
    eapply covers_app_singleton_iff in Hcov. destruct Hcov as [? ?].
    assert (lA = i) by lia. subst i. done.
  Qed.

  Lemma seqT_empty_slice  len i Ls Ls' A As Bs:
    length As = length Ls →
    length Bs = length Ls' →
    seqT len (Ls ++ (i, 0) :: Ls') (As ++ A :: Bs)
    ⊑ seqT len (Ls ++ Ls') (As ++ Bs).
  Proof.
    rewrite !cons_middle. intros Hlen1 Hlen2.
    split.
    - iIntros (l) "Hl". rewrite seqT_eq /seqT_def /=.
      iDestruct "Hl" as "(%Hcov & $ & Hs)". iSplit.
      { iPureIntro; by eapply covers_empty_slice. }
      rewrite big_sepL2_app_inv /=; last naive_solver.
      iDestruct "Hs" as "($ & _ & $)".
    - iIntros (v) "Hv". rewrite seqT_eq /seqT_def /=.
      iDestruct "Hv" as "($ & %Hcov & Hs)". iSplit.
      { iPureIntro; by eapply covers_empty_slice. }
      rewrite seq_vals_app seq_vals_cons /=.
      rewrite big_sepL2_app_inv /=; last by rewrite /seq_vals !map_length; naive_solver.
      iDestruct "Hs" as "(? & _ & ?)". rewrite seq_vals_app. iFrame.
  Qed.


  Lemma seqT_singleton_slice len Ls As:
    seqT len Ls As ≡ seqT len [(0, len)] [seqT len Ls As].
  Proof.
    eapply type_equiv_antisym.
    - eapply seqT_subtype_covers=>Hcov.
      feed pose proof (seqT_nest nil len 0 len nil Ls nil As nil) as Hsq; [try done ..|].
      { by eapply covers_slices_length. }
      rewrite !right_id shift_slices_zero /= in Hsq. done.
    - feed pose proof (sliced_flatten nil len 0 len nil Ls nil As nil) as Hsq; [try naive_solver ..|].
      rewrite !right_id shift_slices_zero /= in Hsq. done.
  Qed.


  Lemma seqT_empty_is_any_zero_vals v:
    v ◁ᵥ seqT 0 [] [] ⊣⊢ v ◁ᵥ anyT 0.
  Proof.
    rewrite seqT_eq /seqT_def anyT_eq /anyT_def existsT_eq /existsT_def valueT_eq /valueT_def /=.
    iSplit.
    - iIntros "(%Hv & _)". iExists _. iSplit; first done.
      by iPureIntro.
    - iIntros "(%x & %Hv & %Hx)". subst. iSplit; first done.
      iPureIntro. split; last done. by eapply covers_nil_iff.
  Qed.

  Lemma seqT_empty_is_any_zero :
    seqT 0 [] [] ≡ anyT 0.
  Proof.
    eapply type_equiv_xtr_intro; intros [l|v]; simpl.
    - eapply ty_own_ty_val_equiv; eauto using seqT_empty_is_any_zero_vals.
      + apply seqT_has_size. rewrite /elements_well_sized. done.
      + apply anyT_has_size.
    - eapply  seqT_empty_is_any_zero_vals.
  Qed.

  Lemma seqT_split Ls Ls' i j As Bs:
    length As = length Ls →
    length Bs = length Ls' →
    i = slices_length Ls →
    j = slices_length Ls' →
    seqT (i + j) (Ls ++ shift_slices i Ls') (As ++ Bs) ≡
    seqT (i + j) [(0, i); (i, j)] [seqT i Ls As; seqT j Ls' Bs].
  Proof.
    intros Hlen1 Hlen2 Hi Hj. eapply type_equiv_antisym.
    - etrans.
      + feed pose proof (seqT_nest As (i + j) i j Ls Ls' nil Bs nil) as Hsq; [try done ..|].
        rewrite !right_id in Hsq. eapply Hsq.
      + feed pose proof (seqT_nest nil (i + j) 0 i nil Ls [(i, j)] As [seqT j Ls' Bs]) as Hsq; [try done ..|].
        rewrite shift_slices_zero /= in Hsq. done.
    - etrans; last first.
      + feed pose proof (sliced_flatten As (i + j) i j  Ls Ls' nil Bs nil) as Hsq; [try naive_solver ..|].
        rewrite !right_id in Hsq. eapply Hsq.
      + feed pose proof (sliced_flatten nil (i + j) 0 i nil Ls [(i, j)] As [seqT j Ls' Bs]) as Hsq; [try naive_solver ..|].
        rewrite shift_slices_zero /= in Hsq. done.
  Qed.


  Lemma seqT_le_app len Ls As Bs Cs:
    Forall2 (⊑) As Bs →
    seqT len Ls (Cs ++ As) ⊑ seqT len Ls (Cs ++ Bs).
  Proof.
    induction 1 as [| A B As Bs Hsub _ IH] in Cs |-*; simpl; eauto.
    rewrite sliced_compat_le //.
    rewrite cons_middle app_assoc. rewrite IH -app_assoc //.
  Qed.

  Global Instance seqT_le : Proper ((=) ==> (=) ==> Forall2 (⊑) ==> (⊑)) seqT.
  Proof.
    intros len len' <- Ls Ls' <- As As' HAs.
    by eapply (seqT_le_app len Ls As As' nil).
  Qed.

  Global Instance seq_proper : Proper ((=) ==> (=) ==> Forall2 (≡) ==> (≡)) seqT.
  Proof. move => ??-> ?? -> ?? Heq. apply type_le_equiv_list; [by apply seqT_le|done]. Qed.

  Lemma seqT_app l i j Ls Ls' As Bs:
    length As = length Ls →
    length Bs = length Ls' →
    i = slices_length Ls →
    j = slices_length Ls' →
    l ◁ₗ seqT (i + j) (Ls ++ shift_slices i Ls') (As ++ Bs) ⊣⊢ l ◁ₗ seqT i Ls As ∗ (l +ₗ i) ◁ₗ seqT j Ls' Bs.
  Proof.
    intros ????. rewrite seqT_split //. iSplit.
    - pose proof (seq_borrow_element l (i + j) 0 i nil [(i, j)] (seqT i Ls As) nil [(seqT j Ls' Bs)]) as Hbor; simpl in Hbor.
      rewrite Hbor; last lia. rewrite shift_loc_0.
      pose proof (seq_borrow_element l (i + j) i j [(0, i)] nil (seqT j Ls' Bs) [placeT l] nil) as Hbor2; simpl in Hbor2.
      rewrite Hbor2; last lia. iIntros "([_ ?] & ?)". iFrame.
    - iIntros "[Hl1 Hl2]". rewrite {3}seqT_eq /seqT_def /=.
      iSplit; first by iPureIntro; eapply covers_pair. rewrite shift_loc_0.
      iDestruct (sepT_loc_in_bounds with "Hl1") as "#Hb1".
      iDestruct (sepT_loc_in_bounds with "Hl2") as "#Hb2".
      iFrame. rewrite -loc_in_bounds_split. iFrame "Hb1 Hb2".
  Qed.


  Lemma seqT_mono lv len LS tys tys' :
    lv ◁ₓ seqT len LS tys -∗
    ([∗ list] ty;ty'∈tys;tys', ∀ lv', ⌜lov_type lv = lov_type lv'⌝ -∗ lv' ◁ₓ ty -∗ lv' ◁ₓ ty') -∗
    lv ◁ₓ seqT len LS tys'.
  Proof.
    iIntros "Hseq Hwand".
    destruct lv => /=.
    all: rewrite seqT_eq/seqT_def/=; iDestruct "Hseq" as "[$ [$ Htys]]".
    - iInduction (LS) as [] "IH" forall (tys tys').
      { iDestruct (big_sepL2_nil_inv_l with "Htys") as %->.
        by iDestruct (big_sepL2_nil_inv_l with "Hwand") as %->. }
      iDestruct (big_sepL2_cons_inv_l with "Htys") as (?? ->) "[Hw ?]".
      iDestruct (big_sepL2_cons_inv_l with "Hwand") as (?? ->) "[Hty ?]". simpl.
      iDestruct ("Hty" $! (LOC _) with "[//] Hw") as "$". iApply ("IH" with "[$] [$]").
    - iInduction (seq_vals LS v) as [] "IH" forall (tys tys').
      { iDestruct (big_sepL2_nil_inv_l with "Htys") as %->.
        by iDestruct (big_sepL2_nil_inv_l with "Hwand") as %->. }
      iDestruct (big_sepL2_cons_inv_l with "Htys") as (?? ->) "[Hw ?]".
      iDestruct (big_sepL2_cons_inv_l with "Hwand") as (?? ->) "[Hty ?]". simpl.
      iDestruct ("Hty" $! (VAL _) with "[//] Hw") as "$". iApply ("IH" with "[$] [$]").
  Qed.


  Global Instance seqT_copy n Ls As:
    TCForall Copy As →
    Copy (seqT n Ls As).
  Proof.
    rewrite TCForall_Forall. intros Hcp. split. rewrite /Persistent.
    iIntros (v) "Hv". rewrite seqT_eq /seqT_def /=.
    iDestruct "Hv" as "(%Hsz & %Hcov & Hall)".
    iDestruct (big_sepL2_impl _ (λ _ v A, □ (v ◁ᵥ A))%I with "Hall") as "Hw".
    iDestruct ("Hw" with "[]") as "#Hw"; last first.
    - iModIntro. iSplit; first done. iSplit; first done.
      iApply big_sepL2_impl; first done.
      iModIntro. iIntros (k x y) " _ _ #$".
    - iModIntro. iIntros (k x y) "%Hidx %Hty".
      eapply Forall_lookup_1 in Hcp; last done.
      iIntros "#$".
  Qed.

End seq.



Section type_array.
  Context `{!refinedcG Σ}.

  Open Scope nat.

  Definition type_arrayT_slices len cnt : list (nat * nat) :=
    (map (λ i, ((i * len), len)) (seq 0 cnt)).

  Definition type_arrayT_def (len : nat) (As : list (type Σ)) : type Σ :=
    seqT (length As * len) (type_arrayT_slices len (length As)) As.

  Definition type_arrayT_aux : seal (@type_arrayT_def). Proof. by eexists. Qed.
  Definition type_arrayT := type_arrayT_aux.(unseal).
  Definition type_arrayT_eq : @type_arrayT = @type_arrayT_def := type_arrayT_aux.(seal_eq).


  Lemma covers_type_arrayT_slices len cnt :
    covers (type_arrayT_slices len cnt) (cnt * len).
  Proof.
    rewrite /type_arrayT_slices. induction cnt in len |-*.
    - rewrite Nat.mul_0_l. eapply covers_nil.
    - replace (S cnt) with (cnt + 1)%nat by lia.
      rewrite seq_app /= map_app /=. eapply covers_app_singleton_iff.
      split; first done. lia.
  Qed.

  Lemma type_arrayT_seqT_empty n:
    type_arrayT n nil ≡ seqT 0 nil nil.
  Proof.
    rewrite type_arrayT_eq /type_arrayT_def /=.
    rewrite /type_arrayT_slices //=.
  Qed.

  Lemma type_arrayT_singleton_seqT len A:
    type_arrayT len [A] ≡ seqT len [(0, len)] [A].
  Proof.
    rewrite type_arrayT_eq /type_arrayT_def /=.
    rewrite Nat.add_0_r /type_arrayT_slices //=.
  Qed.

  Lemma sliced_singleton_type_array len Ls Ls' As A i li Bs:
    length As = length Ls →
    length Bs = length Ls' →
    seqT len (Ls ++ (i, li) :: Ls') (As ++ type_arrayT li [A] :: Bs) ≡
    seqT len (Ls ++ (i, li) :: Ls') (As ++ A :: Bs).
  Proof.
    intros Hlen1 Hlen2.
    etrans; first by eapply sliced_compat_equiv, type_arrayT_singleton_seqT.
    eapply type_equiv_antisym.
    - etrans; first eapply sliced_flatten. { by naive_solver. }
      by rewrite /= Nat.add_0_r.
    - etrans; last eapply seqT_nest'; simpl; eauto.
      + by rewrite /= Nat.add_0_r.
      + by eapply covers_singleton.
  Qed.

  Lemma type_array_slices_add len i j:
    type_arrayT_slices len (i + j) = type_arrayT_slices len i ++ shift_slices (i * len) (type_arrayT_slices len j).
  Proof.
    rewrite /type_arrayT_slices /shift_slices.
    rewrite seq_app Nat.add_comm -fmap_add_seq.
    rewrite !map_app. f_equal. rewrite !map_map.
    eapply map_ext. intros ?. f_equal. lia.
  Qed.

  Lemma type_array_slices_length len i:
    length (type_arrayT_slices len i) = i.
  Proof.
    rewrite /type_arrayT_slices. rewrite map_length seq_length //.
  Qed.

  Lemma type_array_slices_slices_length len i:
    slices_length (type_arrayT_slices len i) = (i * len)%nat.
  Proof.
    destruct i as [|i]; first done.
    replace (S i) with (i + 1)%nat at 1 by lia.
    rewrite type_array_slices_add /=. rewrite Nat.add_0_r.
    rewrite slices_length_last. lia.
  Qed.

  Lemma type_arrayT_split len As Bs:
    type_arrayT len (As ++ Bs) ≡ seqT ((length As + length Bs) * len)%nat [(0, (length As * len)%nat); (length As * len, length Bs * len)%nat] [type_arrayT len As; type_arrayT len Bs].
  Proof.
    rewrite type_arrayT_eq /type_arrayT_def /=. rewrite app_length Nat.mul_add_distr_r.
    rewrite type_array_slices_add.
    rewrite seqT_split //; rewrite ?type_array_slices_length ?type_array_slices_slices_length; lia...
  Qed.

  Lemma type_arrayT_to_singleton_slice len As:
    type_arrayT len As ≡ seqT (length As * len) [(0, (length As * len))] [type_arrayT len As].
  Proof.
    rewrite type_arrayT_eq /type_arrayT_def. rewrite {1}seqT_singleton_slice //.
  Qed.

  Global Instance type_array_le : Proper ((=) ==> Forall2 (⊑) ==> (⊑)) type_arrayT.
  Proof.
    rewrite type_arrayT_eq /type_arrayT_def. intros ????? Hsub. subst. by erewrite Hsub.
  Qed.

  Lemma type_array_has_bounds n As:
    type_arrayT n As `has_bounds` (length As * n)%Z.
  Proof.
    rewrite type_arrayT_eq /type_arrayT_def.
    replace ((length As * n)%Z) with ((length As * n)%nat : Z) by lia.
    eapply seq_has_bounds.
  Qed.

  Global Instance type_array_proper : Proper ((=) ==> Forall2 (≡) ==> (≡)) type_arrayT.
  Proof. move => ??-> ?? Heq. apply type_le_equiv_list; [by apply type_array_le|done]. Qed.

  Lemma shift_slices_twice (i j: nat) Ls:
    shift_slices i (shift_slices j Ls) = shift_slices (i + j) Ls.
  Proof.
    rewrite /shift_slices map_map. eapply map_ext.
    intros [??]; f_equal; lia.
  Qed.


  Lemma type_arrayT_slices_lookup len i (tys: list (type Σ)) ty :
    tys !! i = Some ty →
    type_arrayT_slices len (length tys) = type_arrayT_slices len i ++ [(i * len, len)%nat] ++ shift_slices (S i * len) (type_arrayT_slices len (length tys - i - 1)).
  Proof.
    intros Hlook.
    eapply take_drop_middle in Hlook as Heq. rewrite -Heq.
    eapply lookup_lt_Some in Hlook as Hlt.
    rewrite app_length /= take_length drop_length.
    replace (i `min` length tys)%nat with i by lia.
    replace (i + S (length tys - S i) - i - 1)%nat with (length tys - S i)%nat by lia.
    replace (S (length tys - S i)) with (1 + (length tys - S i))%nat by lia.
    rewrite !type_array_slices_add /=. rewrite !Nat.add_0_r shift_slices_twice.
    f_equal. f_equal. f_equal. lia.
  Qed.


  Lemma type_array_get_type (i : nat) len tys ty l:
    tys !! i = Some ty →
    l ◁ₗ type_arrayT len tys -∗ (l +ₗ i * len) ◁ₗ ty ∗ l ◁ₗ type_arrayT len (<[ i := placeT (l +ₗ i * len)]>tys).
  Proof.
    intros Hlook. rewrite type_arrayT_eq /type_arrayT_def /=.
    iIntros "Hv". erewrite type_arrayT_slices_lookup; last done.
    eapply take_drop_middle in Hlook as Heq. rewrite -{3}Heq.
    eapply lookup_lt_Some in Hlook as Hlt.
    rewrite seq_borrow_element; last first.
    { rewrite /shift_slices map_length !type_array_slices_length.
      rewrite take_length. lia. }
    replace ((i * len)%nat : Z) with (i * len)%Z by lia. iDestruct "Hv" as "[Hl $]".
    rewrite insert_length.
    erewrite type_arrayT_slices_lookup; last done.
    rewrite -{7}Heq. rewrite insert_app_r_alt; last by rewrite take_length; lia.
    rewrite take_length. replace (i - i `min` length tys)%nat with 0 by lia; simpl.
    iFrame.
  Qed.

  Lemma type_array_put_type (i : nat) (len: nat) tys ty l:
    (l +ₗ i * len) ◁ₗ ty -∗ l ◁ₗ type_arrayT len tys -∗ l ◁ₗ type_arrayT len (<[ i := ty ]>tys).
  Proof.
    destruct (decide (i < length tys)%nat) as [Hlt | Hnlt]; last first.
    { rewrite list_insert_ge; last lia. iIntros "_ $". }
    eapply lookup_lt_is_Some_2 in Hlt as Hlook. destruct Hlook as (ty' & Hlook).
    eapply take_drop_middle in Hlook as Heq.
    iIntros "Hi Hl". iDestruct (type_array_get_type with "Hl") as "[_ Hl]"; first done.
    rewrite -Heq. rewrite !insert_app_r_alt; [|by rewrite take_length; lia..].
    rewrite take_length. replace (i - i `min` length tys)%nat with 0 by lia; simpl.
    rewrite type_arrayT_eq /type_arrayT_def. rewrite !app_length /=.
    iApply seq_return_element. iFrame.
  Qed.

  Lemma type_array_replicate_any_equiv len n:
    type_arrayT len (replicate n (anyT len)) ≡ anyT (n * len).
  Proof.
    rewrite type_arrayT_eq /type_arrayT_def. rewrite replicate_length.
    induction n.
    - simpl; rewrite Z.mul_0_l. rewrite /type_arrayT_slices /=.
      rewrite seqT_empty_is_any_zero. done.
    - replace (S n) with (n + 1) at 3 by lia. rewrite replicate_add /=.
      replace ((S n * len)%Z : Z) with ((n * len)%nat + len)%Z by lia.
      symmetry. rewrite anyT_split_into_sliced. etrans.
      { eapply (sliced_compat_equiv _ _ _ nil); symmetry.
        replace ((n * len)%nat: Z) with (n * len)%Z by lia. eapply IHn. }
      simpl; etrans.
      { eapply (sliced_compat_equiv _ _ _ [_]). symmetry. eapply seqT_singleton, anyT_has_size. }
      simpl. etrans.
      { symmetry. replace (n * len)%nat with (len * n)%nat by lia.
        eapply seqT_split; rewrite ?replicate_length ?type_array_slices_length ?type_array_slices_slices_length /=; lia. }
      simpl. replace (S n) with (n + 1)%nat at 1 by lia.
      rewrite type_array_slices_add /=. replace (len * n + 0)%nat with (n * len + 0)%nat by lia.
      by replace (len * n + len)%nat with (len + n * len)%nat by lia.
  Qed.

  Lemma type_array_loc_in_bounds l len tys : l ◁ₗ type_arrayT len tys ⊢ loc_in_bounds l (length tys * len).
  Proof. rewrite type_arrayT_eq /type_arrayT_def. iIntros "Hl". by iApply sepT_loc_in_bounds. Qed.

  Lemma type_array_loc_in_bounds_mem l n len tys :
    0 ≤ n ≤ length tys →
    l ◁ₗ type_arrayT len tys ⊢ loc_in_bounds (l +ₗ (n * len)) 0.
  Proof.
    iIntros (Hle) "Ha".
    iPoseProof (type_array_loc_in_bounds with "Ha") as "Hl".
    iApply loc_in_bounds_shift_loc; last iFrame.
    split_and!; try lia. rewrite Z.add_0_r. enough (n * len ≤ length tys * len)%Z by lia.
    eapply Z.mul_le_mono_nonneg_r; lia.
  Qed.

  Lemma type_arrayT_raw_pointer l li As :
    l ◁ₗ type_arrayT li As ⊢ l ◁ₗ ptrT l (length As * li) 0.
  Proof.
    iIntros "Hl". rewrite ltr_ptrT_iff. rewrite shift_loc_0.
    iDestruct (type_array_loc_in_bounds with "Hl") as "#Hb".
    iSplit; first done.
    iSplit; last by iPureIntro; lia.
    by replace (Z.to_nat (length As * li)) with (length As * li) by lia.
  Qed.


  Lemma type_arrayT_app l len As Bs:
    l ◁ₗ type_arrayT len (As ++ Bs) ⊣⊢ l ◁ₗ type_arrayT len As ∗ (l +ₗ length As * len) ◁ₗ type_arrayT len Bs.
  Proof.
    rewrite type_arrayT_eq /type_arrayT_def /=.
    rewrite app_length type_array_slices_add.
    rewrite Nat.mul_add_distr_r. rewrite seqT_app ?type_array_slices_length ?type_array_slices_slices_length //.
    by replace ((length As * len)%nat : Z) with (length As * len)%Z by lia.
  Qed.


  Lemma type_arrayT_size (sz: nat) As:
    (Forall (λ A, A `has_size` sz)%type As) →
    (type_arrayT sz As `has_size` (length As * sz)%nat).
  Proof.
    rewrite type_arrayT_eq /type_arrayT_def.
    intros Hall. eapply seqT_has_size.
    revert Hall. induction As as [|A As IH] using rev_ind ; simpl; intros Hall.
    - rewrite /elements_well_sized /type_arrayT_slices /=.
      by eapply Forall2_nil.
    - rewrite app_length /= type_array_slices_add.
      eapply Forall_app in Hall as [Hall Hsz].
      rewrite Forall_singleton in Hsz.
      eapply Forall2_app; first eapply IH, Hall.
      eapply Forall2_cons; split; first done.
       by eapply Forall2_nil.
  Qed.


  Lemma type_arrayT_mono lv n tys tys' :
    lv ◁ₓ type_arrayT n tys -∗
    ([∗ list] ty;ty'∈tys;tys', ∀ lv', ⌜lov_type lv = lov_type lv'⌝ -∗ lv' ◁ₓ ty -∗ lv' ◁ₓ ty') -∗
    lv ◁ₓ type_arrayT n tys'.
  Proof.
    iIntros "Harray Hwand".
    rewrite type_arrayT_eq/type_arrayT_def/=.
    iDestruct (big_sepL2_length with "Hwand") as %->.
    by iApply (seqT_mono with "Harray").
  Qed.


  Global Instance type_arrayT_copy n As:
    TCForall Copy As →
    Copy (type_arrayT n As).
  Proof.
    intros ?. rewrite type_arrayT_eq /type_arrayT_def. apply _.
  Qed.

End type_array.





Section slices.
  Context `{!refinedcG Σ}.

  Definition slicesT (len: Z) (Ts: list (Z * Z * type Σ)) : type Σ :=
      constraintT
      (seqT (Z.to_nat len) (map (λ '(i, li), (Z.to_nat i, Z.to_nat li)) Ts.*1) Ts.*2)
      (0 ≤ len ∧ Forall (λ '(i, li), 0 ≤ i ∧ 0 ≤ li) Ts.*1).

  Lemma xtr_slices_iff x len Ts :
    x ◁ₓ slicesT len Ts ⊣⊢ x ◁ₓ (seqT (Z.to_nat len) (map (λ '(i, li), (Z.to_nat i, Z.to_nat li)) Ts.*1) Ts.*2) ∗ ⌜0 ≤ len⌝ ∗ ⌜Forall (λ '(i, li), 0 ≤ i ∧ 0 ≤ li) Ts.*1⌝.
  Proof.
    rewrite /slicesT /=.
    rewrite xtr_constraintT_iff. iSplit; iIntros "(? & ? & ?)"; iFrame.
    iSplit; iFrame.
  Qed.

  Lemma ltr_slices_iff l len Ts :
    l ◁ₗ slicesT len Ts ⊣⊢ l ◁ₗ (seqT (Z.to_nat len) (map (λ '(i, li), (Z.to_nat i, Z.to_nat li)) Ts.*1) Ts.*2) ∗ ⌜0 ≤ len⌝ ∗ ⌜Forall (λ '(i, li), 0 ≤ i ∧ 0 ≤ li) Ts.*1⌝.
  Proof.
    eapply (xtr_slices_iff (LOC l)).
  Qed.

  Lemma vtr_slices_iff v len Ts :
    v ◁ᵥ slicesT len Ts ⊣⊢ v ◁ᵥ (seqT (Z.to_nat len) (map (λ '(i, li), (Z.to_nat i, Z.to_nat li)) Ts.*1) Ts.*2) ∗ ⌜0 ≤ len⌝ ∗ ⌜Forall (λ '(i, li), 0 ≤ i ∧ 0 ≤ li) Ts.*1⌝.
  Proof.
    eapply (xtr_slices_iff (VAL v)).
  Qed.

  Global Instance slices_copy len slices :
    TCForall Copy slices.*2 →
    Copy (slicesT len slices).
  Proof.
    intros Hf.
    rewrite /slicesT.
    apply constraintT_copy.
    apply seqT_copy.
    done.
  Qed.

  Lemma slices_singleton len A:
    A `has_size` len →
    slicesT len [(0, len, A)] ≡ A.
  Proof.
    intros Hsz. eapply type_equiv_xtr_intro; intros x.
    rewrite xtr_slices_iff.
    iSplit.
    - iIntros "(Hx & %Hnz & _)". rewrite  /= seqT_singleton //.
      by replace (Z.to_nat len: Z) with len by lia.
    - iIntros "Hx". iDestruct (xtr_size_non_zero with "Hx") as "%Hx"; first done.
      rewrite  /= seqT_singleton //; last by replace (Z.to_nat len: Z) with len by lia.
      iFrame. iSplit; first done. iPureIntro.
      constructor; first by lia. constructor.
  Qed.


  Lemma slicesT_covers x len Ts:
    x ◁ₓ slicesT len Ts ⊢
      ⌜covers (map (λ '(i, li), (Z.to_nat i, Z.to_nat li)) Ts.*1) (Z.to_nat len)⌝ ∗
      ⌜0 ≤ len⌝ ∗
      ⌜Forall (λ '(i, li), 0 ≤ i ∧ 0 ≤ li) Ts.*1⌝.
  Proof.
    rewrite /slicesT xtr_constraintT_iff. iIntros "[? [? ?]]". iFrame.
    by iApply seqT_covers.
  Qed.

  Lemma slicesT_subseqent_indices x len L j lj A1 k lk A2 R :
    x ◁ₓ slicesT len (L ++ (j, lj, A1) :: (k, lk, A2) :: R) ⊢ ⌜k = (j + lj)%Z⌝.
  Proof.
    iIntros "Hx". iDestruct (slicesT_covers x with "Hx") as "(%Hcov & %Hle1 & %Hle2)".
    rewrite fmap_app !fmap_cons in Hcov.
    rewrite map_app /= in Hcov.
    eapply covers_succ in Hcov.
    rewrite fmap_app !fmap_cons Forall_app !Forall_cons /= in Hle2.
    destruct Hle2 as (? & ? & ? & ?). iPureIntro. lia.
  Qed.


  Lemma ltr_slicesT_loc_in_bounds l n Ts :
    l ◁ₗ slicesT n Ts -∗ ∃ m: nat, ⌜n = m⌝ ∗ loc_in_bounds l m.
  Proof.
    pose proof (xtr_slices_iff (LOC l) n Ts) as Heq; simpl in Heq; rewrite Heq.
    iIntros "(Hx & %Hlen & %HAs)". rewrite sepT_loc_in_bounds. iExists _. iFrame.
    iPureIntro. lia.
  Qed.

  Lemma slicesT_raw_pointer l Ts n :
    l ◁ₗ slicesT n Ts ⊢ l ◁ₗ ptrT l n 0.
  Proof.
    iIntros "Hl". rewrite ltr_ptrT_iff. rewrite shift_loc_0.
    iDestruct (ltr_slicesT_loc_in_bounds with "Hl") as "#[%m [-> Hb]]".
    iSplit; first done.
    iSplit; last by iPureIntro; lia.
    by replace (Z.to_nat m) with m by lia.
  Qed.


  Lemma slices_has_bounds n Ts:
    slicesT n Ts `has_bounds` n.
  Proof.
    rewrite /ty_has_bounds /slicesT.
    iIntros (l) "Hl". rewrite ltr_constraintT_iff.
    rewrite seq_has_bounds. iDestruct "Hl" as "([Hb _] & %Hn & ?)".
    iSplit; last done. by replace (Z.to_nat (Z.to_nat n)) with (Z.to_nat n) by lia.
  Qed.


  Lemma anyT_split i j:
    0 ≤ i →
    0 ≤ j →
    anyT (i + j) ≡ slicesT (i + j) [(0, i, anyT i); (i, j, anyT j)].
  Proof.
    intros Hi Hj. eapply type_equiv_xtr_intro. intros x.
    replace i with (Z.to_nat i: Z) by lia. replace j with (Z.to_nat j: Z) by lia.
    rewrite anyT_split_into_sliced. rewrite /slicesT.
    rewrite xtr_constraintT_iff /=.
    rewrite !Z2Nat.id //. replace (Z.to_nat (i + j)) with (Z.to_nat i + Z.to_nat j)%nat by lia.
    replace (Z.to_nat 0) with 0%nat by lia. iSplit.
    - iIntros "$". iPureIntro. rewrite !Forall_cons Forall_nil. lia.
    - iIntros "($ & _ & _)".
  Qed.


  Lemma sliced_borrow_iff l As len i li A Bs:
    l ◁ₗ slicesT len (As ++ (i, li, A) :: Bs) ⊣⊢
    l ◁ₗ slicesT len (As ++ (i, li, placeT (l +ₗ i)) :: Bs) ∗ (l +ₗ i) ◁ₗ A.
  Proof.
    rewrite /slicesT !ltr_constraintT_iff.
    rewrite !fmap_app !fmap_cons /=.
    rewrite !map_app !map_cons. rewrite Forall_app Forall_cons.
    iSplit.
    - iIntros "[Hl %Hp]". destruct Hp as (Hlen & Hall1 & [Hi Hj] & Hall2).
      rewrite seq_borrow_element; last first.
      { rewrite map_length !fmap_length //. auto. }
      replace (Z.to_nat i: Z) with (i) by lia.
      iDestruct "Hl" as "[$ $]". by naive_solver.
    - iIntros "[[Hl %Hp] Hi]". destruct Hp as (Hlen & Hall1 & [Hi Hj] & Hall2).
      iDestruct (seq_return_element with "[$Hl $Hi]") as "Hcomb".
      iFrame. by naive_solver.
  Qed.


  Definition seq_locs (l: loc) (Ls: list (nat * nat)) :=
    map (λ n, l +ₗ (n: nat)) (Ls.*1).

  Lemma seqT_borrow_all l len Ls T:
    l ◁ₗ seqT len Ls T ⊢
    ty_own_all T (seq_locs l Ls) ∗
    (∀ U, ty_own_all U (seq_locs l Ls) -∗ l ◁ₗ seqT len Ls U).
  Proof.
    rewrite seqT_eq /seqT_def /=.
    iIntros "(%Hcov & Hb  & Hlocs)".
    iSplitL "Hlocs".
    - rewrite /ty_own_all /seq_locs.
      rewrite !big_sepL2_fmap_l. iFrame.
    - iIntros (U) "Hall". iSplit; first done. iFrame.
      rewrite /ty_own_all /seq_locs.
      rewrite !big_sepL2_fmap_l. iFrame.
  Qed.

  Lemma seqT_borrow_all_val v len Ls T:
    v ◁ᵥ seqT len Ls T ⊢
    ty_own_val_all T (seq_vals Ls v) ∗
    (∀ U, ty_own_val_all U (seq_vals Ls v) -∗ v ◁ᵥ seqT len Ls U).
  Proof.
    rewrite seqT_eq /seqT_def /=.
    iIntros "(%Hlen & %Hcov & Hlocs)".
    iSplitL "Hlocs".
    - rewrite /ty_own_val_all //.
    - iIntros (U) "Hall". iSplit; first done. iSplit; first done.
      rewrite /ty_own_val_all //.
  Qed.


  Lemma nested_slices x len As len2 Bs Cs Ls:
    x ◁ₓ seqT len Ls (As ++ (slicesT len2 Bs) :: Cs) ⊣⊢
    ⌜Forall (λ '(i, li), 0 ≤ i ∧ 0 ≤ li) Bs.*1⌝ ∗ ⌜0 ≤ len2⌝ ∗
    x ◁ₓ seqT len Ls (As ++ (seqT (Z.to_nat len2) (map (λ '(i, li), (Z.to_nat i, Z.to_nat li)) Bs.*1) Bs.*2) :: Cs).
  Proof.
    destruct x; simpl.
    - iSplit.
      + iIntros "Hl". iDestruct (seqT_borrow_all with "[$Hl]") as "[Hx Hcont]".
        rewrite /ty_own_all. iDestruct (big_sepL2_app_inv_r with "Hx") as "(%L1 & %L2 & %Hlocs & Hx & Hy)".
        iDestruct (big_sepL2_cons_inv_r with "Hy") as "(%r & %L2' & %Heq & Hl & Hy)". subst.
        rewrite /slicesT. rewrite ltr_constraintT_iff. iDestruct "Hl" as "[Hl %Hall]". destruct Hall.
        iSplit; first by iPureIntro. iSplit; first by iPureIntro.
        iApply "Hcont". rewrite Hlocs. iFrame.
      + iIntros "[%Hall [%Hlen Hl]]". iDestruct (seqT_borrow_all with "[$Hl]") as "[Hx Hcont]".
        rewrite /ty_own_all. iDestruct (big_sepL2_app_inv_r with "Hx") as "(%L1 & %L2 & %Hlocs & Hx & Hy)".
        iDestruct (big_sepL2_cons_inv_r with "Hy") as "(%r & %L2' & %Heq & Hl & Hy)". subst.
        iApply "Hcont". rewrite Hlocs. iFrame. rewrite /slicesT.
        rewrite ltr_constraintT_iff. iFrame.
        iPureIntro. split; last done. lia.
    - iSplit.
      + iIntros "Hl". iDestruct (seqT_borrow_all_val with "[$Hl]") as "[Hx Hcont]".
        rewrite /ty_own_val_all. iDestruct (big_sepL2_app_inv_r with "Hx") as "(%L1 & %L2 & %Hlocs & Hx & Hy)".
        iDestruct (big_sepL2_cons_inv_r with "Hy") as "(%r & %L2' & %Heq & Hl & Hy)". subst.
        rewrite /slicesT. rewrite vtr_constraintT_iff. iDestruct "Hl" as "[Hl %Hall]". destruct Hall.
        iSplit; first by iPureIntro. iSplit; first by iPureIntro.
        iApply "Hcont". rewrite Hlocs. iFrame.
      + iIntros "[%Hall [%Hlen Hl]]". iDestruct (seqT_borrow_all_val with "[$Hl]") as "[Hx Hcont]".
        rewrite /ty_own_val_all. iDestruct (big_sepL2_app_inv_r with "Hx") as "(%L1 & %L2 & %Hlocs & Hx & Hy)".
        iDestruct (big_sepL2_cons_inv_r with "Hy") as "(%r & %L2' & %Heq & Hl & Hy)". subst.
        iApply "Hcont". rewrite Hlocs. iFrame. rewrite /slicesT.
        rewrite vtr_constraintT_iff. iFrame.
        iPureIntro. split; last done. lia.
  Qed.



  Lemma slices_split_slice (i li lj len: Z) A A1 A2 As Bs:
    A ≡ slicesT (li + lj) [(0, li, A1); (li, lj, A2)] →
    slicesT len (As ++ [(i, (li + lj), A)] ++ Bs) ≡
    slicesT len (As ++ [(i, li, A1); ((i + li), lj, A2)] ++ Bs).
  Proof.
    intros Heq. eapply type_equiv_xtr_intro. intros x.
    rewrite /slicesT !xtr_constraintT_iff.
    rewrite !fmap_app !fmap_cons /=.
    rewrite !map_app !map_cons. rewrite !Forall_app !Forall_cons.
    rewrite Heq. rewrite nested_slices.
    rewrite !Forall_cons Forall_nil /=.
    iSplit.
    - iIntros "[[%Hp [%Hlen Hx]] %Hall]".
      destruct Hp as ([? ?] & [? ?] & _).
      destruct Hall as (Hlen' & Hall1 & Hij & Hall2).
      iSplit; last first.
      { iPureIntro. split_and; eauto with lia. }
      replace (Z.to_nat (li + lj)) with (Z.to_nat li + Z.to_nat lj)%nat by lia.
      rewrite sliced_split_slice //.
      + replace (Z.to_nat i + Z.to_nat li)%nat with (Z.to_nat (i + li))%nat by lia.
        done.
      + rewrite !fmap_length //.
      + rewrite !fmap_length //.
    - iIntros "[Hx %Hall]".
      destruct Hall as (Hlen & Hall1 & [? ?] & [? ?] & Hall2).
      iSplit; first iSplit.
      + iPureIntro. lia.
      + iSplit; first by iPureIntro; lia.
        replace (Z.to_nat (li + lj)) with (Z.to_nat li + Z.to_nat lj)%nat by lia.
        rewrite sliced_split_slice //.
        * replace (Z.to_nat i + Z.to_nat li)%nat with (Z.to_nat (i + li))%nat by lia.
           done.
        * rewrite !fmap_length //.
        * rewrite !fmap_length //.
      + iPureIntro. split_and; eauto with lia.
  Qed.


  Definition slices_locs (l: loc) (As: list (Z * Z * type Σ)) :=
    map (λ n, l +ₗ n) (As.*1.*1).

  Definition slices_vals (v: val) (As: list (Z * Z * type Σ)) :=
    seq_vals ((map (λ '(i, li), (Z.to_nat i, Z.to_nat li)) As.*1)) v.

  Lemma slicesT_borrow_all l len As:
    l ◁ₗ slicesT len As ⊢
    ty_own_all (As.*2) (slices_locs l As) ∗
    (∀ T, ty_own_all T (slices_locs l As) -∗ l ◁ₗ slicesT len (zip As.*1 T)).
  Proof.
    rewrite /slicesT ltr_constraintT_iff. iIntros "[Hl %Hall]".
    destruct Hall as [Hle Hall]. rewrite seqT_borrow_all.
    iDestruct "Hl" as "[Hlocs Hcont]".
    iSplitL "Hlocs".
    { rewrite /slices_locs /seq_locs. iStopProof.
      induction As as [|[[i j] A] As IH] in Hall |-*; simpl; first done.
      rewrite !ty_own_all_cons. eapply Forall_cons in Hall as [[Hi Hj] Hall].
      specialize (IH Hall). rewrite IH. iIntros "[? $]".
      rewrite Z2Nat.id //. }
    iIntros (T) "Hall". rewrite /slices_locs /seq_locs.
    iDestruct (big_sepL2_length with "Hall") as "%Hlen".
    rewrite map_length !fmap_length in Hlen.
    rewrite ltr_constraintT_iff. iSpecialize ("Hcont" $! T with "[Hall]").
    { rewrite /slices_locs /seq_locs. iStopProof.
      induction As as [|[[i j] A] As IH] in T, Hall |-*; simpl; first done.
      destruct T; first rewrite /ty_own_all /= //.
      rewrite !ty_own_all_cons. eapply Forall_cons in Hall as [[Hi Hj] Hall].
      specialize (IH Hall). rewrite IH. iIntros "[? $]".
      rewrite Z2Nat.id //. }
    rewrite fst_zip ?fmap_length ?Hlen //.
    rewrite snd_zip ?fmap_length ?Hlen //.
    iFrame. done.
  Qed.

  Lemma slicesT_borrow_all_val v len As:
    v ◁ᵥ slicesT len As ⊢
    ty_own_val_all (As.*2) (slices_vals v As) ∗
    (∀ T, ty_own_val_all T (slices_vals v As) -∗ v ◁ᵥ slicesT len (zip As.*1 T)).
  Proof.
    rewrite /slicesT vtr_constraintT_iff. iIntros "[Hl %Hall]".
    destruct Hall as [Hle Hall]. rewrite seqT_borrow_all_val.
    iDestruct "Hl" as "[$ Hcont]". iIntros (T) "Hall".
    rewrite vtr_constraintT_iff. iDestruct (big_sepL2_length with "Hall") as "%Hlen".
    rewrite /slices_vals /seq_vals !map_length in Hlen.
    rewrite fst_zip ?fmap_length ?Hlen //.
    rewrite snd_zip ?fmap_length ?Hlen //.
    iSplit; last done.
    iApply "Hcont". iFrame.
  Qed.


  Lemma slicesT_has_size Ts len:
    Forall (λ s, ty_has_size s.2 s.1.2 ∧ 0 ≤ s.1.2) Ts →
    0 ≤ len →
    slicesT len Ts `has_size` len.
  Proof.
    move => Hall ?.
    rewrite /slicesT. apply constraintT_has_size.
    rewrite -{2}(Z2Nat.id len); [|lia].
    apply seqT_has_size. rewrite /elements_well_sized.
    rewrite !Forall2_fmap_l Forall2_fmap_r.
    apply Forall_Forall2_diag. apply: Forall_impl; [done|].
    move => -[[??]?]/= [??].
    rewrite Z2Nat.id; [done|lia].
  Qed.


  Lemma ltr_anyT_extract_range i sz l n :
    0 ≤ i →
    0 ≤ sz →
    i + sz ≤ n →
    l ◁ₗ anyT n ⊢ (l +ₗ i) ◁ₗ (anyT sz) ∗ ((l +ₗ i) ◁ₗ (anyT sz) -∗ l ◁ₗ (anyT n)).
  Proof.
    move => ???. iIntros "Hl".
    assert (n = i + sz + (n - sz - i)) as Heq by lia. rewrite {1}Heq.
    rewrite anyT_split; [|lia..].
    specialize (slices_split_slice 0 i sz n (anyT (i + sz)) (anyT i) (anyT sz) nil) as Hiff.
    simpl in Hiff. rewrite -Heq. rewrite Hiff; last first.
    { eapply anyT_split; lia. }
    rewrite Z.add_0_l.
    pose proof (sliced_borrow_iff l [(0, i, anyT i)] n i sz (anyT sz)) as Hbor. rewrite Hbor.
    iDestruct "Hl" as "[Hs $]". iIntros "Hl".
    rewrite {4}Heq anyT_split; [|lia..].
    rewrite -Heq Hiff.
    2: { eapply anyT_split; lia. }
    rewrite Hbor. iFrame.
  Qed.


End slices.



Section array.
  Context `{!refinedcG Σ}.

  Definition arrayT_def {A: Type} (T: A → type Σ) (n: Z) (xs: list A) : type Σ :=
    constraintT (type_arrayT (Z.to_nat n) (T <$> xs)) (0 ≤ n ∧ ∀ a, T a `has_size` n).

  Definition arrayT_aux : seal (@arrayT_def). Proof. by eexists. Qed.
  Definition arrayT {A: Type} := arrayT_aux.(unseal) A.
  Definition arrayT_eq : @arrayT = @arrayT_def := arrayT_aux.(seal_eq).


  Lemma constraintT_le_true A (φ: Prop):
    φ →
    A ⊑ constraintT A φ.
  Proof.
    intros Hφ. eapply type_le_xtr_intro. intros x.
    rewrite xtr_constraintT_iff.
    - iIntros "$". by iPureIntro.
  Qed.

  Lemma constraintT_le A (φ: Prop):
    constraintT A φ ⊑ A.
  Proof.
    eapply type_le_xtr_intro. intros x.
    rewrite xtr_constraintT_iff.
    iIntros "[$ _]".
  Qed.


  Lemma arrayT_type_arrayT {A} (T: A → type Σ) sz xs:
    0 ≤ sz →
    (∀ a, T a `has_size` sz) →
    type_arrayT (Z.to_nat sz) (T <$> xs) ≡ arrayT T sz xs.
  Proof.
    intros ??. eapply type_equiv_antisym.
    - rewrite arrayT_eq. by apply constraintT_le_true.
    - rewrite arrayT_eq. by apply constraintT_le.
  Qed.

  Lemma ty_has_size_eq A n m:
    A `has_size` n → n = m → A `has_size` m.
  Proof. intros ??. subst. done. Qed.

  Lemma arrayT_has_size {A} (T: A → type Σ) sz xs:
    (∀ a, T a `has_size` sz) →
    0 ≤ sz →
    arrayT T sz xs `has_size` (length xs * sz).
  Proof.
    intros ??. rewrite arrayT_eq /arrayT_def.
    eapply constraintT_has_size.
    eapply ty_has_size_eq; first apply type_arrayT_size.
    - eapply Forall_fmap. eapply Forall_forall; simpl.
      replace (Z.to_nat sz : Z) with sz by lia. done.
    - rewrite fmap_length. lia.
  Qed.


  Global Instance arrayT_copy {X: Type} (T: X → type Σ) n xs:
    (∀ x, Copy (T x)) →
    Copy (arrayT T n xs).
  Proof.
    intros ?.
    rewrite arrayT_eq /arrayT_def.
    eapply constraintT_copy.
    eapply type_arrayT_copy.
    eapply TCForall_Forall, Forall_forall.
    intros x [A [-> _]]%elem_of_list_fmap_2. done.
  Qed.


  Lemma arrayT_split_seq {A} (T: A → type Σ) (sz: nat) xs ys:
    arrayT T sz (xs ++ ys) ≡ seqT ((length xs + length ys) * sz) [(0%nat, length xs * sz);  (length xs * sz, length ys * sz)]%nat [arrayT T sz xs; arrayT T sz ys].
  Proof.
    eapply type_equiv_xtr_intro. intros x.
    rewrite {1}arrayT_eq /arrayT_def /=.
    rewrite xtr_constraintT_iff.
    rewrite fmap_app. rewrite type_arrayT_split //.
    rewrite !fmap_length.
    iSplit.
    - iIntros "[Har %HT]". destruct HT as (HT1 & HT2).
      rewrite -!arrayT_type_arrayT //. replace (Z.to_nat sz) with sz by lia. done.
    - iIntros "Hx".
      destruct x; simpl.
      + iDestruct (seqT_borrow_all with "Hx") as "[Hx Hcont]".
        rewrite /seq_locs /=. iDestruct "Hx" as "(Hx1 & Hx2 & _)".
        rewrite arrayT_eq /arrayT_def.
        rewrite !ltr_constraintT_iff.
        iDestruct "Hx1" as "[Hx1 %Hsz]". iDestruct "Hx2" as "[Hx2 %Hsz']".
        iSplit; last done. replace (Z.to_nat sz) with sz by lia.
        iApply "Hcont". by iFrame.
      + iDestruct (seqT_borrow_all_val with "Hx") as "[Hx Hcont]".
        rewrite /seq_vals /=. iDestruct "Hx" as "(Hx1 & Hx2 & _)".
        rewrite arrayT_eq /arrayT_def.
        rewrite !vtr_constraintT_iff.
        iDestruct "Hx1" as "[Hx1 %Hsz]". iDestruct "Hx2" as "[Hx2 %Hsz']".
        iSplit; last done. replace (Z.to_nat sz) with sz by lia.
        iApply "Hcont". by iFrame.
  Qed.

  Lemma arrayT_split {A} (T: A → type Σ) sz xs ys:
    arrayT T sz (xs ++ ys) ≡ slicesT ((length xs + length ys) * sz) [(0, length xs * sz, arrayT T sz xs); (length xs * sz, length ys * sz, arrayT T sz ys)].
  Proof.
    eapply type_equiv_xtr_intro. intros x.
    rewrite {1}arrayT_eq /arrayT_def /=.
    rewrite xtr_constraintT_iff.
    rewrite fmap_app. rewrite type_arrayT_split //.
    rewrite !fmap_length.
    iSplit.
    - iIntros "[Har %HT]". destruct HT as (HT1 & HT2).
      rewrite /slicesT /=.
      rewrite xtr_constraintT_iff. iSplit; last first.
      { iPureIntro. rewrite /= !Forall_cons Forall_nil. lia. }
      rewrite -!arrayT_type_arrayT //.
      replace (Z.to_nat 0) with 0%nat by lia.
      replace ((length xs + length ys) * Z.to_nat sz)%nat with ((Z.to_nat ((length xs + length ys) * sz))) by lia.
      replace (Z.to_nat (length xs * sz)) with ((length xs * Z.to_nat sz)%nat) by lia.
      replace (Z.to_nat (length ys * sz)) with ((length ys * Z.to_nat sz)%nat) by lia.
      done.
    - iIntros "Hslices". rewrite /slicesT /=.
      rewrite xtr_constraintT_iff. iDestruct "Hslices" as "[Hx %Hall]".
      rewrite !Forall_cons Forall_nil /= in Hall.
      destruct x; simpl.
      + iDestruct (seqT_borrow_all with "Hx") as "[Hx Hcont]".
        rewrite /seq_locs /=. iDestruct "Hx" as "(Hx1 & Hx2 & _)".
        rewrite arrayT_eq /arrayT_def.
        rewrite !ltr_constraintT_iff.
        iDestruct "Hx1" as "[Hx1 %Hsz]". iDestruct "Hx2" as "[Hx2 %Hsz']".
        iSplit; last done.
        replace (Z.to_nat 0) with 0%nat by lia.
        replace ((length xs + length ys) * Z.to_nat sz)%nat with ((Z.to_nat ((length xs + length ys) * sz))) by lia.
        replace (Z.to_nat (length xs * sz)) with ((length xs * Z.to_nat sz)%nat) by lia.
        replace (Z.to_nat (length ys * sz)) with ((length ys * Z.to_nat sz)%nat) by lia.
        iApply "Hcont". by iFrame.
      + iDestruct (seqT_borrow_all_val with "Hx") as "[Hx Hcont]".
        rewrite /seq_vals /=. iDestruct "Hx" as "(Hx1 & Hx2 & _)".
        rewrite arrayT_eq /arrayT_def.
        rewrite !vtr_constraintT_iff.
        iDestruct "Hx1" as "[Hx1 %Hsz]". iDestruct "Hx2" as "[Hx2 %Hsz']".
        iSplit; last done.
        replace (Z.to_nat 0) with 0%nat by lia.
        replace ((length xs + length ys) * Z.to_nat sz)%nat with ((Z.to_nat ((length xs + length ys) * sz))) by lia.
        replace (Z.to_nat (length xs * sz)) with ((length xs * Z.to_nat sz)%nat) by lia.
        replace (Z.to_nat (length ys * sz)) with ((length ys * Z.to_nat sz)%nat) by lia.
        iApply "Hcont". by iFrame.
  Qed.


  Lemma arrayT_concat {X: Type} (T: X → type Σ) (Ls: list (nat * nat)) sz ys:
    (∀ x, T x `has_size` sz) →
    0 ≤ sz →
    covers Ls (length (concat ys) * Z.to_nat sz) →
    Forall2 (λ xs p, length xs * Z.to_nat sz = (p.2: nat))%nat ys Ls →
    arrayT T sz (concat ys) ≡ seqT (length (concat ys) * Z.to_nat sz) Ls (map (λ xs, arrayT T sz xs) ys).
  Proof.
    intros Hsz Hle. induction ys as [|y ys IH] using rev_ind in Ls |-*=>//=.
    - intros Hcov Hall. eapply Forall2_nil_inv_l in Hall. subst.
      rewrite -arrayT_type_arrayT //. rewrite /=.
      rewrite type_arrayT_seqT_empty //.
    - rewrite concat_app /= !app_nil_r app_length.
      intros Hcov Hall. eapply Forall2_app_inv_l in Hall as (Ls1 & Ls2 & Hall1 & Hall2 & Heq).
      subst. eapply Forall2_cons_inv_l in Hall2 as ([i li] & Ls1' & Heq & Hall2 & ->).
      eapply Forall2_nil_inv_l in Hall2. subst. simpl in Heq. subst.
      rewrite Nat.mul_add_distr_r in Hcov.
      eapply covers_app_singleton_iff in Hcov as [Hcov Heq].
      assert (i = (length (concat ys) * Z.to_nat sz)%nat) as -> by lia. clear Heq.
      rewrite map_app /=. replace sz with (Z.to_nat sz: Z) by lia. rewrite !Nat2Z.id.
      rewrite arrayT_split_seq. replace (Z.to_nat sz: Z) with sz by lia.
      rewrite IH //.
      eapply type_equiv_antisym.
      + specialize (sliced_flatten nil ((length (concat ys) + length y) * Z.to_nat sz) 0 (length (concat ys) * Z.to_nat sz) nil) as Hflatten.
        simpl in Hflatten. etrans; first by eapply Hflatten; lia.
        rewrite shift_slices_zero. done.
      + specialize (seqT_nest nil ((length (concat ys) + length y) * Z.to_nat sz) 0 (length (concat ys) * Z.to_nat sz) nil) as Hnest.
        simpl in Hnest. etrans; last first.
        { eapply Hnest; simpl; try lia. eapply covers_slices_length. done. }
        rewrite shift_slices_zero. done.
  Qed.

  Lemma arrayT_get_type {X: Type} (T: X → type Σ) (i : nat) len xs x l:
    xs !! i = Some x →
    l ◁ₗ arrayT T len xs -∗ (l +ₗ i * len) ◁ₗ T x ∗ (∀ y, (l +ₗ i * len) ◁ₗ T y -∗ l ◁ₗ arrayT T len (<[i := y]> xs)).
  Proof.
    intros Hlook. rewrite arrayT_eq /arrayT_def.
    rewrite ltr_constraintT_iff. iIntros "[Hl %Hz]".
    iDestruct (type_array_get_type with "Hl") as "[Hi Hl]".
    { rewrite list_lookup_fmap. erewrite Hlook; done. }
    replace (Z.to_nat len: Z) with (len: Z) by lia. iFrame.
    iIntros (y) "Hi". rewrite ltr_constraintT_iff. iSplit; last done.
    iDestruct (type_array_put_type i with "[Hi] Hl") as "?".
    { replace (Z.to_nat len: Z) with (len: Z) by lia. iFrame. }
    rewrite list_insert_insert. rewrite list_fmap_insert //.
  Qed.


  Lemma xtr_array_size {X} x (T: X → type Σ) sz xs:
    x ◁ₓ arrayT T sz xs ⊢ ⌜0 ≤ sz ∧ ∀ y, T y `has_size` sz⌝.
  Proof.
    rewrite arrayT_eq /arrayT_def xtr_constraintT_iff.
    iIntros "[_ $]".
  Qed.

End array.


Global Typeclasses Opaque slicesT.
