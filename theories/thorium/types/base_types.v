
From quiver.base Require Export facts dprop.
From quiver.thorium.types Require Export types.
From quiver.thorium.types Require Import combinators.


Section base_types.
  Context {Σ} `{!refinedcG Σ}.


  (* The copyable value type *)
  Program Definition valueT_def (v: val) (n: Z) : type Σ :=
    {|
      ty_own l := (l ↦ v  ∗ v `has_size_val` n)%I;
      ty_own_val w := (⌜w = v⌝ ∗ v `has_size_val` n)%I ;
      ty_has_size m := n = m;
    |}.
  Next Obligation.
    iIntros (v n m w Heq) "[-> %Heq2]"; simpl in *. subst. done.
  Qed.
  Next Obligation.
    iIntros (v n m l Hop) "[Hl %]".
    iExists _. iFrame. iPureIntro; naive_solver.
  Qed.
  Next Obligation.
    iIntros (v n m w) "Hl [-> %]".
    iFrame. by iPureIntro.
  Qed.

  Definition valueT_aux : valueT_def.(seal).
  Proof. by eexists. Qed.
  Definition valueT := valueT_aux.(unseal).
  Definition valueT_eq : valueT = valueT_def :=
  seal_eq valueT_aux.

  Global Instance valueT_copy v ot: Copy (valueT v ot).
  Proof.
    rewrite valueT_eq. econstructor. apply _.
  Qed.


  Global Instance non_null_value v n:
    NonNull (valueT v n).
  Proof.
    rewrite /NonNull valueT_eq /valueT_def /=.
    iIntros (l) "[Hl _]". by iApply heap_mapsto_loc_in_bounds_0.
  Qed.


  Lemma valueT_has_size v n m:
    valueT v n `has_size` m ↔ n = m.
  Proof. rewrite valueT_eq //=. Qed.

  (* Lemma valueT_has_size_int v n m:
    valueT v n `has_size_int` m ↔ (n = m ∧ 0 ≤ m).
  Proof.
    rewrite /has_size_int. rewrite valueT_has_size. lia.
  Qed. *)

  Lemma valueT_intro l v n:
    v `has_size_val` n →
    l ↦ v  ⊢ l ◁ₗ valueT v n.
  Proof.
    intros Hval. rewrite valueT_eq //=.
    iIntros "$". iPureIntro; eauto.
  Qed.

  Lemma vtr_valueT_intro v w n : ⌜v = w⌝ ∗ ⌜w `has_size_val` n⌝ ⊢ v ◁ᵥ valueT w n.
  Proof.
    rewrite valueT_eq /valueT_def //=.
  Qed.

  Lemma vtr_valueT_elim v w n : v ◁ᵥ valueT w n ⊢ ⌜v = w⌝ ∗ ⌜w `has_size_val` n⌝.
  Proof.
    rewrite valueT_eq /valueT_def //=.
  Qed.

  Lemma valueT_elim_loc A l v n:
    l ◁ₗ valueT v n -∗ v ◁ᵥ A -∗ l ◁ₗ A.
  Proof.
    rewrite valueT_eq //=. iIntros "(Hl & %) Hv".
    iApply (ty_move_back with "Hl Hv"); eauto.
  Qed.

  Lemma valueT_elim_val A v w n:
    w ◁ᵥ valueT v n -∗ v ◁ᵥ A -∗ w ◁ᵥ A.
  Proof.
    rewrite valueT_eq //=. iIntros "(Hl & %) Hv".
    by iDestruct "Hl" as "->".
  Qed.

  Lemma valueT_elim_xtr A x v n:
    x ◁ₓ valueT v n -∗ v ◁ᵥ A -∗ x ◁ₓ A.
  Proof.
    destruct x as [l|w]; simpl; eauto using valueT_elim_val, valueT_elim_loc.
  Qed.

  Lemma valueT_loc_val l A n:
    A `has_size` n →
    l ◁ₗ A ⊣⊢ ∃ v, l ◁ₗ valueT v n ∗ v ◁ᵥ A.
  Proof.
    intros Hop. iSplit.
    - iIntros "Hl".
      iDestruct (ty_move with "Hl") as (v) "[Hl Hv]"; first done.
      iDestruct (ty_size_eq with "Hv") as %Hsz; first done.
      iExists v. iFrame. by iApply valueT_intro.
    - iIntros "[%v [Hl Hv]]".
      iApply (valueT_elim_loc with "Hl Hv"); done.
  Qed.

  Lemma valueT_xtr_val x A n:
    A `has_size` n →
    x ◁ₓ A ⊣⊢ ∃ v, x ◁ₓ valueT v n ∗ v ◁ᵥ A.
  Proof.
    intros Hot. destruct x as [l|v]; first by apply valueT_loc_val.
    iSplit.
    - iIntros "Hv".
      iDestruct (ty_size_eq with "Hv") as %Hsz; first done.
      iExists _. iFrame. by iApply vtr_valueT_intro.
    - iIntros "[%w [Hv Hw]]". by iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
  Qed.


  Global Instance value_ptr_type (l: loc) : PtrType (valueT l (ly_size void*)).
  Proof.
    econstructor.
    - apply valueT_has_size; done.
    - iIntros (v) "Hv". iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
      iExists l. iPureIntro. eapply val_to_of_loc.
  Qed.


  Global Instance type_params_value v n :
    TypeParams (valueT v n) (val * Z) (v, n) (λ p, valueT p.1 p.2) (prod_relation eq eq) | 1.
  Proof.
    intros [v' n'] [? ?]; simpl in *; by subst.
  Qed.

  Lemma valueT_non_negative x v n:
    x ◁ₓ valueT v n ⊢ ⌜0 ≤ n⌝.
  Proof.
    eapply xtr_size_non_zero. by apply valueT_has_size.
  Qed.




  (* The place type *)
  Program Definition placeT_def (l: loc) : type Σ :=
    {|
      ty_own l' := (⌜l = l'⌝)%I;
      ty_own_val w := False%I ;
      ty_has_size n := False;
    |}.
  Solve All Obligations with naive_solver.

  Definition placeT_aux : placeT_def.(seal).
  Proof. by eexists. Qed.
  Definition placeT := placeT_aux.(unseal).
  Definition placeT_eq : placeT = placeT_def :=
    seal_eq placeT_aux.

  Lemma placeT_create l:
    ⊢ l ◁ₗ placeT l.
  Proof. rewrite placeT_eq /placeT_def //=. eauto. Qed.

  Lemma placeT_merge l l' A:
    l ◁ₗ placeT l' ∗ l' ◁ₗ A ⊢ l ◁ₗ A.
  Proof. rewrite placeT_eq /placeT_def //=. by iIntros "[-> $]". Qed.

  Lemma placeT_is_eq l r :
    l ◁ₗ placeT r ⊢ ⌜l = r⌝.
  Proof. rewrite placeT_eq /placeT_def //=. eauto. Qed.

  Global Instance placeT_persistent l r : Persistent (l ◁ₗ placeT r).
  Proof. rewrite placeT_eq /placeT_def /=. apply _. Qed.

  (* The any type *)
  Definition anyT_def (n: Z) : type Σ :=
    existsT (λ (v: val), valueT v n).
  Definition anyT_aux : anyT_def.(seal).
  Proof. by eexists. Qed.
  Definition anyT := anyT_aux.(unseal).
  Definition anyT_eq : anyT = anyT_def :=
    seal_eq anyT_aux.

  Lemma anyT_has_size_iff n m:
    anyT n `has_size` m ↔ n = m.
  Proof.
    rewrite anyT_eq /anyT_def.
    rewrite existsT_has_size. setoid_rewrite valueT_has_size.
    split; eauto using ([]: val).
  Qed.

  Lemma anyT_has_size (n : Z):
    anyT n `has_size` n.
  Proof.
    rewrite anyT_has_size_iff //.
  Qed.


  Lemma ltr_anyT_pts_ly l ly:
    l ◁ₗ anyT (ly_size ly) ⊣⊢ (l ↦|ly|)%I.
  Proof.
    rewrite anyT_eq /anyT_def existsT_eq /existsT_def //=.
    rewrite valueT_eq /valueT_def //=.
    iSplit.
    - iIntros "(%x & Hl & %Heq)". iApply heap_mapsto_layout_iff. iExists _. iFrame.
      iPureIntro. by eapply has_size_val_has_layout.
    - iIntros "(%v & %Heq & % & Hl)". iExists v. iFrame.
      iPureIntro. by eapply has_size_val_has_layout.
  Qed.

  Lemma ltr_anyT_pts_ly_list L LY :
    ([∗ list] l;ly ∈ L;LY, l ◁ₗ anyT (ly_size ly)) ⊣⊢ ([∗ list] l;ly ∈ L;LY, l ↦|ly|).
  Proof.
    iSplit; iIntros "Hlocs"; iApply (big_sepL2_impl with "Hlocs []");
      iModIntro; iIntros (?????) "?"; rewrite ltr_anyT_pts_ly //.
  Qed.


  Global Instance anyT_copy ly: Copy (anyT ly).
  Proof.
    rewrite anyT_eq /anyT_def. apply _.
  Qed.

  Global Instance anyT_non_null ly: NonNull (anyT ly).
  Proof.
    rewrite anyT_eq /anyT_def. apply _.
  Qed.

  (* the any type *)
  Lemma vtr_anyT_intro v n : v `has_size_val` n ⊢ v ◁ᵥ anyT n.
  Proof.
    iIntros "%Hly". rewrite anyT_eq /anyT_def.
    rewrite vtr_existsT_iff.
    iExists v. iApply vtr_valueT_intro.
    iPureIntro. split_and!; done.
  Qed.

  Lemma vtr_anyT_elim v n : v ◁ᵥ anyT n ⊢ v `has_size_val` n.
  Proof.
    iIntros "Hany". rewrite anyT_eq /anyT_def.
    rewrite vtr_existsT_iff. iDestruct "Hany" as "(%x & Hsingle)".
    rewrite vtr_valueT_elim. iDestruct "Hsingle" as "(-> & $)".
  Qed.

  Lemma cast_to_any_ltr A l n:
    A `has_size` n →
    l ◁ₗ A ⊢ l ◁ₗ anyT n.
  Proof.
    iIntros (Hly) "Hl".
    iDestruct (ty_move with "Hl") as "[%v [Hl Hv]]"; first done.
    iPoseProof (ty_size_eq with "Hv") as "%Hly2"; first done.
    iPoseProof (vtr_anyT_intro with "[//]") as "Hv'".
    iApply (ty_move_back with "Hl Hv'").
  Qed.

  Lemma xtr_cast_to_anyT A x n:
    A `has_size` n →
    x ◁ₓ A ⊢ x ◁ₓ anyT n.
  Proof.
    intros Hly.
    destruct x as [l|v]; simpl.
    - by apply cast_to_any_ltr.
    - iIntros "Hv". iDestruct (ty_size_eq with "Hv") as %Hsz; first done.
      by iApply vtr_anyT_intro.
  Qed.


  Lemma xtr_to_any x A n :
   A `has_size` n →
   x ◁ₓ A ⊢ vty A ∗ x ◁ₓ anyT n.
  Proof.
    iIntros (Hly) "Hl".
    rewrite valueT_xtr_val //. iDestruct "Hl" as (v) "[Hl Hv]"; simpl.
    iSplitL "Hv". { iExists _. iFrame. }
    iApply (xtr_cast_to_anyT with "Hl").
    eapply valueT_has_size. done.
  Qed.

  Lemma has_size_inty_any A n:
    A `has_size` n →
    A `is_ty` anyT n.
  Proof.
    intros Hly.
    econstructor. split; intros x.
    - by eapply (xtr_cast_to_anyT A (LOC x)).
    - by eapply (xtr_cast_to_anyT A (VAL x)).
  Qed.


  Global Instance type_params_any n :
    TypeParams (anyT n) Z n anyT eq | 1.
  Proof.
    intros n' ?; simpl in *; by subst.
  Qed.


  Lemma anyT_non_negative x n:
    x ◁ₓ anyT n ⊢ ⌜0 ≤ n⌝.
  Proof.
    eapply xtr_size_non_zero. by apply anyT_has_size.
  Qed.

  (* The void type *)
  Definition voidT_def := anyT 0.
  Definition voidT_aux : voidT_def.(seal).
  Proof. by eexists. Qed.
  Definition voidT := voidT_aux.(unseal).
  Definition voidT_eq : voidT = voidT_def :=
    seal_eq voidT_aux.

  Lemma vtr_void : ⊢ VOID ◁ᵥ voidT.
  Proof.
    rewrite /VOID voidT_eq /voidT_def anyT_eq /anyT_def.
    rewrite existsT_eq /existsT_def //= valueT_eq /valueT_def //=.
    iExists []. iPureIntro. split_and!; done.
  Qed.

  Lemma void_has_size: voidT `has_size` 0.
  Proof.
    rewrite voidT_eq /voidT_def. eapply anyT_has_size_iff. done.
  Qed.

  Global Instance voidT_copy : Copy voidT.
  Proof.
    rewrite voidT_eq /voidT_def. apply _.
  Qed.

  Global Instance voidT_non_null: NonNull voidT.
  Proof.
    rewrite voidT_eq /voidT_def. apply _.
  Qed.

  Global Instance type_params_void : TypeParams voidT unit () (λ _, voidT) (λ _ _, True) | 1.
  Proof.
    intros n' ?; simpl in *; by subst.
  Qed.

  (* the int type *)
  Definition intT_def it n :=
    constraintT (valueT (i2v n it) (int_size it)) (n ∈ it).

  Definition intT_aux : intT_def.(seal).
  Proof. by eexists. Qed.
  Definition intT := intT_aux.(unseal).
  Definition intT_eq : intT = intT_def :=
    seal_eq intT_aux.



  Lemma intT_has_size it n:
    (intT it n) `has_size` (int_size it).
  Proof.
    rewrite intT_eq /intT_def //=.
    rewrite constraintT_has_size valueT_has_size //.
  Qed.

  Global Instance intT_copy it n: Copy (intT it n).
  Proof.
    rewrite intT_eq; apply _.
  Qed.

  Global Instance intT_non_null it n: NonNull (intT it n).
  Proof.
    rewrite intT_eq; apply _.
  Qed.

  Lemma vtr_int z it:
    z ∈ it → ⊢ (i2v z it) ◁ᵥ intT it z.
  Proof.
    intros Hel. rewrite intT_eq /intT_def //= vtr_constraintT_iff valueT_eq //=. simpl.
    iPureIntro; split_and!; eauto.
    rewrite /i2v. destruct (val_of_Z) eqn: Heq.
    + simpl. rewrite int_size_bytes_per_int /has_size_val.
      erewrite val_of_Z_length; eauto.
    + eapply val_of_Z_is_Some with (p := None) in Hel as [? Heq'].
      rewrite Heq' in Heq. naive_solver.
  Qed.


  Lemma vtr_int_inversion v it n:
    v ◁ᵥ intT it n ⊢ ⌜v = i2v n it ∧ n ∈ it⌝.
  Proof.
    rewrite intT_eq /intT_def //= vtr_constraintT_iff valueT_eq //=.
    iIntros ([[G1 G2] G3]). done.
  Qed.

  Global Instance type_params_int it n:
    TypeParams (intT it n) (int_type * Z) (it, n) (λ p, intT p.1 p.2) (prod_relation eq eq) | 1.
  Proof.
    intros [? ?] [? ?]; simpl in *; by subst.
  Qed.

  (* the bool type *)
  (* usually b is a bool_decide, we currently only support strict booleans (i.e., 0 or 1, not n = 0 or n ≠ 0) *)
  Definition boolT_def (it: int_type) (b: bool) : type Σ :=
      intT it (bool_to_Z b).

  Definition boolT_aux : boolT_def.(seal).
  Proof. by eexists. Qed.
  Definition boolT := boolT_aux.(unseal).
  Definition boolT_eq : boolT = boolT_def :=
    seal_eq boolT_aux.


  Global Instance boolT_copy it b: Copy (boolT it b).
  Proof.
    rewrite boolT_eq /boolT_def; destruct b; apply _.
  Qed.

  Global Instance boolT_non_null it b: NonNull (boolT it b).
  Proof.
    rewrite boolT_eq /boolT_def; destruct b; apply _.
  Qed.

  Lemma vtr_bool (b: bool) it:
    ⊢ (b2v b it) ◁ᵥ boolT it b.
  Proof.
    rewrite boolT_eq /boolT_def. iApply vtr_int; eauto with quiver_logic.
  Qed.

  Lemma vtr_bool_inversion v it b:
    v ◁ᵥ boolT it b ⊢ ⌜v = b2v b it⌝.
  Proof.
    rewrite boolT_eq /boolT_def. rewrite vtr_int_inversion.
    by iIntros ([-> _]).
  Qed.

  Lemma boolT_has_size it n:
    (boolT it n) `has_size` (int_size it).
  Proof.
    rewrite boolT_eq /boolT_def. eapply intT_has_size.
  Qed.

  Global Instance type_params_bool it b:
    TypeParams (boolT it b) (int_type * bool) (it, b) (λ p, boolT p.1 p.2) (prod_relation eq eq) | 1.
  Proof.
    intros [? ?] [? ?]; simpl in *; by subst.
  Qed.

  (* a type indicating the value was moved *)

  Definition vty_persist (v: val) (A: type Σ) (P: iProp Σ) : Prop :=
    v ◁ᵥ A ⊢ □ P.


  Lemma vty_fork_persistent v A:
    v ◁ᵥ A ⊢ v ◁ᵥ A ∗ (∀ P, ⌜vty_persist v A P⌝ -∗ □ P).
  Proof.
    iIntros "Hv". rewrite -bi.persistent_and_sep_1. iSplit; first done.
    iIntros (x) "%Hty". by iApply Hty.
  Qed.

  Definition movedT_def (A: type Σ): type Σ := existsT (λ n, existsT (λ v, sepT (valueT v n) (∀ P, ⌜vty_persist v A P⌝ -∗ □ P))).

  Definition movedT_aux : movedT_def.(seal).
  Proof. by eexists. Qed.
  Definition movedT := movedT_aux.(unseal).
  Definition movedT_eq : movedT = movedT_def :=
  seal_eq movedT_aux.


  Global Instance movedT_copy A: Copy (movedT A).
  Proof. rewrite movedT_eq /movedT_def. apply _. Qed.

  Lemma vtr_move v A:
    v ◁ᵥ A ⊢ v ◁ᵥ movedT A.
  Proof.
    iIntros "Hv". rewrite movedT_eq /movedT_def.
    rewrite vtr_existsT_iff. iExists (length v).
    rewrite vtr_existsT_iff. iExists v.
    rewrite vtr_sepT_iff.
    iDestruct (vty_fork_persistent with "Hv") as "[Hv $]".
    iApply vtr_valueT_intro. iSplit; done.
  Qed.


  Lemma vtr_moved_inv v A P:
    vty_persist v A P →
    v ◁ᵥ movedT A ⊢ P.
  Proof.
    rewrite movedT_eq /movedT_def.
    rewrite vtr_existsT_iff. iIntros (Hty) "[%n Hv]".
    rewrite vtr_existsT_iff. iDestruct "Hv" as "[%w Hv]".
    rewrite vtr_sepT_iff. iDestruct "Hv" as "[Hv Hent]".
    iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
    iDestruct ("Hent" $! P with "[//]") as "#P"; last done.
  Qed.

  Global Instance movedT_le :
    Proper ((⊑) ==> (⊑)) (movedT).
  Proof.
    rewrite movedT_eq /movedT_def. intros A B Hsub.
    f_equiv=> n. f_equiv=> v. f_equiv. f_equiv => P.
    f_equiv. f_equiv. rewrite /flip /impl. rewrite /vty_persist.
    intros <-. eapply Hsub.
  Qed.

  Global Instance movedT_proper :
    Proper ((≡) ==> (≡)) (movedT).
  Proof.
    rewrite movedT_eq /movedT_def. intros A B Hsub.
    f_equiv=> n. f_equiv=> v. f_equiv. f_equiv => P.
    f_equiv. f_equiv. rewrite /vty_persist. f_equiv.
    eapply Hsub.
  Qed.

  Lemma xtr_moved_copy x A:
    Copy A →
    x ◁ₓ movedT A ⊢ x ◁ₓ A.
  Proof.
    intros Hcp. rewrite movedT_eq /movedT_def.
    rewrite xtr_existsT_iff. setoid_rewrite xtr_existsT_iff.
    setoid_rewrite xtr_sepT_iff. iDestruct 1 as "(%n & %w & Hv & HP)".
    iDestruct ("HP" $! (w ◁ᵥ A) with "[]") as "#Hw".
    { iPureIntro. rewrite /vty_persist. iIntros "#Hw". by iModIntro. }
    by iApply (valueT_elim_xtr with "Hv Hw").
  Qed.


  Definition empT_def := existsT placeT.
  Definition empT_aux : empT_def.(seal).
  Proof. by eexists. Qed.
  Definition empT := empT_aux.(unseal).
  Definition empT_eq : empT = empT_def :=
    seal_eq empT_aux.

  Lemma empT_intro l : ⊢ l ◁ₗ empT.
  Proof.
    rewrite empT_eq /empT_def ltr_existsT_iff. iExists _. iApply placeT_create.
  Qed.

  Lemma empT_iff l :
    l ◁ₗ empT ⊣⊢ emp.
  Proof.
    iSplit; iIntros "H"; auto.
    by iApply empT_intro.
  Qed.
End base_types.

Global Notation "'int[' it ']' n" := (intT it n) (at level 60, n at next level, format "int[ it ]  n") : ref_type_scope.
Global Notation "'bool[' it ']' b" := (boolT it b) (at level 60, b at next level, format "bool[ it ]  b") : ref_type_scope.
Global Notation "'any[' n ']'" := (anyT n) (at level 60, n at next level, format "any[ n ]") : ref_type_scope.
Global Notation "'value[' n ']' v" := (valueT v n) (at level 60, v at next level, format "value[ n ]  v") : ref_type_scope.
Global Notation moved := (movedT).
Global Notation empty := (empT).
Global Notation void := (voidT).
Global Notation place := (placeT).
