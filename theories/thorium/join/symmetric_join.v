
From quiver.argon.simplification Require Export simplify normalize elim_existentials cleanup.
From quiver.base Require Import classes.
From quiver.argon.base Require Import classes.
From quiver.thorium.logic Require Import syntax pointers.
From quiver.argon.abduction Require Import joined.
From quiver.thorium.join Require Import join_types.

Section normal_form_join_precondition.
Context `{!refinedcG Σ}.

(* STEP ONE: We prepare the join operation.
  To do so, we merge the nfs part of the two propositions
    - taking the lifting existential quantifiers from both the left and the right side
    - adding a type cast for all cases where l ◁ₗ A is on the left and l ◁ₗ B is on the right
    - framing every location type assignment l ◁ₗ A which exists only on one, but not both sides
    - joining every type assignment A `is_ty` B1 and A `is_ty` B2 as `join_type φ A B1 B2`

  STEP TWO: We merge the types according to [types_intersect_pre]
  STEP THREE: We normalize and prune existential quantifiers where possible.
*)

Definition nf_join_pre (φ: dProp) (P1 P2 R: iProp Σ) :=
  simplify (case φ P1 P2) R.

Definition nfs_prepare_merge
  (φ: dProp)
  (* the two nfs judgements that we want to merge *)
  (Γ1 Γ2: list Prop)
  (Δ1 Δ2: list (iProp Σ))
  (P1 P2: iProp Σ)
  (* the precondition we have computed *)
  (R: iProp Σ) :=
  simplify (case φ (nfs Γ1 Δ1 P1) (nfs Γ2 Δ2 P2)) R.

Definition nfs_prepare_merge_process
  (φ: dProp)
  (* the two nfs judgements that we want to merge *)
  (Γ1 Γ2: list Prop)
  (Δ1 Δ2: list (iProp Σ))
  (P1 P2: iProp Σ)
  (* the frames we have computed for each side *)
  (F1 F2: list (iProp Σ))
  (* the precondition we have computed *)
  (R: iProp Σ) :=
  simplify ((case φ ([∗] F1) ([∗] F2))%I -∗ case φ (nfs Γ1 Δ1 P1) (nfs Γ2 Δ2 P2)) R.

Definition join_types_precond (P R: iProp Σ) : Prop :=
  simplify P R.

(* JOIN PRE PASS *)
Lemma nf_join_pre_pass φ P1 P2 Q R :
  nfs_prepare_merge φ nil nil nil nil P1 P2 Q →
  simplify_chain [
    join_types_precond;
    normalize;
    elim_existentials
  ] Q R →
  nf_join_pre φ P1 P2 R.
Proof.
  rewrite /nfs_prepare_merge /nf_join_pre.
  rewrite !nfs_empty. intros Hsimpl Hchain.
  eapply simplify_trans; first done.
  apply: is_simplify_elim_simplify_chain.
 Qed.

(* COLLECTING THE ASSERTIONS *)
Lemma nfs_prepare_merge_ex_l {X} φ Γ1 Γ2 Δ1 Δ2 P1 P2 R:
  once_tc (∀! x: X, nfs_prepare_merge φ Γ1 Γ2 Δ1 Δ2 (P1 x) P2 (R x)) →
  nfs_prepare_merge φ Γ1 Γ2 Δ1 Δ2 (∃ x: X, P1 x) P2 (∃ x, R x).
Proof.
  rewrite /nfs_prepare_merge. rewrite nfs_exists.
  intros ?. rewrite -case_ex_l. simplify.
Qed.

Lemma nfs_prepare_merge_ex_r {X} φ Γ1 Γ2 Δ1 Δ2 P1 P2 R:
  once_tc (∀! x: X, nfs_prepare_merge φ Γ1 Γ2 Δ1 Δ2 P1 (P2 x) (R x)) →
  nfs_prepare_merge φ Γ1 Γ2 Δ1 Δ2 P1 (∃ x: X, P2 x) (∃ x, R x).
Proof.
  rewrite /nfs_prepare_merge. rewrite nfs_exists.
  intros ?. rewrite -case_ex_r. simplify.
Qed.

Lemma nfs_prepare_merge_nfs_l φ Γ1 Γ1' Γ1'' Γ2 Δ1 Δ1' Δ1'' Δ2 P1 P2 R :
  Simpl (Γ1' ++ Γ1'') Γ1 →
  Simpl (Δ1' ++ Δ1'') Δ1 →
  once_tc (nfs_prepare_merge φ Γ1 Γ2 Δ1 Δ2 P1 P2 R) →
  nfs_prepare_merge φ Γ1' Γ2 Δ1' Δ2 (nfs Γ1'' Δ1'' P1) P2 R.
Proof.
  rewrite /once_tc. intros <- <-. rewrite /nfs_prepare_merge /case.
  destruct decide; simplify.
  rewrite nfs_app //.
Qed.

Lemma nfs_prepare_merge_nfs_r φ Γ1 Γ2 Γ2' Γ2'' Δ1 Δ2 Δ2' Δ2'' P1 P2 R :
  Simpl (Γ2' ++ Γ2'') Γ2 →
  Simpl (Δ2' ++ Δ2'') Δ2 →
  once_tc (nfs_prepare_merge φ Γ1 Γ2 Δ1 Δ2 P1 P2 R) →
  nfs_prepare_merge φ Γ1 Γ2' Δ1 Δ2' P1 (nfs Γ2'' Δ2'' P2) R.
Proof.
  rewrite /once_tc. intros <- <-. rewrite /nfs_prepare_merge /case.
  destruct decide; simplify.
  rewrite nfs_app //.
Qed.

Lemma nfs_prepare_merge_process_assertions φ Γ1 Γ2 Δ1 Δ2 P1 P2 R :
  once_tc (nfs_prepare_merge_process φ Γ1 Γ2 Δ1 Δ2 P1 P2 nil nil R) →
  nfs_prepare_merge φ Γ1 Γ2 Δ1 Δ2 P1 P2 R.
Proof.
  rewrite /nfs_prepare_merge_process /nfs_prepare_merge /case.
  destruct decide; eapply simplify_trans, simplify_ent.
  all: iIntros "H"; by iApply "H".
Qed.

(* PROCESSING ASSERTIONS *)
Lemma nfs_prepare_merge_process_ltr_match_l φ l A B Δ1' Δ2 Δ2' P1 P2 F1 F2 R :
  once_tc (FindAtom SHALLOW Δ2 (l ◁ₗ B) Δ2') →
  once_tc (∀! C, nfs_prepare_merge_process φ [C `is_ty` A] [C `is_ty` B] Δ1' Δ2' P1 P2 F1 F2 (R C)) →
  nfs_prepare_merge_process φ nil nil (l ◁ₗ A :: Δ1') Δ2 P1 P2 F1 F2 (∃ C, l ◁ₗ C ∗ R C).
Proof.
  rewrite /once_tc. intros Hfind Hmerge.
  rewrite /nfs_prepare_merge_process.
  rewrite (nfs_find_atom _ Hfind) !nfs_cons_atom.
  eapply simplify_ent. iIntros "[%C [Hl HR]]".
  rewrite (ent_simplify _ _ (Hmerge C)).
  rewrite !nfs_cons_pure.
  iIntros "Hcase"; iSpecialize ("HR" with "Hcase").
  rewrite /case; destruct decide; iDestruct "HR" as "[%Hty $]".
  all: by setoid_rewrite Hty.
Qed.

Lemma nfs_prepare_merge_process_ltr_match_r `{!very_fancy_if} φ l A B Δ1 Δ1' Δ2' P1 P2 F1 F2 R :
  once_tc (FindAtom SHALLOW Δ1 (l ◁ₗ A) Δ1') →
  once_tc (∀! C, nfs_prepare_merge_process φ [C `is_ty` A] [C `is_ty` B] Δ1' Δ2' P1 P2 F1 F2 (R C)) →
  nfs_prepare_merge_process φ nil nil Δ1 (l ◁ₗ B :: Δ2') P1 P2 F1 F2 (∃ C, l ◁ₗ C ∗ R C).
Proof.
  rewrite /once_tc. intros Hfind Hmerge.
  rewrite /nfs_prepare_merge_process.
  rewrite (nfs_find_atom _ Hfind) !nfs_cons_atom.
  eapply simplify_ent. iIntros "[%C [Hl HR]]".
  rewrite (ent_simplify _ _ (Hmerge C)).
  rewrite !nfs_cons_pure.
  iIntros "Hcase"; iSpecialize ("HR" with "Hcase").
  rewrite /case; destruct decide; iDestruct "HR" as "[%Hty $]".
  all: by setoid_rewrite Hty.
Qed.

Lemma nfs_prepare_merge_process_vtr_match_l φ v A B Δ1' Δ2 Δ2' P1 P2 F1 F2 R :
  once_tc (FindAtom SHALLOW Δ2 (v ◁ᵥ B) Δ2') →
  once_tc (∀! C, nfs_prepare_merge_process φ [C `is_ty` A] [C `is_ty` B] Δ1' Δ2' P1 P2 F1 F2 (R C)) →
  nfs_prepare_merge_process φ nil nil (v ◁ᵥ A :: Δ1') Δ2 P1 P2 F1 F2 (∃ C, v ◁ᵥ C ∗ R C).
Proof.
  rewrite /once_tc. intros Hfind Hmerge.
  rewrite /nfs_prepare_merge_process.
  rewrite (nfs_find_atom _ Hfind) !nfs_cons_atom.
  eapply simplify_ent. iIntros "[%C [Hl HR]]".
  rewrite (ent_simplify _ _ (Hmerge C)).
  rewrite !nfs_cons_pure.
  iIntros "Hcase"; iSpecialize ("HR" with "Hcase").
  rewrite /case; destruct decide; iDestruct "HR" as "[%Hty $]".
  all: by setoid_rewrite Hty.
Qed.

Lemma nfs_prepare_merge_process_vtr_match_r `{!very_fancy_if} φ v A B Δ1 Δ1' Δ2' P1 P2 F1 F2 R :
  once_tc (FindAtom SHALLOW Δ1 (v ◁ᵥ A) Δ1') →
  once_tc (∀! C, nfs_prepare_merge_process φ [C `is_ty` A] [C `is_ty` B] Δ1' Δ2' P1 P2 F1 F2 (R C)) →
  nfs_prepare_merge_process φ nil nil Δ1 (v ◁ᵥ B :: Δ2') P1 P2 F1 F2 (∃ C, v ◁ᵥ C ∗ R C).
Proof.
  rewrite /once_tc. intros Hfind Hmerge.
  rewrite /nfs_prepare_merge_process.
  rewrite (nfs_find_atom _ Hfind) !nfs_cons_atom.
  eapply simplify_ent. iIntros "[%C [Hl HR]]".
  rewrite (ent_simplify _ _ (Hmerge C)).
  rewrite !nfs_cons_pure.
  iIntros "Hcase"; iSpecialize ("HR" with "Hcase").
  rewrite /case; destruct decide; iDestruct "HR" as "[%Hty $]".
  all: by setoid_rewrite Hty.
Qed.

Lemma nfs_prepare_merge_process_predicate_match_l φ I O p i o1 o2 Δ1' Δ2 Δ2' P1 P2 F1 F2 R :
  sequence_classes [Predicate I O p; FindAtom SHALLOW Δ2 (p i o2) Δ2'] →
  once_tc (nfs_prepare_merge_process φ nil nil Δ1' Δ2' P1 P2 F1 F2 R) →
  nfs_prepare_merge_process φ nil nil (p i o1 :: Δ1') Δ2 P1 P2 F1 F2 (⌜o1 = o2⌝ ∗ p i o1 ∗ R).
Proof.
  rewrite /sequence_classes !Forall_cons /id. intros (Hpred & Hfind &_) Hmerge.
  rewrite /nfs_prepare_merge_process.
  rewrite (nfs_find_atom _ Hfind) !nfs_cons_atom.
  eapply simplify_ent. iIntros "[-> [Hp HR]]".
  rewrite (ent_simplify _ _ Hmerge).
  iIntros "Hcase"; iSpecialize ("HR" with "Hcase").
  rewrite /case; destruct decide; by iDestruct "HR" as "$".
Qed.

Lemma nfs_prepare_merge_process_predicate_match_r `{!very_fancy_if} φ I O p i o1 o2 Δ1 Δ1' Δ2' P1 P2 F1 F2 R :
  sequence_classes [
    Predicate I O p;
    FindAtom SHALLOW Δ1 (p i o1) Δ1'
  ] →
  once_tc (nfs_prepare_merge_process φ nil nil Δ1' Δ2' P1 P2 F1 F2 R) →
  nfs_prepare_merge_process φ nil nil Δ1 (p i o2 :: Δ2') P1 P2 F1 F2 (⌜o1 = o2⌝ ∗ p i o2 ∗ R).
Proof.
  rewrite /sequence_classes !Forall_cons /id. intros (Hpred & Hfind & _) Hmerge.
  rewrite /nfs_prepare_merge_process.
  rewrite (nfs_find_atom _ Hfind) !nfs_cons_atom.
  eapply simplify_ent. iIntros "[-> [Hp HR]]".
  rewrite (ent_simplify _ _ Hmerge).
  iIntros "Hcase"; iSpecialize ("HR" with "Hcase").
  rewrite /case; destruct decide; by iDestruct "HR" as "$".
Qed.

Lemma nfs_prepare_merge_process_missing_atom_r `{!very_fancy_if} φ A Δ1 Δ2 P1 P2 F1 F2 R :
  once_tc (nfs_prepare_merge_process φ nil nil Δ1 Δ2 P1 P2 (A :: F1) F2 R) →
  nfs_prepare_merge_process φ nil nil Δ1 (A :: Δ2) P1 P2 F1 F2 (A ∗ R).
Proof.
  rewrite /once_tc. intros Hmerge.
  rewrite /nfs_prepare_merge_process.
  rewrite !nfs_cons_atom.
  eapply simplify_ent. iIntros "[A HR]".
  rewrite (ent_simplify _ _ Hmerge) /=.
  iIntros "Hcase".
  rewrite /case; destruct decide.
  - iApply "HR". iFrame.
  - iFrame. by iApply "HR".
Qed.

Lemma nfs_prepare_merge_process_missing_atom_l `{!very_fancy_if} φ A Δ1 Δ2 P1 P2 F1 F2 R :
  once_tc (nfs_prepare_merge_process φ nil nil Δ1 Δ2 P1 P2 F1 (A :: F2) R) →
  nfs_prepare_merge_process φ nil nil (A :: Δ1) Δ2 P1 P2 F1 F2 (A ∗ R).
Proof.
  rewrite /once_tc. intros Hmerge.
  rewrite /nfs_prepare_merge_process.
  rewrite !nfs_cons_atom.
  eapply simplify_ent. iIntros "[A HR]".
  rewrite (ent_simplify _ _ Hmerge) /=.
  iIntros "Hcase".
  rewrite /case; destruct decide.
  - iFrame. by iApply "HR".
  - iApply "HR". iFrame.
Qed.


Lemma nfs_prepare_merge_process_pure_is_ty_l_match φ A B C Γ1 Γ2 Γ2' Δ1 Δ2 P1 P2 F1 F2 R :
  once_tc (FindPureAtom Γ2 (A `is_ty` C) Γ2') →
  once_tc (nfs_prepare_merge_process φ Γ1 Γ2' Δ1 Δ2 P1 P2 F1 F2 R) →
  nfs_prepare_merge_process φ (A `is_ty` B :: Γ1) Γ2 Δ1 Δ2 P1 P2 F1 F2 (⌜types_intersect φ B C A⌝ ∗ R).
Proof.
  rewrite /once_tc /nfs_prepare_merge_process.
  intros Hfind Hmerge.
  rewrite (nfs_find_pure Hfind).
  rewrite !nfs_cons_pure. revert Hmerge.
  rewrite -!simplify_ent_iff.
  rewrite /types_intersect /joined_prop /case; destruct decide.
  - intros ->. by iIntros "[$ Hcont]".
  - intros ->. by iIntros "[$ Hcont]".
Qed.

Lemma nfs_prepare_merge_process_pure_is_ty_r_match φ A B C Γ1 Γ2 Γ1' Δ1 Δ2 P1 P2 F1 F2 R :
  once_tc (FindPureAtom Γ1 (A `is_ty` B) Γ1') →
  once_tc (nfs_prepare_merge_process φ Γ1' Γ2 Δ1 Δ2 P1 P2 F1 F2 R) →
  nfs_prepare_merge_process φ Γ1 (A `is_ty` C :: Γ2) Δ1 Δ2 P1 P2 F1 F2 (⌜types_intersect φ B C A⌝ ∗ R).
Proof.
  rewrite /once_tc /nfs_prepare_merge_process.
  intros Hfind Hmerge.
  rewrite (nfs_find_pure Hfind).
  rewrite !nfs_cons_pure. revert Hmerge.
  rewrite -!simplify_ent_iff.
  rewrite /types_intersect /joined_prop /case; destruct decide.
  - intros ->. by iIntros "[$ Hcont]".
  - intros ->. by iIntros "[$ Hcont]".
Qed.

Lemma nfs_prepare_merge_process_pure_conditional_l φ ψ Γ1 Γ2 Δ1 Δ2 P1 P2 F1 F2 R :
  once_tc (nfs_prepare_merge_process φ Γ1 Γ2 Δ1 Δ2 P1 P2 F1 F2 R) →
  nfs_prepare_merge_process φ (ψ :: Γ1) Γ2 Δ1 Δ2 P1 P2 F1 F2 (⌜then_prop φ ψ⌝ ∗ R).
Proof.
  rewrite /once_tc /nfs_prepare_merge_process.
  rewrite !nfs_cons_pure.
  rewrite -!simplify_ent_iff.
  rewrite /then_prop /case; destruct decide.
  - intros ->. iIntros "[%Hφ Hcont] F1".
    iDestruct ("Hcont" with "F1") as "$".
    iPureIntro; naive_solver.
  - intros ->. iIntros "[%Hφ Hcont] F2".
    iDestruct ("Hcont" with "F2") as "$".
Qed.

Lemma nfs_prepare_merge_process_pure_conditional_r φ ψ Γ1 Γ2 Δ1 Δ2 P1 P2 F1 F2 R :
  once_tc (nfs_prepare_merge_process φ Γ1 Γ2 Δ1 Δ2 P1 P2 F1 F2 R) →
  nfs_prepare_merge_process φ Γ1 (ψ :: Γ2) Δ1 Δ2 P1 P2 F1 F2 (⌜else_prop φ ψ⌝ ∗ R).
Proof.
  rewrite /once_tc /nfs_prepare_merge_process.
  rewrite !nfs_cons_pure.
  rewrite -!simplify_ent_iff.
  rewrite /else_prop /case; destruct decide.
  - intros ->. iIntros "[%Hφ Hcont] F1".
    iDestruct ("Hcont" with "F1") as "$".
  - intros ->. iIntros "[%Hφ Hcont] F2".
    iDestruct ("Hcont" with "F2") as "$".
    iPureIntro; naive_solver.
Qed.

Lemma nfs_prepare_merge_process_done φ P1 P2 F1 F2 R :
  unify (case φ (nfw nil F1 P1) (nfw nil F2 P2)) R →
  nfs_prepare_merge_process φ nil nil nil nil P1 P2 F1 F2 R.
Proof.
  rewrite /unify. intros <-.
  rewrite /nfs_prepare_merge_process !nfs_empty /nfw /= l2p_nil.
  rewrite !left_id. rewrite /case; destruct decide; simplify.
Qed.

(* JOINING TYPES IN THE PRECONDITION *)
Lemma join_types_precond_ex {X: Type} (P: X → iProp Σ) R :
  once_tc (∀! x, join_types_precond (P x) (R x)) →
  join_types_precond (∃ x: X, P x) (∃ x: X, R x).
Proof.
  rewrite /join_types_precond. simplify.
Qed.

Lemma join_types_precond_join_types φ A B C P Q R :
  sequence_classes [
    types_intersect_pre φ A B C Q;
    join_types_precond P R
   ] →
  join_types_precond (⌜types_intersect φ A B C⌝ ∗ P) (Q ∗ R).
Proof.
  rewrite /sequence_classes !Forall_cons /id /=. intros (?&?&?).
  rewrite /types_intersect_pre /join_types_precond.
  simplify.
Qed.

Lemma join_types_precond_frame F P R :
  sequence_classes [
    not_pure F;
    join_types_precond P R
   ] →
  join_types_precond (F ∗ P) (F ∗ R).
Proof.
  rewrite /sequence_classes !Forall_cons /id /=. intros (?&?&?).
  rewrite /join_types_precond. simplify.
Qed.

Lemma join_types_precond_nfs_nil Δ (P R : iProp Σ):
  once_tc (join_types_precond P R) →
  join_types_precond (nfs nil Δ P) (nfs nil Δ R).
Proof.
  rewrite /join_types_precond /nfs.
  simplify.
Qed.


Lemma join_types_precond_then_prop φ ψ P R :
  once_tc (join_types_precond P R) →
  join_types_precond (⌜then_prop φ ψ⌝ ∗ P) (⌜then_prop φ ψ⌝ ∗ R).
Proof.
  rewrite /join_types_precond. simplify.
Qed.

Lemma join_types_precond_else_prop φ ψ P R :
  once_tc (join_types_precond P R) →
  join_types_precond (⌜else_prop φ ψ⌝ ∗ P) (⌜else_prop φ ψ⌝ ∗ R).
Proof.
  rewrite /join_types_precond. simplify.
Qed.

Lemma join_types_precond_framed_prop ψ P R :
  once_tc (join_types_precond P R) →
  join_types_precond (⌜framed_prop ψ⌝ ∗ P) (⌜framed_prop ψ⌝ ∗ R).
Proof.
  rewrite /join_types_precond. simplify.
Qed.

Lemma join_types_precond_done_case φ P1 P2 :
  join_types_precond (case φ P1 P2) (case φ P1 P2).
Proof.
  rewrite /join_types_precond. simplify.
Qed.

Lemma join_types_precond_done_all {X: Type} (P: X → iProp Σ) :
  join_types_precond (∀ x, P x) (∀ x, P x).
Proof.
  rewrite /join_types_precond. simplify.
Qed.

Lemma join_types_precond_done_wand P Q :
  join_types_precond (P -∗ Q) (P -∗ Q).
Proof.
  rewrite /join_types_precond. simplify.
Qed.

Lemma join_types_precond_done_nfw Γ Δ P :
  join_types_precond (nfw Γ Δ P) (nfw Γ Δ P).
Proof.
  rewrite /join_types_precond. simplify.
Qed.

End normal_form_join_precondition.


Existing Class nf_join_pre.
Global Hint Mode nf_join_pre - ! ! ! - : typeclass_instances.

Global Existing Instance nf_join_pre_pass.

Existing Class nfs_prepare_merge.
Global Hint Mode nfs_prepare_merge - - ! ! ! ! ! ! - : typeclass_instances.

Global Existing Instance nfs_prepare_merge_ex_l | 1.
Global Existing Instance nfs_prepare_merge_ex_r | 1.
Global Existing Instance nfs_prepare_merge_nfs_l | 1.
Global Existing Instance nfs_prepare_merge_nfs_r | 1.
Global Existing Instance nfs_prepare_merge_process_assertions | 10.

Existing Class nfs_prepare_merge_process.
Global Hint Mode nfs_prepare_merge_process - - ! ! ! ! ! ! - - - : typeclass_instances.

Global Existing Instance nfs_prepare_merge_process_ltr_match_l | 1.
Global Existing Instance nfs_prepare_merge_process_ltr_match_r | 2.
Global Existing Instance nfs_prepare_merge_process_vtr_match_l | 1.
Global Existing Instance nfs_prepare_merge_process_vtr_match_r | 2.
Global Existing Instance nfs_prepare_merge_process_predicate_match_l | 1.
Global Existing Instance nfs_prepare_merge_process_predicate_match_r | 2.
Global Existing Instance nfs_prepare_merge_process_pure_conditional_l | 5.
Global Existing Instance nfs_prepare_merge_process_pure_conditional_r | 5.
Global Existing Instance nfs_prepare_merge_process_pure_is_ty_l_match | 4.
Global Existing Instance nfs_prepare_merge_process_pure_is_ty_r_match | 4.
Global Existing Instance nfs_prepare_merge_process_missing_atom_l | 10.
Global Existing Instance nfs_prepare_merge_process_missing_atom_r | 11.
Global Existing Instance nfs_prepare_merge_process_done | 1.

Existing Class join_types_precond.
Global Hint Mode join_types_precond - ! - : typeclass_instances.

Global Existing Instance join_types_precond_ex | 1.
Global Existing Instance join_types_precond_join_types | 1.
Global Existing Instance join_types_precond_frame | 1.
Global Existing Instance join_types_precond_nfs_nil | 1.
Global Existing Instance join_types_precond_then_prop | 1.
Global Existing Instance join_types_precond_else_prop | 1.
Global Existing Instance join_types_precond_framed_prop | 1.
Global Existing Instance join_types_precond_done_case | 20.
Global Existing Instance join_types_precond_done_all | 20.
Global Existing Instance join_types_precond_done_wand | 20.
Global Existing Instance join_types_precond_done_nfw | 20.



Section normal_form_join_postcondition.
Context `{!refinedcG Σ}.

Definition nf_join_post (φ: dProp) (P R: iProp Σ) :=
  simplify P R.

Definition nfw_prepare_merge
  (φ: dProp)
  (* the two nfw judgements *)
  (Γ1 Γ2: list Prop)
  (Δ1 Δ2: list (iProp Σ))
  (P1 P2: iProp Σ)
  (* the precondition we have computed *)
  (R: iProp Σ) :=
  simplify (case φ (nfw Γ1 Δ1 P1) (nfw Γ2 Δ2 P2)) R.

Definition nfw_prepare_merge_process
  (φ: dProp)
  (* the two nfs judgements that we want to merge *)
  (Γ1 Γ2: list Prop)
  (Δ1 Δ2: list (iProp Σ))
  (P1 P2: iProp Σ)
  (* the precondition we have computed *)
  Γ Δ (R: iProp Σ) :=
  simplify (case φ (nfw Γ1 Δ1 P1) (nfw Γ2 Δ2 P2)) (nfs Γ Δ R).

(* join types postcondition *)
Definition join_types_postcond (P R: iProp Σ) : Prop :=
  simplify P R.


(* we first need to get to the postcondition *)
Lemma nf_join_post_ex φ {X} (P R: X → iProp Σ):
  (∀! x, nf_join_post φ (P x) (R x)) →
  nf_join_post φ (∃ x, P x) (∃ x, R x).
Proof. rewrite /nf_join_post. simplify. Qed.

Lemma nf_join_post_nfs φ Γ Δ P R:
  nf_join_post φ P R →
  nf_join_post φ (nfs Γ Δ P) (nfs Γ Δ R).
Proof. rewrite /nf_join_post. simplify. Qed.

Lemma nf_join_post_frame φ P Q R:
  nf_join_post φ P R →
  nf_join_post φ (Q ∗ P) (Q ∗ R).
Proof. rewrite /nf_join_post. simplify. Qed.

(* now we start merging the postcondition *)
Lemma nf_join_post_done φ P1 P2 Q R:
  sequence_classes [
    nfw_prepare_merge φ nil nil nil nil P1 P2 Q;
    simplify_chain [
      join_types_postcond;
      normalize;
      elim_existentials
      ] Q R
  ] →
  nf_join_post φ (case φ P1 P2) R.
Proof.
  rewrite /nf_join_post /nfw_prepare_merge /sequence_classes.
  rewrite !Forall_cons !Forall_nil /=.
  rewrite !nfw_empty. intros (?&?&?).
  simplify.
Qed.


(* COLLECTING ASSUMPTIONS *)
(* Lemma nfw_prepare_merge_all_l {X} `{!Inhabited X} φ Γ1 Γ2 Δ1 Δ2 P1 P2 R:
  once_tc (∀! x: X, nfw_prepare_merge φ Γ1 Γ2 Δ1 Δ2 (P1 x) P2 (R x)) →
  nfw_prepare_merge φ Γ1 Γ2 Δ1 Δ2 (∀ x: X, P1 x) P2 (∀ x, R x).
Proof.
  rewrite /nfw_prepare_merge. rewrite nfw_all.
  intros ?. rewrite -case_all_l. simplify.
Qed.

Lemma nfw_prepare_merge_all_r {X} `{!Inhabited X} φ Γ1 Γ2 Δ1 Δ2 P1 P2 R:
  once_tc (∀! x: X, nfw_prepare_merge φ Γ1 Γ2 Δ1 Δ2 P1 (P2 x) (R x)) →
  nfw_prepare_merge φ Γ1 Γ2 Δ1 Δ2 P1 (∀ x: X, P2 x) (∀ x, R x).
Proof.
  rewrite /nfw_prepare_merge. rewrite nfw_all.
  intros ?. rewrite -case_all_r. simplify.
Qed. *)

Lemma nfw_prepare_merge_nfw_l φ Γ1 Γ1' Γ1'' Γ2 Δ1 Δ1' Δ1'' Δ2 P1 P2 R :
  Simpl (Γ1' ++ Γ1'') Γ1 →
  Simpl (Δ1' ++ Δ1'') Δ1 →
  once_tc (nfw_prepare_merge φ Γ1 Γ2 Δ1 Δ2 P1 P2 R) →
  nfw_prepare_merge φ Γ1' Γ2 Δ1' Δ2 (nfw Γ1'' Δ1'' P1) P2 R.
Proof.
  rewrite /once_tc. intros <- <-. rewrite /nfw_prepare_merge /case.
  destruct decide; simplify.
  rewrite nfw_app //.
Qed.

Lemma nfw_prepare_merge_nfw_r φ Γ1 Γ2 Γ2' Γ2'' Δ1 Δ2 Δ2' Δ2'' P1 P2 R :
  Simpl (Γ2' ++ Γ2'') Γ2 →
  Simpl (Δ2' ++ Δ2'') Δ2 →
  once_tc (nfw_prepare_merge φ Γ1 Γ2 Δ1 Δ2 P1 P2 R) →
  nfw_prepare_merge φ Γ1 Γ2' Δ1 Δ2' P1 (nfw Γ2'' Δ2'' P2) R.
Proof.
  rewrite /once_tc. intros <- <-. rewrite /nfw_prepare_merge /case.
  destruct decide; simplify.
  rewrite nfw_app //.
Qed.

Lemma nfw_prepare_merge_process_assumptions φ Γ1 Γ2 Δ1 Δ2 P1 P2 R :
  once_tc (nfw_prepare_merge_process φ Γ1 Γ2 Δ1 Δ2 P1 P2 nil nil R) →
  nfw_prepare_merge φ Γ1 Γ2 Δ1 Δ2 P1 P2 R.
Proof.
  rewrite /once_tc. rewrite /nfw_prepare_merge_process /nfw_prepare_merge.
  rewrite nfs_empty. simplify.
Qed.

(* PROCESSING ASSUMPTIONS *)
Lemma nfw_prepare_merge_process_pure_frame φ ψ Γ1' Γ2 Γ2' Δ1 Δ2 P1 P2 Γ Δ R :
  once_tc (FindPureAtom Γ2 ψ Γ2') →
  once_tc (nfw_prepare_merge_process φ Γ1' Γ2' Δ1 Δ2 P1 P2 (ψ :: Γ) Δ R) →
  nfw_prepare_merge_process φ (ψ :: Γ1') Γ2 Δ1 Δ2 P1 P2 Γ Δ R.
Proof.
  rewrite /once_tc /nfw_prepare_merge_process.
  rewrite /case; destruct decide.
  - intros ?. rewrite nfw_cons_pure nfs_cons_pure.
    intros Hent%ent_simplify. rewrite -Hent.
    eapply simplify_ent. iIntros "$ $".
  - intros Hfind. rewrite (nfw_find_pure Hfind).
    rewrite nfs_cons_pure. rewrite nfw_cons_pure.
    intros Hent%ent_simplify. rewrite -Hent.
    eapply simplify_ent. iIntros "$ $".
Qed.

Lemma nfw_prepare_merge_process_pure_r φ ψ Γ1 Γ2 Δ1 Δ2 P1 P2 Γ Δ R :
  once_tc (nfw_prepare_merge_process φ Γ1 Γ2 Δ1 Δ2 P1 P2 (else_prop φ ψ :: Γ) Δ R) →
  nfw_prepare_merge_process φ Γ1 (ψ :: Γ2) Δ1 Δ2 P1 P2 Γ Δ R.
Proof.
  rewrite /once_tc /nfw_prepare_merge_process.
  rewrite /case; destruct decide.
  - intros Hsimpl. eapply simplify_trans; first done.
    rewrite nfs_cons_pure. eapply simplify_ent.
    iIntros "$". iPureIntro. rewrite /else_prop. naive_solver.
  - rewrite nfs_cons_pure nfw_cons_pure.
    intros Hsimpl%ent_simplify. eapply simplify_ent.
    iIntros "Hnfs %Hψ". iApply (Hsimpl with "[$Hnfs]").
    iPureIntro. rewrite /else_prop. naive_solver.
Qed.

Lemma nfw_prepare_merge_process_pure_l φ ψ Γ1 Γ2 Δ1 Δ2 P1 P2 Γ Δ R :
  once_tc (nfw_prepare_merge_process φ Γ1 Γ2 Δ1 Δ2 P1 P2 (then_prop φ ψ :: Γ) Δ R) →
  nfw_prepare_merge_process φ (ψ :: Γ1) Γ2 Δ1 Δ2 P1 P2 Γ Δ R.
Proof.
  rewrite /once_tc /nfw_prepare_merge_process.
  rewrite /case; destruct decide.
  - rewrite nfs_cons_pure nfw_cons_pure.
    intros Hsimpl%ent_simplify. eapply simplify_ent.
    iIntros "Hnfs %Hψ". iApply (Hsimpl with "[$Hnfs]").
    iPureIntro. rewrite /then_prop. naive_solver.
  - intros Hsimpl. eapply simplify_trans; first done.
    rewrite nfs_cons_pure. eapply simplify_ent.
    iIntros "$". iPureIntro. rewrite /then_prop. naive_solver.
Qed.
(*
Lemma nfw_prepare_merge_process_pure_drop_self_right (φ: dProp) ψ Γ1 Γ2 Δ1 Δ2 P1 P2 Γ Δ R :
  unify ψ (¬ (φ: Prop)) →
  nfw_prepare_merge_process φ Γ1 Γ2 Δ1 Δ2 P1 P2 Γ Δ R →
  nfw_prepare_merge_process φ Γ1 (ψ :: Γ2) Δ1 Δ2 P1 P2 Γ Δ R.
Proof.
  intros ->. rewrite /nfw_prepare_merge_process.
  rewrite /case; destruct decide; first done.
  intros Hsimpl%ent_simplify. eapply simplify_ent. rewrite nfw_cons_pure.
  iIntros "Hnfs _". by iApply Hsimpl.
Qed.

Lemma nfw_prepare_merge_process_pure_drop_self_left (φ: dProp) ψ Γ1 Γ2 Δ1 Δ2 P1 P2 Γ Δ R :
  unify ψ (φ: Prop) →
  nfw_prepare_merge_process φ Γ1 Γ2 Δ1 Δ2 P1 P2 Γ Δ R →
  nfw_prepare_merge_process φ (ψ :: Γ1) Γ2 Δ1 Δ2 P1 P2 Γ Δ R.
Proof.
  intros ->. rewrite /nfw_prepare_merge_process.
  rewrite /case; destruct decide; last done.
  intros Hsimpl%ent_simplify. eapply simplify_ent. rewrite nfw_cons_pure.
  iIntros "Hnfs _". by iApply Hsimpl.
Qed. *)

Lemma nfw_prepare_merge_process_frame F φ Δ1' Δ2 Δ2' P1 P2 Γ Δ R :
  once_tc (FindAtom SHALLOW Δ2 F Δ2') →
  once_tc (nfw_prepare_merge_process φ nil nil Δ1' Δ2' P1 P2 Γ (F :: Δ) R) →
  nfw_prepare_merge_process φ nil nil (F :: Δ1') Δ2 P1 P2 Γ Δ R.
Proof.
  rewrite /once_tc /nfw_prepare_merge_process.
  rewrite /case; destruct decide.
  - intros ?. rewrite nfw_cons_atom nfs_cons_atom.
    intros Hent%ent_simplify. rewrite -Hent.
    eapply simplify_ent. iIntros "$ $".
  - intros Hfind. rewrite (nfw_find_atom _ Hfind).
    rewrite nfs_cons_atom. rewrite nfw_cons_atom.
    intros Hent%ent_simplify. rewrite -Hent.
    eapply simplify_ent. iIntros "$ $".
Qed.

Lemma nfw_prepare_merge_process_return_type_cont φ Φ A1 A2 Δ1 Δ2 B1 B2 Ξ1 Ξ2 Γ Δ R:
  once_tc (∀ v, complete_types φ (VAL v) A1 A2 Δ1 Δ2 B1 B2 Ξ1 Ξ2) →
  once_tc (∀! P, nfw_prepare_merge_process φ nil nil Ξ1 Ξ2 P P Γ Δ (R P)) →
  nfw_prepare_merge_process φ nil nil Δ1 Δ2 (type_cont Φ A1) (type_cont Φ A2) Γ Δ (∃ C, ⌜types_union φ B1 B2 C⌝ ∗ R (type_cont Φ C)).
Proof.
  rewrite /nfw_prepare_merge_process /complete_types.
  intros Hcompl Hnfw. rewrite nfs_exists. eapply simplify_ent.
  iIntros "[%C Hnfs]". rewrite nfs_sep. iDestruct "Hnfs" as "[%Hty Hnfs]".
  eapply ent_simplify in Hnfw.
  rewrite Hnfw /case; destruct decide.
  - rewrite /nfw /type_cont. iIntros "HΓ HΔ". iIntros (v) "Hv".
    iPoseProof (Hcompl) as "Hcompl".
    rewrite /types_union /joined_prop /case in Hty.
    rewrite /case; destruct decide; last naive_solver.
    iDestruct ("Hcompl" with "[$Hv $HΔ]") as "[Hv HΞ]"; simpl.
    setoid_rewrite Hty. iApply ("Hnfs" with "[//] [$HΞ] [$Hv]").
  - rewrite /nfw /type_cont. iIntros "HΓ HΔ". iIntros (v) "Hv".
    iPoseProof (Hcompl) as "Hcompl".
    rewrite /types_union /joined_prop /case in Hty.
    rewrite /case; destruct decide; first naive_solver.
    iDestruct ("Hcompl" with "[$Hv $HΔ]") as "[Hv HΞ]"; simpl.
    setoid_rewrite Hty. iApply ("Hnfs" with "[//] [$HΞ] [$Hv]").
Qed.

Lemma nfw_prepare_merge_process_return_type_post φ Φ A1 A2 Δ1 Δ2 B1 B2 Ξ1 Ξ2 Γ Δ R:
  once_tc (∀ v, complete_types φ (VAL v) A1 A2 Δ1 Δ2 B1 B2 Ξ1 Ξ2) →
  once_tc (∀! P, nfw_prepare_merge_process φ nil nil Ξ1 Ξ2 P P Γ Δ (R P)) →
  nfw_prepare_merge_process φ nil nil Δ1 Δ2 (type_post Φ A1) (type_post Φ A2) Γ Δ (∃ C, ⌜types_union φ B1 B2 C⌝ ∗ R (type_post Φ C)).
Proof.
  rewrite /type_post. intros ??. by eapply nfw_prepare_merge_process_return_type_cont.
Qed.

Lemma nfw_prepare_merge_process_ltr l A1 A2 φ Δ1 Δ2 Δ2' Ξ1 Ξ2 B1 B2 P1 P2 Γ Δ R :
  once_tc (FindAtom SHALLOW Δ2 (l ◁ₗ A2) Δ2') →
  once_tc (complete_types φ (LOC l) A1 A2 Δ1 Δ2' B1 B2 Ξ1 Ξ2) →
  once_tc (∀! C, nfw_prepare_merge_process φ nil nil Ξ1 Ξ2 P1 P2 Γ (l ◁ₗ C :: Δ) (R C)) →
  nfw_prepare_merge_process φ nil nil (l ◁ₗ A1 :: Δ1) Δ2 P1 P2 Γ Δ (∃ C, ⌜types_union φ B1 B2 C⌝ ∗ R C).
Proof.
  rewrite /once_tc /nfw_prepare_merge_process /complete_types.
  intros Hfind2 Hcompl Hnfw. rewrite nfs_exists. eapply simplify_ent.
  iIntros "[%C Hnfs]". rewrite nfs_sep. iDestruct "Hnfs" as "[%Hty Hnfs]".
  rewrite (nfw_find_atom _ Hfind2).
  iPoseProof (Hcompl) as "Hcompl". eapply ent_simplify in Hnfw.
  rewrite nfs_cons_atom in Hnfw. iPoseProof (Hnfw) as "Hnfw".
  rewrite /case; destruct decide.
  - rewrite /nfw. iIntros "HΓ [Hl HΔ]". iDestruct ("Hcompl" with "[$Hl $HΔ]") as "[Hl HΞ]"; simpl.
    rewrite /types_union /joined_prop /case in Hty.
    rewrite /case; destruct decide; last naive_solver.
    setoid_rewrite Hty. iApply ("Hnfw" with "[$Hnfs $Hl] [//] [$HΞ]").
  - rewrite /nfw. iIntros "HΓ [Hl HΔ]". iDestruct ("Hcompl" with "[$Hl $HΔ]") as "[Hl HΞ]"; simpl.
    rewrite /types_union /joined_prop /case in Hty.
    rewrite /case; destruct decide; first naive_solver.
    setoid_rewrite Hty. iApply ("Hnfw" with "[$Hnfs $Hl] [//] [$HΞ]").
Qed.

Lemma nfw_prepare_merge_process_vtr v A1 A2 φ Δ1 Δ2 Δ2' Ξ1 Ξ2 B1 B2 P1 P2 Γ Δ R :
  once_tc (FindAtom SHALLOW Δ2 (v ◁ᵥ A2) Δ2') →
  once_tc (complete_types φ (VAL v) A1 A2 Δ1 Δ2' B1 B2 Ξ1 Ξ2) →
  once_tc (∀! C, nfw_prepare_merge_process φ nil nil Ξ1 Ξ2 P1 P2 Γ (v ◁ᵥ C :: Δ) (R C)) →
  nfw_prepare_merge_process φ nil nil (v ◁ᵥ A1 :: Δ1) Δ2 P1 P2 Γ Δ (∃ C, ⌜types_union φ B1 B2 C⌝ ∗ R C).
Proof.
  rewrite /once_tc /nfw_prepare_merge_process /complete_types.
  intros Hfind2 Hcompl Hnfw. rewrite nfs_exists. eapply simplify_ent.
  iIntros "[%C Hnfs]". rewrite nfs_sep. iDestruct "Hnfs" as "[%Hty Hnfs]".
  rewrite (nfw_find_atom _ Hfind2).
  iPoseProof (Hcompl) as "Hcompl". eapply ent_simplify in Hnfw.
  rewrite nfs_cons_atom in Hnfw. iPoseProof (Hnfw) as "Hnfw".
  rewrite /case; destruct decide.
  - rewrite /nfw. iIntros "HΓ [Hl HΔ]". iDestruct ("Hcompl" with "[$Hl $HΔ]") as "[Hl HΞ]"; simpl.
    rewrite /types_union /joined_prop /case in Hty.
    rewrite /case; destruct decide; last naive_solver.
    setoid_rewrite Hty. iApply ("Hnfw" with "[$Hnfs $Hl] [//] [$HΞ]").
  - rewrite /nfw. iIntros "HΓ [Hl HΔ]". iDestruct ("Hcompl" with "[$Hl $HΔ]") as "[Hl HΞ]"; simpl.
    rewrite /types_union /joined_prop /case in Hty.
    rewrite /case; destruct decide; first naive_solver.
    setoid_rewrite Hty. iApply ("Hnfw" with "[$Hnfs $Hl] [//] [$HΞ]").
Qed.

Lemma nfw_prepare_merge_process_pred I O p i o1 o2 φ Δ1 Δ2 Δ2' P1 P2 Γ Δ R :
  once_tc (Predicate I O p) →
  once_tc (FindAtom SHALLOW Δ2 (p i o2) Δ2') →
  once_tc (nfw_prepare_merge_process φ nil nil Δ1 Δ2' P1 P2 Γ (p i o1 :: Δ) R) →
  nfw_prepare_merge_process φ nil nil (p i o1 :: Δ1) Δ2 P1 P2 Γ Δ (⌜o1 = o2⌝ ∗ R).
Proof.
  rewrite /once_tc /nfw_prepare_merge_process /complete_types.
  intros Hpred Hfind2 Hnfw. eapply simplify_ent.
  iIntros "Hnfs". rewrite nfs_sep. iDestruct "Hnfs" as "[-> Hnfs]".
  rewrite (nfw_find_atom _ Hfind2). eapply ent_simplify in Hnfw.
  rewrite nfs_cons_atom in Hnfw. iPoseProof (Hnfw) as "Hnfw".
  rewrite /case; destruct decide.
  - rewrite /nfw. iIntros "HΓ [Hl HΔ]".
    iApply ("Hnfw" with "[$Hnfs $Hl] [//] [$HΔ]").
  - rewrite /nfw. iIntros "HΓ [Hl HΔ]".
    iApply ("Hnfw" with "[$Hnfs $Hl] [//] [$HΔ]").
Qed.

Lemma nfw_prepare_merge_process_done φ P Γ Δ :
  nfw_prepare_merge_process φ nil nil nil nil P P Γ Δ (nfw Γ Δ P).
Proof.
  rewrite /nfw_prepare_merge_process. eapply simplify_ent.
  rewrite /nfs /nfw.
  iIntros "(HΓ & HΔ & Hcont)". iSpecialize ("Hcont" with "HΓ HΔ").
  rewrite /case; destruct decide; simpl; by iFrame.
Qed.


(* join types in the postcondition *)
Lemma join_types_postcond_done Γ Δ P:
  join_types_postcond (nfw Γ Δ P) (nfw Γ Δ P).
Proof.
  rewrite /join_types_postcond.
  simplify.
Qed.

Lemma join_types_postcond_exists {A: Type} (P R: A → iProp Σ) :
  once_tc (∀! x, join_types_postcond (P x) (R x)) →
  join_types_postcond (∃ x, P x) (∃ x, R x).
Proof. rewrite /join_types_postcond. simplify. Qed.

Lemma join_types_postcond_forall {A: Type} (P R: A → iProp Σ) :
  once_tc (∀! x, join_types_postcond (P x) (R x)) →
  join_types_postcond (∀ x, P x) (∀ x, R x).
Proof. rewrite /join_types_postcond. simplify. Qed.

Lemma join_types_postcond_join_types_sup φ A1 A2 B P Q R:
  sequence_classes [
    types_union_pre φ A1 A2 B Q;
    join_types_postcond P R
  ] →
  join_types_postcond (⌜types_union φ A1 A2 B⌝ ∗ P) (Q ∗ R).
Proof.
  rewrite /sequence_classes !Forall_cons /id. intros (Hent&Hsimpl&?).
  rewrite /join_types_postcond /types_union_pre.
  simplify.
Qed.

End normal_form_join_postcondition.


Existing Class nf_join_post.
Global Hint Mode nf_join_post - ! ! - : typeclass_instances.

Global Existing Instance nf_join_post_ex | 1.
Global Existing Instance nf_join_post_nfs | 1.
Global Existing Instance nf_join_post_frame | 1.
Global Existing Instance nf_join_post_done | 2.

Existing Class nfw_prepare_merge.
Global Hint Mode nfw_prepare_merge - - - - - - ! ! - : typeclass_instances.

Global Existing Instance nfw_prepare_merge_nfw_l | 1.
Global Existing Instance nfw_prepare_merge_nfw_r | 1.
Global Existing Instance nfw_prepare_merge_process_assumptions | 10.


Existing Class nfw_prepare_merge_process.
Global Hint Mode nfw_prepare_merge_process - - ! ! ! ! ! ! - - - : typeclass_instances.

Global Existing Instance nfw_prepare_merge_process_pure_frame | 1.
Global Existing Instance nfw_prepare_merge_process_pure_r | 3.
Global Existing Instance nfw_prepare_merge_process_pure_l | 3.

Global Existing Instance nfw_prepare_merge_process_frame | 1.
Global Existing Instance nfw_prepare_merge_process_ltr | 2.
Global Existing Instance nfw_prepare_merge_process_vtr | 2.
Global Existing Instance nfw_prepare_merge_process_pred | 2.
Global Existing Instance nfw_prepare_merge_process_return_type_cont | 1.
Global Existing Instance nfw_prepare_merge_process_return_type_post | 1.
Global Existing Instance nfw_prepare_merge_process_done | 1.


Existing Class join_types_postcond.
Global Hint Mode join_types_postcond - ! - : typeclass_instances.

Global Existing Instance join_types_postcond_done | 1.
Global Existing Instance join_types_postcond_exists | 1.
Global Existing Instance join_types_postcond_forall | 1.
Global Existing Instance join_types_postcond_join_types_sup | 1.



Section backpropagate_postcond.
Context `{!refinedcG Σ}.

Definition nf_join_backpropagate (P R: iProp Σ) : Prop :=
  simplify P R.


(* next pass lifts the existential quantifiers and preconditions
   from underneath the universal quantifiers *)
Definition sort_quantifiers (P R: iProp Σ) : Prop :=
  simplify P R.

Definition pre_post (E: tele) (Pre : E -t> iProp Σ) (A: tele) (Post: E -t> A -t> iProp Σ) : iProp Σ :=
  ∃.. (x: E), tele_app Pre x ∗ ∀.. (y: A), tele_app (tele_app Post x) y.

Definition separate_pre_post (P : iProp Σ) (E: tele) (A: tele)
  (Pre : E -t> iProp Σ) (Post: E -t> A -t> iProp Σ)  :=
  simplify P (pre_post E Pre A Post).

Definition pre_post_unfold (E: tele) (Pre : E -t> iProp Σ) (A: tele) (Post: E -t> A -t> iProp Σ) (R: iProp Σ) :=
  simplify (pre_post E Pre A Post) R.

Definition merge_then_else Γ Δ (P R : iProp Σ) :=
  simplify (nfs Γ Δ P) R.

Definition nf_join_unwind (P R : iProp Σ) :=
  simplify P R.

Global Instance is_simplify_merge_then_else:
  is_simplify (merge_then_else nil nil).
Proof.
  rewrite /merge_then_else /is_simplify.
  intros P Q. rewrite nfs_empty //.
Qed.

Global Instance is_simplify_join_types_precond :
  is_simplify (join_types_precond: iProp Σ → iProp Σ → Prop).
Proof. rewrite /join_types_precond. apply _. Qed.


Lemma nf_join_backpropagate_pass P R:
  simplify_chain [
    sort_quantifiers;
    merge_then_else nil nil;
    join_types_precond;
    normalize;
    nf_join_unwind;
    normalize;
    elim_existentials
  ] P R →
  nf_join_backpropagate P R.
Proof.
  rewrite /nf_join_backpropagate. simplify.
Qed.

Lemma sort_quantifiers_pass P E Pre A Post Q R:
  sequence_classes [
    separate_pre_post P E A Pre Post;
    pre_post_unfold E Pre A Post Q;
    normalize Q R
  ] →
  sort_quantifiers P R.
Proof.
  rewrite /sequence_classes !Forall_cons Forall_nil /=.
  rewrite /sort_quantifiers /separate_pre_post /pre_post_unfold /normalize.
  intros [Hsep [Hnorm [Hsimpl _]]]. simplify.
Qed.

Lemma separate_pre_post_ex {X: Type} (P: X → iProp Σ)
  (E: tele) (A: tele) (Pre : X → E -t> iProp Σ) (Post: X → (E -t> A -t> iProp Σ)) :
  (∀! x: X, separate_pre_post (P x) E A (Pre x) (Post x)) →
  separate_pre_post (∃ x, P x) (TeleS (λ _, E)) A Pre Post.
Proof.
  rewrite /separate_pre_post /pre_post.
  intros Hsimpl. simplify.
Qed.

Lemma separate_pre_post_all {X: Type} (P: X → iProp Σ)
  (E: tele) (A: tele) (Pre : E -t> iProp Σ) (Post: X → (E -t> A -t> iProp Σ)) :
  (∀! x: X, separate_pre_post (P x) E A Pre (Post x)) →
  separate_pre_post (∀ x, P x) E (TeleS (λ _, A)) Pre (tele_bind (λ e, λ x, (tele_app (Post x) e))).
Proof.
  rewrite /separate_pre_post /pre_post.
  intros Hsimpl. eapply simplify_ent.
  iIntros "Hfin". iIntros (x).
  iDestruct "Hfin" as "[%e [Hpre Hpost]]".
  iSpecialize ("Hpost" $! x).
  eapply ent_simplify in Hsimpl. iApply Hsimpl.
  iExists e. iFrame. iIntros (a).
  iPoseProof ("Hpost" $! a) as "Hpost".
  rewrite tele_app_bind //.
Qed.

Lemma separate_pre_post_sep (P Q: iProp Σ)
  (E: tele) (A: tele) (Pre : E -t> iProp Σ) (Post: E -t> A -t> iProp Σ) :
  separate_pre_post Q E A Pre Post →
  separate_pre_post (P ∗ Q) E A (tele_bind (λ e, (P ∗ (tele_app Pre e))%I)) Post.
Proof.
  rewrite /separate_pre_post /pre_post.
  intros Hsimpl. eapply simplify_ent.
  iIntros "[%e Hfin]". rewrite tele_app_bind.
  iDestruct "Hfin" as "[[HP Hpre] HQ]".
  iFrame. eapply ent_simplify in Hsimpl. iApply Hsimpl.
  iExists _. iFrame.
Qed.

Lemma separate_pre_post_wand (P Q: iProp Σ)
  (E: tele) (A: tele) (Pre : E -t> iProp Σ) (Post: E -t> A -t> iProp Σ) :
  separate_pre_post Q E A Pre Post →
  separate_pre_post (P -∗ Q) E A Pre (tele_bind (λ e, tele_bind (λ a, P -∗ tele_app (tele_app Post e) a)%I)).
Proof.
  rewrite /separate_pre_post /pre_post.
  intros Hsimpl. eapply simplify_ent.
  iIntros "[%e Hfin] HP". rewrite tele_app_bind.
  iDestruct "Hfin" as "[Hpre HQ]".
  eapply ent_simplify in Hsimpl. iApply Hsimpl.
  iExists _. iFrame. iIntros (a).
  iSpecialize ("HQ" $! a). rewrite tele_app_bind.
  by iApply "HQ".
Qed.

Lemma separate_pre_post_nfs Γ Δ (Q: iProp Σ)
  (E: tele) (A: tele) (Pre : E -t> iProp Σ) (Post: E -t> A -t> iProp Σ) :
  separate_pre_post Q E A Pre Post →
  separate_pre_post (nfs Γ Δ Q) E A (tele_bind (λ e, (nfs Γ Δ (tele_app Pre e))%I)) Post.
Proof.
  rewrite /separate_pre_post /pre_post.
  intros Hsimpl. eapply simplify_ent.
  iIntros "[%e Hfin]". rewrite tele_app_bind.
  rewrite (nfs_borrow _ _ Q) nfs_borrow.
  iDestruct "Hfin" as "[[Hpre $] Hcont]".
  iFrame. eapply ent_simplify in Hsimpl. iApply Hsimpl.
  iExists _. iFrame.
Qed.

Lemma separate_pre_post_nfw Γ Δ (Q: iProp Σ)
  (E: tele) (A: tele) (Pre : E -t> iProp Σ) (Post: E -t> A -t> iProp Σ) :
  separate_pre_post Q E A Pre Post →
  separate_pre_post (nfw Γ Δ Q) E A Pre (tele_bind (λ e, tele_bind (λ a, nfw Γ Δ (tele_app (tele_app Post e) a))%I)).
Proof.
  rewrite /separate_pre_post /pre_post.
  intros Hsimpl. eapply simplify_ent. rewrite nfw_borrow.
  iIntros "[%e Hfin] P". rewrite tele_app_bind.
  iDestruct "Hfin" as "[Hpre HQ]".
  eapply ent_simplify in Hsimpl. iApply Hsimpl.
  iExists _. iFrame. iIntros (a).
  iSpecialize ("HQ" $! a). rewrite tele_app_bind.
  rewrite nfw_borrow. by iApply "HQ".
Qed.

Lemma separate_pre_post_done P :
  separate_pre_post P TeleO TeleO emp%I P.
Proof.
  rewrite /separate_pre_post /pre_post /=.
  eapply simplify_ent. iIntros "[_ $]".
Qed.



Lemma pre_post_unfold_exists {X} (E: X → tele) Pre A Post R :
  (∀! x, pre_post_unfold (E x) (Pre x) A (Post x) (R x)) →
  pre_post_unfold (TeleS E) Pre A Post (∃ x, R x).
Proof.
  rewrite /pre_post_unfold /pre_post /=.
  intros Hsi. eapply simplify_ent. iIntros "[%x R]".
  iExists x. eapply ent_simplify in Hsi. rewrite Hsi //.
Qed.

Lemma pre_post_unfold_sep Pre A Post R :
  pre_post_unfold TeleO emp%I A Post R →
  pre_post_unfold TeleO Pre A Post (Pre ∗ R).
Proof.
  rewrite /pre_post_unfold /pre_post /=.
  intros Hsi. eapply simplify_ent. iIntros "[$ R]".
  eapply ent_simplify in Hsi. rewrite Hsi //.
  iDestruct "R" as "[_ $]".
Qed.

Lemma pre_post_unfold_forall {X} (A: X → tele) Post R :
  (∀! y, pre_post_unfold TeleO emp%I (A y) (Post y) (R y)) →
  pre_post_unfold TeleO emp%I (TeleS A) Post (∀ y, R y).
Proof.
  rewrite /pre_post_unfold /pre_post /=.
  intros Hsi. eapply simplify_ent. iIntros "R". iSplitR; first done.
  iIntros (y). eapply ent_simplify in Hsi. iSpecialize ("R" $! y).
  rewrite Hsi //.
  iDestruct "R" as "[_ R]". iApply "R".
Qed.

Lemma pre_post_unfold_wand P:
  pre_post_unfold TeleO emp%I TeleO P P.
Proof.
  rewrite /pre_post_unfold /pre_post /=.
  eapply simplify_ent. iIntros "R". iFrame.
Qed.



(* the next pass goes through the precondition and tries to
   find and merge then-else pairs  *)
Definition merge_then_else_process Γ Δ (P R : iProp Σ) :=
  simplify (nfs Γ Δ P) R.

Lemma merge_then_else_ex {X: Type} (P R: X → iProp Σ) (Γ : list Prop) (Δ: list (iProp Σ)) :
  (∀! x: X, merge_then_else Γ Δ (P x) (R x)) →
  merge_then_else Γ Δ (∃ x, P x) (∃ x, R x).
Proof.
  rewrite /merge_then_else.
  rewrite nfs_exists. simplify.
Qed.

Lemma merge_then_else_nfs Γ1 Γ2 Γ Δ1 Δ2 Δ P R :
  Simpl (Γ1 ++ Γ2) Γ →
  Simpl (Δ1 ++ Δ2) Δ →
  merge_then_else Γ Δ P R →
  merge_then_else Γ1 Δ1 (nfs Γ2 Δ2 P) R.
Proof.
  intros <- <-. rewrite /merge_then_else.
  rewrite nfs_app. simplify.
Qed.

Lemma join_then_else_prop φ P1 P2:
  ⌜joined_prop φ P1 P2⌝ ⊣⊢@{iProp Σ} ⌜then_prop φ P1⌝ ∗ ⌜else_prop φ P2⌝.
Proof.
  rewrite /joined_prop /then_prop /else_prop /case.
  destruct decide; simpl.
  all: iSplit; iIntros "%Hφ"; iPureIntro; naive_solver.
Qed.

Lemma merge_then_else_find_else φ B1 B2 A Γ1 Γ2 Δ P R :
  once_tc (FindPureAtom Γ1 (then_prop φ (A `is_ty` B1)) Γ2) →
  merge_then_else Γ2 Δ P R →
  merge_then_else (else_prop φ (A `is_ty` B2) :: Γ1) Δ P (⌜types_intersect φ B1 B2 A⌝ ∗ R).
Proof.
  rewrite /merge_then_else. rewrite nfs_cons_pure.
  intros Hfind. rewrite (nfs_find_pure Hfind). rewrite nfs_cons_pure.
  rewrite /types_intersect. rewrite join_then_else_prop.
  rewrite -!simplify_ent_iff. intros ->. iIntros "([? ?] & $)"; iFrame.
Qed.

Lemma merge_then_else_find_then φ B1 B2 A Γ1 Γ2 Δ P R :
  once_tc (FindPureAtom Γ1 (else_prop φ (A `is_ty` B2)) Γ2) →
  merge_then_else Γ2 Δ P R →
  merge_then_else (then_prop φ (A `is_ty` B1) :: Γ1) Δ P (⌜types_intersect φ B1 B2 A⌝ ∗ R).
Proof.
  rewrite /merge_then_else. rewrite nfs_cons_pure.
  intros Hfind. rewrite (nfs_find_pure Hfind). rewrite nfs_cons_pure.
  rewrite /types_intersect. rewrite join_then_else_prop.
  rewrite -!simplify_ent_iff. intros ->. iIntros "([? ?] & $)"; iFrame.
Qed.

Lemma merge_then_else_frame_pure ψ Γ Δ P R :
  merge_then_else Γ Δ P R →
  merge_then_else (ψ :: Γ) Δ P (⌜framed_prop ψ⌝ ∗ R).
Proof.
  rewrite /merge_then_else. rewrite nfs_cons_pure.
  eapply simplify_sep. eapply simplify_impl_pure.
  by intros [Hφ].
Qed.

Lemma merge_then_else_done Δ P :
  merge_then_else nil Δ P (nfs nil Δ P).
Proof. rewrite /merge_then_else. simplify. Qed.


(* the final pass goes through the proposition to join
   and cleans up any remaining then/else propositions *)
Definition nf_join_unwind_exec (P R : iProp Σ) :=
  simplify P R.

Definition nf_joined_prop_strengthen (φ ψ : Prop) : Prop :=
  ψ → φ.

Definition nf_joined_prop_weaken (φ ψ : Prop) : Prop :=
  φ → ψ.


Class Unconditional (φ : Prop) : Prop.
Global Instance unconditional_has_layout A n: Unconditional (A `has_size` n) := {}.
Global Instance unconditional_is_ty A B: Unconditional (A `is_ty` B) := {}.
(* Global Instance unconditional_has_op_type A ot: Unconditional (A `has_op_type` ot) := {}. *)

Lemma nf_join_unwind_compute P R:
  simplify_chain [
    nf_join_unwind_exec;
    normalize;
    cleanup;
    normalize
  ] P R →
  nf_join_unwind P R.
Proof.
  rewrite /nf_join_unwind /nf_join_unwind_exec. simplify.
Qed.


Lemma nf_join_unwind_exec_ex {X: Type} (P R: X → iProp Σ) :
  (∀! x: X, nf_join_unwind_exec (P x) (R x)) →
  nf_join_unwind_exec (∃ x, P x) (∃ x, R x).
Proof.
  rewrite /nf_join_unwind_exec. simplify.
Qed.

Lemma nf_join_unwind_exec_all {X: Type} (P R: X → iProp Σ) :
  (∀! x: X, nf_join_unwind_exec (P x) (R x)) →
  nf_join_unwind_exec (∀ x, P x) (∀ x, R x).
Proof.
  rewrite /nf_join_unwind_exec. simplify.
Qed.

Lemma nf_join_unwind_exec_nfs_pure_cons φ ψ Γ Δ P R :
  nf_joined_prop_strengthen φ ψ →
  nf_join_unwind_exec (nfs Γ Δ P) R →
  nf_join_unwind_exec (nfs (φ :: Γ) Δ P) (⌜ψ⌝ ∗ R).
Proof.
  rewrite /nf_join_unwind_exec. rewrite nfs_cons_pure.
  intros ?. eapply simplify_sep. eapply simplify_ent.
  iIntros "%Hφ". iPureIntro. eauto.
Qed.

Lemma nf_join_unwind_exec_nfs_nil Δ P R :
  nf_join_unwind_exec P R →
  nf_join_unwind_exec (nfs nil Δ P) (nfs nil Δ R).
Proof. rewrite /nf_join_unwind_exec. simplify. Qed.

Lemma nf_join_unwind_exec_nfw_pure_cons φ ψ Γ Δ P R :
  nf_joined_prop_weaken φ ψ →
  nf_join_unwind_exec (nfw Γ Δ P) R →
  nf_join_unwind_exec (nfw (φ :: Γ) Δ P) (⌜ψ⌝ -∗ R).
Proof.
  rewrite /nf_join_unwind_exec. rewrite nfw_cons_pure.
  intros ?. rewrite -!simplify_ent_iff. intros ->.
  iIntros "Hnfw %Hφ". iApply "Hnfw". iPureIntro. eauto.
Qed.

Lemma nf_join_unwind_exec_nfw_nil Δ P R :
  nf_join_unwind_exec P R →
  nf_join_unwind_exec (nfw nil Δ P) (nfw nil Δ R).
Proof. rewrite /nf_join_unwind_exec. simplify. Qed.

Lemma nf_join_unwind_exec_refl P :
  nf_join_unwind_exec P P.
Proof. rewrite /nf_join_unwind_exec. simplify. Qed.


(* unwind pure propositions *)
Lemma nf_joined_prop_strengthen_framed_prop φ ψ :
  nf_joined_prop_strengthen φ ψ →
  nf_joined_prop_strengthen (framed_prop φ) ψ.
Proof.
  rewrite /nf_joined_prop_strengthen.
  intros ??; split; auto.
Qed.

Lemma nf_joined_prop_strengthen_then_unconditional φ ψ ξ :
  Unconditional ψ →
  nf_joined_prop_strengthen ψ ξ →
  nf_joined_prop_strengthen (then_prop φ ψ) ξ.
Proof. rewrite /then_prop /nf_joined_prop_strengthen. eauto. Qed.

Lemma nf_joined_prop_strengthen_then_conditional φ ψ ξ :
  nf_joined_prop_strengthen ψ ξ →
  nf_joined_prop_strengthen (then_prop φ ψ) (φ → ξ).
Proof. rewrite /then_prop /nf_joined_prop_strengthen. eauto. Qed.

Lemma nf_joined_prop_strengthen_else_unconditional φ ψ ξ :
  Unconditional ψ →
  nf_joined_prop_strengthen ψ ξ →
  nf_joined_prop_strengthen (else_prop φ ψ) ξ.
Proof. rewrite /else_prop /nf_joined_prop_strengthen. eauto. Qed.

Lemma nf_joined_prop_strengthen_else_conditional φ ψ ξ :
  nf_joined_prop_strengthen ψ ξ →
  nf_joined_prop_strengthen (else_prop φ ψ) (¬ φ → ξ).
Proof. rewrite /else_prop /nf_joined_prop_strengthen. eauto. Qed.
Lemma nf_joined_prop_strengthen_refl φ :
  nf_joined_prop_strengthen φ φ.
Proof. rewrite /nf_joined_prop_strengthen. eauto. Qed.


Lemma nf_joined_prop_weaken_framed_prop φ ψ :
  nf_joined_prop_weaken φ ψ →
  nf_joined_prop_weaken (framed_prop φ) ψ.
Proof.
  rewrite /nf_joined_prop_weaken.
  intros ? [?]; auto.
Qed.

Lemma nf_joined_prop_weaken_then φ ψ ξ :
  nf_joined_prop_weaken ψ ξ →
  nf_joined_prop_weaken (then_prop φ ψ) (φ → ξ).
Proof. rewrite /then_prop /nf_joined_prop_weaken. eauto. Qed.

Lemma nf_joined_prop_weaken_else φ ψ ξ :
  nf_joined_prop_weaken ψ ξ →
  nf_joined_prop_weaken (else_prop φ ψ) (¬ φ → ξ).
Proof. rewrite /else_prop /nf_joined_prop_weaken. eauto. Qed.

Lemma nf_joined_prop_weaken_refl φ :
  nf_joined_prop_weaken φ φ.
Proof. rewrite /nf_joined_prop_weaken. eauto. Qed.

End backpropagate_postcond.

Existing Class nf_join_backpropagate.
Global Hint Mode nf_join_backpropagate - ! - : typeclass_instances.

Global Existing Instance nf_join_backpropagate_pass.

Existing Class sort_quantifiers.
Global Hint Mode sort_quantifiers - ! - : typeclass_instances.

Global Existing Instance sort_quantifiers_pass.

Existing Class separate_pre_post.
Global Hint Mode separate_pre_post - ! - - - - : typeclass_instances.

Global Existing Instance separate_pre_post_ex.
Global Existing Instance separate_pre_post_all.
Global Existing Instance separate_pre_post_sep.
Global Existing Instance separate_pre_post_wand.
Global Existing Instance separate_pre_post_nfs.
Global Existing Instance separate_pre_post_nfw.
Global Existing Instance separate_pre_post_done | 10.

Existing Class pre_post_unfold.
Global Hint Mode pre_post_unfold - ! ! ! ! - : typeclass_instances.

Global Existing Instance pre_post_unfold_exists | 1.
Global Existing Instance pre_post_unfold_forall | 1.
Global Existing Instance pre_post_unfold_wand | 1.
Global Existing Instance pre_post_unfold_sep | 2.

Existing Class merge_then_else.
Global Hint Mode merge_then_else - ! ! ! - : typeclass_instances.

Existing Class merge_then_else_process.
Global Hint Mode merge_then_else_process - ! ! ! - : typeclass_instances.

Global Existing Instance merge_then_else_ex | 1.
Global Existing Instance merge_then_else_nfs | 1.
Global Existing Instance merge_then_else_find_else | 2.
Global Existing Instance merge_then_else_find_then | 2.
Global Existing Instance merge_then_else_frame_pure | 3.
Global Existing Instance merge_then_else_done | 20.

Existing Class nf_join_unwind.
Global Hint Mode nf_join_unwind - ! - : typeclass_instances.

Global Existing Instance nf_join_unwind_compute.

Existing Class nf_join_unwind_exec.
Global Hint Mode nf_join_unwind_exec - ! - : typeclass_instances.

Global Existing Instance nf_join_unwind_exec_ex.
Global Existing Instance nf_join_unwind_exec_all.
Global Existing Instance nf_join_unwind_exec_nfs_nil.
Global Existing Instance nf_join_unwind_exec_nfs_pure_cons.
Global Existing Instance nf_join_unwind_exec_nfw_nil.
Global Existing Instance nf_join_unwind_exec_nfw_pure_cons.
Global Existing Instance nf_join_unwind_exec_refl | 100.

Existing Class nf_joined_prop_strengthen.
Global Hint Mode nf_joined_prop_strengthen ! - : typeclass_instances.

Global Existing Instance nf_joined_prop_strengthen_framed_prop | 1.
Global Existing Instance nf_joined_prop_strengthen_then_unconditional | 2.
Global Existing Instance nf_joined_prop_strengthen_then_conditional | 3.
Global Existing Instance nf_joined_prop_strengthen_else_unconditional | 2.
Global Existing Instance nf_joined_prop_strengthen_else_conditional | 3.
Global Existing Instance nf_joined_prop_strengthen_refl | 4.

Existing Class nf_joined_prop_weaken.
Global Hint Mode nf_joined_prop_weaken ! - : typeclass_instances.

Global Existing Instance nf_joined_prop_weaken_framed_prop | 1.
Global Existing Instance nf_joined_prop_weaken_then | 2.
Global Existing Instance nf_joined_prop_weaken_else | 2.
Global Existing Instance nf_joined_prop_weaken_refl | 3.



(* a join operation for propositions that are already in normal form *)
Section nf_join.
  Context `{!refinedcG Σ}.

  Definition joinable (P Q: iProp Σ) : Prop :=
    sealed True.

  (* NOTE: this lemma should not be registered as an instance *)
  Lemma joinable_intro P Q: joinable P Q.
  Proof. rewrite /joinable sealed_eq //. Qed.

  Lemma joinable_ex_l {X: Type} (P: X → iProp Σ) Q :
    (∀ x, joinable (P x) Q) →
    joinable (∃ x, P x) Q.
  Proof. intros. apply joinable_intro. Qed.

  Lemma joinable_ex_r {X: Type} P (Q: X → iProp Σ) :
    (∀ x, joinable P (Q x)) →
    joinable P (∃ x, Q x).
  Proof. intros.  apply joinable_intro. Qed.

  Lemma joinable_all_l {X: Type} (P: X → iProp Σ) Q :
    (∀ x, joinable (P x) Q) →
    joinable (∀ x, P x) Q.
  Proof. intros.  apply joinable_intro. Qed.

  Lemma joinable_all_r {X: Type} P (Q: X → iProp Σ) :
    (∀ x, joinable P (Q x)) →
    joinable P (∀ x, Q x).
  Proof. intros.  apply joinable_intro. Qed.

  Lemma joinable_sep_l P1 P2 Q :
    joinable P2 Q →
    joinable (P1 ∗ P2) Q.
  Proof. intros.  apply joinable_intro. Qed.

  Lemma joinable_sep_r P Q1 Q2 :
    joinable P Q2 →
    joinable P (Q1 ∗ Q2).
  Proof. intros.  apply joinable_intro. Qed.

  Lemma joinable_wand_l P1 P2 Q :
    joinable P2 Q →
    joinable (P1 -∗ P2) Q.
  Proof. intros.  apply joinable_intro. Qed.

  Lemma joinable_wand_r P Q1 Q2 :
    joinable P Q2 →
    joinable P (Q1 -∗ Q2).
  Proof. intros.  apply joinable_intro. Qed.

  Lemma joinable_nfs_l Γ Δ P Q :
    joinable P Q →
    joinable (nfs Γ Δ P) Q.
  Proof. intros.  apply joinable_intro. Qed.

  Lemma joinable_nfs_r Γ Δ P Q :
    joinable P Q →
    joinable P (nfs Γ Δ Q).
  Proof. intros.  apply joinable_intro. Qed.

  Lemma joinable_nfw_r Γ Δ P Q :
    joinable P Q →
    joinable P (nfw Γ Δ Q).
  Proof. intros.  apply joinable_intro. Qed.

  Lemma joinable_nfw_l Γ Δ P Q :
    joinable P Q →
    joinable (nfw Γ Δ P) Q.
  Proof. intros.  apply joinable_intro. Qed.

  Lemma joinable_same_prop P : joinable P P.
  Proof.  apply joinable_intro. Qed.

  Lemma joinable_same_postcond Φ A B: joinable (type_post Φ A) (type_post Φ B).
  Proof.  apply joinable_intro. Qed.

  Lemma joinable_same_cont Φ A B: joinable (type_cont Φ A) (type_cont Φ B).
  Proof.  apply joinable_intro. Qed.

  Definition nf_join (φ: dProp) (P Q R: iProp Σ) :=
    simplify (case φ P Q) R.

  Lemma nf_join_compute φ P1 P2 Q R S :
    sequence_classes [
      joinable P1 P2;
      nf_join_pre φ P1 P2 Q;
      nf_join_post φ Q R;
      nf_join_backpropagate R S
    ] →
    nf_join φ P1 P2 S.
  Proof.
    rewrite /nf_join_pre /nf_join_post /nf_join /sequence_classes.
    rewrite !Forall_cons /=. intros [? [? [? [? ?]]]].
    simplify.
  Qed.

  Definition sym_join (φ: dProp) (P Q R: iProp Σ) :=
    simplify (case φ P Q) R.

  Lemma sym_join_compute φ P1 P2 Q1 Q2 R :
    simplify_chain [normalize; elim_existentials] P1 Q1 →
    simplify_chain [normalize; elim_existentials] P2 Q2 →
    nf_join φ Q1 Q2 R →
    sym_join φ P1 P2 R.
  Proof.
    rewrite /sym_join /nf_join.
    intros; eapply simplify_trans; last done.
    eapply simplify_case; simplify.
  Qed.

  Lemma sym_join_joined φ (P Q R: iProp Σ):
    once_tc (sym_join φ P Q R) →
    joined φ P Q R.
  Proof.
    rewrite /sym_join /join.
    simplify.
  Qed.


End nf_join.

(* The join instance *)
Global Existing Instance sym_join_joined | 2.

Existing Class nf_join.
Global Hint Mode nf_join - ! ! ! - : typeclass_instances.

(* we make sure the propositions are joinable before we proceed *)
Global Existing Instance nf_join_compute | 1.

(* a symmetric join operation *)
Existing Class sym_join.
Global Hint Mode sym_join - + + + - : typeclass_instances.

Global Existing Instance sym_join_compute | 1.

Existing Class joinable.
Global Hint Mode joinable - ! ! : typeclass_instances.

Global Existing Instance joinable_ex_l | 1.
Global Existing Instance joinable_ex_r | 1.
Global Existing Instance joinable_all_l | 1.
Global Existing Instance joinable_all_r | 1.
Global Existing Instance joinable_sep_l | 1.
Global Existing Instance joinable_sep_r | 1.
Global Existing Instance joinable_wand_l | 1.
Global Existing Instance joinable_wand_r | 1.
Global Existing Instance joinable_nfs_l | 1.
Global Existing Instance joinable_nfs_r | 1.
Global Existing Instance joinable_nfw_l | 1.
Global Existing Instance joinable_nfw_r | 1.
Global Existing Instance joinable_same_prop | 10.
Global Existing Instance joinable_same_cont | 11.
Global Existing Instance joinable_same_postcond | 11.

