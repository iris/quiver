From quiver.argon.simplification Require Import simplify.
From quiver.thorium.logic Require Import pointers.
From quiver.thorium.types Require Import types refinements pointers base_types combinators structs.
From quiver.base Require Import classes.
From quiver.argon.abduction Require Import joined.


Inductive type_scheme `{!refinedcG Σ} : Type := TS (A: type Σ) | TC {X: Type} (S: X → type_scheme).

Global Arguments type_scheme Σ {_}.
Global Coercion TS : type >-> type_scheme.
Notation "? x .. y , P" := (TC (λ x , .. (TC (λ y , P)) ..)) (at level 200, x binder, y binder, right associativity, format "'[ ' '[ ' ? x .. y ']' , '/' P ']'").



Section joining_values.
  Definition values_union_pre {X: Type} (φ: dProp) (a b c: X) : Prop := (case φ a b = c).

  Lemma values_union_pre_default {X: Type} (φ: dProp) (a b: X) :
    values_union_pre φ a b (case φ a b).
  Proof. reflexivity. Qed.

  Lemma values_union_pre_same {X: Type} (φ: dProp) (a: X) :
    values_union_pre φ a a a.
  Proof. rewrite /values_union_pre /case; by destruct decide. Qed.


  Lemma values_union_pre_pair {X Y: Type} φ (a a1 a2 : X) (b b1 b2: Y) :
    once_tc (values_union_pre φ a1 a2 a) →
    once_tc (values_union_pre φ b1 b2 b) →
    values_union_pre φ (a1, b1) (a2, b2) (a, b).
  Proof.
    rewrite /once_tc /values_union_pre /case; destruct decide; naive_solver.
  Qed.


End joining_values.

Existing Class values_union_pre.
Global Hint Mode values_union_pre - ! ! ! - : typeclass_instances.
Global Existing Instance values_union_pre_default | 100.
Global Existing Instance values_union_pre_same | 1.
Global Existing Instance values_union_pre_pair | 2.




Section joining_types.
Context `{!refinedcG Σ}.

(* types_intersect A1 A2 B ↔ "B = A1 ∧ A2" *)
Definition types_intersect (φ: dProp) (A1 A2 B: type Σ) : Prop :=
  joined_prop φ (B `is_ty` A1) (B `is_ty` A2).

Definition types_intersect_all (φ: dProp) (A1 A2 B: list (type Σ)) : Prop :=
  joined_prop φ (B `is_ty_all` A1) (B `is_ty_all` A2).

(* types_union A1 A2 B ↔ "B = A1 ∨ A2" *)
Definition types_union (φ: dProp) (A1 A2 B: type Σ) : Prop :=
  joined_prop φ (A1 `is_ty` B) (A2 `is_ty` B).

Definition types_union_all (φ: dProp) (A1 A2 B: list (type Σ)) : Prop :=
  joined_prop φ (A1 `is_ty_all` B) (A2 `is_ty_all` B).

Definition types_intersect_pre (φ: dProp) (A1 A2 B: type Σ) (R: iProp Σ) : Prop :=
  simplify (⌜types_intersect φ A1 A2 B⌝) R.

Definition types_intersect_all_pre (φ: dProp) (A1 A2 B: list (type Σ)) (R: iProp Σ) : Prop :=
  simplify (⌜types_intersect_all φ A1 A2 B⌝) R.

Definition types_union_pre (φ: dProp) (A1 A2 B: type Σ) (R: iProp Σ) : Prop :=
  simplify (⌜types_union φ A1 A2 B⌝) R.

Definition types_union_all_pre (φ: dProp) (A1 A2 B: list (type Σ)) (R: iProp Σ) : Prop :=
  simplify (⌜types_union_all φ A1 A2 B⌝) R.

(* we widen types by abstracting concrete values (e.g., refinements of integers) *)
Fixpoint widen_type (A: type Σ) (S: type_scheme Σ) : Prop :=
  match S with
  | TS B => A `is_ty` B
  | TC S' => ∃ x, widen_type A (S' x)
  end.

Fixpoint type_scheme_map (F: type Σ → type Σ) (S: type_scheme Σ) :=
  match S with
  | TS A => TS (F A)
  | TC S' => TC (λ x, type_scheme_map F (S' x))
  end.


Definition scheme_union_pre_l (φ: dProp) (S: type_scheme Σ) (A1 A2 B: type Σ) (R: iProp Σ) : Prop :=
  types_union_pre φ A1 A2 B R.

Definition scheme_union_pre_r (φ: dProp) (S: type_scheme Σ) (A1 A2 B: type Σ) (R: iProp Σ) : Prop :=
  types_union_pre φ A1 A2 B R.

(* TYPES INTERSECT *)
Lemma types_intersect_pre_refl φ A B :
  types_intersect_pre φ A A B ⌜B `is_ty` A⌝.
Proof.
  rewrite /types_intersect_pre /types_intersect.
  eapply simplify_ent; rewrite /joined_prop /case.
  by destruct decide.
Qed.

Lemma types_intersect_pre_var_l φ A B C:
  is_var A →
  types_intersect_pre φ A B C (⌜C `is_ty` B⌝ ∗ ⌜B `is_ty` A⌝).
Proof.
  intros _. eapply simplify_ent; rewrite /types_intersect_pre /types_intersect.
  iIntros "[%HC %HA]". iPureIntro.
  rewrite /joined_prop /case; destruct decide.
  all: eauto using is_ty_trans.
Qed.

Lemma types_intersect_pre_var_r φ A B C:
  is_var B →
  types_intersect_pre φ A B C (⌜C `is_ty` A⌝ ∗ ⌜A `is_ty` B⌝).
Proof.
  intros _. eapply simplify_ent; rewrite /types_intersect_pre /types_intersect.
  iIntros "[%HC %HA]". iPureIntro.
  rewrite /joined_prop /case; destruct decide.
  all: eauto using is_ty_trans.
Qed.

Lemma types_intersect_pre_own φ p q A B C R:
  (∀! D, types_intersect_pre φ A B D (R D)) →
  types_intersect_pre φ (ownT p A) (ownT q B) C
    (∃ D, ⌜p = q⌝ ∗ ⌜C `is_ty` ownT p D⌝ ∗ R D).
Proof.
  rewrite /types_intersect_pre. rewrite -!simplify_ent_iff.
  iIntros (Hinter) "[%D [-> [%Hty HR]]]".
  eapply ent_simplify in Hinter. rewrite Hinter.
  rewrite /types_intersect /joined_prop /case.
  destruct decide; iDestruct "HR" as "%Hty2"; iPureIntro.
  - setoid_rewrite Hty. rewrite is_ty_ty_le.
    eapply ownT_le. by rewrite -is_ty_ty_le.
  - setoid_rewrite Hty. rewrite is_ty_ty_le.
    eapply ownT_le. by rewrite -is_ty_ty_le.
Qed.

Lemma types_intersect_pre_optional φ ψ1 ψ2 A B C R:
  (∀! D, types_intersect_pre φ A B D (R D)) →
  types_intersect_pre φ (optionalT ψ1 A) (optionalT ψ2 B) C
    (∃ D, ⌜ψ1 ↔ ψ2⌝ ∗ ⌜C `is_ty` optionalT ψ1 D⌝ ∗ R D).
Proof.
  rewrite /types_intersect_pre. rewrite -!simplify_ent_iff.
  iIntros (Hinter) "[%D [%Hiff [%Hty HR]]]".
  eapply ent_simplify in Hinter. rewrite Hinter.
  rewrite /types_intersect /joined_prop /case.
  destruct decide; iDestruct "HR" as "%Hty2"; iPureIntro.
  - setoid_rewrite Hty. rewrite is_ty_ty_le.
    eapply optionalT_le. by rewrite -is_ty_ty_le.
  - setoid_rewrite Hty. rewrite is_ty_ty_le.
    rewrite optionalT_iff_eq //.
    eapply optionalT_le. by rewrite -is_ty_ty_le.
Qed.

Lemma types_intersect_pre_null_l φ A C :
  types_intersect_pre φ nullT A C ⌜C `is_ty` optionalT (¬ φ) A⌝.
Proof.
  rewrite /types_intersect_pre /types_intersect.
  eapply simplify_ent; rewrite /joined_prop /optionalT /case /=.
  rewrite decide_bool_decide bool_decide_not bool_decide_decide.
  by destruct decide.
Qed.

Lemma types_intersect_pre_null_r φ A C :
  types_intersect_pre φ A nullT C ⌜C `is_ty` optionalT φ A⌝.
Proof.
  rewrite /types_intersect_pre /types_intersect.
  eapply simplify_ent; rewrite /joined_prop /optionalT /case /=.
  by destruct decide.
Qed.

Lemma types_intersect_pre_any_r φ n A C:
  types_intersect_pre φ A (anyT n) C (⌜C `is_ty` A⌝ ∗ ⌜A `has_size` n⌝).
Proof.
  rewrite /types_intersect_pre /types_intersect.
  eapply simplify_ent; rewrite /joined_prop /case.
  iIntros "[%Hty %Hly]".
  iPureIntro; destruct decide; eauto using has_size_inty_any, is_ty_trans.
Qed.

Lemma types_intersect_pre_any_l φ ly A C:
  types_intersect_pre φ (anyT ly) A C (⌜C `is_ty` A⌝ ∗ ⌜A `has_size` ly⌝).
Proof.
  rewrite /types_intersect_pre /types_intersect.
  eapply simplify_ent; rewrite /joined_prop /case.
  iIntros "[%Hty %Hly]".
  iPureIntro; destruct decide; eauto using has_size_inty_any, is_ty_trans.
Qed.

Lemma types_intersect_pre_int φ it1 it2 n1 n2 C:
  types_intersect_pre φ (intT it1 n1) (intT it2 n2) C
    (⌜C `is_ty` intT it1 n1⌝ ∗ ⌜it1 = it2⌝ ∗ ⌜n1 = n2⌝).
Proof.
  rewrite /types_intersect_pre /types_intersect.
  eapply simplify_ent; rewrite /joined_prop /case.
  iIntros "[%Hty [%Hit %Hn]]". subst.
  iPureIntro; destruct decide; eauto.
Qed.

Lemma types_intersect_pre_bool φ C it1 it2 ψ1 ψ2 :
  types_intersect_pre φ (ψ1 @ boolR it1)%RT (ψ2 @ boolR it2)%RT C
    (⌜C `is_ty` (ψ1 @ boolR it1)%RT⌝ ∗ ⌜it1 = it2⌝ ∗ ⌜ψ1 ↔ ψ2⌝).
Proof.
  rewrite /types_intersect_pre /types_intersect.
  eapply simplify_ent; rewrite /joined_prop /case.
  iIntros "[%Hty [%Hit %Hψ]]". subst.
  iPureIntro; destruct decide; eauto.
  setoid_rewrite Hty. eapply is_ty_ty_le.
  rewrite (boolR_iff_eq ψ1 ψ2) //.
Qed.

Lemma types_intersect_pre_refinement φ A B C {X} (T: rtype Σ X) a b:
  is_refinement A T a →
  is_refinement B T b →
  types_intersect_pre φ A B C (⌜a = b⌝ ∗ ⌜C `is_ty` a @ T⌝).
Proof.
  rewrite /types_intersect_pre /types_intersect.
  intros Heq1 Heq2. eapply simplify_ent; rewrite /joined_prop /case.
  iIntros "[%Heq %Hty]". subst.
  iPureIntro; destruct decide; setoid_rewrite Hty; eapply is_ty_ty_le.
  - rewrite Heq1 //.
  - rewrite Heq2 //.
Qed.

Lemma types_intersect_pre_struct φ sl Ts Us C R:
  (∀! Cs, types_intersect_all_pre φ Ts Us Cs (R Cs)) →
  types_intersect_pre φ (structT sl Ts) (structT sl Us) C (∃ Cs, ⌜C `is_ty` structT sl Cs⌝ ∗ R Cs).
Proof.
  rewrite /types_intersect_pre.
  intros Hintersect. eapply simplify_ent; rewrite /joined_prop /case.
  iIntros "[%Cs [%Hty HR]]". eapply ent_simplify in Hintersect.
  rewrite Hintersect.
  rewrite /types_intersect /types_intersect_all.
  rewrite /joined_prop /case; destruct decide; iDestruct "HR" as "%Hjoin"; iPureIntro.
  - setoid_rewrite Hty. eapply is_ty_ty_le.
    eapply structT_le; first done. eapply Forall2_impl; first done.
    intros ? ? ?%is_ty_ty_le. done.
  - setoid_rewrite Hty. eapply is_ty_ty_le.
    eapply structT_le; first done. eapply Forall2_impl; first done.
    intros ? ? ?%is_ty_ty_le. done.
Qed.

(* TYPES INTERSECT ALL *)
Lemma types_intersect_all_cons φ A B C As Bs Cs:
  types_intersect φ A B C →
  types_intersect_all φ As Bs Cs →
  types_intersect_all φ (A :: As) (B :: Bs) (C :: Cs).
Proof.
  rewrite /types_intersect_all /types_intersect /joined_prop /case; destruct decide.
  all: intros Hintersect Hintersect_all; econstructor; eauto.
Qed.

Lemma types_intersect_all_nil φ:
  types_intersect_all φ [] [] [].
Proof.
  rewrite /types_intersect_all /joined_prop /case.
  destruct decide; econstructor.
Qed.


Lemma types_intersect_all_pre_nil φ Cs :
  types_intersect_all_pre φ nil nil Cs (⌜Cs = []⌝).
Proof.
  rewrite /types_intersect_all_pre.
  eapply simplify_ent; rewrite /joined_prop /case.
  iIntros "%". iPureIntro. subst. by apply types_intersect_all_nil.
Qed.

Lemma types_intersect_all_pre_cons φ A B As Bs Cs R1 R2:
  (∀! C, types_intersect_pre φ A B C (R1 C)) →
  (∀! Cr, types_intersect_all_pre φ As Bs Cr (R2 Cr)) →
  types_intersect_all_pre φ (A :: As) (B :: Bs) Cs (∃ C Cr, ⌜Cs = C :: Cr⌝ ∗ R1 C ∗ R2 Cr).
Proof.
  rewrite /types_intersect_all_pre.
  intros Hintersect1 Hintersect2. eapply simplify_ent.
  iIntros "[%C [%Cr [%Heq [HR1 HR2]]]]". subst.
  iPoseProof (ent_simplify with "HR1") as "HR1"; first apply Hintersect1.
  iPoseProof (ent_simplify with "HR2") as "HR2"; first apply Hintersect2.
  iDestruct "HR1" as "%Hint1". iDestruct "HR2" as "%Hint2". iPureIntro.
  by eapply types_intersect_all_cons.
Qed.


(* TYPES UNION *)
Lemma types_union_compat F φ A B C:
  Proper (type_le ==> type_le) F →
  types_union φ A B C →
  types_union φ (F A) (F B) (F C).
Proof.
  intros Hle. rewrite /types_union /joined_prop.
  rewrite /case; destruct decide; rewrite !is_ty_ty_le; by apply Hle.
Qed.

Lemma types_union_compat_struct φ sl As Bs Ds :
  types_union_all φ As Bs Ds →
  types_union φ (structT sl As) (structT sl Bs) (structT sl Ds).
Proof.
  rewrite /types_union /types_union_all /joined_prop /case.
  destruct decide; rewrite !is_ty_ty_le; intros Hunion.
  - eapply structT_le; first done. eapply Forall2_impl; first done.
    intros ? ? ?%is_ty_ty_le. done.
  - eapply structT_le; first done. eapply Forall2_impl; first done.
    intros ? ? ?%is_ty_ty_le. done.
Qed.

Lemma types_union_all_nil φ:
  types_union_all φ [] [] [].
Proof.
  rewrite /types_union_all /joined_prop /case.
  destruct decide; econstructor.
Qed.

Lemma types_union_all_cons φ A B C As Bs Cs:
  types_union φ A B C →
  types_union_all φ As Bs Cs →
  types_union_all φ (A :: As) (B :: Bs) (C :: Cs).
Proof.
  rewrite /types_union_all /types_union /joined_prop /case; destruct decide.
  all: intros Hunion Hunion_all; econstructor; eauto.
Qed.

Lemma types_union_is_ty φ A B C D:
  types_union φ A B C →
  C `is_ty` D →
  types_union φ A B D.
Proof.
  rewrite /types_union /joined_prop.
  rewrite /case; destruct decide; eauto using is_ty_trans.
Qed.

Lemma types_union_pre_compat F φ A B C R:
  Proper (type_le ==> type_le) F →
  (∀! D, types_union_pre φ A B D (R D)) →
  types_union_pre φ (F A) (F B) C (∃ D, ⌜F D `is_ty` C⌝ ∗ R D).
Proof.
  intros Hle. rewrite /types_union_pre.
  intros Hent; eapply simplify_ent.
  iIntros "[%D [%Hty HR]]". eapply ent_simplify in Hent. rewrite Hent.
  iDestruct "HR" as "%Hu". iPureIntro.
  eapply types_union_is_ty; last done.
  by apply: types_union_compat.
Qed.

Lemma types_union_pre_refl φ (A C: type Σ):
  types_union_pre φ A A C ⌜A `is_ty` C⌝.
Proof.
  rewrite /types_union_pre.
  eapply simplify_ent. iIntros "%Hty".
  iPureIntro; rewrite /types_union /joined_prop /case.
  destruct decide; done.
Qed.

Lemma types_union_pre_int φ it n1 n2 n3 C:
  values_union_pre φ n1 n2 n3 →
  types_union_pre φ (intT it n1) (intT it n2) C ⌜intT it n3 `is_ty` C⌝.
Proof.
  rewrite /types_union_pre /types_union /values_union_pre.
  intros ?; subst n3.
  eapply simplify_ent; rewrite /joined_prop /case.
  iIntros "%Hty". iPureIntro; destruct decide; eauto.
Qed.

Lemma types_union_pre_null_r φ A C:
  types_union_pre φ A nullT C ⌜optionalT φ A `is_ty` C⌝.
Proof.
  rewrite /types_union_pre /types_union.
  eapply simplify_ent; rewrite /optionalT /joined_prop /case.
  destruct decide; iIntros "%Hty"; iPureIntro; eauto.
Qed.

Lemma types_union_pre_null_l φ A C:
  types_union_pre φ nullT A C ⌜optionalT (¬ φ) A `is_ty` C⌝.
Proof.
  rewrite /types_union_pre /types_union.
  eapply simplify_ent; rewrite /optionalT /joined_prop /case /=.
  destruct decide; iIntros "%Hty"; iPureIntro.
  all: destruct decide; naive_solver.
Qed.

Lemma types_union_pre_own φ p A B C R:
  (∀! D, types_union_pre φ A B D (R D)) →
  types_union_pre φ (ownT p A) (ownT p B) C (∃ D, ⌜ownT p D `is_ty` C⌝ ∗ R D).
Proof.
  apply: types_union_pre_compat.
Qed.

Lemma types_union_pre_optional φ ψ A B C R:
  (∀! D, types_union_pre φ A B D (R D)) →
  types_union_pre φ (optionalT ψ A) (optionalT ψ B) C (∃ D, ⌜optionalT ψ D `is_ty` C⌝ ∗ R D).
Proof.
  apply: types_union_pre_compat.
Qed.

Lemma types_union_pre_struct φ sl As Bs C R:
  (∀! Ds, types_union_all_pre φ As Bs Ds (R Ds)) →
  types_union_pre φ (structT sl As) (structT sl Bs) C (∃ Ds, ⌜structT sl Ds `is_ty` C⌝ ∗ R Ds).
Proof.
  rewrite /types_union_pre /types_union_all_pre.
  intros Hent; eapply simplify_ent.
  iIntros "[%D [%Hty HR]]". eapply ent_simplify in Hent. rewrite Hent.
  iDestruct "HR" as "%Hu". iPureIntro.
  eapply types_union_is_ty; last done.
  by eapply types_union_compat_struct.
Qed.

Lemma types_union_pre_var_both φ A B C :
  is_var A →
  is_var B →
  types_union_pre φ A B C (⌜A `is_ty` B⌝ ∗ ⌜B `is_ty` C⌝).
Proof.
  rewrite /types_union_pre /types_union. intros _ _.
  eapply simplify_ent; rewrite /joined_prop /case.
  iIntros "[%Hty1 %Hty2]". iPureIntro.
  destruct decide; eauto using is_ty_trans.
Qed.

Lemma types_union_pre_var_l φ A B S C R:
  is_var A →
  widen_type B S →
  scheme_union_pre_l φ S A B C R →
  types_union_pre φ A B C R.
Proof.
  intros _ _. rewrite /scheme_union_pre_l //.
Qed.

Lemma types_union_pre_var_r φ A B S C R:
  is_var B →
  widen_type A S →
  scheme_union_pre_r φ S A B C R →
  types_union_pre φ A B C R.
Proof.
  intros _ _. rewrite /scheme_union_pre_r //.
Qed.


Lemma types_union_pre_refinement φ A B {X} (T: rtype Σ X) x y z C:
  sequence_classes
    [is_refinement A T x;
     is_refinement B T y;
     values_union_pre φ x y z] →
  types_union_pre φ A B C (⌜z @ T `is_ty` C⌝).
Proof.
  rewrite /sequence_classes !Forall_cons /id.
  rewrite /types_union_pre /types_union /joined_prop /values_union_pre.
  rewrite /is_refinement. intros (Heq1 & Heq2 & Heq3 & _); subst z.
  eapply simplify_impl_pure.
  rewrite /case; destruct decide; rewrite !is_ty_ty_le.
  - by rewrite Heq1.
  - by rewrite Heq2.
Qed.


Lemma types_union_all_pre_nil φ (C: list (type Σ)):
  types_union_all_pre φ [] [] [] (⌜C = []⌝).
Proof.
  rewrite /types_union_all_pre. eapply simplify_ent.
  iIntros "%". subst. iPureIntro. eapply types_union_all_nil.
Qed.

Lemma types_union_all_pre_cons φ A B As Bs Cs R1 R2:
  (∀! C, types_union_pre φ A B C (R1 C)) →
  (∀! Cr, types_union_all_pre φ As Bs Cr (R2 Cr)) →
  types_union_all_pre φ (A :: As) (B :: Bs) Cs (∃ C Cr, ⌜Cs = C :: Cr⌝ ∗ R1 C ∗ R2 Cr).
Proof.
  rewrite /types_union_all_pre. intros Hent1 Hent2.
  eapply simplify_ent.
  iIntros "[%C [%Cr HR]]".
  eapply ent_simplify in Hent1. rewrite Hent1.
  eapply ent_simplify in Hent2. rewrite Hent2.
  iDestruct "HR" as "[%C' [%Hty1 %Hty2]]".
  iPureIntro. subst. by eapply types_union_all_cons.
Qed.


(* WIDENING TYPES *)
Lemma widen_type_type_scheme_map F A S:
  (∀ B, A `is_ty` B → F A `is_ty` F B) →
  widen_type A S →
  widen_type (F A) (type_scheme_map F S).
Proof.
  intros Hent. induction S as [B|X S IH]; simpl.
  - eapply Hent.
  - intros [x Hwiden]. exists x. auto.
Qed.


Lemma widen_type_refl A : widen_type A A.
Proof.
  rewrite /widen_type //.
Qed.

Lemma widen_type_int it n: widen_type (intT it n) (? m, intT it m).
Proof.
  rewrite /widen_type //=. by eexists _.
Qed.

Lemma widen_type_bool it φ: widen_type (φ @ boolR it)%RT (? ψ, ψ @ boolR it)%RT.
Proof.
  rewrite /widen_type //=. by eexists _.
Qed.

Lemma widen_type_optional φ A S:
  widen_type A S →
  widen_type (optionalT φ A) (type_scheme_map (optionalT φ) S).
Proof.
  eapply widen_type_type_scheme_map.
  intros B. eapply optionalT_is_ty; done.
Qed.

Lemma widen_type_own p A S:
  widen_type A S →
  widen_type (ownT p A) (type_scheme_map (ownT p) S).
Proof.
  eapply widen_type_type_scheme_map.
  intros B. eapply ownT_is_ty; done.
Qed.

Lemma widen_type_refinement A {X} (T: rtype Σ X) x :
  is_refinement A T x →
  widen_type A (? y, y @ T)%RT.
Proof.
  rewrite /is_refinement. intros Heq.
  rewrite /widen_type //=.
  eexists x. split. rewrite -Heq //.
Qed.


(* incorporating a scheme into a union type *)
Lemma scheme_union_pre_l_choose φ {X: Type} S B1 B2 A R:
  (∀! x, scheme_union_pre_l φ (S x) B1 B2 A (R x)) →
  scheme_union_pre_l φ (? x: X, S x) B1 B2 A (∃ x, R x).
Proof.
  rewrite /scheme_union_pre_l /types_union_pre.
  intros Hunion. eapply simplify_ent. iIntros "[%x HR]".
  by iApply ent_simplify.
Qed.

Lemma scheme_union_pre_r_choose φ {X: Type} S B1 B2 A R:
  (∀! x, scheme_union_pre_r φ (S x) B1 B2 A (R x)) →
  scheme_union_pre_r φ (? x: X, S x) B1 B2 A (∃ x, R x).
Proof.
  rewrite /scheme_union_pre_r /types_union_pre.
  intros Hunion. eapply simplify_ent. iIntros "[%x HR]".
  by iApply ent_simplify.
Qed.

Lemma scheme_union_pre_l_done φ B1' B1 B2 A R:
  types_union_pre φ B1' B2 A R →
  scheme_union_pre_l φ B1' B1 B2 A (⌜then_prop φ (B1 `is_ty` B1')⌝ ∗ R).
Proof.
  rewrite /scheme_union_pre_l /types_union_pre.
  rewrite -!simplify_ent_iff. intros ->.
  iIntros "[%Hthen %Huni]". iPureIntro.
  revert Hthen Huni. rewrite /then_prop /types_union /joined_prop /case.
  destruct decide; eauto using is_ty_trans.
Qed.

Lemma scheme_union_pre_r_done φ B2' B1 B2 A R:
  types_union_pre φ B1 B2' A R →
  scheme_union_pre_r φ B2' B1 B2 A (⌜else_prop φ (B2 `is_ty` B2')⌝ ∗ R).
Proof.
  rewrite /scheme_union_pre_r /types_union_pre.
  rewrite -!simplify_ent_iff. intros ->.
  iIntros "[%Hthen %Huni]". iPureIntro.
  revert Hthen Huni. rewrite /then_prop /types_union /joined_prop /case.
  destruct decide; eauto using is_ty_trans.
Qed.


(* conditionally completing two types *)
Definition complete_types φ x A1 A2 Δ1 Δ2 B1 B2 Ξ1 Ξ2 : Prop :=
  (case φ (x ◁ₓ A1 ∗ [∗] Δ1) (x ◁ₓ A2 ∗ [∗] Δ2)) ⊢
  (case φ (x ◁ₓ B1 ∗ [∗] Ξ1) (x ◁ₓ B2 ∗ [∗] Ξ2)).

Definition complete_types_all φ X A1 A2 Δ1 Δ2 B1 B2 Ξ1 Ξ2 : Prop :=
  (case φ (xtr_all X A1 ∗ [∗] Δ1) (xtr_all X A2 ∗ [∗] Δ2)) ⊢
  (case φ (xtr_all X B1 ∗ [∗] Ξ1) (xtr_all X B2 ∗ [∗] Ξ2)).

Lemma complete_types_refl φ x A1 A2 Δ1 Δ2 :
  complete_types φ x A1 A2 Δ1 Δ2 A1 A2 Δ1 Δ2.
Proof.
  rewrite /complete_types. done.
Qed.

Lemma complete_types_same_type φ x A Δ1 Δ2 :
  complete_types φ x A A Δ1 Δ2 A A Δ1 Δ2.
Proof.
  rewrite /complete_types. done.
Qed.

Lemma complete_types_fill_placeholder_type_l φ x A1 A1' A2 Δ1 Δ1' Δ2 B1 B2 Ξ1 Ξ2 :
  fill_placeholder_type AlwaysCopy x A1 Δ1 A1' Δ1' →
  complete_types φ x A1' A2 Δ1' Δ2 B1 B2 Ξ1 Ξ2 →
  complete_types φ x A1 A2 Δ1 Δ2 B1 B2 Ξ1 Ξ2.
Proof.
  rewrite /complete_types /fill_placeholder_type. by intros ->.
Qed.

Lemma complete_types_fill_placeholder_type_r φ x A1 A2 A2' Δ1 Δ2 Δ2' B1 B2 Ξ1 Ξ2 :
  fill_placeholder_type AlwaysCopy x A2 Δ2 A2' Δ2' →
  complete_types φ x A1 A2' Δ1 Δ2' B1 B2 Ξ1 Ξ2 →
  complete_types φ x A1 A2 Δ1 Δ2 B1 B2 Ξ1 Ξ2.
Proof.
  rewrite /complete_types /fill_placeholder_type. by intros ->.
Qed.

Lemma complete_types_own φ x p A1 A2 B1 B2 Δ1 Δ2 Ξ1 Ξ2 :
  complete_types φ (LOC p) A1 A2 Δ1 Δ2 B1 B2 Ξ1 Ξ2 →
  complete_types φ x (ownT p A1) (ownT p A2) Δ1 Δ2 (ownT p B1) (ownT p B2) Ξ1 Ξ2.
Proof.
  rewrite /complete_types /case; destruct decide; rewrite -!value_own_xtr /=.
  - iIntros (Hent) "[[$ Hp] HΔ1]". iApply Hent. iFrame.
  - iIntros (Hent) "[[$ Hp] HΔ2]". iApply Hent. iFrame.
Qed.


Lemma complete_types_struct φ l sl A1s A2s B1s B2s Δ1 Δ2 Ξ1 Ξ2 :
  complete_types_all φ (LOC <$> member_locs l sl) A1s A2s Δ1 Δ2 B1s B2s Ξ1 Ξ2 →
  complete_types φ (LOC l) (structT sl A1s) (structT sl A2s) Δ1 Δ2 (structT sl B1s) (structT sl B2s) Ξ1 Ξ2.
Proof.
  rewrite /complete_types /complete_types_all.
  rewrite !xtr_all_fmap_loc /case /=.
  destruct decide.
  - iIntros (Hent) "[Hl HΔ1]". rewrite struct_focus_ty_own_all.
    iDestruct "Hl" as "[Hall Hcont]".
    iDestruct (Hent with "[$Hall $HΔ1]") as "[Hl $]".
    by iApply "Hcont".
  - iIntros (Hent) "[Hl HΔ1]". rewrite struct_focus_ty_own_all.
    iDestruct "Hl" as "[Hall Hcont]".
    iDestruct (Hent with "[$Hall $HΔ1]") as "[Hl $]".
    by iApply "Hcont".
Qed.


Lemma complete_types_all_cons φ x X A1 A1s A2 A2s Δ1 Δ1' Δ2 Δ2' B1 B1s B2 B2s Ξ1 Ξ2 :
  complete_types φ x A1 A2 Δ1 Δ2 B1 B2 Δ1' Δ2' →
  complete_types_all φ X A1s A2s Δ1' Δ2' B1s B2s Ξ1 Ξ2 →
  complete_types_all φ (x :: X) (A1 :: A1s) (A2 :: A2s) Δ1 Δ2 (B1 :: B1s) (B2 :: B2s) Ξ1 Ξ2.
Proof.
  rewrite /complete_types_all /complete_types.
  rewrite !xtr_all_cons /case /=; destruct decide.
  - iIntros (Hent1 Hent2) "[[Hx Hall] HΔ]".
    iDestruct (Hent1 with "[$Hx $HΔ]") as "[Hx HΔ]".
    iDestruct (Hent2 with "[$Hall $HΔ]") as "[Hall HΔ]".
    by iFrame.
  - iIntros (Hent1 Hent2) "[[Hx Hall] HΔ]".
    iDestruct (Hent1 with "[$Hx $HΔ]") as "[Hx HΔ]".
    iDestruct (Hent2 with "[$Hall $HΔ]") as "[Hall HΔ]".
    by iFrame.
Qed.

Lemma complete_types_all_nil φ Δ1 Δ2 :
  complete_types_all φ [] [] [] Δ1 Δ2 [] [] Δ1 Δ2.
Proof.
  rewrite /complete_types_all. done.
Qed.

End joining_types.


Global Typeclasses Opaque
types_intersect types_intersect_all types_union types_union_all.


Existing Class types_intersect_pre.
Global Hint Mode types_intersect_pre - - ! ! ! - - : typeclass_instances.

Existing Class types_intersect_all_pre.
Global Hint Mode types_intersect_all_pre - - ! ! ! - - : typeclass_instances.

Existing Class types_union_pre.
Global Hint Mode types_union_pre - - ! ! ! - - : typeclass_instances.

Existing Class types_union_all_pre.
Global Hint Mode types_union_all_pre - - ! ! ! - - : typeclass_instances.

Existing Class widen_type.
Global Hint Mode widen_type - - ! - : typeclass_instances.

Existing Class scheme_union_pre_l.
Global Hint Mode scheme_union_pre_l - - - ! - - - - : typeclass_instances.

Existing Class scheme_union_pre_r.
Global Hint Mode scheme_union_pre_r - - - ! - - - - : typeclass_instances.

Existing Class complete_types.
Global Hint Mode complete_types - - - - ! ! - - - - - - : typeclass_instances.

Existing Class complete_types_all.
Global Hint Mode complete_types_all - - - - ! ! - - - - - - : typeclass_instances.


Global Existing Instance types_intersect_pre_refl | 1.
Global Existing Instance types_intersect_pre_var_r | 2.
Global Existing Instance types_intersect_pre_var_l | 2.
Global Existing Instance types_intersect_pre_any_l | 3.
Global Existing Instance types_intersect_pre_any_r | 3.
Global Existing Instance types_intersect_pre_int | 3.
Global Existing Instance types_intersect_pre_bool | 3.
Global Existing Instance types_intersect_pre_null_r | 3.
Global Existing Instance types_intersect_pre_null_l | 3.
Global Existing Instance types_intersect_pre_own | 3.
Global Existing Instance types_intersect_pre_optional | 3.
Global Existing Instance types_intersect_pre_refinement | 3.
Global Existing Instance types_intersect_pre_struct | 3.

Global Existing Instance types_intersect_all_pre_nil | 1.
Global Existing Instance types_intersect_all_pre_cons | 2.

Global Existing Instance types_union_pre_refl | 1.
Global Existing Instance types_union_pre_int | 2.
Global Existing Instance types_union_pre_null_r | 2.
Global Existing Instance types_union_pre_null_l | 2.
Global Existing Instance types_union_pre_own | 2.
Global Existing Instance types_union_pre_optional | 2.
Global Existing Instance types_union_pre_refinement | 2.
Global Existing Instance types_union_pre_var_both | 3.
Global Existing Instance types_union_pre_var_r | 4.
Global Existing Instance types_union_pre_var_l | 4.

Global Existing Instance types_union_all_pre_nil | 1.
Global Existing Instance types_union_all_pre_cons | 2.

Global Existing Instance widen_type_bool.
Global Existing Instance widen_type_int.
Global Existing Instance widen_type_own.
Global Existing Instance widen_type_optional.
Global Existing Instance widen_type_refinement.


Global Existing Instance scheme_union_pre_l_choose.
Global Existing Instance scheme_union_pre_r_choose.
Global Existing Instance scheme_union_pre_l_done.
Global Existing Instance scheme_union_pre_r_done.


Global Existing Instance complete_types_refl | 100.
Global Existing Instance complete_types_same_type | 1.
Global Existing Instance complete_types_fill_placeholder_type_l | 2.
Global Existing Instance complete_types_fill_placeholder_type_r | 2.
Global Existing Instance complete_types_own | 3.
Global Existing Instance complete_types_struct | 3.

Global Existing Instance complete_types_all_cons.
Global Existing Instance complete_types_all_nil.
