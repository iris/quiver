From quiver.thorium.logic Require Import calls syntax.
From quiver.argon.abduction Require Import abduct proofmode pure.
From quiver.thorium.typing Require Import syntax.


Section abduction_lemmas.
  Context `{!refinedcG Σ}.


  (* When we  are faced with a function call,
     we first evaluate all function arguments. *)
  Lemma abduct_type_call_arg Δ f F Vs e es Ψ R:
    abduct Δ (rwp e (λ v A, type_call f F (Vs ++ [(v, A)]) es Ψ)) R →
    abduct Δ (type_call f F Vs (e::es) Ψ) R.
  Proof.
    by rewrite type_call_arg.
  Qed.


  (* If the function call is of the function type and
     all arguments are evaluated, then we proceed with
     a call to the function type, i.e., [type_call_ft].
     Alternatively, we have a rule for function
     predicate transformer types,
     [abduct_type_call_predicate_transformer].
  *)
  Lemma abduct_type_call_type_ft Δ f ft Vs Ψ R:
    abduct Δ (type_call_ft Vs ft Ψ) R →
    abduct Δ (type_call f (functionT ft) Vs nil Ψ) R.
  Proof. by rewrite type_call_ft_type_call. Qed.

  Lemma abduct_type_call_predicate_transformer Δ v vs lys T Ψ R :
    abduct Δ (type_call_pt vs lys T Ψ) R →
    abduct Δ (type_call v (fnT lys T) vs nil Ψ) R.
  Proof. rewrite -type_call_predicate_transformer //. Qed.

  (* REFINEDC-STYLE FUNCTIONS *)
  Lemma abduct_type_pre_call Δ vs Φ X ft Pre (R : iProp Σ):
    (∀! a, Simpl (([∗ list] v; A ∈ (vs.*1); (ft a).(fp_atys), v ◁ᵥ A) ∗ (ft a).(fp_Pa))%I (Pre a)) →
    abduct Δ (abd_assume CTX ([∗ list] p ∈ vs, p.1 ◁ᵥ p.2) (type_pre_post X RECOVER
      Pre
      (λ a, (∀ (v: val) (y: ((ft a).(fp_rtype))), abd_assume ASSUME (((ft a).(fp_fr) y).(fr_R)) (Φ v ((ft a).(fp_fr) y).(fr_rty))))))%I R →
    abduct Δ (type_call_ft vs (FT X ft) Φ) R.
  Proof.
    rewrite /abd_assume. intros Heq2 Habd.
    rewrite -type_pre_call // /abd_assume. iIntros "[HΔ R] Htys".
    iPoseProof (Habd with "[$HΔ $R]") as "Hcont".
    iSpecialize ("Hcont" with "[Htys]").
    { rewrite /ty_own_val_all. rewrite big_sepL2_fmap_l big_sepL2_fmap_r.
      clear. iInduction vs as [|[v A] vs] "IH"; simpl; auto.
      iDestruct "Htys" as "[$ Htys]". iApply "IH". iFrame. }
    rewrite /type_pre_post. iDestruct "Hcont" as "[%x Hcont]". iExists x.
    rewrite -Heq2. iFrame.
  Qed.


  (* Once we get to the postcondition, we are given a new value and
    a quantified value of type [A]. We will assume both (without lifting them
    into the precondition). If needed, the universal quantifier will be reverted
    when we revert the postcondition. *)
  Lemma abduct_type_fn_ret_intro Δ A Post Ξ (R: iProp Σ):
    abduct Δ (∀ (a: A) (v: val), Post a -∗ Ξ a v) R →
    abduct Δ (type_fn_ret A Post Ξ) R.
  Proof. by rewrite type_fn_ret_intro. Qed.


  (* PREDICATE TRANSFORMERS *)
  Lemma abduct_type_has_sizes_nil Δ P R :
    abduct Δ P R →
    abduct Δ (type_has_sizes [] [] P) R.
  Proof. rewrite -type_has_sizes_nil //. Qed.

  Lemma abduct_type_sizes_cons Δ A As ly lys P R :
    abduct Δ (type_size A (λ n, ⌜n = ly_size ly⌝ ∗ type_has_sizes As lys P))%I R →
    abduct Δ (type_has_sizes (A :: As) (ly :: lys) P) R.
  Proof. rewrite -type_has_sizes_cons //. Qed.

  Lemma type_call_pt_intro vs lys T Ψ:
      type_has_sizes vs.*2 lys
    $ abd_assume ASSERT (ty_own_val_all vs.*2 vs.*1)
    $ type_pred_transform (T vs.*1) (λ A, ∀ v, Ψ v A)
  ⊢ type_call_pt vs lys T Ψ.
  Proof.
    rewrite /type_call_pt /type_has_sizes /abd_assume. iIntros "($ & ?)".
    rewrite /type_pred_transform //.
  Qed.

  Lemma abduct_type_call_pt_intro Δ vs lys T Ψ R:
    abduct Δ (
      abd_simpl (vs.*2)
      $ λ As, abd_simpl (vs.*1)
      $ λ vals, type_has_sizes vs.*2 lys
              $ abd_assume ASSERT (ty_own_val_all As vals)
              $ type_pred_transform (T vals) (λ A, ∀ v, Ψ v A))%I R →
    abduct Δ (type_call_pt vs lys T Ψ) R.
  Proof.
    rewrite /abd_simpl -type_call_pt_intro //.
  Qed.

  Lemma abduct_type_pred_transform_intro Δ {A} P (Φ: A) (R: iProp Σ):
    abduct Δ (
      abd_evars P
    $ λ Q, abd_mark_posts type_precond_yield  Q
    $ λ R', abd_breakpoint (λ Ψ, type_pre RECOVER (R' Ψ)) (λ S, abd_evars S (λ T, T Φ))) R →
    abduct Δ (type_pred_transform P Φ) R.
  Proof.
    rewrite /type_precond /abd_evars /abd_mark_posts /abd_breakpoint.
    rewrite /type_pred_transform.
    iIntros (Habd) "Hctx". iDestruct (Habd with "Hctx") as "Hcont".
    iDestruct "Hcont" as "(%Ψ & HP & %Q & HQ & %Ξ & HΞ & (%Π & HΠ & Hcont))".
    iApply "HP". iApply "HQ". iDestruct ("HΠ" with "Hcont") as "Hcont".
    iDestruct ("HΞ" with "Hcont") as "Hcont". iDestruct "Hcont" as "(%u & Q & _)".
    iFrame.
  Qed.

End abduction_lemmas.

Section abduction_automation.
  Context `{!refinedcG Σ}.

  Global Instance abduct_step_type_call_arg Δ f F vs e es Ψ R:
    AbductStep (preR Δ e (λ v A, type_call f F (vs ++ [(v, A)]) es Ψ) R) Δ (type_call f F vs (e::es) Ψ) R.
  Proof. intros ?. by apply abduct_type_call_arg. Qed.

  Global Instance abduct_step_type_call_type_ft Δ f ft vs Ψ R:
    AbductStep (abduct Δ (type_call_ft vs ft Ψ) R) Δ (type_call f (functionT ft) vs nil Ψ) R.
  Proof. intros ?; by apply abduct_type_call_type_ft. Qed.

  Global Instance abduct_step_type_call_predicate_transformer Δ v vs lys T Ψ R :
    AbductStep (abduct Δ (type_call_pt vs lys T Ψ) R) Δ (type_call v (fnT lys T) vs nil Ψ) R.
  Proof. intros ?; by apply abduct_type_call_predicate_transformer. Qed.

  (* REFINEDC-STYLE FUNCTIONS *)
  Global Instance abduct_step_type_pre_call Δ vs Φ X ft Pre (R : iProp Σ):
    (∀! a, Simpl (([∗ list] v; A ∈ (vs.*1); (ft a).(fp_atys), v ◁ᵥ A) ∗ (ft a).(fp_Pa))%I (Pre a)) →
    AbductStep (abduct Δ (abd_assume CTX ([∗ list] p ∈ vs, p.1 ◁ᵥ p.2) (type_pre_post X RECOVER
      Pre
      (λ a, (∀ (v: val) (y: ((ft a).(fp_rtype))), abd_assume ASSUME (((ft a).(fp_fr) y).(fr_R)) (Φ v ((ft a).(fp_fr) y).(fr_rty))))))%I R)
      Δ (type_call_ft vs (FT X ft) Φ) R | 1.
  Proof. intros ??; by eapply abduct_type_pre_call. Qed.

  Global Instance abduct_step_type_fn_ret_intro Δ A Post Ξ (R: iProp Σ):
    AbductStep (abduct Δ (∀ (a: A) (v: val), abd_simpl (Post a) (λ P, P -∗ Ξ a v)) R) Δ (type_fn_ret A Post Ξ) R.
  Proof. intros ?; by apply abduct_type_fn_ret_intro. Qed.

  (* PREDICATE TRANSFORMERS *)
  Global Instance abduct_step_type_has_sizes_nil Δ P R :
    AbductStep (abduct Δ P R) Δ (type_has_sizes [] [] P) R.
  Proof. intros ?; by apply abduct_type_has_sizes_nil. Qed.

  Global Instance abduct_step_type_sizes_cons Δ A As ly lys P R :
    AbductStep (abduct Δ (type_size A (λ n, ⌜n = ly_size ly⌝ ∗ type_has_sizes As lys P))%I R)
               Δ (type_has_sizes (A :: As) (ly :: lys) P) R.
  Proof. intros ?; by apply abduct_type_sizes_cons. Qed.

  Global Instance abduct_step_type_call_pt_intro Δ vs lys T Ψ R:
    AbductStep (abduct Δ (
      abd_simpl (vs.*2)
      $ λ As, abd_simpl (vs.*1)
      $ λ vals, type_has_sizes vs.*2 lys
              $ abd_assume ASSERT (ty_own_val_all As vals)
              $ type_pred_transform (T vals) (λ A, ∀ v, Ψ v A))%I R)
    Δ (type_call_pt vs lys T Ψ) R.
  Proof. intros ?; by apply abduct_type_call_pt_intro. Qed.

  Global Instance abduct_step_type_pred_transform_intro Δ {A} P (Φ: A) (R: iProp Σ):
    AbductStep
      (abduct Δ (
        abd_evars P
      $ λ Q, abd_mark_posts type_precond_yield  Q
      $ λ R', abd_breakpoint (λ Ψ, type_pre RECOVER (R' Ψ)) (λ S, abd_evars S (λ T, T Φ))) R)
    Δ (type_pred_transform P Φ) R.
  Proof. intros ?. by eapply abduct_type_pred_transform_intro. Qed.

End abduction_automation.





