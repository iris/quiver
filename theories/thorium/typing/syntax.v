From quiver.base Require Import annotations.
From quiver.thorium.logic Require Import syntax pointers.
From quiver.argon.abduction Require Import abduct proofmode.

(* Notation for the Abduction Statements *)
Notation preR Δ e Φ R := (abduct Δ (rwp e Φ) R) (only parsing).
Notation preL Δ p Φ R := (abduct Δ (lwp p Φ) R) (only parsing).
Notation preS Δ s Q Φ R := (abduct Δ (swp s Q Φ) R) (only parsing).


Section synatx_abduction_lemmas.
  Context `{!refinedcG Σ}.

  (** values and locations *)
  Lemma abduct_loc_ctx Δ (l: loc) Δ' Φ (A: type Σ) R:
    qenvs_find_atom DEEP Δ (l ◁ₗ A) Δ' →
    abduct Δ' (Φ l A) R →
    abduct Δ (type_loc l Φ) R.
  Proof.
    rewrite /qenvs_find_atom. iIntros (Hfind Habd) "[HΔ HR]".
    rewrite Hfind. iDestruct "HΔ" as "[Hl HΔ']".
    iApply (type_loc_intro with "Hl"). iFrame.
    iApply Habd. iFrame.
  Qed.

  Lemma abduct_type_loc_find_val Δ Δ' (l: loc) A R Φ:
    qenvs_find_atom DEEP Δ (l ◁ᵥ A) Δ' →
    abduct Δ' (type_val_to_loc l A Φ) R →
    abduct Δ (type_loc l Φ) R.
  Proof.
    rewrite /abduct.
    intros Hfind Hent.
    rewrite Hfind -bi.sep_assoc Hent type_loc_find_val //.
  Qed.

  Lemma abduct_loc_missing Δ (l: loc) Φ R:
    abduct Δ (∃ A: type Σ, l ◁ₗ A ∗ Φ l A) R →
    abduct Δ (type_loc l Φ) R.
  Proof. rewrite -type_loc_missing //. Qed.

  Lemma abduct_val_ctx Δ (v: val) Δ' Φ (A: type Σ) R:
    qenvs_find_atom DEEP Δ (v ◁ᵥ A) Δ' →
    abduct Δ' (Φ v A) R →
    abduct Δ (type_val v Φ) R.
  Proof.
    rewrite /qenvs_find_atom. iIntros (Hfind Habd) "[HΔ HR]".
    rewrite Hfind. iDestruct "HΔ" as "[Hv HΔ']".
    iApply (type_val_intro with "Hv"). iFrame.
    iApply Habd. iFrame.
  Qed.

  Lemma abduct_type_val_own Δ (l: loc) Φ R:
    abduct Δ (type_loc l (λ l A, Φ (l: val) (ownT l A))) R →
    abduct Δ (type_val l Φ) R.
  Proof. intros ?; rewrite -type_val_own //. Qed.

  Lemma abduct_type_val_missing Δ v Φ R :
    exi v →
    abduct Δ (∃ A, v ◁ᵥ A ∗ Φ v A) R →
    abduct Δ (type_val v Φ) R.
  Proof. intros ??. rewrite -type_val_missing //. Qed.

  Lemma abduct_type_loc_or_val_loc Δ l Φ R:
    abduct Δ (type_loc l (λ l, Φ (LOC l))) R →
    abduct Δ (type_loc_or_val (LOC l) Φ) R.
  Proof. rewrite /type_loc /type_loc_or_val //. Qed.

  Lemma abduct_type_loc_or_val_val Δ v Φ R:
    abduct Δ (type_val v (λ w, Φ (VAL w))) R →
    abduct Δ (type_loc_or_val (VAL v) Φ) R.
  Proof. rewrite /type_loc /type_loc_or_val //. Qed.

  (* TYPE SIZE *)
  Lemma abduct_type_size_match Δ A n Φ R:
    Δ ⊨ A `has_size` n →
    abduct Δ (Φ n) R →
    abduct Δ (type_size A Φ) R.
  Proof.
    rewrite -type_size_intro.
    intros ??. by eapply abduct_prove_pure.
  Qed.

  Lemma abduct_type_size_missing Δ A Φ R:
    exi A →
    abduct Δ (∃ n, ⌜A `has_size` n⌝ ∗ Φ n) R →
    abduct Δ (type_size A Φ) R.
  Proof. rewrite type_size_missing //. Qed.

   (* TYPE SIZE *)
   Lemma abduct_type_bounds_match Δ A n Φ R:
    Δ ⊨ A `has_bounds` n →
    abduct Δ (Φ n) R →
    abduct Δ (type_bounds A Φ) R.
  Proof.
    rewrite -type_bounds_intro.
    intros ??. by eapply abduct_prove_pure.
  Qed.

  Lemma abduct_type_bounds_missing Δ A Φ R:
    exi A →
    abduct Δ (∃ n, ⌜A `has_bounds` n⌝ ∗ Φ n) R →
    abduct Δ (type_bounds A Φ) R.
  Proof. rewrite type_bounds_missing //. Qed.


  (* TYPE CONT *)
  Lemma abduct_type_cont_yield (Δ : qenvs Σ) Φ (A : type Σ) (R : iProp Σ) :
    is_var Φ →
    abduct Δ (type_garbage_collect $ abd_yield (type_cont Φ A)) R →
    abduct Δ (type_cont Φ A) R.
  Proof. rewrite /abd_yield //. Qed.

  Lemma abduct_type_cont_intro (Δ : qenvs Σ) Φ (A : type Σ) (R : iProp Σ) :
    abduct Δ (Φ A) R →
    abduct Δ (type_cont Φ A) R.
  Proof.
    rewrite -type_cont_intro //.
  Qed.

  (** syntactic constants *)
  Lemma abduct_void_literal Δ Φ R:
    abduct Δ (∀ v, Φ v voidT) R → abduct Δ (type_val VOID Φ) R.
  Proof.
    iIntros (Habd) "Hctx".
    iApply type_val_intro; first by iApply vtr_void.
    iDestruct (Habd with "Hctx") as "Hv". iApply "Hv".
  Qed.

  Lemma abduct_int_literal n it Δ Φ R:
    Δ ⊨ (n ∈ it) →
    abduct Δ (∀ v, Φ v (intT it n)) R →
    abduct Δ (type_val (i2v n it) Φ) R.
  Proof.
    intros Htriv Habd. apply: abduct_qenvs_entails.
    iIntros (Hel) "Hctx".
    iApply type_val_intro; first by iApply vtr_int.
    iDestruct (Habd with "Hctx") as "Hv". iApply "Hv".
  Qed.

  Lemma abduct_null_literal Δ Φ R:
    abduct Δ (∀ v, Φ v nullT) R → abduct Δ (type_val NULL Φ) R.
  Proof.
    iIntros (Habd) "Hctx".
    iApply type_val_intro; first by iApply vtr_null.
    iDestruct (Habd with "Hctx") as "Hv". iApply "Hv".
  Qed.

  Lemma abduct_type_val_b2v_true Δ it Φ R:
    abduct Δ (∀ v, Φ v (True%DP @ boolR it)%RT)%I R →
    abduct Δ (type_val (b2v true it) Φ) R.
  Proof. intros ?. by rewrite -type_val_b2v. Qed.

  Lemma abduct_type_val_b2v_false Δ it Φ R:
    abduct Δ (∀ v, Φ v (False%DP @ boolR it)%RT)%I R →
    abduct Δ (type_val (b2v false it) Φ) R.
  Proof. intros ?. by rewrite -type_val_b2v. Qed.

  (** bind lemmas for the syntax constructs *)
  (* l-expressions *)
  Lemma preL_load Δ (p: expr) Φ R:
    preL Δ p (λ l L, type_load l L Φ) R → preL Δ (!{PtrOp, Na1Ord, false} p)%E Φ R.
  Proof.
    iIntros (Hpre) "Hctx". iApply lwp_load. by iApply Hpre.
  Qed.

  Lemma preL_loc Δ (l: loc) Φ R:
    abduct Δ (type_loc l Φ) R → preL Δ l Φ R.
  Proof.
    iIntros (Hpre) "Hctx". iApply lwp_loc.
    by iApply Hpre.
  Qed.

  Lemma preL_get_member Δ p s m Φ R :
    preL Δ p (λ l L, type_get_member l L s m Φ) R →
    preL Δ (p at{s} m)%E Φ R.
  Proof. by rewrite lwp_get_member. Qed.

  Lemma preL_at_offset Δ e p ly ot1 ot2 Φ R :
    preR Δ e (λ v B, lwp p (λ l A, type_at_offset l v A B ly ot1 ot2 Φ)) R →
    preL Δ (p at_offset{ly, ot1, ot2} e)%E Φ R.
  Proof. by rewrite lwp_at_offset. Qed.

  (* r-expressions *)
  Lemma preR_val Δ (v: val) Ψ R:
    abduct Δ (type_val v Ψ) R → preR Δ v Ψ R.
  Proof.
    iIntros (Hpre) "Hctx". iApply rwp_val. by iApply Hpre.
  Qed.

  Lemma preR_addr_of Δ p Ψ R:
    preL Δ p (λ l L, type_addr_of l L Ψ) R → preR Δ (&p)%E Ψ R.
  Proof.
    iIntros (Hpre) "Hctx". iApply rwp_addr_of. by iApply Hpre.
  Qed.

  Lemma preR_use Δ p Ψ ot R:
    preL Δ p (λ l L, type_use l L ot Ψ) R → preR Δ (use{ot, Na1Ord, false} p)%E Ψ R.
  Proof.
    iIntros (Hpre) "Hctx". iApply rwp_use. by iApply Hpre.
  Qed.

  Lemma preR_binop Δ Ψ op ot1 ot2 e1 e2 R:
    preR Δ e1 (λ v1 R1, rwp e2 (λ v2 R2, type_bin_op op ot1 ot2 v1 R1 v2 R2 Ψ)) R →
    preR Δ (BinOp op ot1 ot2 e1 e2)%E Ψ R.
  Proof.
    iIntros (Hpre) "Hctx". iApply rwp_binop. by iApply Hpre.
  Qed.

  Lemma preR_unop Δ Ψ op ot e R:
    preR Δ e (λ v R, type_un_op op ot v R Ψ) R →
    preR Δ (UnOp op ot e)%E Ψ R.
  Proof.
    iIntros (Hpre) "Hctx". iApply rwp_unop. by iApply Hpre.
  Qed.

  Lemma preR_if_expr Δ Ψ ot e e1 e2 R:
    preR Δ e (λ v R, type_if_expr ot v R e1 e2 Ψ) R →
    preR Δ (IfE ot e e1 e2) Ψ R.
  Proof. by rewrite rwp_if_expr. Qed.


  Lemma preR_logical_and Δ Ψ ot1 ot2 rit e1 e2 R:
    preR Δ e1 (λ v R, type_logical_and ot1 ot2 rit v R e2 Ψ) R →
    preR Δ (e1 &&{ot1, ot2, rit} e2)%E Ψ R.
  Proof. by rewrite rwp_logical_and. Qed.

  Lemma preR_logical_or Δ Ψ ot1 ot2 rit e1 e2 R:
    preR Δ e1 (λ v R, type_logical_or ot1 ot2 rit v R e2 Ψ) R →
    preR Δ (e1 ||{ot1, ot2, rit} e2)%E Ψ R.
  Proof. by rewrite rwp_logical_or. Qed.

  Lemma preR_call e es Δ Ψ R :
    preR Δ e (λ f F, type_call f F nil es Ψ) R →
    preR Δ (Call e es) Ψ R.
  Proof. by rewrite rwp_call. Qed.

  Lemma preR_annot_expr e n {B} (b : B) Δ Ψ R :
    preR Δ e (λ f F, type_annot_expr b n f F Ψ) R →
    preR Δ (AnnotExpr n b e) Ψ R.
  Proof. by rewrite rwp_annot_expr. Qed.

  Lemma preR_bool_eta Δ ot e it Φ R:
    preR Δ e (λ v A, type_bool_eta ot v A it Φ) R →
    preR Δ (bool_eta ot e it) Φ R.
  Proof. by rewrite rwp_bool_eta. Qed.

  (* statements *)
  Lemma preS_goto Δ Q lb Φ R:
    abduct Δ (type_goto lb Q Φ) R →
    preS Δ (Goto lb) Q Φ R.
  Proof.
    rewrite swp_goto //.
  Qed.

  Lemma preS_loop Δ Q lb lb_exit i Φ R:
    abduct Δ (type_loop lb lb_exit i Q Φ) R →
    preS Δ (Loop lb lb_exit i) Q Φ R.
  Proof.
    rewrite swp_loop //.
  Qed.

  Lemma preS_return Δ e Q Φ R:
    preR Δ e (λ v A, type_return v A Q Φ) R →
    preS Δ (Return e) Q Φ R.
  Proof.
    rewrite swp_return //.
  Qed.

  Lemma preS_assign Δ p e s ot Q Φ R:
    preR Δ e (λ v A, lwp p (λ l B, type_assign l B v A ot s Q Φ)) R →
    preS Δ (p <-{ot} e; s)%E Q Φ R.
  Proof.
    iIntros (Hpre) "Hctx". iApply swp_assign. by iApply Hpre.
  Qed.

  Lemma preS_expr Δ e s Q Φ R:
    preR Δ e (λ v A, abd_assume CTX (v ◁ᵥ A) (swp s Q Φ))%I R →
    preS Δ (expr: e; s)%E Q Φ R.
  Proof.
    iIntros (Hpre) "Hctx". iApply swp_expr. by iApply Hpre.
  Qed.

  Lemma preS_if Δ ot join e s1 s2 Q Φ R:
    preR Δ e (λ v A, type_if_stmt v A ot join s1 s2 Q Φ)%I R →
    preS Δ (if{ot, join}: e then s1 else s2)%E Q Φ R.
  Proof. by rewrite swp_if. Qed.

  Lemma preS_skip Δ s Q Φ R:
    preS Δ s Q Φ R →
    preS Δ (SkipS s)%E Q Φ R.
  Proof. by rewrite swp_skip. Qed.

  Lemma preS_assert Δ ot e s Q Φ R:
    preR Δ e (λ v A, type_assert v A ot s Q Φ)%I R →
    preS Δ (assert{ot}: e; s)%E Q Φ R.
  Proof. by rewrite swp_assert. Qed.

  Lemma preS_annot_stmt Δ {A} (a: A) s Q Φ R:
    abduct Δ (type_annot_stmt a s Q Φ) R →
    preS Δ (annot: a; s)%E Q Φ R.
  Proof. rewrite /type_annot_stmt//. Qed.

End synatx_abduction_lemmas.





(* Notation for the Automation *)
(* NOTE: we could potentially improve the performance by making
   these separate type classes. *)
Notation LExprStep φ Δ p Φ R := (AbductStep φ Δ (lwp p Φ) R).
Notation RExprStep φ Δ e Φ R := (AbductStep φ Δ (rwp e Φ) R).
Notation StatementStep φ Δ s Q Φ R := (AbductStep φ Δ (swp s Q Φ) R).


Section automation_instances.
  Context `{!refinedcG Σ}.

  (* values and locations *)
  Global Instance abduct_step_find_loc Δ l Δ' (A: type Σ) Φ R:
    qenvs_find_atom DEEP Δ (l ◁ₗ A) Δ' →
    AbductStep (abduct Δ' (Φ l A) R) Δ (type_loc l Φ) R | 1.
  Proof. intros ??. by eapply abduct_loc_ctx. Qed.

  Global Instance abduct_step_type_loc_find_val Δ Δ' (l: loc) A R Φ:
    qenvs_find_atom DEEP Δ (l ◁ᵥ A) Δ' →
    AbductStep (abduct Δ' (type_val_to_loc l A Φ) R) Δ (type_loc l Φ) R | 2.
  Proof. intros ??; by eapply abduct_type_loc_find_val. Qed.

  Global Instance abduct_step_miss_loc Δ (l: loc) Φ R:
    AbductStep (abduct Δ (∃ A: type Σ, l ◁ₗ A ∗ Φ l A) R) Δ (type_loc l Φ) R | 10.
  Proof. intros ?. by eapply abduct_loc_missing. Qed.

  (* VAL *)
  Global Instance abduct_step_find_val Δ v Δ' (A: type Σ) Φ R:
    qenvs_find_atom DEEP Δ (v ◁ᵥ A) Δ' →
    AbductStep (abduct Δ' (Φ v A) R) Δ (type_val v Φ) R | 2.
  Proof. intros ??. by eapply abduct_val_ctx. Qed.

  Global Instance abduct_step_type_val_own Δ (l: loc) Φ R:
    AbductStep (abduct Δ (type_loc l (λ l A, Φ (l: val) (ownT l A))) R) Δ (type_val l Φ) R | 3.
  Proof. intros ?; by eapply abduct_type_val_own. Qed.

  Global Instance abduct_step_type_vtr_missing Δ v Φ R :
    exi v →
    AbductStep (abduct Δ (∃ A, v ◁ᵥ A ∗ Φ v A) R)
      Δ (type_val v Φ) R | 10000.
  Proof. intros ??. by eapply abduct_type_val_missing. Qed.

  (* SIZES *)
  Global Instance abduct_step_type_size_match Δ A n Φ R:
    Δ ⊨ A `has_size` n →
    AbductStep (abduct Δ (Φ n) R) Δ (type_size A Φ) R.
  Proof. intros ??. by eapply abduct_type_size_match. Qed.

  Global Instance abduct_step_type_size_missing Δ A Φ R:
    exi A →
    AbductStep (abduct Δ (∃ n, ⌜A `has_size` n⌝ ∗ Φ n) R)
      Δ (type_size A Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_size_missing. Qed.

  (* BOUNDS *)
  Global Instance abduct_step_type_bounds_match Δ A n Φ R:
    Δ ⊨ A `has_bounds` n →
    AbductStep (abduct Δ (Φ n) R) Δ (type_bounds A Φ) R.
  Proof. intros ??. by eapply abduct_type_bounds_match. Qed.

  Global Instance abduct_step_type_bounds_missing Δ A Φ R:
    exi A →
    AbductStep (abduct Δ (∃ n, ⌜A `has_bounds` n⌝ ∗ Φ n) R)
      Δ (type_bounds A Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_bounds_missing. Qed.


  (* VAL_OR_LOC *)
  Global Instance abduct_step_type_loc_or_val_loc Δ l Φ R:
    AbductStep (abduct Δ (type_loc l (λ l, Φ (LOC l))) R) Δ (type_loc_or_val (LOC l) Φ) R | 1.
  Proof. intros ?; by eapply abduct_type_loc_or_val_loc. Qed.

  Global Instance abduct_step_type_loc_or_val_val Δ v Φ R:
    AbductStep (abduct Δ (type_val v (λ w, Φ (VAL w))) R) Δ (type_loc_or_val (VAL v) Φ) R | 1.
  Proof. intros ?; by eapply abduct_type_loc_or_val_val. Qed.

  (* TYPE CONT *)
  Global Instance abduct_step_type_cont_yield Δ Φ A R :
    is_var Φ →
    AbductStep (abduct Δ (type_garbage_collect $ abd_yield (type_cont Φ A)) R) Δ (type_cont Φ A) R | 1.
  Proof. intros ??; by eapply abduct_type_cont_yield. Qed.

  Global Instance abduct_step_type_cont_intro Δ Φ A R :
    AbductStep (abduct Δ (Φ A) R) Δ (type_cont Φ A) R | 2.
  Proof. intros ?; by eapply abduct_type_cont_intro. Qed.

  (* syntactic constants *)
  Global Instance abduct_step_void_literal Δ Φ R:
    AbductStep (abduct Δ (∀ v, Φ v voidT) R) Δ (type_val VOID Φ) R.
  Proof. intros Habd. by apply abduct_void_literal. Qed.

  Global Instance abduct_step_int_literal n it Δ Φ R:
    Δ ⊨ Trivial (n ∈ it) →
    AbductStep (abduct Δ (∀ v, Φ v (intT it n)) R) Δ (type_val (i2v n it) Φ) R.
  Proof. intros Hran Habd. by apply abduct_int_literal. Qed.

  Global Instance abduct_step_null_literal Δ Φ R:
    AbductStep (abduct Δ (∀ v, Φ v nullT) R) Δ (type_val NULL Φ) R.
  Proof. intros Habd. by apply abduct_null_literal. Qed.

  Global Instance abduct_step_type_val_b2v_true Δ it Φ R:
    AbductStep (abduct Δ (∀ v, Φ v (True%DP @ boolR it)%RT)%I R) Δ (type_val (b2v true it) Φ) R.
  Proof. intros ?. by eapply abduct_type_val_b2v_true. Qed.

  Global Instance abduct_step_type_val_b2v_false Δ it Φ R:
    AbductStep (abduct Δ (∀ v, Φ v (False%DP @ boolR it)%RT)%I R) Δ (type_val (b2v false it) Φ) R.
  Proof. intros ?. by eapply abduct_type_val_b2v_false. Qed.

  (* l-expressions *)
  Global Instance lexpr_step_loc Δ l Φ R:
    LExprStep (abduct Δ (type_loc l Φ) R) Δ l Φ R.
  Proof. intros ?. by eapply preL_loc. Qed.

  Global Instance lexpr_step_load Δ p Φ R:
    LExprStep (preL Δ p (λ l L, type_load l L Φ) R) Δ (!{PtrOp, Na1Ord, false} p)%E Φ R.
  Proof. intros ?. by eapply preL_load. Qed.

  Global Instance lexpr_step_get_member Δ p s m Φ R :
    LExprStep (preL Δ p (λ l L, type_get_member l L s m Φ) R) Δ (p at{s} m)%E Φ R.
  Proof. intros ?. by eapply preL_get_member. Qed.

  Global Instance lexpr_step_at_offset Δ e p ly ot1 ot2 Φ R :
    LExprStep (preR Δ e (λ v B, lwp p (λ l A, type_at_offset l v A B ly ot1 ot2 Φ)) R) Δ (p at_offset{ly, ot1, ot2} e)%E Φ R.
  Proof. intros ?. by eapply preL_at_offset. Qed.

  (* r-expressions *)
  Global Instance rexpr_step_val Δ v Φ R:
    RExprStep (abduct Δ (type_val v Φ) R) Δ v Φ R.
  Proof. intros ?. by eapply preR_val. Qed.

  Global Instance rexpr_step_addr_of Δ p Ψ R:
    RExprStep (preL Δ p (λ l L, type_addr_of l L Ψ) R) Δ (&p)%E Ψ R.
  Proof. intros ?. by eapply preR_addr_of. Qed.

  Global Instance rexpr_step_use Δ p ot Ψ R:
    RExprStep (preL Δ p (λ l L, type_use l L ot Ψ) R) Δ (use{ot, Na1Ord, false} p)%E Ψ R.
  Proof. intros ?. by eapply preR_use. Qed.

  Global Instance rexpr_step_binop Δ Ψ op ot1 ot2 e1 e2 R:
    RExprStep (preR Δ e1 (λ v1 R1, rwp e2 (λ v2 R2, type_bin_op op ot1 ot2 v1 R1 v2 R2 Ψ)) R) Δ (BinOp op ot1 ot2 e1 e2)%E Ψ R.
  Proof. intros ?. by eapply preR_binop. Qed.

  Global Instance rexpr_step_unop Δ Ψ op ot e R:
    RExprStep (preR Δ e (λ v R, type_un_op op ot v R Ψ) R) Δ (UnOp op ot e) Ψ R.
  Proof. intros ?. by eapply preR_unop. Qed.

  Global Instance rexpr_step_if_expr Δ Ψ ot e e1 e2 R:
    RExprStep (preR Δ e (λ v R, type_if_expr ot v R e1 e2 Ψ) R) Δ (IfE ot e e1 e2) Ψ R.
  Proof. intros ?. by eapply preR_if_expr. Qed.

  Global Instance rexpr_step_logical_and Δ Ψ ot1 ot2 rit e1 e2 R:
    RExprStep (preR Δ e1 (λ v R, type_logical_and ot1 ot2 rit v R e2 Ψ) R) Δ (e1 &&{ot1, ot2, rit} e2)%E Ψ R.
  Proof. intros ?. by eapply preR_logical_and. Qed.

  Global Instance rexpr_step_logical_or Δ Ψ ot1 ot2 rit e1 e2 R:
    RExprStep (preR Δ e1 (λ v R, type_logical_or ot1 ot2 rit v R e2 Ψ) R) Δ (e1 ||{ot1, ot2, rit} e2)%E Ψ R.
  Proof. intros ?. by eapply preR_logical_or. Qed.

  Global Instance rexpr_step_call e es Δ Ψ R :
    RExprStep (preR Δ e (λ f F, type_call f F nil es Ψ) R) Δ (Call e es) Ψ R.
  Proof. intros ?. by eapply preR_call. Qed.

  Global Instance rexpr_step_annot e n {B} (b : B) Δ Ψ R :
    RExprStep (preR Δ e (λ f F, type_annot_expr b n f F Ψ) R) Δ (AnnotExpr n b e) Ψ R.
  Proof. intros ?. by eapply preR_annot_expr. Qed.

  Global Instance abduct_step_rwp_bool_eta Δ ot e it Φ R:
    RExprStep (preR Δ e (λ v A, type_bool_eta ot v A it Φ) R) Δ (bool_eta ot e it) Φ R.
  Proof. intros ?. by eapply preR_bool_eta. Qed.


  (* statements *)
  Global Instance statement_step_goto Δ Q lb Φ R:
    StatementStep (abduct Δ (type_goto lb Q Φ) R) Δ (Goto lb) Q Φ R.
  Proof. intros ?. by eapply preS_goto. Qed.

  Global Instance statement_step_loop Δ Q lb lb_exit i Φ R:
    StatementStep (abduct Δ (type_loop lb lb_exit i Q Φ) R) Δ (Loop lb lb_exit i) Q Φ R.
  Proof. intros ?. by eapply preS_loop. Qed.

  Global Instance statement_step_return Δ e Q Φ R:
    StatementStep (preR Δ e (λ v A, type_return v A Q Φ) R) Δ (Return e) Q Φ R.
  Proof. intros ?. by eapply preS_return. Qed.

  Global Instance statement_step_assign Δ p e s ot Q Φ R:
    StatementStep (preR Δ e (λ v A, lwp p (λ l B, type_assign l B v A ot s Q Φ)) R) Δ (p <-{ot} e; s)%E Q Φ R.
  Proof. intros ?. by eapply preS_assign. Qed.

  Global Instance statement_step_expr Δ e s Q Φ R:
    StatementStep (preR Δ e (λ v A, abd_assume CTX (v ◁ᵥ A) (swp s Q Φ))%I R) Δ (expr: e; s)%E Q Φ R.
  Proof. intros ?. by eapply preS_expr. Qed.

  Global Instance statement_step_if Δ ot join e s1 s2 Q Φ R:
    StatementStep (preR Δ e (λ v A, type_if_stmt v A ot join s1 s2 Q Φ)%I R) Δ (if{ot, join}: e then s1 else s2)%E Q Φ R | 1.
  Proof. intros ?. by eapply preS_if. Qed.

  Global Instance statement_step_skip Δ s Q Φ R:
    StatementStep (preS Δ s Q Φ R) Δ (SkipS s)%E Q Φ R.
  Proof. intros ?. by eapply preS_skip. Qed.

  Global Instance statement_step_assert Δ ot e s Q Φ R:
    StatementStep (preR Δ e (λ v A, type_assert v A ot s Q Φ)%I R) Δ (assert{ot}: e; s)%E Q Φ R.
  Proof. intros ?. by eapply preS_assert. Qed.

  Global Instance statement_step_annot_stmt Δ {A} (a: A) s Q Φ R:
    StatementStep (abduct Δ (type_annot_stmt a s Q Φ)%I R) Δ (annot: a; s)%E Q Φ R.
  Proof. intros ?. by eapply preS_annot_stmt. Qed.
End automation_instances.
