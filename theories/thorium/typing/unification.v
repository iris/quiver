From quiver.thorium.types Require Import types base_types combinators pointers structs arrays.
From quiver.argon.abduction Require Export proofmode.
From quiver.thorium.typing Require Export type_cast.


Class is_logic_evar {X: Type} (x: X).
Global Hint Mode is_logic_evar - ! : typeclass_instances.

Definition levar {X: Type} (x: X) := x.
Global Typeclasses Opaque levar.

Global Instance future_evar_fevar {X: Type} (x: X) :
  is_logic_evar (levar x) := {}.

Global Instance future_evar_fst {X Y: Type} (x: X * Y) :
  is_logic_evar x → is_logic_evar (x.1) := {}.

Global Instance future_evar_snd {X Y: Type} (x: X * Y) :
  is_logic_evar x → is_logic_evar (x.2) := {}.



(* unification *)
Section unifiable.
  Context `{!refinedcG Σ}.

  Definition unifiable (Δ: qenvs Σ) (x: loc_or_val_type) (A B : type Σ) : Prop := sealed True.
  Definition unifiable_all (Δ: qenvs Σ) (x: loc_or_val_type) (A B : list (type Σ)) : Prop := sealed True.
  Definition unifiable_scheme (Δ: qenvs Σ) (x: loc_or_val_type) (A: type Σ) (T: Type) (S: T → type Σ)  : Prop := sealed True.

  (* This lemma should _never_ be an instance of the type class. *)
  Lemma unify_types Δ x A B : unifiable x Δ A B.
  Proof.
    rewrite /unifiable sealed_eq //.
  Qed.

  (* the instances for checking unifiability *)
  Lemma unifiable_var Δ x A B:
    exi A → unifiable Δ x A B.
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_scheme_logic_evar Δ x (A B: type Σ):
    is_logic_evar B →
    unifiable Δ x A B.
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_int Δ x it1 it2 n m:
    unifiable Δ x (intT it1 n) (intT it2 m).
  Proof. apply unify_types. Qed.

  Lemma unifiable_bool Δ x it1 it2 b1 b2:
    unifiable Δ x (boolT it1 b1) (boolT it2 b2).
  Proof. apply unify_types. Qed.

  Lemma unifiable_bool_int1 Δ it x b :
    unifiable Δ x (int[it] 1) (bool[it] b).
  Proof. apply unify_types. Qed.
  Lemma unifiable_bool_int2 Δ it x b :
    unifiable Δ x (int[it] 0) (bool[it] b).
  Proof. apply unify_types. Qed.

  Lemma unifiable_null Δ x : unifiable Δ x nullT nullT.
  Proof. apply unify_types. Qed.

  Lemma unifiable_void Δ x : unifiable Δ x voidT voidT.
  Proof. apply unify_types. Qed.

  Lemma unifiable_any Δ A x n:
    Δ ⊨ A `has_size` n →
    unifiable Δ x A (anyT n).
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_own Δ (A B: type Σ) x p q:
    unifiable Δ x A B →
    unifiable Δ x (ownT p A) (ownT q B).
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_optional Δ x (A B: type Σ) φ ψ:
    unifiable Δ x A B →
    unifiable Δ x (optionalT φ A) (optionalT ψ B).
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_exists_right Δ y X (S: X → type Σ) (A: type Σ):
    (∀ x, unifiable Δ y A (S (levar x))) →
    unifiable Δ y A (existsT S).
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_exists_left Δ X y (A: X → type Σ) (B: type Σ):
    (∀ x, unifiable Δ y (A x) B) →
    unifiable Δ y (existsT A) B.
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_constraint_right Δ y (A B: type Σ) φ:
    unifiable Δ y A B →
    unifiable Δ y A (constraintT B φ).
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_constraint_left Δ y (A B: type Σ) φ:
    unifiable Δ y A B →
    unifiable Δ y (constraintT A φ) B.
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_sep_right Δ y (A B: type Σ) P:
    unifiable Δ y A B →
    unifiable Δ y A (sepT B P).
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_sep_left Δ y (A B: type Σ) P:
    unifiable Δ y A B →
    unifiable Δ y (sepT A P) B.
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_struct Δ y (A B: list (type Σ)) sl1 sl2:
    unifiable_all Δ y A B →
    unifiable Δ y (structT sl1 A) (structT sl2 B).
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_type_array Δ y (A B: list (type Σ)) ly1 ly2:
    unifiable_all Δ y A B →
    unifiable Δ y (type_arrayT ly1 A) (type_arrayT ly2 B).
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_array {X: Type} Δ y (T: X → type Σ) xs ys n m:
    unifiable Δ y (arrayT T n xs) (arrayT T m ys).
  Proof. apply unify_types. Qed.

  Lemma unifiable_fix_left Δ {X: Type} (F: (X → type Σ) → X → type Σ) `{!Inhabited X} `{!TypeMono F} x y (B: type Σ):
    (∀ R, unifiable Δ y (F R x) B) →
    unifiable Δ y (x @ fixR F) B.
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_fix_right Δ {X: Type} (F: (X → type Σ) → X → type Σ) `{!Inhabited X} `{!TypeMono F} x y (A: type Σ):
    (∀ R, unifiable Δ y A (F R x)) →
    unifiable Δ y A (x @ fixR F).
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_lam_left Δ {X: Type} (F: X → type Σ) `{!Inhabited X} x y (B: type Σ):
    (unifiable Δ y (F x) B) →
    unifiable Δ y (x @ lamR F) B.
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_lam_right Δ {X: Type} (F: X → type Σ) `{!Inhabited X} x y (A: type Σ):
    (unifiable Δ y A (F x)) →
    unifiable Δ y A (x @ lamR F).
  Proof. intros ?; apply unify_types. Qed.

  Lemma unifiable_refinement Δ {X} (R1 R2: rtype Σ X) lv (x y: X):
    unify R1 R2 →
    unifiable Δ lv (x @ R1) (y @ R2).
  Proof.  intros ?. apply unify_types. Qed.


  Lemma unifiable_value_val Δ Δ' y v ot (A B: type Σ) :
    qenvs_find_atom DEEP Δ (v ◁ᵥ A) Δ' →
    unifiable Δ y A B →
    unifiable Δ y (valueT v ot)  B.
  Proof. intros ? ?; apply unify_types. Qed.

  Lemma unifiable_value_loc Δ Δ' y l (A B: type Σ) :
    qenvs_find_atom DEEP Δ (l ◁ₗ A) Δ' →
    unifiable Δ' y (ownT l A) B →
    unifiable Δ y (valueT l ptr_size) B.
  Proof. intros ? ?; apply unify_types. Qed.

  Lemma unifiable_place Δ Δ' l (A B: type Σ) :
    qenvs_find_atom DEEP Δ (l ◁ₗ A) Δ' →
    unifiable Δ' LOC_TYPE A B →
    unifiable Δ LOC_TYPE (placeT l) B.
  Proof. intros ? ?; apply unify_types. Qed.


  Lemma unifiable_opt Δ y v w :
    unifiable Δ y (optT v) (optT w).
  Proof. apply unify_types. Qed.


  Lemma unify_all_types Δ A B y : unifiable_all Δ y A B.
  Proof.
    rewrite /unifiable_all sealed_eq //.
  Qed.

  Lemma unifiable_all_nil Δ y : unifiable_all Δ y [] [].
  Proof. apply unify_all_types. Qed.

  Lemma unifiable_all_cons Δ y (A B: type Σ) (A': list (type Σ)) (B': list (type Σ)):
    unifiable Δ y A B →
    unifiable_all Δ y A' B' →
    unifiable_all Δ y (A :: A') (B :: B').
  Proof. intros ? ?; apply unify_all_types. Qed.

  Lemma unifiable_all_app Δ y (A B: list (type Σ)) (A': list (type Σ)) (B': list (type Σ)):
    unifiable_all Δ y A B →
    unifiable_all Δ y A' B' →
    unifiable_all Δ y (A ++ A') (B ++ B').
  Proof. intros ? ?; apply unify_all_types. Qed.

  Lemma unifiable_all_var Δ y (A B: list (type Σ)):
    exi A →
    unifiable_all Δ y A B.
  Proof. intros ?; apply unify_all_types. Qed.

  (* unifiable schemes *)
  Lemma unifiable_scheme_choose Δ y T (A: type Σ) B:
    (∀ x: T, unifiable Δ y A (B (levar x))) →
    unifiable_scheme Δ y A T B.
  Proof. intros ?. rewrite /unifiable_scheme sealed_eq //. Qed.

End unifiable.

Existing Class unifiable.
Global Hint Mode unifiable - - - + + + : typeclass_instances.
Global Existing Instance unifiable_var | 1.
Global Existing Instance unifiable_scheme_logic_evar | 1.
Global Existing Instance unifiable_int | 1.
Global Existing Instance unifiable_bool_int1 | 1.
Global Existing Instance unifiable_bool_int2 | 1.
Global Existing Instance unifiable_bool | 1.
Global Existing Instance unifiable_null | 1.
Global Existing Instance unifiable_void | 1.
Global Existing Instance unifiable_any | 1.
Global Existing Instance unifiable_own | 1.
Global Existing Instance unifiable_optional | 1.
Global Existing Instance unifiable_exists_right | 1.
Global Existing Instance unifiable_exists_left | 1.
Global Existing Instance unifiable_constraint_right | 1.
Global Existing Instance unifiable_constraint_left | 1.
Global Existing Instance unifiable_sep_right | 1.
Global Existing Instance unifiable_sep_left | 1.
Global Existing Instance unifiable_struct | 1.
Global Existing Instance unifiable_type_array | 1.
Global Existing Instance unifiable_array | 1.
Global Existing Instance unifiable_fix_left | 2.
Global Existing Instance unifiable_fix_right | 2.
Global Existing Instance unifiable_lam_left | 2.
Global Existing Instance unifiable_lam_right | 2.
Global Existing Instance unifiable_refinement | 1.
Global Existing Instance unifiable_value_loc | 1.
Global Existing Instance unifiable_value_val | 2.
Global Existing Instance unifiable_place | 1.
Global Existing Instance unifiable_opt | 1.

Existing Class unifiable_all.
Global Hint Mode unifiable_all - - - - + + : typeclass_instances.
Global Existing Instance unifiable_all_nil | 1.
Global Existing Instance unifiable_all_cons | 1.
Global Existing Instance unifiable_all_app | 1.
Global Existing Instance unifiable_all_var | 1.

Existing Class unifiable_scheme.
Global Hint Mode unifiable_scheme - - - + + - + : typeclass_instances.
Global Existing Instance unifiable_scheme_choose | 1.

Section type_unification.
  Context `{!refinedcG Σ}.

  (* typeclass to check whether we can unify already upto evars *)
  Definition instantiate_scheme (A: type Σ) (T: Type) (B: T → type Σ) (t: T) :=
    ∀ x Φ, (Φ t ⊢ type_unify x RECOVER A T B Φ).

  Lemma instantiate_scheme_refl T A t:
    instantiate_scheme (A t) T A t.
  Proof.
    intros x Φ. rewrite /type_unify /type_pre_type /type_pre_post.
    iIntros "H1 H2". iExists _. iFrame.
  Qed.

  Lemma instantiate_scheme_any T A `{!Inhabited T}:
    instantiate_scheme A T (λ _, A) inhabitant.
  Proof.
    intros x Φ. rewrite /type_unify /type_pre_type /type_pre_post.
    iIntros "H1 H2". iExists _. iFrame.
  Qed.

  Lemma instantiate_scheme_pair A T1 T2 B t1 t2 :
    instantiate_scheme A T1 (λ t1, B (t1, t2)) t1 →
    instantiate_scheme A (T1 * T2) B (t1, t2).
  Proof.
    rewrite /instantiate_scheme /type_unify /type_pre_type /type_pre_post. intros ? x Φ.
    iIntros "H1 H2". iDestruct (H _ (λ t1, Φ (t1, t2)) with "H1 H2") as "[%t1' H]".
    iExists _. iFrame.
  Qed.

  (* ideally, we can simply instantiate the scheme *)
  Lemma abduct_type_unify_evars Δ x A T B Φ t R:
    instantiate_scheme A T B t →
    abduct Δ (Φ t) R →
    abduct Δ (type_unify x RECOVER A T B Φ) R.
  Proof.
    intros Hinst Habd. iIntros "Hctx".
    iApply Hinst. iDestruct (Habd with "Hctx") as "$".
  Qed.

  (* if not, we trigger a type cast *)
  Lemma abduct_type_unify_cast Δ x A T B Φ R :
    abduct Δ (x ◁ₓ A -∗ type_pre_type x RECOVER T B Φ) R →
    abduct Δ (type_unify x RECOVER A T B Φ) R.
  Proof. done. Qed.

  Global Instance abduct_step_type_unify_evars Δ x A T B t Φ R:
    instantiate_scheme A T B t →
    AbductStep (abduct Δ (Φ t) R) Δ (type_unify x RECOVER A T B Φ) R | 1.
  Proof. intros ??. by eapply abduct_type_unify_evars. Qed.

  Global Instance abduct_step_type_unify_cast Δ x A T B Φ R :
    AbductStep (abduct Δ (x ◁ₓ A -∗ type_pre_type x RECOVER T B Φ) R) Δ (type_unify x RECOVER A T B Φ) R | 2.
  Proof. intros ?. by eapply abduct_type_unify_cast. Qed.

End type_unification.


Existing Class instantiate_scheme.
Global Hint Mode instantiate_scheme - - - ! - - : typeclass_instances.
Global Existing Instance instantiate_scheme_refl  | 2.
Global Existing Instance instantiate_scheme_any   | 1.
Global Existing Instance instantiate_scheme_pair  | 1.
