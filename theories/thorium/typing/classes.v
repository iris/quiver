From quiver.base Require Import facts.
From quiver.argon.base Require Import classes.
From quiver.thorium.types Require Import
  types base_types combinators pointers structs arrays refinements bytes.
From quiver.argon.simplification Require Import simplify elim_existentials cleanup.
From quiver.argon.abduction Require Import abduct proofmode precond.

(* Automation for location equality testing [loc_eq] *)
Definition loc_eq (l1 l2 : loc) (n1 n2 : Z) : Prop :=
  l1 +ₗ n1 = l2 +ₗ n2.

Global Typeclasses Opaque loc_eq.

Section loc_eq.
  Lemma loc_eq_shift_l l1 l2 n1 n1' n2 :
    loc_eq l1 l2 (n1' + n1) n2 →
    loc_eq (l1 +ₗ n1') l2 n1 n2.
  Proof. by rewrite /loc_eq shift_loc_assoc. Qed.

  Lemma loc_eq_shift_r l1 l2 n1 n2 n2' :
    loc_eq l1 l2 n1 (n2' + n2) →
    loc_eq l1 (l2 +ₗ n2') n1 n2.
  Proof. by rewrite /loc_eq shift_loc_assoc. Qed.

  Lemma loc_eq_shift_0 l1 l2 n1 n2 :
    (* TODO: Should this instead generate a sidecondiiton that n1 =
    n2? But this should only be attempted to be solved once we know
    that l1 and l2 are for the same base location. *)
    loc_eq l1 l2 n1 n2 →
    loc_eq (l1 +ₗ 0) (l2 +ₗ 0) n1 n2.
  Proof. rewrite /loc_eq !shift_loc_0 => ->. done. Qed.

  Lemma loc_eq_end l n1 n2 :
    n1 = n2 →
    loc_eq l l n1 n2.
  Proof. by rewrite /loc_eq => ->. Qed.
End loc_eq.

Ltac is_zero x := idtac; lazymatch x with | 0%Z => idtac end.

Ltac loc_eq_step :=
  match goal with
  (* +ₗ 0 is used as a guard in some rules so it has to match up *)
  | |- loc_eq (_ +ₗ ?x) (_ +ₗ ?y) _ _ => is_zero x; is_zero y; apply loc_eq_shift_0
  | |- loc_eq (_ +ₗ ?x) _ _ _ => assert_fails (is_zero x); apply loc_eq_shift_l
  | |- loc_eq _ (_ +ₗ ?x) _ _ => assert_fails (is_zero x); apply loc_eq_shift_r
  end.

Ltac loc_eq :=
  repeat loc_eq_step;
  lazymatch goal with | |- loc_eq ?l ?l _ _ => apply loc_eq_end end;
  solve_solve.



(* Thorium context search *)
Section thorium_context_search.
  Context `{!refinedcG Σ}.

  Global Instance ltr_atom l (A: type Σ):
    Atom (l ◁ₗ A).
  Proof. by apply is_atom. Qed.

  Global Instance vtr_atom v (A: type Σ):
    Atom (v ◁ᵥ A).
  Proof. by apply is_atom. Qed.

  Global Instance vty_atom (A: type Σ):
    Atom (vty A).
  Proof. by apply is_atom. Qed.



  Global Instance match_atom_xtr_loc l A:
    MatchAtom (l ◁ₗ A) (LOC l ◁ₓ A).
  Proof. done. Qed.

  Global Instance match_atom_xtr_val v A:
    MatchAtom (v ◁ᵥ A) (VAL v ◁ₓ A).
  Proof. done. Qed.

  (* Must be declared via Hint Extern since the location might be an evar *)
  Lemma match_atom_loc_eq l1 l2 n1 n2 A :
    loc_eq l1 l2 n1 n2 →
    MatchAtom ((l1 +ₗ n1) ◁ₗ A) ((l2 +ₗ n2) ◁ₗ A).
  Proof. rewrite /Solve /MatchAtom /loc_eq => ->//. Qed.

  (* class for finding atoms in types *)
  Class FindAtomInType (x: loc_or_val) (A: type Σ) (P: iProp Σ) (B: type Σ) :=
    mk_find_in_type: x ◁ₓ A ⊣⊢ P ∗ x ◁ₓ B.

  Class FindAtomInList (x: list loc_or_val) (A: list (type Σ)) (P: iProp Σ) (B: list (type Σ)) :=
    {
      find_in_list_len: length A = length B;
      find_in_list_iff:  xtr_all x A ⊣⊢ P ∗ xtr_all x B
    }.

  Global Instance find_in_prop_ltr l A B P:
    FindAtomInType (LOC l) A P B →
    FindAtomInProp (l ◁ₗ A) P (l ◁ₗ B) | 2.
    rewrite /FindAtomInType /FindAtomInProp /=. intros Hfind.
    rewrite Hfind //.
  Qed.

  Global Instance find_in_prop_vtr v A B P:
    FindAtomInType (VAL v) A P B →
    FindAtomInProp (v ◁ᵥ A) P (v ◁ᵥ B) | 2.
  Proof.
    rewrite /FindAtomInType /FindAtomInProp /=. intros Hfind.
    rewrite Hfind //.
  Qed.

  Global Instance find_in_prop_xtr x A B P:
    FindAtomInType x A P B →
    FindAtomInProp (x ◁ₓ A) P (x ◁ₓ B) | 2.
  Proof.
    rewrite /FindAtomInType /FindAtomInProp /=. intros Hfind.
    rewrite Hfind //.
  Qed.

  Global Instance find_in_list_head x xs A As B P:
    FindAtomInType x A P B →
    FindAtomInList (x :: xs) (A :: As) P (B :: As) | 1.
  Proof.
    rewrite /FindAtomInType /=. intros Hfind. split; first done.
    rewrite !xtr_all_cons Hfind bi.sep_assoc //.
  Qed.

  Global Instance find_in_list_tail x xs A As Bs P:
    FindAtomInList xs As P Bs →
    FindAtomInList (x :: xs) (A :: As) P (A :: Bs) | 2.
  Proof.
    rewrite /FindAtomInType /=. intros [Hlen Hfind].
    split; first by simpl; congruence.
    rewrite !xtr_all_cons Hfind bi.sep_assoc (bi.sep_comm _ P) -bi.sep_assoc //.
  Qed.

End thorium_context_search.

Global Hint Mode FindAtomInType - - ! ! ! - : typeclass_instances.
Global Hint Mode FindAtomInList - - ! ! ! - : typeclass_instances.


Global Hint Extern 20 (MatchAtom ((?l1 +ₗ _) ◁ₗ _) ((?l2 +ₗ _) ◁ₗ _)) =>
  (is_ground l1; is_ground l2; notypeclasses refine (match_atom_loc_eq _ _ _ _ _ _); loc_eq)
    : typeclass_instances.



Section find_atoms.
  Context `{!refinedcG Σ}.

  Global Instance find_atom_in_owned x p A P :
    MatchAtom (p ◁ₗ A) P →
    FindAtomInType x (ownT p A) P (valueT p ptr_size) | 1.
  Proof.
    rewrite /MatchAtom /FindAtomInType. intros Hfind.
    rewrite -value_own_xtr. rewrite Hfind.
    rewrite bi.sep_comm //.
  Qed.

  Global Instance find_atom_in_own x p A B P :
    FindAtomInType (LOC p) A P B →
    FindAtomInType x (ownT p A) P (ownT p B) | 2.
  Proof.
    rewrite /FindAtomInType /=. intros Hfind.
    rewrite -!value_own_xtr. rewrite Hfind.
    iSplit; iIntros "($ & $ & $)".
  Qed.

  Global Instance find_atom_place p A P :
    sequence_classes [MatchAtom (p ◁ₗ A) P; TCUnless (unify A (placeT p))] →
    FindAtomInType (LOC p) A P (placeT p) | 1.
  Proof.
    rewrite /sequence_classes !Forall_cons /=.
    rewrite /MatchAtom /FindAtomInType /=. intros (Hfind & _).
    rewrite Hfind. iSplit.
    - iIntros "$". iApply placeT_create.
    - iIntros "[$ _]".
  Qed.

  Global Instance find_atom_sep_l x A P Q B:
    FindAtomInType x A P B →
    FindAtomInType x (sepT A Q) P (sepT B Q).
  Proof.
    rewrite /FindAtomInType /=. intros Hfind.
    rewrite !xtr_sepT_iff Hfind. rewrite bi.sep_assoc //.
  Qed.

  Global Instance find_atom_sep_r x A P Q R:
    FindAtomInProp Q P R →
    FindAtomInType x (sepT A Q) P (sepT A R).
  Proof.
    rewrite /FindAtomInType /=. intros Hfind.
    rewrite !xtr_sepT_iff Hfind.
    iSplit; iIntros "($ & $ & $)".
  Qed.

  Global Instance find_atom_constraint x A B P φ:
    FindAtomInType x A P B →
    FindAtomInType x (constraintT A φ) P (constraintT B φ).
  Proof.
    rewrite /FindAtomInType /=. intros Hfind.
    rewrite !xtr_constraintT_iff Hfind -bi.sep_assoc.
    iSplit; iIntros "($ & $ & $)".
  Qed.

  Global Instance find_atom_struct_loc p sl Xs As P Bs:
    Simpl (map LOC (member_locs p sl)) Xs →
    FindAtomInList Xs As P Bs →
    FindAtomInType (LOC p) (structT sl As) P (structT sl Bs).
  Proof.
    rewrite /FindAtomInType /Simpl /=. intros <- [Hlen Hfind].
    iSplit.
    - iIntros "Hp". iDestruct (struct_focus_ty_own_all with "Hp") as "[Hall Hcont]".
      rewrite !xtr_all_fmap_loc in Hfind. rewrite Hfind.
      iDestruct "Hall" as "[$ Hall]".
      iApply "Hcont". iFrame.
    - iIntros "[HP Hp]". iDestruct (struct_focus_ty_own_all with "Hp") as "[Hall Hcont]".
      rewrite !xtr_all_fmap_loc in Hfind. iCombine "HP Hall" as "Hall".
      rewrite -Hfind. iApply "Hcont". iFrame.
  Qed.

  Global Instance find_atom_struct_val p sl Xs As P Bs:
    Simpl (map VAL (member_vals p sl)) Xs →
    FindAtomInList Xs As P Bs →
    FindAtomInType (VAL p) (structT sl As) P (structT sl Bs).
  Proof.
    rewrite /FindAtomInType /Simpl /=. intros <- [Hlen Hfind].
    iSplit.
    - iIntros "Hp". iDestruct (struct_focus_ty_own_all_val with "Hp") as "[Hall Hcont]".
      rewrite !xtr_all_fmap_val in Hfind. rewrite Hfind.
      iDestruct "Hall" as "[$ Hall]".
      iApply "Hcont". iFrame.
    - iIntros "[HP Hp]". iDestruct (struct_focus_ty_own_all_val with "Hp") as "[Hall Hcont]".
      rewrite !xtr_all_fmap_val in Hfind. iCombine "HP Hall" as "Hall".
      rewrite -Hfind. iApply "Hcont". iFrame.
  Qed.

  Global Instance find_atom_slices_loc p len Xs Ls As Bs P Rs:
    sequence_classes [
      Simpl (Ls.*2) As;
      Simpl (map LOC (slices_locs p Ls)) Xs;
      FindAtomInList Xs As P Bs;
      Simpl (zip (Ls.*1) Bs) Rs
    ] →
    FindAtomInType (LOC p) (slicesT len Ls) P (slicesT len Rs).
  Proof.
    rewrite /sequence_classes !Forall_cons /=.
    intros (<- & <- & [Hlen Hfind] & <- & _).
    rewrite /FindAtomInType /=.
    iSplit.
    - iIntros "Hp". iDestruct (slicesT_borrow_all with "Hp") as "[Hall Hcont]".
      rewrite !xtr_all_fmap_loc in Hfind. rewrite Hfind.
      iDestruct "Hall" as "[$ Hall]".
      iApply "Hcont". iFrame.
    - iIntros "[HP Hp]". iDestruct (slicesT_borrow_all with "Hp") as "[Hall Hcont]".
      rewrite !xtr_all_fmap_loc in Hfind.
      iCombine "HP Hall" as "Hall". rewrite fmap_length in Hlen.
      rewrite snd_zip; last by rewrite fmap_length Hlen.
      unfold slices_locs in *. rewrite fst_zip; last by rewrite fmap_length Hlen.
      rewrite -Hfind. iDestruct ("Hcont" with "Hall") as "Hp".
      rewrite zip_fst_snd //.
  Qed.

  Global Instance find_atom_slices_val p len Xs Ls As Bs P Rs:
    sequence_classes [
      Simpl (Ls.*2) As;
      Simpl (map VAL (slices_vals p Ls)) Xs;
      FindAtomInList Xs As P Bs;
      Simpl (zip (Ls.*1) Bs) Rs
    ] →
    FindAtomInType (VAL p) (slicesT len Ls) P (slicesT len Rs).
  Proof.
    rewrite /sequence_classes !Forall_cons /=.
    intros (<- & <- & [Hlen Hfind] & <- & _).
    rewrite /FindAtomInType /=.
    iSplit.
    - iIntros "Hp". iDestruct (slicesT_borrow_all_val with "Hp") as "[Hall Hcont]".
      rewrite !xtr_all_fmap_val in Hfind. rewrite Hfind.
      iDestruct "Hall" as "[$ Hall]".
      iApply "Hcont". iFrame.
    - iIntros "[HP Hp]". iDestruct (slicesT_borrow_all_val with "Hp") as "[Hall Hcont]".
      rewrite !xtr_all_fmap_val in Hfind.
      iCombine "HP Hall" as "Hall". rewrite fmap_length in Hlen.
      rewrite snd_zip; last by rewrite fmap_length Hlen.
      unfold slices_vals in *. rewrite fst_zip; last by rewrite fmap_length Hlen.
      rewrite -Hfind. iDestruct ("Hcont" with "Hall") as "Hp".
      rewrite zip_fst_snd //.
  Qed.

End find_atoms.


Section solve_instances.
  Context `{!refinedcG Σ}.

  Global Instance qenvs_entails_not_null (Ω1 Ω2 Ω Ω': list (iProp Σ)) v A:
    sequence_classes [Simpl (Ω1 ++ Ω2) Ω; FindAtom DEEP Ω (v ◁ᵥ A) Ω'; NonNullVal A] →
    QE nil nil Ω1 Ω2 ⊨ Solve (not_null v) | 5.
  Proof.
    rewrite /sequence_classes/Simpl/FindAtom/Solve/qenvs_entails !Forall_cons /= => -[<-[Hfind [? _]]].
    iIntros "[? [? [#? ?]]]".
    iAssert ([∗] (Ω1 ++ Ω2))%I with "[$]" as "Hv". rewrite Hfind.
    iApply non_null_not_null. by iDestruct "Hv" as "[$ _]".
  Qed.

End solve_instances.


Section extract_from_atom.
  Context `{!refinedcG Σ}.

  Global Instance extract_from_atom_int_val v it n :
    extract_from_atom (v ◁ᵥ intT it n) (n ∈ it) | 1.
  Proof.
    rewrite /extract_from_atom. rewrite vtr_int_inversion.
    iIntros "%"; iPureIntro; naive_solver.
  Qed.

  Global Instance extract_from_atom_int_loc l it n :
    extract_from_atom (l ◁ₗ intT it n) (n ∈ it) | 1.
  Proof.
    rewrite /extract_from_atom. iIntros "A".
    iDestruct (ty_move with "A") as "(%v & Hl & Hv)";
      first apply intT_has_size.
    rewrite vtr_int_inversion. iDestruct "Hv" as "%".
    iPureIntro; naive_solver.
  Qed.

End extract_from_atom.


Section precond_extension.
  Context `{!refinedcG Σ}.

  Lemma abduct_type_precond_is_ty Δ X pm (x y: X → type Σ) (P: X → iProp Σ) E M B C R:
    TCOr (projection X (type Σ) x) (projection X (type Σ) y) →
    abduct Δ (type_precond X pm P (λ z, ⌜x z `is_ty` y z⌝ :: E z)%I M B C) R →
    abduct Δ (type_precond X pm (λ z, ⌜x z `is_ty` y z⌝ ∗ P z)%I E M B C) R.
  Proof. rewrite type_precond_pure_evars //. Qed.

  Global Instance abduct_step_type_precond_is_ty Δ X pm (x y: X → type Σ) (P: X → iProp Σ) E M B C R:
    TCOr (projection X (type Σ) x) (projection X (type Σ) y) →
    AbductStep (abduct Δ (type_precond X pm P (λ z, ⌜x z `is_ty` y z⌝ :: E z)%I M B C) R)
      Δ (type_precond X pm (λ z, ⌜x z `is_ty` y z⌝ ∗ P z)%I E M B C) R | 1.
  Proof.
    intros ??; by eapply abduct_type_precond_is_ty.
  Qed.

  Lemma abduct_type_precond_is_ty_all Δ X pm (x y: X → list (type Σ)) (P: X → iProp Σ) E M B C R:
    TCOr (projection X (list (type Σ)) x) (projection X (list (type Σ)) y) →
    abduct Δ (type_precond X pm P (λ z, ⌜x z `is_ty_all` y z⌝ :: E z)%I M B C) R →
    abduct Δ (type_precond X pm (λ z, ⌜x z `is_ty_all` y z⌝ ∗ P z)%I E M B C) R.
  Proof. rewrite type_precond_pure_evars //. Qed.

  Global Instance abduct_step_type_precond_is_ty_all Δ X pm (x y: X → list (type Σ)) (P: X → iProp Σ) E M B C R:
    TCOr (projection X (list (type Σ)) x) (projection X (list (type Σ)) y) →
    AbductStep (abduct Δ (type_precond X pm P (λ z, ⌜x z `is_ty_all` y z⌝ :: E z)%I M B C) R)
      Δ (type_precond X pm (λ z, ⌜x z `is_ty_all` y z⌝ ∗ P z)%I E M B C) R | 1.
  Proof.
    intros ??; by eapply abduct_type_precond_is_ty_all.
  Qed.

  Lemma abduct_type_precond_has_size (Δ: qenvs Σ) X pm A n P E M M' B C R:
    sequence_classes [
      projection X Z n;
      (∀! x, Simpl (insert_ascending (M x) 5%nat ⌜A x `has_size` n x⌝%I) (M' x))
    ] →
    abduct Δ (type_precond X pm P E M' B C) R →
    abduct Δ (type_precond X pm (λ x, ⌜A x `has_size` n x⌝ ∗ P x)%I E M B C) R.
  Proof.
    rewrite /sequence_classes !Forall_cons /evar_forall. intros (_ & Hsimpl & _) ?. rewrite -type_precond_sep_maybe; first done.
    intros x. rewrite -insert_ascending_sep -Hsimpl //.
  Qed.

  Global Instance abduct_step_type_precond_has_size (Δ: qenvs Σ) X pm A n P E M M' B C R:
    sequence_classes [
      projection X Z n;
      (∀! x, Simpl (insert_ascending (M x) 5%nat ⌜A x `has_size` n x⌝%I) (M' x))
    ] →
    AbductStep (abduct Δ (type_precond X pm P E M' B C) R)
      Δ (type_precond X pm (λ x, ⌜A x `has_size` n x⌝ ∗ P x)%I E M B C) R | 1.
  Proof.
    intros ??; by eapply abduct_type_precond_has_size.
  Qed.


End precond_extension.

Section elim_existentials.
  Context `{!refinedcG Σ}.

  Lemma resolve_this_existential_prop_is_ty_r  (X A B: type Σ):
    is_syn_eq X A →
    unify X B →
    resolve_this_existential_prop X (B `is_ty` A).
  Proof. rewrite /is_syn_eq /unify /resolve_this_existential_prop. by intros -> ->. Qed.

  Lemma resolve_this_existential_prop_is_ty_l (X A B: type Σ):
    is_syn_eq X A →
    unify X B →
    resolve_this_existential_prop X (A `is_ty` B).
  Proof. rewrite /is_syn_eq /unify /resolve_this_existential_prop. by intros -> ->. Qed.

  Lemma resolve_this_existential_prop_is_ty_all_r (X A B: list (type Σ)):
    is_syn_eq X A →
    unify X B →
    resolve_this_existential_prop X (B `is_ty_all` A).
  Proof. rewrite /is_syn_eq /unify /resolve_this_existential_prop. by intros -> ->. Qed.

  Lemma resolve_this_existential_prop_is_ty_all_l (X A B: list (type Σ)):
    is_syn_eq X A →
    unify X B →
    resolve_this_existential_prop X (A `is_ty_all` B).
  Proof. rewrite /is_syn_eq /unify /resolve_this_existential_prop. by intros -> ->. Qed.

End elim_existentials.
Global Existing Instance resolve_this_existential_prop_is_ty_r.
Global Existing Instance resolve_this_existential_prop_is_ty_l.
Global Existing Instance resolve_this_existential_prop_is_ty_all_r.
Global Existing Instance resolve_this_existential_prop_is_ty_all_l.




(* simplify atom *)
Section simplify_atom.
  Context `{!refinedcG Σ}.

  Global Instance simplify_atom_empT l b :
    simplify_atom b (l ◁ₗ empT) (True).
  Proof.
    rewrite /simplify_atom.
    destruct b; apply simplify_ent; iIntros "_"; [|done].
    iApply empT_intro.
  Qed.

  Global Instance simplify_atom_ownT_empT_val b v r :
    simplify_atom b (v ◁ᵥ ownT r empT) (v ◁ᵥ valueT r ptr_size).
  Proof.
    rewrite /simplify_atom.
    destruct b; apply simplify_ent; rewrite ownT_ty_own_val_own empT_iff.
    - rewrite vtr_valueT_elim. iIntros "[$ _]".
    - iIntros "[-> _]". by iApply vtr_valueT_intro.
  Qed.

  Global Instance simplify_atom_ownT_empT_loc b l r :
    simplify_atom b (l ◁ₗ ownT r empT) (l ◁ₗ valueT r ptr_size).
  Proof.
    rewrite /simplify_atom.
    destruct b; apply simplify_ent; rewrite -valueT_ownT empT_iff right_id //.
  Qed.

  Global Instance simplify_atom_vtr_any v n :
    simplify_atom false (v ◁ᵥ anyT n) (True).
  Proof. apply simplify_ent. by iIntros "_". Qed.

  Global Instance simplify_atom_vty T :
    simplify_atom false (vty T) (True).
  Proof. apply simplify_ent. by iIntros "_". Qed.

  Global Instance simplify_atom_ptr_id l i n:
    simplify_atom false ((l +ₗ i) ◁ₗ ptrT l n i) (True).
  Proof. apply simplify_ent. by iIntros "_". Qed.

End simplify_atom.



(* Automation for inferring type sizes *)
Existing Class ty_has_size.
Global Hint Mode ty_has_size - - ! - : typeclass_instances.

Existing Class ty_has_bounds.
Global Hint Mode ty_has_bounds - - ! - : typeclass_instances.

(* op/layout type instances *)
Section size_type_instaces.
  Context `{!refinedcG Σ}.

  Global Instance has_size_int n m it:
    Simpl (int_size it) m →
    (intT it n) `has_size` m | 1.
  Proof. intros <-. eapply intT_has_size. Qed.

  Global Instance has_size_bool b m it:
    Simpl (int_size it) m →
    (boolT it b) `has_size` m | 1.
  Proof. intros <-. eapply boolT_has_size. Qed.

  Global Instance has_size_any n:
    (anyT n) `has_size` n | 1.
  Proof. eapply anyT_has_size. Qed.

  Global Instance has_size_zeros n:
    (zerosT n) `has_size` n | 1.
  Proof. by eapply zerosT_has_size. Qed.

  Global Instance has_size_value v n:
    (valueT v n) `has_size` n | 1.
  Proof. by eapply valueT_has_size. Qed.

  Global Instance has_size_void :
    voidT `has_size` 0.
  Proof.
    rewrite voidT_eq /voidT_def. apply _.
  Qed.

  Global Instance has_size_ptr A:
    PtrType A →
    A `has_size` ptr_size | 2.
  Proof. intros ?. eapply ptr_type_size. Qed.

  Global Instance has_size_opt v (n: Z):
    Simpl (ptr_size: Z) n →
    (optT v) `has_size` n | 1.
  Proof.
    rewrite /opt /Simpl. intros ->. by eapply sepT_has_size, valueT_has_size.
  Qed.

  Global Instance has_size_constraintT A φ n:
    A `has_size` n →
    constraintT A φ `has_size` n | 1.
  Proof.
    intros ?. by eapply constraintT_has_size.
  Qed.

  Global Instance has_size_sepT A P n:
    A `has_size` n →
    sepT A P `has_size` n | 1.
  Proof.
    intros ?. by eapply sepT_has_size.
  Qed.

  Global Instance has_size_existsT {X} `{!Inhabited X} (T: X → type Σ) n:
    (∀ x, T x `has_size` n) →
    (ex x, T x) `has_size` n | 1.
  Proof.
    intros ?. eapply existsT_has_size. done.
  Qed.

  Global Instance has_size_fixR {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a ot:
    (∀ R a, T R a `has_size` ot) →
    ((a @ fixR T) `has_size` ot) | 1.
  Proof. by eapply fixR_has_size. Qed.

  Global Instance has_size_lamR {A} `{!Inhabited A} (T:  A → type Σ) a ot:
    (T a `has_size` ot) →
    ((a @ lamR T)%RT `has_size` ot) | 1.
  Proof. by eapply lamR_has_size. Qed.

  Global Instance has_size_struct sl As :
    (TCForall2 (λ A m, A `has_size` ly_size m.2)%type As (field_members (sl_members sl))) →
    structT sl As `has_size` (ly_size sl) | 1.
  Proof.
    rewrite structT_eq /= /is_struct_size.
    split; first done. by eapply TCForall2_Forall2.
  Qed.

  Global Instance has_size_arrayT {X} (T: X → type Σ) (sz: Z) (xs: list X) n:
    sequence_classes [(∀ x, T x `has_size` n); Solve (n = sz ∧ 0 ≤ sz)] →
    (arrayT T sz xs `has_size` (n * length xs)).
  Proof.
    rewrite /sequence_classes !Forall_cons /= /Solve.
    intros (Hsz & [Hle Heq] & _). subst.
    rewrite Z.mul_comm. eapply arrayT_has_size; eauto.
  Qed.

  Global Instance has_size_slicesT len Ts n:
    Trivial (len = n) →
    TCForall (λ s, TCAnd (ty_has_size s.2 s.1.2) (Solve (0 ≤ s.1.2))) Ts →
    Solve (0 ≤ n) →
    slicesT len Ts `has_size` n.
  Proof.
    rewrite /Trivial/Solve => -> /TCForall_Forall Hall ?.
    apply slicesT_has_size; [|done].
    apply: Forall_impl; [done|]. by move => ? [??].
  Qed.


  (* [has_bounds] *)
  Global Instance has_bounds_to_has_size A n:
    A `has_size` n →
    A `has_bounds` n | 2.
  Proof. intros ?; by eapply has_size_has_bounds. Qed.

  Global Instance has_bounds_constraintT A φ n:
    A `has_bounds` n →
    constraintT A φ `has_bounds` n | 1.
  Proof.
    intros ?. by eapply constraintT_has_bounds.
  Qed.

  Global Instance has_bounds_sepT A P n:
    A `has_bounds` n →
    sepT A P `has_bounds` n | 1.
  Proof.
    intros ?. by eapply sepT_has_bounds.
  Qed.

  Global Instance has_bounds_existsT {X} `{!Inhabited X} (T: X → type Σ) n:
    (∀ x, T x `has_bounds` n) →
    (ex x, T x) `has_bounds` n | 1.
  Proof.
    intros ?. eapply existsT_has_bounds. done.
  Qed.

  Global Instance has_bounds_fixR {A} `{!Inhabited A} (T: (A → type Σ) → A → type Σ) `{!TypeMono T} a ot:
    (∀ R a, T R a `has_bounds` ot) →
    ((a @ fixR T)%RT `has_bounds` ot) | 1.
  Proof. by eapply fixR_has_bounds. Qed.

  Global Instance has_bounds_lamR {A} `{!Inhabited A} (T: A → type Σ) a ot:
    (T a `has_bounds` ot) →
    ((a @ lamR T)%RT `has_bounds` ot) | 1.
  Proof. by eapply lamR_has_bounds. Qed.

  Global Instance has_bounds_struct sl As :
    structT sl As `has_bounds` (ly_size sl) | 1.
  Proof.
    eapply struct_has_bounds.
  Qed.

  Global Instance has_bounds_type_array len As:
    type_arrayT len As `has_bounds` (length As * len) | 2.
  Proof.
    eapply type_array_has_bounds.
  Qed.

  Global Instance has_bounds_seq Ls As n:
    seqT n Ls As `has_bounds` n | 1.
  Proof.
    eapply seq_has_bounds.
  Qed.

  Global Instance has_bounds_slices Ts n:
    slicesT n Ts `has_bounds` n | 1.
  Proof.
    eapply slices_has_bounds.
  Qed.

  Global Instance has_bounds_raw_ptr l len i :
    ptrT l len i `has_bounds` (len - i) | 1.
  Proof. eapply raw_pointer_has_bounds. Qed.

  Global Instance has_bounds_array {X} (T: X → type Σ) len xs:
    arrayT T len xs `has_bounds` (length xs * len) | 1.
  Proof.
    rewrite arrayT_eq /arrayT_def.
    rewrite /ty_has_bounds.
    iIntros (l) "Hl". rewrite ltr_constraintT_iff.
    iDestruct "Hl" as "[Hl %Hsz]".
    iDestruct (has_bounds_type_array with "Hl") as "Hl".
    rewrite fmap_length. iDestruct "Hl" as "[Hl _]".
    replace (length xs * Z.to_nat len) with (length xs * len) by lia.
    iFrame. iPureIntro. lia.
  Qed.


  (* non-null *)
  Global Instance has_op_type_non_null A n:
    A `has_bounds` n →
    NonNull A.
  Proof.
    rewrite /NonNull.
    intros Hot l. iIntros "Hl". iDestruct (Hot with "Hl") as "[Hb Hle]".
    iApply loc_in_bounds_shorten; last done. lia.
  Qed.

End size_type_instaces.





(* proof mode classes *)

(* tracking of argument values and function stack locations *)
Inductive local_map (m : gmap string loc) := LocalMap.
Existing Class local_map.
Global Hint Mode local_map - : typeclass_instances.

Inductive val_map (vs: gmap string val) : Prop := ArgVals.
Existing Class val_map.
Global Hint Mode val_map - : typeclass_instances.

Inductive is_arg_val (v: val) : Prop := IsArgVal.
Existing Class is_arg_val.
Global Hint Mode is_arg_val - : typeclass_instances.

Ltac is_arg_val :=
  match goal with
  | H: val_map ?M |- is_arg_val ?v =>
    lazymatch M with context [v] => apply IsArgVal end
  end.

Global Hint Extern 1 (is_arg_val _) => (is_arg_val) : typeclass_instances.
