From quiver.argon.base Require Export judgments.
From quiver.thorium.logic Require Import assume.
From quiver.argon.abduction Require Import abduct proofmode assume.
From quiver.thorium.types Require Import types base_types pointers refinements arrays bytes structs pointers.


Section assume_type.
  Context `{!refinedcG Σ}.

  (* TYPE ASSUME *)
  Lemma abduct_abd_assume_ltr_place_id Δ m l Q R:
    abduct Δ Q R →
    abduct Δ (abd_assume m (l ◁ₗ placeT l) Q) R.
  Proof. rewrite -abd_assume_drop //. Qed.

  Lemma abduct_abd_assume_vtr_value_id Δ m v n Q R:
    abduct Δ Q R →
    abduct Δ (abd_assume m (v ◁ᵥ valueT v n) Q) R.
  Proof. rewrite -abd_assume_drop //. Qed.

  Lemma abduct_abd_assume_vtr_own_loc_id Δ m (l: loc) A Q R:
    abduct Δ (abd_assume m (l ◁ₗ A) Q) R →
    abduct Δ (abd_assume m (l ◁ᵥ ownT l A) Q) R.
  Proof. rewrite -abd_assume_vtr_own_loc_id //. Qed.

  Lemma abduct_abd_assume_vtr_opt_id Δ m v Q R:
    abduct Δ Q R →
    abduct Δ (abd_assume m (v ◁ᵥ optT v) Q) R.
  Proof. rewrite -abd_assume_drop //. Qed.

  Lemma abduct_abd_assume_ltr_assume Δ m l A Q R:
    Atom (l ◁ₗ A) →
    abduct Δ (l ◁ₗ A -∗ Q) R →
    abduct Δ (abd_assume m (l ◁ₗ A) Q) R.
  Proof. intros ?. rewrite -abd_assume_ltr_typing //. Qed.

  Lemma abduct_abd_assume_ltr_dangling Δ m l A Q R:
    sequence_classes [uni l; not_in l Δ; not_in l Q] →
    abduct Δ (abd_fail "memory leak") R →
    abduct Δ (abd_assume m (l ◁ₗ A) Q) R.
  Proof.
    intros ?; rewrite abd_fail_anything //.
  Qed.

  Lemma abduct_abd_assume_ltr_duplicate Δ Δ' m l A B Q R:
    qenvs_find_atom DEEP Δ (l ◁ₗ B) Δ' →
    abduct Δ' (type_resolve_duplicate l A B Q) R →
    abduct Δ (abd_assume m (l ◁ₗ A) Q) R.
  Proof.
    rewrite /abd_assume /type_resolve_duplicate.
    iIntros (Hfind Habd) "Hctx Hl1". rewrite Hfind.
    iDestruct ("Hctx" ) as "[[Hl2 HΔ] R]".
    iApply (Habd with "[$R $HΔ] Hl1 Hl2").
  Qed.

  Lemma abduct_abd_assume_vtr_assume Δ m v A Q R:
    Atom (v ◁ᵥ A) →
    abduct Δ (v ◁ᵥ A -∗ Q) R →
    abduct Δ (abd_assume m (v ◁ᵥ A) Q) R.
  Proof. intros ?. rewrite -abd_assume_vtr_typing //. Qed.

  Lemma abduct_abd_assume_vtr_duplicate_atom m Δ Δ' v A B Q R:
    sequence_classes [Atom (v ◁ᵥ A); qenvs_find_atom SHALLOW Δ (v ◁ᵥ B) Δ'] →
    abduct Δ' (abd_assume m (v ◁ᵥ A) (type_recycle v B Q)) R →
    abduct Δ  (abd_assume m (v ◁ᵥ A) Q) R.
  Proof.
    rewrite /sequence_classes !Forall_cons /= /type_recycle /abd_assume.
    intros (? & Henv & _) Habd. iIntros "[HΔ HR] Hv". rewrite Henv.
    iDestruct "HΔ" as "[Hv' HΔ]".
    iApply (Habd with "[$HR $HΔ] Hv Hv'").
  Qed.

  (* the combination of [uni v], [v ∉ Δ], and [v ∉ Q] makes [v] unreachable,
     and hence we can drop the typing for [v] *)
  Lemma abduct_abd_assume_vtr_dangling Δ m v B Q R:
    sequence_classes [uni v; not_in v Δ; not_in v Q] →
    abduct Δ (type_recycle v B Q) R →
    abduct Δ (abd_assume m (v ◁ᵥ B) Q) R.
  Proof. intros ??; rewrite -abd_assume_vtr_recycle //. Qed.

  Lemma abduct_abd_assume_vtr_recycle Δ v B Q R:
    abduct Δ (type_recycle v B Q) R →
    abduct Δ (abd_assume RECYCLE (v ◁ᵥ B) Q) R.
  Proof. intros ?; rewrite -abd_assume_vtr_recycle //. Qed.



  Global Instance abduct_step_abd_assume_ltr_place_id Δ m l Q R:
    AbductStep (abduct Δ Q R)
      Δ (abd_assume m (l ◁ₗ placeT l) Q) R | 1.
  Proof. intros ?. by eapply abduct_abd_assume_ltr_place_id. Qed.

  Global Instance abduct_step_abd_assume_vtr_value_id Δ m v n Q R:
    AbductStep (abduct Δ Q R)
      Δ (abd_assume m (v ◁ᵥ valueT v n) Q) R | 1.
  Proof. intros ?. by eapply abduct_abd_assume_vtr_value_id. Qed.

  Global Instance abduct_step_abd_assume_vtr_own_loc_id Δ m (l: loc) A Q R:
    AbductStep (abduct Δ (abd_assume m (l ◁ₗ A) Q) R)
      Δ (abd_assume m (l ◁ᵥ ownT l A) Q) R | 1.
  Proof. intros ?. by eapply abduct_abd_assume_vtr_own_loc_id. Qed.

  Global Instance abduct_step_abd_assume_vtr_opt_id Δ m v Q R:
    AbductStep (abduct Δ Q R)
      Δ (abd_assume m (v ◁ᵥ optT v) Q) R.
  Proof. intros ?. by eapply abduct_abd_assume_vtr_opt_id. Qed.

  Global Instance abduct_step_abd_assume_ltr_typing Δ m l A Q R:
    Atom (l ◁ₗ A) →
    AbductStep (abduct Δ (l ◁ₗ A -∗ Q) R)
      Δ (abd_assume m (l ◁ₗ A) Q) R | 10.
  Proof. intros ??. by eapply abduct_abd_assume_ltr_assume. Qed.

  Global Instance abduct_step_abd_assume_ltr_duplicate Δ Δ' m l A B Q R:
    qenvs_find_atom DEEP Δ (l ◁ₗ B) Δ' →
    AbductStep (abduct Δ' (type_resolve_duplicate l A B Q) R)
      Δ (abd_assume m (l ◁ₗ A) Q) R | 9.
  Proof.
    intros ??. by eapply abduct_abd_assume_ltr_duplicate.
  Qed.

  Global Instance abduct_step_abd_assume_ltr_dangling Δ m l A Q R:
    sequence_classes [uni l; not_in l Δ; not_in l Q] →
    AbductStep (abduct Δ (abd_fail "memory leak") R)
      Δ (abd_assume m (l ◁ₗ A) Q) R.
  Proof. intros ??. by eapply abduct_abd_assume_ltr_dangling. Qed.

  Global Instance abduct_step_abd_assume_vtr_typing Δ m v A Q R:
    Atom (v ◁ᵥ A) →
    AbductStep (abduct Δ (v ◁ᵥ A -∗ Q) R)
      Δ (abd_assume m (v ◁ᵥ A) Q) R | 10.
  Proof. intros ??. by eapply abduct_abd_assume_vtr_assume. Qed.

  Global Instance abduct_step_abd_assume_vtr_duplicate_atom m Δ Δ' v A B Q R:
    sequence_classes [Atom (v ◁ᵥ A); qenvs_find_atom SHALLOW Δ (v ◁ᵥ B) Δ'] →
    AbductStep (abduct Δ' (abd_assume m (v ◁ᵥ A) (type_recycle v B Q)) R)
      Δ (abd_assume m (v ◁ᵥ A) Q) R | 3.
  Proof. intros ??. by eapply abduct_abd_assume_vtr_duplicate_atom. Qed.

  (* the combination of [uni v], [v ∉ Δ], and [v ∉ Q] makes [v] unreachable,
    and hence we can drop the typing for [v] *)
  Global Instance abduct_step_abd_assume_vtr_dangling Δ m v B Q R:
    sequence_classes [uni v; not_in v Δ; not_in v Q] →
    AbductStep (abduct Δ (type_recycle v B Q) R)
      Δ (abd_assume m (v ◁ᵥ B) Q) R | 2.
  Proof. intros ??. by eapply abduct_abd_assume_vtr_dangling. Qed.

  Global Instance abduct_step_abd_assume_vtr_recycle Δ v B Q R:
    AbductStep (abduct Δ (type_recycle v B Q) R)
      Δ (abd_assume RECYCLE (v ◁ᵥ B) Q) R.
  Proof. intros ?. by eapply abduct_abd_assume_vtr_recycle. Qed.




  (* RESOLVING DUPLICATES *)
  Lemma abduct_type_resolve_duplicate_left_slices Δ l len Ls B P R:
    abduct Δ (abd_assume CTX (l ◁ₗ (slicesT len Ls)) (abd_assume CTX ((l +ₗ 0) ◁ₗ B) P)) R →
    abduct Δ (type_resolve_duplicate l (slicesT len Ls) B P) R.
  Proof. rewrite -type_resolve_duplicate_left_offset //. Qed.

  Global Instance abduct_step_type_resolve_duplicate_left_slices Δ l len Ls B P R:
    AbductStep (abduct Δ (abd_assume CTX (l ◁ₗ (slicesT len Ls)) (abd_assume CTX ((l +ₗ 0) ◁ₗ B) P)) R)
      Δ (type_resolve_duplicate l (slicesT len Ls) B P) R.
  Proof. intros ?. by eapply abduct_type_resolve_duplicate_left_slices. Qed.

  Lemma abduct_type_resolve_duplicate_right_slices Δ l len Ls A P R:
    abduct Δ (abd_assume CTX ((l +ₗ 0) ◁ₗ A) (abd_assume CTX (l ◁ₗ (slicesT len Ls)) P)) R →
    abduct Δ (type_resolve_duplicate l A (slicesT len Ls) P) R.
  Proof. rewrite -type_resolve_duplicate_right_offset //. Qed.

  Global Instance abduct_step_type_resolve_duplicate_right_slices Δ l len Ls A P R:
    AbductStep (abduct Δ (abd_assume CTX ((l +ₗ 0) ◁ₗ A) (abd_assume CTX (l ◁ₗ (slicesT len Ls)) P)) R)
      Δ (type_resolve_duplicate l A (slicesT len Ls) P) R.
  Proof. intros ?. by eapply abduct_type_resolve_duplicate_right_slices. Qed.

  Lemma abduct_type_resolve_duplicate_left_place Δ l r B P R:
    abduct Δ (abd_assume CTX (l ◁ₗ placeT r) (abd_assume CTX (r ◁ₗ B) P)) R →
    abduct Δ (type_resolve_duplicate l (placeT r) B P) R.
  Proof. rewrite -type_resolve_duplicate_left_place //. Qed.

  Global Instance abduct_step_type_resolve_duplicate_left_place Δ l r B P R:
    AbductStep (abduct Δ (abd_assume CTX (l ◁ₗ placeT r) (abd_assume CTX (r ◁ₗ B) P)) R)
      Δ (type_resolve_duplicate l (placeT r) B P) R.
  Proof. intros ?. by eapply abduct_type_resolve_duplicate_left_place. Qed.

  Lemma abduct_type_resolve_duplicate_right_place Δ l r A P R:
    abduct Δ (abd_assume CTX (l ◁ₗ placeT r) (abd_assume CTX (r ◁ₗ A) P)) R →
    abduct Δ (type_resolve_duplicate l A (placeT r) P) R.
  Proof. rewrite -type_resolve_duplicate_right_place //. Qed.

  Global Instance abduct_step_type_resolve_duplicate_right_place Δ l r A P R:
    AbductStep (abduct Δ (abd_assume CTX (l ◁ₗ placeT r) (abd_assume CTX (r ◁ₗ A) P)) R)
      Δ (type_resolve_duplicate l A (placeT r) P) R.
  Proof. intros ?. by eapply abduct_type_resolve_duplicate_right_place. Qed.


  Lemma abduct_type_resolve_duplicate_right_ptr Δ l A b i n P R:
    abduct Δ (abd_assume CTX (l ◁ₗ A) P) R →
    abduct Δ (type_resolve_duplicate l A (ptrT b i n) P) R.
  Proof. rewrite -type_resolve_duplicate_right_drop //. Qed.

  Global Instance abduct_step_type_resolve_duplicate_right_ptr Δ l A b i n P R:
    AbductStep (abduct Δ (abd_assume CTX (l ◁ₗ A) P) R)
      Δ (type_resolve_duplicate l A (ptrT b i n) P) R.
  Proof. intros ?. by eapply abduct_type_resolve_duplicate_right_ptr. Qed.

  Lemma abduct_type_resolve_duplicate_left_ptr Δ l B b i n P R:
    abduct Δ (abd_assume CTX (l ◁ₗ B) P) R →
    abduct Δ (type_resolve_duplicate l (ptrT b i n) B P) R.
  Proof. rewrite -type_resolve_duplicate_left_drop //. Qed.

  Global Instance abduct_step_type_resolve_duplicate_left_ptr Δ l B b i n P R:
    AbductStep (abduct Δ (abd_assume CTX (l ◁ₗ B) P) R)
      Δ (type_resolve_duplicate l (ptrT b i n) B P) R.
  Proof. intros ?. by eapply abduct_type_resolve_duplicate_left_ptr. Qed.

  (* type recycle *)
  Lemma abduct_type_recycle_exists Δ lv {X: Type} (A: X → type Σ) Q R :
    (abduct Δ (∀ x, type_recycle lv (A x) Q) R) →
    abduct Δ (type_recycle lv (ex x, A x) Q) R.
  Proof. by rewrite -type_recycle_exists. Qed.

  Global Instance abduct_step_type_recycle_exists Δ lv {X: Type} (A: X → type Σ) Q R :
    AbductStep (abduct Δ (∀ x, type_recycle lv (A x) Q) R)
      Δ (type_recycle lv (ex x, A x) Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_exists. Qed.

  Lemma abduct_type_recycle_sep Δ lv A P Q R:
    abduct Δ (abd_assume RECYCLE P (type_recycle lv A Q)) R →
    abduct Δ (type_recycle lv (A ∗∗ P) Q) R.
  Proof. by rewrite -type_recycle_sep. Qed.

  Global Instance abduct_step_type_recycle_sep Δ lv A P Q R:
    AbductStep (abduct Δ (abd_assume RECYCLE P (type_recycle lv A Q)) R)
      Δ (type_recycle lv (A ∗∗ P) Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_sep. Qed.

  Lemma abduct_type_recycle_constraint Δ lv A φ Q R:
    abduct Δ (abd_assume RECYCLE ⌜φ⌝ (type_recycle lv A Q)) R →
    abduct Δ (type_recycle lv {A | φ} Q) R.
  Proof. by rewrite -type_recycle_constraint. Qed.

  Global Instance abduct_step_type_recycle_constraint Δ lv A φ Q R:
    AbductStep (abduct Δ (abd_assume RECYCLE ⌜φ⌝ (type_recycle lv A Q)) R)
      Δ (type_recycle lv {A | φ} Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_constraint. Qed.

  Lemma abduct_type_recycle_int Δ v it (n: Z) Q R:
    abduct Δ (abd_assume RECYCLE ⌜(n ∈ it)%Z⌝ Q) R →
    abduct Δ (type_recycle v (intT it n) Q) R.
  Proof. by rewrite -type_recycle_int. Qed.

  Global Instance abduct_step_type_recycle_int Δ v it (n: Z) Q R:
    AbductStep (abduct Δ (abd_assume RECYCLE ⌜(n ∈ it)%Z⌝ Q) R)
      Δ (type_recycle v (intT it n) Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_int. Qed.

  Lemma abduct_type_recycle_zeros Δ v n Q R:
    abduct Δ (abd_assume RECYCLE ⌜0 ≤ n⌝ Q) R →
    abduct Δ (type_recycle v (zerosT n) Q) R.
  Proof. by rewrite -type_recycle_zeros. Qed.

  Global Instance abduct_step_type_recycle_zeros Δ v n Q R:
    AbductStep (abduct Δ (abd_assume RECYCLE ⌜0 ≤ n⌝ Q) R)
      Δ (type_recycle v (zerosT n) Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_zeros. Qed.

  Lemma abduct_type_recycle_array Δ v {X} (T: X → type Σ) xs n Q R:
    (∀ x, Copy (T x)) →
    abduct Δ (abd_assume RECYCLE ⌜0 ≤ n⌝ Q) R →
    abduct Δ (type_recycle v (arrayT T n xs) Q) R.
  Proof. intros ??. rewrite -type_recycle_array //. Qed.

  Global Instance abduct_step_type_recycle_array Δ v {X} (T: X → type Σ) xs n Q R:
    (∀ x, Copy (T x)) →
    AbductStep (abduct Δ (abd_assume RECYCLE ⌜0 ≤ n⌝ Q) R)
      Δ (type_recycle v (arrayT T n xs) Q) R.
  Proof. intros ??. by eapply abduct_type_recycle_array. Qed.

  Lemma abduct_type_recycle_value Δ v w n Q R:
    abduct Δ (abd_assume RECYCLE ⌜0 ≤ n⌝ Q) R →
    abduct Δ (type_recycle v (valueT w n) Q) R.
  Proof. by rewrite -type_recycle_value. Qed.

  Global Instance abduct_step_type_recycle_value Δ v w n Q R:
    AbductStep (abduct Δ (abd_assume RECYCLE ⌜0 ≤ n⌝ Q) R)
      Δ (type_recycle v (valueT w n) Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_value. Qed.

  Lemma abduct_type_recycle_opt Δ v w Q R:
    abduct Δ Q R →
    abduct Δ (type_recycle v (optT w) Q) R.
  Proof. by rewrite -type_recycle_opt. Qed.

  Global Instance abduct_step_type_recycle_opt Δ v w Q R:
    AbductStep (abduct Δ Q R)
      Δ (type_recycle v (optT w) Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_opt. Qed.

  Lemma abduct_type_recycle_any Δ v n Q R:
    abduct Δ (abd_assume RECYCLE ⌜0 ≤ n⌝ Q) R →
    abduct Δ (type_recycle v (anyT n) Q) R.
  Proof. by rewrite -type_recycle_any. Qed.

  Global Instance abduct_step_type_recycle_any Δ v n Q R:
    AbductStep (abduct Δ (abd_assume RECYCLE ⌜0 ≤ n⌝ Q) R)
      Δ (type_recycle v (anyT n) Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_any. Qed.

  Lemma abduct_type_recycle_null Δ v Q R:
    abduct Δ Q R →
    abduct Δ (type_recycle v nullT Q) R.
  Proof. rewrite -type_recycle_drop //. Qed.

  Global Instance abduct_step_type_recycle_null Δ v Q R:
    AbductStep (abduct Δ Q R)
      Δ (type_recycle v nullT Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_null. Qed.

  Lemma abduct_type_recycle_void Δ v Q R:
    abduct Δ Q R →
    abduct Δ (type_recycle v voidT Q) R.
  Proof. rewrite -type_recycle_drop //. Qed.

  Global Instance abduct_step_type_recycle_void Δ v Q R:
    AbductStep (abduct Δ Q R)
      Δ (type_recycle v voidT Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_void. Qed.

  Lemma abduct_type_recycle_bool Δ v it b Q R:
    abduct Δ Q R →
    abduct Δ (type_recycle v (boolT it b) Q) R.
  Proof. rewrite -type_recycle_drop //. Qed.

  Global Instance abduct_step_type_recycle_bool Δ v it b Q R:
    AbductStep (abduct Δ Q R)
      Δ (type_recycle v (boolT it b) Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_bool. Qed.

  Lemma abduct_type_recycle_own Δ v p A Q R:
    abduct Δ (abd_assume RECYCLE (p ◁ₗ A) Q) R →
    abduct Δ (type_recycle v (ownT p A) Q) R.
  Proof. by rewrite -type_recycle_own. Qed.

  Global Instance abduct_step_type_recycle_own Δ v p A Q R:
    AbductStep (abduct Δ (abd_assume RECYCLE (p ◁ₗ A) Q) R)
      Δ (type_recycle v (ownT p A) Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_own. Qed.

  Lemma abduct_type_recycle_struct Δ v sl As P Q R:
    Simpl ([∗ list] v; A ∈ member_vals v sl; As, v ◁ᵥ A)%I P →
    abduct Δ (abd_assume RECYCLE P Q) R →
    abduct Δ (type_recycle v (structT sl As) Q) R.
  Proof. intros <-. by rewrite -type_recycle_struct. Qed.

  Global Instance abduct_step_type_recycle_struct Δ v sl As P Q R:
    Simpl ([∗ list] v; A ∈ member_vals v sl; As, v ◁ᵥ A)%I P →
    AbductStep (abduct Δ (abd_assume RECYCLE P Q) R)
      Δ (type_recycle v (structT sl As) Q) R.
  Proof. intros ??. by eapply abduct_type_recycle_struct. Qed.

  Lemma abduct_type_recycle_moved Δ v A Q R:
    abduct Δ Q R →
    abduct Δ (type_recycle v (movedT A) Q) R.
  Proof. rewrite -type_recycle_drop //. Qed.

  Global Instance abduct_step_type_recycle_moved Δ v A Q R:
    AbductStep (abduct Δ Q R)
      Δ (type_recycle v (movedT A) Q) R.
  Proof. intros ?. by eapply abduct_type_recycle_moved. Qed.

  Lemma abduct_type_recycle_type_var Δ v A B P R:
    sequence_classes [exi A; Δ ⊨ ctx_hyp (A `is_ty` B)] →
    abduct Δ (type_recycle v B P) R →
    abduct Δ (type_recycle v A P) R.
  Proof.
    rewrite /sequence_classes !Forall_cons /=.
    intros (? & Hent & ?) Habd. apply: abduct_qenvs_entails.
    intros [Hty]. rewrite /type_recycle. rewrite Hty //.
  Qed.

  Global Instance abduct_step_type_recycle_type_var Δ v A B P R:
    sequence_classes [exi A; Δ ⊨ ctx_hyp (A `is_ty` B)] →
    AbductStep (abduct Δ (type_recycle v B P) R)
      Δ (type_recycle v A P) R | 8.
  Proof. intros ??. by eapply abduct_type_recycle_type_var. Qed.

  Lemma abduct_type_recycle_fallthrough Δ v A P R:
    abduct Δ (vty A -∗ P) R →
    abduct Δ (type_recycle v A P) R.
  Proof.
    rewrite -type_recycle_fallthrough //.
  Qed.

  Global Instance abduct_step_type_recycle_fallthrough Δ v A P R:
    AbductStep (abduct Δ (vty A -∗ P) R)
      Δ (type_recycle v A P) R | 10.
  Proof.
    intros ?. by eapply abduct_type_recycle_fallthrough.
  Qed.

End assume_type.


Section virtual_types.
  Context `{!refinedcG Σ}.

  Definition virtual_type (A: type Σ) :=
    sealed True.

  Lemma sep_virtual_type A P :
    virtual_type (sepT A P).
  Proof. rewrite /virtual_type sealed_eq. done. Qed.

  Lemma constraint_virtual_type A φ :
    virtual_type (constraintT A φ).
  Proof. rewrite /virtual_type sealed_eq. done. Qed.

  Lemma exists_virtual_type X (A: X → type Σ) :
    virtual_type (ex x, A x).
  Proof. rewrite /virtual_type sealed_eq. done. Qed.

  Lemma type_var_virtual_type A B:
    exi A →
    ctx_hyp (A `is_ty` B) →
    virtual_type A.
  Proof. rewrite /virtual_type sealed_eq. done. Qed.

  Lemma moved_virtual_type A B :
    ctx_hyp (A `is_ty` B) →
    virtual_type (movedT A).
  Proof. rewrite /virtual_type sealed_eq. done. Qed.

  Lemma moved_own_virtual_type p A :
    virtual_type (movedT (ownT p A)).
  Proof. rewrite /virtual_type sealed_eq. done. Qed.

  Lemma moved_copy_virtual_type A :
    Copy A →
    virtual_type (movedT A).
  Proof. rewrite /virtual_type sealed_eq. done. Qed.

  Lemma refinement_virtual_type {X} (R: rtype Σ X) (x: X) B :
    unfold_refinement R x B →
    virtual_type (x @ R).
  Proof. rewrite /virtual_type sealed_eq. done. Qed.


  (* corresponding abduction lemmas *)
  Lemma abduct_type_virt_refl Δ lv A Φ R:
    abduct Δ (Φ A) R →
    abduct Δ (type_virt lv A Φ) R.
  Proof. rewrite -type_virt_refl //. Qed.

  Lemma abduct_type_virt_sep Δ lv A P Φ R:
    abduct Δ (abd_assume CTX P (type_virt lv A Φ) ) R →
    abduct Δ (type_virt lv (sepT A P) Φ) R.
  Proof. rewrite -type_virt_sep //. Qed.

  Lemma abduct_type_virt_constraint Δ lv A φ Φ R:
    abduct Δ (abd_assume CTX (⌜φ⌝) (type_virt lv A Φ)) R →
    abduct Δ (type_virt lv (constraintT A φ) Φ) R.
  Proof. rewrite -type_virt_constraint //. Qed.

  Lemma abduct_type_virt_exists Δ lv X (A: X → type Σ) Φ R:
    (abduct Δ (∀ x, type_virt lv (A x) Φ) R) →
    abduct Δ (type_virt lv (ex x, A x) Φ) R.
  Proof. rewrite -type_virt_exists //. Qed.

  Lemma abduct_type_virt_is_ty Δ lv A B Φ R:
    Δ ⊨ ctx_hyp (A `is_ty` B) →
    abduct Δ (type_virt lv B Φ) R →
    abduct Δ (type_virt lv A Φ) R.
  Proof.
    intros Hent Habd. apply: abduct_qenvs_entails=>?.
    rewrite -type_virt_is_ty //.
  Qed.

  Lemma abduct_type_virt_moved_copy Δ lv A Φ R:
    Δ ⊨ Copy A →
    abduct Δ (type_virt lv A Φ) R →
    abduct Δ (type_virt lv (movedT A) Φ) R.
  Proof.
    intros Hent Habd. apply: abduct_qenvs_entails=>?.
    rewrite -type_virt_moved_copy //.
  Qed.

  Lemma abduct_type_virt_moved_own Δ lv (p: loc) A Φ R:
    abduct Δ (type_virt lv (valueT p ptr_size) Φ) R →
    abduct Δ (type_virt lv (movedT (ownT p A)) Φ) R.
  Proof. rewrite -type_virt_moved_own //. Qed.

  Lemma abduct_type_virt_moved_is_ty Δ lv A B Φ R:
    Δ ⊨ ctx_hyp (A `is_ty` B) →
    abduct Δ (type_virt lv (movedT B) Φ) R →
    abduct Δ (type_virt lv (movedT A) Φ) R.
  Proof.
    intros Hent Habd. apply: abduct_qenvs_entails=>?.
    rewrite -type_virt_moved_is_ty //.
  Qed.

  Lemma abduct_type_virt_refinement Δ lv {X} (T: rtype Σ X) (x: X) B Φ R:
    unfold_refinement T x B →
    abduct Δ (type_virt lv B Φ) R →
    abduct Δ (type_virt lv (x @ T) Φ) R.
  Proof.
    intros Hent Habd. rewrite -type_virt_equiv //.
  Qed.


  Global Instance abduct_step_type_virt_refl Δ lv A Φ R:
    AbductStep (abduct Δ (Φ A) R)
      Δ (type_virt lv A Φ) R | 20.
  Proof. intros ?. by eapply abduct_type_virt_refl. Qed.

  Global Instance abduct_step_type_virt_sep Δ lv A P Φ R:
    AbductStep (abduct Δ (abd_assume CTX P (type_virt lv A Φ)) R)
      Δ (type_virt lv (sepT A P) Φ) R.
  Proof. intros ?. by eapply abduct_type_virt_sep. Qed.

  Global Instance abduct_step_type_virt_constraint Δ lv A φ Φ R:
    AbductStep (abduct Δ (abd_assume CTX (⌜φ⌝) (type_virt lv A Φ)) R)
      Δ (type_virt lv (constraintT A φ) Φ) R.
  Proof. intros ?. by eapply abduct_type_virt_constraint. Qed.

  Global Instance abduct_step_type_virt_exists Δ lv X (A: X → type Σ) Φ R:
    AbductStep (abduct Δ (∀ x, type_virt lv (A x) Φ) R)
      Δ (type_virt lv (ex x, A x) Φ) R.
  Proof. intros ?. by eapply abduct_type_virt_exists. Qed.

  Global Instance abduct_step_type_virt_is_ty Δ lv A B Φ R:
    Δ ⊨ ctx_hyp (A `is_ty` B) →
    AbductStep (abduct Δ (type_virt lv B Φ) R)
      Δ (type_virt lv A Φ) R.
  Proof. intros ??. by eapply abduct_type_virt_is_ty. Qed.

  Global Instance abduct_step_type_virt_moved_copy Δ lv A Φ R:
    Δ ⊨ Copy A →
    AbductStep (abduct Δ (type_virt lv A Φ) R)
      Δ (type_virt lv (movedT A) Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_virt_moved_copy. Qed.

  Global Instance abduct_step_type_virt_moved_own Δ lv (p: loc) A Φ R:
    AbductStep (abduct Δ (type_virt lv (valueT p ptr_size) Φ) R)
      Δ (type_virt lv (movedT (ownT p A)) Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_virt_moved_own. Qed.

  Global Instance abduct_step_type_virt_moved_is_ty Δ lv A B Φ R:
    Δ ⊨ ctx_hyp (A `is_ty` B) →
    AbductStep (abduct Δ (type_virt lv (movedT B) Φ) R)
      Δ (type_virt lv (movedT A) Φ) R | 3.
  Proof. intros ??. by eapply abduct_type_virt_moved_is_ty. Qed.

  Global Instance abduct_step_type_virt_refinement Δ lv {X} (T: rtype Σ X) (x: X) B Φ R:
    unfold_refinement T x B →
    AbductStep (abduct Δ (type_virt lv B Φ) R)
      Δ (type_virt lv (x @ T) Φ) R.
  Proof. intros ??. by eapply abduct_type_virt_refinement. Qed.

End virtual_types.

Existing Class virtual_type.
Global Hint Mode virtual_type - - ! : typeclass_instances.

Global Existing Instance sep_virtual_type.
Global Existing Instance constraint_virtual_type.
Global Existing Instance refinement_virtual_type.
Global Existing Instance exists_virtual_type.
Global Existing Instance type_var_virtual_type | 10.
Global Existing Instance moved_virtual_type | 2.
Global Existing Instance moved_own_virtual_type | 1.
Global Existing Instance moved_copy_virtual_type | 3.


Section learn_simpl.
  Context `{!refinedcG Σ}.

  Definition learn_simpl (φ: Prop) (A B: type Σ) : Prop :=
    φ → A ⊑ B.

  Lemma learn_simpl_refl φ A : learn_simpl φ A A.
  Proof. rewrite /learn_simpl //. Qed.

  Lemma learn_simpl_own φ p A B :
    learn_simpl φ A B →
    learn_simpl φ (ownT p A) (ownT p B).
  Proof.
    rewrite /learn_simpl //. intros Hsub Hφ.
    rewrite Hsub //.
  Qed.

  Lemma learn_simpl_optional_some (φ: Prop) (ψ: dProp) A B:
    unify φ ψ →
    learn_simpl φ A B →
    learn_simpl φ (optionalT ψ A) B.
  Proof.
    rewrite /unify. intros -> Hsimpl Hφ.
    rewrite /optionalT /case. destruct decide; naive_solver.
  Qed.

  Lemma learn_simpl_optional_none (φ: Prop) (ψ: dProp) A:
    unify φ (¬ ψ)%DP →
    learn_simpl φ (optionalT ψ A) nullT.
  Proof.
    rewrite /unify. simpl; intros -> Hφ.
    rewrite /optionalT /case.
    destruct decide; naive_solver.
  Qed.

  Lemma learn_simpl_optional_some_double_neg (φ: Prop) (ψ: dProp) A B:
    unify φ (¬ (¬ ψ)) →
    learn_simpl φ A B →
    learn_simpl φ (optionalT ψ A) B.
  Proof.
    rewrite /unify. intros -> Hsimpl Hφ.
    rewrite /optionalT /case. destruct decide; naive_solver.
  Qed.

  Lemma learn_simpl_optional_lift (φ: Prop) (ψ: dProp) A B:
    learn_simpl φ A B →
    learn_simpl φ (optionalT ψ A) (optionalT ψ B).
  Proof.
    intros Hsimpl Hφ. rewrite Hsimpl //.
  Qed.

  Lemma learn_simpl_prop_optional_not_null φ v A :
    learn_simpl_prop (not_null v) (v ◁ᵥ optionalT φ A) (v ◁ᵥ constraintT A φ).
  Proof.
    rewrite /learn_simpl_prop.
    iIntros (Hnn) "Hv". rewrite /optionalT /case.
    case_decide.
    - rewrite vtr_constraintT_iff. by iFrame.
    - iPoseProof (nullT_inv with "Hv") as "%Ha".
      simpl in Hnn. done.
  Qed.

  Lemma learn_simpl_prop_optional_not_not_null φ v A :
    learn_simpl_prop (¬ not_null v) (v ◁ᵥ optionalT φ A) (v ◁ᵥ nullT).
  Proof.
    rewrite /learn_simpl_prop.
    iIntros (Hnn) "Hv".
    rewrite vtr_nullT_iff.
    simpl in Hnn.
    apply dec_stable in Hnn.
    done.
  Qed.

  Lemma learn_simpl_exists (φ: Prop) {X: Type} (A B: X → type Σ):
    (∀! x, learn_simpl φ (A x) (B x)) →
    learn_simpl φ (existsT A) (existsT B).
  Proof.
    intros Hsimpl Hφ. eapply existsT_le. intros ?.
    by eapply Hsimpl.
  Qed.

  Lemma learn_simpl_constraint (φ ψ: Prop) (A B: type Σ):
    (learn_simpl φ A B) →
    learn_simpl φ (constraintT A ψ) (constraintT B ψ).
  Proof.
    intros Hsimpl Hφ. rewrite Hsimpl //.
  Qed.

  Lemma learn_simpl_struct (φ: Prop) sl T U :
    TCForall2 (λ A B, learn_simpl φ A B) T U →
    learn_simpl φ (structT sl T) (structT sl U).
  Proof.
    rewrite TCForall2_Forall2.
    intros Hall Hφ.
    eapply Forall2_impl in Hall; last first.
    { intros x y Hxy; eapply (Hxy Hφ). }
    eapply structT_le; done.
  Qed.

  Lemma learn_simpl_array (φ: Prop) ly T U :
    TCForall2 (λ A B, learn_simpl φ A B) T U →
    learn_simpl φ (type_arrayT ly T) (type_arrayT ly U).
  Proof.
    rewrite TCForall2_Forall2.
    intros Hall Hφ.
    eapply Forall2_impl in Hall; last first.
    { intros x y Hxy; eapply (Hxy Hφ). }
    eapply type_array_le; done.
  Qed.

  Lemma learn_simpl_prop_vtr φ v A B:
    learn_simpl φ A B →
    learn_simpl_prop φ (v ◁ᵥ A) (v ◁ᵥ B).
  Proof.
    rewrite /learn_simpl /learn_simpl_prop.
    intros Hsub Hφ; rewrite Hsub //.
  Qed.

  Lemma learn_simpl_prop_ltr φ l A B:
    learn_simpl φ A B →
    learn_simpl_prop φ (l ◁ₗ A) (l ◁ₗ B).
  Proof.
    rewrite /learn_simpl /learn_simpl_prop.
    intros Hsub Hφ; rewrite Hsub //.
  Qed.

  Lemma learn_simpl_prop_vty φ A B:
    learn_simpl φ A B →
    learn_simpl_prop φ (vty A) (vty B).
  Proof.
    rewrite /learn_simpl /learn_simpl_prop.
    intros Hsub Hφ. rewrite /vty. iIntros "[%v Hv]".
    iExists v. destruct (Hsub Hφ) as [_ Hval].
    by iApply Hval.
  Qed.

End learn_simpl.

Existing Class learn_simpl.
Global Hint Mode learn_simpl - - ! ! - : typeclass_instances.

Global Existing Instance learn_simpl_refl | 10.
Global Existing Instance learn_simpl_own | 2.
Global Existing Instance learn_simpl_optional_none | 1.
Global Existing Instance learn_simpl_optional_some | 1.
Global Existing Instance learn_simpl_optional_some_double_neg | 1.
Global Existing Instance learn_simpl_optional_lift | 2.
Global Existing Instance learn_simpl_array | 2.
Global Existing Instance learn_simpl_struct | 2.
Global Existing Instance learn_simpl_exists | 2.
Global Existing Instance learn_simpl_constraint | 2.

Global Existing Instance learn_simpl_prop_vtr | 3.
Global Existing Instance learn_simpl_prop_ltr | 3.
Global Existing Instance learn_simpl_prop_vty | 3.
Global Existing Instance learn_simpl_prop_optional_not_null | 2.
Global Existing Instance learn_simpl_prop_optional_not_not_null | 2.
