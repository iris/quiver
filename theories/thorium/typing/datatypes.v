From quiver.thorium.logic Require Import datatypes type_cast.
From quiver.thorium.typing Require Import assume_type controlflow.
From quiver.argon.abduction Require Import abduct proofmode pure.


Section structs.
  Context `{!refinedcG Σ}.

  (* STRUCT-MEMBER *)
  Lemma abduct_get_member_struct (s: struct_layout) i Δ Φ R T U L l m:
    field_index_of (sl_members s) m = Some i →
    T !! i = Some L →
    Simpl (<[i:=placeT (l at{s}ₗ m)]> T) U →
    (abduct Δ (abd_assume CTX (l ◁ₗ structT s U) $ Φ (l at{s}ₗ m) L) R) →
    abduct Δ (type_get_member l (structT s T) s m Φ) R.
  Proof.
    iIntros (H1 H2 Hsimpl Habd) "HΔ".
    rewrite -type_get_member_struct_type //.
    iIntros "Hl". iDestruct (Habd with "HΔ") as "Hcont".
    rewrite /abd_assume -Hsimpl. by iApply "Hcont".
  Qed.

  Lemma abduct_type_get_member_place Δ x l sl m Φ R:
    abduct Δ (type_loc l (λ r A, type_get_member r A sl m Φ)) R →
    abduct Δ (type_get_member x (placeT l) sl m Φ) R.
  Proof.
    rewrite -type_get_member_place //.
  Qed.

  Lemma abduct_type_get_member_any Δ r (sl: struct_layout) m Φ R:
    abduct Δ (abd_simpl (map anyT (member_sizes sl)) (λ L, type_get_member r (structT sl L) sl m Φ)) R →
    abduct Δ (type_get_member r (anyT (ly_size sl)) sl m Φ) R.
  Proof. rewrite /abd_simpl -type_get_member_any //. Qed.

  Lemma abduct_type_get_member_zeros Δ r (sl: struct_layout) m Φ R:
    abduct Δ (abd_simpl (map zerosT (member_sizes sl)) (λ L, type_get_member r (structT sl L) sl m Φ)) R →
    abduct Δ (type_get_member r (zerosT (ly_size sl)) sl m Φ) R.
  Proof. rewrite /abd_simpl -type_get_member_zeros //. Qed.

  (* DEPRECATED: we add the places instead, see [abduct_type_get_member_var_places]*)
  Lemma abduct_type_get_member_var Δ l A sl m n Φ R:
    exi A →
    (length (sl_members sl) = n) →
    abduct Δ (existsN n (λ L, abduct_pure (A `is_ty` (structT sl L)) (type_get_member l (structT sl L) sl m Φ))) R →
    abduct Δ (type_get_member l A sl m Φ) R.
  Proof.
    rewrite /existsN. intros _ Heq Habd.
    iIntros "Ctx".
    iPoseProof (Habd with "Ctx") as "[%L [%Hlen Hcont]]".
    rewrite /abduct_pure. rewrite /type_get_member.
    by iDestruct "Hcont" as "[-> Hcont]".
  Qed.

  Lemma abduct_type_get_member_var_places Δ l A sl m X Φ R:
    exi A →
    Simpl (map (λ m, placeT (l at{sl}ₗ m)) (field_names (sl_members sl))) X →
    abduct Δ (abduct_pure (A `is_ty` (structT sl X)) (type_get_member l (structT sl X) sl m Φ)) R →
    abduct Δ (type_get_member l A sl m Φ) R.
  Proof.
    intros _ _ Habd. iIntros "Ctx". iPoseProof (Habd with "Ctx") as "Hcont".
    rewrite /abduct_pure. iDestruct "Hcont" as "[%Hty Hcont]".
    rewrite /type_get_member. iIntros "Hl". destruct Hty as [Hty]. rewrite Hty.
    by iApply "Hcont".
  Qed.

  Lemma abduct_type_get_member_virtual Δ l A sl m Φ R:
    Δ ⊨ virtual_type A →
    abduct Δ (type_virt (LOC l) A (λ A', type_get_member l A' sl m Φ)) R →
    abduct Δ (type_get_member l A sl m Φ) R.
  Proof. rewrite -type_get_member_virtual //. Qed.


  Lemma abduct_type_get_member_slices Δ l len Ls (sl: struct_layout) m Φ R:
    abduct Δ (type_project_slice l 0 (ly_size sl) Ls (λ i li A L R, ⌜li = (ly_size sl)⌝ ∗
      (l ◁ₗ slicesT len (L ++ (0, ly_size sl: Z, placeT (l +ₗ 0)) :: R) -∗
      type_get_member (l +ₗ 0) A sl m Φ)))%I R →
    abduct Δ (type_get_member l (slicesT len Ls) sl m Φ) R.
  Proof. intros. rewrite -type_get_member_slices //. Qed.



  (* STRUCT-MEMBER *)
  Global Instance abduct_step_get_member_structT (s: struct_layout) i Δ Φ R T U L l m:
    Computable (field_index_of (sl_members s) m) (Some i) →
    Computable (T !! i) (Some L) →
    Simpl (<[i:=placeT (l at{s}ₗ m)]> T) U →
    AbductStep (abduct Δ (abd_assume CTX (l ◁ₗ structT s U) $ Φ (l at{s}ₗ m) L) R) Δ (type_get_member l (structT s T) s m Φ) R | 1.
  Proof.
    intros ????. by eapply abduct_get_member_struct.
  Qed.

  Global Instance abduct_step_type_get_member_place Δ x l sl m Φ R:
    AbductStep (abduct Δ (type_loc l (λ r A, type_get_member r A sl m Φ)) R) Δ (type_get_member x (placeT l) sl m Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_get_member_place. Qed.

  Global Instance abduct_step_type_get_member_any Δ r (sl: struct_layout) m Φ R:
    AbductStep (abduct Δ (abd_simpl (map anyT (member_sizes sl)) (λ L, type_get_member r (structT sl L) sl m Φ)) R) Δ (type_get_member r (anyT (ly_size sl)) sl m Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_get_member_any. Qed.

  Global Instance abduct_step_type_get_member_zeros Δ r (sl: struct_layout) m Φ R:
    AbductStep (abduct Δ (abd_simpl (map zerosT (member_sizes sl)) (λ L, type_get_member r (structT sl L) sl m Φ)) R) Δ (type_get_member r (zerosT (ly_size sl)) sl m Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_get_member_zeros. Qed.

  Global Instance abduct_step_type_get_member_virtual Δ l A sl m Φ R:
    Δ ⊨ virtual_type A →
    AbductStep (abduct Δ (type_virt (LOC l) A (λ A', type_get_member l A' sl m Φ)) R)
      Δ (type_get_member l A sl m Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_get_member_virtual. Qed.

  Global Instance abduct_step_type_get_member_var Δ l A sl m X Φ R:
    exi A →
    Simpl (map (λ m, placeT (l at{sl}ₗ m)) (field_names (sl_members sl))) X →
    AbductStep (abduct Δ (abduct_pure (A `is_ty` (structT sl X)) (type_get_member l (structT sl X) sl m Φ)) R)
      Δ (type_get_member l A sl m Φ) R.
  Proof. intros ???. by eapply abduct_type_get_member_var_places. Qed.


  Global Instance abduct_step_type_get_member_slices Δ l len Ls (sl: struct_layout) m Φ R:
    AbductStep _ _ _ _ | 5 := abduct_type_get_member_slices Δ l len Ls sl m Φ R.

End structs.


Section arrays.
  Context `{!refinedcG Σ}.

  (* AT OFFSET *)
  Lemma abduct_type_at_offset_value Δ l v A ot w ly ot1 ot2 Φ R:
    abduct Δ (type_val w (λ u B, abd_assume CTX (v ◁ᵥ valueT u ot) (type_at_offset l u A B ly ot1 ot2 Φ))) R →
    abduct Δ (type_at_offset l v A (valueT w ot) ly ot1 ot2 Φ) R.
  Proof. intros ?; by rewrite -type_at_offset_value. Qed.

  Global Instance abduct_step_type_at_offset_value Δ l v A ot w ly ot1 ot2 Φ R:
    AbductStep (abduct Δ (type_val w (λ u B, abd_assume CTX (v ◁ᵥ valueT u ot) (type_at_offset l u A B ly ot1 ot2 Φ))) R)
      Δ (type_at_offset l v A (valueT w ot) ly ot1 ot2 Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_at_offset_value. Qed.

  Lemma abduct_type_at_offset_place Δ l r v ly ot1 ot2 B Φ R :
    abduct Δ (type_loc r (λ r' A, abd_assume CTX (l ◁ₗ placeT r') (type_at_offset r' v A B ly ot1 ot2 Φ))) R →
    abduct Δ (type_at_offset l v (placeT r) B ly ot1 ot2 Φ) R.
  Proof. intros ?; by rewrite -type_at_offset_place. Qed.

  Global Instance abduct_step_type_at_offset_place Δ l r v ly ot1 ot2 B Φ R :
    AbductStep (abduct Δ (type_loc r (λ r' A, abd_assume CTX (l ◁ₗ placeT r') (type_at_offset r' v A B ly ot1 ot2 Φ))) R)
      Δ (type_at_offset l v (placeT r) B ly ot1 ot2 Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_at_offset_place. Qed.

  Lemma abduct_type_at_offset_raw_pointer Δ l v ly it i A Φ R :
    abduct Δ (type_ptr_offset l A (ly_size ly * i) (λ r B, abd_assume CTX (v ◁ᵥ intT it i) (Φ r B))) R →
    abduct Δ (type_at_offset l v A (intT it i) ly PtrOp (IntOp it) Φ) R.
  Proof. intros ?; by rewrite -type_at_offset_raw_pointer. Qed.

  Global Instance abduct_step_type_at_offset_raw_pointer Δ l v ly it i A Φ R :
    AbductStep (abduct Δ (type_ptr_offset l A (ly_size ly * i) (λ r B, abd_assume CTX (v ◁ᵥ intT it i) (Φ r B))) R)
      Δ (type_at_offset l v A (intT it i) ly PtrOp (IntOp it) Φ) R | 2.
  Proof. intros ?. by eapply abduct_type_at_offset_raw_pointer. Qed.

  Lemma abduct_type_at_offset_type_var Δ l v A B ly it Φ R:
    exi B →
    abduct Δ ((∃ n, ⌜B `is_ty` intT it n⌝ ∗ type_at_offset l v A (intT it n) ly PtrOp (IntOp it) Φ)) R →
    abduct Δ (type_at_offset l v A B ly PtrOp (IntOp it) Φ) R.
  Proof. intros ??. by rewrite -type_at_offset_type_var. Qed.

  Global Instance abduct_step_type_at_offset_type_var Δ l v A B ly it Φ R:
    exi B →
    AbductStep (abduct Δ ((∃ n, ⌜B `is_ty` intT it n⌝ ∗ type_at_offset l v A (intT it n) ly PtrOp (IntOp it) Φ)) R)
      Δ (type_at_offset l v A B ly PtrOp (IntOp it) Φ) R | 10.
  Proof. intros ??. by eapply abduct_type_at_offset_type_var. Qed.



  (* PTR OFFSET *)
  Lemma abduct_type_ptr_offset_create_raw_pointer Δ l i A Φ R:
    abduct Δ (type_bounds A (λ n, ⌜0 ≤ i ≤ n⌝ ∗ (l ◁ₗ A -∗ Φ (l +ₗ i) (ptrT l n i)))%I) R →
    abduct Δ (type_ptr_offset l A i Φ) R.
  Proof. intros ?. by rewrite -type_ptr_offset_bounds. Qed.

  Global Instance abduct_step_type_ptr_offset_create_raw_pointer Δ l i A Φ R:
    AbductStep (abduct Δ (type_bounds A (λ n, ⌜0 ≤ i ≤ n⌝ ∗ (l ◁ₗ A -∗ Φ (l +ₗ i) (ptrT l n i)))%I) R)
      Δ (type_ptr_offset l A i Φ) R | 10.
  Proof. intros ?. eapply abduct_type_ptr_offset_create_raw_pointer; eauto. Qed.

  Lemma abduct_type_ptr_offset_shift_raw_pointer Δ l r i j n Φ R:
    abduct Δ (⌜0 ≤ i + j ≤ n⌝ ∗ Φ (r +ₗ (i + j)) (ptrT r n (i + j))) R →
    abduct Δ (type_ptr_offset l (ptrT r n i) j Φ) R.
  Proof. intros ?; by rewrite -type_ptr_offset_shift_raw_pointer. Qed.

  Global Instance abduct_step_type_ptr_offset_shift_raw_pointer Δ l r i j n Φ R:
    AbductStep (abduct Δ (⌜0 ≤ i + j ≤ n⌝ ∗ Φ (r +ₗ (i + j)) (ptrT r n (i + j))) R)
      Δ (type_ptr_offset l (ptrT r n i) j Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_ptr_offset_shift_raw_pointer. Qed.

  Lemma abduct_type_ptr_offset_exi l i A Φ Δ R :
    exi A →
    abduct Δ (⌜A `is_ty` empT⌝ ∗ abd_assume CTX (l ◁ₗ empT) $ type_loc (l +ₗ i) (λ r' B, abduct_class (NonNull B) (Φ r' B))) R →
    abduct Δ (type_ptr_offset l A i Φ) R.
  Proof. by rewrite -type_ptr_offset_exi. Qed.

  Global Instance abduct_step_type_ptr_offset_exi l i A Φ Δ R H :
    AbductStep _ _ _ _ := abduct_type_ptr_offset_exi l i A Φ Δ R H.

  Lemma abduct_type_ptr_offset_empT l i Φ Δ R :
    abduct Δ (abd_assume CTX (l ◁ₗ empT) $ type_loc (l +ₗ i) (λ r' B, abduct_class (NonNull B) (Φ r' B))%I) R →
    abduct Δ (type_ptr_offset l empT i Φ) R.
  Proof. by rewrite type_ptr_offset_empT. Qed.

  Global Instance abduct_step_type_ptr_offset_empT l i Φ Δ R :
    AbductStep _ _ _ _ := abduct_type_ptr_offset_empT l i Φ Δ R.

  (* OFFSET-TO-IDX *)
  Lemma abduct_type_offset_to_idx_mul_l Δ i sz Φ R:
    abduct Δ (Φ i) R →
    abduct Δ (@type_offset_to_idx Σ (sz * i) sz Φ) R.
  Proof. by rewrite -type_offset_to_idx_mul_l. Qed.
  Global Instance abduct_step_type_offset_to_idx_mul_l Δ i sz Φ R :
    AbductStep _ _ _ _ := abduct_type_offset_to_idx_mul_l Δ i sz Φ R.

  Lemma type_offset_to_idx_mk_array_layout (n: nat) ly (Φ: Z → iProp Σ) :
    Φ n ⊢ @type_offset_to_idx Σ (ly_size (mk_array_layout ly n)) (ly_size ly) Φ.
  Proof.
    rewrite /type_offset_to_idx. iIntros "HΦ". iExists n. iFrame.
    iPureIntro. rewrite ly_size_mk_array_layout. lia.
  Qed.

  Lemma abduct_type_offset_to_idx_mk_array_layout Δ (n: nat) ly (Φ: Z → iProp Σ) R:
    abduct Δ (Φ n) R →
    abduct Δ (@type_offset_to_idx Σ (ly_size (mk_array_layout ly n)) (ly_size ly) Φ) R.
  Proof. rewrite -type_offset_to_idx_mk_array_layout //. Qed.

  Global Instance abduct_step_type_offset_to_idx_mk_array_layout Δ (n: nat) ly (Φ: Z → iProp Σ) R :
    AbductStep _ _ _ _ := abduct_type_offset_to_idx_mk_array_layout  Δ n ly Φ R.


  (* USE AND ASSIGN  *)
  Lemma abduct_type_use_raw_ptr  Δ r i ot m l n Φ R:
    Simpl (ly_size (ot_layout ot)) m →
    abduct Δ (type_loc r (λ r' A, type_use_at_idx r' A m i Φ)) R →
    abduct Δ (type_use l (ptrT r n i) ot Φ) R.
  Proof. intros <-. rewrite -type_use_raw_ptr //. Qed.

  Global Instance abduct_step_type_use_raw_ptr Δ r i ot m l n Φ R:
    Simpl (ly_size (ot_layout ot)) m →
    AbductStep (abduct Δ (type_loc r (λ r' A, type_use_at_idx r' A m i Φ)) R)
      Δ (type_use l (ptrT r n i) ot Φ) R | 5.
  Proof. intros ??. by eapply abduct_type_use_raw_ptr. Qed.

  Lemma abuduct_type_assign_raw_pointer Δ r n l i v B ot m s cfg Φ R:
    Simpl (ly_size (ot_layout ot)) m →
    abduct Δ (abduct_class (B `has_size` m) (type_loc r (λ r' A, type_assign_at_idx r' A v B m i (swp s cfg Φ)))) R →
    abduct Δ (type_assign l (ptrT r n i) v B ot s cfg Φ) R.
  Proof. intros <-. rewrite /abduct_class. rewrite -type_assign_raw_pointer //. Qed.

  Global Instance abduct_step_type_assign_raw_pointer Δ r n l i v B ot m s cfg Φ R:
    Simpl (ly_size (ot_layout ot)) m →
    AbductStep (abduct Δ (abduct_class (B `has_size` m) (type_loc r (λ r' A, type_assign_at_idx r' A v B m i (swp s cfg Φ)))) R)
      Δ (type_assign l (ptrT r n i) v B ot s cfg Φ) R | 5.
  Proof. intros ??. by eapply abuduct_type_assign_raw_pointer. Qed.

  Lemma abduct_type_get_member_raw_ptr Δ r i (sl : struct_layout) l n m Φ R :
    abduct Δ (type_loc r (λ r' A, type_borrow_at_idx r' A (ly_size sl) i (λ r'' B, type_get_member r'' B sl m Φ))) R →
    abduct Δ (type_get_member l (ptrT r n i) sl m Φ) R.
  Proof. by rewrite -type_get_member_raw_ptr. Qed.
  Global Instance abduct_step_type_get_member_raw_ptr Δ r i (sl : struct_layout) l n m Φ R :
    AbductStep _ _ _ _ := abduct_type_get_member_raw_ptr Δ r i sl l n m Φ R.

  (* POINTER USE/ASSIGN *)
  Lemma abduct_type_assign_at_idx_value Δ r A v w n m i P R:
    abduct Δ (type_val w (λ u B, abd_assume CTX (v ◁ᵥ valueT w n) $ (type_assign_at_idx r A u B m i P))) R →
    abduct Δ (type_assign_at_idx r A v (valueT w n) m i P) R.
  Proof. intros ?. rewrite -type_assign_at_idx_value //. Qed.

  Global Instance abduct_step_type_assign_at_idx_value Δ r A v w n m i P R:
    AbductStep (abduct Δ (type_val w (λ u B, abd_assume CTX (v ◁ᵥ valueT w n) $ (type_assign_at_idx r A u B m i P))) R)
      Δ (type_assign_at_idx r A v (valueT w n) m i P) R | 1.
  Proof. intros ?. by apply abduct_type_assign_at_idx_value. Qed.

  Lemma abduct_type_use_at_idx_any Δ l n m i Φ R:
    abduct Δ ((⌜0 ≤ i⌝ ∗ ⌜0 ≤ m⌝ ∗ ⌜i + m ≤ n⌝ ∗ abd_assume CTX (l ◁ₗ anyT n) $ ∀ v, Φ v (anyT m))%I) R →
    abduct Δ (type_use_at_idx l (anyT n) m i Φ) R.
  Proof. by rewrite -type_use_at_idx_any. Qed.
  Global Instance abduct_step_type_use_at_idx_any Δ l n m i Φ R:
    AbductStep _ _ _ _ := abduct_type_use_at_idx_any Δ l n m i Φ R.

  Lemma abduct_type_assign_at_idx_any l n m v A i Φ Δ R:
    abduct Δ (⌜0 ≤ i⌝ ∗ ⌜0 ≤ m⌝ ∗ ⌜i + m ≤ n⌝ ∗ ⌜A `has_size` m⌝ ∗ abd_assume CTX (l ◁ₗ anyT n) $ Φ)%I R →
    abduct Δ (type_assign_at_idx l (anyT n) v A m i Φ) R.
  Proof. by rewrite -type_assign_at_idx_any. Qed.
  Global Instance abduct_step_type_assign_at_idx_any l n m v A i Φ Δ R:
    AbductStep _ _ _ _ := abduct_type_assign_at_idx_any l n m v A i Φ Δ R.


  Lemma abduct_type_assign_at_idx_array_copy Δ X (T: X → type Σ) r xs sz v x m i P R:
    (∀ x, Copy (T x)) →
    abduct Δ (type_offset_to_idx i sz (λ j, ⌜m = sz⌝ ∗ ⌜0 ≤ j < length xs⌝ ∗ (abd_assume CTX (r ◁ₗ arrayT T sz (<[Z.to_nat j := x]> xs)) $ abd_assume CTX (v ◁ᵥ (T x))%RT $ P)%RT)%I) R →
    abduct Δ (type_assign_at_idx r (arrayT T sz xs) v (T x) m i P) R.
  Proof.
    intros ??. rewrite -type_assign_at_idx_array_copy //.
  Qed.

  Global Instance abduct_step_type_assign_at_idx_array_copy Δ X (T: X → type Σ) r xs sz v x m i P R:
    (∀ x, Copy (T x)) →
    AbductStep (abduct Δ (type_offset_to_idx i sz (λ j, ⌜m = sz⌝ ∗ ⌜0 ≤ j < length xs⌝ ∗ (abd_assume CTX (r ◁ₗ arrayT T sz (<[Z.to_nat j := x]> xs)) $ abd_assume CTX (v ◁ᵥ (T x))%RT $ P)%RT)%I) R)
      Δ (type_assign_at_idx r (arrayT T sz xs) v (T x) m i P) R | 1.
  Proof. intros ??. by eapply abduct_type_assign_at_idx_array_copy. Qed.

  Lemma abduct_type_assign_at_idx_array_exi_arg Δ X (T: X → type Σ) r xs sz v B m i P R:
    exi B →
    abduct Δ (∃ x, ⌜B `is_ty` (T x)⌝ ∗ type_assign_at_idx r (arrayT T sz xs) v (T x) m i P) R →
    abduct Δ (type_assign_at_idx r (arrayT T sz xs) v B m i P) R.
  Proof. intros ??. rewrite -type_assign_at_idx_array_exi_arg //. Qed.

  Global Instance abduct_step_type_assign_at_idx_array_exi_arg Δ X (T: X → type Σ) r xs sz v B m i P R:
    exi B →
    AbductStep (abduct Δ (∃ x, ⌜B `is_ty` (T x)⌝ ∗ type_assign_at_idx r (arrayT T sz xs) v (T x) m i P) R)
      Δ (type_assign_at_idx r (arrayT T sz xs) v B m i P) R | 2.
  Proof. intros ??. by eapply abduct_type_assign_at_idx_array_exi_arg. Qed.

  Lemma abduct_type_use_at_idx_array_copy Δ X `{!Inhabited X} (T: X → type Σ) xs r sz m i Φ R:
    (∀ x, Copy (T x)) →
    abduct Δ (type_offset_to_idx i sz (λ j, ⌜m = sz⌝ ∗ ⌜0 ≤ j < length xs⌝ ∗ (abd_assume CTX (r ◁ₗ arrayT T sz xs) $ ∀ x, Φ x (T (xs !!! (Z.to_nat j))%RT)))%I) R →
    abduct Δ (type_use_at_idx r (arrayT T sz xs) m i Φ) R.
  Proof. intros ??. rewrite -type_use_at_idx_array_copy //. Qed.

  Global Instance abduct_step_type_use_at_idx_array_copy Δ X `{!Inhabited X} (T: X → type Σ) xs r sz m i Φ R:
    (∀ x, Copy (T x)) →
    AbductStep (abduct Δ (type_offset_to_idx i sz (λ j, ⌜m = sz⌝ ∗ ⌜0 ≤ j < length xs⌝ ∗ (abd_assume CTX (r ◁ₗ arrayT T sz xs) $ ∀ x, Φ x (T (xs !!! (Z.to_nat j))%RT)))%I) R)
      Δ (type_use_at_idx r (arrayT T sz xs) m i Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_use_at_idx_array_copy. Qed.

  Lemma abduct_type_borrow_at_idx_array Δ X `{!Inhabited X} (T: X → type Σ) xs r sz m i Φ R:
    abduct Δ (type_offset_to_idx i sz (λ j, (⌜m = sz⌝ ∗ ⌜0 ≤ j < length xs⌝ ∗
         (abd_assume CTX (r ◁ₗ wandT (λ x, (r +ₗ i) ◁ₗ T x) (λ x, arrayT T sz (<[Z.to_nat j := x]> xs)))
            $ Φ (r +ₗ i) (T (xs !!! (Z.to_nat j)))%RT)))%I) R →
    abduct Δ (type_borrow_at_idx r (arrayT T sz xs) m i Φ) R.
  Proof. by rewrite -type_borrow_at_idx_array. Qed.
  Global Instance abduct_step_type_borrow_at_idx_array Δ X `{!Inhabited X} (T: X → type Σ) xs r sz m i Φ R:
    AbductStep _ _ _ _ := abduct_type_borrow_at_idx_array Δ X T xs r sz m i Φ R.


  (* PREFIXES AND RANGES *)
  Lemma abduct_type_extract_prefix_match Δ (l: loc) A n m Φ (R : iProp Σ):
    (Δ ⊨ (A `has_bounds` n ∧ Solve (n = m))) →
    abduct Δ (type_move l A n (λ v, Φ l v)) R →
    abduct Δ (type_extract_prefix l A m Φ) R.
  Proof.
    intros Hent Habd. apply: abduct_qenvs_entails. rewrite /Solve. intros [? Heq].
    subst n. rewrite -type_move_extract_prefix //.
  Qed.

  Global Instance abduct_step_type_extract_prefix_match Δ (l: loc) A n m Φ (R : iProp Σ):
    (Δ ⊨ (A `has_bounds` n ∧ Solve (n = m))) →
    AbductStep (abduct Δ (type_move l A n (λ v, Φ l v)) R)
      Δ (type_extract_prefix l A m Φ) R | 5.
  Proof. intros ??; by eapply abduct_type_extract_prefix_match. Qed.

  Lemma abduct_extract_prefix_raw_pointer Δ r l i li n Φ R:
    abduct Δ (type_loc r (λ r' A, abd_assume CTX (l ◁ₗ ptrT r' n i) (type_extract_range r' A i li Φ))) R →
    abduct Δ (type_extract_prefix l (ptrT r n i) li Φ) R.
  Proof. intros ?; by rewrite -type_extract_prefix_raw_pointer. Qed.

  Global Instance abduct_step_extract_prefix_raw_pointer Δ r l i li n Φ R:
    AbductStep (abduct Δ (type_loc r (λ r' A, abd_assume CTX (l ◁ₗ ptrT r' n i) (type_extract_range r' A i li Φ))) R)
      Δ (type_extract_prefix l (ptrT r n i) li Φ) R | 2.
  Proof. intros ?. by eapply abduct_extract_prefix_raw_pointer. Qed.

  Lemma abduct_type_extract_prefix_any Δ l n m Φ R:
    abduct Δ (⌜0 ≤ n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, n, placeT (l +ₗ 0)); (n, m - n, anyT (m - n))] -∗ type_move (l +ₗ 0) (anyT n) n (λ v, Φ (l +ₗ 0) v))) R →
    abduct Δ (type_extract_prefix l (anyT m) n Φ) R.
  Proof. intros ?; by rewrite -type_extract_prefix_any. Qed.

  Global Instance abduct_step_extract_prefix_any Δ l n m Φ R:
    AbductStep (abduct Δ (⌜0 ≤ n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, n, placeT (l +ₗ 0)); (n, m - n, anyT (m - n))] -∗ type_move (l +ₗ 0) (anyT n) n (λ v, Φ (l +ₗ 0) v))) R)
      Δ (type_extract_prefix l (anyT m) n Φ) R | 10.
  Proof. intros ?. by eapply abduct_type_extract_prefix_any. Qed.

  Lemma abduct_type_extract_prefix_zeros Δ l n m Φ R:
    abduct Δ (⌜0 ≤ n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, n, placeT (l +ₗ 0)); (n, m - n, zerosT (m - n))] -∗ type_move (l +ₗ 0) (zerosT n) n (λ v, Φ (l +ₗ 0) v))) R →
    abduct Δ (type_extract_prefix l (zerosT m) n Φ) R.
  Proof. intros ?; by rewrite -type_extract_prefix_zeros. Qed.

  Global Instance abduct_step_extract_prefix_zeros Δ l n m Φ R:
    AbductStep (abduct Δ (⌜0 ≤ n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, n, placeT (l +ₗ 0)); (n, m - n, zerosT (m - n))] -∗ type_move (l +ₗ 0) (zerosT n) n (λ v, Φ (l +ₗ 0) v))) R)
      Δ (type_extract_prefix l (zerosT m) n Φ) R | 10.
  Proof. intros ?. by eapply abduct_type_extract_prefix_zeros. Qed.

  Lemma abduct_type_extract_prefix_start Δ l A n Φ R:
    abduct Δ (type_extract_range l A 0 n Φ) R →
    abduct Δ (type_extract_prefix l A n Φ) R.
  Proof. intros ?; by rewrite -type_extract_prefix_start. Qed.

  Global Instance abduct_step_extract_prefix_start Δ l A n Φ R:
    AbductStep (abduct Δ (type_extract_range l A 0 n Φ) R)
      Δ (type_extract_prefix l A n Φ) R | 100.
  Proof. intros ?. by eapply abduct_type_extract_prefix_start. Qed.

  (* EXTRACT RANGE *)
  Lemma abduct_type_extract_range_ptr Δ l r j i n li Φ R :
    abduct Δ (type_loc r (λ r' A, abd_assume CTX (l ◁ₗ ptrT r' n j) (type_extract_range r' A (i + j) li Φ))) R →
    abduct Δ (type_extract_range l (ptrT r n j) i li Φ) R.
  Proof. intros ?; by rewrite -type_extract_range_ptr. Qed.

  Global Instance abduct_step_extract_range_ptr Δ l r j i n li Φ R :
    AbductStep (abduct Δ (type_loc r (λ r' A, abd_assume CTX (l ◁ₗ ptrT r' n j) (type_extract_range r' A (i + j) li Φ))) R)
      Δ (type_extract_range l (ptrT r n j) i li Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_extract_range_ptr. Qed.

  Lemma abduct_type_extract_range_any Δ l i n m Φ R :
    abduct Δ (⌜0 ≤ i⌝ ∗ ⌜0 ≤ n⌝ ∗ ⌜i + n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, i, anyT i); (i, n, placeT (l +ₗ i)); (i + n, m - n - i, anyT (m - n - i))] -∗ type_move (l +ₗ i) (anyT n) n (λ v, Φ (l +ₗ i) v))) R →
    abduct Δ (type_extract_range l (anyT m) i n Φ) R.
  Proof. intros ?; by rewrite -type_extract_range_any. Qed.

  Global Instance abduct_step_extract_range_any Δ l i n m Φ R :
    AbductStep (abduct Δ (⌜0 ≤ i⌝ ∗ ⌜0 ≤ n⌝ ∗ ⌜i + n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, i, anyT i); (i, n, placeT (l +ₗ i)); (i + n, m - n - i, anyT (m - n - i))] -∗ type_move (l +ₗ i) (anyT n) n (λ v, Φ (l +ₗ i) v))) R)
      Δ (type_extract_range l (anyT m) i n Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_extract_range_any. Qed.

  Lemma abduct_type_extract_range_zeros Δ l i n m Φ R :
    abduct Δ (⌜0 ≤ i⌝ ∗ ⌜0 ≤ n⌝ ∗ ⌜i + n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, i, zerosT i); (i, n, placeT (l +ₗ i)); (i + n, m - n - i, zerosT (m - n - i))] -∗ type_move (l +ₗ i) (zerosT n) n (λ v, Φ (l +ₗ i) v))) R →
    abduct Δ (type_extract_range l (zerosT m) i n Φ) R.
  Proof. intros ?; by rewrite -type_extract_range_zeros. Qed.

  Global Instance abduct_step_extract_range_zeros Δ l i n m Φ R :
    AbductStep (abduct Δ (⌜0 ≤ i⌝ ∗ ⌜0 ≤ n⌝ ∗ ⌜i + n ≤ m⌝ ∗ (l ◁ₗ slicesT m [(0, i, zerosT i); (i, n, placeT (l +ₗ i)); (i + n, m - n - i, zerosT (m - n - i))] -∗ type_move (l +ₗ i) (zerosT n) n (λ v, Φ (l +ₗ i) v))) R)
      Δ (type_extract_range l (zerosT m) i n Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_extract_range_zeros. Qed.

  Lemma abduct_type_extract_range_slices Δ l len Ts i li Φ R:
    abduct Δ (type_project_slice l i li Ts (λ i' li' A L R,
        l ◁ₗ slicesT len (L ++ (i', li', placeT (l +ₗ i')) :: R) -∗
        type_extract_range (l +ₗ i') A (i - i') li Φ))%I R →
    abduct Δ (type_extract_range l (slicesT len Ts) i li Φ) R.
  Proof. intros ?; by rewrite -type_extract_range_slices. Qed.

  Global Instance abduct_step_type_extract_range_slices Δ l len Ts i li Φ R:
    AbductStep _ _ _ _ | 1 := abduct_type_extract_range_slices Δ l len Ts i li Φ R.

  Lemma abduct_type_extract_range_var_middle Δ l A i li Φ R:
    exi A →
    abduct Δ (
      type_bounds A (λ n,
      ∃ A1 A2 A3,
        ⌜A `is_ty` slicesT n [(0, i, A1); (i, li, A2); (i + li, n - i - li, A3)]%Z⌝ ∗
        (abd_assume CTX (l ◁ₗ slicesT n [(0, i, A1); (i, li, placeT (l +ₗ i)); (i + li, n - i - li, A3)]) $ type_move (l +ₗ i) A2 li (λ v, Φ (l +ₗ i) v))
      )%I) R →
    abduct Δ (type_extract_range l A i li Φ) R.
  Proof.
    rewrite -type_extract_range_var_middle //.
  Qed.

  Global Instance abduct_step_type_extract_range_var_middle Δ l A i li Φ R:
    exi A →
    AbductStep (abduct Δ (
      type_bounds A (λ n,
      ∃ A1 A2 A3,
        ⌜A `is_ty` slicesT n [(0, i, A1); (i, li, A2); (i + li, n - i - li, A3)]%Z⌝ ∗
        (abd_assume CTX (l ◁ₗ slicesT n [(0, i, A1); (i, li, placeT (l +ₗ i)); (i + li, n - i - li, A3)]) $ type_move (l +ₗ i) A2 li (λ v, Φ (l +ₗ i) v))
      )%I) R) Δ (type_extract_range l A i li Φ) R.
  Proof. intros ??. by eapply abduct_type_extract_range_var_middle. Qed.

  Lemma abduct_type_extract_range_default Δ l A n m Φ R:
    abduct Δ (⌜n = 0⌝ ∗ type_move l A m (Φ l)) R →
    abduct Δ (type_extract_range l A n m Φ) R.
  Proof. by rewrite -type_extract_range_default. Qed.

  Global Instance abduct_step_type_extract_range_default Δ l A n m Φ R:
    AbductStep (abduct Δ (⌜n = 0⌝ ∗ type_move l A m (Φ l)) R)
      Δ (type_extract_range l A n m Φ) R | 100.
  Proof. intros ?. by eapply abduct_type_extract_range_default. Qed.

  (* PROJECTION *)
  Lemma abduct_type_project_slice_cons_skip Δ l i li j lj A As Φ R:
    Δ ⊨ Solve (j + lj ≤ i) →
    abduct Δ (type_project_slice l i li As (λ i' li' v L' R', Φ i' li' v ((j, lj, A) :: L') R')) R →
    abduct Δ (type_project_slice l i li ((j, lj, A) :: As) Φ) R.
  Proof. intros _. rewrite -type_project_slice_cons_skip //. Qed.

  Global Instance abduct_step_type_project_slice_cons_skip Δ l i li j lj A As Φ R H:
    AbductStep _ _ _ _ | 10 := abduct_type_project_slice_cons_skip Δ l i li j lj A As Φ R H.


  Lemma abduct_type_project_slice_cons_match Δ l i li j lj A As Φ R:
    Δ ⊨ Solve (j ≤ i ∧ i + li ≤ j + lj) →
    abduct Δ (Φ j lj A [] As) R →
    abduct Δ (type_project_slice l i li ((j, lj, A) :: As) Φ) R.
  Proof.
    intros ? Habd. rewrite -type_project_slice_cons_match //.
    apply: abduct_qenvs_entails. rewrite /Solve. intros [? ?].
    iIntros "Hctx". iDestruct (Habd with "Hctx") as "$". by iPureIntro.
  Qed.

  Global Instance abduct_step_type_project_slice_cons_match Δ l i li j lj A As Φ R H:
    AbductStep _ _ _ _ | 1 := abduct_type_project_slice_cons_match Δ l i li j lj A As Φ R H.

  Lemma abduct_type_project_slice_empty Δ l i li Φ R:
    abduct Δ (abd_fail "extracting a slice from an empty slices type") R →
    abduct Δ (type_project_slice l i li nil Φ) R.
  Proof. intros. rewrite -type_project_slice_empty //. Qed.

  Global Instance abduct_step_type_project_slice_empty Δ l i li Φ R:
    AbductStep (abduct Δ (abd_fail "extracting a slice from an empty slices type") R)
      Δ (type_project_slice l i li nil Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_project_slice_empty. Qed.

  Lemma abduct_type_project_slice_singleton_default Δ l i li j lj A Φ R:
    abduct Δ (⌜j ≤ i⌝ ∗ ⌜i + li ≤ j + lj⌝ ∗ Φ j lj A nil nil) R →
    abduct Δ (type_project_slice l i li [(j, lj, A)] Φ) R.
  Proof. intros. rewrite -type_project_slice_cons_match //. Qed.

  Global Instance abduct_step_type_project_slice_singleton_default Δ l i li j lj A Φ R:
    AbductStep _ _ _ _ | 9 := abduct_type_project_slice_singleton_default Δ l i li j lj A Φ R.

  Lemma abduct_type_project_slice_cons_default Δ l i li j lj A As Φ R:
    abduct Δ (case (j ≤ i ∧ i + li ≤ j + lj)
      (Φ j lj A [] As)
      (type_project_slice l i li As (λ i' li' v L R, Φ i' li' v ((j, lj, A) :: L) R))) R →
    abduct Δ (type_project_slice l i li ((j, lj, A) :: As) Φ) R.
  Proof. intros. rewrite -type_project_slice_default //. Qed.

  Global Instance abduct_step_type_project_slice_cons_default Δ l i li j lj A As Φ R:
    AbductStep _ _ _ _ | 15 := abduct_type_project_slice_cons_default Δ l i li j lj A As Φ R.

End arrays.

Section zeros.
  Context `{!refinedcG Σ}.

  Lemma abduct_type_from_zeros_unfold_refinement A X (R : rtype Σ A) F Φ n r Δ Q :
    (∀! x, unfold_refinement R (r x) (F x)) →
    abduct Δ (type_from_zeros X n (λ x, F x) Φ) Q →
    abduct Δ (type_from_zeros X n (λ x, (r x) @ R)%RT Φ) Q.
  Proof. move => ?. by rewrite <-type_from_zeros_unfold_refinement. Qed.
  Global Instance abduct_step_type_from_zeros_unfold_refinement A X (R : rtype Σ A) F Φ n r Δ Q H :
    AbductStep _ _ _ _ | 100 :=
    abduct_type_from_zeros_unfold_refinement A X (R : rtype Σ A) F Φ n r Δ Q H.


  Lemma abduct_type_from_zeros_existsT X Y (T : X → Y → type Σ) Φ n Δ R:
    abduct Δ (type_from_zeros (X * Y) n (λ x, T x.1 x.2) (λ P, (Φ (λ x, ∃ y, P (x, y))%I))) R →
    abduct Δ (type_from_zeros X n (λ x, existsT (T x)) Φ) R.
  Proof. by rewrite -type_from_zeros_existsT. Qed.
  Global Instance abduct_step_type_from_zeros_existsT X Y (T : X → Y → type Σ) Φ n Δ R:
    AbductStep _ _ _ _ := abduct_type_from_zeros_existsT X Y T Φ n Δ R.

  Lemma abduct_type_from_zeros_constraintT X (T : X → type Σ) Q Φ n Δ R :
    abduct Δ (type_from_zeros X n (λ x, T x) (λ P, (Φ (λ x, P x ∗ ⌜Q x⌝)%I))) R →
    abduct Δ (type_from_zeros X n (λ x, constraintT (T x) (Q x)) Φ) R.
  Proof. by rewrite -type_from_zeros_constraintT. Qed.
  Global Instance abduct_step_type_from_zeros_constraintT X (T : X → type Σ) Q Φ n Δ R :
    AbductStep _ _ _ _ := abduct_type_from_zeros_constraintT X T Q Φ n Δ R.

  Lemma abduct_type_from_zeros_sepT X (T : X → type Σ) Q Φ n Δ R `{!∀ x, Persistent (Q x)} :
    abduct Δ (type_from_zeros X n (λ x, T x) (λ P, (Φ (λ x, P x ∗ Q x)%I))) R →
    abduct Δ (type_from_zeros X n (λ x, sepT (T x) (Q x)) Φ) R.
  Proof. by rewrite -type_from_zeros_sepT. Qed.
  Global Instance abduct_step_type_from_zeros_sepT X (T : X → type Σ) Q Φ n Δ R `{!∀ x, Persistent (Q x)} :
    AbductStep _ _ _ _ := abduct_type_from_zeros_sepT X T Q Φ n Δ R.

  Lemma abduct_type_from_zeros_intT X it Φ n z Δ R :
    abduct Δ (Φ (λ x, ⌜n = int_size it⌝ ∗ ⌜z x = 0⌝)%I) R →
    abduct Δ (type_from_zeros X n (λ x, intT it (z x)) Φ) R.
  Proof. by rewrite -type_from_zeros_intT. Qed.
  Global Instance abduct_step_type_from_zeros_intT X it Φ n z Δ R :
    AbductStep _ _ _ _ := abduct_type_from_zeros_intT X it Φ n z Δ R.

  Lemma abduct_type_from_zeros_boolT X it Φ n z Δ R :
    abduct Δ (Φ (λ x, ⌜n = int_size it⌝ ∗ ⌜z x = false⌝)%I) R →
    abduct Δ (type_from_zeros X n (λ x, boolT it (z x)) Φ) R.
  Proof. by rewrite -type_from_zeros_boolT. Qed.
  Global Instance abduct_step_type_from_zeros_boolT X it Φ n z Δ R :
    AbductStep _ _ _ _ := abduct_type_from_zeros_boolT X it Φ n z Δ R.

  Lemma abduct_type_from_zeros_nullT X Φ n Δ R :
    abduct Δ (Φ (λ x, ⌜n = ptr_size⌝)%I) R →
    abduct Δ (type_from_zeros X n (λ x, nullT) Φ) R.
  Proof. by rewrite -type_from_zeros_nullT. Qed.
  Global Instance abduct_step_type_from_zeros_nullT X Φ n Δ R :
    AbductStep _ _ _ _ := abduct_type_from_zeros_nullT X Φ n Δ R.

  Lemma abduct_type_from_zeros_optionalT X Φ n (φ : dProp) A Δ R :
    abduct Δ ((if decide φ then type_from_zeros X n (λ x : X, A x) Φ else Φ (λ _ : X, ⌜n = ptr_size⌝)))%I R →
    abduct Δ (type_from_zeros X n (λ x, optionalT φ (A x)) Φ) R.
  Proof. by rewrite -type_from_zeros_optionalT. Qed.
  Global Instance abduct_step_type_from_zeros_optionalT X Φ n φ A Δ R :
    AbductStep _ _ _ _ := abduct_type_from_zeros_optionalT X Φ n φ A Δ R.

  Lemma abduct_type_from_zeros_optionalT_force_null X Φ n (φ : X → dProp) A Δ R :
    abduct Δ (Φ (λ x : X, ⌜¬ φ x⌝ ∗ ⌜n = ptr_size⌝))%I R →
    abduct Δ (type_from_zeros X n (λ x, optionalT (φ x) (A x)) Φ) R.
  Proof. by rewrite -type_from_zeros_optionalT_force_null. Qed.

  Global Instance abduct_step_type_from_zeros_optionalT_force_null X Φ n φ A Δ R :
    AbductStep _ _ _ _ | 2 := abduct_type_from_zeros_optionalT_force_null X Φ n φ A Δ R.

  Lemma abduct_type_from_zeros_structT X (sl : struct_layout) Ts Φ n (ns : list Z) Δ R :
    Simpl (member_sizes sl) ns →
    abduct Δ (type_from_zeros_all X ns Ts (λ P, Φ (λ x, P x ∗ ⌜n = ly_size sl⌝)%I)) R →
    abduct Δ (type_from_zeros X n (λ x, structT sl (Ts x)) Φ) R.
  Proof. move => ?. by rewrite -type_from_zeros_structT. Qed.
  Global Instance abduct_step_type_from_zeros_structT X sl Ts Φ n ns Δ R H:
    AbductStep _ _ _ _ := abduct_type_from_zeros_structT X sl Ts Φ n ns Δ R H.

  Lemma abduct_type_from_zeros_array A X (T : A → type Σ) Φ n m xs Δ R :
    abduct Δ (type_offset_to_idx n m (λ l, type_from_zeros A m T (λ P,
        Φ (λ x, ∃ y, P y ∗ ⌜0 ≤ m⌝ ∗
            ⌜(∀ a : A, T a `has_size` m)⌝ ∗ ⌜xs x = replicate (Z.to_nat l) y⌝)%I))) R →
    abduct Δ (type_from_zeros X n (λ x, arrayT T m (xs x)) Φ) R.
  Proof. by rewrite -type_from_zeros_array. Qed.
  Global Instance abduct_step_type_from_zeros_array A X (T : A → type Σ) Φ n m xs Δ R :
    AbductStep _ _ _ _ := abduct_type_from_zeros_array A X T Φ n m xs Δ R.
End zeros.
