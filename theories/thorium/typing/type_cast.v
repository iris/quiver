From quiver.thorium.logic Require Export type_cast.
From quiver.argon.abduction Require Import abduct proofmode pure precond.
From quiver.argon.abduction Require Export precond.
From quiver.thorium.typing Require Import assume_type.
From quiver.argon.simplification Require Import simplify elim_existentials normalize cleanup.



Section progress_and_recovery.
  Context `{!refinedcG Σ}.

  Class TypeProgress (Δ : qenvs Σ) (lv: loc_or_val) (X: Type) (A: type Σ) (B: X → type Σ) (p: progress) := {}.

  Global Instance abduct_step_type_progress_type Δ lv X A B p Φ R :
    TypeProgress Δ lv X A B p →
    AbductStep (abduct Δ (Φ p) R)
      Δ (type_progress_type X lv A B Φ) R.
  Proof.
    intros ??. rewrite -type_progress_type_intro //.
  Qed.


  Lemma abduct_type_recover_val_moved Δ v A P (R : iProp Σ):
    Δ ⊨ Copy A →
    abduct Δ (abd_assume CTX (v ◁ᵥ A) P) R →
    abduct Δ (type_recover RECOVER (VAL v) (movedT A) P) R.
  Proof.
    rewrite /abd_assume /type_recover. intros Hcp Habd.
    apply: abduct_qenvs_entails=> Hcp'.
    iIntros "Hctx Hv". iApply (Habd with "Hctx").
    specialize (xtr_moved_copy (VAL v)) as Hent.
    simpl in Hent; rewrite Hent //.
  Qed.

  Lemma abduct_type_recover_val Δ v A P (R : iProp Σ):
    abduct Δ (abd_assume CTX (v ◁ᵥ A) P) R →
    abduct Δ (type_recover RECOVER (VAL v) A P) R.
  Proof. done. Qed.


  Lemma abduct_type_recover_loc Δ pm l A P (R : iProp Σ):
    abduct Δ P R →
    abduct Δ (type_recover pm (LOC l) A P) R.
  Proof. done. Qed.

  Lemma abduct_type_recover_consume Δ v A P (R : iProp Σ):
    abduct Δ P R →
    abduct Δ (type_recover CONSUME (VAL v) A P) R.
  Proof. done. Qed.

  Global Instance abduct_step_type_recover_val_copy Δ v A P (R : iProp Σ):
    Δ ⊨ Copy A →
    AbductStep (abduct Δ (abd_assume CTX (v ◁ᵥ A) P) R)
      Δ (type_recover RECOVER (VAL v) (movedT A) P) R | 1.
  Proof. intros ??; by eapply abduct_type_recover_val_moved. Qed.

  Global Instance abduct_step_type_recover_val Δ v A P (R : iProp Σ):
    AbductStep (abduct Δ (abd_assume CTX (v ◁ᵥ A) P) R)
      Δ (type_recover RECOVER (VAL v) A P) R | 2.
  Proof. intros ?; by eapply abduct_type_recover_val. Qed.

  Global Instance abduct_step_type_recover_loc Δ pm l A P (R : iProp Σ):
    AbductStep (abduct Δ P R)
      Δ (type_recover pm (LOC l) A P) R.
  Proof. intros ?; by eapply abduct_type_recover_loc. Qed.

  Global Instance abduct_step_type_recover_consume Δ v A P (R : iProp Σ):
    AbductStep (abduct Δ P R)
      Δ (type_recover CONSUME (VAL v) A P) R.
  Proof. intros ?; by eapply abduct_type_recover_consume. Qed.

End progress_and_recovery.

Global Hint Mode TypeProgress - - - - - ! ! - : typeclass_instances.



Section type_casts.
  Context `{!refinedcG Σ}.

  (* By default a type cast is blocked.
     For debugging a Quiver proof, it can be helpful
     to disable this instance. *)
  Global Instance type_progress_default Δ lv X A B :
    TypeProgress Δ lv X A B BLOCKED | 10000000 := {}.


  (* integer casting *)
  Global Instance type_progress_int_guaranteed Δ lv n it X (n': X → Z) (it': X → int_type) :
    TypeProgress Δ lv X (intT it n) (λ x, intT (it' x) (n' x)) GUARANTEED := {}.

  Lemma abduct_type_cast_int Δ X pm lv it1 it2 n1 n2 Φ (R : iProp Σ):
    abduct Δ (type_recover pm lv (intT it1 n1) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜n2 x = n1⌝)))%I R →
    abduct Δ (type_cast X pm lv (intT it1 n1) (λ x, intT (it2 x) (n2 x)) Φ) R.
  Proof. rewrite -type_cast_int //. Qed.

  Global Instance abduct_step_type_cast_int Δ X pm lv it1 it2 n1 n2 Φ (R : iProp Σ):
    AbductStep (abduct Δ (type_recover pm lv (intT it1 n1) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜n2 x = n1⌝)))%I R)
      Δ (type_cast X pm lv (intT it1 n1) (λ x, intT (it2 x) (n2 x)) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_int. Qed.


  Global Instance type_progress_bool_int_guaranteed Δ lv it (b: bool) X (n': X → Z) (it': X → int_type) :
    TypeProgress Δ lv X (boolT it b) (λ x, intT (it' x) (n' x)) GUARANTEED := {}.

  Lemma abduct_type_cast_bool_int Δ X pm lv it1 it2 (b: bool) n Φ (R : iProp Σ):
    abduct Δ (type_recover pm lv (boolT it1 b) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜n x = bool_to_Z b⌝)))%I R →
    abduct Δ (type_cast X pm lv (boolT it1 b) (λ x, intT (it2 x) (n x)) Φ) R.
  Proof. rewrite -type_cast_bool_int //. Qed.

  Global Instance abduct_step_type_cast_bool_int Δ X pm lv it1 it2 (b: bool) n Φ (R : iProp Σ):
    AbductStep (abduct Δ (type_recover pm lv (boolT it1 b) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜n x = bool_to_Z b⌝)))%I R)
      Δ (type_cast X pm lv (boolT it1 b) (λ x, intT (it2 x) (n x)) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_bool_int. Qed.

  Global Instance type_progress_int_bool_guaranteed0 Δ lv it X (b': X → bool) (it': X → int_type) :
    TypeProgress Δ lv X (intT it 0) (λ x, boolT (it' x) (b' x)) GUARANTEED := {}.
  Global Instance type_progress_int_bool_guaranteed1 Δ lv it X (b': X → bool) (it': X → int_type) :
    TypeProgress Δ lv X (intT it 1) (λ x, boolT (it' x) (b' x)) GUARANTEED := {}.

  Lemma abduct_type_cast_int_bool Δ X pm lv it1 it2 (b: Z) (b': X → bool) Φ (R : iProp Σ):
    abduct Δ (type_recover pm lv (intT it1 b) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜Some (b' x) = Z_to_bool b⌝)))%I R →
    abduct Δ (type_cast X pm lv (intT it1 b) (λ x, boolT (it2 x) (b' x)) Φ) R.
  Proof. rewrite -type_cast_int_bool //. Qed.

  Global Instance abduct_step_type_cast_int_bool Δ X pm lv it1 it2 (b: Z) (b': X → bool) Φ (R : iProp Σ):
    AbductStep (abduct Δ (type_recover pm lv (intT it1 b) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜Some (b' x) = Z_to_bool b⌝)))%I R)
      Δ (type_cast X pm lv (intT it1 b) (λ x, boolT (it2 x) (b' x)) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_int_bool. Qed.

  (* void casting *)
  Global Instance type_progress_void_guaranteed Δ lv X :
    TypeProgress Δ lv X (voidT) (λ x, voidT) GUARANTEED := {}.

  Lemma abduct_type_cast_void Δ X pm lv Φ (R : iProp Σ):
    abduct Δ (type_recover pm lv voidT (Φ (λ x, True)))%I R →
    abduct Δ (type_cast X pm lv (voidT) (λ x, voidT) Φ) R.
  Proof. rewrite -type_cast_void //. Qed.

  Global Instance abduct_step_type_cast_void Δ X pm lv Φ (R : iProp Σ):
    AbductStep (abduct Δ (type_recover pm lv voidT (Φ (λ x, True)))%I R)
      Δ (type_cast X pm lv (voidT) (λ x, voidT) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_void. Qed.


  (* boolean casting *)
  Global Instance type_progress_bool_guaranteed Δ lv b it X (b': X → bool) (it': X → int_type) :
    TypeProgress Δ lv X (boolT it b) (λ x, boolT (it' x) (b' x)) GUARANTEED := {}.

  Lemma abduct_type_cast_bool Δ X pm lv it1 it2 (b: bool) (b': X → bool) Φ (R : iProp Σ):
    abduct Δ (type_recover pm lv (boolT it1 b) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜b' x = b⌝)))%I R →
    abduct Δ (type_cast X pm lv (boolT it1 b) (λ x, boolT (it2 x) (b' x)) Φ) R.
  Proof. rewrite -type_cast_bool //. Qed.

  Global Instance abduct_step_type_cast_bool Δ X pm lv it1 it2 (b: bool) (b': X → bool) Φ (R : iProp Σ):
    AbductStep (abduct Δ (type_recover pm lv (boolT it1 b) (Φ (λ x, ⌜it2 x = it1⌝ ∗ ⌜b' x = b⌝)))%I R)
      Δ (type_cast X pm lv (boolT it1 b) (λ x, boolT (it2 x) (b' x)) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_bool. Qed.

  (* zeros *)
  Global Instance type_progress_zeros Δ lv X n A :
    (* TODO: Add a typeclass precondition here? *)
    TypeProgress Δ lv X (zerosT n) A GUARANTEED | 100 := {}.

  Lemma abduct_type_from_zeros_all_cons X Ts Φ n ns T Δ R :
    abduct Δ (type_from_zeros X n T (λ P, type_from_zeros_all X ns Ts (λ Ps, Φ (λ x, P x ∗ Ps x)%I))) R →
    abduct Δ (type_from_zeros_all X (n::ns) (λ x, (T x :: Ts x)) Φ) R.
  Proof. by rewrite -type_from_zeros_all_cons. Qed.
  Global Instance abduct_step_type_from_zeros_all_cons X Ts Φ n ns T Δ R :
    AbductStep _ _ _ _ := abduct_type_from_zeros_all_cons X Ts Φ n ns T Δ R.

  Lemma abduct_type_from_zeros_all_nil X Φ Δ R :
    abduct Δ (Φ (λ x, True)%I) R →
    abduct Δ (type_from_zeros_all X [] (λ x, []) Φ) R.
  Proof. by rewrite -type_from_zeros_all_nil. Qed.
  Global Instance abduct_step_type_from_zeros_all_nil X Φ Δ R :
    AbductStep _ _ _ _ := abduct_type_from_zeros_all_nil X Φ Δ R.

  Lemma abduct_type_cast_from_zeros X pm lv A Φ n Δ R :
    abduct Δ (type_recover pm lv (zerosT n) (type_from_zeros X n A (λ P, abd_evars P Φ))) R →
    abduct Δ (type_cast X pm lv (zerosT n) A Φ) R.
  Proof. by rewrite -type_cast_from_zeros. Qed.
  Global Instance abduct_step_type_cast_from_zeros X pm lv A Φ n Δ R :
    AbductStep _ _ _ _ | 100 := abduct_type_cast_from_zeros X pm lv A Φ n Δ R .

  (* any *)
  Global Instance type_progress_any_tgt Δ lv X A (n: X → Z) :
    TypeProgress Δ lv X A (λ x, anyT (n x)) GUARANTEED | 1 := {}.

  Lemma abduct_type_cast_any_tgt Δ pm lv X A (n: X → Z) Φ R:
    abduct Δ (lv ◁ₓ A -∗ Φ (λ x, ∃ v, lv ◁ₓ valueT v (n x)))%I R →
    abduct Δ (type_cast X pm lv A (λ x, anyT (n x)) Φ) R.
  Proof.
    rewrite -type_cast_any_tgt //.
  Qed.

  Global Instance abduct_step_type_cast_any_tgt Δ pm lv X A (n: X → Z) Φ R:
    AbductStep (abduct Δ (lv ◁ₓ A -∗ Φ (λ x, ∃ v, lv ◁ₓ valueT v (n x))) R)%I
      Δ (type_cast X pm lv A (λ x, anyT (n x)) Φ) R | 10.
  Proof. intros ?. by eapply abduct_type_cast_any_tgt. Qed.


  (* values *)
  Global Instance type_progress_value_val_guaranteed Δ X (v: val) (w: X → val) A (m: X → Z) :
    TypeProgress Δ (VAL v) X A (λ x, valueT (w x) (m x)) GUARANTEED := {}.

  Global Instance type_progress_own_value_guaranteed Δ (lv: loc_or_val) X (p: loc) (A: type Σ) (q: X → loc) :
    TypeProgress Δ lv X (ownT p A) (λ x, valueT (q x) ptr_size) GUARANTEED := {}.

  Global Instance type_progress_value_loc_guaranteed Δ X (l: loc) (w: X → val) A m :
    projection X val w →
    TypeProgress Δ (LOC l) X A (λ x, valueT (w x) m) GUARANTEED := {}.

  (* the issue is the size of the value has not yet been determined *)
  Global Instance type_progress_value_loc_maybe Δ X (l: loc) A (w: X → val) n (m: X → Z) :
    sequence_classes [Δ ⊨ A `has_bounds` n] →
    TypeProgress Δ (LOC l) X A (λ x, valueT (w x) (m x)) (MAYBE 100) | 100 := {}.

  Lemma abduct_type_cast_value_val Δ X pm (v: val) A (w: X → val) (n: X → Z) Φ (R : iProp Σ):
    abduct Δ (type_size A (λ m, abd_assume CTX (v ◁ᵥ A) (Φ (λ x, ⌜w x = v⌝ ∗ ⌜n x = m⌝))))%I R →
    abduct Δ (type_cast X pm (VAL v) A (λ x, valueT (w x) (n x)) Φ) R.
  Proof. rewrite -type_cast_value_val //. Qed.

  Global Instance abduct_step_type_cast_value_val Δ X pm (v: val) A (w: X → val) (n: X → Z) Φ (R : iProp Σ):
    AbductStep (abduct Δ (type_size A (λ m, abd_assume CTX (v ◁ᵥ A) (Φ (λ x, ⌜w x = v⌝ ∗ ⌜n x = m⌝))))%I R)%I
      Δ (type_cast X pm (VAL v) A (λ x, valueT (w x) (n x)) Φ) R | 2.
  Proof. intros ?; by eapply abduct_type_cast_value_val. Qed.

  Lemma abduct_type_cast_own_value Δ (X: Type) pm lv (p: loc) (A: type Σ) (q: X → loc) Φ (R : iProp Σ) :
    abduct Δ (abd_assume CTX (p ◁ₗ A) (type_recover pm lv (valueT p ptr_size) (Φ (λ x, ⌜p = q x⌝))))%I R →
    abduct Δ (type_cast X pm lv (ownT p A) (λ x, valueT (q x) ptr_size) Φ) R.
  Proof. rewrite -type_cast_own_value //. Qed.

  Global Instance abduct_step_type_cast_own_value Δ (X: Type) pm lv (p: loc) (A: type Σ) (q: X → loc) Φ (R : iProp Σ) :
    AbductStep (abduct Δ (abd_assume CTX (p ◁ₗ A) (type_recover pm lv (valueT p ptr_size) (Φ (λ x, ⌜p = q x⌝))))%I R)
      Δ (type_cast X pm lv (ownT p A) (λ x, valueT (q x) ptr_size) Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_cast_own_value. Qed.

  (* [type_cast] for [valueT] with a location and a fixed size *)
  Lemma abduct_type_cast_value_loc Δ X pm l A (v: X → val) n Φ R:
    projection X val v →
    abduct Δ (type_extract_prefix l A n (λ r w, Φ (λ x, ⌜v x = w⌝)%I)) R →
    abduct Δ (type_cast X pm (LOC l) A (λ x, valueT (v x) n) Φ) R.
  Proof. rewrite -type_cast_value_loc //. Qed.

  Global Instance abduct_step_type_cast_value_loc Δ pm X l A (v: X → val) n Φ R:
    projection X val v →
    AbductStep (abduct Δ (type_extract_prefix l A n (λ r w, Φ (λ x, ⌜v x = w⌝)%I)) R)
      Δ (type_cast X pm (LOC l) A (λ x, valueT (v x) n) Φ) R | 10.
  Proof. intros ??. by eapply abduct_type_cast_value_loc. Qed.

  Lemma abduct_type_cast_value_loc_maybe Δ X pm (l: loc) A (w: X → val) n m Φ (R : iProp Σ):
    sequence_classes [Δ ⊨ (A `has_size` n)] →
    abduct Δ (∀ v, abd_assume CTX (v ◁ᵥ A) (Φ (λ x, ⌜w x = v⌝ ∗ ⌜m x = n⌝)))%I R →
    abduct Δ (type_cast X pm (LOC l) A (λ x, valueT (w x) (m x)) Φ) R.
  Proof.
    rewrite /sequence_classes !Forall_cons Forall_nil /=. intros (? & _).
    intros Habd. apply: abduct_qenvs_entails. intros Hsz. rewrite -type_cast_value_loc_maybe //.
  Qed.

  Global Instance abduct_step_type_cast_value_loc_maybe Δ X pm (l: loc) A (w: X → val) n m Φ (R : iProp Σ):
    sequence_classes [Δ ⊨ (A `has_size` n)] →
    AbductStep (abduct Δ (∀ v, abd_assume CTX (v ◁ᵥ A) (Φ (λ x, ⌜w x = v⌝ ∗ ⌜m x = n⌝)))%I R)
      Δ (type_cast X pm (LOC l) A (λ x, valueT (w x) (m x)) Φ) R | 15.
  Proof. intros ??. by eapply abduct_type_cast_value_loc_maybe. Qed.

  Lemma abduct_type_cast_value_loc_maybe_bounds Δ X pm (l: loc) A (w: X → val) n m Φ (R : iProp Σ):
    sequence_classes [Δ ⊨ (A `has_bounds` n)] →
    abduct Δ (type_move l A n (λ v, Φ (λ x, ⌜m x = n⌝ ∗ ⌜w x = v⌝)))%I R →
    abduct Δ (type_cast X pm (LOC l) A (λ x, valueT (w x) (m x)) Φ) R.
  Proof.
    rewrite /sequence_classes !Forall_cons Forall_nil /=. intros (? & _).
    intros Habd. apply: abduct_qenvs_entails. intros Hsz. rewrite -type_cast_value_loc_maybe_bounds //.
  Qed.

  Global Instance abduct_step_type_cast_value_loc_maybe_bounds Δ X pm (l: loc) A (w: X → val) n m Φ (R : iProp Σ):
    sequence_classes [Δ ⊨ (A `has_bounds` n)] →
    AbductStep (abduct Δ (type_move l A n (λ v, Φ (λ x, ⌜m x = n⌝ ∗ ⌜w x = v⌝)))%I R)
      Δ (type_cast X pm (LOC l) A (λ x, valueT (w x) (m x)) Φ) R | 20.
  Proof. intros ??. by eapply abduct_type_cast_value_loc_maybe_bounds. Qed.


  (** NOTE: the behavior of this type casting rule is incomplete:
      if [lv] is a location, the value stored there could be
      smaller or larger than the size of the type [B]. In those
      cases, it would be better to fill [lv] first and then proceed
      with type casting.
  *)
  Global Instance type_progress_value_src Δ lv X v n B:
    TypeProgress Δ lv X (valueT v n) B GUARANTEED := {}.

  Lemma abduct_type_cast_value_src Δ X pm lv v n B Φ R:
    abduct Δ (type_recover pm lv (valueT v n) (Φ (λ x, v ◁ᵥ B x))) R →
    abduct Δ (type_cast X pm lv (valueT v n) B Φ) R.
  Proof.
    rewrite -type_cast_value_src //.
  Qed.

  Global Instance abduct_step_type_cast_value_src Δ X pm lv v n B Φ R:
    AbductStep (abduct Δ (type_recover pm lv (valueT v n) (Φ (λ x, v ◁ᵥ B x))) R)
      Δ (type_cast X pm lv (valueT v n) B Φ) R | 2.
  Proof. intros ?. by eapply abduct_type_cast_value_src. Qed.

  Lemma abduct_type_cast_loc_val_no_own Δ X lv pm v n1 n2 Φ R:
    Δ ⊨ Solve (n1 = n2) →
    abduct Δ (Φ (λ x, emp)%I) R →
    abduct Δ (type_cast X pm lv (valueT v n1) (λ x: X, valueT v n2) Φ) R.
  Proof.
    intros ??. rewrite -type_cast_loc_val_no_own. eapply abduct_prove_pure; done.
  Qed.

  Global Instance abduct_step_type_cast_loc_val_no_own Δ X lv pm v n1 n2 Φ R:
    Δ ⊨ Solve (n1 = n2) →
    AbductStep (abduct Δ (Φ (λ x, emp)%I) R)
      Δ (type_cast X pm lv (valueT v n1) (λ x: X, valueT v n2) Φ) R | 1.
  Proof. intros ??. by eapply abduct_type_cast_loc_val_no_own. Qed.


  Lemma abduct_type_cast_value_any X pm lv v n m Φ Δ R:
    abduct Δ (type_recover pm lv (valueT v n) (Φ (λ x, ⌜n = m x⌝)%I)) R →
    abduct Δ (type_cast X pm lv (valueT v n) (λ x, anyT (m x)) Φ) R.
  Proof. by rewrite -type_cast_value_any. Qed.
  Global Instance abduct_step_type_cast_value_any X pm lv v n m Φ Δ R:
    AbductStep _ _ _ _ | 1 := abduct_type_cast_value_any X pm lv v n m Φ Δ R.



  (* places *)
  Global Instance type_progress_place_src_guaranteed Δ X l r A :
    TypeProgress Δ (LOC l) X (placeT r) A GUARANTEED := {}.

  Global Instance type_progress_place_tgt_guaranteed Δ X l r A :
    TypeProgress Δ (LOC l) X A (λ x, placeT (r x)) GUARANTEED := {}.

  Lemma abduct_type_cast_place_src Δ X pm l r A Φ R:
    abduct Δ (abd_assume CTX (l ◁ₗ placeT r) (Φ (λ x, r ◁ₗ A x))) R →
    abduct Δ (type_cast X pm (LOC l) (placeT r) A Φ) R.
  Proof.
    rewrite -type_cast_place_src //.
  Qed.

  Global Instance abduct_step_type_cast_place_src Δ X pm l r A Φ R:
    AbductStep (abduct Δ (abd_assume CTX (l ◁ₗ placeT r) (Φ (λ x, r ◁ₗ A x))) R)
      Δ (type_cast X pm (LOC l) (placeT r) A Φ) R | 2.
  Proof. intros ?. by eapply abduct_type_cast_place_src. Qed.

  Lemma abduct_type_cast_place_tgt Δ X pm l r A Φ R:
    abduct Δ (abd_assume CTX (l ◁ₗ A) (Φ (λ x, ⌜r x = l⌝)%I)) R →
    abduct Δ (type_cast X pm (LOC l) A (λ x, placeT (r x)) Φ) R.
  Proof. rewrite -type_cast_place_tgt //. Qed.

  Global Instance abduct_step_type_cast_place_tgt Δ X pm l r A Φ R:
    AbductStep (abduct Δ (abd_assume CTX (l ◁ₗ A) (Φ (λ x, ⌜r x = l⌝)%I)) R)
      Δ (type_cast X pm (LOC l) A (λ x, placeT (r x)) Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_cast_place_tgt. Qed.

  (* shallow optionals *)
  Global Instance type_progress_opt_own_guaranteed Δ lv v X p A :
    TypeProgress Δ lv X (optT v) (λ x, ownT (p x) (A x)) GUARANTEED := {}.

  Global Instance type_progress_opt_null_guaranteed Δ lv v X  :
    TypeProgress Δ lv X (optT v) (λ x, nullT) GUARANTEED := {}.

  Global Instance type_progress_opt_optional_guranteed Δ lv v X φ A :
    TypeProgress Δ lv X (optT v) (λ x, optionalT (φ x) (A x)) GUARANTEED := {}.

  Global Instance type_progress_opt_opt_guaranteed Δ lv v X w :
    TypeProgress Δ lv X (optT v) (λ x, optT (w x)) GUARANTEED := {}.

  Global Instance type_progress_loc_opt_guaranteed Δ l A X v:
    TypeProgress Δ (LOC l) X A (λ x, optT (v x)) GUARANTEED := {}.

  Global Instance type_progress_own_opt_guaranteed Δ X v w p A :
    TypeProgress Δ (VAL v) X (ownT p A) (λ x, optT (w x)) GUARANTEED := {}.

  Global Instance type_progress_null_opt_guaranteed Δ X v w :
    TypeProgress Δ (VAL v) X nullT (λ x, optT (w x)) GUARANTEED := {}.

  Global Instance type_progress_optional_opt_guaranteed Δ X v w φ A :
    TypeProgress Δ (VAL v) X (optionalT φ A) (λ x, optT (w x)) GUARANTEED := {}.


  Lemma abduct_type_cast_opt_own Δ X pm lv p v A Φ R:
    abduct Δ (type_recover pm lv (optT v) (Φ (λ x, v ◁ᵥ ownT (p x) (A x)))) R →
    abduct Δ (type_cast X pm lv (optT v) (λ x, ownT (p x) (A x)) Φ) R.
  Proof. rewrite -type_cast_opt_lookup //. Qed.

  Global Instance abduct_step_type_cast_opt_own Δ X pm lv p v A Φ R:
    AbductStep (abduct Δ (type_recover pm lv (optT v) (Φ (λ x, v ◁ᵥ ownT (p x) (A x)))) R)
      Δ (type_cast X pm lv (optT v) (λ x, ownT (p x) (A x)) Φ) R.
  Proof. intros ?. by eapply abduct_type_cast_opt_own. Qed.

  Lemma abduct_type_cast_opt_null Δ X pm lv v Φ R:
    abduct Δ (type_recover pm lv (optT v) (Φ (λ x, v ◁ᵥ nullT))) R →
    abduct Δ (type_cast X pm lv (optT v) (λ x, nullT) Φ) R.
  Proof. rewrite -type_cast_opt_lookup //. Qed.

  Global Instance abduct_step_type_cast_opt_null Δ X pm lv v Φ R:
    AbductStep (abduct Δ (type_recover pm lv (optT v) (Φ (λ x, v ◁ᵥ nullT))) R)
      Δ (type_cast X pm lv (optT v) (λ x, nullT) Φ) R.
  Proof. intros ?. by eapply abduct_type_cast_opt_null. Qed.

  Lemma abduct_type_cast_opt_optional Δ X pm lv v φ A Φ R:
    abduct Δ (type_recover pm lv (optT v) (Φ (λ x, v ◁ᵥ optionalT (φ x) (A x)))) R →
    abduct Δ (type_cast X pm lv (optT v) (λ x, optionalT (φ x) (A x)) Φ) R.
  Proof. rewrite -type_cast_opt_lookup //. Qed.

  Global Instance abduct_step_type_cast_opt_optional Δ X pm lv v φ A Φ R:
    AbductStep (abduct Δ (type_recover pm lv (optT v) (Φ (λ x, v ◁ᵥ optionalT (φ x) (A x)))) R)
      Δ (type_cast X pm lv (optT v) (λ x, optionalT (φ x) (A x)) Φ) R.
  Proof. intros ?. by eapply abduct_type_cast_opt_optional. Qed.

  Lemma abduct_type_cast_opt_opt Δ X pm lv v w Φ R:
    abduct Δ (type_recover pm lv (optT v) (Φ (λ x, ⌜w x = v⌝))%I) R →
    abduct Δ (type_cast X pm lv (optT v) (λ x, optT (w x)) Φ) R.
  Proof. rewrite -type_cast_opt_opt //. Qed.

  Global Instance abduct_step_type_cast_opt_opt Δ X pm lv v w Φ R:
    AbductStep (abduct Δ (type_recover pm lv (optT v) (Φ (λ x, ⌜w x = v⌝))%I) R)
      Δ (type_cast X pm lv (optT v) (λ x, optT (w x)) Φ) R.
  Proof. intros ?. by eapply abduct_type_cast_opt_opt. Qed.

  Lemma abduct_type_cast_loc_opt Δ X pm l v A Φ R:
    abduct Δ (type_move l A ptr_size (λ w, Φ (λ x, w ◁ᵥ optT (v x)))) R →
    abduct Δ (type_cast X pm (LOC l) A (λ x, optT (v x)) Φ) R.
  Proof. rewrite -type_cast_loc_opt //. Qed.

  Global Instance abduct_step_type_cast_loc_opt Δ X pm l v A Φ R:
    AbductStep (abduct Δ (type_move l A ptr_size (λ w, Φ (λ x, w ◁ᵥ optT (v x)))) R)
      Δ (type_cast X pm (LOC l) A (λ x, optT (v x)) Φ) R.
  Proof. intros ?. by eapply abduct_type_cast_loc_opt. Qed.

  Lemma abduct_type_cast_null_opt Δ X pm v w Φ R:
    abduct Δ (abd_assume CTX (v ◁ᵥ nullT) (Φ (λ x, ⌜w x = v⌝))%I) R →
    abduct Δ (type_cast X pm (VAL v) nullT (λ x, optT (w x)) Φ) R.
  Proof. rewrite -type_cast_null_opt //. Qed.

  Global Instance abduct_step_type_cast_null_opt Δ X pm v w Φ R:
    AbductStep (abduct Δ (abd_assume CTX (v ◁ᵥ nullT) (Φ (λ x, ⌜w x = v⌝))%I) R)
      Δ (type_cast X pm (VAL v) nullT (λ x, optT (w x)) Φ) R.
  Proof. intros ?. by eapply abduct_type_cast_null_opt. Qed.

  Lemma abduct_type_cast_own_opt Δ X pm v w p A Φ R:
    abduct Δ (⌜NonNull A⌝ ∗ abd_assume CTX (v ◁ᵥ ownT p A) (Φ (λ x, ⌜w x = v⌝))%I) R →
    abduct Δ (type_cast X pm (VAL v) (ownT p A) (λ x, optT (w x)) Φ) R.
  Proof. rewrite -type_cast_own_opt //. Qed.

  Global Instance abduct_step_type_cast_own_opt Δ X pm v w p A Φ R:
    AbductStep (abduct Δ (⌜NonNull A⌝ ∗ abd_assume CTX (v ◁ᵥ ownT p A) (Φ (λ x, ⌜w x = v⌝))%I) R)
      Δ (type_cast X pm (VAL v) (ownT p A) (λ x, optT (w x)) Φ) R.
  Proof. intros ?. by eapply abduct_type_cast_own_opt. Qed.

  Lemma abduct_type_cast_optional_opt Δ X pm v w φ A Φ R:
    abduct Δ (⌜NonNullVal A⌝ ∗ abd_assume CTX (v ◁ᵥ optionalT φ A) (Φ (λ x, ⌜w x = v⌝))%I) R →
    abduct Δ (type_cast X pm (VAL v) (optionalT φ A) (λ x, optT (w x)) Φ) R.
  Proof. rewrite -type_cast_optional_opt //. Qed.

  Global Instance abduct_step_type_cast_optional_opt Δ X pm v w φ A Φ R:
    AbductStep (abduct Δ (⌜NonNullVal A⌝ ∗ abd_assume CTX (v ◁ᵥ optionalT φ A) (Φ (λ x, ⌜w x = v⌝))%I) R)
      Δ (type_cast X pm (VAL v) (optionalT φ A) (λ x, optT (w x)) Φ) R.
  Proof. intros ?. by eapply abduct_type_cast_optional_opt. Qed.



  (* pointers *)
  Global Instance type_progress_own_guaranteed Δ (lv: loc_or_val) (p: loc) X (q: X → loc) (A : type Σ) (B: X → type Σ) :
    TypeProgress Δ lv X (ownT p A) (λ x, ownT (q x) (B x)) GUARANTEED := {}.

  Lemma abduct_type_cast_own Δ (X: Type) pm lv (p: loc) (q: X → loc) (A : type Σ) (B: X → type Σ) Φ (R : iProp Σ) :
    abduct Δ (abd_assume CTX (p ◁ₗ A) (type_recover pm lv (valueT p ptr_size) (Φ (λ x, p ◁ₗ B x ∗ ⌜p = q x⌝)%I))) R →
    abduct Δ (type_cast X pm lv (ownT p A) (λ x, ownT (q x) (B x)) Φ) R.
  Proof. rewrite -type_cast_own //. Qed.

  Global Instance abduct_step_type_cast_own Δ (X: Type) pm lv (p: loc) (q: X → loc) (A : type Σ) (B: X → type Σ) Φ (R : iProp Σ) :
    AbductStep (abduct Δ (abd_assume CTX (p ◁ₗ A) (type_recover pm lv (valueT p ptr_size) (Φ (λ x, p ◁ₗ B x ∗ ⌜p = q x⌝)%I))) R)
      Δ (type_cast X pm lv (ownT p A) (λ x, ownT (q x) (B x)) Φ) R.
  Proof. intros ?. by eapply abduct_type_cast_own. Qed.

  Global Instance type_progress_own_optional_guaranteed Δ (lv: loc_or_val) (p: loc) (A: type Σ) X (φ: X → dProp) (B: X → type Σ) :
    TypeProgress Δ lv X (ownT p A) (λ x, optionalT (φ x) (B x)) GUARANTEED := {}.

  Lemma abduct_type_cast_own_optional Δ (X: Type) pm lv (p: loc) (A: type Σ) (φ: X → dProp) (B: X → type Σ) Φ (R : iProp Σ) :
    abduct Δ (abd_assume CTX (lv ◁ₓ ownT p A) (Φ (λ x, ⌜φ x⌝ ∗ lv ◁ₓ B x)))%I R →
    abduct Δ (type_cast X pm lv (ownT p A) (λ x, optionalT (φ x) (B x)) Φ) R.
  Proof. rewrite -type_cast_own_optional //. Qed.

  Global Instance abduct_step_type_cast_own_optional Δ (X: Type) pm lv (p: loc) (A: type Σ) (φ: X → dProp) (B: X → type Σ) Φ (R : iProp Σ) :
    AbductStep (abduct Δ (abd_assume CTX (lv ◁ₓ ownT p A) (Φ (λ x, ⌜φ x⌝ ∗ lv ◁ₓ B x)))%I R)
      Δ (type_cast X pm lv (ownT p A) (λ x, optionalT (φ x) (B x)) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_own_optional. Qed.

  Global Instance type_progress_optional_own_guaranteed Δ (lv: loc_or_val) (φ: dProp) (B: type Σ) X (p: X → loc) (A: X → type Σ) :
    TypeProgress Δ lv X (optionalT φ B) (λ x, ownT (p x) (A x)) GUARANTEED := {}.

  Lemma abduct_type_cast_optional_own Δ (X: Type) pm lv (φ: dProp) (B: type Σ) (p: X → loc) (A: X → type Σ) Φ (R : iProp Σ) :
    abduct Δ (⌜φ⌝ ∗ (abd_assume CTX (lv ◁ₓ B) (Φ (λ x, lv ◁ₓ ownT (p x) (A x)))))%I R →
    abduct Δ (type_cast X pm lv (optionalT φ B) (λ x, ownT (p x) (A x)) Φ) R.
  Proof. rewrite -type_cast_optional_own //. Qed.

  Global Instance abduct_step_type_cast_optional_own Δ (X: Type) pm lv (φ: dProp) (B: type Σ) (p: X → loc) (A: X → type Σ) Φ (R : iProp Σ) :
    AbductStep (abduct Δ (⌜φ⌝ ∗ (abd_assume CTX (lv ◁ₓ B) (Φ (λ x, lv ◁ₓ ownT (p x) (A x)))))%I R)
      Δ (type_cast X pm lv (optionalT φ B) (λ x, ownT (p x) (A x)) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_optional_own. Qed.


  Global Instance type_progress_null_guaranteed Δ (lv: loc_or_val) X :
    TypeProgress Δ lv X nullT (λ x, nullT) GUARANTEED := {}.

  Lemma abduct_type_cast_null Δ (X: Type) pm lv Φ (R : iProp Σ) :
    abduct Δ (type_recover pm lv nullT (Φ (λ _, emp)))%I R →
    abduct Δ (type_cast X pm lv nullT (λ x, nullT) Φ) R.
  Proof. rewrite -type_cast_null //. Qed.

  Global Instance abduct_step_type_cast_null Δ (X: Type) pm lv Φ (R : iProp Σ) :
    AbductStep (abduct Δ (type_recover pm lv nullT (Φ (λ _, emp)))%I R)
      Δ (type_cast X pm lv nullT (λ x, nullT) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_null. Qed.

  Global Instance type_progress_null_optional_guaranteed Δ (lv: loc_or_val) X (φ: X → dProp) (B: X → type Σ) :
    TypeProgress Δ lv X nullT (λ x, optionalT (φ x) (B x)) GUARANTEED := {}.

  Lemma abduct_type_cast_null_optional Δ (X: Type) pm lv (φ: X → dProp) (B: X → type Σ) Φ (R : iProp Σ) :
    abduct Δ (type_recover pm lv nullT (Φ (λ x, ⌜¬ φ x⌝)))%I R →
    abduct Δ (type_cast X pm lv nullT (λ x, optionalT (φ x) (B x)) Φ) R.
  Proof. rewrite -type_cast_null_optional //. Qed.

  Global Instance abduct_step_type_cast_null_optional Δ (X: Type) pm lv (φ: X → dProp) (B: X → type Σ) Φ (R : iProp Σ) :
    AbductStep (abduct Δ (type_recover pm lv nullT (Φ (λ x, ⌜¬ φ x⌝)))%I R)
      Δ (type_cast X pm lv nullT (λ x, optionalT (φ x) (B x)) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_null_optional. Qed.

  Global Instance type_progress_optional_null_guaranteed Δ (lv: loc_or_val) X (φ: dProp) (B: type Σ) :
    TypeProgress Δ lv X (optionalT φ B) (λ x, nullT) GUARANTEED := {}.

  Lemma abduct_type_cast_optional_null Δ (X: Type) pm lv (φ: dProp) (B: type Σ) Φ (R : iProp Σ) :
    abduct Δ (⌜¬ φ⌝ ∗ type_recover pm lv nullT (Φ (λ _, emp)))%I R →
    abduct Δ (type_cast X pm lv (optionalT φ B) (λ x, nullT) Φ) R.
  Proof. rewrite -type_cast_optional_null //. Qed.

  Global Instance abduct_step_type_cast_optional_null Δ (X: Type) pm lv (φ: dProp) (B: type Σ) Φ (R : iProp Σ) :
  AbductStep (abduct Δ (⌜¬ φ⌝ ∗ type_recover pm lv nullT (Φ (λ _, emp)))%I R)
    Δ (type_cast X pm lv (optionalT φ B) (λ x, nullT) Φ) R.
Proof. intros ?; by eapply abduct_type_cast_optional_null. Qed.

  Global Instance type_progress_own_null_impossible Δ (lv: loc_or_val) (p: loc) X (q: X → loc) (A : type Σ) (B: X → type Σ) :
    TypeProgress Δ lv X (ownT p A) (λ x, nullT) IMPOSSIBLE := {}.

  Global Instance type_progress_null_own_impossible Δ (lv: loc_or_val) X (p: X → loc) (A : X → type Σ) (B: type Σ) :
    TypeProgress Δ lv X nullT (λ x, ownT (p x) (A x)) IMPOSSIBLE := {}.


  Global Instance type_progress_optional_guaranteed Δ (lv: loc_or_val) (φ: dProp) (A: type Σ) X (ψ: X → dProp) (B: X → type Σ) :
    one_of_classes [exi A; (projection X (type Σ) B); (∀ x, exi (B x)); (∀ x, unify A (B x))] →
    TypeProgress Δ lv X (optionalT φ A) (λ x, optionalT (ψ x) (B x)) GUARANTEED := {}.

  Lemma abduct_type_cast_optional Δ X pm lv φ A (ψ: X → dProp) (B: X → type Σ) Φ R :
    one_of_classes [exi A; (projection X (type Σ) B); (∀ x, exi (B x)); (∀ x, unify A (B x))] →
    abduct Δ (type_recover pm lv (movedT (optionalT φ A)) (Φ (λ x, ⌜dprop_iff φ (ψ x)⌝ ∗ ⌜A `is_ty` (B x)⌝)))%I R →
    abduct Δ (type_cast X pm lv (optionalT φ A) (λ x, optionalT (ψ x) (B x)) Φ) R.
  Proof.
    rewrite -type_cast_optional //.
  Qed.

  Global Instance abduct_step_type_cast_optional Δ X pm lv φ A (ψ: X → dProp) (B: X → type Σ) Φ R :
    one_of_classes [exi A; (projection X (type Σ) B); (∀ x, exi (B x)); (∀ x, unify A (B x))] →
    AbductStep (abduct Δ (type_recover pm lv (movedT (optionalT φ A)) (Φ (λ x, ⌜dprop_iff φ (ψ x)⌝ ∗ ⌜A `is_ty` (B x)⌝)))%I R)
      Δ (type_cast X pm lv (optionalT φ A) (λ x, optionalT (ψ x) (B x)) Φ) R.
  Proof. intros ??; by eapply abduct_type_cast_optional. Qed.


  Global Instance type_progress_optional_optional Δ (lv: loc_or_val) X (A: type Σ) φ ψ (B: X → type Σ) :
    TypeProgress Δ lv X (optionalT ψ A) (λ x, optionalT (φ x) (B x)) GUARANTEED := {}.

  Lemma abduct_type_cast_optional_case Δ (lv: loc_or_val) pm X (A: type Σ) φ ψ (B: X → type Σ) Φ R:
    abduct Δ (case φ (type_cast X pm lv A (λ x, optionalT (ψ x) (B x)) Φ) (type_cast X pm lv nullT (λ x, optionalT (ψ x) (B x)) Φ)) R →
    abduct Δ (type_cast X pm lv (optionalT φ A) (λ x, optionalT (ψ x) (B x)) Φ) R.
  Proof. rewrite -type_cast_optional_case //. Qed.

  Global Instance abduct_step_type_cast_optional_case Δ (lv: loc_or_val) pm X (A: type Σ) φ ψ (B: X → type Σ) Φ R:
    AbductStep (abduct Δ (case φ (type_cast X pm lv A (λ x, optionalT (ψ x) (B x)) Φ) (type_cast X pm lv nullT (λ x, optionalT (ψ x) (B x)) Φ)) R)
      Δ (type_cast X pm lv (optionalT φ A) (λ x, optionalT (ψ x) (B x)) Φ) R | 100.
  Proof. intros ?; by eapply abduct_type_cast_optional_case. Qed.

  (* constraints, separating conjunction, and existential quantification *)
  Lemma abduct_type_cast_constraint_tgt Δ (X: Type) pm lv (A : type Σ) (φ: X → Prop) (B: X → type Σ) Φ (R : iProp Σ) :
    abduct Δ (abd_assume CTX (lv ◁ₓ A ) (Φ (λ x, ⌜φ x⌝ ∗ lv ◁ₓ B x)))%I R →
    abduct Δ (type_cast X pm lv A (λ x, constraintT (B x) (φ x)) Φ) R.
  Proof. rewrite -type_cast_constraint_tgt //. Qed.

  Global Instance type_progress_constraint_tgt_guaranteed Δ (lv: loc_or_val) X (A: type Σ) (φ: X → Prop) (B: X → type Σ) :
    TypeProgress Δ lv X A (λ x, constraintT (B x) (φ x)) GUARANTEED := {}.

  Lemma abduct_type_cast_constraint_src Δ (X: Type) lv pm (A : type Σ) (φ: Prop) (B: X → type Σ) Φ (R : iProp Σ) :
    abduct Δ (abd_assume CTX (lv ◁ₓ A) (abd_assume CTX ⌜φ⌝ (Φ (λ x, lv ◁ₓ B x))))%I R →
    abduct Δ (type_cast X pm lv (constraintT A φ) B Φ) R.
  Proof. rewrite -type_cast_constraint_src //. Qed.

  Global Instance type_progress_constraint_src_guaranteed Δ (lv: loc_or_val) X (A: type Σ) (φ: Prop) (B: X → type Σ) :
    TypeProgress Δ lv X (constraintT A φ) B GUARANTEED := {}.

  Lemma abduct_type_cast_sep_tgt Δ (X: Type) lv pm (A : type Σ) (P: X → iProp Σ) (B: X → type Σ) Φ (R : iProp Σ) :
    abduct Δ (abd_assume CTX (lv ◁ₓ A) (Φ (λ x, P x ∗ lv ◁ₓ B x)))%I R →
    abduct Δ (type_cast X pm lv A (λ x, sepT (B x) (P x)) Φ) R.
  Proof. rewrite -type_cast_sep_tgt //. Qed.

  Global Instance type_progress_sep_tgt_guaranteed Δ (lv: loc_or_val) X (A: type Σ) (P: X → iProp Σ) (B: X → type Σ) :
    TypeProgress Δ lv X A (λ x, sepT (B x) (P x)) GUARANTEED := {}.

  Lemma abduct_type_cast_sep_src Δ (X: Type) lv pm (A : type Σ) (P: iProp Σ) (B: X → type Σ) Φ (R : iProp Σ) :
    abduct Δ (abd_assume CTX (lv ◁ₓ A) (abd_assume CTX P (Φ (λ x, lv ◁ₓ B x))))%I R →
    abduct Δ (type_cast X pm lv (sepT A P) B Φ) R.
  Proof. rewrite -type_cast_sep_src //. Qed.

  Global Instance type_progress_sep_src_guaranteed Δ (lv: loc_or_val) X (A: type Σ) (P: iProp Σ) (B: X → type Σ) :
    TypeProgress Δ lv X (sepT A P) B GUARANTEED := {}.

  Lemma abduct_type_cast_exists_tgt Δ (X: Type) lv pm (Y: X → Type) (A : type Σ) (B: ∀ x: X, Y x → type Σ) Φ (R : iProp Σ) :
    abduct Δ (abd_assume CTX (lv ◁ₓ A) (Φ (λ x, ∃ y, lv ◁ₓ B x y)))%I R →
    abduct Δ (type_cast X pm lv A (λ x, existsT (B x)) Φ) R.
  Proof. rewrite -type_cast_exists_tgt //. Qed.

  Global Instance type_progress_exists_tgt_guaranteed Δ (lv: loc_or_val) X (Y: X → Type) (A: type Σ) (B: ∀ x: X, Y x → type Σ) :
    TypeProgress Δ lv X A (λ x, existsT (B x)) GUARANTEED := {}.

  Lemma abduct_type_cast_exists_src Δ (X: Type) lv pm Y (A : Y → type Σ) (B: X→ type Σ) Φ (R : iProp Σ) :
    (abduct Δ (∀ y, abd_assume CTX (lv ◁ₓ A y) (Φ (λ x, lv ◁ₓ B x)))%I R) →
    abduct Δ (type_cast X pm lv (existsT A) B Φ) R.
  Proof. rewrite -type_cast_exists_src //. Qed.

  Global Instance type_progress_exists_src_guaranteed Δ (lv: loc_or_val) X Y (A: Y → type Σ) (B: X → type Σ) :
    TypeProgress Δ lv X (existsT A) B GUARANTEED := {}.

  (* constraints, separating conjunction, and existential quantification *)
  Global Instance abduct_step_type_cast_constraint_tgt Δ (X: Type) lv pm (A : type Σ) (φ: X → Prop) (B: X → type Σ) Φ (R : iProp Σ) :
    AbductStep (abduct Δ (abd_assume CTX (lv ◁ₓ A) (Φ (λ x, ⌜φ x⌝ ∗ lv ◁ₓ B x)))%I R)
      Δ (type_cast X pm lv A (λ x, constraintT (B x) (φ x)) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_constraint_tgt. Qed.

  Global Instance abduct_step_type_cast_constraint_src Δ (X: Type) lv pm (A : type Σ) (φ: Prop) (B: X → type Σ) Φ (R : iProp Σ) :
    AbductStep (abduct Δ (abd_assume CTX (lv ◁ₓ A) (abd_assume CTX ⌜φ⌝ (Φ (λ x, lv ◁ₓ B x))))%I R)
      Δ (type_cast X pm lv (constraintT A φ) B Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_constraint_src. Qed.

  Global Instance abduct_step_type_cast_sep_tgt Δ (X: Type) lv pm (A : type Σ) (P: X → iProp Σ) (B: X → type Σ) Φ (R : iProp Σ) :
    AbductStep (abduct Δ (abd_assume CTX (lv ◁ₓ A) (Φ (λ x, P x ∗ lv ◁ₓ B x)))%I R)
      Δ (type_cast X pm lv A (λ x, sepT (B x) (P x)) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_sep_tgt. Qed.

  Global Instance abduct_step_type_cast_sep_src Δ (X: Type) lv pm (A : type Σ) (P: iProp Σ) (B: X → type Σ) Φ (R : iProp Σ) :
    AbductStep (abduct Δ (abd_assume CTX (lv ◁ₓ A) (abd_assume CTX P (Φ (λ x, lv ◁ₓ B x))))%I R)
      Δ (type_cast X pm lv (sepT A P) B Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_sep_src. Qed.

  Global Instance abduct_step_type_cast_exists_tgt Δ (X: Type) lv pm (Y: X → Type) (A : type Σ) (B: ∀ x: X, Y x → type Σ) Φ (R : iProp Σ) :
    AbductStep (abduct Δ (abd_assume CTX (lv ◁ₓ A) (Φ (λ x, ∃ y, lv ◁ₓ B x y)))%I R)
      Δ (type_cast X pm lv A (λ x, existsT (B x)) Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_exists_tgt. Qed.

  Global Instance abduct_step_type_cast_exists_src Δ (X: Type) lv pm Y (A : Y → type Σ) (B: X→ type Σ) Φ (R : iProp Σ) :
    AbductStep (abduct Δ (∀ y, abd_assume CTX (lv ◁ₓ A y) (Φ (λ x, lv ◁ₓ B x)))%I R)
      Δ (type_cast X pm lv (existsT A) B Φ) R.
  Proof. intros ?; by eapply abduct_type_cast_exists_src. Qed.


  (* moved *)
  Global Instance type_progress_moved_src_fill_is_ty Δ lv X A A' B :
    sequence_classes [exi A; Δ ⊨ ctx_hyp (A `is_ty` A')] →
    TypeProgress Δ lv X (movedT A) B GUARANTEED | 2 := {}.

  Global Instance type_progress_moved_src_fill_copy Δ lv X A B :
    Δ ⊨ Copy A →
    TypeProgress Δ lv X (movedT A) B GUARANTEED | 2 := {}.

  Global Instance type_progress_moved_src_fill_own Δ lv X p A B :
    TypeProgress Δ lv X (movedT (ownT p A)) B GUARANTEED | 1 := {}.

  Lemma abduct_type_cast_moved Δ X pm lv A B Φ R:
    Δ ⊨ virtual_type (movedT A) →
    abduct Δ (type_virt lv (movedT A) (λ A', lv ◁ₓ A' -∗ Φ (λ x, lv ◁ₓ B x)))%I R →
    abduct Δ (type_cast X pm lv (movedT A) B Φ) R.
  Proof. rewrite -type_cast_virtual //. Qed.

  Global Instance abduct_step_type_cast_moved Δ X pm lv A B Φ R:
    Δ ⊨ virtual_type (movedT A) →
    AbductStep (abduct Δ (type_virt lv (movedT A) (λ A', lv ◁ₓ A' -∗ Φ (λ x, lv ◁ₓ B x)))%I R)
      Δ (type_cast X pm lv (movedT A) B Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_cast_moved. Qed.


  (* structs *)
  Global Instance type_progress_struct Δ l X sl1 sl2 As Bs:
    (Trivial (sl1 = sl2)) →
    TypeProgress Δ (LOC l) X (structT sl1 As) (λ x, structT sl2 (Bs x)) GUARANTEED := {}.

  Lemma abduct_type_cast_struct Δ X l pm sl1 sl2 As Bs Qs (Φ: (X → iProp Σ) → iProp Σ) R:
    Trivial (sl1 = sl2) →
    (∀! x, Simpl ([∗ list] l; A ∈ member_locs l sl2; Bs x, l ◁ₗ A)%I (Qs x)) →
    abduct Δ (abd_simpl (member_locs l sl1) (λ Ls, abd_assume CTX ([∗ list] l; A ∈ Ls; As, l ◁ₗ A) (Φ Qs))) R →
    abduct Δ (type_cast X pm (LOC l) (structT sl1 As) (λ x, structT sl2 (Bs x)) Φ) R.
  Proof.
    rewrite /evar_forall /abd_simpl. intros -> Heq2. intros Habd. rewrite -type_cast_struct /abd_assume.
    iIntros "[Hctx HR]". iSplit; first done.
    rewrite /ty_own_all. iIntros "Hall".
    iDestruct (Habd with "[$Hctx $HR]") as "HP".
    rewrite /abd_assume. iSpecialize ("HP" with "Hall").
    rewrite /type_mono. iExists _. iFrame.
    iIntros (x) "HQ". rewrite -Heq2. iFrame.
  Qed.

  Global Instance abduct_step_type_cast_struct Δ X l pm sl1 sl2 As Bs Qs (Φ: (X → iProp Σ) → iProp Σ) R:
    Trivial (sl1 = sl2) →
    (∀! x, Simpl ([∗ list] l; A ∈ member_locs l sl2; Bs x, l ◁ₗ A)%I (Qs x)) →
    AbductStep (abduct Δ (abd_simpl (member_locs l sl1) (λ Ls, abd_assume CTX ([∗ list] l; A ∈ Ls; As, l ◁ₗ A) (Φ Qs))) R)
      Δ (type_cast X pm (LOC l) (structT sl1 As) (λ x, structT sl2 (Bs x)) Φ) R.
  Proof. intros ???. by eapply abduct_type_cast_struct. Qed.


  (* type cast any struct *)
  Global Instance type_progress_any_struct_guaranteed Δ X l sl m Ts :
    TypeProgress Δ (LOC l) X (anyT m) (λ x, structT sl (Ts x)) GUARANTEED := {}.

  Lemma abduct_type_cast_any_fixed_size Δ X pm l m sl Ts Us Φ R:
    Simpl (map anyT (member_sizes sl)) Ts →
    abduct Δ (type_extract_prefix l (anyT m) (ly_size sl) (λ l v, l ◁ₗ structT sl Ts -∗ Φ (λ x, l ◁ₗ structT sl (Us x))))%I R →
    abduct Δ (type_cast X pm (LOC l) (anyT m) (λ x, structT sl (Us x)) Φ) R.
  Proof. intros <- ?. rewrite -type_cast_any_fixed_size //. Qed.

  Global Instance abduct_step_type_cast_any_fixed_size Δ X pm  l m sl Ts Us Φ R:
    Simpl (map anyT (member_sizes sl)) Ts →
    AbductStep (abduct Δ (type_extract_prefix l (anyT m) (ly_size sl) (λ l v, l ◁ₗ structT sl Ts -∗ Φ (λ x, l ◁ₗ structT sl (Us x))))%I R)
      Δ (type_cast X pm (LOC l) (anyT m) (λ x, structT sl (Us x)) Φ) R.
  Proof. intros ??. by eapply abduct_type_cast_any_fixed_size. Qed.

  (* slices *)
  Global Instance type_progress_slices_slices_guaranteed X Δ l n m As Bs :
    TypeProgress Δ (LOC l) X (slicesT n As) (λ x, (slicesT (m x) (Bs x))) GUARANTEED := {}.

  Lemma abduct_type_cast_slices Δ X pm l len len' As Bs Φ Qs R:
    (∀! x, Simpl ([∗ list] b ∈ Bs x, (l +ₗ b.1.1) ◁ₗ b.2)%I (Qs x)) →
    abduct Δ (abd_simpl (([∗ list] a ∈ As, (l +ₗ a.1.1) ◁ₗ a.2)%I) (λ Ls,
                              abd_assume CTX (Ls) (Φ (λ x, ⌜len = len' x⌝ ∗ ⌜Forall2 eq As.*1 (Bs x).*1⌝ ∗ Qs x)%I))) R →
    abduct Δ (type_cast X pm (LOC l) (slicesT len As) (λ x, slicesT (len' x) (Bs x)) Φ) R.
  Proof.
    rewrite /evar_forall/Simpl/Trivial /abd_simpl => HQ.
    rewrite -type_cast_slices // /abd_assume => Habd.
    iIntros "[Hctx HR]". iIntros "Hall".
    iDestruct (Habd with "[$Hctx $HR]") as "HP".
    iExists _. iSplitR; [|by iApply "HP"]. iIntros (?) "?". by rewrite HQ.
  Qed.

  Global Instance abduct_step_type_cast_slices Δ X pm l len len' As Bs Φ Qs R H1:
    AbductStep _ _ _ _ := abduct_type_cast_slices Δ X pm l len len' As Bs Φ Qs R H1.

  Global Instance type_progress_slices_sized_guaranteed Δ X l len Ls n B :
    (∀ x, B x `has_size` n) →
    TypeProgress Δ (LOC l) X (slicesT len Ls) B GUARANTEED := {}.

  Lemma abduct_type_cast_slices_project_slice Δ X pm l len Ls n B Φ R:
    (∀ x, B x `has_size` n) →
    abduct Δ (type_project_slice l 0 n Ls (λ i li A L R, ⌜li = n⌝ ∗
      abd_assume CTX (l ◁ₗ slicesT len (L ++ (0, n, placeT (l +ₗ 0)) :: R) ∗ (l +ₗ 0) ◁ₗ A) (Φ (λ x, (l +ₗ 0) ◁ₗ B x))))%I R →
    abduct Δ (type_cast X pm (LOC l) (slicesT len Ls) B Φ) R.
  Proof. intros ??. rewrite -type_cast_slices_project_slice//. Qed.


  Lemma abduct_type_cast_slices_project_slice' Δ X pm l len Ls n m A B Φ R:
    (∀ x, B x `has_size` n) →
    Solve (n = m) →
    abduct Δ (type_project_slice l 0 n ((0, m, A) :: Ls) (λ i li A L R, ⌜li = n⌝ ∗
      abd_assume CTX (l ◁ₗ slicesT len (L ++ (0, n, placeT (l +ₗ 0)) :: R) ∗ (l +ₗ 0) ◁ₗ A) (Φ (λ x, (l +ₗ 0) ◁ₗ B x))))%I R →
    abduct Δ (type_cast X pm (LOC l) (slicesT len ((0, m, A) :: Ls)) B Φ) R.
  Proof. intros ??. rewrite -type_cast_slices_project_slice'//. Qed.

  Global Instance abduct_step_type_cast_slices_project_slice' Δ X pm l len Ls n m A B Φ R H1 H2:
    AbductStep _ _ _ _ | 5 := abduct_type_cast_slices_project_slice' Δ X pm l len Ls n m A B Φ R H1 H2.



  Global Instance type_progress_slices_array_guaranteed X Y Δ l n As (T: Y → type Σ) sz ys :
    TypeProgress Δ (LOC l) X (slicesT n As) (λ x, arrayT T (sz x) (ys x)) GUARANTEED := {}.

  Lemma abduct_type_cast_slices_array Δ {X Y} pm l n m k As Φ (xs: X → list Y) P sz B R:
    sequence_classes [
        (∀ x, B x `has_size` k);
        Δ ⊨ Solve (0 ≤ k);
        Simpl (length As) m;
        Simpl ([∗ list] p ∈ As, (l +ₗ p.1.1) ◁ₗ p.2)%I P
    ] →
    abduct Δ (abd_assume CTX P $ Φ (λ x, existsN m (λ Xs, abd_simpl (concat Xs) (λ ys, ⌜xs x = ys⌝ ∗ ⌜sz x = k⌝ ∗ ⌜Forall2 (λ zs p, p.1.2 = (length zs * k)%Z) Xs As⌝ ∗ abd_simpl ([∗ list] p;ys ∈ As;Xs, (l +ₗ p.1.1) ◁ₗ arrayT B (sz x) ys) (λ P, P))))%I) R →
    abduct Δ (type_cast X pm (LOC l) (slicesT n As) (λ x, arrayT B (sz x) (xs x))%RT Φ) R.
  Proof.
    rewrite /sequence_classes !Forall_cons /id Forall_nil. intros (?& ?& <- & <- & _) Habd.
    apply: abduct_qenvs_entails. rewrite /Solve. intros Hle.
    rewrite -type_cast_slices_array //.
  Qed.

  Global Instance abduct_step_type_cast_slices_array Δ {X Y} pm l n m k As Φ (xs: X → list Y) P sz B R:
    sequence_classes [
        (∀ x, B x `has_size` k);
        Δ ⊨ Solve (0 ≤ k);
        Simpl (length As) m;
        Simpl ([∗ list] p ∈ As, (l +ₗ p.1.1) ◁ₗ p.2)%I P
    ] →
    AbductStep (abduct Δ (abd_assume CTX P $ Φ (λ x, existsN m (λ Xs, abd_simpl (concat Xs) (λ ys, ⌜xs x = ys⌝ ∗ ⌜sz x = k⌝ ∗ ⌜Forall2 (λ zs p, p.1.2 = (length zs * k)%Z) Xs As⌝ ∗ abd_simpl ([∗ list] p;ys ∈ As;Xs, (l +ₗ p.1.1) ◁ₗ arrayT B (sz x) ys) (λ P, P))))%I) R)
      Δ (type_cast X pm (LOC l) (slicesT n As) (λ x, arrayT B (sz x) (xs x))%RT Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_cast_slices_array. Qed.

  Lemma abduct_type_cast_slices_any Δ {X} pm l n As Φ P sz R:
    Simpl ([∗ list] p ∈ As, (l +ₗ p.1.1) ◁ₗ p.2)%I P →
    abduct Δ (
    abd_assume CTX P $ Φ (λ x,
      ⌜sz x = n⌝ ∗ abd_simpl ([∗ list] p ∈ As, (l +ₗ p.1.1) ◁ₗ anyT p.1.2) (λ P, P))%I) R →
    abduct Δ (type_cast X pm (LOC l) (slicesT n As) (λ x, anyT (sz x))%RT Φ) R.
  Proof.
    intros <- Habd. rewrite -type_cast_slices_any //.
  Qed.

  Global Instance abduct_step_type_cast_slices_any Δ {X} pm l n As Φ P sz R:
    Simpl ([∗ list] p ∈ As, (l +ₗ p.1.1) ◁ₗ p.2)%I P →
    AbductStep (abduct Δ (
    abd_assume CTX P $ Φ (λ x,
      ⌜sz x = n⌝ ∗ abd_simpl ([∗ list] p ∈ As, (l +ₗ p.1.1) ◁ₗ anyT p.1.2) (λ P, P))%I) R)
      Δ (type_cast X pm (LOC l) (slicesT n As) (λ x, anyT (sz x))%RT Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_cast_slices_any. Qed.

  (* arrays *)
  Global Instance type_progress_array_guaranteed X Y Δ lv (T: Y → type Σ) sz sz' xs ys :
    TypeProgress Δ lv X (arrayT T sz xs) (λ x, arrayT T (sz' x) (ys x)) GUARANTEED := {}.

  Lemma abduct_type_cast_array_copy Δ X Y pm lv (T: Y → type Σ) sz sz' xs ys  Φ (R : iProp Σ):
    (∀ x, Copy (T x)) →
    abduct Δ (type_recover pm lv (arrayT T sz xs) (Φ (λ x, ⌜sz = sz' x⌝ ∗ ⌜xs = ys x⌝)))%I R →
    abduct Δ (type_cast X pm lv (arrayT T sz xs) (λ x, arrayT T (sz' x) (ys x)) Φ) R.
  Proof.
    intros ? Habd. rewrite -type_cast_array_copy //.
  Qed.

  Global Instance abduct_step_type_cast_array_copy Δ X Y pm lv (T: Y → type Σ) sz sz' xs ys  Φ (R : iProp Σ):
    (∀ x, Copy (T x)) →
    AbductStep (abduct Δ (type_recover pm lv (arrayT T sz xs) (Φ (λ x, ⌜sz = sz' x⌝ ∗ ⌜xs = ys x⌝)))%I R)
      Δ (type_cast X pm lv (arrayT T sz xs) (λ x, arrayT T (sz' x) (ys x)) Φ) R | 2.
  Proof. intros ??; by eapply abduct_type_cast_array_copy. Qed.

  Lemma abduct_type_cast_array Δ X Y lv pm (T: Y → type Σ) sz sz' xs ys  Φ (R : iProp Σ):
    abduct Δ (Φ (λ x, ⌜sz = sz' x⌝ ∗ ⌜xs = ys x⌝))%I R →
    abduct Δ (type_cast X pm lv (arrayT T sz xs) (λ x, arrayT T (sz' x) (ys x)) Φ) R.
  Proof.
    intros Habd. rewrite -type_cast_array //.
  Qed.

  Global Instance abduct_step_type_cast_array Δ X Y lv pm  (T: Y → type Σ) sz sz' xs ys  Φ (R : iProp Σ):
    AbductStep (abduct Δ (Φ (λ x, ⌜sz = sz' x⌝ ∗ ⌜xs = ys x⌝))%I R)
      Δ (type_cast X pm lv (arrayT T sz xs) (λ x, arrayT T (sz' x) (ys x)) Φ) R | 10.
  Proof. intros ?; by eapply abduct_type_cast_array. Qed.


  (* refinements *)
  Global Instance progress_type_refinement_eq Δ lv X {Y} T (t: Y) u :
    TypeProgress Δ lv X (t @ T) (λ x, (u x) @ T)%RT GUARANTEED := {}.

  Lemma abduct_type_cast_refinement_eq Δ lv pm X {Y} T (t: Y) u Φ R:
    abduct Δ (Φ (λ x, ⌜u x = t⌝)%I) R →
    abduct Δ (type_cast X pm lv (t @ T) (λ x, (u x) @ T)%RT Φ) R.
  Proof.
    iIntros (Habd) "Hctx". iDestruct (Habd with "Hctx") as "HΦ".
    rewrite /type_cast. iIntros "Hlv". iExists _. iFrame.
    iIntros (x) "%Hty". by subst.
  Qed.

  Global Instance abduct_step_type_cast_refinement_eq Δ pm lv X {Y} T (t: Y) u Φ R:
    AbductStep (abduct Δ (Φ (λ x, ⌜u x = t⌝)%I) R)
      Δ (type_cast X pm lv (t @ T) (λ x, (u x) @ T)%RT Φ) R | 10.
  Proof. intros ?. by eapply abduct_type_cast_refinement_eq. Qed.


  Global Instance progress_type_refinement_unfold_tgt Δ (lv: loc_or_val) X A {Y} (T: X → rtype Σ Y) r C :
    (∀! x, unfold_refinement (T x) (r x) (C x)) →
    TypeProgress Δ lv X A (λ x, (r x) @ (T x))%RT GUARANTEED := {}.

  Lemma abduct_type_cast_refinement_unfold_tgt  Δ pm (lv: loc_or_val) X A {Y} (T: X → rtype Σ Y) r C Φ R:
    (∀! x, unfold_refinement (T x) (r x) (C x)) →
    abduct Δ (type_cast X pm lv A C Φ)%I R →
    abduct Δ (type_cast X pm lv A (λ x, (r x) @ (T x))%RT Φ)%I R.
  Proof.
    rewrite /evar_forall /unfold_refinement.
    iIntros (Heq2 Habd) "Hctx". iDestruct (Habd with "Hctx") as "Hcont".
    rewrite /type_cast. by setoid_rewrite Heq2.
  Qed.

  Global Instance abduct_step_type_cast_refinement_unfold_tgt Δ pm (lv: loc_or_val) X A {Y} (T: X → rtype Σ Y) r C Φ R:
    (∀! x, unfold_refinement (T x) (r x) (C x)) →
    AbductStep (abduct Δ (type_cast X pm lv A C Φ)%I R)
      Δ (type_cast X pm lv A (λ x, (r x) @ (T x))%RT Φ) R | 15.
  Proof. intros ??; by eapply abduct_type_cast_refinement_unfold_tgt. Qed.

  Global Instance progress_type_refinement_unfold_src Δ (lv: loc_or_val) X B {Y} (T: rtype Σ Y) r C :
    (unfold_refinement T r C) →
    TypeProgress Δ lv X (r @ T)%RT B GUARANTEED := {}.

  Lemma abduct_type_cast_refinement_unfold_src  Δ (lv: loc_or_val) pm X B {Y} (T: rtype Σ Y) r C Φ R:
    (unfold_refinement T r C) →
    abduct Δ (type_cast X pm lv C B Φ)%I R →
    abduct Δ (type_cast X pm lv (r @ T)%RT B Φ)%I R.
  Proof.
    rewrite /unfold_refinement.
    iIntros (Heq1 Habd) "Hctx". iDestruct (Habd with "Hctx") as "Hcont".
    rewrite /type_cast. by setoid_rewrite Heq1.
  Qed.

  Global Instance abduct_step_type_cast_refinement_unfold_src Δ (lv: loc_or_val) pm X B {Y} (T: rtype Σ Y) r C Φ R:
    (unfold_refinement T r C) →
    AbductStep (abduct Δ (type_cast X pm lv C B Φ)%I R)
      Δ (type_cast X pm lv (r @ T)%RT B Φ) R | 15.
  Proof. intros ??; by eapply abduct_type_cast_refinement_unfold_src. Qed.


  (** variables *)
  Global Instance type_progress_existential_type_var_is_ty Δ lv X A A' B :
    sequence_classes [exi A; Δ ⊨ ctx_hyp (A `is_ty` A')] →
    TypeProgress Δ lv X A B GUARANTEED | 10 := {}.

  Global Instance type_progress_existential_type_var Δ lv X A B Y y C sub:
    sequence_classes [exi A; ∀! x, TypeParams (B x) Y (y x) C sub] →
    TypeProgress Δ lv X A B GUARANTEED | 11 := {}.

  (* existential variable in the target *)
  Global Instance progress_type_target_var_maybe Δ lv X A B :
    projection X (type Σ) B →
    TypeProgress Δ lv X A B (MAYBE 20) := {}.


  Lemma type_cast_known_type_variable X pm lv A1 A1' A2 Φ :
    A1 `is_ty` A1' →
    (lv ◁ₓ A1' -∗ Φ (λ x, lv ◁ₓ A2 x)) ⊢
    type_cast X pm lv A1 A2 Φ.
  Proof.
    rewrite /type_cast. iIntros ([Hty]) "Hcont Hlv".
    destruct lv; simpl; setoid_rewrite Hty; iSpecialize ("Hcont" with "Hlv").
    all: iExists _; iFrame; by iIntros (x) "$".
  Qed.

  Lemma abduct_type_cast_known_type_variable Δ X lv pm A1 A1' A2 Φ R:
    sequence_classes [exi A1; Δ ⊨ ctx_hyp (A1 `is_ty` A1')] →
    abduct Δ (lv ◁ₓ A1' -∗ Φ (λ x, lv ◁ₓ A2 x))%I R →
    abduct Δ (type_cast X pm lv A1 A2 Φ) R.
  Proof.
    rewrite /sequence_classes !Forall_cons /=. intros (_ & Hty & _) Habd.
    apply: abduct_qenvs_entails. rewrite /ctx_hyp. intros ?.
    rewrite -type_cast_known_type_variable //.
  Qed.

  Global Instance abduct_step_type_cast_variable_known_existential Δ X pm lv A1 A1' A2 Φ R:
    sequence_classes [exi A1; Δ ⊨ ctx_hyp (A1 `is_ty` A1')] →
    AbductStep (abduct Δ (lv ◁ₓ A1' -∗ Φ (λ x, lv ◁ₓ A2 x))%I R)
      Δ (type_cast X pm lv A1 A2 Φ) R | 10.
  Proof. intros ??. by eapply abduct_type_cast_known_type_variable. Qed.

  Lemma vty_fork_persistent v A1 X (A2: X → type Σ):
    (∀ x, Copy (A2 x)) →
    v ◁ᵥ A1 ⊢ v ◁ᵥ A1 ∗ (∀ x, ⌜A1 `is_ty` A2 x⌝ -∗ v ◁ᵥ A2 x).
  Proof.
    intros Hcp. assert (∀ x, Persistent (v ◁ᵥ A2 x)).
    { intros x. eapply Hcp. }
    iIntros "Hv1". rewrite -bi.persistent_and_sep_1. iSplit; first done.
    iIntros (x) "%Hty". destruct Hty as [Hty]. by rewrite Hty.
  Qed.

  Lemma type_cast_unknown_type_variable_copy_src X pm lv A1 A2 Φ :
    Copy A1 →
    type_recover pm lv A1 (Φ (λ x, ⌜A1 `is_ty` A2 x⌝)) ⊢
    type_cast X pm lv A1 A2 Φ.
  Proof.
    intros Hcp. rewrite /type_recover /type_cast.
    destruct lv; simpl; iIntros "Hcont".
    - iIntros "Hl". iExists _. iFrame. iIntros (x) "%Hty".
      destruct Hty as [Hty]. rewrite Hty //.
    - iIntros "#Hv". destruct pm.
      + iExists _. iFrame. iIntros (x) "%Hty".
        destruct Hty as [Hty]. rewrite Hty //.
      + iDestruct ("Hcont" with "Hv") as "Hcont".
        iExists _. iFrame. iIntros (x) "%Hty".
        destruct Hty as [Hty]. rewrite Hty //.
  Qed.


  Lemma type_cast_unknown_type_variable_copy_tgt X pm lv A1 A2 Φ :
    (∀ x, Copy (A2 x)) →
    type_recover pm lv A1 (Φ (λ x, ⌜A1 `is_ty` A2 x⌝)) ⊢
    type_cast X pm lv A1 A2 Φ.
  Proof.
    intros Hcp. rewrite /type_recover /type_cast.
    destruct lv; simpl; iIntros "Hcont".
    - iIntros "Hl". iExists _. iFrame. iIntros (x) "%Hty".
      destruct Hty as [Hty]. rewrite Hty //.
    - iIntros "Hv". destruct pm.
      + iExists _. iFrame. iIntros (x) "%Hty".
        destruct Hty as [Hty]. rewrite Hty //.
      + iDestruct (vty_fork_persistent with "Hv") as "[Hv1 Hv2]".
        iExists _. iFrame. by iApply "Hcont".
  Qed.

  Lemma type_cast_unknown_type_variable_move X pm lv A1 A2 Φ :
    type_recover pm lv (movedT A1) (Φ (λ x, ⌜A1 `is_ty` A2 x⌝)) ⊢
    type_cast X pm lv A1 A2 Φ.
  Proof.
    rewrite /type_cast.
    iIntros "HΦ Hv". iExists _. iDestruct (type_recover_moved with "HΦ Hv") as "[$ ?]".
    iIntros (x) "%Hty". destruct Hty as [Hty]. destruct lv; simpl; rewrite Hty //.
  Qed.

  Lemma abduct_type_cast_existential_type_variable_src_copy Δ X pm lv A1 A2 Φ R:
    sequence_classes [exi A1; Δ ⊨ TCOr (Copy A1) (∀ x, Copy (A2 x))] →
    abduct Δ (type_recover pm lv A1 (Φ (λ x, ⌜A1 `is_ty` A2 x⌝))%I) R →
    abduct Δ (type_cast X pm lv A1 A2 Φ) R.
  Proof.
    rewrite /sequence_classes !Forall_cons /=.
    intros (_ & Hcp' & _) Habd.
    apply: abduct_qenvs_entails. rewrite /ctx_hyp. intros [Hcp|Hcp]%tc_or_iff.
    - rewrite -type_cast_unknown_type_variable_copy_src //.
    - rewrite -type_cast_unknown_type_variable_copy_tgt //.
  Qed.

  Global Instance abduct_step_type_cast_existential_type_variable_src_copy Δ X pm lv A1 A2 Φ R:
    sequence_classes [exi A1; Δ ⊨ TCOr (Copy A1) (∀ x, Copy (A2 x))] →
    AbductStep (abduct Δ (type_recover pm lv A1 (Φ (λ x, ⌜A1 `is_ty` A2 x⌝))%I) R)
      Δ (type_cast X pm lv A1 A2 Φ) R | 11.
  Proof. intros ??. by eapply abduct_type_cast_existential_type_variable_src_copy. Qed.

  Lemma abduct_type_cast_existential_type_variable_src_move Δ X pm lv A1 A2 Φ R:
    exi A1 →
    abduct Δ (type_recover pm lv (movedT A1) (Φ (λ x, ⌜A1 `is_ty` A2 x⌝))%I) R →
    abduct Δ (type_cast X pm lv A1 A2 Φ) R.
  Proof.
    intros _ Habd. rewrite -type_cast_unknown_type_variable_move //.
  Qed.

  Global Instance abduct_step_type_cast_existential_type_variable_src_move Δ X pm lv A1 A2 Φ R:
    exi A1 →
    AbductStep (abduct Δ (type_recover pm lv (movedT A1) (Φ (λ x, ⌜A1 `is_ty` A2 x⌝))%I) R)
      Δ (type_cast X pm lv A1 A2 Φ) R | 12.
  Proof. intros ??. by eapply abduct_type_cast_existential_type_variable_src_move. Qed.


  Lemma abduct_type_cast_existential_type_variable_tgt_copy Δ X pm lv A1 A2 Φ R:
    sequence_classes [projection X (type Σ) A2; Δ ⊨ Copy A1]  →
    abduct Δ (type_recover pm lv A1 (Φ (λ x, ⌜A1 `is_ty` A2 x⌝))%I) R →
    abduct Δ (type_cast X pm lv A1 A2 Φ) R.
  Proof.
    rewrite /sequence_classes !Forall_cons /=.
    intros (_ & Hcp' & _) Habd.
    apply: abduct_qenvs_entails. rewrite /ctx_hyp. intros Hcp.
    rewrite -type_cast_unknown_type_variable_copy_src //.
  Qed.

  Global Instance abduct_step_type_cast_existential_type_variable_tgt_copy Δ X pm lv A1 A2 Φ R:
    sequence_classes [projection X (type Σ) A2; Δ ⊨ Copy A1]  →
    AbductStep (abduct Δ (type_recover pm lv A1 (Φ (λ x, ⌜A1 `is_ty` A2 x⌝))%I) R)
      Δ (type_cast X pm lv A1 A2 Φ) R | 13.
  Proof. intros ??. by eapply abduct_type_cast_existential_type_variable_tgt_copy. Qed.

  Lemma abduct_type_cast_existential_type_variable_tgt_move Δ X pm lv A1 A2 Φ R:
    projection X (type Σ) A2 →
    abduct Δ (type_recover pm lv (movedT A1) (Φ (λ x, ⌜A1 `is_ty` A2 x⌝))%I) R →
    abduct Δ (type_cast X pm lv A1 A2 Φ) R.
  Proof.
    intros _ Habd. rewrite -type_cast_unknown_type_variable_move //.
  Qed.

  Global Instance abduct_step_type_cast_existential_type_variable_tgt_move Δ X pm lv A1 A2 Φ R:
    projection X (type Σ) A2 →
    AbductStep (abduct Δ (type_recover pm lv (movedT A1) (Φ (λ x, ⌜A1 `is_ty` A2 x⌝))%I) R)
      Δ (type_cast X pm lv A1 A2 Φ) R | 14.
  Proof. intros ??. by eapply abduct_type_cast_existential_type_variable_tgt_move. Qed.


  (* WAND *)
  Global Instance type_progress_wand_guaranteed Δ A l X (P : A → iProp Σ) T B :
    TypeProgress Δ (LOC l) X (wandT P T) B GUARANTEED := {}.

  Lemma abduct_type_cast_wandT Δ X pm Y l A (P : Y → iProp Σ) T Φ R:
    abduct Δ (type_precond Y pm P (λ _, []) (λ _, []) (λ _, [])
                (λ y, Some (abd_assume CTX (l ◁ₗ T y) $ Φ (λ x, l ◁ₗ A x))%I)) R →
    abduct Δ (type_cast X pm (LOC l) (wandT P T) A Φ) R.
  Proof. by rewrite -type_cast_wandT. Qed.

  Global Instance abduct_step_type_cast_wandT Δ X pm Y l A (P : Y → iProp Σ) T Φ R:
    AbductStep _ _ _ _ := abduct_type_cast_wandT Δ X pm Y l A P T Φ R.


End type_casts.

Section precond_extension.
  Context `{!refinedcG Σ}.

  Lemma abduct_type_precond_existential_type_var Δ X pm Y A B (y: X → Y) C sub (P: X → iProp Σ) E M B' C' R:
    sequence_classes [exi A; ∀! x, TypeParams (B x) Y (y x) C sub] →
    abduct Δ (∃ z: Y, ⌜A `is_ty` C z⌝ ∗ type_precond X pm (λ x, ⌜sub (y x) z⌝ ∗ P x) E M B' C') R →
    abduct Δ (type_precond X pm (λ x, ⌜A `is_ty` B x⌝ ∗ P x) E M B' C')%I R.
  Proof.
    rewrite /sequence_classes !Forall_cons /id /evar_forall.
    intros (_ & ? & _) ?; rewrite -type_precond_existential_type_var; eauto.
  Qed.

  Global Instance abduct_step_type_precond_existential_type_var Δ X pm Y A B (y: X → Y) C sub (P: X → iProp Σ) E M B' C' R:
    sequence_classes [exi A; ∀! x, TypeParams (B x) Y (y x) C sub] →
    AbductStep (abduct Δ (∃ z: Y, ⌜A `is_ty` C z⌝ ∗ type_precond X pm (λ x, ⌜sub (y x) z⌝ ∗ P x) E M B' C') R)
      Δ (type_precond X pm (λ x, ⌜A `is_ty` B x⌝ ∗ P x) E M B' C')%I R | 1.
  Proof.
    intros ??; by eapply abduct_type_precond_existential_type_var.
  Qed.


  Lemma abduct_type_precond_xtr_non_dep Δ (X: Type) pm (lv: loc_or_val) (A2: X → type Σ) (P: X → iProp Σ) E M M' B C R:
    (∀! x n, Simpl (insert_ascending (M x) n (lv ◁ₓ A2 x)) (M' n x)) →
    abduct Δ (
      type_loc_or_val lv (λ lv' A1,
      type_progress_type X lv' A1 A2 (λ p,
        type_progress_match p
          (type_cast X pm lv' A1 A2 (λ Q, type_precond X pm (λ x, Q x ∗ P x) E M B C))
          (abd_assume CTX (lv' ◁ₓ A1) (type_precond X pm P E M (λ x, (lv' ◁ₓ A2 x) :: B x) C))
          (λ n, abd_assume CTX (lv' ◁ₓ A1) (type_precond X pm P E (M' n) B C))
          (abd_fail "type cast failed")
      ))%I
    ) R →
    abduct Δ (type_precond_xtr X pm (λ _, lv) A2 P E M B C) R.
  Proof.
    rewrite /evar_forall. intros Hsimpl Habd. rewrite -type_precond_xtr_non_dep //.
    intros x n. rewrite -Hsimpl. rewrite insert_ascending_sep //.
  Qed.

  Global Instance abduct_step_type_precond_xtr_non_dep Δ pm (X: Type) (lv: loc_or_val) (A2: X → type Σ) (P: X → iProp Σ) E M M' B C R:
    (∀! x n, Simpl (insert_ascending (M x) n (lv ◁ₓ A2 x)) (M' n x)) →
    AbductStep (abduct Δ (
      type_loc_or_val lv (λ lv' A1,
      type_progress_type X lv' A1 A2 (λ p,
        type_progress_match p
          (type_cast X pm lv' A1 A2 (λ Q, type_precond X pm (λ x, Q x ∗ P x) E M B C))
          (abd_assume CTX (lv' ◁ₓ A1) (type_precond X pm P E M (λ x, (lv' ◁ₓ A2 x) :: B x) C))
          (λ n, abd_assume CTX (lv' ◁ₓ A1) (type_precond X pm P E (M' n) B C))
          (abd_fail "type cast failed")
      ))%I
    ) R)
      Δ (type_precond_xtr X pm (λ _, lv) A2 P E M B C) R | 1.
  Proof.
    intros ??; by eapply abduct_type_precond_xtr_non_dep.
  Qed.

  Lemma abduct_type_precond_xtr_dep Δ (X: Type) pm (lv: X → loc_or_val) (A: X → type Σ) (P: X → iProp Σ) E M B C R:
    abduct Δ (type_precond X pm P E M (λ x, (lv x ◁ₓ A x) :: B x) C) R →
    abduct Δ (type_precond_xtr X pm lv A P E M B C) R.
  Proof. rewrite -type_precond_xtr_dep //. Qed.

  Global Instance abduct_step_type_precond_xtr_dep Δ (X: Type) pm (lv: X → loc_or_val) (A: X → type Σ) (P: X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (type_precond X pm P E M (λ x, (lv x ◁ₓ A x) :: B x) C) R)
      Δ (type_precond_xtr X pm lv A P E M B C) R | 10.
  Proof. intros ?; by eapply abduct_type_precond_xtr_dep. Qed.

  Lemma abduct_type_precond_sep_xtr Δ (X: Type) pm lv (A: X → type Σ) (P: X → iProp Σ) E M B C R:
    abduct Δ (type_precond_xtr X pm lv A P E M B C) R →
    abduct Δ (type_precond X pm (λ x, lv x ◁ₓ A x ∗ P x)%I E M B C) R.
  Proof. rewrite -type_precond_sep_xtr //. Qed.

  Global Instance abduct_step_type_precond_sep_xtr Δ (X: Type) pm lv (A: X → type Σ) (P: X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (type_precond_xtr X pm lv A P E M B C) R)
      Δ (type_precond X pm (λ x, lv x ◁ₓ A x ∗ P x)%I E M B C) R | 1.
  Proof. intros ?; by eapply abduct_type_precond_sep_xtr. Qed.

  Lemma abduct_type_precond_sep_vtr Δ (X: Type) pm (v: X → val) (A: X → type Σ) (P: X → iProp Σ) E M B C R:
    abduct Δ (type_precond_xtr X pm (λ x, VAL (v x)) A P E M B C) R →
    abduct Δ (type_precond X pm (λ x, v x ◁ᵥ A x ∗ P x)%I E M B C) R.
  Proof. rewrite -type_precond_sep_vtr //. Qed.

  Global Instance abduct_step_type_precond_sep_vtr Δ (X: Type) pm (v: X → val) (A: X → type Σ) (P: X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (type_precond_xtr X pm (λ x, VAL (v x)) A P E M B C) R)
      Δ (type_precond X pm (λ x, v x ◁ᵥ A x ∗ P x)%I E M B C) R | 2.
  Proof. intros ?; by eapply abduct_type_precond_sep_vtr. Qed.

  Lemma abduct_type_precond_sep_ltr Δ (X: Type) pm (l: X → loc) (A: X → type Σ) (P: X → iProp Σ) E M B C R:
    abduct Δ (type_precond_xtr X pm (λ x, LOC (l x)) A P E M B C) R →
    abduct Δ (type_precond X pm (λ x, l x ◁ₗ A x ∗ P x)%I E M B C) R.
  Proof. rewrite -type_precond_sep_ltr //. Qed.

  Global Instance abduct_step_type_precond_sep_ltr Δ (X: Type) pm (l: X → loc) (A: X → type Σ) (P: X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (type_precond_xtr X pm (λ x, LOC (l x)) A P E M B C) R)
      Δ (type_precond X pm (λ x, l x ◁ₗ A x ∗ P x)%I E M B C) R | 2.
  Proof. intros ?; by eapply abduct_type_precond_sep_ltr. Qed.


  (* MAYBE *)
  Lemma abduct_type_maybe_cast Δ Δ' pm (X: Type) (lv: loc_or_val) A1 A2 Φ (R: iProp Σ):
    qenvs_find_atom DEEP Δ (lv ◁ₓ A1) Δ' →
    abduct Δ' (type_cast X pm lv A1 A2 Φ) R →
    abduct Δ (type_maybe X pm (λ x, lv ◁ₓ A2 x) Φ) R.
  Proof.
    intros Hf Habd. rewrite -type_maybe_cast //.
    iIntros "[HΔ HR]". rewrite Hf. iDestruct "HΔ" as "[$ HΔ]".
    iApply Habd. iFrame.
  Qed.

  Global Instance abduct_step_type_maybe_cast Δ Δ' pm (X: Type) (lv: loc_or_val) A1 A2 Φ (R: iProp Σ):
    qenvs_find_atom DEEP Δ (lv ◁ₓ A1) Δ' →
    AbductStep (abduct Δ' (type_cast X pm lv A1 A2 Φ) R)
      Δ (type_maybe X pm (λ x, lv ◁ₓ A2 x) Φ) R.
  Proof. intros ??. by eapply abduct_type_maybe_cast. Qed.


  Lemma abduct_type_maybe_ltr Δ Δ' pm X l A1 A2 Φ R:
    qenvs_find_atom DEEP Δ (l ◁ₗ A1) Δ' →
    (abduct Δ' (type_cast X pm (LOC l) A1 A2 Φ) R) →
    (abduct Δ (type_maybe X pm (λ x, l ◁ₗ A2 x) Φ) R).
  Proof.
    intros ??. eapply (abduct_type_maybe_cast _ _ _ _ (LOC l)); eauto.
  Qed.

  Global Instance abduct_step_type_maybe_ltr Δ Δ' pm X l A1 A2 Φ R:
    qenvs_find_atom DEEP Δ (l ◁ₗ A1) Δ' →
    AbductStep (abduct Δ' (type_cast X pm (LOC l) A1 A2 Φ) R)
      Δ (type_maybe X pm (λ x, l ◁ₗ A2 x) Φ) R.
  Proof. intros ??. by eapply abduct_type_maybe_ltr. Qed.


  Lemma abduct_type_maybe_vtr Δ Δ' pm X v A1 A2 Φ R:
    qenvs_find_atom DEEP Δ (v ◁ᵥ A1) Δ' →
    (abduct Δ' (type_cast X pm (VAL v) A1 A2 Φ) R) →
    (abduct Δ (type_maybe X pm (λ x, v ◁ᵥ A2 x) Φ) R).
  Proof.
    intros ??. eapply (abduct_type_maybe_cast _ _ _ _ (VAL v)); eauto.
  Qed.

  Global Instance abduct_step_type_maybe_vtr Δ Δ' pm X v A1 A2 Φ R:
    qenvs_find_atom DEEP Δ (v ◁ᵥ A1) Δ' →
    AbductStep (abduct Δ' (type_cast X pm (VAL v) A1 A2 Φ) R)
      Δ (type_maybe X pm (λ x, v ◁ᵥ A2 x) Φ) R.
  Proof. intros ??. by eapply abduct_type_maybe_vtr. Qed.


  Lemma abduct_type_maybe_has_size_unknown Δ (X: Type) pm (A: X → type Σ) (n: X → Z) Φ (R: iProp Σ):
    sequence_classes [projection X Z n; projection X (type Σ) A] →
    abduct Δ (Φ (λ x, ∃ v, ⌜A x = valueT v (n x)⌝)%I) R →
    abduct Δ (type_maybe X pm (λ x, A x `has_size` n x) Φ) R.
  Proof.
    intros. rewrite -type_maybe_has_size_unknown //.
  Qed.

  Global Instance abduct_step_type_maybe_has_size_unknown Δ (X: Type) pm (A: X → type Σ) (n: X → Z) Φ (R: iProp Σ):
    sequence_classes [projection X Z n; projection X (type Σ) A] →
    AbductStep (abduct Δ (Φ (λ x, ∃ v, ⌜A x = valueT v (n x)⌝)%I) R)
      Δ (type_maybe X pm (λ x, A x `has_size` n x) Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_maybe_has_size_unknown. Qed.


  Lemma abduct_type_maybe_has_size_known Δ (X: Type) pm (A: type Σ) m (n: X → Z) Φ (R: iProp Σ):
    sequence_classes [projection X Z n; Δ ⊨ A `has_size` m] →
    abduct Δ (Φ (λ x, ⌜n x = m⌝)%I) R →
    abduct Δ (type_maybe X pm (λ x, A `has_size` n x) Φ) R.
  Proof.
    rewrite /sequence_classes !Forall_cons /=. intros (_ & Hsz & _) Habd.
    rewrite -type_maybe_has_size_known //.
    eapply abduct_prove_pure; done.
  Qed.

  Global Instance abduct_step_type_maybe_has_size_known Δ (X: Type) pm (A: type Σ) m (n: X → Z) Φ (R: iProp Σ):
    sequence_classes [projection X Z n; Δ ⊨ A `has_size` m] →
    AbductStep (abduct Δ (Φ (λ x, ⌜n x = m⌝)%I) R)
      Δ (type_maybe X pm (λ x, A `has_size` n x) Φ) R | 1.
  Proof. intros ??. by eapply abduct_type_maybe_has_size_known. Qed.


End precond_extension.