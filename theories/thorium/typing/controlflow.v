From quiver.thorium.logic Require Import pointers controlflow.
From quiver.argon.simplification Require Import simplify.
From quiver.thorium.typing Require Import assume_type garbage_collection unification arithmetic.
From quiver.argon.abduction Require Import abduct proofmode pure.
From quiver.argon.base Require Import judgments.


Section abduction_lemmas.
  Context `{!refinedcG Σ}.

  Class if_cond_type (ot: op_type) (T: Type) (A: T → type Σ) : Prop.


  Definition type_return_fill (A: type Σ) Φ : iProp Σ :=
    ∀ v, v ◁ᵥ A -∗ ∃ B, v ◁ᵥ B ∗ Φ B.

  (* TYPE POST *)
  Lemma abduct_type_post_yield (Δ : qenvs Σ) Φ (A : type Σ) (R : iProp Σ) :
    abduct Δ (abd_remove_idents $ abd_prune_assumptions $ type_return_fill A (λ B, abd_yield (type_post Φ B))%I) R →
    abduct Δ (type_post Φ A) R.
  Proof.
    rewrite /abd_remove_idents /abd_prune_assumptions /type_return_fill /abd_yield /type_post /type_cont.
    iIntros (Habd) "Hctx". iIntros (v) "Hv".
    iDestruct (Habd with "Hctx Hv") as "[%B [Hv HΦ]]".
    by iApply "HΦ".
  Qed.

  (* FUNCTION RETURN *)
  Lemma abduct_type_return Δ v A Q Φ R :
    abduct Δ (Φ v A) R →
    abduct Δ (type_return v A Q Φ) R.
  Proof.
    rewrite /type_return.
    iIntros (Habd) "Ctx Hv".
    iApply swp_return. iApply rwp_val. rewrite /type_val.
    iExists _. iFrame. iApply type_return_resolve.
    iApply (Habd with "Ctx").
  Qed.

  (* when we eventually hit the unwind, we collect all garbage
    and then fill the context *)
  Lemma abduct_type_unwind_execute Δ Φ v A R :
    abduct Δ (
      type_return_fill A (* we fill the type A (and the context) with local variables before removing them *)
    $ λ B, type_prune_args $
           type_size B
    $ λ n,
        (* before the value [v] goes out of scope, we give it back to the context in case it is used somewhere *)
        ∀ w, abd_assume ASSERT (w ◁ᵥ valueT v n) $ abd_assume ASSUME (v ◁ᵥ B) $ type_post Φ (valueT w n))%I R  →
    abduct Δ (type_unwind v A Φ) R.
  Proof.
    rewrite /type_unwind /type_return_fill /type_prune_args /type_size /abd_assume /type_post.
    iIntros (Habd) "Hctx Hv". iDestruct (Habd with "Hctx Hv") as "[%B [Hv HΦ]]".
    iDestruct "HΦ" as "(%n & %Hz & Hcont)".
    iDestruct (ty_size_eq with "Hv") as %->.
    rewrite /type_cont. iApply ("Hcont" $! v with "[] Hv []").
    { by iApply vtr_valueT_intro. }
    { by iApply vtr_valueT_intro. }
  Qed.

  Lemma abduct_type_return_fill Δ A Φ R :
    abduct Δ (∀ v, abd_assume ASSERT (v ◁ᵥ A) $ type_garbage_collect $ type_fill_ctx  $ type_val v (λ _ B, Φ B)) R →
    abduct Δ (type_return_fill A Φ) R.
  Proof.
    rewrite /type_return_fill /abd_assume /type_garbage_collect /type_fill_ctx /type_val.
    iIntros (Habd) "Hctx". iIntros (v) "Hv". iDestruct (Habd with "Hctx Hv") as "[%B [Hv HΦ]]".
    iExists _. iFrame.
  Qed.

  (* GOTO *)
  Lemma abduct_type_goto Δ cfg lb s Φ R:
    ComputeMapLookup cfg lb s →
    abduct Δ (swp s cfg Φ) R →
    abduct Δ (type_goto lb cfg Φ) R.
  Proof.
    intros ?; rewrite -type_goto_jump //.
  Qed.

  Lemma abduct_type_goto_btr Δ Δ' Q lb P Φ R:
    qenvs_find_atom SHALLOW Δ (lb ◁ᵦ P; Q; Φ) Δ' →
    abduct Δ' P R →
    abduct Δ (type_goto lb Q Φ) R.
  Proof.
    iIntros (Hent Habd).
    rewrite -type_goto_btr.
    iIntros "[HΔ HR]". rewrite Hent.
    iDestruct "HΔ" as "[$ HΔ]".
    iApply Habd. iFrame.
  Qed.

  Lemma abduct_type_break_at Δ Δmv Δpers lb P P' R cfg Φ:
    qenvs_move_assumptions_to_assertions Δ Δmv →
    qenvs_create_persistent_copy Δ Δpers →
    clear_universals (∀! Q, abduct Δmv (abd_assume CTX (□ (lb ◁ᵦ abd_cont_wrap Q; cfg; Φ))%I P) (P' Q)) →
    abduct Δpers (abd_evars P' (λ P'', P'' (type_goto lb cfg Φ)%I)) R →
    abduct Δ (type_break_at lb cfg Φ P) R.
  Proof.
    rewrite /type_break_at /clear_universals /abd_assume.
    intros Hmv Hpers Habd1 Habd2.
    iIntros "[Δ R]". rewrite qenvs_ctx_persistent_copy -Hpers.
    iDestruct "Δ" as "[Δp Δ]".
    iApply (Habd1 with "[Δp Δ R] []").
    -rewrite (qenvs_ctx_assumptions_to_assertions Δ) -Hmv.
      iFrame. iDestruct (Habd2 with "[$Δp $R]") as "Hx".
      rewrite /abd_evars. iDestruct "Hx" as "(%Ψ & Hm & HΨ)".
      iApply ("Hm" with "HΨ").
    - iModIntro. iApply btr_type_goto.
  Qed.

  Global Instance abduct_step_type_break_at Δ Δmv Δpers lb P P' R cfg Φ:
    qenvs_move_assumptions_to_assertions Δ Δmv →
    qenvs_create_persistent_copy Δ Δpers →
    AbductStep (
      clear_universals (∀! Q, abduct Δmv (abd_assume CTX (□ (lb ◁ᵦ abd_cont_wrap Q; cfg; Φ))%I P) (P' Q)) ∧
    abduct Δpers (abd_evars P' (λ P'', P'' (type_goto lb cfg Φ)%I)) R)
    Δ (type_break_at lb cfg Φ P) R.
  Proof. intros ?? [? ?]. by eapply abduct_type_break_at. Qed.

  (* LOOPS *)
  (* for the [type_loop] rule see flexihints.v *)
  Lemma abduct_type_loop_core Δ lb I Q Φ R:
    abduct Δ (type_pre CONSUME (I ∗ type_precond_yield $ type_loop_core_assuming_inv lb I Q Φ)%I) R →
    abduct Δ (type_loop_core lb I Q Φ) R.
  Proof.
    rewrite /type_precond /type_loop_core /type_loop_core_assuming_inv /type_precond_yield /type_fill_ctx /=.
    iIntros (Habd) "[HΔ HQ]". iDestruct (Habd with "[$HΔ $HQ]") as "H".
    iDestruct "H" as "(%x & [I Hcont] & _)". by iApply "Hcont".
  Qed.

  Global Instance abduct_step_type_loop_core Δ lb I Q Φ R:
    AbductStep (abduct Δ (type_pre CONSUME (I ∗ type_precond_yield $ type_loop_core_assuming_inv lb I Q Φ)%I) R) Δ  (type_loop_core lb I Q Φ) R.
  Proof. intros ?. by eapply abduct_type_loop_core. Qed.

  Lemma abduct_type_loop_core_assuming_inv Δ Δ' lb I Q Ω Φ R:
    qenvs_sep_assertions Δ Ω →
    qenvs_create_persistent_copy Δ Δ' →
    abduct Δ' (type_loop_core_assuming_inv_ctx lb I Ω Q Φ) R →
    abduct Δ (type_loop_core_assuming_inv lb I Q Φ) R.
  Proof.
    rewrite /type_loop_core_assuming_inv /type_loop_core_assuming_inv_ctx. iIntros (Hctx1 Hctx2 Habd) "[Δ R]".
    rewrite qenvs_ctx_persistent_copy Hctx1 -Hctx2.
    iDestruct "Δ" as "[Δ' Ω]".
    iIntros "I". iApply (Habd with "[$Δ' $R] Ω I").
  Qed.

  Global Instance abduct_step_type_loop_core_assuming_inv Δ Δ' lb I Q Ω Φ R:
    qenvs_sep_assertions Δ Ω →
    qenvs_create_persistent_copy Δ Δ' →
    AbductStep (abduct Δ' (type_loop_core_assuming_inv_ctx lb I Ω Q Φ) R) Δ  (type_loop_core_assuming_inv lb I Q Φ) R.
  Proof. intros ???. by eapply abduct_type_loop_core_assuming_inv. Qed.

  Lemma abduct_type_loop_core_assuming_inv_ctx Δ Δpers Ω I Q lb s Φ F R:
    Q !! lb = Some s →
    qenvs_create_persistent_copy Δ Δpers →
    (∀! P, abduct Δpers
      (abd_assume ASSERT (□ (lb ◁ᵦ abd_simpl ([∗] Ω)%I (λ Q, type_pre CONSUME (I ∗ Q ∗ type_precond_yield (type_garbage_collect $ abd_yield P)))%I; Q; Φ))%I $
      abd_assume ASSERT ([∗] Ω) $
      abd_assume ASSUME I $
      swp s Q Φ) (F P)) →
    abduct Δ (abd_evars F (λ G, type_loop_fixp G)) R →
    abduct Δ (type_loop_core_assuming_inv_ctx lb I Ω Q Φ) R.
  Proof.
    rewrite /type_loop_core_assuming_inv_ctx /type_loop_fixp /abd_assume. rewrite /abd_simpl.
    iIntros (Hlookup Hcopy Hbody Habd) "[Δ R] Ω I".
    rewrite qenvs_ctx_persistent_copy. iDestruct "Δ" as "[Δ' Δ]".
    rewrite -(type_goto_checkpoint_with_inv (I ∗ [∗] Ω ∗ qenvs_ctx_prop (qenvs_persistent_copy Δ)) F); [| done |].
    - iFrame. rewrite /abd_evars in Habd.
      iDestruct (Habd with "[$Δ $R]") as "(%Ψ & #Hw1 & %P & HP & #Hw2)".
      iExists P. iFrame. iModIntro. iIntros "HP". iApply "Hw1". by iApply "Hw2".
    - rewrite /btr. iIntros (P) "[#Hent HF]". iIntros "[I [Ω #HΔ]]".
      rewrite /abd_assume in Hbody.
      iApply (Hbody with "[HF] [] Ω I").
      + iFrame. rewrite Hcopy. iFrame "HΔ".
      + iModIntro. rewrite /btr. iModIntro.
        rewrite /type_precond. iIntros "(%x & Hx)"; simpl.
        iDestruct "Hx" as "[[I [Ω P]] _]". rewrite /type_precond_yield /type_garbage_collect /abd_yield.
        iApply "Hent". by iFrame.
  Qed.

  Global Instance abduct_step_type_loop_core_assuming_inv_ctx Δ Δpers Ω I Q lb s Φ F R:
    ComputeMapLookup Q lb s →
    qenvs_create_persistent_copy Δ Δpers →
    AbductStep
    ((∀! P, abduct Δpers
      (abd_assume ASSERT (□ (lb ◁ᵦ abd_simpl ([∗] Ω)%I (λ Q, type_pre CONSUME (I ∗ Q ∗ type_precond_yield (type_garbage_collect $ abd_yield P)))%I; Q; Φ))%I $
      abd_assume ASSERT ([∗] Ω) $
      abd_assume ASSUME I $
      swp s Q Φ) (F P)) ∧ abduct Δ (abd_evars F (λ G, type_loop_fixp G)) R)
      Δ (type_loop_core_assuming_inv_ctx lb I Ω Q Φ) R.
  Proof.
    rewrite /ComputeMapLookup. intros??[? ?].
    by eapply abduct_type_loop_core_assuming_inv_ctx.
  Qed.

  Lemma abduct_type_loop_fixp Δ (F: iProp Σ → iProp Σ) G Q R:
    (∀! P, abduct qenvs_empty (abd_assume ASSERT (loop_continue P) $ F (abd_loop_continue P)) (G P)) →
    (∀ P, loop_separate_loop_continue P (G P) Q) →
    abduct Δ Q R →
    abduct Δ (type_loop_fixp F) R.
  Proof.
    iIntros (Hbody Hfact Habd) "[Δ R]".
    rewrite /type_loop_fixp. iExists Q.
    iDestruct (Habd with "[$Δ $R]") as "$".
    iModIntro. iIntros "Q". specialize (Hbody Q).
    rewrite /abd_assume /loop_continue /abd_loop_continue in Hbody.
    iApply (Hbody with "[] Q").
    rewrite qenvs_ctx_empty. rewrite -Hfact.
    iSplit; first done. by iIntros "$".
  Qed.

  Global Instance abduct_step_type_loop_fixp Δ (F: iProp Σ → iProp Σ) G Q R:
    AbductStep
      ((∀! P, abduct qenvs_empty (abd_assume ASSERT (loop_continue P) $ F (abd_loop_continue P)) (G P)) ∧
      (∀ P, loop_separate_loop_continue P (G P) Q) ∧
      abduct Δ Q R)
    Δ (type_loop_fixp F) R.
  Proof. intros (? & ? & ?). by eapply abduct_type_loop_fixp. Qed.

  (* FREEING LOCATIONS *)
  Lemma abduct_type_free_locs_nil Δ P R:
    abduct Δ P R →
    abduct Δ (type_free_locs nil P) R.
  Proof. by rewrite -type_free_locs_nil. Qed.

  Lemma abduct_type_free_locs_cons Δ l ly L P R:
    abduct Δ (type_loc l (λ r A, type_free_loc_typed r A ly (type_free_locs L P))) R →
    abduct Δ (type_free_locs ((l, ly)::L) P) R.
  Proof.
    by rewrite -type_free_locs_cons.
  Qed.

  Lemma abduct_type_dealloc_all_nil Δ P R:
    abduct Δ P R →
    abduct Δ (type_dealloc_all nil P) R.
  Proof. by rewrite -type_dealloc_all_nil. Qed.

  Lemma abduct_type_dealloc_all_cons Δ l ly L P R:
    abduct Δ (type_loc l (λ r A, type_free_loc_typed r A (ly_size ly) (type_dealloc_all L P))) R →
    abduct Δ (type_dealloc_all ((l, ly)::L) P) R.
  Proof.
    by rewrite -type_dealloc_all_cons.
  Qed.

  Lemma abduct_type_free_loc_typed_has_layout Δ l A ly P R :
    Δ ⊨ A `has_size` ly →
    abduct Δ (∀ v, abd_assume CTX (v ◁ᵥ A) P) R →
    abduct Δ (type_free_loc_typed l A ly P) R.
  Proof.
    intros ??. apply: abduct_qenvs_entails. intros Hly.
    rewrite -type_free_loc_typed_has_layout //.
  Qed.

  Lemma abduct_type_free_loc_typed_value Δ l v ly P R :
    abduct Δ P R →
    abduct Δ (type_free_loc_typed l (valueT v ly) ly P) R.
  Proof.
    rewrite /abduct_pure. iIntros (Habd) "Hctx".
    iDestruct (Habd with "Hctx") as "HΦ".
    iApply type_free_loc_typed_has_layout.
    rewrite /abd_assume. by iIntros (w) "_".
  Qed.

  Lemma abduct_type_free_loc_typed_place Δ l r ly P R :
    abduct Δ (type_loc r (λ r' A, type_free_loc_typed r' A ly (abd_assume CTX (l ◁ₗ placeT r') P))) R →
    abduct Δ (type_free_loc_typed l (placeT r) ly P) R.
  Proof.
    rewrite -type_free_loc_typed_place //.
  Qed.

  Lemma abduct_type_free_loc_typed_is_var Δ l n A P R:
    exi A →
    abduct Δ (abduct_class (A `has_size` n) P) R →
    abduct Δ (type_free_loc_typed l A n P) R.
  Proof.
    intros ?. rewrite -type_free_loc_typed_is_ty //.
  Qed.

  Lemma abduct_type_free_loc_typed_virtual Δ l A ly P R :
    Δ ⊨ virtual_type A →
    abduct Δ (type_virt (LOC l) A (λ A', type_free_loc_typed l A' ly P)) R →
    abduct Δ (type_free_loc_typed l A ly P) R.
  Proof. move => ?. by rewrite -type_free_loc_typed_virtual. Qed.

  Lemma abduct_type_free_loc_typed_struct Δ l T sl P L LY R:
    (* the identity map forces computation *)
    Simpl (map id (member_locs l sl)) L →
    Simpl (map id (member_sizes sl)) LY →
    abduct Δ (type_free_locs_typed L T LY P) R →
    abduct Δ (type_free_loc_typed l (structT sl T) (ly_size sl) P) R.
  Proof.
    intros <- <-. rewrite !map_id. rewrite -type_free_loc_typed_struct //.
  Qed.


  Lemma abduct_type_free_locs_typed_nil Δ P R:
    abduct Δ P R →
    abduct Δ (type_free_locs_typed nil nil nil P) R.
  Proof. by rewrite -type_free_locs_typed_nil. Qed.

  Lemma abduct_type_free_locs_typed_cons Δ l ly A L T LY P R:
    abduct Δ (type_free_loc_typed l A ly (type_free_locs_typed L T LY P)) R →
    abduct Δ (type_free_locs_typed (l :: L) (A :: T) (ly :: LY) P) R.
  Proof. by rewrite -type_free_locs_typed_cons. Qed.


  (* IF-EXPRESSIONS *)
  Lemma abduct_type_if_cond_type_if_expr Δ (v: val) (A: type Σ) e1 e2 ot Φ R:
    abduct Δ (type_if_cond v A ot (λ φ, type_if_expr_prop φ e1 e2 Φ)) R →
    abduct Δ (type_if_expr ot v A e1 e2 Φ) R.
  Proof. rewrite -type_if_cond_type_if_expr //. Qed.

  Lemma abduct_type_if_expr_prop_case Δ Δmv Δpers φ e1 e2 Φ Q R:
    qenvs_move_assumptions_to_assertions Δ Δmv →
    qenvs_create_persistent_copy Δ Δpers →
    clear_universals (∀! Θ, abduct Δmv (case φ (rwp e1 (λ _ A, type_cont Θ A)) (rwp e2 (λ _ A, type_cont Θ A))) (Q Θ)) →
    (* instead of joining the value, we proceed with a new, universally quantified value *)
    abduct Δpers (Q (λ A, ∀ v: val, Φ v A)%I) R →
    abduct Δ (type_if_expr_prop φ e1 e2 Φ) R.
  Proof.
    rewrite /abduct /type_if_expr_prop.
    iIntros (Hmv Hpers Habd1 Habd2) "[HΔ HR]".
    rewrite qenvs_ctx_persistent_copy (qenvs_ctx_assumptions_to_assertions Δ).
    rewrite -Hmv -Hpers. iDestruct "HΔ" as "[Hmv Hpers]".
    iPoseProof (Habd2 with "[$Hmv $HR]") as "HQ".
    iPoseProof (Habd1 with "[$Hpers $HQ]") as "HΘ".
    rewrite /case /type_cont. destruct decide; iApply (wp_wand with "HΘ").
    - iIntros (w) "[%A [Hw HΦ]]". iPoseProof ("HΦ" with "Hw") as "[%B [Hw HΦ]]".
      iExists _. iFrame. iApply "HΦ".
    - iIntros (w) "[%A [Hw HΦ]]". iPoseProof ("HΦ" with "Hw") as "[%B [Hw HΦ]]".
      iExists _. iFrame. iApply "HΦ".
  Qed.

  (* LOGICAL AND and LOGICAL OR *)
  Lemma abduct_type_logical_or Δ v ot1 ot2 rit A e Φ R:
    abduct Δ (abd_breakpoint (λ Φ, abd_pose_proof None fancy_if $ abd_pose_proof None very_fancy_if $ type_if_expr ot1 v A (b2v true rit) (bool_eta ot2 e rit) (λ v A, type_to_bool v A Φ)) (λ Q, Q (λ A, ∀ w, Φ w A)%I)) R →
    abduct Δ (type_logical_or ot1 ot2 rit v A e Φ) R.
  Proof.
    iIntros (Habd) "Hctx". rewrite -type_logical_or_type_if_expr.
    iDestruct (Habd with "Hctx") as "Hcont".
    rewrite /abd_breakpoint /abd_pose_proof.
    iDestruct ("Hcont") as "(%Ψ & Hw & HΨ)".
    iSpecialize ("Hw" with "HΨ [] []").
    { iPureIntro. apply FancyIf. }
    { iPureIntro. apply VeryFancyIf. }
    rewrite /type_if_expr.
    iIntros "Hv". rewrite /bool_eta /b2v /=.
    iApply (wp_wand with "(Hw Hv)").
    iIntros (w) "(%B & Hw & Hcont)".
    rewrite /type_to_bool. iDestruct ("Hcont" with "Hw") as "(%C & Hw & HΦ)".
    iExists _. iFrame. by iApply "HΦ".
  Qed.

  Lemma abduct_type_logical_and Δ v ot1 ot2 rit A e Φ R:
    abduct Δ (abd_breakpoint (λ Φ, abd_pose_proof None fancy_if $ abd_pose_proof None very_fancy_if $ type_if_expr ot1 v A (bool_eta ot2 e rit) (b2v false rit)  (λ v A, type_to_bool v A Φ)) (λ Q, Q (λ A, ∀ w, Φ w A)%I)) R →
    abduct Δ (type_logical_and ot1 ot2 rit v A e Φ) R.
  Proof.
    iIntros (Habd) "Hctx". rewrite -type_logical_and_type_if_expr.
    iDestruct (Habd with "Hctx") as "Hcont".
    rewrite /abd_breakpoint /abd_pose_proof.
    iDestruct ("Hcont") as "(%Ψ & Hw & HΨ)".
    iSpecialize ("Hw" with "HΨ [] []").
    { iPureIntro. apply FancyIf. }
    { iPureIntro. apply VeryFancyIf. }
    rewrite /type_if_expr.
    iIntros "Hv". rewrite /bool_eta /b2v /=.
    iApply (wp_wand with "(Hw Hv)").
    iIntros (w) "(%B & Hw & Hcont)".
    rewrite /type_to_bool. iDestruct ("Hcont" with "Hw") as "(%C & Hw & HΦ)".
    iExists _. iFrame. by iApply "HΦ".
  Qed.

  (* TYPE BOOL ETA *)
  Lemma abduct_type_bool_eta_type_if_cond Δ ot v A it Φ R:
    abduct Δ (type_if_cond v A ot (λ φ, ∀ w, Φ w (φ @ boolR it)%RT)%I) R →
    abduct Δ (type_bool_eta ot v A it Φ) R.
  Proof. rewrite -type_bool_eta_type_if_cond //. Qed.

  (* TYPE BOOL *)
  Lemma abduct_type_to_bool_intro Δ v φ it Φ R:
    abduct Δ (Φ (φ @ boolR it)%RT) R →
    abduct Δ (type_to_bool v (φ @ boolR it)%RT Φ) R.
  Proof. rewrite -type_to_bool_intro //. Qed.

  Lemma abduct_type_to_bool_if_true_l Δ v (φ ψ: dProp) it Φ R:
    abduct Δ (type_to_bool v ((φ ∨ ψ)%DP @ boolR it)%RT Φ) R →
    abduct Δ (type_to_bool v (case φ True%DP ψ @ boolR it)%RT Φ) R.
  Proof. rewrite -type_to_bool_if_true_l //. Qed.

  Lemma abduct_type_to_bool_if_true_r Δ v (φ ψ: dProp) it Φ R:
    abduct Δ (type_to_bool v ((φ → ψ)%DP @ boolR it)%RT Φ) R →
    abduct Δ (type_to_bool v (case φ ψ True%DP @ boolR it)%RT Φ) R.
  Proof. rewrite -type_to_bool_if_true_r //. Qed.

  Lemma abduct_type_to_bool_if_false_l Δ v (φ ψ: dProp) it Φ R:
    abduct Δ (type_to_bool v ((¬ φ ∧ ψ)%DP @ boolR it)%RT Φ) R →
    abduct Δ (type_to_bool v (case φ False%DP ψ @ boolR it)%RT Φ) R.
  Proof. rewrite -type_to_bool_if_false_l //. Qed.

  Lemma abduct_type_to_bool_if_false_r Δ v (φ ψ: dProp) it Φ R:
    abduct Δ (type_to_bool v ((φ ∧ ψ)%DP @ boolR it)%RT Φ) R →
    abduct Δ (type_to_bool v (case φ ψ False%DP @ boolR it)%RT Φ) R.
  Proof. rewrite -type_to_bool_if_false_r //. Qed.

  Lemma abduct_type_to_bool_eta Δ v (φ: dProp) it Φ R:
    abduct Δ (type_to_bool v (φ @ boolR it)%RT Φ) R →
    abduct Δ (type_to_bool v (case φ True%DP False%DP @ boolR it)%RT Φ) R.
  Proof. rewrite -type_to_bool_eta //. Qed.


  (* IF-STATEMENTS *)
  Lemma abduct_type_if_cond_type_if_stmt Δ (v: val) (A: type Σ) s1 s2 join ot Q Φ R:
    abduct Δ (type_if_cond v A ot (λ φ, type_if_prop φ join s1 s2 Q Φ)) R →
    abduct Δ (type_if_stmt v A ot join s1 s2 Q Φ) R.
  Proof. rewrite -type_if_cond_type_if_stmt //. Qed.

  Lemma abduct_type_if_prop_case_no_join Δ φ s1 s2 cfg Φ R :
    abduct Δ (case φ (swp s1 cfg Φ) (swp s2 cfg Φ)) R →
    abduct Δ (type_if_prop φ None s1 s2 cfg Φ) R.
  Proof.
    rewrite /type_if_prop /case //.
  Qed.

  Lemma abduct_type_if_prop_case_join Δ Δpers Δmv  φ s1 s2 lb cfg Φ Q R:
    qenvs_move_assumptions_to_assertions Δ Δmv →
    qenvs_create_persistent_copy Δ Δpers →
    clear_universals (∀ P, abduct Δmv (abd_assume CTX (□ (lb ◁ᵦ type_fill_ctx (abd_cont_wrap P); cfg; Φ))%I $ case φ (swp s1 cfg Φ) (swp s2 cfg Φ)) (Q P)) →
    abduct Δpers (Q (type_goto lb cfg Φ)) R →
    abduct Δ (type_if_prop φ (Some lb) s1 s2 cfg Φ) R.
  Proof.
    rewrite /type_if_prop.
    iIntros (Hmv Hpers Habd1 Habd2) "[HΔ HR]".
    rewrite qenvs_ctx_persistent_copy (qenvs_ctx_assumptions_to_assertions Δ).
    rewrite -Hmv -Hpers. iDestruct "HΔ" as "[Hmv Hpers]".
    iDestruct (Habd2 with "[$Hmv $HR]") as "HQ".
    iDestruct (Habd1 with "[$Hpers $HQ]") as "HΘ".
    rewrite /abd_assume /case. iApply "HΘ".
    iModIntro. iApply btr_type_goto.
  Qed.

  (* ASSERT *)
  Lemma abduct_type_assert_bool Δ v ot A s Q Φ R:
    abduct Δ (type_if_cond v A ot (λ φ, abduct_pure φ (swp s Q Φ))) R →
    abduct Δ (type_assert v A ot s Q Φ) R.
  Proof. rewrite /abduct_pure type_assert_type_if_cond //=. Qed.

  (* IF-CONDITIONS *)
  Lemma abduct_type_if_cond_value Δ v w ot1 ot2 Φ R:
    abduct Δ (type_val w (λ u A, type_if_cond u A ot2 (λ φ, abd_assume CTX (v ◁ᵥ valueT u ot1) (Φ φ)))) R →
    abduct Δ (type_if_cond v (valueT w ot1) ot2 Φ) R.
  Proof. rewrite -type_if_cond_value //. Qed.

  Lemma abduct_type_if_cond_match Δ v ot A T B Φ R:
    if_cond_type ot T B →
    unifiable_scheme Δ VAL_TYPE A T B →
    abduct Δ (type_unify (VAL v) RECOVER A T B (λ t, type_if_cond_force v (B t) ot Φ)) R →
    abduct Δ (type_if_cond v A ot Φ) R.
  Proof.
    rewrite /type_if_cond_force /type_if_cond /type_unify. intros ?? Habd. iIntros "Hctx Hv".
    iPoseProof (Habd with "Hctx Hv") as "Hpre".
    rewrite /type_pre_type /type_pre_post. iDestruct "Hpre" as "[%t [Hv Hpre]]".
    iSpecialize ("Hpre" with "Hv"). iApply "Hpre".
  Qed.


  (* rules for actually evaluating the conditions *)
  Global Instance if_cond_type_bool_bool: if_cond_type BoolOp dProp (λ φ, (φ @ boolR u8)%RT) := {}.
  Global Instance if_cond_type_int_bool it: if_cond_type (IntOp it) dProp (λ φ, (φ @ boolR it)%RT) | 1 := {}.
  Global Instance if_cond_type_int_int it: if_cond_type (IntOp it) Z (λ n, (n @ intR it)%RT) | 2 := {}.
  Global Instance if_cond_type_ptr X A:
    bin_op_types (EqOp u8) PtrOp PtrOp X unit A (λ _, nullT) →
    if_cond_type PtrOp X A | 1 := {}.

  Lemma abduct_type_if_cond_force_bool_boolT Δ (v: val) φ (Φ: dProp → iProp Σ) R :
    abduct Δ (abd_assume CTX (v ◁ᵥ φ @ boolR u8) (Φ φ)) R →
    abduct Δ (type_if_cond_force v (φ @ boolR u8)%RT BoolOp Φ) R.
  Proof. rewrite -type_if_cond_force_bool_boolT //. Qed.

  Lemma abduct_type_if_cond_force_int_boolT Δ (v: val) it φ (Φ: dProp → iProp Σ) R :
    abduct Δ (abd_assume CTX (v ◁ᵥ φ @ boolR it) (Φ φ)) R →
    abduct Δ (type_if_cond_force v (φ @ boolR it)%RT (IntOp it) Φ) R.
  Proof. rewrite -type_if_cond_force_int_boolT //. Qed.

  Lemma abduct_type_if_cond_force_int_intT Δ (v: val) n it (Φ: dProp → iProp Σ) R :
    abduct Δ (abd_assume CTX (v ◁ᵥ n @ intR it) (Φ (n ≠ 0)%DP)) R →
    abduct Δ (type_if_cond_force v (n @ intR it)%RT (IntOp it) Φ) R.
  Proof. rewrite -type_if_cond_force_int_intT //. Qed.

  Lemma abduct_type_if_cond_force_ptr Δ (v: val) (A: type Σ) (Φ: dProp → iProp Σ) R :
    abduct Δ (type_null_check v A (λ φ B, abd_assume CTX (v ◁ᵥ B) (Φ φ))) R →
    abduct Δ (type_if_cond_force v A PtrOp Φ) R.
  Proof. rewrite -type_if_cond_force_ptr //. Qed.

End abduction_lemmas.


Global Typeclasses Opaque type_return_fill.

Section abduction_automation.
  Context `{!refinedcG Σ}.

    (* TYPE POST *)
  Global Instance abduct_step_type_post_yield Δ Φ A R :
    AbductStep (abduct Δ (abd_remove_idents $ abd_prune_assumptions $ type_return_fill A (λ B, abd_yield (type_post Φ B))%I) R)
      Δ (type_post Φ A) R.
  Proof. intros ?; by eapply abduct_type_post_yield. Qed.


  (* FUNCTION RETURN *)
  Global Instance abduct_step_type_return Δ v A Q Φ R :
    AbductStep (abduct Δ (Φ v A) R) Δ (type_return v A Q Φ) R.
  Proof. intros ?. by apply abduct_type_return. Qed.

  Global Instance abduct_step_type_unwind_execute Δ Φ v A R :
    AbductStep (abduct Δ (
      type_return_fill A (* we fill the type A (and the context) with local variables before removing them *)
    $ λ B, type_prune_args $
           type_size B
    $ λ n,
        (* before the value [v] goes out of scope, we give it back to the context in case it is used somewhere *)
        ∀ w, abd_assume ASSERT (w ◁ᵥ valueT v n) $ abd_assume ASSUME (v ◁ᵥ B) $ type_post Φ (valueT w n))%I R)
      Δ (type_unwind v A Φ) R.
  Proof. intros ?. by eapply abduct_type_unwind_execute. Qed.

  Global Instance abduct_step_type_return_fill Δ A Φ R :
    AbductStep (abduct Δ (∀ v, abd_assume ASSERT (v ◁ᵥ A) $ type_garbage_collect $ type_fill_ctx  $ type_val v (λ _ B, Φ B)) R)
      Δ (type_return_fill A Φ) R.
  Proof. intros ?. by eapply abduct_type_return_fill. Qed.


  (* FREEING *)
  Global Instance abduct_step_type_free_locs_nil Δ P R:
    AbductStep (abduct Δ P R) Δ (type_free_locs nil P) R | 1.
  Proof. intros ?. by apply abduct_type_free_locs_nil. Qed.

  Global Instance abduct_step_type_free_locs_cons Δ l ly L P R:
  AbductStep (abduct Δ (type_loc l (λ r' A, type_free_loc_typed r' A ly (type_free_locs L P))) R) Δ (type_free_locs ((l, ly)::L) P) R | 1.
  Proof. intros ?. by apply abduct_type_free_locs_cons. Qed.

  Global Instance abduct_step_type_dealloc_all_nil Δ P R:
    AbductStep (abduct Δ P R) Δ (type_dealloc_all nil P) R.
  Proof. intros ?; by apply abduct_type_dealloc_all_nil. Qed.

  Global Instance abduct_step_type_dealloc_all_cons Δ l ly L P R:
    AbductStep (abduct Δ (type_loc l (λ r' A, type_free_loc_typed r' A (ly_size ly) (type_dealloc_all L P))) R)
      Δ (type_dealloc_all ((l, ly)::L) P) R.
  Proof. intros ?. by apply abduct_type_dealloc_all_cons. Qed.

  Global Instance abduct_step_type_free_loc_typed_has_layout Δ l A ly P R :
    Δ ⊨ A `has_size` ly →
    AbductStep (abduct Δ (∀ v, abd_assume CTX (v ◁ᵥ A) P) R) Δ (type_free_loc_typed l A ly P) R | 3.
  Proof. intros ??. by apply abduct_type_free_loc_typed_has_layout. Qed.

  Global Instance abduct_step_type_free_loc_typed_value Δ l v ly P R :
    AbductStep (abduct Δ P R) Δ (type_free_loc_typed l (valueT v ly) ly P) R | 2.
  Proof.
    intros ?. by apply abduct_type_free_loc_typed_value.
  Qed.

  Global Instance abduct_step_type_free_loc_typed_place Δ l r ly P R :
    AbductStep (abduct Δ (type_loc r (λ r' A, type_free_loc_typed r' A ly (abd_assume CTX (l ◁ₗ placeT r') P))) R)
    Δ (type_free_loc_typed l (placeT r) ly P) R | 2.
  Proof. intros ?. by apply abduct_type_free_loc_typed_place. Qed.

  Global Instance abduct_step_type_free_loc_typed_is_var Δ l n A P R:
    exi A →
    AbductStep (abduct Δ (abduct_class (A `has_size` n) P) R) Δ (type_free_loc_typed l A n P) R | 1.
  Proof. intros ??. by apply abduct_type_free_loc_typed_is_var. Qed.

  Global Instance abduct_step_type_free_loc_typed_virtual Δ l A ly P R H:
    AbductStep _ _ _ _ | 2 := abduct_type_free_loc_typed_virtual Δ l A ly P R H.

  Global Instance abduct_step_type_free_loc_typed_struct Δ l sl L LY T P R :
    Simpl (map id (member_locs l sl)) L →
    Simpl (map id (member_sizes sl)) LY →
    AbductStep (abduct Δ (type_free_locs_typed L T LY P) R) Δ (type_free_loc_typed l (structT sl T) (ly_size sl) P) R.
  Proof. intros ???. by eapply abduct_type_free_loc_typed_struct. Qed.

  Global Instance abduct_step_type_free_locs_typed_nil Δ P R:
    AbductStep (abduct Δ P R) Δ (type_free_locs_typed nil nil nil P) R.
  Proof. intros ?. by apply abduct_type_free_locs_typed_nil. Qed.

  Global Instance abduct_step_type_free_locs_typed_cons Δ l ly A L T LY P R:
    AbductStep (abduct Δ (type_free_loc_typed l A ly (type_free_locs_typed L T LY P)) R) Δ (type_free_locs_typed (l :: L) (A :: T) (ly :: LY) P) R.
  Proof. intros ?. by apply abduct_type_free_locs_typed_cons. Qed.

  (* GOTO *)
  Global Instance abduct_step_type_goto Δ Q lb s Φ R:
    ComputeMapLookup Q lb s →
    AbductStep (abduct Δ (swp s Q Φ) R) Δ (type_goto lb Q Φ) R | 50.
  Proof. intros ??. by eapply abduct_type_goto. Qed.

  Global Instance abduct_step_type_goto_btr  Δ Δ' Q lb P Φ R:
    qenvs_find_atom SHALLOW Δ (lb ◁ᵦ P; Q; Φ) Δ' →
    AbductStep (abduct Δ' P R) Δ (type_goto lb Q Φ) R | 1.
  Proof. intros ??. by eapply abduct_type_goto_btr. Qed.

  (* IF-EXPRESSIONS *)
  Global Instance abduct_step_type_if_cond_type_if_expr Δ (v: val) (A: type Σ) e1 e2 ot Φ R:
    AbductStep (abduct Δ (type_if_cond v A ot (λ φ, type_if_expr_prop φ e1 e2 Φ)) R) Δ (type_if_expr ot v A e1 e2 Φ) R.
  Proof. intros ?. by apply abduct_type_if_cond_type_if_expr. Qed.

  Global Instance abduct_step_type_if_expr_prop_type_if Δ Δmv Δpers φ e1 e2 Φ Q R:
    qenvs_move_assumptions_to_assertions Δ Δmv →
    qenvs_create_persistent_copy Δ Δpers →
    AbductStep (clear_universals (∀! Θ, abduct Δmv (case φ (rwp e1 (λ _ A, type_cont Θ A)) (rwp e2 (λ _ A, type_cont Θ A))) (Q Θ)) ∧
                  abduct Δpers (Q (λ A, ∀ v: val, Φ v A)%I) R)
    Δ (type_if_expr_prop φ e1 e2 Φ) R.
  Proof. intros ?? [??]. by eapply abduct_type_if_expr_prop_case. Qed.

  (* LOGICAL AND and LOGICAL OR *)
  Global Instance abduct_step_type_logical_or Δ v ot1 ot2 rit A e Φ R:
    AbductStep (
      abduct Δ (abd_breakpoint (λ Φ, abd_pose_proof None fancy_if $ abd_pose_proof None very_fancy_if $ type_if_expr ot1 v A (b2v true rit) (bool_eta ot2 e rit) (λ v A, type_to_bool v A Φ)) (λ Q, Q (λ A, ∀ w, Φ w A)%I)) R) Δ (type_logical_or ot1 ot2 rit v A e Φ) R.
  Proof. intros ?. by eapply abduct_type_logical_or. Qed.

  Global Instance abduct_step_type_logical_and Δ v ot1 ot2 rit A e Φ R:
    AbductStep (
      abduct Δ (abd_breakpoint (λ Φ, abd_pose_proof None fancy_if $ abd_pose_proof None very_fancy_if $ type_if_expr ot1 v A (bool_eta ot2 e rit) (b2v false rit) (λ v A, type_to_bool v A Φ)) (λ Q, Q (λ A, ∀ w, Φ w A)%I)) R) Δ (type_logical_and ot1 ot2 rit v A e Φ) R.
  Proof. intros ?. by eapply abduct_type_logical_and. Qed.

  (* TYPE BOOL ETA *)
  Global Instance abduct_step_type_bool_eta_type_if_cond Δ ot v A it Φ R:
    AbductStep (abduct Δ (type_if_cond v A ot (λ φ, ∀ w, Φ w (φ @ boolR it)%RT)%I) R)
      Δ (type_bool_eta ot v A it Φ) R.
  Proof. intros ?. by eapply abduct_type_bool_eta_type_if_cond. Qed.

  (* TYPE BOOL *)
  Global Instance abduct_step_type_to_bool_intro Δ v φ it Φ R:
    AbductStep (abduct Δ (Φ (φ @ boolR it)%RT) R) Δ (type_to_bool v (φ @ boolR it)%RT Φ) R | 10.
  Proof. intros ?. by eapply abduct_type_to_bool_intro. Qed.

  Global Instance abduct_step_type_to_bool_eta Δ v (φ: dProp) it Φ R:
    AbductStep (abduct Δ (type_to_bool v (φ @ boolR it)%RT Φ) R) Δ (type_to_bool v (case φ True%DP False%DP @ boolR it)%RT Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_to_bool_eta. Qed.

  Global Instance abduct_step_type_to_bool_if_true_l Δ v (φ ψ: dProp) it Φ R:
    AbductStep (abduct Δ (type_to_bool v ((φ ∨ ψ)%DP @ boolR it)%RT Φ) R) Δ (type_to_bool v (case φ True%DP ψ @ boolR it)%RT Φ) R | 2.
  Proof. intros ?. by eapply abduct_type_to_bool_if_true_l. Qed.

  Global Instance abduct_step_type_to_bool_if_true_r Δ v (φ ψ: dProp) it Φ R:
    AbductStep (abduct Δ (type_to_bool v ((φ → ψ)%DP @ boolR it)%RT Φ) R) Δ (type_to_bool v (case φ ψ True%DP @ boolR it)%RT Φ) R | 2.
  Proof. intros ?. by eapply abduct_type_to_bool_if_true_r. Qed.

  Global Instance abduct_step_type_to_bool_if_false_l Δ v (φ ψ: dProp) it Φ R:
    AbductStep (abduct Δ (type_to_bool v ((¬ φ ∧ ψ)%DP @ boolR it)%RT Φ) R) Δ (type_to_bool v (case φ False%DP ψ @ boolR it)%RT Φ) R | 2.
  Proof. intros ?. by eapply abduct_type_to_bool_if_false_l. Qed.

  Global Instance abduct_step_type_to_bool_if_false_r Δ v (φ ψ: dProp) it Φ R:
    AbductStep (abduct Δ (type_to_bool v ((φ ∧ ψ)%DP @ boolR it)%RT Φ) R) Δ (type_to_bool v (case φ ψ False%DP @ boolR it)%RT Φ) R | 2.
  Proof. intros ?. by eapply abduct_type_to_bool_if_false_r. Qed.


  (* IF-STATEMENTS *)
  Global Instance abduct_step_type_if_cond_type_if_stmt Δ (v: val) (A: type Σ) s1 s2 join ot Q Φ R:
    AbductStep (abduct Δ (type_if_cond v A ot (λ φ, type_if_prop φ join s1 s2 Q Φ)) R) Δ (type_if_stmt v A ot join s1 s2 Q Φ) R.
  Proof. intros ?. by apply abduct_type_if_cond_type_if_stmt. Qed.

  Global Instance abduct_step_type_if_prop_case_no_join Δ φ s1 s2 cfg Φ R :
    AbductStep (abduct Δ (case φ (swp s1 cfg Φ) (swp s2 cfg Φ)) R) Δ (type_if_prop φ None s1 s2 cfg Φ) R.
  Proof. intros ?. by apply abduct_type_if_prop_case_no_join. Qed.

  Global Instance abduct_step_type_if_prop_case_join Δ Δmv Δpers φ s1 s2 lb cfg Φ Q R:
    qenvs_move_assumptions_to_assertions Δ Δmv →
    qenvs_create_persistent_copy Δ Δpers →
    AbductStep
      (clear_universals (∀ P, abduct Δmv (abd_assume CTX (□ (lb ◁ᵦ type_fill_ctx (abd_cont_wrap P); cfg; Φ))%I $ case φ (swp s1 cfg Φ) (swp s2 cfg Φ)) (Q P))
      ∧ abduct Δpers (Q (type_goto lb cfg Φ)) R)
    Δ (type_if_prop φ (Some lb) s1 s2 cfg Φ) R.
  Proof. intros ?? [??]. by eapply abduct_type_if_prop_case_join. Qed.

  (* ASSERT *)
  Global Instance abduct_step_type_assert_bool Δ v ot A s Q Φ R:
    AbductStep (abduct Δ (type_if_cond v A ot (λ φ, abduct_pure φ (swp s Q Φ))) R) Δ (type_assert v A ot s Q Φ) R.
  Proof. intros ?. by apply abduct_type_assert_bool. Qed.

  (* IF-CONDITIONS *)
  Global Instance abduct_step_type_if_cond_value Δ v w ot1 ot2 Φ R:
    AbductStep (abduct Δ (type_val w (λ u A, type_if_cond u A ot2 (λ φ, abd_assume CTX (v ◁ᵥ valueT u ot1) (Φ φ)))) R) Δ (type_if_cond v (valueT w ot1) ot2 Φ) R | 1.
  Proof. intros ?. by apply abduct_type_if_cond_value. Qed.

  Global Instance abduct_step_type_if_cond_match Δ v ot A T B Φ R:
    if_cond_type ot T B →
    unifiable_scheme Δ VAL_TYPE A T B →
    AbductStep (abduct Δ (type_unify (VAL v) RECOVER A T B (λ t, type_if_cond_force v (B t) ot Φ)) R) Δ (type_if_cond v A ot Φ) R | 2.
  Proof. intros ???. by eapply abduct_type_if_cond_match. Qed.

  Global Instance abduct_step_type_if_cond_force_bool_boolT Δ (v: val) φ (Φ: dProp → iProp Σ) R :
    AbductStep (abduct Δ (abd_assume CTX (v ◁ᵥ φ @ boolR u8) (Φ φ)) R) Δ (type_if_cond_force v (φ @ boolR u8)%RT BoolOp Φ) R.
  Proof. intros ?. by apply abduct_type_if_cond_force_bool_boolT. Qed.

  Global Instance abduct_step_type_if_cond_force_int_boolT Δ (v: val) it φ (Φ: dProp → iProp Σ) R :
    AbductStep (abduct Δ (abd_assume CTX (v ◁ᵥ φ @ boolR it) (Φ φ)) R) Δ (type_if_cond_force v (φ @ boolR it)%RT (IntOp it) Φ) R.
  Proof. intros ?. by apply abduct_type_if_cond_force_int_boolT. Qed.

  Global Instance abduct_step_type_if_cond_force_int_intT Δ (v: val) n it (Φ: dProp → iProp Σ) R :
    AbductStep (abduct Δ (abd_assume CTX (v ◁ᵥ n @ intR it) (Φ (n ≠ 0)%DP)) R) Δ (type_if_cond_force v (n @ intR it)%RT (IntOp it) Φ) R | 2.
  Proof. intros ?. by apply abduct_type_if_cond_force_int_intT. Qed.

  Global Instance abduct_step_type_if_cond_force_ptr Δ (v: val) (A: type Σ) (Φ: dProp → iProp Σ) R :
    AbductStep (abduct Δ (type_null_check v A (λ φ B, abd_assume CTX (v ◁ᵥ B) (Φ φ))) R) Δ (type_if_cond_force v A PtrOp Φ) R.
  Proof. intros ?. by apply abduct_type_if_cond_force_ptr. Qed.

End abduction_automation.
