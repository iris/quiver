From quiver.thorium.logic Require Import arithmetic pointers.
From quiver.argon.abduction Require Import abduct proofmode pure.
From quiver.thorium.typing Require Import assume_type unification arithmetic.


Section abduction_lemmas.
  Context `{!refinedcG Σ}.

  (* MOVE *)
  Lemma abuct_type_move_var Δ l A n Φ R:
    exi A →
    abduct Δ (⌜A `has_size` n⌝ ∗ (∀ v, v ◁ᵥ A -∗ Φ v)) R →
    abduct Δ (type_move l A n Φ) R.
  Proof. rewrite -type_move_sized //. Qed.

  Lemma abduct_type_move_value Δ l v n m Φ R:
    abduct Δ (⌜n = m⌝ ∗ Φ v) R →
    abduct Δ (type_move l (valueT v n) m Φ) R.
  Proof. rewrite -type_move_value //. Qed.

  Lemma abduct_type_move_opt Δ l v m Φ R:
    abduct Δ (⌜m = ptr_size⌝ ∗ Φ v) R →
    abduct Δ (type_move l (optT v) m Φ) R.
  Proof. rewrite -type_move_opt //. Qed.

  Lemma abduct_type_move_own Δ l p n A (Φ: val → iProp Σ) R:
    abduct Δ (⌜n = ptr_size⌝ ∗ abd_assume CTX (p ◁ₗ A) (Φ p)) R →
    abduct Δ (type_move l (ownT p A) n Φ) R.
  Proof. rewrite -type_move_own //. Qed.

  Lemma abduct_type_move_place Δ l r n Φ R:
    abduct Δ (type_loc r (λ r' A, abd_assume CTX (l ◁ₗ placeT r') (type_move r' A n Φ))) R →
    abduct Δ (type_move l (placeT r) n Φ) R.
  Proof. rewrite -type_move_place //. Qed.

  Lemma abduct_type_move_has_size Δ l A n m Φ R:
    Δ ⊨ A `has_size` n →
    abduct Δ (⌜n = m⌝ ∗ (∀ v, v ◁ᵥ A -∗ Φ v)) R →
    abduct Δ (type_move l A m Φ) R.
  Proof.
    intros Hs Habd. apply: abduct_qenvs_entails. intros Hsz.
    rewrite -type_move_sized //. iIntros "Hctx".
    iDestruct (Habd with "Hctx") as "[-> $]". by iPureIntro.
  Qed.

  Lemma abuct_type_move_virtual Δ l A n Φ R:
    Δ ⊨ virtual_type A →
    abduct Δ (type_virt (LOC l) A (λ A', type_move l A' n Φ)) R →
    abduct Δ (type_move l A n Φ) R.
  Proof. rewrite -type_move_virtual //. Qed.

  Lemma abduct_type_move_struct_move_all Δ (n: Z) (sl : struct_layout) l ls As ns Φ R:
    Simpl (member_locs l sl) ls →
    Simpl (member_sizes sl) ns →
    abduct Δ (⌜n = ly_size sl⌝ ∗ type_move_all ls As ns (λ vs, ∀ v, abd_simpl (map (λ '(v, n), valueT v n) (zip vs ns)) (λ Bs, v ◁ᵥ structT sl Bs -∗ Φ v)))%I R →
    abduct Δ (type_move l (structT sl As) n Φ) R.
  Proof.
    intros <- <-. rewrite -type_move_struct_move_all //.
  Qed.

  Lemma abduct_type_move_slicesT_move_all Δ Ts Ts' l ls ns Φ R len n :
    Simpl (slices_locs l Ts) ls →
    Simpl (Ts.*2) Ts' →
    Simpl (Ts.*1.*2) ns →
    abduct Δ (⌜n = len⌝ ∗ type_move_all ls Ts' ns (λ vs,
      ∀ v, abd_simpl (map (λ '(v, T), (T.1.1, T.1.2, valueT v T.1.2)) (zip vs Ts)) (λ Bs,
             v ◁ᵥ slicesT len Bs -∗ Φ v)))%I R →
    abduct Δ (type_move l (slicesT len Ts) n Φ) R.
  Proof. rewrite /Simpl/abd_simpl. move => <- <- <-. by rewrite type_move_slicesT_move_all. Qed.

  (* MOVE ALL*)
  Lemma abduct_type_move_all_nil Δ Φ R:
    abduct Δ (Φ nil) R →
    abduct Δ (type_move_all [] [] [] Φ) R.
  Proof. rewrite -type_move_all_nil //. Qed.

  Lemma abduct_type_move_all_cons Δ l ls A As n ns Φ R:
    abduct Δ (type_move l A n (λ v, type_move_all ls As ns (λ vs, Φ (v :: vs)))) R →
    abduct Δ (type_move_all (l :: ls) (A :: As) (n :: ns) Φ) R.
  Proof. rewrite -type_move_all_cons //. Qed.



  (* ADDROF *)
  Lemma abduct_type_addr_of_own Δ (l: loc) A (Ψ: val → type Σ → iProp Σ) R:
    abduct Δ (Ψ l (ownT l A)) R →
    abduct Δ  (type_addr_of l A Ψ) R.
  Proof.
    rewrite type_addr_of_own //.
  Qed.

  (* USE *)
  Lemma abduct_type_use_move Δ ot n l A Φ R :
    Simpl (ly_size (ot_layout ot)) n →
    Δ ⊨ A `has_size` n →
    abduct Δ (∀ v, abd_assume CTX (l ◁ₗ valueT v n) (abd_assume CTX (v ◁ᵥ A) (Φ v (valueT v n)))) R →
    abduct Δ (type_use l A ot Φ) R.
  Proof.
    intros <- ??. apply: abduct_qenvs_entails. intros ?. rewrite -type_use_move //.
  Qed.

  Lemma abduct_type_use_copy Δ ot n l A Ψ R:
    Simpl (ly_size (ot_layout ot)) n →
    Δ ⊨ (Copy A ∧ A `has_size` n) →
    abduct Δ (∀ v, abd_assume CTX (l ◁ₗ A) (abd_assume CTX (v ◁ᵥ A) (Ψ v A))) R →
    abduct Δ (type_use l A ot Ψ) R.
  Proof.
    intros <- Hent ?.
    apply: abduct_qenvs_entails; intros [??].
    rewrite -type_use_copy //.
  Qed.

  Lemma abduct_type_use_value Δ ot n l v Φ R :
    Simpl (ly_size (ot_layout ot)) n →
    abduct Δ (abd_assume CTX (l ◁ₗ valueT v n) (Φ v (valueT v n))) R →
    abduct Δ (type_use l (valueT v n) ot Φ) R.
  Proof. intros <-. rewrite -type_use_value //. Qed.

  Lemma abduct_type_use_place Δ ot l r Φ R:
    abduct Δ (type_loc r (λ r' B, abd_assume CTX (l ◁ₗ placeT r') (type_use r' B ot Φ))) R →
    abduct Δ (type_use l (placeT r) ot Φ) R.
  Proof. rewrite -type_use_place //. Qed.

  (* by default, if we do not know the type, we use the move rule for now *)
  Lemma abduct_type_use_var Δ l ot n A Φ R:
    exi A →
    Simpl (ly_size (ot_layout ot)) n →
    abduct Δ (abduct_class (A `has_size` n) (∀ v, abd_assume CTX (l ◁ₗ valueT v n) (abd_assume CTX (v ◁ᵥ A) (Φ v (valueT v n))))) R →
    abduct Δ (type_use l A ot Φ) R.
  Proof.
    rewrite /abduct_class. intros Hvar <- Habd.
    iIntros "Ctx". iDestruct (Habd with "Ctx") as "[% Hcont]".
    by iApply type_use_move.
  Qed.

  Lemma abduct_type_use_virtual Δ l A ot Ψ R:
    Δ ⊨ virtual_type A →
    abduct Δ (type_virt (LOC l) A (λ A', type_use l A' ot Ψ)) R →
    abduct Δ (type_use l A ot Ψ) R.
  Proof. rewrite -type_use_virtual //. Qed.

  (* LOAD *)
  Lemma abduct_type_load_use Δ l A Φ R :
    abduct Δ (type_use l A PtrOp (λ v B, type_val_to_loc v B Φ)) R →
    abduct Δ (type_load l A Φ) R.
  Proof.
    rewrite -type_load_use //.
  Qed.

  (* ASSIGN *)
  Lemma abduct_type_assign_move Δ l A v B ot n s Q Ψ R:
    Simpl (ly_size (ot_layout ot)) n →
    Δ ⊨ (A `has_size` n ∧ B `has_size` n) →
    abduct Δ (abd_assume CTX (l ◁ₗ valueT v (ly_size (ot_layout ot))) (abd_assume CTX (v ◁ᵥ B) (∀ w, abd_assume CTX (w ◁ᵥ A) (swp s Q Ψ)))) R →
    abduct Δ (type_assign l A v B ot s Q Ψ) R.
  Proof.
    intros <- Hent ?. apply: abduct_qenvs_entails. intros [??].
    rewrite -type_assign_move //.
  Qed.

  Lemma abduct_type_assign_copy Δ l A v B ot n s Q Ψ R:
    Simpl (ly_size (ot_layout ot)) n →
    Δ ⊨ (Copy B ∧ A `has_size` n ∧ B `has_size` n) →
    abduct Δ (abd_assume CTX (l ◁ₗ B) (abd_assume CTX (v ◁ᵥ B) (∀ w, abd_assume CTX (w ◁ᵥ A) (swp s Q Ψ)))) R →
    abduct Δ (type_assign l A v B ot s Q Ψ) R.
  Proof.
    intros <- Hent ?. apply: abduct_qenvs_entails. intros [? [? ?]].
    rewrite -type_assign_copy //.
  Qed.

  Lemma abduct_type_assign_place Δ l r v A ot s Q Φ R:
    abduct Δ (type_loc r (λ r' B, abd_assume CTX (l ◁ₗ placeT r') (type_assign r' B v A ot s Q Φ))) R →
    abduct Δ (type_assign l (placeT r) v A ot s Q Φ) R.
  Proof. rewrite type_assign_place //. Qed.

  (* by default, if we immediately assign to an unknown type,
     we require it to be of the `any` type. *)
  Lemma abduct_type_assign_var Δ l A v B ot n s Q Φ R:
    exi A →
    Simpl (ly_size (ot_layout ot)) n →
    abduct Δ (abduct_pure (A `is_ty` anyT n) (type_assign l (anyT n) v B ot s Q Φ)) R →
    abduct Δ (type_assign l A v B ot s Q Φ) R.
  Proof.
    rewrite /abduct_pure. iIntros (_ <- Habd) "Hctx".
    iDestruct (Habd with "Hctx") as "[%Heq Hctx]".
    rewrite /type_assign. by setoid_rewrite Heq.
  Qed.

  Lemma abduct_type_assign_type_virt Δ l A v B ot s Q Φ R:
    Δ ⊨ virtual_type A →
    abduct Δ (type_virt (LOC l) A (λ A', type_assign l A' v B ot s Q Φ)) R →
    abduct Δ (type_assign l A v B ot s Q Φ) R.
  Proof. rewrite -type_assign_type_virt //. Qed.


  (* VAL-TO-LOC *)
  Lemma abduct_type_val_to_loc_own Δ v (p: loc) A R Φ:
    abduct Δ (abd_assume CTX (v ◁ᵥ valueT p ptr_size) (Φ p A)) R →
    abduct Δ (type_val_to_loc v (ownT p A) Φ) R.
  Proof. rewrite type_val_to_loc_own //. Qed.

  Lemma abduct_type_val_to_loc_value_loc Δ v (p: loc) R Φ:
    abduct Δ (abd_assume CTX (v ◁ᵥ valueT p ptr_size) (Φ p (placeT p))) R →
    abduct Δ (type_val_to_loc v (valueT p ptr_size) Φ) R.
  Proof. rewrite type_val_to_loc_value_loc //. Qed.

  Lemma abduct_type_val_to_loc_value_val Δ v w R Φ:
    abduct Δ (type_val w (λ u A, type_val_to_loc u A (λ p B, abd_assume CTX (v ◁ᵥ valueT p ptr_size) (Φ p B))))%I R →
    abduct Δ (type_val_to_loc v (valueT w ptr_size) Φ) R.
  Proof. rewrite type_val_to_loc_value_val //. Qed.

  Lemma abduct_type_val_to_loc_var Δ v (A: type Σ) R Φ:
    exi A →
    abduct Δ (∃ p B, abduct_pure (A `is_ty` (ownT p B)) (v ◁ᵥ valueT p ptr_size -∗ Φ p B)) R →
    abduct Δ (type_val_to_loc v A Φ) R.
  Proof.
    rewrite /abduct_pure. iIntros (_ Habd) "Ctx".
    iPoseProof (Habd with "Ctx") as "(%p & %B & %Heq & Hcont)".
    rewrite /type_val_to_loc.
    setoid_rewrite Heq.
    iPoseProof (type_val_to_loc_own) as "Hent".
    rewrite /type_val_to_loc.
    by iApply "Hent".
  Qed.


  Lemma abduct_type_val_to_loc_optional Δ v (A: type Σ) Φ R :
    unifiable_scheme Δ VAL_TYPE A (dProp * type Σ)%type (λ x, optionalT x.1 x.2) →
    abduct Δ (type_unify (VAL v) RECOVER A (dProp * type Σ) (λ x, optionalT x.1 x.2) (λ x, ⌜x.1⌝ ∗ type_val_to_loc v x.2 Φ)%I) R →
    abduct Δ (type_val_to_loc v A Φ) R.
  Proof. intros _. rewrite -type_val_to_loc_optional //. Qed.

  Lemma abduct_type_val_to_loc_own_cast Δ v (A: type Σ) Φ R :
    unifiable_scheme Δ VAL_TYPE A (loc * type Σ)%type (λ x, ownT x.1 x.2) →
    abduct Δ (type_unify (VAL v) RECOVER A (loc * type Σ) (λ x, ownT x.1 x.2) (λ x, type_val_to_loc v (ownT x.1 x.2) Φ)%I) R →
    abduct Δ (type_val_to_loc v A Φ) R.
  Proof. intros _. rewrite -type_val_to_loc_own_cast //. Qed.

  Lemma abduct_type_val_to_loc_opt Δ v w Φ R:
    abduct Δ (abd_assume CTX (v ◁ᵥ optT w) (type_val w (λ u A, type_val_to_loc u A Φ))) R →
    abduct Δ (type_val_to_loc v (optT w) Φ) R.
  Proof. rewrite -type_val_to_loc_opt //. Qed.

  Lemma abduct_type_val_to_loc_virt Δ v A Φ R:
    Δ ⊨ virtual_type A →
    abduct Δ (type_virt (VAL v) A (λ B, type_val_to_loc v B Φ)) R →
    abduct Δ (type_val_to_loc v A Φ) R.
  Proof.
    intros _. rewrite -type_val_to_loc_virt //.
  Qed.

  (* pointer arithmetic *)
  Lemma abduct_type_bin_op_ptr_offset_op Δ ly v1 v2 A1 A2 ot1 ot2 Φ R:
    abduct Δ (type_val_to_loc v2 A2 (λ l A2', type_at_offset l v1 A2' A1 ly ot2 ot1 (λ r A, Φ (r: val) (ownT r A)))) R →
    abduct Δ (type_bin_op (PtrOffsetOp ly) ot1 ot2 v1 A1 v2 A2 Φ) R.
  Proof.
    rewrite -type_bin_op_ptr_offset_op //.
  Qed.

  Global Instance abduct_step_type_bin_op_ptr_offset_op Δ ly v1 v2 A1 A2 ot1 ot2 Φ R:
    AbductStep (abduct Δ (type_val_to_loc v2 A2 (λ l A2', type_at_offset l v1 A2' A1 ly ot2 ot1 (λ r A, Φ (r: val) (ownT r A)))) R)
      Δ (type_bin_op (PtrOffsetOp ly) ot1 ot2 v1 A1 v2 A2 Φ) R.
  Proof. intros ?. by eapply abduct_type_bin_op_ptr_offset_op. Qed.


  (* POINTER CASTS *)
  Lemma abduct_type_un_op_cast_ptr Δ v A Φ R:
    Δ ⊨ PtrType A →
    abduct Δ (Φ v A) R →
    abduct Δ (type_un_op (CastOp PtrOp) PtrOp v A Φ) R.
  Proof.
    intros ??. apply: abduct_qenvs_entails.
    intros ?. by rewrite -type_un_op_cast_ptr.
  Qed.

  Lemma abduct_type_un_op_cast_ptr_loc Δ (l: loc) A Φ R:
    abduct Δ (Φ (l: val) A) R →
    abduct Δ (type_un_op (CastOp PtrOp) PtrOp l  A Φ) R.
  Proof.
    intros ?. rewrite -type_un_op_cast_ptr_loc //.
  Qed.


  Global Instance un_op_cast_int_null_ptr it : un_op_types (CastOp PtrOp) (IntOp it) unit (λ _, intT it 0) | 1 := {}.

  Global Instance unop_cast_int_null_ptr_typing it :
    un_op_typing_rule True (CastOp PtrOp) (IntOp it) (intT it 0) nullT.
  Proof.
    split.
    - apply _.
    - intros _ v Ψ. iIntros "Hcont".
      rewrite /type_un_op_force -type_un_op_cast_null_ptr.
      rewrite /abd_assume. iIntros (w) "Hw". by iApply "Hcont".
  Qed.



  (* NULL-POINTER COMPARISON *)
  Global Instance bin_op_ne_opt_null it : bin_op_types (NeOp it) PtrOp PtrOp val unit (λ x, optT x) (λ _, nullT) | 1 := {}.
  Global Instance bin_op_ne_null_opt it : bin_op_types (NeOp it) PtrOp PtrOp unit val (λ _, nullT) (λ x, optT x) | 1 := {}.
  Global Instance bin_op_ne_optional_null it : bin_op_types (NeOp it) PtrOp PtrOp (dProp * type Σ)%type unit (λ x, optionalT x.1 x.2) (λ _, nullT) | 2 := {}.
  Global Instance bin_op_ne_null_optional it : bin_op_types (NeOp it) PtrOp PtrOp unit (dProp * type Σ)%type (λ _, nullT) (λ x, optionalT x.1 x.2) | 2 := {}.
  Global Instance bin_op_ne_own_null it : bin_op_types (NeOp it) PtrOp PtrOp (loc * type Σ)%type unit (λ x, ownT x.1 x.2) (λ _, nullT) | 3 := {}.
  Global Instance bin_op_ne_null_own it : bin_op_types (NeOp it) PtrOp PtrOp unit (loc * type Σ)%type (λ _, nullT) (λ x, ownT x.1 x.2) | 3 := {}.
  Global Instance bin_op_ne_null_null it: bin_op_types (NeOp it) PtrOp PtrOp unit unit (λ _, nullT) (λ _, nullT) | 10 := {}.

  Global Instance bin_op_eq_opt_null it : bin_op_types (EqOp it) PtrOp PtrOp val unit (λ x, optT x) (λ _, nullT) | 1 := {}.
  Global Instance bin_op_eq_null_opt it : bin_op_types (EqOp it) PtrOp PtrOp unit val (λ _, nullT) (λ x, optT x) | 1 := {}.
  Global Instance bin_op_eq_optional_null it : bin_op_types (EqOp it) PtrOp PtrOp (dProp * type Σ)%type unit (λ x, optionalT x.1 x.2) (λ _, nullT) | 2 := {}.
  Global Instance bin_op_eq_null_optional it : bin_op_types (EqOp it) PtrOp PtrOp unit (dProp * type Σ)%type (λ _, nullT) (λ x, optionalT x.1 x.2) | 2 := {}.
  Global Instance bin_op_eq_own_null it : bin_op_types (EqOp it) PtrOp PtrOp (loc * type Σ)%type unit (λ x, ownT x.1 x.2) (λ _, nullT)| 3 := {}.
  Global Instance bin_op_eq_null_own it : bin_op_types (EqOp it) PtrOp PtrOp unit (loc * type Σ)%type (λ _, nullT) (λ x, ownT x.1 x.2) | 3 := {}.
  Global Instance bin_op_eq_null_null it: bin_op_types (EqOp it) PtrOp PtrOp unit unit (λ _, nullT) (λ _, nullT) | 10 := {}.


  Global Instance binop_ne_null_typing_custom A it Φ:
    bin_op_typing_rule_custom (NeOp it) PtrOp PtrOp A nullT Φ
  $ λ v w, type_null_check v A (λ φ B, abd_assume CTX (v ◁ᵥ B) (abd_assume CTX (w ◁ᵥ nullT) (∀ u, Φ u (φ @ boolR it)%RT))).
  Proof.
    rewrite /bin_op_typing_rule_custom /type_bin_op_force.
    intros v1 v2. rewrite -type_null_check_type_binop_ne_null //.
    rewrite /type_null_check /abd_assume.
    iIntros "(%φ & %B & Hw & Hx)". iExists φ, B. iFrame.
    iIntros "Hv1 Hv2". iApply ("Hx" with "Hv1 Hv2").
  Qed.

  Global Instance binop_eq_null_typing_custom A it Φ:
    bin_op_typing_rule_custom (EqOp it) PtrOp PtrOp A nullT Φ
  $ λ v w, type_null_check v A (λ φ B, abd_assume CTX (v ◁ᵥ B) (abd_assume CTX (w ◁ᵥ nullT) (∀ u, Φ u ((¬ φ)%DP @ boolR it)%RT))).
  Proof.
    rewrite /bin_op_typing_rule_custom /type_bin_op_force.
    intros v1 v2. rewrite -type_null_check_type_binop_eq_null //.
    rewrite /type_null_check /abd_assume.
    iIntros "(%φ & %B & Hw & Hx)". iExists φ, B. iFrame.
    iIntros "Hv1 Hv2". iApply ("Hx" with "Hv1 Hv2").
  Qed.

  Global Instance binop_null_ne_typing_custom A it Φ:
    bin_op_typing_rule_custom (NeOp it) PtrOp PtrOp nullT A Φ
  $ λ v w, type_null_check w A (λ φ B, abd_assume CTX (v ◁ᵥ nullT) (abd_assume CTX (w ◁ᵥ B) (∀ u, Φ u (φ @ boolR it)%RT))).
  Proof.
    rewrite /bin_op_typing_rule_custom /type_bin_op_force.
    intros v1 v2. rewrite -type_null_check_type_binop_null_ne //.
    rewrite /type_null_check /abd_assume.
    iIntros "(%φ & %B & Hw & Hx)". iExists φ, B. iFrame.
    iIntros "Hv1 Hv2". iApply ("Hx" with "Hv1 Hv2").
  Qed.

  Global Instance binop_null_eq_typing_custom A it Φ:
    bin_op_typing_rule_custom (EqOp it) PtrOp PtrOp nullT A Φ
  $ λ v w, type_null_check w A (λ φ B, abd_assume CTX (v ◁ᵥ nullT) (abd_assume CTX (w ◁ᵥ B) (∀ u, Φ u ((¬ φ)%DP @ boolR it)%RT))).
  Proof.
    rewrite /bin_op_typing_rule_custom /type_bin_op_force.
    intros v1 v2. rewrite -type_null_check_type_binop_null_eq //.
    rewrite /type_null_check /abd_assume.
    iIntros "(%φ & %B & Hw & Hx)". iExists φ, B. iFrame.
    iIntros "Hv1 Hv2". iApply ("Hx" with "Hv1 Hv2").
  Qed.


  (* NULL-CHECK *)
  Lemma abduct_type_null_check_non_null_val Δ A v Φ R:
    Δ ⊨ NonNullVal A →
    abduct Δ (Φ True%DP A) R →
    abduct Δ (type_null_check v A Φ) R.
  Proof.
    intros ? Habd; apply: abduct_qenvs_entails. intros HNV.
    rewrite -type_null_check_non_null_val //.
    iIntros "Ctx". iSplit; first done.
    iApply (Habd with "Ctx").
  Qed.

  Lemma abduct_type_null_check_optional Δ φ A v Φ R:
    Δ ⊨ NonNullVal A →
    abduct Δ (Φ φ (optionalT φ A)) R →
    abduct Δ (type_null_check v (optionalT φ A) Φ) R.
  Proof.
    intros ? Habd; apply: abduct_qenvs_entails. intros HNV.
    rewrite -type_null_check_option_non_null_val //.
    iIntros "Ctx".
    iSplit; first done.
    iApply (Habd with "Ctx").
  Qed.

  Lemma abduct_type_null_check_optional_default Δ A v φ Φ R:
    abduct Δ (abduct_class (NonNullVal A) (Φ φ (optionalT φ A))) R →
    abduct Δ (type_null_check v (optionalT φ A) Φ) R.
  Proof.
    rewrite /abduct_class type_null_check_option_non_null_val //.
  Qed.

  Lemma abduct_type_null_check_owned Δ v p A Φ R:
    abduct Δ (⌜NonNull A⌝ ∗ Φ True%DP (ownT p A)) R →
    abduct Δ (type_null_check v (ownT p A) Φ) R.
  Proof. by rewrite -type_null_check_owned //. Qed.

  Lemma abduct_type_null_check_null Δ v Φ R:
    abduct Δ (Φ False%DP nullT) R →
    abduct Δ (type_null_check v nullT Φ) R.
  Proof. rewrite -type_null_check_null //. Qed.

  Lemma abduct_type_null_check_opt Δ v w Φ R:
    abduct Δ (Φ (not_null w) (optT w)) R →
    abduct Δ (type_null_check v (optT w) Φ) R.
  Proof. rewrite -type_null_check_opt //. Qed.

End abduction_lemmas.



Section abduction_automation.
  Context `{!refinedcG Σ}.

  (* MOVE *)
  Global Instance abduct_step_type_move_has_size Δ l A n m Φ R:
    Δ ⊨ A `has_size` n →
    AbductStep (abduct Δ (⌜n = m⌝ ∗ (∀ v, v ◁ᵥ A -∗ Φ v)) R)
      Δ (type_move l A m Φ) R | 5.
  Proof. intros ??. by eapply abduct_type_move_has_size. Qed.

  Global Instance abduct_step_type_move_var Δ l A n Φ R:
    exi A →
    AbductStep (abduct Δ (⌜A `has_size` n⌝ ∗ (∀ v, v ◁ᵥ A -∗ Φ v)) R)
      Δ (type_move l A n Φ) R | 10.
  Proof. intros ??. by eapply abuct_type_move_var. Qed.

  Global Instance abduct_step_type_move_value Δ l v n m Φ R:
    AbductStep (abduct Δ (⌜n = m⌝ ∗ Φ v) R)
      Δ (type_move l (valueT v n) m Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_move_value. Qed.

  Global Instance abduct_step_type_move_opt Δ l v m Φ R:
    AbductStep (abduct Δ (⌜m = ptr_size⌝ ∗ Φ v) R )
      Δ (type_move l (optT v) m Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_move_opt. Qed.

  Global Instance abduct_step_type_move_own Δ l p n A (Φ: val → iProp Σ) R:
    AbductStep (abduct Δ (⌜n = ptr_size⌝ ∗ abd_assume CTX (p ◁ₗ A) (Φ p)) R)
      Δ (type_move l (ownT p A) n Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_move_own. Qed.

  Global Instance abduct_step_type_move_place Δ l r n Φ R:
    AbductStep (abduct Δ (type_loc r (λ r' A, abd_assume CTX (l ◁ₗ placeT r') (type_move r' A n Φ))) R)
      Δ (type_move l (placeT r) n Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_move_place. Qed.

  Global Instance abuct_step_type_move_virtual Δ l A n Φ R:
    Δ ⊨ virtual_type A →
    AbductStep (abduct Δ (type_virt (LOC l) A (λ A', type_move l A' n Φ)) R)
      Δ (type_move l A n Φ) R | 8.
  Proof. intros ??. by eapply abuct_type_move_virtual. Qed.

  Global Instance abduct_step_type_move_struct_move_all Δ (n: Z) (sl : struct_layout) l ls As ns Φ R:
    Simpl (member_locs l sl) ls →
    Simpl (member_sizes sl) ns →
    AbductStep (abduct Δ (⌜n = ly_size sl⌝ ∗ type_move_all ls As ns (λ vs, ∀ v, abd_simpl (map (λ '(v, n), valueT v n) (zip vs ns)) (λ Bs, v ◁ᵥ structT sl Bs -∗ Φ v)))%I R)
      Δ (type_move l (structT sl As) n Φ) R.
  Proof. intros ???. eapply abduct_type_move_struct_move_all; eauto. Qed.

  Global Instance abduct_step_type_move_slicesT_move_all Δ Ts Ts' l ls ns Φ R len n H1 H2 H3:
    AbductStep _ _ _ _ := abduct_type_move_slicesT_move_all Δ Ts Ts' l ls ns Φ R len n H1 H2 H3.

  (* MOVE ALL *)
  Global Instance abduct_step_type_move_all_nil Δ Φ R:
    AbductStep (abduct Δ (Φ nil) R) Δ (type_move_all [] [] [] Φ) R.
  Proof. intros ?. by eapply abduct_type_move_all_nil. Qed.

  Global Instance abduct_step_type_move_all_cons Δ l ls A As n ns Φ R:
    AbductStep (abduct Δ (type_move l A n (λ v, type_move_all ls As ns (λ vs, Φ (v :: vs)))) R)
      Δ (type_move_all (l :: ls) (A :: As) (n :: ns) Φ) R.
  Proof. intros ?. by eapply abduct_type_move_all_cons. Qed.

  (* ADDROF *)
  Global Instance abduct_step_type_addr_of_own Δ (l: loc) A (Ψ: val → type Σ → iProp Σ) R:
    AbductStep (abduct Δ (Ψ l (ownT l A)) R) Δ  (type_addr_of l A Ψ) R.
  Proof. intros ?. by apply abduct_type_addr_of_own. Qed.

  (* USE *)
  Global Instance abduct_step_type_use_move Δ ot n l A Φ R :
    Simpl (ly_size (ot_layout ot)) n →
    Δ ⊨ A `has_size` n →
    AbductStep (abduct Δ (∀ v, l ◁ₗ valueT v n -∗ v ◁ᵥ A -∗ Φ v (valueT v n)) R) Δ (type_use l A ot Φ) R | 10.
  Proof. intros ???. by eapply abduct_type_use_move. Qed.

  Global Instance abduct_step_type_use_copy Δ ot n l A Ψ R:
    Simpl (ly_size (ot_layout ot)) n →
    Δ ⊨ (Copy A ∧ A `has_size` n) →
    AbductStep (abduct Δ (∀ v, l ◁ₗ A -∗ v ◁ᵥ A -∗ Ψ v A) R) Δ (type_use l A ot Ψ) R | 8.
  Proof. intros ???. by eapply abduct_type_use_copy. Qed.

  Global Instance abduct_step_type_use_value Δ ot n l v Φ R :
    Simpl (ly_size (ot_layout ot)) n →
    AbductStep (abduct Δ (l ◁ₗ valueT v n -∗ Φ v (valueT v n)) R) Δ (type_use l (valueT v n) ot Φ) R | 1.
  Proof. intros ??. by eapply abduct_type_use_value. Qed.

  Global Instance abduct_step_type_use_place Δ ot l r Φ R:
    AbductStep (abduct Δ (type_loc r (λ r' B, abd_assume CTX (l ◁ₗ placeT r') (type_use r' B ot Φ))) R) Δ (type_use l (placeT r) ot Φ) R | 1.
  Proof. intros ?. by apply abduct_type_use_place. Qed.

  Global Instance abduct_step_type_use_var Δ l ot n A Φ R:
    exi A →
    Simpl (ly_size (ot_layout ot)) n →
    AbductStep (abduct Δ (abduct_class (A `has_size` n) (∀ v, l ◁ₗ valueT v n -∗ v ◁ᵥ A -∗ Φ v (valueT v n))) R) Δ (type_use l A ot Φ) R | 12.
  Proof. intros ???. by eapply abduct_type_use_var. Qed.

  (* other use instances: for accessing raw pointers in [datatypes] *)

  (* This should be after use_move and use_copy to avoid unfolding when possible. *)
  Global Instance abduct_step_type_use_virtual Δ l A ot Ψ R:
    Δ ⊨ virtual_type A →
    AbductStep (abduct Δ (type_virt (LOC l) A (λ A', type_use l A' ot Ψ)) R)
      Δ (type_use l A ot Ψ) R | 11.
  Proof. intros ??. by eapply abduct_type_use_virtual. Qed.

  (* LOAD *)
  Global Instance abduct_step_type_load_use Δ l A Φ R :
    AbductStep (abduct Δ (type_use l A PtrOp (λ v B, type_val_to_loc v B Φ)) R) Δ (type_load l A Φ) R.
  Proof. intros ?. by apply abduct_type_load_use. Qed.

  (* ASSIGN *)
  Global Instance abduct_step_type_assign_move Δ l A v B ot n s Q Ψ R:
    Simpl (ly_size (ot_layout ot)) n →
    Δ ⊨ (A `has_size` n ∧ B `has_size` n) →
    AbductStep (abduct Δ (abd_assume CTX (l ◁ₗ valueT v (ly_size (ot_layout ot))) ((abd_assume CTX (v ◁ᵥ B) (∀ w, abd_assume CTX (w ◁ᵥ A) (swp s Q Ψ))))) R)
      Δ (type_assign l A v B ot s Q Ψ) R | 10.
  Proof. intros ???. by eapply abduct_type_assign_move. Qed.

  Global Instance abduct_step_type_assign_copy Δ l A v B ot n s Q Ψ R:
    Simpl (ly_size (ot_layout ot)) n →
    Δ ⊨ (Copy B ∧ A `has_size` n ∧ B `has_size` n) →
    AbductStep (abduct Δ (abd_assume CTX (l ◁ₗ B) (abd_assume CTX (v ◁ᵥ B) (∀ w, abd_assume CTX (w ◁ᵥ A) (swp s Q Ψ)))) R) Δ (type_assign l A v B ot s Q Ψ) R | 8.
  Proof. intros ???. by eapply abduct_type_assign_copy. Qed.

  Global Instance abduct_step_type_assign_place Δ l r v A ot s Q Φ R:
    AbductStep (abduct Δ (type_loc r (λ r' B, abd_assume CTX (l ◁ₗ placeT r') (type_assign r' B v A ot s Q Φ))) R)  Δ (type_assign l (placeT r) v A ot s Q Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_assign_place. Qed.

  Global Instance abduct_step_type_assign_var Δ l A v B ot n s Q Φ R:
    exi A →
    Simpl (ly_size (ot_layout ot)) n →
    AbductStep (abduct Δ (abduct_pure (A `is_ty` anyT n) (type_assign l (anyT n) v B ot s Q Φ)) R) Δ (type_assign l A v B ot s Q Φ) R | 12.
  Proof. intros ???. by eapply abduct_type_assign_var. Qed.

  Global Instance abduct_step_type_assign_type_virt Δ l A v B ot s Q Φ R:
    Δ ⊨ virtual_type A →
    AbductStep (abduct Δ (type_virt (LOC l) A (λ A', type_assign l A' v B ot s Q Φ)) R)
      Δ (type_assign l A v B ot s Q Φ) R | 10.
  Proof. intros ??. by eapply abduct_type_assign_type_virt. Qed.

  (* other assign instances: for accessing raw pointers in [datatypes] *)

  (* VAL-TO-LOC *)
  Global Instance abduct_step_type_val_to_loc_own Δ v (p: loc) A R Φ:
    AbductStep (abduct Δ (abd_assume CTX (v ◁ᵥ valueT p ptr_size) (Φ p A)) R) Δ (type_val_to_loc v (ownT p A) Φ) R | 2.
  Proof. intros ?; by eapply abduct_type_val_to_loc_own. Qed.

  Global Instance abduct_step_type_val_to_loc_value_loc Δ v (p: loc) R Φ:
    AbductStep (abduct Δ (abd_assume CTX (v ◁ᵥ valueT p ptr_size) (Φ p (placeT p))) R) Δ (type_val_to_loc v (valueT p ptr_size) Φ) R | 2.
  Proof. intros ?; by eapply abduct_type_val_to_loc_value_loc. Qed.

  Global Instance abduct_step_type_val_to_loc_value_val Δ v w R Φ:
    AbductStep (abduct Δ (type_val w (λ u A, type_val_to_loc u A (λ p B, abd_assume CTX (v ◁ᵥ valueT p ptr_size) (Φ p B))))%I R) Δ (type_val_to_loc v (valueT w ptr_size) Φ) R | 3.
  Proof. intros ?; by eapply abduct_type_val_to_loc_value_val. Qed.

  Global Instance abduct_step_type_val_to_loc_var Δ v A R Φ:
    exi A →
    AbductStep (abduct Δ (∃ p B, abduct_pure (A `is_ty` (ownT p B)) (abd_assume CTX (v ◁ᵥ valueT p ptr_size) (Φ p B))) R) Δ (type_val_to_loc v A Φ) R | 12.
  Proof. intros ??; by eapply abduct_type_val_to_loc_var. Qed.

  Global Instance abduct_step_type_val_to_loc_own_cast Δ v (A: type Σ) Φ R :
    unifiable_scheme Δ VAL_TYPE A (loc * type Σ)%type (λ x, ownT x.1 x.2) →
    AbductStep (abduct Δ (type_unify (VAL v) RECOVER A (loc * type Σ) (λ x, ownT x.1 x.2) (λ x, type_val_to_loc v (ownT x.1 x.2) Φ)%I) R) Δ (type_val_to_loc v A Φ) R | 14.
  Proof.
    intros ??. by eapply abduct_type_val_to_loc_own_cast.
  Qed.

  Global Instance abduct_step_type_val_to_loc_optional Δ v (A: type Σ) Φ R :
    unifiable_scheme Δ VAL_TYPE A (dProp * type Σ)%type (λ x, optionalT x.1 x.2) →
    AbductStep (abduct Δ (type_unify (VAL v) RECOVER A (dProp * type Σ) (λ x, optionalT x.1 x.2) (λ x, ⌜x.1⌝ ∗ type_val_to_loc v x.2 Φ)%I) R) Δ (type_val_to_loc v A Φ) R | 14.
  Proof.
    intros ??. by eapply abduct_type_val_to_loc_optional.
  Qed.

  Global Instance abduct_step_type_val_to_loc_opt Δ v w Φ R:
    AbductStep (abduct Δ (abd_assume CTX (v ◁ᵥ optT w) (type_val w (λ u A, type_val_to_loc u A Φ))) R)
      Δ (type_val_to_loc v (optT w) Φ) R.
  Proof. intros ?. by eapply abduct_type_val_to_loc_opt. Qed.

  Global Instance abduct_step_type_val_to_loc_virt Δ v A Φ R:
    Δ ⊨ virtual_type A →
    AbductStep (abduct Δ (type_virt (VAL v) A (λ B, type_val_to_loc v B Φ)) R)
      Δ (type_val_to_loc v A Φ) R | 10.
  Proof. intros ??. by eapply abduct_type_val_to_loc_virt. Qed.

  (* PTR-TO-PTR CAST *)
  Global Instance abduct_step_type_un_op_cast_ptr_loc Δ (l: loc) A Φ R:
    AbductStep (abduct Δ (Φ (l: val) A) R)
      Δ (type_un_op (CastOp PtrOp) PtrOp l A Φ) R | 1.
  Proof. intros ?. by eapply abduct_type_un_op_cast_ptr_loc. Qed.

  Global Instance abduct_step_type_un_op_cast_ptr Δ v A Φ R:
    Δ ⊨ PtrType A →
    AbductStep (abduct Δ (Φ v A) R)
      Δ (type_un_op (CastOp PtrOp) PtrOp v A Φ) R | 2.
  Proof. intros ??. by eapply abduct_type_un_op_cast_ptr. Qed.


  (* NULL CHECK *)
  Global Instance abduct_step_type_null_check_non_null_val Δ A v Φ R:
    Δ ⊨ NonNullVal A →
    AbductStep (abduct Δ (Φ True%DP A) R) Δ (type_null_check v A Φ) R.
  Proof. intros ??; by eapply abduct_type_null_check_non_null_val. Qed.

  Global Instance abduct_step_type_null_check_optional Δ A v φ Φ R:
    Δ ⊨ NonNullVal A →
    AbductStep (abduct Δ (Φ φ (optionalT φ A)) R) Δ (type_null_check v (optionalT φ A) Φ) R.
  Proof. intros ??; by eapply abduct_type_null_check_optional. Qed.

  Global Instance abduct_step_type_null_check_owned Δ v p A Φ R:
    AbductStep (abduct Δ (⌜NonNull A⌝ ∗ Φ True%DP (ownT p A)) R) Δ (type_null_check v (ownT p A) Φ) R | 10.
  Proof.
    intros ?. by eapply abduct_type_null_check_owned.
  Qed.

  Global Instance abduct_step_type_null_check_optional_default Δ A φ v Φ R:
    AbductStep (abduct Δ (abduct_class (NonNullVal A) (Φ φ (optionalT φ A))) R) Δ (type_null_check v (optionalT φ A) Φ) R.
  Proof.
    intros ?; by eapply abduct_type_null_check_optional_default.
  Qed.

  Global Instance abduct_step_type_null_check_null Δ v Φ R:
    AbductStep (abduct Δ (Φ False%DP nullT) R)
      Δ (type_null_check v nullT Φ) R.
  Proof. intros ?. by eapply abduct_type_null_check_null. Qed.

  Global Instance abduct_step_type_null_check_opt Δ v w Φ R:
    AbductStep (abduct Δ (Φ (not_null w) (optT w)) R)
      Δ (type_null_check v (optT w) Φ) R.
  Proof. intros ?. by eapply abduct_type_null_check_opt. Qed.

End abduction_automation.
