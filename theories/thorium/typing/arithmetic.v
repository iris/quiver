From quiver.thorium.logic Require Import arithmetic.
From quiver.argon.abduction Require Import abduct proofmode pure.
From quiver.thorium.typing Require Export unification.

Section prepare_arithmetic_operations.
  Context `{!refinedcG Σ}.

  Class bin_op_types (op: bin_op) (ot1 ot2: op_type) (T1 T2: Type) (A1: T1 → type Σ) (A2: T2 → type Σ) : Prop.
  Class un_op_types (op: un_op) (ot: op_type) (T: Type) (A: T → type Σ) : Prop.

  (* BINARY OPERATORS *)
  Lemma abduct_type_bin_op Δ op ot1 ot2 v1 v2 A1 A2 T1 T2 B1 B2 R Φ:
    bin_op_types op ot1 ot2 T1 T2 B1 B2 →
    unifiable_scheme Δ VAL_TYPE A1 T1 B1 →
    unifiable_scheme Δ VAL_TYPE A2 T2 B2 →
    abduct Δ
      (type_unify (VAL v1) RECOVER A1 T1 B1 (λ t1,
        type_unify (VAL v2) RECOVER A2 T2 B2 (λ t2,
          type_bin_op_force op ot1 ot2 v1 (B1 t1) v2 (B2 t2) Φ))) R →
    abduct Δ (type_bin_op op ot1 ot2 v1 A1 v2 A2 Φ) R.
  Proof.
    rewrite /type_bin_op /type_unify /type_bin_op_force.
    iIntros (Hbin Hunif1 Hunif2 Habd) "Hctx Hv1 Hv2".
    iPoseProof (Habd with "Hctx Hv1") as "Hctx".
    rewrite /type_pre_type /type_pre_post. iDestruct "Hctx" as "[%t1 [Hv1 Hctx]]".
    iDestruct ("Hctx" with "Hv2") as "[%t2 [Hv2 Hctx]]".
    rewrite /type_bin_op. iDestruct ("Hctx" with "Hv1 Hv2") as "$".
  Qed.


  Lemma abduct_type_bin_op_both_value Δ op ot ly v1 v2 w R Φ:
    abduct Δ (type_val w (λ w' A, abduct_class (Copy A) (abd_assume CTX (v1 ◁ᵥ valueT w' ly) (abd_assume CTX (v2 ◁ᵥ valueT w' ly) ((type_bin_op op ot ot w A w A Φ)))))) R →
    abduct Δ (type_bin_op op ot ot v1 (valueT w ly) v2 (valueT w ly) Φ) R.
  Proof.
    rewrite /type_val /abduct_class /abd_assume.
    iIntros (Habd) "Hctx". iPoseProof (Habd with "Hctx") as "Hctx".
    iDestruct "Hctx" as (A) "(Hw & H)".
    iDestruct "H" as (Hcp) "Hbin". rewrite /type_bin_op. iDestruct "Hw" as "#Hw".
    iIntros "#Hv1 #Hv2".
    iDestruct (vtr_valueT_elim with "Hv1") as "[-> _]".
    iDestruct (vtr_valueT_elim with "Hv2") as "[-> _]".
    iApply ("Hbin" with "Hv1 Hv2 Hw Hw").
  Qed.

  Lemma abduct_type_bin_op_value_and_arg Δ op ot ly v1 v2 A R Φ:
    abduct Δ (abduct_class (Copy A) (abd_assume CTX (v1 ◁ᵥ valueT v2 ly) (type_bin_op op ot ot v2 A v2 A Φ))) R →
    abduct Δ (type_bin_op op ot ot v1 (valueT v2 ly) v2 A Φ) R.
  Proof.
    rewrite /type_val /abduct_class /abd_assume.
    iIntros (Habd) "Hctx". iPoseProof (Habd with "Hctx") as "Hctx".
    iDestruct "Hctx" as (Hcp) "Hbin".
    rewrite /type_bin_op. iIntros "#Hv1 #Hv2".
    iDestruct (vtr_valueT_elim with "Hv1") as "[-> _]".
    iApply ("Hbin" with "Hv1 Hv2 Hv2").
  Qed.

  Lemma abduct_type_bin_op_arg_and_value Δ op ot ly v1 v2 A R Φ:
    abduct Δ (abduct_class (Copy A) (abd_assume CTX (v2 ◁ᵥ valueT v1 ly) (type_bin_op op ot ot v1 A v1 A Φ))) R →
    abduct Δ (type_bin_op op ot ot v1 A v2 (valueT v1 ly) Φ) R.
  Proof.
    rewrite /type_val /abduct_class /abd_assume.
    iIntros (Habd) "Hctx". iPoseProof (Habd with "Hctx") as "Hctx".
    iDestruct "Hctx" as (Hcp) "Hbin".
    rewrite /type_bin_op. iIntros "#Hv1 #Hv2".
    iDestruct (vtr_valueT_elim with "Hv2") as "[-> _]".
    iApply ("Hbin" with "Hv2 Hv1 Hv1").
  Qed.

  Lemma abduct_type_bin_op_value_left Δ op ot1 ly1 ot2 v1 w1 v2 B Φ R:
    abduct Δ (type_val w1 (λ w' A, abd_assume CTX (v1 ◁ᵥ valueT w' ly1) (type_bin_op op ot1 ot2 w' A v2 B Φ))) R →
    abduct Δ (type_bin_op op ot1 ot2 v1 (valueT w1 ly1) v2 B Φ) R.
  Proof.
    rewrite /type_val /abd_assume /abduct_class.
    iIntros (Habd) "Hctx". iPoseProof (Habd with "Hctx") as "Hctx".
    iDestruct "Hctx" as (A) "(Hw1 & H)". rewrite /type_bin_op.
    iIntros "#Hv1 Hv2". iDestruct (vtr_valueT_elim with "Hv1") as "[-> _]".
    iApply ("H" with "Hv1 Hw1 Hv2").
  Qed.

  Lemma abduct_type_bin_op_value_right Δ op ot1 ot2 ly2 v1 A v2 w2 Φ R:
    abduct Δ (type_val w2 (λ w2' B, abd_assume CTX (v2 ◁ᵥ valueT w2' ly2) (type_bin_op op ot1 ot2 v1 A w2' B Φ))) R →
    abduct Δ (type_bin_op op ot1 ot2 v1 A v2 (valueT w2 ly2) Φ) R.
  Proof.
    rewrite /type_val /abd_assume /abduct_class.
    iIntros (Habd) "Hctx". iPoseProof (Habd with "Hctx") as "Hctx".
    iDestruct "Hctx" as (B) "(Hw2 & H)". rewrite /type_bin_op.
    iIntros "Hv1 #Hv2". iDestruct (vtr_valueT_elim with "Hv2") as "[-> _]".
    iApply ("H" with "Hv2 Hv1 Hw2").
  Qed.

  (* UNARY OPERATORS *)
  Lemma abduct_type_un_op Δ op ot v A T B R Φ:
    un_op_types op ot T B →
    unifiable_scheme Δ VAL_TYPE A T B →
    abduct Δ (type_unify (VAL v) RECOVER A T B (λ t, type_un_op_force op ot v (B t) Φ)) R →
    abduct Δ (type_un_op op ot v A Φ) R.
  Proof.
    rewrite /type_un_op /type_unify /type_un_op_force.
    iIntros (Hbin Huni1 Habd) "Hctx Hv".
    iPoseProof (Habd with "Hctx Hv") as "Hctx".
    rewrite /type_pre_type /type_pre_post. iDestruct "Hctx" as "[%t [Hv Hctx]]".
    rewrite /type_un_op. iDestruct ("Hctx" with "Hv") as "$".
  Qed.

  Lemma abduct_type_un_op_value Δ op ot ly v w R Φ:
    abduct Δ (type_val w (λ w' A, abd_assume CTX (v ◁ᵥ valueT w' ly) (type_un_op op ot w' A Φ))) R →
    abduct Δ (type_un_op op ot v (valueT w ly) Φ) R.
  Proof.
    rewrite /type_val /abduct_class /type_un_op /abd_assume.
    iIntros (Habd) "Hctx". iPoseProof (Habd with "Hctx") as "Hctx".
    iDestruct "Hctx" as (A) "(Hw & H)".
    iIntros "#Hv". iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
    by iApply ("H" with "Hv Hw").
  Qed.


  (* normal typing rules where the arguments are copy and not lost during the inference *)
  Class bin_op_typing_rule (φ: Prop) op ot1 ot2 A1 A2 A3 := {
    bin_op_typing_rule_left_copy: Copy A1;
    bin_op_typing_rule_right_copy: Copy A2;
    bin_op_typing_rule_result : φ → ∀ v1 v2 Ψ, (∀ v3, (Ψ v3 A3)) ⊢ type_bin_op_force op ot1 ot2 v1 A1 v2 A2 Ψ
  }.

  Class un_op_typing_rule (φ: Prop) op ot A B := {
    un_op_typing_rule_copy_arg: Copy A;
    un_op_typing_rule_result : φ → ∀ v Ψ, (∀ w, (Ψ w B)) ⊢ type_un_op_force op ot v A Ψ
  }.

  Class bin_op_typing_rule_custom op ot1 ot2 A1 A2 Φ P :=
    bin_op_typing_rule_custom_intro : ∀ v1 v2, P v1 v2 ⊢ type_bin_op_force op ot1 ot2 v1 A1 v2 A2 Φ.

  Class un_op_typing_rule_custom op ot A Φ P :=
    un_op_typing_rule_custom_intro : ∀ v, P v ⊢ type_un_op_force op ot v A Φ.

  Lemma bin_op_typing_rule_intro (φ: Prop) op ot1 ot2 A1 `{!Copy A1} A2 `{!Copy A2} A3:
    (φ → ∀ v1 v2 Ψ, (∀ v3, Ψ v3 A3) ⊢ type_bin_op_force op ot1 ot2 v1 A1 v2 A2 Ψ) →
    bin_op_typing_rule φ op ot1 ot2 A1 A2 A3.
  Proof. split; auto. Qed.

  Lemma un_op_typing_rule_intro (φ: Prop) op ot A `{!Copy A} B:
    (φ → ∀ v Ψ, (∀ w, Ψ w B) ⊢ type_un_op_force op ot v A Ψ) →
    un_op_typing_rule φ op ot A B.
  Proof. split; auto. Qed.

  (* abduction rules for the operator typing rules *)
  Local Existing Instances
    bin_op_typing_rule_left_copy
    bin_op_typing_rule_right_copy
    un_op_typing_rule_copy_arg.

  Lemma abduct_bin_op_force_rule_copy Δ φ op ot1 ot2 v1 v2 A1 A2 A3 R Φ:
    bin_op_typing_rule φ op ot1 ot2 A1 A2 A3 →
    abduct Δ (abd_assume ASSERT (v1 ◁ᵥ A1) $ abd_assume ASSERT (v2 ◁ᵥ A2) $ abduct_pure φ (∀ v3, Φ v3 A3)) R →
    abduct Δ (type_bin_op_force op ot1 ot2 v1 A1 v2 A2 Φ) R.
  Proof.
    rewrite /abduct_pure /type_bin_op_force /abd_assume /type_bin_op.
    iIntros (Hrule Habd) "Hctx #Hv1 #Hv2".
    iPoseProof (Habd with "Hctx Hv1 Hv2") as "[%Hφ Hpost]".
    iPoseProof (bin_op_typing_rule_result Hφ) as "Hcont".
    rewrite /type_bin_op_force /type_bin_op /abd_assume.
    iApply ("Hcont" with "Hpost Hv1 Hv2").
  Qed.

  Lemma abduct_un_op_force_rule_copy Δ φ op ot v A B R Φ:
    un_op_typing_rule φ op ot A B →
    abduct Δ (abd_assume ASSERT (v ◁ᵥ A) $ abduct_pure φ (∀ w, Φ w B)) R →
    abduct Δ (type_un_op_force op ot v A Φ) R.
  Proof.
    rewrite /abduct_pure /type_un_op_force /type_un_op /abd_assume.
    iIntros (Hrule Habd) "Hctx #Hv".
    iPoseProof (Habd with "Hctx Hv") as "[%Hφ Hpost]".
    iPoseProof (un_op_typing_rule_result Hφ) as "Hcont".
    rewrite /type_un_op_force /type_un_op /abd_assume.
    iApply ("Hcont" with "Hpost Hv").
  Qed.

  Lemma abduct_bin_op_force_custom Δ op ot1 ot2 v1 A1 v2 A2 Φ P R:
    bin_op_typing_rule_custom op ot1 ot2 A1 A2 Φ P →
    abduct Δ (P v1 v2) R →
    abduct Δ (type_bin_op_force op ot1 ot2 v1 A1 v2 A2 Φ) R.
  Proof.
    rewrite /bin_op_typing_rule_custom. by intros ->.
  Qed.

  Lemma abduct_un_op_force_custom Δ op ot v A Φ P R:
    un_op_typing_rule_custom op ot A Φ P →
    abduct Δ (P v) R →
    abduct Δ (type_un_op_force op ot v A Φ) R.
  Proof. rewrite /un_op_typing_rule_custom. by intros ->. Qed.


  (* BINARY OPERATORS *)
  Global Instance abduct_step_type_bin_op_both_value Δ op ot ly v1 v2 w R Φ:
    AbductStep (abduct Δ (type_val w (λ w' A, abduct_class (Copy A) (abd_assume CTX (v1 ◁ᵥ valueT w' ly) (abd_assume CTX (v2 ◁ᵥ valueT w' ly) ((type_bin_op op ot ot w A w A Φ)))))) R) Δ (type_bin_op op ot ot v1 (valueT w ly) v2 (valueT w ly) Φ) R | 1.
  Proof. intros ?; by apply abduct_type_bin_op_both_value. Qed.

  Global Instance abduct_step_type_bin_op_arg_and_value Δ op ot ly v1 v2 A R Φ:
    AbductStep (abduct Δ (abduct_class (Copy A) (abd_assume CTX (v2 ◁ᵥ valueT v1 ly) (type_bin_op op ot ot v1 A v1 A Φ))) R) Δ (type_bin_op op ot ot v1 A v2 (valueT v1 ly) Φ) R | 2.
  Proof. intros ?; by apply abduct_type_bin_op_arg_and_value. Qed.

  Global Instance abduct_step_type_bin_op_value_and_arg Δ op ot ly v1 v2 A R Φ:
    AbductStep (abduct Δ (abduct_class (Copy A) (abd_assume CTX (v1 ◁ᵥ valueT v2 ly) (type_bin_op op ot ot v2 A v2 A Φ))) R) Δ (type_bin_op op ot ot v1 (valueT v2 ly) v2 A Φ) R | 2.
  Proof. intros ?; by apply abduct_type_bin_op_value_and_arg. Qed.

  Global Instance abduct_step_type_bin_op_value_left Δ op ot1 ot2 ly1 v1 w1 v2 B Φ R:
    AbductStep (abduct Δ (type_val w1 (λ w1' A, abd_assume CTX (v1 ◁ᵥ valueT w1 ly1) (type_bin_op op ot1 ot2 w1' A v2 B Φ))) R) Δ (type_bin_op op ot1 ot2 v1 (valueT w1 ly1) v2 B Φ) R | 3.
  Proof. intros ?; by apply abduct_type_bin_op_value_left. Qed.

  Global Instance abduct_step_type_bin_op_value_right Δ op ot1 ot2 ly2 v1 A v2 w2 Φ R:
    AbductStep (abduct Δ (type_val w2 (λ w2' B, abd_assume CTX (v2 ◁ᵥ valueT w2' ly2) (type_bin_op op ot1 ot2 v1 A w2 B Φ))) R) Δ (type_bin_op op ot1 ot2 v1 A v2 (valueT w2 ly2) Φ) R | 3.
  Proof. intros ?; by apply abduct_type_bin_op_value_right. Qed.

  Global Instance abduct_step_type_bin_op Δ op ot1 ot2 v1 v2 A1 A2 T1 T2 B1 B2 R Φ:
    bin_op_types op ot1 ot2 T1 T2 B1 B2 →
    unifiable_scheme Δ VAL_TYPE A1 T1 B1 →
    unifiable_scheme Δ VAL_TYPE A2 T2 B2 →
    AbductStep (abduct Δ
        (type_unify (VAL v1) RECOVER A1 T1 B1 (λ t1,
          type_unify (VAL v2) RECOVER A2 T2 B2 (λ t2,
          type_bin_op_force op ot1 ot2 v1 (B1 t1) v2 (B2 t2) Φ))) R)
      Δ (type_bin_op op ot1 ot2 v1 A1 v2 A2 Φ) R | 4.
  Proof. intros ? ? ? ?; by eapply abduct_type_bin_op. Qed.

  Global Instance abduct_step_bin_op_force_rule_copy Δ φ op ot1 ot2 v1 v2 A1 A2 A3 R Φ:
    bin_op_typing_rule φ op ot1 ot2 A1 A2 A3 →
    AbductStep (abduct Δ (abd_assume CTX (v1 ◁ᵥ A1) $ abd_assume CTX (v2 ◁ᵥ A2) $ abduct_pure φ (∀ v3, Φ v3 A3)) R) Δ (type_bin_op_force op ot1 ot2 v1 A1 v2 A2 Φ) R | 2.
  Proof. intros ??; by eapply abduct_bin_op_force_rule_copy. Qed.

  Global Instance abduct_step_bin_op_force_custom Δ op ot1 ot2 v1 A1 v2 A2 Φ P R:
    bin_op_typing_rule_custom op ot1 ot2 A1 A2 Φ P →
    AbductStep (abduct Δ (P v1 v2) R) Δ (type_bin_op_force op ot1 ot2 v1 A1 v2 A2 Φ) R | 1.
  Proof. intros ??; by eapply abduct_bin_op_force_custom. Qed.

  (* UNARY OPERATORS *)
  Global Instance abduct_step_type_un_op Δ op ot v A T B R Φ:
    un_op_types op ot T B →
    unifiable_scheme Δ VAL_TYPE A T B →
    AbductStep (abduct Δ (type_unify (VAL v) RECOVER A T B (λ t, type_un_op_force op ot v (B t) Φ)) R) Δ (type_un_op op ot v A Φ) R | 2.
  Proof. intros ? ? ?; by eapply abduct_type_un_op. Qed.

  Global Instance abduct_step_type_un_op_value Δ op ot ly v w R Φ:
    AbductStep (abduct Δ (type_val w (λ w' A, abd_assume CTX (v ◁ᵥ valueT w' ly) (type_un_op op ot w' A Φ))) R) Δ (type_un_op op ot v (valueT w ly) Φ) R.
  Proof. intros ?; by eapply abduct_type_un_op_value. Qed.

  Global Instance abduct_step_un_op_force_rule_copy Δ φ op ot v A B R Φ:
    un_op_typing_rule φ op ot A B →
    AbductStep (abduct Δ (abd_assume CTX (v ◁ᵥ A) $ abduct_pure φ (∀ w, Φ w B)) R) Δ (type_un_op_force op ot v A Φ) R | 2.
  Proof. intros ??; by eapply abduct_un_op_force_rule_copy. Qed.

  Global Instance abduct_step_un_op_force_custom Δ op ot v A Φ P R:
    un_op_typing_rule_custom op ot A Φ P →
    AbductStep (abduct Δ (P v) R) Δ (type_un_op_force op ot v A Φ) R | 1.
  Proof. intros ??; by eapply abduct_un_op_force_custom. Qed.

End prepare_arithmetic_operations.


(* Hint Modes for the type classes *)
Global Hint Mode bin_op_types - - + + + - - - - : typeclass_instances.
Global Hint Mode un_op_types - - + + - - : typeclass_instances.
Global Hint Mode bin_op_typing_rule - - - ! ! ! ! ! - : typeclass_instances.
Global Hint Mode un_op_typing_rule - - - ! ! ! - : typeclass_instances.
Global Hint Mode bin_op_typing_rule_custom - - ! ! ! ! ! - - : typeclass_instances.
Global Hint Mode un_op_typing_rule_custom - - ! ! ! - - : typeclass_instances.



Section arithmetic_instances.
  Context `{!refinedcG Σ}.

  (* BINARY INTEGER OPERATIONS *)
  Global Instance bin_op_plus_int_int it  : bin_op_types AddOp (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_minus_int_int it : bin_op_types SubOp (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_mult_int_int it  : bin_op_types MulOp (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_div_int_int it   : bin_op_types DivOp (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_mod_int_int it   : bin_op_types ModOp (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_and_int_int it   : bin_op_types AndOp (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_or_int_int it    : bin_op_types OrOp  (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_xor_int_int it   : bin_op_types XorOp (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_shl_int_int it   : bin_op_types ShlOp (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_shr_int_int it   : bin_op_types ShrOp (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.

  Global Instance bin_op_typing_rule_int_arith it n1 n2 n3 op φ :
    Computable (int_arithop_result it n1 n2 op) (Some n3) →
    Simpl (int_arithop_sidecond it n1 n2 n3 op) φ →
    bin_op_typing_rule φ op (IntOp it) (IntOp it) (intT it n1) (intT it n2) (intT it n3).
  Proof.
    rewrite /Computable /Simpl. intros Harith Hcond.
    apply: bin_op_typing_rule_intro.
    subst φ. iIntros (Hcond v1 v2 Ψ) "Hv".
    iApply type_bin_op_int_arith; done.
  Qed.


  (* BINARY INTEGER COMPARISONS *)
  Global Instance bin_op_eq_int_int it it2 : bin_op_types (EqOp it2) (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_neq_int_int it it2 : bin_op_types (NeOp it2) (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_lt_int_int it it2 : bin_op_types (LtOp it2) (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_le_int_int it it2 : bin_op_types (LeOp it2) (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_gt_int_int it it2 : bin_op_types (GtOp it2) (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.
  Global Instance bin_op_ge_int_int it it2 : bin_op_types (GeOp it2) (IntOp it) (IntOp it) Z Z (λ n, intT it n) (λ n, intT it n) := {}.

  Definition eval_int_rel (op: bin_op) (n1 n2 : Z) : option (dProp * int_type) :=
    match op with
    | EqOp rit => Some ((n1 = n2)%DP, rit)
    | NeOp rit => Some ((n1 ≠ n2)%DP, rit)
    | LtOp rit => Some ((n1 < n2)%DP, rit)
    | GtOp rit => Some ((n1 > n2)%DP, rit)
    | LeOp rit => Some ((n1 ≤ n2)%DP, rit)
    | GeOp rit => Some ((n1 >= n2)%DP, rit)
    | _ => None
    end.

  Lemma eval_int_rel_compat op n1 n2 φ it:
    eval_int_rel op n1 n2 = Some (φ, it) →
    eval_bin_op_relII op n1 n2 = Some (bool_decide φ, it).
  Proof. destruct op; simpl; naive_solver. Qed.

  Global Instance bin_op_typing_rule_int_comp it1 it2 n1 n2 φ op :
    Computable (eval_int_rel op n1 n2) (Some (φ, it2)) →
    bin_op_typing_rule True op (IntOp it1) (IntOp it1) (intT it1 n1) (intT it1 n2) (φ @ boolR it2).
  Proof.
    rewrite /Computable. intros Harith.
    apply: bin_op_typing_rule_intro.
    iIntros (_ v1 v2 Ψ) "Hv". iApply type_bin_op_int_comp; first by eapply eval_int_rel_compat.
    done.
  Qed.


  (* BINARY BOOLEAN COMPARISONS *)
  Global Instance bin_op_and_bool_bool it : bin_op_types AndOp (IntOp it) (IntOp it) bool bool  (λ b, boolT it b) (λ b, boolT it b) := {}.
  Global Instance bin_op_or_bool_bool it : bin_op_types OrOp (IntOp it) (IntOp it) bool bool  (λ b, boolT it b) (λ b, boolT it b) := {}.
  Global Instance bin_op_xor_bool_bool it : bin_op_types XorOp (IntOp it) (IntOp it) bool bool  (λ b, boolT it b) (λ b, boolT it b) := {}.

  Global Instance bin_op_typing_rule_bool_log it op φ ψ ξ:
    Computable (bool_log_op op φ ψ) (Some ξ) →
    bin_op_typing_rule True op  (IntOp it) (IntOp it) (φ @ boolR it) (ψ @ boolR it) (ξ @ boolR it) | 1.
  Proof.
    rewrite /Computable. intros Hlog.
    apply: bin_op_typing_rule_intro.
    iIntros (_ v1 v2 Ψ) "Hv".
    iApply type_bin_op_bool_log; done.
  Qed.

  Global Instance bin_op_typing_rule_bool_log_decide it op (φ ψ : Prop) `{!Decision φ} `{!Decision ψ} ξ:
    Computable (bool_log_op op (DProp φ) (DProp ψ)) (Some ξ) →
    bin_op_typing_rule True op  (IntOp it) (IntOp it) (boolT it (bool_decide φ)) (boolT it (bool_decide ψ)) (ξ @ boolR it) | 2.
  Proof.
    rewrite /Computable. intros Hlog.
    apply: bin_op_typing_rule_intro.
    iIntros (_ v1 v2 Ψ) "Hv".
    change (boolT it (bool_decide φ)) with (DProp φ @ boolR it)%RT.
    change (boolT it (bool_decide ψ)) with (DProp ψ @ boolR it)%RT.
    iApply type_bin_op_bool_log; done.
  Qed.

  Global Instance bin_op_typing_rule_bool_log_raw it op b1 b2 b:
    Computable (bool_log_op_raw op b1 b2) (Some b) →
    bin_op_typing_rule True op  (IntOp it) (IntOp it) (boolT it b1) (boolT it b2) (boolT it b) | 3.
  Proof.
    rewrite /Computable. intros Hlog.
    apply: bin_op_typing_rule_intro.
    iIntros (_ v1 v2 Ψ) "Hv".
    iApply type_bin_op_bool_log_raw; done.
  Qed.


  (* UNARY INTEGER OPERATIONS *)
  Global Instance un_op_cast_int_int it it2 : un_op_types (CastOp (IntOp it2)) (IntOp it) Z (λ n, intT it n) := {}.
  Global Instance un_op_neg_int it : un_op_types NegOp (IntOp it) Z (λ n, intT it n) := {}.

  Global Instance un_op_typing_rule_cast_int_int it1 it2 n :
    un_op_typing_rule (n ∈ it2) (CastOp (IntOp it2)) (IntOp it1) (intT it1 n) (intT it2 n) | 3.
  Proof.
    apply: un_op_typing_rule_intro.
    iIntros (Hran v Ψ) "HΨ". by iApply type_un_op_cast_int_int.
  Qed.

  Definition int_extend (it1 it2 : int_type) : Prop :=
    ∀ n, n ∈ it1 → n ∈ it2.

  Lemma int_extend_half_modulus it1 it2 :
    it_byte_size_log it1 <= it_byte_size_log it2 →
    int_half_modulus it1 ≤ int_half_modulus it2.
  Proof.
    intros Hsz.
    rewrite /int_half_modulus.
    eapply Z.pow_le_mono_r; first lia.
    rewrite /bits_per_int /bytes_per_int /bits_per_byte.
    rewrite -Z.sub_le_mono_r.
    eapply Z.mul_le_mono_nonneg_r; first done.
    eapply inj_le.
    eapply Nat.pow_le_mono_r; lia.
  Qed.

  Lemma int_extend_min_int it1 it2 :
    it_byte_size_log it1 <= it_byte_size_log it2 → it_signed it1 = it_signed it2 → min_int it2 <= min_int it1.
  Proof.
    intros Hsz Hsgn. rewrite /min_int.
    rewrite Hsgn. destruct (it_signed it2); last done.
    rewrite -Z.opp_le_mono.
    by eapply int_extend_half_modulus.
  Qed.

  Lemma int_extend_max_int it1 it2 :
    it_byte_size_log it1 <= it_byte_size_log it2 → it_signed it1 = it_signed it2 → max_int it1 <= max_int it2.
  Proof.
    intros Hsz Hsgn. rewrite /max_int.
    rewrite Hsgn.
    rewrite -Z.sub_le_mono_r.
    destruct (it_signed it2).
    - by eapply int_extend_half_modulus.
    - rewrite !int_modulus_twice_half_modulus.
      eapply Z.mul_le_mono_nonneg_l; first done.
      by eapply int_extend_half_modulus.
  Qed.

  Lemma int_extend_upscale it1 it2 :
    it_byte_size_log it1 <= it_byte_size_log it2 → it_signed it1 = it_signed it2 → int_extend it1 it2.
  Proof.
    intros Hle Hsgn n [Hlb Hub]. split; etrans; eauto using int_extend_min_int, int_extend_max_int.
  Qed.

  Existing Class int_extend.

  Global Instance int_extend_upscale_inst it1 it2:
    Computable ((it_byte_size_log it1) <=? (it_byte_size_log it2)) true →
    Computable (eqb (it_signed it1) (it_signed it2)) true →
    int_extend it1 it2.
  Proof.
    rewrite /Computable.
    intros Hle%Zle_bool_imp_le Hsn%eqb_prop.
    by eapply int_extend_upscale.
  Qed.

  Global Instance un_op_typing_rule_cast_int_int_extend_signed_unsigned it1 it2 n :
    sequence_classes [
      Computable (it_signed it1) true;
      Computable (it_signed it2) false;
      int_extend (IntType (it_byte_size_log it1) false) (it2)
      ] →
    un_op_typing_rule (0 ≤ n) (CastOp (IntOp it2)) (IntOp it1) (intT it1 n) (intT it2 n) | 2.
  Proof.
    rewrite /sequence_classes !Forall_cons Forall_nil /=.
    rewrite /Computable. intros (Hsigned & ? & Hext & ?).
    apply: un_op_typing_rule_intro.
    iIntros (Hran v Ψ) "HΨ". iApply type_un_op_cast_int_int; last done.
    intros Hel. eapply Hext.
    split; first done.
    destruct Hel as [Hel1 Hel2].
    revert Hel2.
    rewrite /max_int /= /int_modulus Hsigned /int_half_modulus /bits_per_int.
    rewrite /bits_per_byte /bytes_per_int /=.
    intros Hle. etrans; first apply Hle.
    rewrite -Z.sub_le_mono_r.
    eapply Z.pow_le_mono_r; lia.
  Qed.

  Global Instance un_op_typing_rule_cast_int_int_extend it1 it2 n :
    int_extend it1 it2 →
    un_op_typing_rule True (CastOp (IntOp it2)) (IntOp it1) (intT it1 n) (intT it2 n) | 1.
  Proof.
    intros Hext; apply: un_op_typing_rule_intro.
    iIntros (Hran v Ψ) "HΨ". iApply type_un_op_cast_int_int; last done.
    by eapply Hext.
  Qed.

  Global Instance un_op_typing_rule_neg_int it n :
    un_op_typing_rule (n ∈ it → (-n) ∈ it) NegOp (IntOp it) (intT it n) (intT it (-n)).
  Proof.
    apply: un_op_typing_rule_intro.
    iIntros (Hran v Ψ) "HΨ". by iApply type_un_op_neg_int.
  Qed.



  (* UNARY BOOLEAN OPERATIONS *)
  Global Instance un_op_cast_int_bool it : un_op_types (CastOp BoolOp) (IntOp it) Z (λ n, intT it n) | 2 := {}.
  Global Instance un_op_cast_bool_bool it : un_op_types (CastOp BoolOp) (IntOp it) bool (λ b, boolT it b) | 1 := {}.


  Global Instance un_op_typing_rule_cast_bool_int it n:
    un_op_typing_rule True (CastOp BoolOp) (IntOp it) (intT it n) ((n ≠ 0)%DP @ boolR u8) | 2.
  Proof.
    apply: un_op_typing_rule_intro.
    iIntros (_ v Ψ) "HΨ". by iApply type_un_op_cast_bool_int.
  Qed.

  Global Instance un_op_typing_rule_cast_bool_zero it:
    un_op_typing_rule True (CastOp BoolOp) (IntOp it) (intT it 0) (False%DP @ boolR u8) | 1.
  Proof.
    apply: un_op_typing_rule_intro.
    iIntros (_ v ψ) "Ψ". by iApply type_un_op_cast_bool_int.
  Qed.

  Global Instance un_op_typing_rule_cast_bool_one it:
    un_op_typing_rule True (CastOp BoolOp) (IntOp it) (intT it 1) (True%DP @ boolR u8) | 1.
  Proof.
    apply: un_op_typing_rule_intro.
    iIntros (_ v ψ) "Ψ". by iApply type_un_op_cast_bool_int.
  Qed.

  Global Instance un_op_typing_rule_cast_bool_bool_refinement it (φ: dProp):
    un_op_typing_rule True (CastOp BoolOp) (IntOp it) (φ @ boolR it) (φ @ boolR u8) | 1.
  Proof.
    apply: un_op_typing_rule_intro.
    iIntros (_ v ψ) "Ψ". by iApply type_un_op_cast_bool_bool.
  Qed.

  Global Instance un_op_typing_rule_cast_bool_bool_raw it b:
    un_op_typing_rule True (CastOp BoolOp) (IntOp it) (boolT it b) (boolT u8 b) | 2.
  Proof.
    apply: un_op_typing_rule_intro.
    iIntros (_ v ψ) "Ψ". by iApply type_un_op_cast_bool_bool.
  Qed.

End arithmetic_instances.
