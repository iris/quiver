From quiver.argon.abduction Require Import abduct proofmode.
From quiver.thorium.typing Require Import assume_type.
From quiver.thorium.typing Require Import classes.
From quiver.thorium.logic Require Import syntax assume.


(* This operation collects all unreachable values in the context and eliminates them. *)
Section garbage_collection.
  Context `{!refinedcG Σ}.


  (* The garbage collection proceeds as follows:
      1. [type_garbage_collect] We contract the context by following (v: valueT w ot) links
         and removing identities (ideally there should be none).
      2. [type_unreachable] We search for values that are unreachable from the rest of the context.
         If we find any, we turn them into `vty A` typings.
      3. [type_recycle] We "recycle" all `vty A` typings as much as possible.
      4. We process the new information and discard with all the unused values.
  *)

  Definition type_unreachable (P: iProp Σ) := P.

  (* GARBAGE COLLECTION: we condense bindings in the context *)
  Lemma abduct_type_garbage_collect_find_moved Δ Δ' v A P R:
    sequence_classes_backtracked [
      qenvs_find_atom SHALLOW Δ (v ◁ᵥ movedT A) Δ';
      once_tc (Δ ⊨ virtual_type (movedT A))
    ] →
    abduct Δ' (type_virt (VAL v) (movedT A) (λ B, abd_assume ASSERT (v ◁ᵥ B) (type_garbage_collect P))) R →
    abduct Δ (type_garbage_collect P) R.
  Proof.
    rewrite /qenvs_find_atom /qenvs_add_sep_assertion.
    rewrite /sequence_classes_backtracked /once_tc.
    rewrite !Forall_cons Forall_nil /=. intros (Hfind1 & Hvirt & _) Habd.
    iIntros "Ctx". rewrite Hfind1. iDestruct "Ctx" as "[[Hv HΔ] R]".
    iDestruct (Habd with "[$HΔ $R]") as "Hcont". rewrite /type_virt /abd_assume.
    iDestruct ("Hcont" with "Hv") as "(%B & Hv & Hcont)". by iApply "Hcont".
  Qed.

  Lemma abduct_type_garbage_collect_find_val Δ Δ' Δ'' Δ''' v w ot A P R:
    sequence_classes_backtracked [
      qenvs_find_atom SHALLOW Δ (v ◁ᵥ valueT w ot) Δ'; (* find a binding from one value to another *)
      sequence_classes [qenvs_find_atom DEEP Δ' (w ◁ᵥ A) Δ''; Δ ⊨ Copy A; qenvs_add_sep_assertion Δ' (v ◁ᵥ A) Δ'''] (* find a copy assertion *)
    ] →
    abduct Δ''' (type_garbage_collect P) R →
    abduct Δ (type_garbage_collect P) R.
  Proof.
    rewrite /qenvs_find_atom /qenvs_add_sep_assertion.
    rewrite /sequence_classes_backtracked /sequence_classes.
    rewrite !Forall_cons Forall_nil /=. intros (Hfind1 & (Hfind2 & Hent & Hadd & _) & _) Habd.
    eapply abduct_qenvs_entails; [by eapply Hent|intros Hcp].
    iIntros "Ctx". iApply Habd. rewrite Hfind1 Hfind2 Hadd.
    iDestruct "Ctx" as "[(Hv & #Hw & HΔ) $]". rewrite Hfind2. iFrame "Hw HΔ".
    by iDestruct (vtr_valueT_elim with "Hv") as "[-> _]".
  Qed.


  Lemma abduct_type_garbage_collect_find_vty Δ Δ' A A' P R:
    sequence_classes_backtracked [
        qenvs_find_atom SHALLOW Δ (vty A) Δ'; (* find a vty binding  *)
        sequence_classes [exi A; Δ ⊨ ctx_hyp (A `is_ty` A')] (* find type assignment *)
    ] →
    abduct Δ' (∀ v, type_recycle v A' (type_garbage_collect P)) R →
    abduct Δ (type_garbage_collect P) R.
  Proof.
    rewrite /qenvs_find_atom /qenvs_add_sep_assertion.
    rewrite /sequence_classes_backtracked /sequence_classes.
    rewrite !Forall_cons Forall_nil /=. intros (Hfind1 & (_ & Hent & _) & _) Habd.
    eapply abduct_qenvs_entails; [by eapply Hent|rewrite /ctx_hyp; intros Hty].
    iIntros "Ctx". rewrite Hfind1 /vty. iDestruct "Ctx" as "[[[%v Hv] Δ] R]".
    iDestruct (Habd with "[$Δ $R]") as "Hcont". rewrite /type_recycle.
    destruct Hty as [Hty]. rewrite Hty. iApply "Hcont". iFrame.
  Qed.


  Lemma abduct_type_garbage_collect_done Δ P R:
    abduct Δ (type_unreachable P) R →
    abduct Δ (type_garbage_collect P) R.
  Proof.
    rewrite /type_unreachable /type_garbage_collect //.
  Qed.


  (* UNREACHABLE *)
  Lemma abduct_type_unreachable Δ Δ' v A P R:
    sequence_classes_backtracked [
      qenvs_find_atom SHALLOW Δ (v ◁ᵥ A) Δ';
      sequence_classes [uni v; not_in v Δ'; not_in v P]
    ] →
    abduct Δ' (type_recycle v A (type_unreachable P)) R →
    abduct Δ (type_unreachable P) R.
  Proof.
    rewrite /qenvs_find_atom /qenvs_add_sep_assertion.
    rewrite /sequence_classes_backtracked /sequence_classes.
    rewrite !Forall_cons Forall_nil /=. intros (Hfind1 & _ & _) Habd.
    iIntros "Ctx". rewrite Hfind1. iDestruct "Ctx" as "[(Hv & HΔ) R]".
    iDestruct (Habd with "[$HΔ $R]") as "Hcont". rewrite /type_recycle.
    by iApply "Hcont".
  Qed.

  Global Instance abduct_step_type_unreachable Δ Δ' v A P R:
    sequence_classes_backtracked [
      qenvs_find_atom SHALLOW Δ (v ◁ᵥ A) Δ';
      sequence_classes [uni v; not_in v Δ'; not_in v P]
    ] →
    AbductStep (abduct Δ' (type_recycle v A (type_unreachable P)) R)
        Δ (type_unreachable P) R | 1.
  Proof.
    intros ??. by eapply abduct_type_unreachable.
  Qed.

  Lemma abduct_type_unreachable_intro Δ (P: iProp Σ) R:
    abduct Δ P R →
    abduct Δ (type_unreachable P) R.
  Proof. rewrite /type_unreachable //. Qed.

  Global Instance abduct_step_type_unreachable_intro Δ (P: iProp Σ) R:
    AbductStep (abduct Δ P R)
      Δ (type_unreachable P) R | 20.
  Proof. intros ?. by eapply abduct_type_unreachable_intro. Qed.

  (* lemma for manually triggering garbage collection *)
  Lemma trigger_gc Δ (P R: iProp Σ):
    abduct Δ (type_garbage_collect P) R →
    abduct Δ P R.
  Proof. by rewrite /type_garbage_collect. Qed.

  (* typeclass instances *)
  Global Instance abduct_step_type_garbage_collect_find_moved Δ Δ' v A P R:
    sequence_classes_backtracked [
      qenvs_find_atom SHALLOW Δ (v ◁ᵥ movedT A) Δ';
      once_tc (Δ ⊨ virtual_type (movedT A))
    ] →
    AbductStep (abduct Δ' (type_virt (VAL v) (movedT A) (λ B, abd_assume ASSERT (v ◁ᵥ B) (type_garbage_collect P))) R)
      Δ (type_garbage_collect P) R | 2.
  Proof. intros ??. by eapply abduct_type_garbage_collect_find_moved. Qed.

  Global Instance abduct_step_type_garbage_collect_find_val Δ Δ' Δ'' Δ''' v w ot A P R:
    sequence_classes_backtracked [
      qenvs_find_atom SHALLOW Δ (v ◁ᵥ valueT w ot) Δ'; (* find a binding from one value to another *)
      sequence_classes [qenvs_find_atom DEEP Δ' (w ◁ᵥ A) Δ''; Δ ⊨ Copy A; qenvs_add_sep_assertion Δ' (v ◁ᵥ A) Δ'''] (* find a copy assertion *)
    ] →
    AbductStep (abduct Δ''' (type_garbage_collect P) R)
      Δ (type_garbage_collect P) R | 2.
  Proof.
    intros ??. by eapply abduct_type_garbage_collect_find_val.
  Qed.

  Global Instance abduct_step_type_garbage_collect_find_vty Δ Δ' A A' P R:
    sequence_classes_backtracked [
      qenvs_find_atom SHALLOW Δ (vty A) Δ'; (* find a vty binding  *)
      sequence_classes [exi A; Δ ⊨ ctx_hyp (A `is_ty` A')] (* find type assignment *)
    ] →
    AbductStep (abduct Δ' (∀ v, type_recycle v A' (type_garbage_collect P)) R)
      Δ (type_garbage_collect P) R | 2.
  Proof.
    intros ??. by eapply abduct_type_garbage_collect_find_vty.
  Qed.

  Global Instance abduct_step_type_garbage_collect_done Δ P R:
    AbductStep (abduct Δ (type_unreachable P) R) Δ (type_garbage_collect P) R | 4.
  Proof. intros ?. by eapply abduct_type_garbage_collect_done. Qed.

End garbage_collection.

Global Typeclasses Opaque type_unreachable.


Section prune_argument_variables.
  Context `{!refinedcG Σ}.

  Definition type_prune_args (P: iProp Σ) : iProp Σ := P.

  Lemma abduct_type_prune_args_intro Δ P R:
    abduct Δ P R →
    abduct Δ (type_prune_args P) R.
  Proof.
    rewrite /type_prune_args //.
  Qed.

  Lemma abduct_type_prune_args_find_arg_val Δ Δ' v A P R:
    sequence_classes_backtracked [
      qenvs_find_atom SHALLOW Δ (v ◁ᵥ A) Δ';
      sequence_classes [is_arg_val v; Copy A; no_uni A]
    ] →
    (abduct Δ' (type_recycle v A (type_prune_args P)) R) →
    abduct Δ (type_prune_args P) R.
  Proof.
    rewrite /sequence_classes_backtracked /sequence_classes.
    rewrite !Forall_cons Forall_nil /id /type_recycle /=.
    intros (Hfind & (Harg & Hcopy & Hno_uni & _) & _) Habd.
    iIntros "Hctx". rewrite Hfind. iDestruct "Hctx" as "[[Hv HΔ] HR]".
    iDestruct (Habd with "[$HΔ $HR] Hv") as "$".
  Qed.

  Global Instance abduct_step_type_prune_args_intro Δ P R:
    AbductStep (abduct Δ P R)
      Δ (type_prune_args P) R | 10.
  Proof. intros ?. by eapply abduct_type_prune_args_intro. Qed.

  Global Instance abduct_step_type_prune_args_find_arg_val Δ Δ' v A P R:
    sequence_classes_backtracked [
      qenvs_find_atom SHALLOW Δ (v ◁ᵥ A) Δ';
      sequence_classes [is_arg_val v; Copy A; no_uni A]
    ] →
    AbductStep (abduct Δ' (type_recycle v A (type_prune_args P)) R)
        Δ (type_prune_args P) R | 1.
  Proof.
    intros ??. by eapply abduct_type_prune_args_find_arg_val.
  Qed.

End prune_argument_variables.

Global Typeclasses Opaque type_prune_args.


(* the proof mode for filling contexts *)
Section context_filling.
  Context `{!refinedcG Σ}.

  Definition fill_type (T: loc_or_val_type) (Δ: qenvs Σ) (A: type Σ) (Δ': qenvs Σ) (B: type Σ) :=
    ∀ x: T, lov x ◁ₓ A ∗ qenvs_ctx_prop Δ ⊢ lov x ◁ₓ B ∗ qenvs_ctx_prop Δ'.

  Definition fill_type_list (X: loc_or_val_type) (Δ: qenvs Σ) (T: list (type Σ)) (Δ': qenvs Σ) (U: list (type Σ)) :=
    ∀ L: list X, ([∗ list] x; A ∈ L; T, lov x ◁ₓ A) ∗ qenvs_ctx_prop Δ ⊢ ([∗ list] x; B ∈ L; U, lov x ◁ₓ B) ∗ qenvs_ctx_prop Δ'.

  Lemma fill_type_return T Δ A:
    fill_type T Δ A Δ A.
  Proof. iIntros (l) "$". Qed.

  Lemma fill_type_placeT Δ l A B Δ' Δ'':
    qenvs_find_atom SHALLOW Δ (l ◁ₗ A) Δ' →
    fill_type LOC_TYPE Δ' A Δ'' B →
    fill_type LOC_TYPE Δ (placeT l) Δ'' B.
  Proof.
    rewrite /qenvs_find_atom.
    iIntros (Hfind Hfill r) "[Hr HΔ]"; simpl.
    rewrite placeT_eq //=. iDestruct "Hr" as "->".
    iApply Hfill. rewrite Hfind //.
  Qed.

  Lemma fill_type_valueT_loc T Δ Δ' Δ'' l A B:
    sequence_classes [
      uni l;
      qenvs_find_atom SHALLOW Δ (l ◁ₗ A) Δ';
      fill_type LOC_TYPE Δ' A Δ'' B
    ] →
    fill_type T Δ (valueT l ptr_size) Δ'' (ownT l B).
  Proof.
    rewrite /sequence_classes !Forall_cons Forall_nil /id /=.
    rewrite /qenvs_find_atom. intros (? & Hfind & Hfill & _).
    iIntros (r) "[Hr HΔ]".
    rewrite -value_own_xtr. iFrame.
    iApply Hfill. rewrite Hfind //.
  Qed.


  Lemma fill_type_valueT_val T Δ Δ' Δ'' v ly A B:
    sequence_classes [
      uni v;
      qenvs_find_atom SHALLOW Δ (v ◁ᵥ A) Δ';
      not_in v Δ';
      fill_type T Δ' A Δ'' B
    ] →
    fill_type T Δ (valueT v ly) Δ'' B.
  Proof.
    rewrite /sequence_classes !Forall_cons Forall_nil /id /=.
    intros (? & Hfind & Hnot_in & Hfill & _).
    iIntros (r) "[Hr HΔ]". iApply Hfill.
    rewrite Hfind. iDestruct "HΔ" as "[Hv HΔ]".
    iFrame. destruct T; simpl.
    - iApply (valueT_elim_loc with "Hr"). iFrame.
    - iDestruct (vtr_valueT_elim with "Hr") as "[% ?]".
      by simplify_eq.
  Qed.

  Lemma fill_type_valueT_val_copy T Δ Δ' Δ'' v ly A B:
    sequence_classes [
      qenvs_find_atom DEEP Δ (v ◁ᵥ A) Δ';
      Copy A;
      fill_type T Δ A Δ'' B
    ] →
    fill_type T Δ (valueT v ly) Δ'' B.
  Proof.
    rewrite /sequence_classes !Forall_cons Forall_nil /id /=.
    intros (Hfind & Hcp & Hfill & _).
    iIntros (r) "[Hr HΔ]". iApply Hfill.
    rewrite Hfind. iDestruct "HΔ" as "[#Hv HΔ]".
    iFrame "Hv". iFrame. destruct T; simpl.
    - iApply (valueT_elim_loc with "Hr Hv").
    - iDestruct (vtr_valueT_elim with "Hr") as "[% ?]".
      by simplify_eq.
  Qed.

  Lemma fill_type_structT ty Δ Δ' s T U:
    fill_type_list ty Δ T Δ' U →
    fill_type ty Δ (structT s T) Δ' (structT s U).
  Proof.
    iIntros (Htys x) "[Hs HΔ]"; simpl in *. destruct ty; simpl in *.
    - iPoseProof (struct_focus_ty_own_all with "Hs") as "[Hty Hwand]".
      specialize (Htys (member_locs x s)).
      iPoseProof (Htys with "[$Hty $HΔ]") as "[Hty $]".
      by iApply "Hwand".
    - iPoseProof (struct_focus_ty_own_all_val with "Hs") as "[Hty Hwand]".
      specialize (Htys (member_vals x s)).
      iPoseProof (Htys with "[$Hty $HΔ]") as "[Hty $]".
      by iApply "Hwand".
  Qed.

  Lemma fill_type_slicesT ty Δ Δ' len T U As Bs:
    sequence_classes [
      Simpl (As.*2) T;
      fill_type_list ty Δ T Δ' U;
      Simpl (zip As.*1 U) Bs
    ] →
    fill_type ty Δ (slicesT len As) Δ' (slicesT len Bs).
  Proof.
    rewrite /sequence_classes /Simpl !Forall_cons Forall_nil /id /=.
    intros (<- & Htys & <- & _).
    iIntros (x) "[Hs HΔ]"; simpl in *. destruct ty; simpl in *.
    - iPoseProof (slicesT_borrow_all with "Hs") as "[Hty Hwand]".
      specialize (Htys (slices_locs x As)).
      iPoseProof (Htys with "[$Hty $HΔ]") as "[Hty $]".
      by iApply "Hwand".
    - iPoseProof (slicesT_borrow_all_val with "Hs") as "[Hty Hwand]".
      specialize (Htys (slices_vals x As)).
      iPoseProof (Htys with "[$Hty $HΔ]") as "[Hty $]".
      by iApply "Hwand".
  Qed.


  Lemma fill_type_ownT T r Δ A Δ' B:
    fill_type LOC_TYPE Δ A Δ' B →
    fill_type T Δ (ownT r A) Δ' (ownT r B).
  Proof.
    rewrite /fill_type.
    iIntros (Hent l) "Ctx".
    rewrite -value_own_xtr -bi.sep_assoc Hent.
    rewrite bi.sep_assoc value_own_xtr //.
  Qed.


  Lemma fill_type_list_nil T Δ :
    fill_type_list T Δ nil Δ nil.
  Proof. done. Qed.

  Lemma fill_type_list_cons X Δ Δ' Δ'' A B T U :
    fill_type X Δ A Δ' B →
    fill_type_list X Δ' T Δ'' U →
    fill_type_list X Δ (A::T) Δ'' (B::U).
  Proof.
    rewrite /fill_type /fill_type_list //=.
    iIntros (HAB HTU L); destruct L as [|l L]; first by iIntros "[? _]".
    iIntros "[[Hl HT] HΔ]". simpl.
    iPoseProof (HAB with "[$Hl $HΔ]") as "[$ HΔ']".
    iApply HTU. iFrame.
  Qed.


  Definition fill_ctx (L: list loc_or_val) (Δ: qenvs Σ) (Δ': qenvs Σ) :=
    qenvs_ctx_prop Δ ⊢ qenvs_ctx_prop Δ'.

  Lemma fill_ctx_nil Δ:
    fill_ctx nil Δ Δ.
  Proof. done. Qed.

  Lemma fill_ctx_cons_miss l L Δ Δ':
    fill_ctx L Δ Δ' →
    fill_ctx (l::L) Δ Δ'.
  Proof.
    by rewrite /fill_ctx.
  Qed.

  Lemma fill_ctx_cons_hit x X Δ Δ' Δ'' Δ''' Δ'''' A B:
    sequence_classes [
      qenvs_find_atom SHALLOW Δ (x ◁ₓ A) Δ';
      fill_type (lov_type x) Δ' A Δ'' B;
      qenvs_add_sep_assertion Δ'' (x ◁ₓ B) Δ''';
      fill_ctx X Δ''' Δ''''
    ] →
    fill_ctx (x :: X) Δ Δ''''.
  Proof.
    rewrite /sequence_classes !Forall_cons Forall_nil /id /=.
    rewrite /qenvs_find_atom /qenvs_add_sep_assertion /fill_type.
    intros (Hfind & Hty & Hadd & Hctx & _).
    iIntros "HΔ".
    rewrite Hfind; destruct x; rewrite Hty /= -Hadd Hctx //.
  Qed.

  Definition dom_ctx (Δ: list (iProp Σ)) (L: list loc_or_val) :=
    ∀ x, x ∈ L → ∃ A, x ◁ₓ A ∈ Δ.

  Lemma dom_ctx_nil :
    dom_ctx nil nil.
  Proof. rewrite /dom_ctx. intros l. set_solver. Qed.

  Lemma dom_ctx_cons_loc Δ L l A :
    dom_ctx Δ L →
    dom_ctx (l ◁ₗ A :: Δ) (LOC l :: L).
  Proof.
    rewrite /dom_ctx. intros HΔ r . set_solver.
  Qed.

  Lemma dom_ctx_cons_val Δ L v A :
    dom_ctx Δ L →
    dom_ctx (v ◁ᵥ A :: Δ) (VAL v :: L).
  Proof.
    rewrite /dom_ctx. intros HΔ r . set_solver.
  Qed.

  Lemma dom_ctx_skip Δ L P :
    dom_ctx Δ L →
    dom_ctx (P :: Δ) L.
  Proof.
    rewrite /dom_ctx. intros HΔ r . set_solver.
  Qed.

  (* We can at an arbitrary point in the abduction fill the context
    (see the lemma below). To not slow down the proof search unnecessarily,
    we only atomatically fill the context at certain program points.
    *)
  Lemma abduct_fill_ctx Δ Δ' Ω L P R:
    qenvs_sep_assertions Δ Ω →
    dom_ctx Ω L →
    fill_ctx L Δ Δ' →
    abduct Δ' P R →
    abduct Δ P R.
  Proof.
    rewrite /abduct. intros Hsep Hdom Hfill Habd.
    iIntros "[HΔ HR]". iApply Habd. iFrame "HR".
    unfold fill_ctx in Hfill. by iApply Hfill.
  Qed.

  (* instructions for triggering filling *)
  Definition type_fill (x: loc_or_val) (A: type Σ) (P: type Σ → iProp Σ) : iProp Σ :=
    x ◁ₓ A -∗ ∃ B, x ◁ₓ B ∗ P B.

  Definition type_fill_ctx (P: iProp Σ) := P.

  Lemma abduct_type_fill_ctx Δ Δ' Ω L P R:
    qenvs_sep_assertions Δ Ω →
    dom_ctx Ω L →
    fill_ctx L Δ Δ' →
    abduct Δ' P R →
    abduct Δ (type_fill_ctx P) R.
  Proof. rewrite /type_fill_ctx. by eapply abduct_fill_ctx. Qed.


  Lemma abduct_type_fill Δ Δ' x A B P R:
    fill_type (lov_type x) Δ A Δ' B →
    abduct Δ' (P B) R →
    abduct Δ (type_fill x A P) R.
  Proof.
    intros Hfill Habd; rewrite /type_fill.
    iIntros "[Δ R] Hl".
    destruct x; iDestruct (Hfill with "[$Δ $Hl]") as "[Hl Δ']".
    all: iExists _; iFrame; iApply Habd; iFrame.
  Qed.


  Global Instance abduct_step_type_fill_ctx Δ Δ' Ω L P R:
    qenvs_sep_assertions Δ Ω →
    dom_ctx Ω L →
    fill_ctx L Δ Δ' →
    AbductStep (abduct Δ' P R) Δ (type_fill_ctx P) R.
  Proof. intros ????; by eapply abduct_type_fill_ctx. Qed.

  Global Instance abduct_step_type_fill Δ Δ' x A B P R:
    fill_type (lov_type x) Δ A Δ' B →
    AbductStep (abduct Δ' (P B) R) Δ (type_fill x A P) R.
  Proof. intros ??. by eapply abduct_type_fill. Qed.

End context_filling.


Global Typeclasses Opaque type_fill type_fill_ctx.

Existing Class dom_ctx.
Global Existing Instance dom_ctx_nil.
Global Existing Instance dom_ctx_cons_loc | 2.
Global Existing Instance dom_ctx_cons_val | 2.
Global Existing Instance dom_ctx_skip | 100.

Existing Class fill_type.
Global Existing Instance fill_type_placeT.
Global Existing Instance fill_type_structT.
Global Existing Instance fill_type_slicesT.
Global Existing Instance fill_type_ownT | 3.
Global Existing Instance fill_type_valueT_loc | 1.
(* Global Existing Instance fill_type_valueT_unused_loc | 2. *)
Global Existing Instance fill_type_valueT_val | 3 .
Global Existing Instance fill_type_valueT_val_copy | 4.
Global Existing Instance fill_type_return | 100.

Existing Class fill_type_list.
Global Existing Instance fill_type_list_nil.
Global Existing Instance fill_type_list_cons.

Existing Class fill_ctx.
Global Existing Instance fill_ctx_nil.
Global Existing Instance fill_ctx_cons_hit | 2.
Global Existing Instance fill_ctx_cons_miss | 10.

