From iris.prelude Require Import options prelude.
From iris.base_logic Require Import iprop.
From refinedc Require Import base.
From quiver.base Require Import dprop annotations.

(* a type class opague identity function *)
Definition opaque {A: Type} (a: A): A := a.
Global Typeclasses Opaque opaque.


(* sealing mechanism to avoid accidental unification of type classes *)
Definition sealed_aux : @opaque.(seal).
Proof. by eexists. Qed.
Definition sealed {A} := sealed_aux.(unseal) A.
Definition sealed_eq_aux: @sealed = @opaque := seal_eq sealed_aux.
Lemma sealed_eq {A} (a: A) : sealed a = a.
Proof. by rewrite sealed_eq_aux. Qed.

Global Typeclasses Opaque sealed.


(* a simple type class for forcing unification *)
Class unify {A: Type} (a b: A) :=
  unifiable: a = b.

Global Hint Mode unify - - - : typeclass_instances.


Global Instance unify_refl {A: Type} (a: A):
  unify a a | 1.
Proof. rewrite /unify //. Qed.

(* we built some special automation for inferring functions *)
Ltac fill_ctx K x :=
  let c := context K [x] in exact c.

Ltac unify_fun :=
  match goal with
  | [ |- unify (?F ?x) ?R ] =>
      is_evar F;
      is_ground x;
      is_ground R;
      match R with
      | context K [x] =>
          let G := constr:(λ x, ltac:(fill_ctx K x)) in
          unify F G; apply unify_refl
      end
  | [ |- unify ?R (?F ?x) ] =>
      is_evar F;
      is_ground x;
      is_ground R;
      match R with
      | context K [x] =>
          let G := constr:(λ x, ltac:(fill_ctx K x)) in
          unify F G; apply unify_refl
      end
  end.

Global Hint Extern 2 (unify _ _) => unify_fun : typeclass_instances.





(* Trivial side conditions are side conditions
   that we expect to be solvable directly
   (i.e., without additional assumptions)
   in the current proof context.
*)
Class Trivial (φ: Prop) := trivial_true : φ.
Global Hint Mode Trivial ! : typeclass_instances.




Ltac solve_trivial := simpl; done.

Global Instance trival_eq_refl {A} (a: A): Trivial (a = a).
Proof. done. Qed.

Global Instance trivial_True : Trivial True.
Proof. done. Qed.

Global Hint Extern 10 (Trivial _) => (rewrite /Trivial; simpl; solve_trivial) : typeclass_instances.
Global Hint Extern 3 (Trivial (@elem_of Z int_type _ _ _)) => (rewrite /Trivial; done) : typeclass_instances.


(* TODO: Is there a better place for this? *)
(* TODO: Can we add an instance for the other direction?
   The following does not work since unification does not do the beta expansion: *)
(* Global Instance Z_mul_inj_r z `{!Trivial (z ≠ 0%Z)} : Inj (=) (=) (λ x, Z.mul x z). *)
Global Instance Z_mul_inj_l z `{!Trivial (z ≠ 0%Z)} : Inj (=) (=) (Z.mul z).
Proof.
  unfold Trivial in *. move => x y Heq.
  rewrite -(Z_div_mult_full x z) // -(Z_div_mult_full y z) //.
  rewrite (Z.mul_comm x) (Z.mul_comm y) Heq. done.
Qed.

Global Instance Z_inj_l z : Inj (=) (=) (Z.add z).
Proof.
  unfold Trivial in *. move => x y Heq. lia.
Qed.


Global Instance Z_inj_le_mul_l z `{!Trivial (z > 0%Z)} : Inj (≤) (≤) (Z.mul z).
Proof.
  unfold Trivial in *. move => x y Heq.
  rewrite -(Z_div_mult_full x z); last lia.
  rewrite -(Z_div_mult_full y z); last lia.
  rewrite (Z.mul_comm x) (Z.mul_comm y).
  eapply Z_div_le; lia.
Qed.


Global Instance Z_inj_le_add_l z : Inj (≤) (≤) (Z.add z).
Proof.
  unfold Trivial in *. move => x y Heq. lia.
Qed.


Class Invertible {X Y: Type} (f: X → Y) (g: Y → X) :=
  {
    invertible_eq : ∀ x y, x = f y → y = g x
  }.


Global Instance invertible_add (z: Z) : Invertible (Z.add z) (Z.add (-z)).
Proof.
  split; intros; lia.
Qed.

Global Instance invertible_mul (z: Z) `{!Trivial (z ≠ 0)} : Invertible (Z.mul z) (λ x, Z.div x z) | 2.
Proof.
  split; intros. subst.
  rewrite Z.mul_comm Z_div_mult_full //.
Qed.

Global Instance invertible_neg : Invertible (Z.opp) (Z.opp).
Proof.
  split; intros; lia.
Qed.

Global Instance invertible_invert : Invertible (Z.mul (-1)) (Z.mul (-1)) | 1.
Proof.
  split; intros; lia.
Qed.

Global Instance invertible_neg_bool : Invertible negb negb.
Proof.
  split; intros x y; destruct x, y; naive_solver.
Qed.

Global Instance invertible_rev X : Invertible (@rev X) (@rev X).
Proof.
  split; intros. subst; rewrite rev_involutive; done.
Qed.

Global Instance invertible_succ : Invertible S pred.
Proof.
  split; intros; lia.
Qed.


(* Solve is used to solve sidecondition and by default dispatches to a
[lia-based] solver. *)
Class Solve (φ: Prop) := solve_true : φ.
Global Hint Mode Solve ! : typeclass_instances.

Lemma solve_unfold_int_elem_of_it (z : Z) (it : int_type) :
  z ∈ it = (min_int it ≤ z ∧ z ≤ max_int it).
Proof. done. Qed.

Lemma solve_remove_double_negation P `{!Decision P}:
¬ ¬ P → P.
Proof. intros. destruct (decide P); done. Qed.


Ltac reduce_closed_Z :=
  repeat match goal with
    | |- context [Z.of_nat (ly_size ?e)] => reduce_closed (Z.of_nat (ly_size e))
    | H : context [Z.of_nat (ly_size ?e)] |- _ => reduce_closed (Z.of_nat (ly_size e))
    | |- context [ly_size ?e] => reduce_closed (ly_size e)
    | H : context [ly_size ?e] |- _ => reduce_closed (ly_size e)
    | |- context [min_int ?e] => reduce_closed (min_int e)
    | H : context [min_int ?e] |- _ => reduce_closed (min_int e)
    | |- context [max_int ?e] => reduce_closed (max_int e)
    | H : context [max_int ?e] |- _ => reduce_closed (max_int e)
    end.

Ltac clear_maps := repeat match goal with | H : gmap _ _ |- _ => clearbody H end.


Ltac solve_lia :=
  try done; lia.

Ltac solve_eq :=
  simplify_eq;
  autorewrite with simplify_pure_rewrite_db;
  done.

Ltac solve_solve :=
  (* clear the body of the function since otherwise simpl in * might diverge *)
  clear_maps;
  (* TODO: Should should we use simpl in * instead? There does not
  seem to be a big difference in performance. *)
  cbn [dProp_prop dtrue dfalse dle dlt dgt dge deq dne del dand dor dimpl diff dneg fst snd] in *;
  (* simpl in *; *)
  repeat match goal with
    | H : ?xs !! ?i = Some ?y |- _ => learn_hyp (lookup_lt_Some xs i y H)
    | H: ¬ ¬ ?P |- _ => apply (@solve_remove_double_negation P _) in H
    end;
  try rewrite -> solve_unfold_int_elem_of_it in *;
  reduce_closed_Z;
  (try split_and!);
  (solve_lia || solve_eq).

Global Hint Extern 10 (Solve _) => (rewrite /Solve; solve_solve) : typeclass_instances.



(* Computable side conditions are side conditions
   that we expect to be solvable during type class
   inference by using merely computation (e.g.,
   the result of [length [1;2;3]]).
*)
Class Computable {A: Type} (a b: A) :=
  compute_result: a = b.

Global Hint Extern 1 (Computable _ _) => (rewrite /Computable; reflexivity) : typeclass_instances.
Global Hint Mode Computable - ! ! : typeclass_instances.

Class Simpl {A: Type} (a b: A) :=
  simpl_terms: a = b.
Global Hint Extern 1 (Simpl _ _) => (rewrite /Simpl; simpl; reflexivity) : typeclass_instances.
Global Hint Mode Simpl - - - : typeclass_instances.


Class CBN {A: Type} (a b: A) :=
  cbn_terms: a = b.
Global Hint Extern 1 (CBN _ _) => (rewrite /CBN; cbn; reflexivity) : typeclass_instances.
Global Hint Mode CBN - - - : typeclass_instances.


(* to normalize a term, we use vm compute *)
Class Normalize {X: Type} (x y: X) :=
  normalize_eq: x = y.

Global Hint Mode Normalize - ! - : typeclass_instances.
Global Hint Extern 2 (Normalize _ _) => (rewrite /Normalize; vm_compute; reflexivity) : typeclass_instances.


Class ComputeMapLookup {X Y: Type} `{!EqDecision X, !Countable X} (m: gmap X Y) (x: X) (y: Y) :=
  compute_map_lookup: m !! x = Some y.
Global Hint Mode ComputeMapLookup - - - - ! ! - : typeclass_instances.

Ltac compute_map_lookup :=
  lazymatch goal with
  | |- ComputeMapLookup ?Q ?x ?y => rewrite /ComputeMapLookup; try (is_var Q; unfold Q)
  | _ => fail "unknown goal for compute_map_lookup"
  end;
  solve [repeat lazymatch goal with
  | |- <[?x:=?s]> ?Q !! ?y = Some ?res =>
    lazymatch x with
    | y => change_no_check (Some s = Some res); reflexivity
    | _ => change_no_check (Q !! y = Some res)
    end
  end ].


Global Hint Extern 4 (ComputeMapLookup _ _ _) => compute_map_lookup : typeclass_instances.



Class ComputeSubst (σ: list (string * val)) (cfg: gmap string stmt) (cfg': gmap string stmt) :=
  compute_subst: subst_stmt σ <$> cfg = cfg'.


Ltac compute_subst :=
  lazymatch goal with
  | |- ComputeSubst ?s (f_code ?fn) ?cfg =>
    rewrite /ComputeSubst /fn /f_code /LocInfo /LocInfoE;
    rewrite !fmap_insert fmap_empty;
    simpl_subst;
    (repeat rewrite subst_loop);
    reflexivity
  end.

Global Hint Extern 4 (ComputeSubst _ _ _) => compute_subst : typeclass_instances.



(* a simple type class for checking for variables *)
Class is_var {A: Type} (a: A) :=
  { mk_is_var: sealed (∃ b, a = b) }.
Global Hint Mode is_var - + : typeclass_instances.

Lemma is_var_intro {A: Type} (a: A): is_var a.
Proof.
  split. rewrite sealed_eq. exists a. reflexivity.
Qed.

Global Hint Extern 2 (is_var ?x) => (is_var x; exact (is_var_intro x)) : typeclass_instances.


(* a simple type class for asserting that an iProp is not pure *)
Class not_pure {Σ: gFunctors} (P: iProp Σ) :=
  { mk_not_pure: sealed (∃ Q, P = Q) }.
Global Hint Mode not_pure - ! : typeclass_instances.

Lemma not_pure_intro {Σ: gFunctors} (P: iProp Σ): not_pure P.
Proof. split. rewrite sealed_eq. exists P. reflexivity. Qed.


Ltac not_pure :=
  match goal with
  |- not_pure (?P) =>
    match P with
    | bi_pure _ => fail 1
    | _ => exact (not_pure_intro P)
    end
  end.

Global Hint Extern 1 (not_pure _) => not_pure : typeclass_instances.

(* finding a hypothesis in the context *)
Class ctx_hyp (A: Prop) : Prop := mk_ctx_hyp: A.
Global Hint Mode ctx_hyp ! : typeclass_instances.

Lemma ctx_hyp_intro {A: Prop} (a: A): ctx_hyp A.
Proof. done. Defined.

Ltac ctx_hyp :=
  match goal with
  | [H: ?A |- ctx_hyp ?B] => unify A B; apply (ctx_hyp_intro H)
  end.

Global Hint Extern 1 (ctx_hyp _) => (ctx_hyp) : typeclass_instances.


(* Removing Hypothesis *)
Definition ctx_remove (φ ψ: Prop) := ψ.
Global Typeclasses Opaque ctx_remove.

Lemma ctx_remove_intro (φ ψ : Prop) :
  ψ →
  ctx_remove φ ψ.
Proof. rewrite /ctx_remove //. Qed.

Ltac ctx_remove_hyp A :=
  match goal with
  | [H: A |- _] =>  clear H
  end.



(* sequence classes *)
Definition sequence_classes (φ: list Prop) : Prop :=
  Forall id φ.

Existing Class sequence_classes.
Global Hint Mode sequence_classes ! : typeclass_instances.

Lemma sequence_classes_nil : sequence_classes nil.
Proof. rewrite /sequence_classes. rewrite Forall_nil. done. Qed.

Lemma sequence_classes_cons (φ: Prop) (Φ: list Prop) :
  φ → sequence_classes Φ → sequence_classes (φ :: Φ).
Proof. rewrite /sequence_classes. rewrite Forall_cons. done. Qed.

Ltac sequence_classes :=
  match goal with
  | |- sequence_classes nil => apply sequence_classes_nil
  | |- sequence_classes (?φ :: ?Φ) =>
    eapply sequence_classes_cons; [once solve[typeclasses eauto]|sequence_classes]
  end.

Global Hint Extern 1 (sequence_classes _) => once sequence_classes : typeclass_instances.


(* sequence with backtracking *)
Definition sequence_classes_backtracked (φ: list Prop) : Prop :=
  Forall id φ.

Existing Class sequence_classes_backtracked.
Global Hint Mode sequence_classes_backtracked ! : typeclass_instances.

Lemma sequence_classes_backtracked_nil : sequence_classes_backtracked nil.
Proof. rewrite /sequence_classes_backtracked. rewrite Forall_nil. done. Qed.

Lemma sequence_classes_backtracked_cons (φ: Prop) (Φ: list Prop) :
  φ → sequence_classes_backtracked Φ → sequence_classes_backtracked (φ :: Φ).
Proof. rewrite /sequence_classes_backtracked. rewrite Forall_cons. done. Qed.

Ltac sequence_classes_backtracked :=
  match goal with
  | |- sequence_classes_backtracked nil => apply sequence_classes_backtracked_nil
  | |- sequence_classes_backtracked (?φ :: ?Φ) =>
    eapply sequence_classes_backtracked_cons; [solve[typeclasses eauto]|sequence_classes_backtracked]
  end.

Global Hint Extern 1 (sequence_classes_backtracked _) => once sequence_classes_backtracked : typeclass_instances.


(* one of classes *)
Definition one_of_classes (φ: list Prop) : Prop :=
  Exists id φ.

Existing Class one_of_classes.
Global Hint Mode one_of_classes ! : typeclass_instances.

Lemma one_of_classes_cons_first (φ: Prop) (Φ: list Prop) :
  φ → one_of_classes (φ :: Φ).
Proof.
  rewrite /one_of_classes. rewrite Exists_cons /=. auto.
Qed.

Lemma one_of_classes_cons_rest (φ: Prop) (Φ: list Prop) :
  one_of_classes Φ → one_of_classes (φ :: Φ).
Proof.
  rewrite /one_of_classes. rewrite Exists_cons /=. auto.
Qed.

Ltac one_of_classes :=
  match goal with
  | |- one_of_classes nil => fail
  | |- one_of_classes (?φ :: ?Φ) =>
      (eapply one_of_classes_cons_first; once solve[typeclasses eauto]) ||
      (eapply one_of_classes_cons_rest; [one_of_classes])
  end.

Global Hint Extern 1 (one_of_classes _) => once one_of_classes : typeclass_instances.


(* ONCE TYPECLASS *)
Definition once_tc (φ: Prop) : Prop := φ.

Existing Class once_tc.
Global Hint Mode once_tc ! : typeclass_instances.
Global Typeclasses Opaque once_tc.

Lemma once_tc_proof (φ: Prop): φ → once_tc φ.
Proof. done. Qed.

Ltac once_tc :=
  match goal with
  | |- once_tc ?φ =>
    eapply once_tc_proof; [once solve[typeclasses eauto]]
  end.

Global Hint Extern 1 (once_tc _) => once_tc : typeclass_instances.


(* type class for linear integer arithmetic *)
Definition lia (P: Prop) := P.

Existing Class lia.
Global Hint Mode lia ! : typeclass_instances.
Global Hint Extern 2 (lia _) => (rewrite /lia; lia) : typeclass_instances.

(* type class that will be instantiated with an evar *)
Definition tc_evar {A: Type} (P: A → Prop) : Prop := ∃ a, P a.

Existing Class tc_evar.
Global Hint Mode tc_evar - - : typeclass_instances.

Global Instance tc_evar_proof (A: Type) (P: A → Prop) (a: A):
  P a → tc_evar P.
Proof. rewrite /tc_evar. eauto. Qed.



Class not_in {X Y: Type} (x: X) (y: Y) :=
  { mk_not_in : sealed (x = x ∧ y = y) }.
Global Hint Mode not_in - - ! ! : typeclass_instances.

Lemma not_in_intro {X Y: Type} (x: X) (y: Y) : not_in x y.
Proof. split. rewrite sealed_eq. split; done. Qed.

Ltac not_in :=
  match goal with
  | |- not_in ?x ?y =>
      match y with
      | context K [x] => fail 1
      | _ => exact (not_in_intro x y)
      end
  end.

Global Hint Extern 2 (not_in _ _) => (not_in) : typeclass_instances.

Class is_in {X Y: Type} (x: X) (y: Y) :=
  { mk_is_in : sealed (∃ a b, a = x ∧ b = y) }.
Global Hint Mode is_in - - ! ! : typeclass_instances.

Lemma is_in_intro {X Y: Type} (x: X) (y: Y) : is_in x y.
Proof. split. rewrite sealed_eq. eauto. Qed.

Ltac is_in :=
  match goal with
  | |- is_in ?x ?y =>
    lazymatch y with context[x] =>
      exact (is_in_intro x y)
    end
  end.

Global Hint Extern 2 (is_in _ _) => (is_in) : typeclass_instances.

Definition is_syn_eq {A} (a b : A) := a = b.
Existing Class is_syn_eq.
Global Hint Mode is_syn_eq ! - - : typeclass_instances.

Ltac is_syn_eq :=
  lazymatch goal with
  | |- is_syn_eq ?x ?y =>
    let x := eval lazy [projT1 projT2 fst snd] in x in
    let y := eval lazy [projT1 projT2 fst snd] in y in
    lazymatch x with y => exact (@eq_refl _ x) end
  end.

Global Hint Extern 2 (is_syn_eq _ _) => (is_syn_eq) : typeclass_instances.


(* printing terms *)
Class print {X: Type} (x: X) :=
  print_core: sealed True.
Global Hint Mode print - + : typeclass_instances.

Lemma do_print {X: Type} (s: X) : print s.
Proof. rewrite /print sealed_eq //. Qed.

Ltac print_loud := match goal with |- print ?x => idtac x; apply do_print end.
Ltac print_silent := apply do_print.

Global Hint Extern 2 (print _) => (print_loud) : typeclass_instances.


(* This universal quantifer will automatically instantiate evars that are
   applied to it with lamdas to not trip up unification. That is, Coq's
   unification algorithm is good, but not flawless. In particular, if ?f is an
   evar then Coq can have issues unifying ?f x with, for example, 3 = x. It does not
   always have these issues, but in some cases. Since we build up the precondition as an evar, we
   need this unification to work in various places (also during type class
   search). The ∀! makes sure that f is instantiated with a lambda and that ?f x
   is simplified to something like ?g{x := x}  where g is a new evar. *)
Definition evar_forall {A: Type} (P: A → Prop) : Prop := ∀ a, P a.
Global Arguments  evar_forall : simpl never.
Global Typeclasses Opaque evar_forall.

Notation "'∀!' x .. y , P" :=
  (evar_forall (λ x, .. (evar_forall (λ y, P)) ..))
  (at level 200, x binder, y binder, right associativity,
   format "'[hv' '∀!'  x  ..  y ,  '/ ' P ']'") : type_scope.


Ltac is_forall_type T :=
  match T with
  | ∀ _, _ => idtac
  end.

(* The tactic [inst_evar_lam] instantiates a function evar [f]
   with a lambda to help unification. *)

Ltac inst_evar_lam f :=
  is_evar f;
  let T := type of f in
  is_forall_type T;
  let g := fresh in
  epose (g := (ltac:(refine ((λ x, _): T))));
  unify f g; clear g.

Ltac inst_evar_rec f :=
  inst_evar_lam f;
  repeat lazymatch goal with
  | |- context [f ?a] =>
    let g := eval simpl in (f a) in
    (* NOTE: change_no_check here leads to strange Coq bugs *)
    change (f a) with g;
    try inst_evar_rec g
  end.

Ltac inst_evars a :=
  repeat match goal with
  | |- context[?f a] =>
    is_evar f; inst_evar_rec f
  end.

Ltac evar_forall_intro :=
  match goal with
  | |- evar_forall ?P =>
    let x := fresh "x" in
    intro x; inst_evars x
  end.

Existing Class evar_forall.
Global Hint Extern 1 (evar_forall _) => (evar_forall_intro; shelve) : typeclass_instances.


(* fun_evar *)
Definition fun_evar {A: Type} (a: A) (φ: A → Prop): Prop := φ a.
Global Typeclasses Opaque fun_evar.

Ltac fun_var_unfold :=
  match goal with
  | |- fun_evar ?a ?φ =>
    let a' := eval simpl in a in
    change (φ a');
    simpl
  end.

Ltac fun_evar :=
  match goal with
  | |- fun_evar (?F ?a) ?φ =>
    is_evar F;
    is_var a;
    let T := type of F in
    let G := fresh "G" in
    epose (G := (ltac:(refine ((λ a, _): T))));
    unify F G; clear G;
    fun_var_unfold
  end.

Definition fun_evar_tc {A: Type} (a: A) (φ: A → Prop): Prop := fun_evar a φ.
Existing Class fun_evar_tc.
Global Hint Mode fun_evar_tc - - - : typeclass_instances.
Global Hint Extern 0 (fun_evar_tc _ _) =>
  (rewrite {1}/fun_evar_tc; fun_evar; [apply _]) : typeclass_instances.




(* TCOr *)
Lemma tc_or_iff P1 P2:
  TCOr P1 P2 ↔ P1 ∨ P2.
Proof.
  split; intros [?|?]; eauto using TCOr_r, TCOr_l.
Qed.


(* FLAGS *)

(* flag for enabling joining *)
Inductive fancy_if : Prop := FancyIf.
Existing Class fancy_if.

Notation default_if := (TCUnless fancy_if).

(* flag for enabling strong joining *)
Inductive very_fancy_if : Prop := VeryFancyIf.
Existing Class very_fancy_if.

(* flag for enabling conjunction commuting *)
Inductive fancy_conj : Prop := FancyConj.
Existing Class fancy_conj.

Notation default_conj := (TCUnless fancy_conj).


Inductive exact_post : Prop := ExactPost.
Existing Class exact_post.