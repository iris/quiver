From iris.prelude Require Import options prelude.
From iris.proofmode Require Import tactics.
From iris.base_logic Require Import iprop.

(* decidable propositions and a case distinction operator *)
Record dProp := DProp {
  dProp_prop :> Prop;
  dProp_decidable : Decision dProp_prop;
}.
Global Existing Instance dProp_decidable.

Declare Scope decidable_prop_scope.
Delimit Scope decidable_prop_scope with DP.
Bind Scope decidable_prop_scope with dProp.

Global Instance dProp_inhabited : Inhabited dProp := populate (DProp True _).

Add Printing Constructor dProp.

Definition dtrue := DProp True _.
Definition dfalse := DProp False _.
Definition dle (x y: Z) := DProp (x ≤ y)%Z _.
Definition dlt (x y: Z) := DProp (x < y)%Z _.
Definition dgt (x y: Z) := DProp (x > y)%Z _.
Definition dge (x y: Z) := DProp (x >= y)%Z _.
Definition deq {X} `{EqDecision X} (x y: X) := DProp (x = y)%Z _.
Definition dne {X} `{EqDecision X} (x y: X) := DProp (x ≠ y)%Z _.
Definition del {X} `{EqDecision X} (x: X) (L: list X) := DProp (x ∈ L) _.
Definition dand (P Q: dProp) := DProp (P ∧ Q) _.
Definition dor (P Q: dProp) := DProp (P ∨ Q) _.
Definition dimpl (P Q: dProp) := DProp (P → Q) _.
Definition diff (P Q: dProp) := DProp (P ↔ Q) _.
Definition dneg (P: dProp) := DProp (¬ P) _.


Definition dprop_iff (φ ψ: dProp) : Prop := φ ↔ ψ.

Notation "'True'" := dtrue : decidable_prop_scope.
Notation "'False'" := dfalse : decidable_prop_scope.
Infix "≤" := dle : decidable_prop_scope.
Infix "<" := dlt : decidable_prop_scope.
Infix ">" := dgt : decidable_prop_scope.
Infix ">=" := dge : decidable_prop_scope.
Infix "=" := deq : decidable_prop_scope.
Infix "≠" := dne : decidable_prop_scope.
Infix "∈" := del : decidable_prop_scope.
Infix "∧" := dand : decidable_prop_scope.
Infix "∨" := dor : decidable_prop_scope.
Infix "→" := dimpl : decidable_prop_scope.
Infix "↔" := diff : decidable_prop_scope.
Notation "'¬' P" := (dneg P) : decidable_prop_scope.

Definition case {X: Type} (φ: dProp) (P Q: X) := if decide φ then P else Q.

Global Instance dProp_equiv: Equiv dProp := dprop_iff.

Global Instance dProp_equiv_equivalence: Equivalence dProp_equiv.
Proof.
  split; unfold dProp_equiv, dprop_iff; eauto.
  intros ?????. etrans; eauto.
Qed.

Global Instance case_proper {X: Type} (R : relation X):
  Proper (dprop_iff ==> R ==> R ==> R) (case).
Proof.
  intros [φ ?] [ψ ?]; unfold dprop_iff; simpl; intros Hiff.
  intros x1 x2 ? y1 y2 ?; rewrite /case /=.
  destruct (decide φ); destruct (decide ψ); naive_solver.
Qed.


Section case_lemmas.
  Context {Σ: gFunctors}.

  Lemma case_ex_l {A} φ (P: A → iProp Σ) (Q: iProp Σ) :
    (∃ x, case φ (P x) Q) ⊢ case φ (∃ x, P x) Q.
  Proof.
    rewrite /case; destruct decide; auto.
    iIntros "[%_ $]".
  Qed.

  Lemma case_ex_r {A} φ (P: iProp Σ) (Q: A → iProp Σ) :
    (∃ x, case φ P (Q x)) ⊢ case φ P (∃ x, Q x).
  Proof.
    rewrite /case; destruct decide; auto.
    iIntros "[%_ $]".
  Qed.

  Lemma case_all_l {A} `{!Inhabited A} φ (P: A → iProp Σ) (Q: iProp Σ) :
    (∀ x, case φ (P x) Q) ⊢ case φ (∀ x, P x) Q.
  Proof.
    rewrite /case; destruct decide; auto.
    iIntros "H". by iApply ("H" $! inhabitant).
  Qed.

  Lemma case_all_r {A} `{!Inhabited A} φ (P: iProp Σ) (Q: A → iProp Σ) :
    (∀ x, case φ P (Q x)) ⊢ case φ P (∀ x, Q x).
  Proof.
    rewrite /case; destruct decide; auto.
    iIntros "H". by iApply ("H" $! inhabitant).
  Qed.

  Lemma case_same φ {X: Type} (x: X):
    case φ x x = x.
  Proof.
    rewrite /case; destruct decide; naive_solver.
  Qed.

End case_lemmas.

Global Arguments DProp _ {_}.
