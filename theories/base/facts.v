From caesium Require Export lifting proofmode tactics notation.
Set Default Proof Using "Type".


(* copied from RefinedC, should be upstreamed *)
Lemma and_proper (A B C : Prop) :
  (A → B ↔ C) →
  (A ∧ B) ↔ (A ∧ C).
Proof. naive_solver. Qed.


Lemma and_pers_sep {Σ: gFunctors} (P Q R: iProp Σ) :
  (P ∧ Q) ∗ (Q -∗ □ R) ⊢ P ∗ R.
Proof.
  rewrite bi.sep_and_r.
  etrans; first (etrans; last first).
  { eapply bi.persistently_and_sep_r_1. }
  - iIntros "HPQ". iSplit.
    + iDestruct "HPQ" as "[[HP _] _]". by iApply "HP".
    + iDestruct "HPQ" as "[_ [HQ HR]]". by iApply ("HR" with "HQ").
  - iIntros "[HP #HR]". by iFrame.
Qed.

(* to upstream*)
Lemma zip_length {X Y: Type} (L: list X) (R: list Y):
  length L = length R → length (zip L R) = length L.
Proof.
  induction L as [|l L IH] in R |-*; destruct R; simpl; naive_solver.
Qed.


Lemma list_lookup_total_lookup A  (xs : list A) j x `{!Inhabited A}:
  (j < length xs)%nat →
  xs !!! j = x ↔ xs !! j = Some x.
Proof. rewrite list_lookup_alt. naive_solver lia. Qed.

Lemma list_to_set_swap {X: Type} `{Countable X} `{!Inhabited X} (xs: list X) (i j: nat) :
  i < length xs →
  j < length xs →
  list_to_set
  (<[i:=xs !!! j]>
      (<[j:=xs !!! i]> xs)) = (list_to_set xs: gset X).
Proof.
  intros ??. set_unfold. intros x.
  rewrite !elem_of_list_lookup.
  do 2 setoid_rewrite list_lookup_insert_Some.
  setoid_rewrite insert_length.
  setoid_rewrite list_lookup_total_lookup; [|lia..].
  split; [naive_solver lia|].
  move => [k Hk]. move: (Hk) => /(lookup_lt_Some _ _ _)?.
  destruct (decide (k = i)); destruct (decide (k = j)); naive_solver lia.
Qed.


Lemma foldr_impl_Forall G Ps :
  foldr (λ P Q : Prop, P → Q) G Ps ↔ (Forall id Ps → G).
Proof.
  elim: Ps. { naive_solver. }
  move => /= ?? ->. rewrite Forall_cons. naive_solver.
Qed.

Section big_op.
Context {PROP : bi}.
Implicit Types P Q : PROP.
Implicit Types Ps Qs : list PROP.
Implicit Types A : Type.
Lemma big_sepL_sepL2_diag_iff {A} (Φ : nat → A → A → PROP) (l : list A) :
  ([∗ list] k↦y ∈ l, Φ k y y) ⊣⊢
  ([∗ list] k↦y1;y2 ∈ l;l, Φ k y1 y2).
Proof.
  rewrite big_sepL2_alt. rewrite bi.pure_True // left_id.
  rewrite zip_diag big_sepL_fmap /=. done.
Qed.
End big_op.


(* int size computes *)
Fixpoint pow2 (n: nat) :=
  match n with
  | O => 1%nat
  | S n => (pow2 n * 2)%nat
  end.

Lemma pow2_exp n:
  pow2 n = (2^n)%nat.
Proof.
  induction n; simpl; lia.
Qed.

Definition int_size (it: int_type) :=
  let (sz, _) := it in pow2 sz.

Lemma int_size_ly_size it:
  int_size it = ly_size it.
Proof.
  destruct it as [sz ?]; simpl.
  rewrite pow2_exp. done.
Qed.

Lemma int_size_bytes_per_int (it: int_type) : int_size it = bytes_per_int it.
Proof.
  rewrite int_size_ly_size /ly_size /it_layout //.
Qed.

Definition ptr_size : nat :=
  8.

Global Arguments ptr_size /.


(* various standard RefinedC types are inhabited,
   which makes them eligible for inferring existential
   quantifiers over them. *)
Global Instance loc_inhabited: Inhabited loc :=
  {| inhabitant := NULL_loc |}.

Global Instance int_type_inhabited: Inhabited int_type :=
  {| inhabitant := u8 |}.

Global Instance op_type_inhabited: Inhabited op_type :=
  {| inhabitant := PtrOp |}.

Global Instance loc_decision (l1 l2: loc): Decision (l1 = l2).
Proof. solve_decision. Defined.

Global Instance loc_eq_dec: EqDecision loc.
Proof. solve_decision. Defined.


Global Instance shift_loc_inj_loc l:
  Inj eq eq (shift_loc l).
Proof.
  intros n1 n2. rewrite /shift_loc. intros ?. simplify_eq. lia.
Qed.

Global Instance shift_loc_inj_idx n:
  Inj eq eq (λ l, shift_loc l n).
Proof.
  intros l1 l2. rewrite /shift_loc. intros ?. simplify_eq.
  destruct l1, l2; simpl in *. subst. f_equal. lia.
Qed.

Global Instance shift_loc_inj_at_offset sl m:
  Inj eq eq (λ l, l at{sl}ₗ m).
Proof. rewrite /GetMemberLoc. apply _. Qed.


(* we never want to split a location into a
   provenance and an integer. *)
Global Typeclasses Opaque loc.
Global Typeclasses Opaque int_elem_of_it.
Global Typeclasses Opaque prod_relation.

Definition b2v b it := (i2v (bool_to_Z b) it).
Global Arguments b2v : simpl never.
Global Typeclasses Opaque b2v.

(* integer conversions *)
Lemma i2v_val_to_Z it z:
  z ∈ it → val_to_Z (i2v z it) it = Some z.
Proof.
  intros Hel. unfold i2v. eapply val_to_of_Z.
  edestruct (val_of_Z_is_Some None) as [v He]; first done.
  rewrite He; simpl. done.
Qed.

Lemma val_to_Z_i2v n m it:
  val_to_Z (i2v n it) it = Some m → n ∈ it → n = m.
Proof.
  intros Hval Hel.
  eapply i2v_val_to_Z in Hel.
  rewrite Hel in Hval.
  by simplify_eq.
Qed.

Lemma val_of_Z_inv n it v:
  val_of_Z n it None = Some v → v = i2v n it ∧ n ∈ it.
Proof.
  rewrite /i2v. intros Hval. rewrite Hval; simpl.
  eapply val_to_of_Z in Hval.
  eapply val_to_Z_in_range in Hval as Hran.
  split; done.
Qed.

Lemma val_of_Z_i2v n it:
  n ∈ it → val_of_Z n it None = Some (i2v n it).
Proof.
  intros Hran. rewrite /i2v. destruct val_of_Z eqn: Heq; first done.
  eapply (val_of_Z_is_Some None) in Hran as [v' ?]. simplify_eq.
Qed.

Lemma val_of_bool_b2v b:
  val_of_bool b = b2v b u8.
Proof.
  eapply val_of_bool_iff_val_of_Z.
  rewrite val_of_Z_bool //.
Qed.


Lemma val_to_byte_prov_i2v n it:
  val_to_byte_prov (i2v n it) = None.
Proof.
  rewrite /i2v.
  destruct (val_of_Z) eqn: Heq; simpl.
  - by eapply val_of_Z_to_prov.
  - destruct bytes_per_int; done.
Qed.


Ltac i2v_simpl :=
  match goal with
  | [H: val_to_Z (i2v _ _) _ = Some _ |- _] =>
    eapply val_to_Z_i2v in H; [subst|done]
  | [H: val_of_Z (bool_to_Z _) _ None = _ |- _] =>
    rewrite val_of_Z_bool in H
  | [H: val_of_Z ?n ?it None = Some ?v |- _] =>
    eapply val_of_Z_inv in H as [? ?]; subst v
  end.

Lemma val_to_loc_NULL l:
  val_to_loc NULL = Some l ↔ l = NULL_loc.
Proof.
  rewrite /NULL /val_to_loc /= bool_decide_decide.
  destruct decide; last by naive_solver.
  naive_solver.
Qed.

Definition zero_bytes (n: nat) : val :=
  repeat (MByte byte0 None) n.

Lemma zero_in_int_type (it: int_type):
  0 ∈ it.
Proof.
  split; first apply min_int_le_0.
  etrans; last eapply max_int_ge_127. lia.
Qed.

Lemma zero_bytes_i2v it:
  i2v 0 it = zero_bytes (ly_size it).
Proof.
  rewrite /zero_bytes /i2v /val_of_Z /=.
  rewrite bool_decide_decide; destruct decide as [Hd|Hd]; last first.
  { exfalso. eapply Hd, zero_in_int_type. }
  simpl. rewrite /ly_size /it_layout.
  generalize (bytes_per_int it) as n.
  induction n as [|n IH]; simpl; first done.
  f_equal; eauto. f_equal. eapply byte_eq; simpl.
  by rewrite Zmod_0_l.
Qed.


Lemma zero_bytes_length n: length (zero_bytes n) = n.
Proof. rewrite /zero_bytes. rewrite repeat_length //. Qed.

Lemma zero_bytes_sum_list xs:
  zero_bytes (sum_list xs) = concat (zero_bytes <$> xs).
Proof.
  rewrite /zero_bytes; induction xs as [|x xr IH]; simpl; auto.
  rewrite repeat_app IH //.
Qed.

Lemma val_to_loc_null_bytes:
  val_to_loc NULL_bytes = Some NULL_loc.
Proof.
  rewrite /val_to_loc /NULL_bytes /ptr_size /=.
  rewrite bool_decide_decide; destruct decide; naive_solver.
Qed.


Definition eval_bin_op_arithII op it n1 n2 :=
  match op with
  | AddOp => Some (n1 + n2)
  | SubOp => Some (n1 - n2)
  | MulOp => Some (n1 * n2)
  (* we need to take `quot` and `rem` here for the correct rounding
  behavior, i.e. rounding towards 0 (instead of `div` and `mod`,
  which round towards floor)*)
  | DivOp => if bool_decide (n2 ≠ 0) then Some (n1 `quot` n2) else None
  | ModOp => if bool_decide (n2 ≠ 0) then Some (n1 `rem` n2) else None
  | AndOp => Some (Z.land n1 n2)
  | OrOp => Some (Z.lor n1 n2)
  | XorOp => Some (Z.lxor n1 n2)
  (* For shift operators (`ShlOp` and `ShrOp`), behaviors are defined if:
    - lhs is nonnegative, and
    - rhs (also nonnegative) is less than the number of bits in lhs.
    See: https://en.cppreference.com/w/c/language/operator_arithmetic, "Shift operators". *)
  | ShlOp => if bool_decide (0 ≤ n1 ∧ 0 ≤ n2 < bits_per_int it) then Some (n1 ≪ n2) else None
  (* NOTE: when lhs is negative, Coq's `≫` is not semantically equivalent to C's `>>`.
    Counterexample: Coq `-1000 ≫ 10 = 0`; C `-1000 >> 10 == -1`.
    This is because `≫` is implemented by `Z.div`. *)
  | ShrOp => if bool_decide (0 ≤ n1 ∧ 0 ≤ n2 < bits_per_int it) then Some (n1 ≫ n2) else None
  | _ => None
  end.

Lemma eval_bin_op_arithII_iff op n n1 n2 it σ v:
  n1 ∈ it →
  n2 ∈ it →
  (if it_signed it then n ∈ it else True) →
  eval_bin_op_arithII op it n1 n2 = Some n →
  eval_bin_op op (IntOp it) (IntOp it) σ (i2v n1 it) (i2v n2 it) v ↔
  v = i2v (if it_signed it then n else n `mod` int_modulus it) it.
Proof.
  rewrite /eval_bin_op_arithII. intros Hn1 Hn2 Hran Hop. split.
  - inversion 1; subst; simplify_eq.
    { destruct op; simplify_eq. }
    destruct it_signed eqn: Hsign.
    all: do 3 (i2v_simpl); by simplify_eq.
  - intros ->.
    econstructor 7; eauto using i2v_val_to_Z.
    destruct it_signed eqn: Hsign; eauto using i2v_val_to_Z, val_of_Z_i2v, int_modulus_mod_in_range.
Qed.

Lemma eval_bin_op_signed_arithII_iff op n n1 n2 it σ v:
  it_signed it = true →
  n1 ∈ it →
  n2 ∈ it →
  n ∈ it →
  eval_bin_op_arithII op it n1 n2 = Some n →
  eval_bin_op op (IntOp it) (IntOp it) σ (i2v n1 it) (i2v n2 it) v ↔
  v = i2v n it.
Proof.
  intros Hsign Hn1 Hn2 Hn Hop. specialize (eval_bin_op_arithII_iff op n n1 n2 it σ v).
  rewrite Hsign. intros Heq; eapply Heq; eauto.
Qed.

Lemma eval_bin_op_unsigned_arithII_iff op n n1 n2 it σ v:
  it_signed it = false →
  n1 ∈ it →
  n2 ∈ it →
  eval_bin_op_arithII op it n1 n2 = Some n →
  eval_bin_op op (IntOp it) (IntOp it) σ (i2v n1 it) (i2v n2 it) v ↔
  v = i2v (n `mod` int_modulus it) it.
Proof.
  intros Hsign Hn1 Hn2 Hop. specialize (eval_bin_op_arithII_iff op n n1 n2 it σ v).
  rewrite Hsign. intros Heq; eapply Heq; eauto.
Qed.


Definition eval_bin_op_relII op n1 n2 :=
  match op with
  | EqOp rit => Some (bool_decide (n1 = n2), rit)
  | NeOp rit => Some (bool_decide (n1 ≠ n2), rit)
  | LtOp rit => Some (bool_decide (n1 < n2), rit)
  | GtOp rit => Some (bool_decide (n1 > n2), rit)
  | LeOp rit => Some (bool_decide (n1 <= n2), rit)
  | GeOp rit => Some (bool_decide (n1 >= n2), rit)
  | _ => None
  end.


Lemma eval_bin_op_relII_iff op n m b rit it σ v:
  n ∈ it →
  m ∈ it →
  eval_bin_op_relII op n m = Some (b, rit) →
  eval_bin_op op (IntOp it) (IntOp it) σ (i2v n it) (i2v m it) v ↔
  v = (b2v b rit).
Proof.
  rewrite /eval_bin_op_relII. intros Hn1 Hn2 Hop. split.
  - inversion 1; subst; simplify_eq; last first.
    { destruct op; simplify_eq. }
    repeat i2v_simpl. by simplify_eq.
  - intros ->.
    econstructor 6; eauto using i2v_val_to_Z.
    eapply val_of_Z_bool.
Qed.


(* We improve the automation for tedious side goals *)
Global Hint Immediate val_to_of_loc mem_cast_id_loc : quiver_logic.
Global Hint Resolve mem_cast_id_int val_to_Z_i2v i2v_val_to_Z : quiver_logic.
Global Hint Resolve bool_to_Z_elem_of_int_type : quiver_logic.
Global Hint Extern 4  => done : quiver_logic.


(* pointer comparison *)
Lemma eval_bin_op_ptr_cmp l1 l2 op h v b it:
  match op with | EqOp it' | NeOp it' => it' = it | _ =>  False end →
  heap_loc_eq l1 l2 h.(st_heap) = Some b →
  eval_bin_op op PtrOp PtrOp h l1 l2 v
   ↔ val_of_Z (bool_to_Z (if op is EqOp _ then b else negb b)) it None = Some v.
Proof.
  move => ??. split.
  - inversion 1; rewrite ->?val_to_of_loc in *; simplify_eq/= => //; destruct op => //; simplify_eq; done.
  - move => ?. apply: CmpOpPP; rewrite ?val_to_of_loc //. destruct op => //; simplify_eq; done.
Qed.

Lemma eval_bin_op_val_to_loc op v1 l1 v2 l2 σ v3:
  val_to_loc v1 = Some l1 →
  val_to_loc v2 = Some l2 →
  match op with EqOp _ => True | NeOp _ => True | _ => False end →
  eval_bin_op op PtrOp PtrOp σ v1 v2 v3 ↔ eval_bin_op op PtrOp PtrOp σ l1 l2 v3.
Proof.
  destruct op; simpl; intros Hv1 Hv2 ?; try naive_solver.
  - split; inversion 1; subst; simplify_eq.
    + econstructor; eauto with quiver_logic.
    + do 2 revert select (val_to_loc _ = Some _).
      rewrite !val_to_of_loc. intros; simplify_eq.
      econstructor; eauto with quiver_logic.
  - split; inversion 1; subst; simplify_eq.
    + econstructor; eauto with quiver_logic.
    + do 2 revert select (val_to_loc _ = Some _).
      rewrite !val_to_of_loc. intros; simplify_eq.
      econstructor; eauto with quiver_logic.
Qed.

(* statement induction *)
Lemma stmt_ind (P: stmt → Prop) :
  (∀ (b : label), P (Goto b)) →
  (∀ (e : expr), P (Return e)) →
  (∀ (ot : op_type) (join : option label) (e : expr) (s1 s2 : stmt), P s1 → P s2 → P (IfS ot join e s1 s2)) →
  (∀ (it : int_type) (e : expr) (m : gmap Z nat) (bs : list stmt) (def : stmt),
    Forall P bs → P def → P (Switch it e m bs def)) →
  (∀ (o : order) (ot : op_type) (e1 e2 : expr) (s : stmt), P s → P (Assign o ot e1 e2 s)) →
  (∀ (ly : layout) (e : expr) (s : stmt), P s → P (Free ly e s)) →
  (∀ (s : stmt), P s → P (SkipS s)) →
  (P StuckS) →
  (∀ (e : expr) (s : stmt), P s → P (ExprS e s)) →
  ∀ (s: stmt), P s.
Proof.
  move => *. generalize dependent P => P. match goal with | s : stmt |- _ => revert s end.
  fix FIX 1. move => [ ^s] => ??? Hswitch ????? *.
  4: { apply Hswitch; [apply Forall_true => ?|]; by apply: FIX. }
  all: auto.
Qed.


(* Statement Substitutions *)
Lemma subst_expr_app L1 L2 e:
  (subst_l (L1 ++ L2) e = subst_l L2 (subst_l L1 e)).
Proof.
  induction L1 as [|[x v] L1 IH] in e |-*; simpl; unfold subst_l; fold subst_l.
  - done.
  - rewrite IH. done.
Qed.

Lemma subst_stmt_app L1 L2 s:
  (subst_stmt (L1 ++ L2) s = subst_stmt L2 (subst_stmt L1 s)).
Proof.
  induction s as [| | | it e m bs s Hall IH| | | | |] using stmt_ind; unfold subst_stmt; fold subst_stmt.
  all: try rewrite !subst_expr_app.
  all: try congruence.
  rewrite IH. f_equal. clear IH. unfold fmap.
  induction Hall; simpl; congruence.
Qed.

Lemma subst_stmt_nil s:
  subst_stmt [] s = s.
Proof.
  induction s as [| | | it e m bs s Hall IH| | | | |] using stmt_ind; unfold subst_stmt; fold subst_stmt.
  all: try unfold subst_l.
  all: try congruence.
  rewrite IH. f_equal. clear IH. unfold fmap.
  induction Hall; simpl; congruence.
Qed.

Lemma subst_cfg_app L1 L2 (Q: gmap string stmt):
  subst_stmt (L1 ++ L2) <$> Q = subst_stmt L2 <$> (subst_stmt L1 <$> Q).
Proof.
  rewrite -map_fmap_compose. eapply map_fmap_ext.
  intros ???. simpl. rewrite subst_stmt_app //.
Qed.

Lemma subst_cfg_nil (cfg: gmap string stmt):
  subst_stmt [] <$> cfg = cfg.
Proof.
  rewrite -{2}(map_fmap_id cfg). eapply map_fmap_ext.
  intros ???. simpl. rewrite subst_stmt_nil //.
Qed.

(* Iris facts *)
Lemma big_sepL2_trans {Σ: gFunctors} {A B C: Type} (L1: list A) (L2: list B) (L3: list C) (F: A → B → iProp Σ) G:
  ([∗ list] a; b ∈ L1; L2, F a b) -∗ ([∗ list] b; c ∈ L2; L3, G b c) -∗ ([∗ list] a; c ∈ L1; L3, ∃ b, F a b ∗ G b c).
Proof.
  iRevert (L2 L3). iInduction L1 as [|a L1] "IH";
  iIntros (L2 L3); destruct L2, L3; simpl; eauto.
  - iIntros "[]".
  - iIntros "[Hab HF] [Hbc HG]". iSplitL "Hab Hbc"; first by iExists _; iFrame.
    iApply ("IH" with "HF HG").
Qed.


Lemma big_sepL_seq {Σ} {X: Type} (L: list X) (Φ: nat → X → iProp Σ):
  ([∗ list] i ↦ A ∈ L, Φ i A) ⊣⊢ ([∗ list] i; A ∈ seq 0 (length L); L, Φ i A).
Proof.
  induction L as [|A L IH] in Φ |-*.
  - done.
  - rewrite /= -seq_shift big_sepL2_fmap_l IH //.
Qed.

Lemma big_sepL_pure_Forall {Σ} {X} (L: list X) (P: X → Prop):
  ([∗ list] l ∈ L, ⌜P l⌝: iProp Σ) ⊣⊢ ⌜Forall P L⌝.
Proof.
induction L; simpl.
  - rewrite Forall_nil //.
  - rewrite Forall_cons IHL. iSplit; iIntros "%"; iPureIntro; done.
Qed.

Lemma big_sepL_equiv {Σ} (L1 L2: list (iProp Σ)):
  L1 ≡ L2 → ([∗] L1) ⊣⊢ ([∗] L2).
Proof.
  induction 1 as [|P Q L1 L2 HPQ HL12 IH]; simpl; first done.
  rewrite HPQ IH //.
Qed.

(* missing in stdpp *)
Lemma list_insert_lookup {X: Type} `{!Inhabited X} (T: list X) (i: nat):
  <[i := (T !!! i)]> T = T.
Proof.
  destruct (decide (i < length T)%nat) as [Hlt|]; last first.
  { rewrite list_insert_ge //. lia. }
  eapply lookup_lt_is_Some_2 in Hlt as [x Hlook].
  rewrite (list_lookup_total_correct _ _ x) //.
  by eapply list_insert_id.
Qed.


(* fact about loc_in_bounds *)
Lemma loc_in_bounds_null_loc `{!refinedcG Σ} l n:
  loc_in_bounds l n ⊢ ⌜l ≠ NULL_loc⌝.
Proof.
  iIntros "Hloc". destruct (decide (l = NULL_loc)) as [Heq|]; last by iPureIntro.
  iDestruct (loc_in_bounds_has_alloc_id with "Hloc") as %Halloc_id.
  exfalso. destruct Halloc_id as [aid Hprov].
  rewrite Heq in Hprov. discriminate.
Qed.

Lemma loc_in_bounds_offset_loc `{!refinedcG Σ} l ly (n m: nat):
  loc_in_bounds l (ly_size ly * (n + m)) ⊣⊢
  loc_in_bounds l (ly_size ly * n) ∗ loc_in_bounds (l offset{ly}ₗ n) (ly_size ly * m).
Proof.
  rewrite Nat.mul_add_distr_l. rewrite -loc_in_bounds_split. f_equiv.
  rewrite /offset_loc.
  by replace ((ly_size ly * n)%nat: Z) with (ly_size ly * n)%Z by lia.
Qed.

Lemma loc_in_bounds_array_offset `{!refinedcG Σ} l i ly (len : nat):
  0 ≤ i ≤ len →
  loc_in_bounds l (ly_size ly * len) ⊢ loc_in_bounds (l offset{ly}ₗ i) 0.
Proof.
  iIntros (Hle) "Hl".
  iApply loc_in_bounds_offset; last by iFrame.
  - done.
  - simpl. assert (0 ≤ ly_size ly * i) as Hle' by lia.
    eapply Zplus_le_compat_l in Hle'.
    by rewrite Z.add_0_r in Hle'.
  - simpl. rewrite Z.add_0_r. eapply Zplus_le_compat_l.
    rewrite Nat2Z.inj_mul. eapply Zmult_le_compat_l; lia.
Qed.


Lemma loc_in_bounds_shift_loc `{!refinedcG Σ} l j (n i: nat):
  0 ≤ j ≤ j + i ≤ n →
  loc_in_bounds l n -∗ loc_in_bounds (l +ₗ j) i.
Proof.
  iIntros (Hleq) "Hb". iApply loc_in_bounds_offset; last iFrame.
  - done.
  - destruct l; simpl. lia.
  - destruct l; simpl. lia.
Qed.

Lemma ly_size_mk_array_layout ly len:
  ly_size (mk_array_layout ly len) = (ly_size ly * len)%nat.
Proof. done. Qed.

Lemma mk_array_layout_single ly:
  mk_array_layout ly 1 = ly.
Proof.
  rewrite /mk_array_layout /ly_mult.
  rewrite Nat.mul_1_r. destruct ly; done.
Qed.

Lemma loc_layout_array l ly len:
  l `has_layout_loc` mk_array_layout ly len ↔
  l `has_layout_loc` ly.
Proof.
  rewrite /has_layout_loc /=.
  rewrite /mk_array_layout /ly_mult. destruct ly; done.
Qed.

Lemma has_layout_loc_True l ly :
  l `has_layout_loc` ly = True.
Proof. by rewrite /has_layout_loc/aligned_to caesium_config.enforce_alignment_value. Qed.

Lemma check_fields_aligned_alt_correct_always sl l:
  check_fields_aligned_alt sl.(sl_members) l.
Proof. eapply check_fields_aligned_alt_correct. by rewrite has_layout_loc_True. Qed.


Section ghost_state_lemmas.
  Context `{!refinedcG Σ}.


Lemma heap_mapsto_layout_iff l ly:
  (∃ v, l ↦ v ∗ ⌜v `has_layout_val` ly⌝) ⊣⊢ l ↦|ly|.
Proof.
  iSplit.
  - iDestruct 1 as (v) "[Hl %]". iExists _. iFrame. rewrite has_layout_loc_True. iSplit; done.
  - iDestruct 1 as (v) "(% & % & Hl)". iExists _. iFrame.
    by iPureIntro.
Qed.


Lemma wp_deref_aligned v Φ vl l ot q E o (mc : bool):
  o = ScOrd ∨ o = Na1Ord →
  val_to_loc vl = Some l →
  v `has_layout_val` ot_layout ot →
  l↦{q}v -∗ ▷ (∀ st, l ↦{q} v -∗ Φ (if mc then mem_cast v ot st else v)) -∗ WP !{ot, o, mc} (Val vl) @ E {{ Φ }}.
Proof.
  intros ???. eapply wp_deref; eauto. rewrite has_layout_loc_True. done.
Qed.

End ghost_state_lemmas.


Definition ly_of_size (n: Z) := (mk_array_layout u8 (Z.to_nat n)).


Lemma ly_size_ly_of_size (n: Z) :
  ly_size (ly_of_size n) = Z.to_nat n.
Proof.
  rewrite /ly_of_size. rewrite ly_size_mk_array_layout /=.
  rewrite Nat.add_0_r //.
Qed.

(* additional notation *)
Definition While (cond body post: label) : stmt :=
  Goto cond.

Global Typeclasses Opaque While.


(* structs shorthands *)
Definition member_locs (l: loc) (sl: struct_layout) : list loc :=
  (λ n, l at{sl}ₗ n) <$> field_names (sl_members sl).

Global Arguments member_locs _ _ /.

Definition member_layouts (sl: struct_layout) : list layout :=
  omap (λ '(o, ly), const ly <$> o) (sl_members sl).

Global Arguments member_layouts _ /.

Definition member_sizes (sl: struct_layout) : list Z :=
      Z.of_nat <$> (ly_size <$> member_layouts sl).

Global Arguments member_sizes _ /.


Lemma member_locs_layouts_length l sl:
  length (member_locs l sl) = length (member_layouts sl).
Proof.
  unfold member_locs, member_layouts.
  induction (sl_members sl) as [| [[n|] ly] Ms IH]; simpl; first done.
  all: rewrite IH //.
Qed.


Lemma member_locs_layouts_zip l sl:
  zip (member_locs l sl) (member_layouts sl) =
  omap (λ '(o, ly), (λ n, (l at{sl}ₗ n, ly)) <$> o) (sl_members sl).
Proof.
  unfold member_locs, member_layouts.
  induction (sl_members sl) as [| [[n|] ly] Ms IH]; simpl; first done.
  - f_equal. done.
  - done.
Qed.

Definition GetMemberVal (v: val) (s: struct_layout) (m : var_name) : val :=
  match list_find (λ x, x.1 = Some m) (sl_members s) with
  | Some (_, (_, ly)) => take (ly_size ly) (drop (default 0%nat (offset_of (sl_members s) m)) v)
  | None => []
  end.

Notation "v 'at{' s '}ᵥ' m" := (GetMemberVal v s m) (at level 10, format "v  'at{' s '}ᵥ'  m") : stdpp_scope.
Global Typeclasses Opaque GetMemberVal.
Arguments GetMemberVal : simpl never.


(* snoc lists *)
Lemma list_inv_snoc {A} (xs : list A) :
  xs = nil ∨ ∃ x xr, xs = xr ++ [x].
Proof.
  induction xs as [|x xr IH]; eauto.
  destruct IH as [->|[y [yr ->]]]; eauto.
  - right. eexists x, nil. eauto.
  - right. eexists y, (x :: yr). eauto.
Qed.

Lemma list_snoc_ind {A} (P : list A → Prop) :
  P [] →
  (∀ x xs, P xs → P (xs ++ [x])) →
  ∀ xs, P xs.
Proof.
  intros Hnil Hsnoc xs. remember (length xs) as k. revert xs Heqk.
  induction k as [|k]; simpl.
  - intros [|]; eauto; simpl. lia.
  - intros xs. destruct (list_inv_snoc xs) as [->|[x [xr Heq]]].
    + simpl. lia.
    + subst xs. rewrite app_length /=. intros Hlen.
      eapply Hsnoc. eapply IHk. lia.
Qed.

Lemma zip_fmap {A B C: Type} (f: A → B) (g: A → C) (l: list A):
  zip (f <$> l) (g <$> l) = (λ x, (f x, g x)) <$> l.
Proof. induction l; simpl; auto. f_equal. done. Qed.



Lemma forall_id_nil: Forall id [] ↔ True.
Proof. done. Qed.

Lemma forall_id_cons φ Γ: Forall id (φ :: Γ) ↔ φ ∧ Forall id Γ.
Proof. rewrite Forall_cons //. Qed.

Lemma forall_id_app Γ1 Γ2: Forall id (Γ1 ++ Γ2) ↔ Forall id Γ1 ∧ Forall id Γ2.
Proof. rewrite Forall_app //. Qed.

Lemma forall_id_singleton φ: Forall id [φ] ↔ φ.
Proof. rewrite forall_id_cons forall_id_nil //. naive_solver. Qed.

Lemma forall_id_equiv (L1 L2: list Prop):
  L1 ≡ L2 → Forall id L1 ↔ Forall id L2.
Proof.
  induction 1 as [|φ ψ L1 L2 Hφψ H12 IH]; simpl; first done.
  rewrite !Forall_cons Hφψ IH //.
Qed.

Section l2p.
  Context {Σ: gFunctors}.

  Definition l2p (Γ: list Prop): iProp Σ := ⌜Forall id Γ⌝%I.

  Lemma l2p_nil : l2p [] ⊣⊢ emp.
  Proof. rewrite /l2p /=. rewrite forall_id_nil //. Qed.

  Lemma l2p_cons φ Γ : l2p (φ :: Γ) ⊣⊢ ⌜φ⌝ ∗ l2p Γ.
  Proof. rewrite /l2p /=. rewrite forall_id_cons //. iSplit; iPureIntro; auto. Qed.

  Lemma l2p_app Γ1 Γ2 : l2p (Γ1 ++ Γ2) ⊣⊢ l2p Γ1 ∗ l2p Γ2.
  Proof.
    rewrite /l2p /=. rewrite forall_id_app.
    iSplit; iPureIntro; auto.
  Qed.

  Lemma l2p_big_sep Γ : l2p Γ ⊣⊢ ([∗ list] φ ∈ Γ, ⌜φ⌝).
  Proof.
    induction Γ as [|φ Γ IH].
    - rewrite l2p_nil big_sepL_nil //.
    - rewrite l2p_cons IH //.
  Qed.

  Lemma l2p_equiv Γ1 Γ2 : Γ1 ≡ Γ2 → l2p Γ1 ⊣⊢ l2p Γ2.
  Proof.
    intros HΓ. rewrite /l2p /=. rewrite forall_id_equiv //.
  Qed.

  Global Instance l2p_proper: Proper ((≡) ==> (≡)) l2p.
  Proof. intros Γ1 Γ2 HΓ. by eapply l2p_equiv. Qed.

End l2p.

(* an existential quantifier over a list of a fixed length *)
Definition existsN {Σ: gFunctors} {X} (n: nat) (P: list X → iProp Σ) : iProp Σ :=
  ∃ xs, ⌜length xs = n⌝ ∗ P xs.


(* ascending insert *)
Fixpoint insert_ascending {X: Type} (L: list (nat * X)) (n: nat) (P: X) : list (nat * X) :=
  match L with
  | [] => [(n, P)]
  | (m, Q) :: Ls =>
    if (n <? m)%nat then (n, P) :: L else (m, Q) :: insert_ascending Ls n P
  end.

Lemma insert_ascending_sep {Σ: gFunctors} n (P: iProp Σ) Qs:
  [∗] (insert_ascending Qs n P).*2 ⊣⊢ P ∗ [∗] (Qs.*2).
Proof.
  induction Qs as [|[m Q] Qs IH]; simpl; auto.
  destruct (n <? m)%nat; simpl; first done.
  rewrite IH. iSplit; iIntros "(? & ? & ?)"; iFrame.
Qed.

(** rep (from refinedc/theories/lithium/base.v)

 The [rep] tactic is an alternative to the [repeat] and [do] tactics
 that supports left-biased depth-first branching with optional
 backtracking on failure. *)
Module Rep.
  Import Ltac2.
  Import Ltac2.Printf.

  (* Exception to signal how many more steps should be backtracked*)
  Ltac2 Type exn ::= [ RepBacktrack (int) ].

  (* calls [tac] [n] times (n = None means infinite) on the first goal
  under focus, stops on failure of [tac] and then backtracks [nback]
  steps. *)
  Ltac2 rec rep (n : int option) (nback : int) (tac : (unit -> unit)) : int :=
    (* if there are no goals left, we are done *)
    match Control.case (fun _ => Control.focus 1 1 (fun _ => ())) with
    | Err _ => 0
    | Val _ =>
      (* check if we should do another repetition *)
      let do_rep := match n with | None => true | Some n => Int.gt n 0 end in
      match do_rep with
      | false => 0
      | true =>
        (* backtracking point *)
        let res := Control.case (fun _ =>
          (* run tac on the first goal *)
          let tac_res := Control.focus 1 1 (fun _ => Control.case tac) in
          match tac_res  with
          | Err _ =>
              (* if tac failed, either start the backtracking or return 0 *)
              match Int.gt nback 0 with
              | true => Control.zero (RepBacktrack nback)
              | false => 0
              end
          | Val _ =>
              (* compute new n and recurse *)
              let new_n :=
                match n with | None => None | Some n => Some (Int.sub n 1) end in
              let n_steps := rep new_n nback tac in
              Int.add n_steps 1
          end) in
        match res with
        | Err e =>
            match e with
            | RepBacktrack n =>
                (* if we catch a RepBacktrack, either rethrow it with
                one less or return 0 *)
                match Int.gt n 0 with
                | true => Control.zero (RepBacktrack (Int.sub n 1))
                | false => 0
                end
            | _ => Control.zero e
            end
        | Val (r, _) => r
        end
      end
    end.

  Ltac2 print_steps (n : int) :=
    printf "Did %i steps." n.

  Ltac2 rec pos_to_ltac2_int (n : constr) : int :=
    lazy_match! n with
    | xH => 1
    | xO ?n => Int.mul (pos_to_ltac2_int n) 2
    | xI ?n => Int.add (Int.mul (pos_to_ltac2_int n) 2) 1
    end.

  Ltac2 rec z_to_ltac2_int (n : constr) : int :=
    lazy_match! n with
    | Z0 => 0
    | Z.pos ?n => pos_to_ltac2_int n
    | Z.neg ?n => Int.neg (pos_to_ltac2_int n)
    end.


  (* Calls tac on a new subgoal of type Z and converts the resulting Z
  to an int. *)
  Ltac2 int_from_z_subgoal (tac : unit -> unit) : int :=
    let x := Control.focus 1 1 (fun _ =>
      let x := open_constr:(_ : Z) in
      match Constr.Unsafe.kind x with
      | Constr.Unsafe.Cast x _ _ =>
          match Constr.Unsafe.kind x with
          | Constr.Unsafe.Evar e _ =>
              Control.new_goal e;
              x
          | _ => Control.throw Assertion_failure
          end
      | _ => Control.throw Assertion_failure
      end) in
    (* new goal has index 2 because it was added after goal number 1 *)
    Control.focus 2 2 (fun _ =>
      tac ();
      (* check that the goal is closed *)
      Control.enter (fun _ => Control.throw Assertion_failure));
    Control.focus 1 1 (fun _ =>
      let x := Std.eval_vm None x in
      z_to_ltac2_int x).

  (* Necessary because Some and None cannot be used in ltac2: quotations. *)
  Ltac2 some (n : int) : int option := Some n.
  Ltac2 none : int option := None.
End Rep.

(** rep repeatedly applies tac to the goal in a depth-first manner. In
particular, if tac generates multiple subgoals, the process continues
with the first subgoal and only looks at the second subgoal if the
first subgoal (and all goals spawed from it) are solved. If [tac]
fails, the complete process stops (unlike [repeat] which continues
with other subgoals).

[rep n tac] iterates this process at most n times.
[rep <- n tac] backtracks n steps on failure. *)
Tactic Notation "rep" tactic3(tac) :=
  let r := ltac2:(tac |-
    Rep.print_steps (Rep.rep Rep.none 0 (fun _ => Ltac1.run tac))) in
  r tac.

(* rep is carefully written such that all goals are passed to Ltac2
and rep can apply tac in a depth-first manner to only the first goal.
In particular, the behavior of [all: rep 10 tac.] is equivalent to
[all: rep 5 tac. all: rep 5 tac.], even if the first call spawns new
subgoals. (See also the tests.) *)
Tactic Notation "rep" int(n) tactic3(tac) :=
  let ntac := do n (refine (1 + _)%Z); refine 0%Z in
  let r := ltac2:(ntac tac |-
    let n := Rep.int_from_z_subgoal (fun _ => Ltac1.run ntac) in
    Rep.print_steps (Rep.rep (Rep.some n) 0 (fun _ => Ltac1.run tac))) in
  r ntac tac.

Tactic Notation "rep" "<-" int(n) tactic3(tac) :=
  let ntac := do n (refine (1 + _)%Z); refine 0%Z in
  let r := ltac2:(ntac tac |-
     let n := Rep.int_from_z_subgoal (fun _ => Ltac1.run ntac) in
     Rep.print_steps (Rep.rep (Rep.none) n (fun _ => Ltac1.run tac))) in
  r ntac tac.
