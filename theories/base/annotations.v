From refinedc Require Import base.
From refinedc.typing Require Export annotations.

Inductive context_hint_annot :=
  | CtxHintAnnot (i : string).

Definition Loop (lb lb_exit: label) (inv: string) :=
  Goto lb.

Global Typeclasses Opaque Loop.

Lemma subst_loop σ lb lb_exit i:
  subst_stmt σ (Loop lb lb_exit i) = Loop lb lb_exit i.
Proof.
  rewrite /Loop /subst_stmt //.
Qed.