From quiver.base Require Export facts dprop.


Section simplification.
  Context {Σ: gFunctors}.


  (* [simplify] can be used to simplify an abducted proposition.
    When we abduct a proposition [P], we will, for example,
    introduce a number of redundant quantifiers. The reason is
    that we do not know a priori for which existential quantifiers
    in the goal we need to introduce one in the precondition [R] and
    for which ones we can pick an instantiation. We delay this choice
    by *always* introducing a quantifier in the precondition.

    Simplify can then be used to simplify the abduction precondition
    (P below) into a simpler precondition (R below).
  *)
  Definition simplify_def (P: iProp Σ) (R: iProp Σ): Prop :=
      R ⊢ P.

  Definition simplify_aux : seal simplify_def.
  Proof. by eexists. Qed.
  Definition simplify := simplify_aux.(unseal).
  Lemma simplify_eq : simplify = simplify_def.
  Proof. rewrite -simplify_aux.(seal_eq) //. Qed.


  Global Instance simplify_ent_proper:
    Proper ((⊢) ==> flip (⊢) ==> impl) simplify.
  Proof.
    intros P P' HP R R' HR.
    rewrite /impl simplify_eq /simplify_def.
    rewrite HP HR //.
  Qed.

  Global Instance simplify_iff_proper:
    Proper ((≡) ==> (≡) ==> iff) simplify.
  Proof.
    intros P P' HP R R' HR.
    rewrite /impl simplify_eq /simplify_def HP HR //.
  Qed.

  (* we prove some generic properties about simplify,
     which can be used in the simplification passes *)

  Lemma simplify_trans P Q R:
    simplify P Q → simplify Q R → simplify P R.
  Proof.
    rewrite simplify_eq /simplify_def. intros H1 H2.
    by rewrite H2 H1.
  Qed.

  Lemma simplify_refl R:
    simplify R R.
  Proof.
    rewrite simplify_eq /simplify_def. done.
  Qed.

  Lemma simplify_ent (P Q: iProp Σ):
    (P ⊢ Q) → simplify Q P.
  Proof. rewrite simplify_eq //. Qed.

  Lemma ent_simplify (P Q: iProp Σ):
    simplify Q P → (P ⊢ Q).
  Proof. rewrite simplify_eq //. Qed.

  Lemma simplify_ent_iff P Q:
    (P ⊢ Q) ↔ simplify Q P.
  Proof. rewrite simplify_eq //. Qed.

  Lemma simplify_impl_pure (φ ψ: Prop):
    (φ → ψ) → simplify ⌜ψ⌝ ⌜φ⌝.
  Proof.
    intros Himpl. eapply simplify_ent. iIntros "%".
    iPureIntro. auto.
  Qed.

  Lemma simplify_sep (P1 P2 R1 R2: iProp Σ):
    simplify P1 R1 →
    simplify P2 R2 →
    simplify (P1 ∗ P2) (R1 ∗ R2).
  Proof.
    rewrite simplify_eq /simplify_def. by intros -> ->.
  Qed.

  Lemma simplify_or (P1 P2 R1 R2: iProp Σ):
    simplify P1 R1 →
    simplify P2 R2 →
    simplify (P1 ∨ P2) (R1 ∨ R2).
  Proof.
    rewrite simplify_eq /simplify_def. by intros -> ->.
  Qed.

  Lemma simplify_and (P1 P2 R1 R2: iProp Σ):
    simplify P1 R1 →
    simplify P2 R2 →
    simplify (P1 ∧ P2) (R1 ∧ R2).
  Proof.
    rewrite simplify_eq /simplify_def. by intros -> ->.
  Qed.

  Lemma simplify_exists {X: Type} (P R: X → iProp Σ):
    (∀ x: X, simplify (P x) (R x)) →
    simplify (∃ x: X, P x) (∃ x: X, R x).
  Proof.
    rewrite simplify_eq /simplify_def. intros Hent.
    iIntros "(%x & HR)". iExists x. by iApply Hent.
  Qed.

  Lemma simplify_all {X: Type} (P R: X → iProp Σ):
    (∀ x: X, simplify (P x) (R x)) →
    simplify (∀ x: X, P x) (∀ x: X, R x).
  Proof.
    rewrite simplify_eq /simplify_def. intros Hent.
    iIntros "HR" (x). by iApply Hent.
  Qed.

  Lemma simplify_exists_pick {X: Type} (x: X) (P: X → iProp Σ) R:
    simplify (P x) R →
    simplify (∃ x: X, P x) R.
  Proof.
    rewrite simplify_eq /simplify_def. intros Hent.
    iIntros "HR". iExists x. by iApply Hent.
  Qed.

  Lemma simplify_exists_anon {X: Type} `{!Inhabited X} (P R: iProp Σ):
    simplify P R →
    simplify (∃ _: X, P) R.
  Proof.
    intros Hx. by eapply (simplify_exists_pick inhabitant).
  Qed.

  Lemma simplify_case (φ: dProp) P1 P2 R1 R2:
    simplify P1 R1 →
    simplify P2 R2 →
    simplify (case φ P1 P2) (case φ R1 R2).
  Proof.
    rewrite /case. destruct decide; done.
  Qed.

  (* typically the left of a wand is an atom,
     so we do not simplify there, which would be contravariant. *)
  Lemma simplify_wand P Q R:
    simplify Q R →
    simplify (P -∗ Q) (P -∗ R).
  Proof.
    rewrite simplify_eq /simplify_def. by intros ->.
  Qed.

  (* we can weaken wands, but rarely use this automatically *)
  Lemma simplify_wand_drop P Q R:
    simplify P R →
    simplify (Q -∗ P) R.
  Proof.
    rewrite simplify_eq /simplify_def. intros ->.
    by iIntros "$ _".
  Qed.

End simplification.


Create HintDb simplify.
Global Hint Resolve simplify_sep simplify_exists simplify_all: simplify.
Global Hint Resolve simplify_or simplify_and simplify_refl simplify_trans simplify_wand simplify_case : simplify.

Ltac simplify := eauto with simplify.





Section simplify_chain.
  Context {Σ: gFunctors}.

  Inductive simplify_chain : list (iProp Σ → iProp Σ → Prop) → iProp Σ → iProp Σ → Prop :=
  | simplify_chain_nil P :
    simplify_chain [] P P
  | simplify_chain_cons P Q R (f: iProp Σ → iProp Σ → Prop) F :
    f P Q → simplify_chain F Q R → simplify_chain (f :: F) P R.

  Lemma simplify_chain_nil_iff (P R : iProp Σ):
    simplify_chain [] P R ↔ P = R.
  Proof.
    split.
    - inversion 1; eauto.
    - intros ->; eauto using simplify_chain_nil.
  Qed.

  Lemma simplify_chain_cons_iff f F (P R : iProp Σ):
    simplify_chain (f :: F) P R ↔ ∃ Q, f P Q ∧ simplify_chain F Q R.
  Proof.
    split.
    - inversion 1; eauto.
    - destruct 1 as [?[??]]; eauto using simplify_chain_cons.
  Qed.


  Class is_simplify (s: iProp Σ → iProp Σ → Prop) : Prop :=
    is_simplify_simpl P Q: s P Q → simplify P Q.

  Lemma is_simplify_elim_simplify_chain P Q ss:
    simplify_chain ss P Q → is_simplify (simplify_chain ss) → simplify P Q.
  Proof.
    intros. eapply is_simplify_simpl; eauto.
  Qed.

  Global Instance is_simplify_simplify:
    is_simplify simplify.
  Proof.
    intros P Q. simplify.
  Qed.

  Global Instance is_simplify_simplify_chain_nil:
    is_simplify (simplify_chain nil).
  Proof.
    intros P R.
    rewrite simplify_chain_nil_iff. intros ->. simplify.
  Qed.

  Global Instance is_simplify_simplify_chain_cons s ss:
    is_simplify s →
    is_simplify (simplify_chain ss) →
    is_simplify (simplify_chain (s :: ss)).
  Proof.
    intros ?? P R. rewrite simplify_chain_cons_iff.
    intros [Q [Hsimpl Hchain]]. simplify.
  Qed.

End simplify_chain.

Existing Class simplify_chain.
Global Hint Mode simplify_chain - ! - - : typeclass_instances.
Global Hint Mode simplify_chain - - + + : typeclass_instances.


Ltac simplify_chain :=
  match goal with
  | |- simplify_chain nil ?P ?Q => unify P Q; apply simplify_chain_nil
  | |- simplify_chain (?s :: ?ss) ?P ?Q =>
    eapply simplify_chain_cons; [once solve[apply _]|simplify_chain]
  end.

Global Hint Extern 1 (simplify_chain _ _ _) => once simplify_chain : typeclass_instances.

Ltac simplify_chain_elim :=
  match goal with
  | [H: simplify_chain ?L ?P ?R |- simplify ?P' ?R'] =>
      unify P P'; unify R R'; apply: is_simplify_elim_simplify_chain
  end.

Global Hint Extern 2 (simplify _ _) => simplify_chain_elim : simplify.
