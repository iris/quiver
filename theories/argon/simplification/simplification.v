From quiver.argon.simplification Require Export simplify normalize cleanup simplification_instances elim_existentials.


Lemma simplification_chain {Σ: gFunctors} (P1 P2 P3 P4: iProp Σ):
  normalize P1 P2 →
  cleanup P2 P3 →
  elim_existentials P3 P4 →
  simplify P1 P4.
Proof.
  intros Hnorm Hclean Hexists.
  eapply simplify_trans; first apply Hnorm.
  eapply simplify_trans; first apply Hclean.
  eapply Hexists.
Qed.


Ltac simplify_done := by apply simplify_refl.

Tactic Notation "simplify_iter" int(n) :=
  normalize;
  do n (prune_exists; cleanup; normalize).
