From quiver.base Require Import facts classes dprop.
From quiver.argon.simplification Require Import simplify normalize.


(* In another simplification pass, we eliminate redundant quantifiers:
    We go through all the existential quantifiers, try to instantiate them
    and if that fails, backtrack and decide too skip the quantifier.
    We go through all the universal quantifiers and remove those that
    quantify over nothing.
*)

Section elim_exists.
  Context {Σ: gFunctors}.
  Implicit Types (P Q R: iProp Σ) (Δ: list (iProp Σ)).

  Definition elim_existentials (P R: iProp Σ) :=
    simplify P R.

  Definition elim_this_existential (P R: iProp Σ) :=
    simplify P R.

  Definition elim_this_universal (P R: iProp Σ) :=
    simplify P R.

  Definition resolve_this_existential {A: Type} (a: A) (P R: iProp Σ) :=
    simplify P R.

  Definition resolve_this_universal {A: Type} (a b: A) (P R: iProp Σ) :=
    simplify P (⌜a = b⌝ -∗ R).

  Definition resolve_this_existential_prop {A: Type} (a: A) (φ: Prop) := φ.

  (* BASIC LEMMAS *)
  Lemma elim_existentials_pass P R:
    elim_existentials P R → simplify P R.
  Proof. done. Qed.

  Global Instance is_simplify_elim_existentials:
    is_simplify elim_existentials.
  Proof. rewrite /elim_existentials. apply _. Qed.



  (* general elim existentials pass *)
  Lemma elim_existentials_wand (P Q R: iProp Σ):
    elim_existentials Q R →
    elim_existentials (P -∗ Q) (P -∗ R).
  Proof. rewrite /elim_existentials. simplify. Qed.

  Lemma elim_existentials_conjunction (P1 P2 Q1 Q2: iProp Σ):
    elim_existentials P1 Q1 →
    elim_existentials P2 Q2 →
    elim_existentials (P1 ∧ P2) (Q1 ∧ Q2).
  Proof. rewrite /elim_existentials. simplify. Qed.

  Lemma elim_existentials_case φ (P1 P2 R1 R2: iProp Σ) :
    elim_existentials P1 R1 →
    elim_existentials P2 R2 →
    elim_existentials (case φ P1 P2) (case φ R1 R2).
  Proof. rewrite /elim_existentials. simplify. Qed.


  Lemma elim_existentials_nfs Γ Δ (P R : iProp Σ):
    elim_existentials P R →
    elim_existentials (nfs Γ Δ P) (nfs Γ Δ R).
  Proof.
    rewrite /elim_existentials. simplify.
  Qed.

  Lemma elim_existentials_nfw Γ Δ (P R : iProp Σ):
    elim_existentials P R →
    elim_existentials (nfw Γ Δ P) (nfw Γ Δ R).
  Proof.
    rewrite /elim_existentials. simplify.
  Qed.

  Lemma elim_existentials_finish P:
    elim_existentials P P.
  Proof. rewrite /elim_existentials. simplify. Qed.


  Lemma elim_existentials_empty_exists {A: Type} `{!Inhabited A} (P R: iProp Σ):
    elim_existentials P R →
    elim_existentials (∃ a: A, P) R.
  Proof. rewrite /elim_existentials. eapply simplify_exists_anon. Qed.


  Lemma elim_existentials_exists_elim X F R R':
    elim_this_existential (∃ x: X, F x) R →
    elim_existentials R R' →
    elim_existentials (∃ x: X, F x) R'.
  Proof.
    rewrite /elim_this_existential /elim_existentials //.
    simplify.
  Qed.

  Lemma elim_existentials_exists_skip X (F G: X → iProp Σ):
    (∀! x, elim_existentials (F x) (G x)) →
    elim_existentials (∃ x: X, F x) (∃ x: X, G x).
  Proof. rewrite /elim_existentials. simplify. Qed.


  Lemma elim_existentials_empty_forall {A: Type} (P R: iProp Σ):
    elim_existentials P R →
    elim_existentials (∀ a: A, P) R.
  Proof.
    rewrite /elim_existentials. eapply simplify_trans.
    eapply simplify_ent. by iIntros "$".
  Qed.

  Lemma elim_existentials_forall_skip X (F G: X → iProp Σ):
    (∀! x, elim_existentials (F x) (G x)) →
    elim_existentials (∀ x: X, F x) (∀ x: X, G x).
  Proof. rewrite /elim_existentials. simplify. Qed.



  (* pass to eliminate this existential, move in, then pick evar *)
  Lemma elim_this_existental_unit (G: unit → iProp Σ):
    elim_this_existential (∃ x: unit, G x) (G tt).
  Proof.
    rewrite /elim_this_existential.
    eapply simplify_ent. iIntros "G". by iExists tt.
  Qed.

  Lemma elim_this_existental_commute {X Y: Type} (G: X → Y → iProp Σ) F:
    (∀! y, elim_this_existential (∃ x: X, G x y) (F y)) →
    elim_this_existential (∃ x: X, ∃ y: Y, G x y) (∃ y: Y, F y).
  Proof.
    rewrite /elim_this_existential simplify_eq /simplify_def.
    iIntros (Hent) "[%y HF]". iDestruct (Hent with "HF") as "[%x G]".
    iExists _, _. iFrame.
  Qed.

  Lemma elim_this_existental_inst {X: Type} (x: X) G R:
    resolve_this_existential x (G x) R →
    elim_this_existential (∃ x: X, G x) R.
  Proof.
    rewrite /resolve_this_existential /elim_this_existential.
    rewrite simplify_eq /simplify_def. iIntros (Hent) "R".
    iExists _. by iApply Hent.
  Qed.

  (* resolve this existential (and remove the proposition from Γ) *)
  Lemma resolve_this_existential_prove_pure {A: Type} (a: A) P φ Γ Δ:
    resolve_this_existential_prop a φ →
    resolve_this_existential a (nfs (φ :: Γ) Δ P) (nfs Γ Δ P).
  Proof.
    rewrite /resolve_this_existential /resolve_this_existential_prop simplify_eq /simplify_def.
    rewrite /nfs l2p_cons. iIntros (HΦ) "($ & $ & $)". by iPureIntro.
  Qed.

  Lemma resolve_this_existential_skip_pure {A: Type} (a: A) P P' φ Γ Γ' Δ Δ':
    resolve_this_existential a (nfs Γ Δ P) (nfs Γ' Δ' P') →
    resolve_this_existential a (nfs (φ :: Γ) Δ P) (nfs (φ :: Γ') Δ' P').
  Proof.
    rewrite /resolve_this_existential /resolve_this_existential_prop simplify_eq /simplify_def.
    rewrite /nfs !l2p_cons. iIntros (Hent) "([$ HΓ] & HΔ & HP)". iApply Hent. iFrame.
  Qed.

  Lemma resolve_this_existential_skip_wand {A: Type} (a: A) (P Q R R': iProp Σ):
    resolve_this_existential a Q R →
    unify (P -∗ R)%I R' →
    resolve_this_existential a (P -∗ Q) R'.
  Proof.
    rewrite /resolve_this_existential /resolve_this_existential_prop.
    intros ? <-. simplify.
  Qed.

  (* rules to resolve the existential *)
  (* We must be very careful to not unify x accidentally so we compare x with a using is_syn_eq *)
  Lemma resolve_this_existential_prop_eq_l {A: Type} (x a b: A):
    is_syn_eq x a →
    unify x b →
    resolve_this_existential_prop x (a = b).
  Proof. by rewrite /is_syn_eq /unify /resolve_this_existential_prop => -> ->. Qed.

  Lemma resolve_this_existential_prop_eq_r {A: Type} (x a b: A):
    is_syn_eq x a →
    unify x b →
    resolve_this_existential_prop x (b = a).
  Proof. by rewrite /is_syn_eq /unify /resolve_this_existential_prop // => -> ->. Qed.

  Lemma resolve_this_existential_prop_dprop_r (x a b: dProp):
    is_syn_eq x a →
    unify x b →
    resolve_this_existential_prop x (dprop_iff b a).
  Proof. rewrite /is_syn_eq /unify /resolve_this_existential_prop //. intros ??. by subst. Qed.

  Lemma resolve_this_existential_prop_dprop_l (x a b: dProp):
    is_syn_eq x a →
    unify x b →
    resolve_this_existential_prop x (dprop_iff a b).
  Proof. rewrite /is_syn_eq /unify /resolve_this_existential_prop //. intros ??. by subst. Qed.

  Lemma resolve_this_existential_prop_equiv_r {A: Type} `{!Equiv A} `{Equivalence A (≡)} (x a b: A):
    is_syn_eq x a →
    unify x b →
    resolve_this_existential_prop x (b ≡ a).
  Proof. rewrite /is_syn_eq /unify /resolve_this_existential_prop. by intros -> ->. Qed.

  Lemma resolve_this_existential_prop_equiv_l {A: Type} `{!Equiv A} `{Equivalence A (≡)} (x a b: A):
    is_syn_eq x a →
    unify x b →
    resolve_this_existential_prop x (a ≡ b).
  Proof. rewrite /is_syn_eq /unify /resolve_this_existential_prop. by intros -> ->. Qed.

  Lemma resolve_this_existential_prop_double_negation {A: Type} (a: A) φ:
    resolve_this_existential_prop a φ →
    resolve_this_existential_prop a (¬ ¬ φ).
  Proof. rewrite /resolve_this_existential_prop. naive_solver. Qed.



  (* universals *)
  Lemma elim_existentials_forall_elim X F R R':
    elim_this_universal (∀ x: X, F x) R →
    elim_existentials R R' →
    elim_existentials (∀ x: X, F x) R'.
  Proof.
    rewrite /elim_this_existential /elim_existentials //.
    simplify.
  Qed.

  Lemma elim_this_universal_unit (G: unit → iProp Σ):
    elim_this_universal (∀ x: unit, G x) (G tt).
  Proof.
    rewrite /elim_this_existential.
    eapply simplify_ent. iIntros "G". iIntros (x).
    destruct x; done.
  Qed.

  Lemma elim_this_universal_commute {X Y: Type} (G: X → Y → iProp Σ) F:
    (∀! y, elim_this_universal (∀ x: X, G x y) (F y)) →
    elim_this_universal (∀ x: X, ∀ y: Y, G x y) (∀ y: Y, F y).
  Proof.
    rewrite /elim_this_universal simplify_eq /simplify_def.
    iIntros (Hent) "HF". iIntros (x y). iApply (Hent y with "[HF]").
    by iApply "HF".
  Qed.

  Lemma elim_this_universal_inst {X: Type} y (G H: X → iProp Σ) :
    (∀! x, resolve_this_universal x y (G x) (H x)) →
    elim_this_universal (∀ x: X, G x) (H y).
  Proof.
    rewrite /resolve_this_universal /elim_this_universal.
    rewrite simplify_eq /simplify_def. iIntros (Hent) "H".
    iIntros (x). iApply Hent. iIntros "->". done.
  Qed.


  Lemma resolve_this_universal_nfw_eq_l {A: Type} (a b: A) Γ Δ P:
    resolve_this_universal a b (nfw ((a = b) :: Γ) Δ P) (nfw Γ Δ P).
  Proof.
    rewrite /resolve_this_universal nfw_cons_pure.
    eapply simplify_refl.
  Qed.

  Lemma resolve_this_universal_nfw_eq_r {A: Type} (a b: A) Γ Δ P:
    resolve_this_universal a b (nfw ((b = a) :: Γ) Δ P) (nfw Γ Δ P).
  Proof.
    rewrite /resolve_this_universal nfw_cons_pure.
    eapply simplify_ent. iIntros "Hw ->". by iApply "Hw".
  Qed.


  Lemma resolve_this_universal_nfw_eq_l_invertible  {A B: Type} (a: A) (b: B) (f: A → B) (g: B → A) Γ Δ P:
    Invertible f g →
    resolve_this_universal a (g b) (nfw ((f a = b) :: Γ) Δ P) (nfw Γ Δ P).
  Proof.
    rewrite /resolve_this_universal nfw_cons_pure.
    intros Hinvertible. eapply simplify_ent.
    iIntros "Hw %Hf". iApply "Hw". iPureIntro. by eapply invertible_eq.
  Qed.

  Lemma resolve_this_universal_nfw_eq_r_invertible {A B: Type} (a: A) (b: B) (f: A → B) (g: B → A) Γ Δ P:
    Invertible f g →
    resolve_this_universal a (g b) (nfw ((b = f a) :: Γ) Δ P) (nfw Γ Δ P).
  Proof.
    rewrite /resolve_this_universal nfw_cons_pure.
    intros Hinvertible. eapply simplify_ent.
    iIntros "Hw %Hf". iApply "Hw". iPureIntro. by eapply invertible_eq.
  Qed.

  Lemma resolve_this_universal_nfw_skip {A: Type} (a b: A) φ Γ Γ' Δ Δ' P P':
    resolve_this_universal a b (nfw Γ Δ P) (nfw Γ' Δ' P') →
    resolve_this_universal a b (nfw (φ :: Γ) Δ P) (nfw (φ :: Γ') Δ' P').
  Proof.
    rewrite /resolve_this_universal.
    rewrite !nfw_cons_pure -!simplify_ent_iff. intros <-.
    iIntros "Hw %Hx %Hy". iApply ("Hw" with "[//] [//]").
  Qed.


End elim_exists.


Existing Class elim_existentials.
Global Hint Mode elim_existentials - + - : typeclass_instances.

Global Existing Instance elim_existentials_finish | 100000.
Global Existing Instance elim_existentials_wand.
Global Existing Instance elim_existentials_conjunction.
Global Existing Instance elim_existentials_case.
Global Existing Instance elim_existentials_nfs.
Global Existing Instance elim_existentials_nfw.
Global Existing Instance elim_existentials_empty_exists | 1.
Global Existing Instance elim_existentials_empty_forall | 1.

Global Existing Instance elim_existentials_exists_elim  | 2.
Global Existing Instance elim_existentials_forall_elim  | 2.
Global Existing Instance elim_existentials_exists_skip  |  1000.
Global Existing Instance elim_existentials_forall_skip | 1000.

Existing Class elim_this_existential.
Global Hint Mode elim_this_existential - + - : typeclass_instances.

Global Existing Instance elim_this_existental_unit | 1.
Global Existing Instance elim_this_existental_commute | 2.
Global Existing Instance elim_this_existental_inst | 100.

Existing Class elim_this_universal.
Global Hint Mode elim_this_universal - + - : typeclass_instances.

Global Existing Instance elim_this_universal_unit | 1.
Global Existing Instance elim_this_universal_commute | 2.
Global Existing Instance elim_this_universal_inst | 100.



Existing Class resolve_this_existential.
Global Hint Mode resolve_this_existential - - - ! - : typeclass_instances.

Global Existing Instance resolve_this_existential_prove_pure | 1.
Global Existing Instance resolve_this_existential_skip_pure  | 20.
Global Existing Instance resolve_this_existential_skip_wand  | 1.

Existing Class resolve_this_universal.
Global Hint Mode resolve_this_universal - - ! - ! - : typeclass_instances.

Global Existing Instance resolve_this_universal_nfw_eq_l | 1.
Global Existing Instance resolve_this_universal_nfw_eq_r | 1.
Global Existing Instance resolve_this_universal_nfw_eq_l_invertible | 2.
Global Existing Instance resolve_this_universal_nfw_eq_r_invertible | 2.
Global Existing Instance resolve_this_universal_nfw_skip | 100.



Existing Class resolve_this_existential_prop.
Global Hint Mode resolve_this_existential_prop - - ! : typeclass_instances.

Global Existing Instance resolve_this_existential_prop_eq_l.
Global Existing Instance resolve_this_existential_prop_eq_r.
Global Existing Instance resolve_this_existential_prop_dprop_r.
Global Existing Instance resolve_this_existential_prop_dprop_l.
Global Existing Instance resolve_this_existential_prop_equiv_r.
Global Existing Instance resolve_this_existential_prop_equiv_l.
Global Existing Instance resolve_this_existential_prop_double_negation.



Ltac prune_exists :=
  eapply simplify_trans; [apply: elim_existentials_pass|simpl].
