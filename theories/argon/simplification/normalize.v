
From quiver.base Require Import facts classes dprop.
From quiver.argon.base Require Import classes.
From quiver.argon.base Require Export normalforms.
From quiver.argon.simplification Require Import simplify.


(* the normal form of our preconditions *)
(* one simplification step is transforming an almost left goal into it's normal form:
    ∃ a_1 ... a_n, ([∗ list] φ ∈ Γ; ⌜φ⌝) ∗ [∗] Δ ∗ (∀ b_1, ..., b_m. ([∗ list] φ ∈ Γ'; ⌜φ⌝) ∗ [∗] Δ' -∗ Φ v A
where Φ is the postcondition (or we recursively descend into Φ). *)

Section normalize.
  Context {Σ: gFunctors}.


  Lemma simplify_nfs Γ Δ (P R: iProp Σ):
    simplify P R →
    simplify (nfs Γ Δ P) (nfs Γ Δ R).
  Proof.
    rewrite simplify_eq /simplify_def /nfs.
    by intros ->.
  Qed.

  Lemma simplify_nfw Γ Δ (P R: iProp Σ):
    simplify P R →
    simplify (nfw Γ Δ P) (nfw Γ Δ R).
  Proof.
    rewrite simplify_eq /simplify_def /nfw.
    by intros ->.
  Qed.


  (* A judgment for bringing a proposition P into its normal form. *)
  Definition normalize (P R: iProp Σ) :=
    simplify P R.

  (* We collect existential quantifiers directly in R while
    atoms and pure propositions go into Γ and Δ first
    before they are added into R at the end. *)
  Definition normalize_sep (Γ: list Prop) (Δ: list (iProp Σ)) (P R: iProp Σ) :=
    simplify (nfs Γ Δ P) R.

  (* We collect universal quantifiers directly in R while
    atoms and pure propositions go into Γ and Δ first
    before they are added into R at the end. *)
  Definition normalize_wand (Γ : list Prop) (Δ: list (iProp Σ)) (P R: iProp Σ) :=
    simplify (nfw Γ Δ P) R.


  Lemma normalize_simplify P Q:
    simplify P Q ↔ normalize P Q.
  Proof. done. Qed.

  (* the normalization pass *)
  Lemma normalize_pass P Q:
    normalize P Q → simplify P Q.
  Proof. done. Qed.

  Global Instance is_simplify_normalize:
    is_simplify normalize.
  Proof. rewrite /normalize. apply _. Qed.


  (* NORMALIZE *)
  Lemma normalize_refl P:
    normalize P P.
  Proof. rewrite /normalize. simplify. Qed.

  Lemma normalize_case (φ: dProp) P1 P2 R1 R2 :
    normalize P1 R1 →
    normalize P2 R2 →
    normalize (case φ P1 P2) (case φ R1 R2).
  Proof.
    rewrite /normalize. simplify.
  Qed.

  Lemma normalize_conj (P1 P2 R1 R2 : iProp Σ):
    normalize P1 R1 →
    normalize P2 R2 →
    normalize (P1 ∧ P2) (R1 ∧ R2).
  Proof.
    rewrite /normalize. simplify.
  Qed.

  Lemma normalize_exists_sep {A} (P: A → iProp Σ) R:
    normalize_sep nil nil (∃ x, P x) R →
    normalize (∃ x, P x) R.
  Proof.
    rewrite /normalize_sep /normalize.
    rewrite nfs_empty //.
  Qed.

  Lemma normalize_sep_sep P Q R:
    normalize_sep nil nil (P ∗ Q) R →
    normalize (P ∗ Q) R.
  Proof.
    rewrite /normalize_sep /normalize.
    rewrite nfs_empty //.
  Qed.

  Lemma normalize_normalize_sep_nfs Γ Δ P R :
    normalize_sep Γ Δ P R →
    normalize (nfs Γ Δ P) R.
  Proof. done. Qed.

  Lemma normalize_forall {A} (P: A → iProp Σ) R:
    (normalize_wand nil nil (∀ x, P x) R) →
    normalize (∀ x, P x) R.
  Proof.
    rewrite /normalize_wand /normalize.
    rewrite nfw_empty //.
  Qed.

  Lemma normalize_wand_wand P Q R:
    normalize_wand nil nil (P -∗ Q) R →
    normalize (P -∗ Q) R.
  Proof.
    rewrite /normalize_wand /normalize.
    rewrite nfw_empty //.
  Qed.

  Lemma normalize_normalize_wand_nfw Γ Δ P R :
    normalize_wand Γ Δ P R →
    normalize (nfw Γ Δ P) R.
  Proof. done. Qed.

  (* NORMALIZE SEPARATING CONJUNCTION  *)
  Lemma normalize_sep_normalize_empty P R:
    normalize P R →
    normalize_sep nil nil P R.
  Proof. rewrite /normalize_sep. rewrite nfs_empty //. Qed.

  Lemma normalize_sep_normalize_non_empty Γ Δ P R:
    normalize P R →
    normalize_sep Γ Δ P (nfs Γ Δ R).
  Proof.
    rewrite /normalize_sep /normalize. by eapply simplify_nfs.
  Qed.

  Lemma normalize_sep_exists Γ Δ A P R:
    (∀! x: A, normalize_sep Γ Δ (P x) (R x)) →
    normalize_sep Γ Δ (∃ x: A, P x) (∃ x: A, R x).
  Proof.
    rewrite /normalize_sep. intros Hnfs.
    rewrite nfs_exists. simplify.
  Qed.

  Lemma normalize_sep_sep_atom Γ Δ A `{!Atom A} P R:
    normalize_sep Γ (A :: Δ) P R →
    normalize_sep Γ Δ (A ∗ P) R.
  Proof.
    rewrite /normalize_sep. intros Hnfs.
    rewrite nfs_borrow -bi.sep_assoc -nfs_borrow.
    rewrite -nfs_cons_atom //.
  Qed.

  Lemma normalize_sep_sep_pure Γ Δ φ P R:
    normalize_sep (φ :: Γ) Δ P R →
    normalize_sep Γ Δ (⌜φ⌝ ∗ P) R.
  Proof.
    rewrite /normalize_sep. intros Hnfs.
    rewrite nfs_borrow -bi.sep_assoc -nfs_borrow.
    rewrite -nfs_cons_pure //.
  Qed.

  Lemma normalize_sep_conj_sep Γ Δ (P1 P2 Q R: iProp Σ):
    normalize_sep Γ Δ (Q ∗ (P1 ∧ P2)) R →
    normalize_sep Γ Δ ((P1 ∧ P2) ∗ Q) R.
  Proof.
    rewrite /normalize_sep. rewrite -!simplify_ent_iff.
    rewrite bi.sep_comm //.
  Qed.

  Lemma normalize_sep_case_sep φ Γ Δ (P1 P2 Q R: iProp Σ):
    normalize_sep Γ Δ (Q ∗ (case φ P1 P2)) R →
    normalize_sep Γ Δ ((case φ P1 P2) ∗ Q) R.
  Proof.
    rewrite /normalize_sep. rewrite -!simplify_ent_iff.
    rewrite bi.sep_comm //.
  Qed.

  Lemma normalize_sep_sep_emp_l Γ Δ P R:
    normalize_sep Γ Δ P R →
    normalize_sep Γ Δ (emp ∗ P) R.
  Proof.
    rewrite /normalize_sep. eapply simplify_trans.
    eapply simplify_nfs. rewrite -bi.emp_sep_1 //.
    simplify.
  Qed.

  Lemma normalize_sep_sep_emp_r Γ Δ P R:
    normalize_sep Γ Δ P R →
    normalize_sep Γ Δ (P ∗ emp) R.
  Proof.
    rewrite /normalize_sep. eapply simplify_trans.
    eapply simplify_nfs.
    rewrite bi.sep_comm -bi.emp_sep_1 //.
    simplify.
  Qed.

  Lemma normalize_sep_sep_exists A Γ Δ P P' R:
    (normalize_sep Γ Δ (∃ a: A, P a ∗ P') R) →
    normalize_sep Γ Δ ((∃ a: A, P a) ∗ P') R.
  Proof.
    rewrite /normalize_sep. eapply simplify_trans.
    eapply simplify_nfs. rewrite bi.sep_exist_r.
    simplify.
  Qed.

  Lemma normalize_sep_sep_sep Γ Δ P1 P2 P3 R:
    normalize_sep Γ Δ (P1 ∗ (P2 ∗ P3)) R →
    normalize_sep Γ Δ ((P1 ∗ P2) ∗ P3) R.
  Proof.
    rewrite /normalize_sep. eapply simplify_trans.
    eapply simplify_nfs. rewrite bi.sep_assoc //.
    simplify.
  Qed.

  Lemma normalize_sep_sep_pure_conj Γ Δ φ ψ P R:
    normalize_sep Γ Δ (⌜φ⌝ ∗ (⌜ψ⌝ ∗ P)) R →
    normalize_sep Γ Δ (⌜φ ∧ ψ⌝ ∗ P) R.
  Proof.
    rewrite /normalize_sep. eapply simplify_trans.
    eapply simplify_nfs. eapply simplify_ent.
    iIntros "[%Hφ [%Hψ $]]". by iPureIntro.
  Qed.

  Lemma normalize_sep_nfs Γ1 Γ2 Γ Δ1 Δ2 Δ P R:
    Simpl (Γ1 ++ Γ2) Γ →
    Simpl (Δ1 ++ Δ2) Δ →
    normalize_sep Γ Δ P R →
    normalize_sep Γ1 Δ1 (nfs Γ2 Δ2 P) R.
  Proof.
    intros <- <-. rewrite /normalize_sep. eapply simplify_trans.
    rewrite nfs_app. simplify.
  Qed.

  Lemma normalize_sep_nfs_sep Γ1 Δ1 Γ2 Δ2 P Q R:
    normalize_sep Γ1 Δ1 (nfs Γ2 Δ2 (P ∗ Q)) R →
    normalize_sep Γ1 Δ1 ((nfs Γ2 Δ2 P) ∗ Q) R.
  Proof.
    eapply simplify_trans. rewrite bi.sep_comm -nfs_sep bi.sep_comm //.
    eapply simplify_refl.
  Qed.

  Lemma normalize_sep_exists_pair Γ Δ A B P R:
    normalize_sep Γ Δ (∃ a, ∃ b, P (a, b)) R →
    normalize_sep Γ Δ (∃ a: A * B, P a) R.
  Proof.
    rewrite /normalize_sep. eapply simplify_trans.
    eapply simplify_nfs. eapply simplify_ent.
    iIntros "(%a & %b & P)". by iExists (a, b).
  Qed.

  Lemma normalize_sep_exists_dep_pair Γ (Δ: list (iProp Σ)) (A: Type) (B: A → Type) P R:
    normalize_sep Γ Δ (∃ a, ∃ b: B a, P (existT a b)) R →
    normalize_sep Γ Δ (∃ p: sigT B, P p) R.
  Proof.
    rewrite /normalize_sep. eapply simplify_trans.
    eapply simplify_nfs. eapply simplify_ent.
    iIntros "(%a & %b & P)". by iExists _.
  Qed.

  (* NORMALIZE WANDS *)
  Lemma normalize_wand_normalize_empty P R:
    normalize P R →
    normalize_wand nil nil P R.
  Proof. rewrite /normalize_wand. rewrite nfw_empty //. Qed.

  Lemma normalize_wand_normalize_non_empty Γ Δ P R:
    normalize P R →
    normalize_wand Γ Δ P (nfw Γ Δ R).
  Proof.
    rewrite /normalize_wand /normalize. by eapply simplify_nfw.
  Qed.

  Lemma normalize_wand_forall Γ Δ A P R:
    (∀! x: A, normalize_wand Γ Δ (P x) (R x)) →
    normalize_wand Γ Δ (∀ x: A, P x) (∀ x: A, R x).
  Proof.
    rewrite /normalize_wand. intros Hnfw.
    rewrite nfw_all. simplify.
  Qed.

  Lemma normalize_wand_wand_atom Γ Δ A `{!Atom A} P R:
    normalize_wand Γ (A :: Δ) P R →
    normalize_wand Γ Δ (A -∗ P) R.
  Proof.
    rewrite /normalize_wand. intros Hnfw.
    rewrite nfw_borrow.
    rewrite bi.wand_curry bi.sep_comm -bi.wand_curry.
    rewrite -nfw_borrow. rewrite -nfw_cons_atom //.
  Qed.

  Lemma normalize_wand_wand_pure Γ Δ φ P R:
    normalize_wand (φ :: Γ) Δ P R →
    normalize_wand Γ Δ (⌜φ⌝ -∗ P) R.
  Proof.
    rewrite /normalize_wand. intros Hnfw.
    rewrite nfw_borrow.
    rewrite bi.wand_curry bi.sep_comm -bi.wand_curry.
    rewrite -nfw_borrow. rewrite -nfw_cons_pure //.
  Qed.

  Lemma normalize_wand_wand_emp Γ Δ P R:
    normalize_wand Γ Δ P R →
    normalize_wand Γ Δ (emp -∗ P) R.
  Proof.
    rewrite /normalize_wand. eapply simplify_trans.
    eapply simplify_nfw. rewrite bi.emp_wand //.
    simplify.
  Qed.

  Lemma normalize_wand_wand_sep Γ Δ P1 P2 P3 R:
    normalize_wand Γ Δ (P1 -∗ P2 -∗ P3) R →
    normalize_wand Γ Δ ((P1 ∗ P2) -∗ P3) R.
  Proof.
    rewrite /normalize_wand. eapply simplify_trans.
    eapply simplify_nfw. rewrite bi.wand_curry.
    simplify.
  Qed.

  Lemma normalize_wand_wand_exists A Γ Δ P P' R:
    (normalize_wand Γ Δ (∀ a: A, P a -∗ P') R) →
    normalize_wand Γ Δ ((∃ a: A, P a) -∗ P') R.
  Proof.
    rewrite /normalize_wand. eapply simplify_trans.
    eapply simplify_nfw. rewrite bi.exist_wand_forall.
    simplify.
  Qed.

  Lemma normalize_wand_wand_pure_conj Γ Δ φ ψ P R:
    normalize_wand Γ Δ (⌜φ⌝ -∗ ⌜ψ⌝ -∗ P) R →
    normalize_wand Γ Δ (⌜φ ∧ ψ⌝ -∗ P) R.
  Proof.
    rewrite /normalize_wand. eapply simplify_trans.
    eapply simplify_nfw. eapply simplify_ent.
    iIntros "HP [%Hφ %Hψ]". by iApply "HP".
  Qed.

  Lemma normalize_wand_forall_pair Γ Δ A B (P: A * B → iProp Σ) R:
    normalize_wand Γ Δ (∀ a b, P (a, b)) R →
    normalize_wand Γ Δ (∀ a: A * B, P a) R.
  Proof.
    rewrite /normalize_wand. eapply simplify_trans.
    eapply simplify_nfw. eapply simplify_ent.
    iIntros "HP" ([a b]). by iApply "HP".
  Qed.

  Lemma normalize_wand_forall_dep_pair Γ Δ A (B: A → Type) (P: sigT B → iProp Σ) R:
    normalize_wand Γ Δ (∀ a b, P (existT a b)) R →
    normalize_wand Γ Δ (∀ a: sigT B, P a) R.
  Proof.
    rewrite /normalize_wand. eapply simplify_trans.
    eapply simplify_nfw. eapply simplify_ent.
    iIntros "HP" ([a b]). by iApply "HP".
  Qed.


  Lemma normalize_wand_nfw Γ1 Γ2 Γ Δ1 Δ2 Δ P R:
    Simpl (Γ1 ++ Γ2) Γ →
    Simpl (Δ1 ++ Δ2) Δ →
    normalize_wand Γ Δ P R →
    normalize_wand Γ1 Δ1 (nfw Γ2 Δ2 P) R.
  Proof.
    intros <- <-. rewrite /normalize_wand.
    eapply simplify_trans. rewrite nfw_app.
    simplify.
  Qed.

  Lemma normalize_wand_wand_nfs Γ1 Γ2 Δ1 Δ2 P Q R:
    normalize_wand Γ1 Δ1 (nfw Γ2 Δ2 (P -∗ Q)) R →
    normalize_wand Γ1 Δ1 (nfs Γ2 Δ2 P -∗ Q) R.
  Proof.
    eapply simplify_trans. eapply simplify_ent. rewrite /nfw /nfs.
    iIntros "HQ HΓ1 HΔ1 (HΓ2 & HΔ2 & HP)".
    iApply ("HQ" with "HΓ1 HΔ1 HΓ2 HΔ2 HP").
  Qed.

End normalize.

Global Typeclasses Opaque nfs nfw.
Arguments nfs_lin _ _ _ _ /.
Arguments nfw_lin _ _ _ _ /.

Existing Class normalize.
Global Hint Mode normalize - ! - : typeclass_instances.

Existing Class normalize_sep.
Global Hint Mode normalize_sep - ! ! ! - : typeclass_instances.

Existing Class normalize_wand.
Global Hint Mode normalize_wand - ! ! ! - : typeclass_instances.


Global Existing Instance normalize_refl | 100.
Global Existing Instance normalize_case | 2.
Global Existing Instance normalize_conj | 2.
Global Existing Instance normalize_exists_sep | 2.
Global Existing Instance normalize_sep_sep | 2.
Global Existing Instance normalize_normalize_sep_nfs | 2.
Global Existing Instance normalize_forall | 2.
Global Existing Instance normalize_wand_wand | 2.
Global Existing Instance normalize_normalize_wand_nfw | 2.

Global Existing Instance normalize_sep_normalize_empty | 99.
Global Existing Instance normalize_sep_normalize_non_empty | 100.
Global Existing Instance normalize_sep_exists | 3 .
Global Existing Instance normalize_sep_exists_pair | 2.
Global Existing Instance normalize_sep_exists_dep_pair | 2.
Global Existing Instance normalize_sep_sep_atom | 2.
Global Existing Instance normalize_sep_sep_pure | 3.
Global Existing Instance normalize_sep_conj_sep | 2.
Global Existing Instance normalize_sep_case_sep | 2.
Global Existing Instance normalize_sep_sep_pure_conj | 2.
Global Existing Instance normalize_sep_sep_emp_l | 2.
Global Existing Instance normalize_sep_sep_emp_r | 2.
Global Existing Instance normalize_sep_sep_exists | 2.
Global Existing Instance normalize_sep_sep_sep | 2.
Global Existing Instance normalize_sep_nfs | 2.
Global Existing Instance normalize_sep_nfs_sep | 1.


Global Existing Instance normalize_wand_normalize_empty | 99.
Global Existing Instance normalize_wand_normalize_non_empty | 100.
Global Existing Instance normalize_wand_forall | 3.
Global Existing Instance normalize_wand_forall_pair | 2.
Global Existing Instance normalize_wand_forall_dep_pair | 2.
Global Existing Instance normalize_wand_wand_atom | 3.
Global Existing Instance normalize_wand_wand_pure | 3.
Global Existing Instance normalize_wand_wand_pure_conj | 2.
Global Existing Instance normalize_wand_nfw | 2.
Global Existing Instance normalize_wand_wand_emp | 2.
Global Existing Instance normalize_wand_wand_sep | 2.
Global Existing Instance normalize_wand_wand_exists | 2.
Global Existing Instance normalize_wand_wand_nfs | 2.


Ltac normalize := eapply simplify_trans; [apply: normalize_pass| simpl].



(* unfolding the normal form *)
Section nf_unfold.
  Context {Σ: gFunctors}.

  Definition nf_unfold (P R : iProp Σ) :=
    simplify P R.

  (* the unfolding pass *)
  Lemma nf_unfold_pass P R:
    nf_unfold P R → simplify P R.
  Proof. rewrite /nf_unfold //. Qed.

  Global Instance is_simplify_nf_unfold: is_simplify nf_unfold.
  Proof.
    intros P Q. rewrite /nf_unfold. simplify.
  Qed.



  Lemma nf_unfold_nf_cons_pure φ Γ Δ P R:
    nf_unfold (nfs Γ Δ P) R →
    nf_unfold (nfs (φ :: Γ) Δ P) (⌜φ⌝ ∗ R)%I.
  Proof.
    rewrite /nf_unfold simplify_eq /simplify_def /nfs //=.
    rewrite l2p_cons.
    iIntros (Hent) "[$ HR]". by iApply Hent.
  Qed.

  Lemma nf_unfold_nf_cons_atom A Δ P R:
    nf_unfold (nfs nil Δ P) R →
    nf_unfold (nfs nil (A :: Δ) P) (A ∗ R)%I.
  Proof.
    rewrite /nf_unfold simplify_eq /simplify_def /nfs //=.
    rewrite l2p_nil.
    iIntros (Hent) "[$ HR]". iDestruct (Hent with "HR") as "(_ & $ & $)".
  Qed.

  Lemma nf_unfold_nf_nil P R:
    nf_unfold P R →
    nf_unfold (nfs nil nil P) R.
  Proof.
    rewrite /nf_unfold. rewrite nfs_empty //.
  Qed.

  Lemma nf_unfold_nfw_cons_pure φ Γ Δ P R:
    nf_unfold (nfw Γ Δ P) R →
    nf_unfold (nfw (φ :: Γ) Δ P) (⌜φ⌝ -∗ R)%I.
  Proof.
    rewrite /nf_unfold simplify_eq /simplify_def /nfw //=.
    rewrite l2p_cons.
    iIntros (Hent) "R [φ Γ] Δ". by iApply (Hent with "(R φ) Γ Δ").
  Qed.

  Lemma nf_unfold_nfw_cons_atom A Δ P R:
    nf_unfold (nfw nil Δ P) R →
    nf_unfold (nfw nil (A :: Δ) P) (A -∗ R)%I.
  Proof.
    rewrite /nf_unfold simplify_eq /simplify_def /nfw //=.
    iIntros (Hent) "R Γ [A Δ]". by iApply (Hent with "(R A) Γ Δ").
  Qed.

  Lemma nf_unfold_nfw_nil P R:
    nf_unfold P R →
    nf_unfold (nfw nil nil P) R.
  Proof.
    rewrite /nf_unfold. rewrite nfw_empty //.
  Qed.

  Lemma nf_unfold_exists A (P R: A → iProp Σ):
    (∀! a: A, nf_unfold (P a) (R a)) →
    nf_unfold (∃ a: A, P a) (∃ a: A, R a).
  Proof. apply simplify_exists. Qed.

  Lemma nf_unfold_all A (P R: A → iProp Σ):
    (∀! a: A, nf_unfold (P a) (R a)) →
    nf_unfold (∀ a: A, P a) (∀ a: A, R a).
  Proof. apply simplify_all. Qed.

  Lemma nf_unfold_case φ (P P' R R': iProp Σ):
    nf_unfold P R →
    nf_unfold P' R' →
    nf_unfold (case φ P P') (case φ R R').
  Proof. eapply simplify_case. Qed.

  Lemma nf_unfold_done P:
    nf_unfold P P.
  Proof. eapply simplify_refl. Qed.

  Lemma nf_unfold_wand P Q R:
    nf_unfold Q R →
    nf_unfold (P -∗ Q) (P -∗ R).
  Proof. eapply simplify_wand. Qed.

End nf_unfold.

Existing Class nf_unfold.
Global Hint Mode nf_unfold - ! - : typeclass_instances.
Global Existing Instance nf_unfold_nf_cons_pure.
Global Existing Instance nf_unfold_nf_cons_atom.
Global Existing Instance nf_unfold_nf_nil.
Global Existing Instance nf_unfold_nfw_cons_pure.
Global Existing Instance nf_unfold_nfw_cons_atom.
Global Existing Instance nf_unfold_nfw_nil.
Global Existing Instance nf_unfold_exists.
Global Existing Instance nf_unfold_all.
Global Existing Instance nf_unfold_case.
Global Existing Instance nf_unfold_wand.
Global Existing Instance nf_unfold_done | 100.


Ltac nf_unfold := eapply simplify_trans; [apply: nf_unfold_pass|simpl].

