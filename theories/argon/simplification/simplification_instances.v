From quiver.base Require Import dprop facts classes.
From quiver.argon.simplification Require Import simplify normalize cleanup.


Section simplify_pure.

  Global Instance simplify_pure_class b (φ: Prop):
    φ → simplify_pure b φ True.
  Proof. rewrite /simplify_pure; destruct b; done. Qed.

  Global Instance simplify_pure_inj {X Y: Type} (R: relation X) (S: relation Y) b (f: Y → X) x y:
    Inj S R f →
    Proper (S ==> R) f →
    simplify_pure b (R (f x) (f y)) (S x y).
  Proof.
    rewrite /Inj /simplify_pure. intros ??.
    destruct b; naive_solver.
  Qed.

  Global Instance simplify_pure_inj2 {X Y Z: Type} (R: relation X) (S: relation Y) (T: relation Z) b (f: Z → Y → X) x1 x2 y1 y2:
    Inj2 T S R f →
    Proper (T ==> S ==> R) f →
    simplify_pure b (R (f x1 x2) (f y1 y2)) (T x1 y1 ∧ S x2 y2).
  Proof.
    rewrite /Inj2 /simplify_pure. intros Hinj Hprop.
    destruct b.
    - intros [? ?]. by eapply Hprop.
    - naive_solver.
  Qed.

  Global Instance simplify_pure_inj_eq {X Y: Type} (R: relation X) `{!Reflexive R} b (f: Y → X) x y:
    Inj eq R f →
    simplify_pure b (R (f x) (f y)) (x = y).
  Proof.
    rewrite /Inj /simplify_pure. intros Hinj.
    destruct b; naive_solver.
  Qed.

  Global Instance simplify_pure_inj2_eq {X Y Z: Type} (R: relation X) `{!Reflexive R} b (f: Z → Y → X) x1 x2 y1 y2:
    Inj2 eq eq R f →
    simplify_pure b (R (f x1 x2) (f y1 y2)) (x1 = y1 ∧ x2 = y2).
  Proof.
    rewrite /Inj /simplify_pure. intros Hinj.
    destruct b.
    - intros [? ?]. subst. reflexivity.
    - intros Hx. eapply Hinj. done.
  Qed.

  Global Instance simplify_pure_refl {X: Type} (R: relation X) `{!Reflexive R} b x:
    simplify_pure b (R x x) True.
  Proof.
    rewrite /simplify_pure. destruct b; naive_solver.
  Qed.

  Global Instance simplify_pure_prod_relation {X Y: Type} (R: relation X) (S: relation Y) b p1 p2:
    simplify_pure b (prod_relation R S p1 p2) (R p1.1 p2.1 ∧ S p1.2 p2.2).
  Proof.
    rewrite /simplify_pure. destruct b; naive_solver.
  Qed.

  Global Instance simplify_pure_double_negation_assert φ:
    simplify_pure true (¬¬φ) φ.
  Proof.
    rewrite /simplify_pure. naive_solver.
  Qed.

  Global Instance simplify_pure_double_negation_assume φ `{!Decision φ}:
    simplify_pure false (¬¬φ) φ.
  Proof.
    rewrite /simplify_pure. destruct (decide (φ)); naive_solver.
  Qed.

  Global Instance simplify_pure_eq_Some A (o : option A) b x x' :
    TCFastDone (o = Some x') →
    simplify_pure b (o = Some x) (x = x').
  Proof. intros ->. rewrite /simplify_pure; destruct b; naive_solver. Qed.

  Global Instance simplify_pure_0_leq_mul_pos_l b n1 n2 :
    Trivial (0 < n1) →
    simplify_pure b (0 ≤ n1 * n2) (0 ≤ n2).
  Proof. rewrite /Trivial /simplify_pure. destruct b; lia. Qed.

  Global Instance simplify_pure_leq_not_lt b n1 n2 :
    Solve (¬ n1 < n2) →
    simplify_pure b (n1 ≤ n2) (n1 = n2) | 2.
  Proof. rewrite /simplify_pure /Solve. destruct b; lia. Qed.

  Global Instance simplify_pure_leq_not_eq b n1 n2 :
    Solve (¬ n1 = n2) →
    simplify_pure b (n1 ≤ n2) (n1 < n2) | 2.
  Proof. rewrite /simplify_pure /Solve. destruct b; lia. Qed.

  Global Instance simplify_pure_not_lt b n1 n2 :
    simplify_pure b (¬ n1 < n2) (n2 ≤ n1) | 1.
  Proof.
    rewrite /simplify_pure /Solve. destruct b; lia.
  Qed.

  Global Instance simplify_pure_not_le b n1 n2 :
    simplify_pure b (¬ n1 ≤ n2) (n2 < n1) | 1.
  Proof.
    rewrite /simplify_pure /Solve. destruct b; lia.
  Qed.


  Global Instance simplify_pure_eq_zero b (x: nat):
    simplify_pure b (Z.of_nat x = 0) (x = 0%nat).
  Proof.
    rewrite /simplify_pure; destruct b; lia.
  Qed.

  Global Instance simplify_prop_eq_plus_one b (x y: nat):
    simplify_pure b (Z.of_nat x = Z.of_nat y + 1) (x = (y + 1)%nat).
  Proof.
    rewrite /simplify_pure; destruct b; lia.
  Qed.


  Global Instance simplify_pure_non_negative b n it :
    sequence_classes
      [Computable (it_signed it) false;
       Solve (n ≤ max_int it)] →
    simplify_pure b (n ∈ it) (0 ≤ n) | 1.
  Proof.
    rewrite /simplify_pure /sequence_classes /Computable /Solve !Forall_cons /id.
    intros (Heq & ? & _).
    rewrite /elem_of /int_elem_of_it.
    rewrite /min_int Heq.
    destruct b; naive_solver.
  Qed.

  Global Instance simplify_pure_proper b :
    Proper ((≡) ==> (≡) ==> (≡)) (simplify_pure b).
  Proof.
    intros ?? Ha ?? Hb. rewrite /simplify_pure; destruct b; rewrite Ha Hb //.
  Qed.

  Global Instance simplify_pure_bool_decide (P : dProp) p:
    simplify_pure true (bool_decide P = p) (P = deq p true).
  Proof.
    rewrite /simplify_pure. intros ->. destruct p; done.
  Qed.

  Global Instance simplify_pure_bool_decide' (P : dProp) p:
    simplify_pure true (p = bool_decide P) (P = deq p true).
  Proof.
    rewrite /simplify_pure.
    intros ->. destruct p; done.
  Qed.

  (* Note: We don't declare this directly as an instance of simplify_pure, as simplify_pure should make progress. *)
  Lemma simplify_pure_reflexive b φ :
    simplify_pure b φ φ.
  Proof. rewrite /simplify_pure; destruct b; done. Qed.

  Lemma simplify_atom_reflexive {Σ: gFunctors} b (A : iProp Σ) :
    simplify_atom b A A.
  Proof. rewrite /simplify_atom; destruct b; eapply simplify_refl. Qed.

End simplify_pure.




Section simplifiction_lemmas.

  Lemma list_length_0 {A} (x : list A) : length x = 0%nat ≡ (x = []).
  Proof.
    split; last by intros ->. destruct x; done.
  Qed.

  Lemma times_plus_one (n m k: Z) : n * m + n = n * (m + 1).
  Proof. lia. Qed.

  Lemma times_u8_id n: ly_size u8 * n = n.
  Proof. reduce_closed_Z. lia. Qed.

  Lemma times_i8_id n: ly_size i8 * n = n.
  Proof. reduce_closed_Z. lia. Qed.

  Lemma minus_zero_id n: n - 0 = n.
  Proof. lia. Qed.

End simplifiction_lemmas.

Global Hint Extern 10 (simplify_atom ?b _ _) =>
  (progress autorewrite with simplify_pure_rewrite_db; exact (simplify_atom_reflexive b _)) : typeclass_instances.

Global Hint Extern 10 (simplify_pure ?b _ _) =>
  (progress autorewrite with simplify_pure_rewrite_db; exact (simplify_pure_reflexive b _)) : typeclass_instances.

Global Hint Rewrite @app_length @replicate_length @insert_length @app_nil_r : simplify_pure_rewrite_db.
Global Hint Rewrite <- @app_assoc @cons_middle : simplify_pure_rewrite_db.
Global Hint Rewrite @rev_involutive : simplify_pure_rewrite_db.
Global Hint Rewrite @list_length_0 : simplify_pure_rewrite_db.
Global Hint Rewrite @list_insert_lookup : simplify_pure_rewrite_db.
Global Hint Rewrite @times_plus_one @times_u8_id @times_i8_id @minus_zero_id : simplify_pure_rewrite_db.
Global Hint Rewrite @andb_true_r @andb_true_l @orb_true_r @orb_true_l : simplify_pure_rewrite_db.