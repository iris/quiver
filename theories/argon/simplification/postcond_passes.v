From quiver.argon.simplification Require Import simplify normalize.
From quiver.argon.base Require Import judgments.
From quiver.base Require Import classes.

Section mark_post.
  Context {Σ: gFunctors}.

  Definition mark_postconditions (F: iProp Σ → iProp Σ) (P: iProp Σ) (Q: iProp Σ) : Prop :=
    (∀ R, simplify R (F R)) → simplify P Q.

  Lemma mark_postconditions_exists F {X: Type} (P Q : X → iProp Σ) :
    (∀! x, mark_postconditions F (P x) (Q x)) →
    mark_postconditions F (∃ x, P x) (∃ x, Q x).
  Proof. rewrite /mark_postconditions /evar_forall. intros H1 HR. simplify. Qed.

  Lemma mark_postconditions_nfs F Γ Ω P Q :
    mark_postconditions F P Q →
    mark_postconditions F (nfs Γ Ω P) (nfs Γ Ω Q).
  Proof. rewrite /mark_postconditions. intros H1 HR. simplify. Qed.

  Lemma mark_postconditions_conj F P1 P2 Q1 Q2 :
    mark_postconditions F P1 Q1 →
    mark_postconditions F P2 Q2 →
    mark_postconditions F (P1 ∧ P2) (Q1 ∧ Q2).
  Proof. rewrite /mark_postconditions. intros H1 H2 HR. simplify. Qed.

  Lemma mark_postconditions_case (φ: dProp) F P1 P2 Q1 Q2 :
    mark_postconditions F P1 Q1 →
    mark_postconditions F P2 Q2 →
    mark_postconditions F (case φ P1 P2) (case φ Q1 Q2).
  Proof. rewrite /mark_postconditions. intros H1 H2 HR. simplify. Qed.

  (* use this for wand, nfw, forall, and postconditions *)
  Lemma mark_postconditions_mark F P :
    mark_postconditions F P (F P).
  Proof.
    rewrite /mark_postconditions. intros HR. simplify.
  Qed.
End mark_post.

Existing Class mark_postconditions.
Global Hint Mode mark_postconditions - - ! - : typeclass_instances.

Global Existing Instance mark_postconditions_exists | 1.
Global Existing Instance mark_postconditions_nfs | 1.
Global Existing Instance mark_postconditions_conj | 1.
Global Existing Instance mark_postconditions_case | 1.
Global Existing Instance mark_postconditions_mark | 100.
