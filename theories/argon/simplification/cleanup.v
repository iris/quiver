From quiver.base Require Import dprop facts classes.
From quiver.argon.base Require Import classes judgments.
From quiver.argon.simplification Require Import simplify normalize.



Section cleanup_pass.
  Context {Σ: gFunctors}.

  Definition cleanup (P R: iProp Σ) :=
    simplify P R.

  Definition simplify_prop (star : bool) (P R : iProp Σ) : Prop :=
    if star then simplify P R else simplify R P.

  Definition simplify_pure (star: bool) (φ ψ: Prop) : Prop :=
    if star then ψ → φ else φ → ψ.

  Definition simplify_atom (star: bool) (P R: iProp Σ) : Prop :=
    if star then simplify P R else simplify R P.


  Lemma cleanup_pass (P R: iProp Σ) :
    cleanup P R →
    simplify P R.
  Proof. rewrite /cleanup //. Qed.

  Global Instance is_simplify_cleanup: is_simplify cleanup.
  Proof.
    intros P Q. rewrite /cleanup. simplify.
  Qed.

  Lemma cleanup_refl P : cleanup P P.
  Proof.
    rewrite /cleanup. simplify.
  Qed.

  Lemma cleanup_exists {A} (P: A → iProp Σ) R :
    (∀! x, cleanup (P x) (R x)) → cleanup (∃ x, P x) (∃ x, R x).
  Proof. rewrite /cleanup. simplify. Qed.

  Lemma cleanup_forall {A} (P: A → iProp Σ) R :
    (∀! x, cleanup (P x) (R x)) → cleanup (∀ x, P x) (∀ x, R x).
  Proof. rewrite /cleanup. simplify. Qed.

  Lemma cleanup_sep_exists {A} (P: A → iProp Σ) Q R :
    cleanup (∃ x, P x ∗ Q) R →
    cleanup ((∃ x, P x) ∗ Q) R.
  Proof.
    eapply simplify_trans, simplify_ent.
    iIntros "[%x [P $]]". iExists x. iFrame.
  Qed.

  Lemma cleanup_sep_assoc P1 P2 P3 R :
    cleanup (P1 ∗ (P2 ∗ P3)) R →
    cleanup ((P1 ∗ P2) ∗ P3) R.
  Proof.
    eapply simplify_trans, simplify_ent. by iIntros "($ & $ & $)".
  Qed.

  Lemma cleanup_sep_emp P R:
    cleanup P R →
    cleanup (emp ∗ P) R.
  Proof.
    rewrite /cleanup. rewrite -!simplify_ent_iff.
    intros ->. by iIntros "$".
  Qed.

  Lemma cleanup_sep_simplify A P Q R :
    once_tc (simplify_prop true A P) →
    cleanup (P ∗ Q) R →
    cleanup (A ∗ Q) R.
  Proof.
    rewrite /cleanup /simplify_prop /once_tc.
    rewrite -!simplify_ent_iff. by intros ->.
  Qed.

  Lemma cleanup_sep_skip ψ A P R :
    once_tc (extract_from_atom A ψ) →
    (ψ → cleanup P R) →
    cleanup (A ∗ P) (A ∗ R).
  Proof.
    rewrite /cleanup /once_tc /extract_from_atom.
    rewrite -!simplify_ent_iff.
    iIntros (Hψ Hent) "[A R]". iDestruct (Hψ with "A") as "%Hψ'".
    rewrite Hent //. iFrame.
  Qed.

  Lemma cleanup_nfs_prop φ Γ Δ P R:
    cleanup (⌜φ⌝ ∗ nfs Γ Δ P) R →
    cleanup (nfs (φ :: Γ) Δ P) R.
  Proof.
    rewrite /cleanup.
    rewrite nfs_cons_pure //.
  Qed.

  Lemma cleanup_nfs_sep A Δ P R:
    cleanup (A ∗ nfs nil Δ P) R →
    cleanup (nfs nil (A :: Δ) P) R.
  Proof.
    rewrite /cleanup.
    rewrite nfs_cons_atom //.
  Qed.


  Lemma cleanup_nfs_nil_nil P R:
    cleanup P R →
    cleanup (nfs nil nil P) R.
  Proof.
    rewrite /cleanup.
    intros Hent%ent_simplify.
    eapply simplify_ent. rewrite Hent.
    rewrite /nfs /=. by iIntros "$".
  Qed.

  Lemma cleanup_wand_emp P R :
    cleanup P R →
    cleanup (emp -∗ P) R.
  Proof.
    rewrite /once_tc /cleanup -!simplify_ent_iff.
    intros ->. by iIntros "$".
  Qed.

  Lemma cleanup_wand_sep P1 P2 Q R :
    cleanup (P1 -∗ P2 -∗ Q)  R →
    cleanup (P1 ∗ P2 -∗ Q) R.
  Proof.
    rewrite /cleanup -!simplify_ent_iff.
    intros ->. iIntros "Hw [P1 P2]".
    by iApply ("Hw" with "P1 P2").
  Qed.

  Lemma cleanup_wand_exists {X: Type} (P: X → iProp Σ) Q R :
    cleanup (∀ x, P x -∗ Q)  R →
    cleanup ((∃ x, P x) -∗ Q) R.
  Proof.
    rewrite /cleanup -!simplify_ent_iff.
    intros ->. iIntros "Hw (%x & HP)".
    by iApply ("Hw" with "HP").
  Qed.


  Lemma cleanup_wand_skip φ A P R :
    once_tc (extract_from_atom A φ) →
    (φ → cleanup P R) →
    cleanup (A -∗ P) (A -∗ R).
  Proof.
    rewrite /once_tc /extract_from_atom /cleanup -!simplify_ent_iff.
    iIntros (Hφ Hent) "Hw A". iDestruct (Hφ with "A") as "%Hφ'".
    iApply Hent; first done. by iApply "Hw".
  Qed.

  Lemma cleanup_wand_simplify A P Q R :
    once_tc (simplify_prop false A P) →
    cleanup (P -∗ Q) R →
    cleanup (A -∗ Q) R.
  Proof.
    rewrite /simplify_prop /cleanup.
    move => /ent_simplify HA /ent_simplify. rewrite -HA => ?.
    by apply simplify_ent.
  Qed.

  Lemma cleanup_nfw_cons_sep A Δ P R :
    cleanup (A -∗ nfw nil Δ P) R →
    cleanup (nfw nil (A :: Δ) P) R.
  Proof.
    rewrite /cleanup -!simplify_ent_iff nfw_cons_atom //.
  Qed.

  Lemma cleanup_nfw_cons_pure φ Γ P Δ R :
    cleanup (⌜φ⌝ -∗ nfw Γ Δ P) R →
    cleanup (nfw (φ :: Γ) Δ P) R.
  Proof.
    rewrite /cleanup -!simplify_ent_iff nfw_cons_pure //.
  Qed.

  Lemma cleanup_nfw_nil P R :
    cleanup P R →
    cleanup (nfw nil nil P) (nfw nil nil R).
  Proof.
    rewrite /cleanup !nfw_empty //.
  Qed.

  Lemma cleanup_case (φ: dProp) P1 P2 R1 R2 :
    (φ → cleanup P1 R1) →
    (¬ φ → cleanup P2 R2) →
    cleanup (case φ P1 P2) (case φ R1 R2).
  Proof.
    rewrite /cleanup /case.
    destruct decide; auto.
  Qed.

  Lemma cleanup_case_same (φ: dProp) P R :
    cleanup P R →
    cleanup (case φ P P) R.
  Proof.
    rewrite /cleanup /case.
    destruct decide; auto.
  Qed.

  Lemma cleanup_case_nested_or (φ ψ: dProp) P Q R :
    once_tc (cleanup (case (φ ∨ ψ) P Q) R) →
    cleanup (case φ P (case ψ P Q)) R.
  Proof.
    rewrite /cleanup /once_tc /case.
    destruct (decide (φ ∨ ψ)%DP), (decide φ), (decide ψ); naive_solver.
  Qed.

  Lemma cleanup_case_nested_and (φ ψ: dProp) P Q R :
    once_tc (cleanup (case (φ ∧ ψ) P Q) R) →
    cleanup (case φ (case ψ P Q) Q) R.
  Proof.
    rewrite /cleanup /case.
    destruct (decide (φ ∧ ψ)%DP), (decide φ), (decide ψ); naive_solver.
  Qed.

  Lemma cleanup_case_nested_not_or_l (φ ψ: dProp) P Q R :
    once_tc (cleanup (case ((¬ φ) ∨ ψ) P Q) R) →
    cleanup (case φ (case ψ P Q) P) R.
  Proof.
    rewrite /cleanup /case.
    destruct (decide ((¬ φ) ∨ ψ)%DP), (decide φ), (decide ψ); simpl in *; naive_solver.
  Qed.

  Lemma cleanup_case_nested_not_or_r (φ ψ: dProp) P Q R :
    once_tc (cleanup (case (φ ∨ ¬ ψ) P Q) R) →
    cleanup (case φ P (case ψ Q P)) R.
  Proof.
    rewrite /cleanup /case.
    destruct (decide (φ ∨ ¬ ψ)%DP), (decide φ), (decide ψ); simpl in *; naive_solver.
  Qed.

  Lemma cleanup_case_true_left (φ: dProp) P :
    cleanup (case φ True%I (abd_exact_post P)) (abd_exact_post P).
  Proof.
    rewrite /cleanup /case.
    destruct decide; simplify.
    eapply simplify_ent; by iIntros "_".
  Qed.

  Lemma cleanup_case_true_right (φ: dProp) P :
    cleanup (case φ (abd_exact_post P) True%I) (abd_exact_post P).
  Proof.
    rewrite /cleanup /case.
    destruct decide; simplify.
    eapply simplify_ent; by iIntros "_".
  Qed.

  Lemma cleanup_conj_true_l P R :
    cleanup P R →
    cleanup (True ∧ P) R.
  Proof.
    rewrite /cleanup.
    eapply simplify_trans, simplify_ent. by iIntros "$".
  Qed.

  Lemma cleanup_conj_true_r P R :
    cleanup P R →
    cleanup (P ∧ True) R.
  Proof.
    rewrite /cleanup.
    eapply simplify_trans, simplify_ent. by iIntros "$".
  Qed.

  Lemma cleanup_conj_same (P R : iProp Σ):
    once_tc (cleanup P R) →
    cleanup (P ∧ P) R.
  Proof.
    rewrite /once_tc /cleanup -!simplify_ent_iff.
    intros ->. by iIntros "$".
  Qed.

  Lemma cleanup_conj (P1 P2 R1 R2 : iProp Σ):
    cleanup P1 R1 →
    cleanup P2 R2 →
    cleanup (P1 ∧ P2) (R1 ∧ R2).
  Proof.
    rewrite /cleanup.
    intros Hs1%ent_simplify Hs2%ent_simplify.
    eapply simplify_ent. rewrite Hs1 Hs2 //.
  Qed.



  (* simplify prop instance *)
  Lemma simplify_prop_pure_conj b φ ψ:
    simplify_prop b (⌜φ ∧ ψ⌝) (⌜φ⌝ ∗ ⌜ψ⌝).
  Proof.
    rewrite /simplify_prop; destruct b; rewrite -simplify_ent_iff.
    - iIntros "[%Hx %Hy]". iPureIntro. done.
    - iIntros "[%Hx %Hy]". iPureIntro. done.
  Qed.

  Lemma simplify_prop_simplify_pure b φ ψ:
    simplify_pure b φ ψ →
    simplify_prop b ⌜φ⌝ ⌜ψ⌝.
  Proof.
    rewrite /simplify_pure /once_tc /simplify_prop; destruct b; rewrite -!simplify_ent_iff.
    all: iIntros (Himpl) "%Hpure"; iPureIntro; naive_solver.
  Qed.

  Lemma simplify_prop_simplify_atom A `{!Atom A} b P:
    simplify_atom b A P →
    simplify_prop b A P.
  Proof.
    rewrite /simplify_atom /once_tc /simplify_prop; destruct b; done.
  Qed.



  Lemma simplify_pure_solve φ:
    Solve φ →
    simplify_pure true φ True.
  Proof.
    rewrite /simplify_pure; done.
  Qed.
End cleanup_pass.


Existing Class cleanup.
Global Hint Mode cleanup - ! - : typeclass_instances.
Global Existing Instance cleanup_refl | 100.
Global Existing Instance cleanup_exists.
Global Existing Instance cleanup_forall.
Global Existing Instance cleanup_sep_assoc | 1.
Global Existing Instance cleanup_sep_exists | 1.
Global Existing Instance cleanup_sep_emp | 1.
Global Existing Instance cleanup_sep_simplify | 5.
Global Existing Instance cleanup_sep_skip | 10.
Global Existing Instance cleanup_nfs_prop | 1.
Global Existing Instance cleanup_nfs_sep | 1.
Global Existing Instance cleanup_nfs_nil_nil | 1.

Global Existing Instance cleanup_wand_sep | 1.
Global Existing Instance cleanup_wand_exists | 1.
Global Existing Instance cleanup_wand_emp | 1.
Global Existing Instance cleanup_wand_simplify | 5.
Global Existing Instance cleanup_wand_skip | 10.
Global Existing Instance cleanup_nfw_cons_sep | 1.
Global Existing Instance cleanup_nfw_cons_pure | 1.
Global Existing Instance cleanup_nfw_nil | 1.

Global Existing Instance cleanup_case | 4.
Global Existing Instance cleanup_case_true_left | 1.
Global Existing Instance cleanup_case_true_right | 1.
Global Existing Instance cleanup_case_same | 1.
Global Existing Instance cleanup_case_nested_or | 2.
Global Existing Instance cleanup_case_nested_and | 2.
Global Existing Instance cleanup_case_nested_not_or_l | 3.
Global Existing Instance cleanup_case_nested_not_or_r | 3.
Global Existing Instance cleanup_conj_true_l | 1.
Global Existing Instance cleanup_conj_true_r | 1.
Global Existing Instance cleanup_conj_same | 2.
Global Existing Instance cleanup_conj | 3.

Existing Class simplify_prop.
Global Hint Mode simplify_prop ! + ! - : typeclass_instances.
Global Existing Instance simplify_prop_pure_conj | 1.
Global Existing Instance simplify_prop_simplify_pure | 2.
Global Existing Instance simplify_prop_simplify_atom | 2.

Existing Class simplify_pure.
Global Hint Mode simplify_pure + ! - : typeclass_instances.
Global Existing Instance simplify_pure_solve | 9.

Existing Class simplify_atom.
Global Hint Mode simplify_atom ! + ! - : typeclass_instances.

Ltac cleanup :=
  eapply simplify_trans; first apply: cleanup_pass.
