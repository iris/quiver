From quiver.base Require Import facts classes dprop.

Section judgments.
  Context {Σ: gFunctors}.

  (* instruction to indicate that the feature is current not implemented *)
  Definition abd_unimplemented {A: Type} (a: A) : iProp Σ :=
    False.

  Definition abd_fail {A: Type} (a: A) : iProp Σ :=
    False.

  Definition abd_exact_post (P: iProp Σ) := P.

  Definition abd_cont_wrap {A} (P: A) : A := P.

  (* if *)
  Definition abd_if (φ: dProp) (P1 P2: iProp Σ) : iProp Σ :=
    case φ P1 P2.

  (* we can use the following for separation logic predicates *)
  Definition abd_pred {In Out: Type} (p: In → Out → iProp Σ) (i: In) (Φ: Out → iProp Σ) : iProp Σ :=
    ∃ o, p i o ∗ Φ o.

  Definition abd_remove_idents (P: iProp Σ) : iProp Σ := P.

  Definition abd_prune_assumptions (P: iProp Σ) : iProp Σ := P.

  Definition abd_loop_continue (P: iProp Σ) : iProp Σ := P.



  Inductive assum_mode : Type :=
    ASSUME   (* pure assertions become assumptions that will be reverted *)
  | ASSERT   (* pure assertions are known already and will not be reverted *)
  | CTX      (* pure assertions are ASSUME if they contain a universally quantified variable, ASSERT otherwise *)
  | RECYCLE. (* we destruct assumptions as much as possible *)

  Definition abd_assume (m: assum_mode) (P Q: iProp Σ) : iProp Σ :=
    P -∗ Q.

  Definition abd_learn_simpl (φ: Prop) (P: iProp Σ): iProp Σ :=
    ⌜φ⌝ -∗ P.



  (* Lemmas about simple Argon judgments *)
  Lemma abd_unimplemented_anything {A: Type} (a: A) (P: iProp Σ):
    abd_unimplemented a ⊢ P.
  Proof.
    rewrite /abd_unimplemented. iIntros "%Hfalse". done.
  Qed.

  Lemma abd_fail_anything {A: Type} (a: A) (P: iProp Σ):
    abd_fail a ⊢ P.
  Proof.
    rewrite /abd_fail. iIntros "%Hfalse". done.
  Qed.

  (* SEPARATION LOGIC PREDICATES *)
  Lemma abd_pred_missing {In Out: Type} (p: In → Out → iProp Σ) (i: In) (Φ: Out → iProp Σ) :
    (∃ o, p i o ∗ Φ o) ⊢ abd_pred p i Φ.
  Proof. done. Qed.

  Lemma abd_pred_intro {In Out: Type} (p: In → Out → iProp Σ) (i: In) (o: Out) (Φ: Out → iProp Σ) :
    p i o ∗ Φ o ⊢ abd_pred p i Φ.
  Proof. iIntros "H". iExists _. iFrame. Qed.


  (* ASSUME *)
  Lemma abd_assume_drop m P Q :
    Q ⊢ abd_assume m P Q.
  Proof.
    iIntros "Q P". iApply "Q".
  Qed.

  Lemma abd_assume_exists {A} (P: A → iProp Σ) m Q :
    (∀ x, abd_assume m (P x) Q) ⊢ abd_assume m (∃ x, P x) Q.
  Proof.
    iIntros "H". iIntros "HP".
    iDestruct "HP" as (x) "HP".
    iApply ("H" $! x).
    iApply "HP".
  Qed.

  Lemma abd_assume_sep m P1 P2 Q :
    abd_assume m P1 (abd_assume m P2 Q) ⊢ abd_assume m (P1 ∗ P2) Q.
  Proof.
    iIntros "H". iIntros "HP".
    iDestruct "HP" as "[HP1 HP2]".
    iApply ("H" with "HP1 HP2").
  Qed.

  Lemma abd_assume_false m (P: iProp Σ):
    True ⊢ abd_assume m False P.
  Proof.
    rewrite /abd_assume. iIntros "_ %Hf". done.
  Qed.

End judgments.

Global Typeclasses Opaque abd_unimplemented abd_fail abd_exact_post abd_cont_wrap abd_if abd_pred.

Global Typeclasses Opaque abd_assume abd_learn_simpl.

Global Typeclasses Opaque abd_loop_continue abd_remove_idents abd_prune_assumptions.