From quiver.base Require Import dprop.
From quiver.argon.base Require Import classes judgments normalforms.

(* progress indicator *)
Inductive progress := GUARANTEED | BLOCKED | MAYBE (p: nat) | IMPOSSIBLE.

Definition type_progress_match {Σ: gFunctors} (p: progress) (G B : iProp Σ) (M: nat → iProp Σ) (I: iProp Σ) : iProp Σ :=
  match p with
  | GUARANTEED => G
  | BLOCKED => B
  | MAYBE p => M p
  | IMPOSSIBLE => I
  end.

Notation "'match' x 'with' '|' 'GUARANTEED' => G '|' 'BLOCKED' => B '|' 'MAYBE' p => M '|' 'IMPOSSIBLE' => I 'end'" := (type_progress_match x G B (λ p, M) I)
  (at level 200, x at level 100, G at level 200, B at level 200, p binder, M at level 200, I at level 200, only printing,
  format "'match'  x  'with' '//' '|'  'GUARANTEED'  =>  G '//' '|'  'BLOCKED'  =>  B '//' '|'  'MAYBE'  p  =>  M '//' '|'  'IMPOSSIBLE'  =>  I '//' 'end'").

Global Typeclasses Opaque type_progress_match.

(* consume vs. recover *)
Inductive precond_mode := CONSUME | RECOVER.


Section existentially_quantified_preconditions.
  Context {Σ: gFunctors}.
  Implicit Types (G: iProp Σ).

  Lemma type_progress_match_guaranteed G B M I :
    G ⊢ type_progress_match GUARANTEED G B M I.
  Proof. done. Qed.

  Lemma type_progress_match_blocked G B M I :
    B ⊢ type_progress_match BLOCKED G B M I.
  Proof. done. Qed.

  Lemma type_progress_match_maybe G B M n I :
    M n ⊢ type_progress_match (MAYBE n) G B M I.
  Proof. done. Qed.

  Lemma type_progress_match_impossible G B M I :
    I ⊢ type_progress_match IMPOSSIBLE G B M I.
  Proof. done. Qed.



  Definition type_precond (X: Type) (pm: precond_mode) (P: X → iProp Σ) (E: X → list (iProp Σ)) (M: X → list (nat * iProp Σ)) (B: X → list (iProp Σ)) (C: X → option (iProp Σ)) : iProp Σ :=
    ∃ x: X, (P x) ∗ ([∗] E x) ∗ ([∗] (M x).*2) ∗ ([∗] B x) ∗ (default emp (C x)).

  Definition type_precond_evars (X: Type) (P: X → iProp Σ) (Φ: iProp Σ → iProp Σ) : iProp Σ :=
    ∃ Q, (Q -∗ ∃ x, P x) ∗ Φ Q.

  Definition type_precond_yield (P: iProp Σ) : iProp Σ := P.

  Definition type_maybe_select (X: Type) (pm: precond_mode) (M: X → list (nat * iProp Σ)) (B: X → list (iProp Σ)) (C: X → option (iProp Σ)) : iProp Σ :=
    type_precond X pm (λ _, emp%I) (λ _, nil) M B C.

  Definition type_maybe (X: Type) (pm: precond_mode) (P: X → iProp Σ) (Φ: (X → iProp Σ) → iProp Σ) : iProp Σ :=
    ∃ Q, (∀ x, Q x -∗ P x) ∗ Φ Q.

  (* shorthands, type_pre_type is left transparent *)
  Definition type_pre_post (T: Type) (pm: precond_mode) (P: T → iProp Σ) (Φ: T → iProp Σ) : iProp Σ :=
    ∃ t, P t ∗ Φ t.


  Global Instance big_sep_id_proper :
    Proper ((≡) ==> (≡)) (λ Ps: list (iProp Σ), [∗] Ps)%I.
  Proof.
    intros Ps1 Ps2 HPs. induction HPs; eauto.
    simpl. f_equiv; done.
  Qed.


  Global Instance type_precond_proper X pm:
    Proper (pointwise_relation _ (≡) ==> pointwise_relation _ (≡) ==> pointwise_relation _ (≡) ==> pointwise_relation _ (≡) ==> pointwise_relation _ (≡) ==> (≡)) (type_precond X pm).
  Proof.
    rewrite /type_precond. intros P1 P2 HP E1 E2 HE M1 M2 HM B1 B2 HB C1 C2 HC.
    f_equiv=>x. rewrite HP HE HB HC. do 3 f_equiv.
    specialize (HM x).  induction HM as [|[n p] [n' p'] M M' [Heq1 Heq2] _ IH]; simpl in *; eauto.
    rewrite Heq2 IH //.
  Qed.


  (* PRECOND *)
  Lemma type_precond_sep_blocked X pm (P1 P2: X → iProp Σ) E M B C:
    type_precond X pm P2 E M (λ x, P1 x :: B x) C
  ⊢ type_precond X pm (λ x, P1 x ∗ P2 x) E M B C.
  Proof.
    rewrite /type_precond. iIntros "H". iDestruct "H" as (x) "(HP & HE & HM & [? ?] & HC)".
    iExists x. iFrame.
  Qed.


  (* Base Cases *)
  Lemma type_precond_add_emp (X: Type) pm (P: X → iProp Σ) E M B C:
      type_precond X pm (λ x, P x ∗ emp) E M B C
    ⊢ type_precond X pm (λ x, P x) E M B C.
  Proof.
    rewrite /type_precond. iDestruct 1 as (x) "([HP _] & HM & HB & HC)".
    iExists x. iFrame.
  Qed.

  Lemma type_precond_emp_done X pm (C: X → option (iProp Σ)):
    (∃ x: X, default emp (C x)) ⊢ type_precond X pm (λ _, emp) (λ _, nil) (λ _, nil) (λ _, nil) C.
  Proof.
    rewrite /type_precond. iIntros "(%x & C)". iExists x; simpl. iFrame.
  Qed.

  Lemma type_precond_emp_evars (X: Type) pm E1 E2 M (B: X → list (iProp Σ)) C P:
    (∀ x, ([∗] (E1 x :: E2 x ++ (M x).*2 ++ B x ++ [default emp (type_precond_yield <$> (C x))]))%I = (P x)) →
    type_precond_evars X (λ x, P x)%I (λ Q, type_precond unit pm (λ _, Q) (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None))
    ⊢ type_precond X pm (λ _, emp) (λ x, E1 x :: E2 x) M B C.
  Proof.
    rewrite /type_precond_evars /type_precond. iIntros (Hs) "H". iDestruct "H" as (Q) "[HQ HQ']".
    iDestruct "HQ'" as "(%u & HQ' & _)". iDestruct ("HQ" with "HQ'") as (x) "HQ".
    iExists x. rewrite -Hs /=. iDestruct "HQ" as "(E1 & E2 & M & B & C & _)".
    iFrame. destruct C; done.
  Qed.

  Lemma type_precond_emp_maybe (X: Type) pm (P: X → iProp Σ) M1 M2 (B: X → list (iProp Σ)) C:
    type_maybe_select X pm (λ x, M1 x :: M2 x) B C
    ⊢ type_precond X pm (λ _, emp) (λ _, nil) (λ x, M1 x :: M2 x) B C.
  Proof.
    rewrite /type_maybe_select //.
  Qed.

  Lemma type_precond_emp_lift_exists X Y pm B1 B2 (C: X * Y → option (iProp Σ)):
    (∃ y: Y, type_precond X pm (λ _, emp)%I (λ _, [emp])%I (λ _, nil) (λ x, B1 (x, y) :: B2 (x, y)) (λ x, C (x, y)))
  ⊢ type_precond (X * Y) pm (λ _, emp)%I (λ _, nil) (λ _, nil) (λ x, B1 x :: B2 x) C.
  Proof.
    rewrite /type_precond. iIntros "(%y & %x & ? & ? & ? & ? & ?)".
    iExists (x, y). by iFrame.
  Qed.

  Lemma type_precond_emp_stuck (X: Type) pm B1 B2 (C: X → option (iProp Σ)):
    False ⊢ type_precond X pm (λ _, emp) (λ _, nil) (λ _, nil) (λ x, B1 x :: B2 x) C.
  Proof.
    rewrite /type_precond. iIntros "H". iDestruct "H" as %H. done.
  Qed.

  (* separating conjunction *)
  Lemma type_precond_sep_assoc (X: Type) pm (P Q R: X → iProp Σ) E M B C:
    type_precond X pm (λ x, P x ∗ (Q x ∗ R x)) E M B C
    ⊢ type_precond X pm (λ x, (P x ∗ Q x) ∗ R x) E M B C.
  Proof.
    rewrite /type_precond. iIntros "H". iDestruct "H" as (x) "([HP HQ] & HE & HM & HB & HC)".
    iExists x. by iFrame.
  Qed.

  Lemma type_precond_sep_exists_pair (X: Type) pm (Y: Type) (P: X → Y → iProp Σ) Q E M B C:
    type_precond (X * Y) pm (λ s, P (s.1) (s.2) ∗ Q (s.1)) (λ s, E (s.1)) (λ s, M (s.1)) (λ s, B (s.1)) (λ s, C (s.1))
    ⊢ type_precond X pm (λ x, (∃ y : Y, P x y) ∗ Q x) E M B C.
  Proof.
    rewrite /type_precond. iIntros "H". iDestruct "H" as (s) "([HP HQ] & HE & HM & HB & HC)".
    iExists (s.1). iFrame. iExists (s.2). done.
  Qed.

  Lemma type_precond_sep_exists (X: Type) pm (Y: X → Type) (P: ∀ x, Y x → iProp Σ) Q E M B C:
    type_precond (sigT Y) pm (λ s, P (projT1 s) (projT2 s) ∗ Q (projT1 s)) (λ s, E (projT1 s)) (λ s, M (projT1 s)) (λ s, B (projT1 s)) (λ s, C (projT1 s))
    ⊢ type_precond X pm (λ x, (∃ y : Y x, P x y) ∗ Q x) E M B C.
  Proof.
    rewrite /type_precond. iIntros "H". iDestruct "H" as (s) "([HP HQ] & HE & HM & HB & HC)".
    iExists (projT1 s). iFrame. iExists (projT2 s). done.
  Qed.

  Lemma type_precond_sep_continuation (X: Type) pm (Y: X → Type) (P: X → iProp Σ) (Q: X → iProp Σ) E M B:
    type_precond X pm Q E M B (λ x, Some (P x))
    ⊢ type_precond X pm (λ x, P x ∗ Q x) E M B (λ _, None).
  Proof.
    rewrite /type_precond. iIntros "H". iDestruct "H" as (x) "(HQ & HE & HM & HB & HP)".
    iExists x. iFrame.
  Qed.

  Lemma type_precond_sep_emp (X: Type) pm (P : X → iProp Σ) E M B C:
    type_precond X pm P E M B C ⊢ type_precond X pm (λ x, emp ∗ P x) E M B C.
  Proof.
    rewrite /type_precond. iIntros "H". iDestruct "H" as (x) "(HP & HE & HM & HB & HC)".
    iExists x. iFrame.
  Qed.

  (* pure propositions *)
  (* NOTE: make sure that x and y are projections, otherwise precond matching can diverge *)
  Lemma type_precond_pure_evars (X: Type) pm (φ: X → Prop) (P: X → iProp Σ) E M B C:
    type_precond X pm P (λ z, ⌜φ z⌝ :: E z) M B C
  ⊢ type_precond X pm (λ z, ⌜φ z⌝ ∗ P z) E M B C.
  Proof.
    rewrite /type_precond. iIntros "H". iDestruct "H" as (z) "(HP & [Hx HE] & HM & HB & HC)".
    iExists z. iFrame.
  Qed.

  Lemma type_precond_impl X pm (φ ψ: X → Prop) (P: X → iProp Σ) E M B C:
    (∀ x, (ψ x) → (φ x)) →
    type_precond X pm (λ x, ⌜ψ x⌝ ∗ P x)%I E M B C
    ⊢ type_precond X pm (λ x, ⌜φ x⌝ ∗ P x)%I E M B C.
  Proof.
    rewrite /type_precond. iIntros (Hsimpl) "Hpre". iDestruct "Hpre" as (x) "((%Hφ & Hpre) & HE & HM & HB & HC)".
    iExists x. iFrame. iPureIntro. by eapply Hsimpl.
  Qed.

  Lemma type_precond_pure_conj X pm φ ψ (P: X → iProp Σ) E M B C:
    type_precond X pm (λ x, ⌜φ x⌝ ∗ (⌜ψ x⌝ ∗ P x))%I E M B C
    ⊢ type_precond X pm (λ x, ⌜φ x ∧ ψ x⌝ ∗ P x)%I E M B C.
  Proof.
    rewrite /type_precond. iIntros "Hpre". iDestruct "Hpre" as (x) "((%Hφ & %Hψ & Hpre) & HE & HM & HB & HC)".
    iExists x. iFrame. iPureIntro. done.
  Qed.

  Lemma type_precond_sep_pure_lift (X: Type) pm (φ: Prop) (P: X → iProp Σ) E M B C:
    ⌜φ⌝ ∗ type_precond X pm P E M B C
  ⊢ type_precond X pm (λ x, ⌜φ⌝ ∗ P x) E M B C.
  Proof.
    rewrite /type_precond. iIntros "H". iDestruct "H" as "(Hφ & H)".
    iDestruct "H" as (z) "(HP & HE & HM & HB & HC)". iExists z. iFrame.
  Qed.

  Lemma type_precond_forall2_nil_l X Y Z pm (us: X → list Y) φ (P: X → iProp Σ) E M B C :
    (type_precond X pm (λ z, ⌜us z = nil⌝ ∗ P z) E M B C)
    ⊢ type_precond X pm (λ z, ⌜Forall2 φ (us z) (nil: list Z)⌝ ∗ P z)%I E M B C.
  Proof.
    rewrite /type_precond. iIntros "(%x & [%Heq P] & ? & ? & ? & ?)". iExists _.
    iFrame. iPureIntro. rewrite Heq. done.
  Qed.

  Lemma type_precond_forall2_nil_r X Y Z pm (us: X → list Y) φ (P: X → iProp Σ) E M B C :
    (type_precond X pm (λ z, ⌜us z = nil⌝ ∗ P z) E M B C)
    ⊢ type_precond X pm (λ z, ⌜Forall2 φ (nil: list Z) (us z)⌝ ∗ P z)%I E M B C.
  Proof.
    rewrite /type_precond. iIntros "(%x & [%Heq P] & ? & ? & ? & ?)". iExists _.
    iFrame. iPureIntro. rewrite Heq. done.
  Qed.

  Lemma type_precond_forall2_cons_r X Y Z pm (us: list Y) x (y: X → list Z) φ (P: X → iProp Σ) E M B C :
    (∃ u ur, ⌜us = u :: ur⌝ ∗ type_precond X pm (λ z, ⌜Forall2 φ (u :: ur) (x z :: y z)⌝ ∗ P z)%I E M B C)
    ⊢ type_precond X pm (λ z, ⌜Forall2 φ (us) (x z :: y z)⌝ ∗ P z)%I E M B C.
  Proof.
    iIntros "(%u & %ur & -> & Hpre)".
    rewrite /type_precond. iDestruct "Hpre" as "(%z & (%Hall & ?) & ? & ? & ? & ?)".
    iExists _. iFrame. iPureIntro. done.
  Qed.

  Lemma type_precond_forall2_cons_l X Y Z pm (us: list Y) x (y: X → list Z) φ (P: X → iProp Σ) E M B C :
    (∃ u ur, ⌜us = u :: ur⌝ ∗ type_precond X pm (λ z,  ⌜Forall2 φ (x z :: y z) (u :: ur)⌝ ∗ P z)%I E M B C)
    ⊢ type_precond X pm (λ z, ⌜Forall2 φ (x z :: y z) (us)⌝ ∗ P z)%I E M B C.
  Proof.
    iIntros "(%u & %ur & -> & Hpre)".
    rewrite /type_precond. iDestruct "Hpre" as "(%z & (%Hall & ?) & ? & ? & ? & ?)".
    iExists _. iFrame. iPureIntro. done.
  Qed.

  Lemma type_precond_forall2_cons X Y Z pm x y (xr: X → list Y) (yr: X → list Z) φ (P: X → iProp Σ) E M B C :
    type_precond X pm (λ z, ⌜φ (x z) (y z)⌝ ∗ ⌜Forall2 φ (xr z) (yr z)⌝ ∗ P z)%I E M B C
  ⊢ type_precond X pm (λ z, ⌜Forall2 φ (x z :: xr z) (y z :: yr z)⌝ ∗ P z)%I E M B C.
  Proof.
    rewrite /type_precond. iIntros "(%z & [%Hφ [%Hall P]] & ? & ? & ? & ?)".
    iExists _. iFrame. iPureIntro. eapply Forall2_cons. done.
  Qed.


  Lemma type_precond_sep_pure_blocked (X: Type) pm (φ: X → Prop) (P: X → iProp Σ) E M B C:
    type_precond X pm P E M (λ x, ⌜φ x⌝ :: B x) C
  ⊢ type_precond X pm (λ x, ⌜φ x⌝ ∗ P x) E M B C.
  Proof.
    rewrite -type_precond_sep_blocked //.
  Qed.

  Lemma type_precond_sep_nfs X pm Γ Ω (P: X → iProp Σ) Q E M B C:
    type_precond X pm (λ x, nfs_lin (Γ x) (Ω x) (P x) ∗ Q x)%I E M B C
    ⊢ type_precond X pm (λ x, nfs (Γ x) (Ω x) (P x) ∗ Q x)%I E M B C.
  Proof.
    rewrite /type_precond. f_equiv. intros ?. rewrite nfs_nfs_lin //.
  Qed.


  Lemma type_precond_sep_case X pm φ (P Q R: X → iProp Σ) E M B C :
    case φ (type_precond X pm (λ x, P x ∗ R x) E M B C) (type_precond X pm (λ x, (Q x) ∗ R x) E M B C)
  ⊢ type_precond X pm (λ x, case φ (P x) (Q x) ∗ R x) E M B C.
  Proof.
    rewrite /type_precond. iIntros "Hcase".
    rewrite /case; destruct decide; simpl; done.
  Qed.

  Lemma type_precond_sep_case_blocked X pm φ (P Q R: X → iProp Σ) E M B C:
    type_precond X pm R E M (λ x, case (φ x) (P x) (Q x) :: B x) C
    ⊢ type_precond X pm (λ x, case (φ x) (P x) (Q x) ∗ R x) E M B C.
  Proof.
    rewrite -type_precond_sep_blocked //.
  Qed.

  Lemma type_precond_conj_sep_l X pm P (Q : X → iProp Σ):
    type_precond X pm (λ x, P)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None) ∧ type_precond X pm (λ x, Q x ∗ emp)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)
  ⊢ type_precond X pm (λ x, (P ∧ Q x) ∗ emp) (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None).
  Proof.
    rewrite /type_precond. rewrite bi.and_exist_l /=.
    iIntros "(%x & HPQ)". iExists x.
    repeat iSplit; [| | done..].
    - iDestruct "HPQ" as "(P & _)". iDestruct "P" as "(%y & [$ _])".
    - iDestruct "HPQ" as "(_ & [HQ _] & _)". iFrame.
  Qed.

  Lemma type_precond_conj_sep_r X pm (P : X → iProp Σ) Q:
    type_precond X pm (λ x, P x ∗ emp)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None) ∧
    type_precond X pm (λ _, Q) (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)
  ⊢ type_precond X pm (λ x, (P x ∧ Q) ∗ emp) (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None).
  Proof.
    rewrite /type_precond. rewrite bi.and_exist_r /=.
    iIntros "(%x & HPQ)". iExists x.
    repeat iSplit; [| | done..].
    - iDestruct "HPQ" as "[HP _]". iDestruct "HP" as "([$ _] & _)".
    - iDestruct "HPQ" as "[_ Q]". iDestruct "Q" as "(%y & [$ _])".
  Qed.

  Lemma type_precond_conj_blocked X pm (P Q R : X → iProp Σ) E M B C:
    type_precond X pm (λ x, R x) E M (λ x, (P x ∧ Q x) :: B x) C
  ⊢ type_precond X pm (λ x, (P x ∧ Q x) ∗ R x) E M B C.
  Proof.
    rewrite /type_precond.
    iIntros "HR". iDestruct "HR" as (x) "(? & ? & ? & [? ?] & ?)".
    iExists _. iFrame.
  Qed.

  (* add maybe *)
  Lemma type_precond_sep_maybe (X: Type) pm (P Q: X → iProp Σ) E M M' B C :
    (∀ x, [∗] (M' x).*2 ⊣⊢ P x ∗ [∗] ((M x).*2)) →
    type_precond X pm (λ x, Q x) E M' B C ⊢
    type_precond X pm (λ x, P x ∗ Q x) E M B C.
  Proof.
    rewrite /type_precond. iIntros (Heq) "(%x & HQ & HE & HM & HB & HD)".
    iExists x. iFrame. by rewrite Heq.
  Qed.

  Lemma type_precond_evars_ent X (P: X → iProp Σ) R Φ :
    (R ⊢ ∃ x, P x)%I →
    Φ R ⊢ type_precond_evars X P Φ.
  Proof.
    iIntros (Hent) "HΦ". iExists R. iFrame.
    rewrite Hent. iIntros "$".
  Qed.

  Lemma type_precond_sep_predicate X pm Q E M B C I O p (i: I) (o: X → O) :
    Predicate I O p →
    (abd_pred p i (λ o', type_precond X pm (λ x, ⌜o' = o x⌝ ∗ Q x) E M B C)) ⊢ type_precond X pm (λ x, p i (o x) ∗ Q x) E M B C.
  Proof.
    rewrite /abd_pred. iIntros (Hp) "H". iDestruct "H" as (o') "[Hp Hprecond]".
    iDestruct "Hprecond" as (x) "([%HQ HP] & HE & HM & HB & HC)". subst.
    iExists x. iFrame.
  Qed.

  Lemma type_precond_sep_predicate_dependent X pm Q E M B C I O p (i: X → I) (o: X → O) :
    Predicate I O p →
    type_precond X pm Q E M (λ x, p (i x) (o x) :: B x) C ⊢ type_precond X pm (λ x, p (i x) (o x) ∗ Q x) E M B C.
  Proof.
    iIntros (Hp) "Hprecond". iDestruct "Hprecond" as (x) "(HP & HE & HM & [HQ HB] & HC)".
    iExists x. iFrame.
  Qed.

  (* existsN *)
  Lemma type_precond_existsN_zero X Y pm (P: X → list Y → iProp Σ) Q E M B C:
    type_precond X pm (λ x, P x nil ∗ Q x) E M B C
  ⊢ type_precond X pm (λ x, existsN 0%nat (P x) ∗ Q x) E M B C.
  Proof.
    rewrite /type_precond /existsN.
    iIntros "(%x & [HP HQ] & ? & ? & ? & ?)". iExists x. iFrame.
    iExists nil. iFrame. by iPureIntro.
  Qed.

  Lemma type_precond_existsN_succ X Y pm n (P: X → list Y → iProp Σ) Q E M B C:
    type_precond X pm (λ x, ∃ y, existsN n (λ ys, P x (y :: ys)) ∗ Q x) E M B C
  ⊢ type_precond X pm (λ x, existsN (S n) (P x) ∗ Q x) E M B C.
  Proof.
    rewrite /type_precond /existsN. iIntros "(%x & [%y [[%yr [%Hlen HP]] HQ]] & ? & ? & ? & ?)".
    iExists x. iFrame. iExists (y :: yr). iFrame. iPureIntro.
    rewrite /= Hlen //.
  Qed.

  (* MAYBE SELECT *)
  Lemma type_maybe_select_first (X: Type) pm (M: X → iProp Σ) n Ms B C:
    type_maybe X pm (λ x, M x) (λ Q, type_precond X pm Q (λ _, nil) Ms B C) ⊢
    type_maybe_select X pm (λ x, (n, M x) :: Ms x) B C .
  Proof.
    rewrite /type_maybe_select /type_maybe.
    rewrite /type_precond. iIntros "[%Q [HQM HCont]]".
    iDestruct "HCont" as "(%x & ? & ? & ? & ? & ?)". iExists _. iFrame.
    by iApply "HQM".
  Qed.

  Lemma type_maybe_select_skip (X: Type) pm (M: X → iProp Σ) n Ms B C:
    type_maybe_select X pm Ms (λ x, M x :: B x) C ⊢
    type_maybe_select X pm (λ x, (n, M x) :: Ms x) B C .
  Proof.
    rewrite /type_maybe_select /type_maybe.
    rewrite /type_precond. iIntros "HCont".
    iDestruct "HCont" as "(%x & ? & ? & ? & [? ?] & ?)". iExists _. iFrame.
  Qed.

  Lemma type_maybe_select_impossible (X: Type) pm B C:
    False ⊢ type_maybe_select X pm (λ _, []) B C.
  Proof.
    iIntros "%Hf". done.
  Qed.

  (* VARIABLES *)

End existentially_quantified_preconditions.


Notation type_pre pm P := (type_precond unit pm (λ _, P) (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)).

Global Typeclasses Opaque
  type_precond
  type_pre_post
  type_precond_evars
  type_precond_yield
  type_maybe
  type_maybe_select.
