From quiver.base Require Export facts classes.


(* resource predicates *)
Class Predicate {Σ: gFunctors} (I O: Type) (P: I → O → iProp Σ) : Prop := {}.


(* Atoms are the propositions that can be part of the
   spatial context [Δ] in [abduct Δ P R]. *)
Class Atom {Σ: gFunctors} (A: iProp Σ) : Prop :=
  atom : sealed (∃ B, A = B).

Lemma is_atom {Σ: gFunctors} (A: iProp Σ): Atom A.
Proof. rewrite /Atom sealed_eq. by exists A. Qed.

Global Instance predicate_atom {Σ: gFunctors} (I O: Type) (P: I → O → iProp Σ) i o:
  Predicate I O P →
  Atom (P i o).
Proof. by intros ?; apply is_atom. Qed.



(* Context Interactions *)
Section context_search.
  Context {Σ: gFunctors}.

  Inductive find_depth : Type := DEEP | SHALLOW.

  Class MatchAtom (A B: iProp Σ) : Prop :=
    match_atom : A ⊣⊢ B.

  Class FindAtom (f: find_depth) (Δ: list (iProp Σ)) (A: iProp Σ) (Δ': list (iProp Σ)) :=
    mk_find_atom: [∗] Δ ⊣⊢ A ∗ [∗] Δ'.

  Class FindAtomInProp (P: iProp Σ) (A: iProp Σ) (Q: iProp Σ) :=
    mk_find_in_prop: P ⊣⊢ A ∗ Q.

  Lemma find_atom fd Δ A Δ' `{FA: !FindAtom fd Δ A Δ'}:
    [∗] Δ ⊢ A ∗ [∗] Δ'.
  Proof.
    rewrite mk_find_atom //.
  Qed.

  Lemma find_atom_rev fd Δ A Δ' `{FA: !FindAtom fd Δ A Δ'}:
    A ∗ [∗] Δ' ⊢ [∗] Δ.
  Proof.
    rewrite mk_find_atom //.
  Qed.

  (* rules for finding an atom in a context *)
  Global Instance find_atom_head fd Δ A B:
    MatchAtom A B →
    FindAtom fd (A :: Δ) B Δ | 2.
  Proof.
    rewrite /FindAtom /MatchAtom. intros Hmatch.
    simpl. rewrite Hmatch //.
  Qed.

  Global Instance find_atom_in_prop Δ P A Q :
    FindAtomInProp P A Q →
    FindAtom DEEP (P :: Δ) A (Q :: Δ) | 3.
  Proof.
    rewrite /FindAtomInProp /FindAtom. intros Hfind.
    simpl. rewrite Hfind bi.sep_assoc //.
  Qed.

  Global Instance find_atom_tail fd (Δ: list (iProp Σ)) A B Δ':
    FindAtom fd Δ A Δ' →
    FindAtom fd (B :: Δ) A (B :: Δ') | 4.
  Proof.
    rewrite /FindAtom. intros Hfind.
    rewrite //= Hfind !bi.sep_assoc (bi.sep_comm B A) //.
  Qed.


  Global Instance find_atom_app_l fd Δ1 Δ1' (A: iProp Σ) Δ2:
    FindAtom fd Δ1 A Δ1' →
    FindAtom fd (Δ1 ++ Δ2) A (Δ1' ++ Δ2) | 2.
  Proof.
    rewrite /FindAtom. intros Heq.
    rewrite !big_sepL_app Heq bi.sep_assoc //.
  Qed.

  Global Instance find_atom_app_r fd Δ1 (A: iProp Σ) Δ2 Δ2':
    FindAtom fd Δ2 A Δ2' →
    FindAtom fd (Δ1 ++ Δ2) A (Δ1 ++ Δ2') | 3.
  Proof.
    rewrite /FindAtom. intros Heq.
    rewrite !big_sepL_app Heq bi.sep_assoc (bi.sep_comm _ A) -bi.sep_assoc //.
  Qed.


  Global Instance MatchAtomRefl A: MatchAtom A A.
  Proof. rewrite /MatchAtom. done. Qed.


  Global Instance find_in_prop_match A B:
    MatchAtom A B →
    FindAtomInProp A B emp | 1.
  Proof.
    rewrite /FindAtomInProp /MatchAtom. intros Hmatch.
    rewrite Hmatch // right_id //.
  Qed.

  (* sometimes we want to find an exact proposition in the context *)
  Definition find_in_ctx (A P: iProp Σ) : iProp Σ := A ∗ P.

End context_search.

Global Typeclasses Opaque find_in_ctx.
Global Hint Mode MatchAtom - ! ! : typeclass_instances.
Global Hint Mode FindAtom - + ! ! - : typeclass_instances.
Global Hint Mode FindAtomInProp - ! ! - : typeclass_instances.


Section pure_context_search.

  Class MatchPureAtom (A B: Prop) : Prop :=
    match_pure_atom : A ↔ B.

  Class FindPureAtom (Δ: list (Prop)) (A: Prop) (Δ': list (Prop)) :=
    mk_find_pure_atom: Forall id Δ ↔ (A ∧ Forall id Δ').

  Lemma find_atom_pure Δ A Δ' `{FA: !FindPureAtom Δ A Δ'}:
    Forall id Δ → A ∧ Forall id Δ'.
  Proof.
    rewrite mk_find_pure_atom //.
  Qed.

  Lemma find_atom_pure_rev Δ A Δ' `{FA: !FindPureAtom Δ A Δ'}:
    A ∧ Forall id Δ' → Forall id Δ.
  Proof.
    rewrite mk_find_pure_atom //.
  Qed.

  (* rules for finding an atom in a context *)
  Global Instance find_pure_atom_head Γ A B:
    MatchPureAtom A B →
    FindPureAtom (A :: Γ) B Γ | 2.
  Proof.
    rewrite /FindPureAtom /MatchPureAtom. intros Hmatch.
    rewrite Forall_cons /= Hmatch //.
  Qed.

  Global Instance find_pure_atom_tail (Γ: list (Prop)) A B Γ':
    FindPureAtom Γ A Γ' →
    FindPureAtom (B :: Γ) A (B :: Γ') | 4.
  Proof.
    rewrite /FindPureAtom. intros Hfind.
    rewrite !Forall_cons //= Hfind; naive_solver.
  Qed.

  Global Instance MatchPureAtomRefl A: MatchPureAtom A A.
  Proof. rewrite /MatchPureAtom. done. Qed.

End pure_context_search.

Global Hint Mode MatchPureAtom ! ! : typeclass_instances.
Global Hint Mode FindPureAtom ! ! - : typeclass_instances.


(* type class for extracting a pure proposition from an atom *)
Definition extract_from_atom {Σ: gFunctors} (A: iProp Σ) (φ: Prop) := A ⊢ ⌜φ⌝.
Existing Class extract_from_atom.
Global Hint Mode extract_from_atom - ! - : typeclass_instances.

Global Instance  destruct_atom_default {Σ: gFunctors} (A: iProp Σ): extract_from_atom A True | 1000.
Proof. rewrite /extract_from_atom. by iIntros "_". Qed.

Section extract_from_atom_instances.
  Context {Σ: gFunctors}.

  (* technically pure propositions are not atoms, but this instance is convenient *)
  Global Instance extract_from_atom_pure φ:
    extract_from_atom (⌜φ⌝: iProp Σ)%I φ | 1.
  Proof.
    rewrite /extract_from_atom //.
  Qed.

End extract_from_atom_instances.