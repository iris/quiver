
From quiver.base Require Import facts.
From quiver.argon.base Require Import classes.

Section linearizing.
  Context {Σ: gFunctors}.

  (* linerarize a list of iProps with a continuation *)
  Fixpoint lin_sep (Ω: list (iProp Σ)) (Q: iProp Σ) : iProp Σ :=
    match Ω with
    | [] => Q
    | P :: Ω => P ∗ lin_sep Ω Q
    end.

  Lemma lin_sep_iff Ω Q: lin_sep Ω Q ⊣⊢ ([∗] Ω) ∗ Q.
  Proof.
    induction Ω as [| P Ω]; simpl.
    - rewrite left_id //.
    - rewrite IHΩ. by rewrite -bi.sep_assoc.
  Qed.

  Fixpoint lin_wand (Ω: list (iProp Σ)) (Q: iProp Σ) : iProp Σ :=
    match Ω with
    | [] => Q
    | P :: Ω => P -∗ lin_wand Ω Q
    end.

  Lemma lin_wand_iff Ω Q: lin_wand Ω Q ⊣⊢ (([∗] Ω) -∗ Q).
  Proof.
    induction Ω as [| P Ω]; simpl.
    - rewrite left_id //.
    - rewrite IHΩ. rewrite -bi.wand_curry //.
  Qed.

End linearizing.


Section normalforms.
  Context {Σ: gFunctors}.

  (* a large separating conjunction *)
  Definition nfs (Γ: list Prop) (Δ: list (iProp Σ)) (Φ: iProp Σ) : iProp Σ :=
    l2p Γ ∗ ([∗] Δ) ∗ Φ.

  (* a large wand *)
  Definition nfw (Γ: list Prop) (Δ: list (iProp Σ)) (P: iProp Σ) : iProp Σ :=
    l2p Γ -∗ ([∗] Δ) -∗ P.

  Definition nfs_lin (Γ: list Prop) (Δ: list (iProp Σ)) (Φ: iProp Σ) : iProp Σ :=
    lin_sep (map bi_pure Γ) (lin_sep Δ Φ).

  Definition nfw_lin (Γ: list Prop) (Δ: list (iProp Σ)) (P: iProp Σ) : iProp Σ :=
    lin_wand (map bi_pure Γ) (lin_wand Δ P).

  Lemma nfs_nfs_lin Γ Δ Φ: nfs Γ Δ Φ ⊣⊢ nfs_lin Γ Δ Φ.
  Proof.
    rewrite /nfs /nfs_lin !lin_sep_iff.
    rewrite l2p_big_sep. rewrite big_sepL_fmap //.
  Qed.

  Lemma nfw_nfw_lin Γ Δ P: nfw Γ Δ P ⊣⊢ nfw_lin Γ Δ P.
  Proof.
    rewrite /nfw /nfw_lin !lin_wand_iff.
    rewrite l2p_big_sep. rewrite big_sepL_fmap //.
  Qed.

  (* basic properties *)
  Global Instance nfs_proper : Proper (equiv ==> equiv ==> equiv ==> equiv) nfs.
  Proof.
    intros Γ Γ' HΓ Δ Δ' HΔ P P' HP. rewrite /nfs.
    f_equiv; last f_equiv; auto.
    - by f_equiv.
    - by eapply big_sepL_equiv.
  Qed.

  Global Instance nfw_proper : Proper (equiv ==> equiv ==> equiv ==> equiv) nfw.
  Proof.
    intros Γ Γ' HΓ Δ Δ' HΔ P P' HP. rewrite /nfw.
    f_equiv; last f_equiv; auto.
    - by f_equiv.
    - by eapply big_sepL_equiv.
  Qed.

  Lemma nfs_empty (P: iProp Σ):
    nfs nil nil P ⊣⊢ P.
  Proof.
    rewrite /nfs.
    iSplit.
    - iIntros "(_ & _ & $)".
    - iIntros "$". simpl; done.
  Qed.

  Lemma nfw_empty (P: iProp Σ):
    nfw nil nil P ⊣⊢ P.
  Proof.
    rewrite /nfw.
    iSplit.
    - iIntros "Hcont". by iApply "Hcont".
    - iIntros "HP _ _". iApply "HP".
  Qed.

  Lemma nfs_cons_atom Γ A Δ (P: iProp Σ):
    nfs Γ (A :: Δ) P ⊣⊢ A ∗ nfs Γ Δ P.
  Proof.
    rewrite /nfs; simpl. iSplit; iIntros "(? & ? & ?)"; by iFrame.
  Qed.

  Lemma nfs_cons_pure Γ φ Δ (P: iProp Σ):
    nfs (φ :: Γ) Δ P ⊣⊢ ⌜φ⌝ ∗ nfs Γ Δ P.
  Proof.
    rewrite /nfs; simpl. rewrite l2p_cons -bi.sep_assoc //.
  Qed.

  Lemma nfw_cons_atom Γ A Δ (P: iProp Σ):
    nfw Γ (A :: Δ) P ⊣⊢ (A -∗ nfw Γ Δ P).
  Proof.
    rewrite /nfw; simpl.
    iSplit; iIntros "Hnfw".
    - iIntros "HA HΓ HΔ". by iApply ("Hnfw" with "[$] [$]").
    - iIntros "HΓ [HA HΔ]". by iApply ("Hnfw" with "[$] [$]").
  Qed.

  Lemma nfw_cons_pure Γ φ Δ (P: iProp Σ):
    nfw (φ :: Γ) Δ P ⊣⊢ (⌜φ⌝ -∗ nfw Γ Δ P).
  Proof.
    rewrite /nfw; simpl.
    iSplit; iIntros "Hnfw".
    - iIntros "Hφ HΓ HΔ". rewrite l2p_cons.
      by iApply ("Hnfw" with "[$] [$]").
    - rewrite l2p_cons.
      iIntros "[Hφ HΓ] HΔ". by iApply ("Hnfw" with "[$] [$]").
  Qed.

  Lemma nfs_app Γ1 Γ2 Δ1 Δ2 P :
    nfs (Γ1 ++ Γ2) (Δ1 ++ Δ2) P ⊣⊢ nfs Γ1 Δ1 (nfs Γ2 Δ2 P).
  Proof.
    rewrite /nfs. rewrite !big_sepL_app l2p_app -!bi.sep_assoc.
    iSplit; by iIntros "($ & $ & $ & $ & $)".
  Qed.

  Lemma nfw_app Γ1 Γ2 Δ1 Δ2 P :
    nfw (Γ1 ++ Γ2) (Δ1 ++ Δ2) P ⊣⊢ nfw Γ1 Δ1 (nfw Γ2 Δ2 P).
  Proof.
    rewrite /nfw. rewrite !big_sepL_app l2p_app.
    rewrite -!bi.wand_curry.
    iSplit; iIntros "Hnfw ? ? ? ?"; by iApply ("Hnfw" with "[$] [$] [$] [$]").
  Qed.

  Lemma nfs_exists Γ Δ {X: Type} (P: X → iProp Σ) :
    nfs Γ Δ (∃ x, P x) ⊣⊢ ∃ x, nfs Γ Δ (P x).
  Proof.
    rewrite /nfs. iSplit.
    - iIntros "(HΓ & HΔ & [%x HP])". iExists x. iFrame.
    - iIntros "[%x (HΓ & HΔ & HP)]". iFrame. by iExists x.
  Qed.

  Lemma nfs_sep Γ Δ (P Q: iProp Σ) :
    nfs Γ Δ (P ∗ Q) ⊣⊢ P ∗ nfs Γ Δ Q.
  Proof.
    rewrite /nfs. iSplit; iIntros "($ & $ & $ & $)".
  Qed.


  Lemma nfs_borrow Γ Δ P :
    nfs Γ Δ P ⊣⊢ P ∗ nfs Γ Δ emp.
  Proof.
    rewrite /nfs. iSplit.
    - iIntros "(HΓ & HΔ & HP)". iFrame.
    - iIntros "[HP (HΓ & [HΔ _])]". iFrame.
  Qed.

  Lemma nfw_all Γ Δ {X: Type} (P: X → iProp Σ) :
    nfw Γ Δ (∀ x, P x) ⊣⊢ ∀ x, nfw Γ Δ (P x).
  Proof.
    rewrite /nfw. iSplit.
    - iIntros "HP". iIntros (x) "HΓ HΔ". by iApply ("HP" with "[$] [$]").
    - iIntros "HP HΓ HΔ". iIntros (x). by iApply ("HP" with "[$] [$]").
  Qed.

  Lemma nfw_borrow Γ Δ P :
    nfw Γ Δ P ⊣⊢ (nfs Γ Δ emp -∗ P).
  Proof.
    rewrite /nfw /nfs. iSplit.
    - iIntros "HP (HΓ & HΔ & _)". by iApply ("HP" with "[$] [$]").
    - iIntros "HP HΓ HΔ". by iApply ("HP" with "[$]").
  Qed.

  Lemma nfs_find_atom fd {Γ F Δ Δ'} {P : iProp Σ}:
    FindAtom fd Δ F Δ' →
    nfs Γ Δ P ⊣⊢ nfs Γ (F :: Δ') P.
  Proof.
    rewrite /nfs. intros ->.
    rewrite -!bi.sep_assoc //.
  Qed.

  Lemma nfw_find_atom fd {Γ F Δ Δ'} {P : iProp Σ}:
    FindAtom fd Δ F Δ' →
    nfw Γ Δ P ⊣⊢ nfw Γ (F :: Δ') P.
  Proof.
    rewrite /nfw. intros ->.
    rewrite /= -!bi.wand_curry //.
  Qed.

  Lemma nfs_find_pure {φ Γ Γ' Δ} {P : iProp Σ}:
    FindPureAtom Γ φ Γ' →
    nfs Γ Δ P ⊣⊢ nfs (φ :: Γ') Δ P.
  Proof.
    rewrite /nfs. rewrite l2p_cons.
    intros Hfind; iSplit; iIntros "(%HΓ & $ & $)"; iPureIntro.
    - rewrite /FindPureAtom in Hfind. naive_solver.
    - rewrite /FindPureAtom in Hfind. by eapply Hfind.
  Qed.

  Lemma nfw_find_pure {φ Γ Γ' Δ} {P : iProp Σ}:
    FindPureAtom Γ φ Γ' →
    nfw Γ Δ P ⊣⊢ nfw (φ :: Γ') Δ P.
  Proof.
    rewrite /nfw. rewrite l2p_cons.
    intros Hfind; iSplit; iIntros "H %HΓ HΔ";
    iApply ("H" with "[] HΔ"); iPureIntro.
    - rewrite /FindPureAtom in Hfind. by eapply Hfind.
    - rewrite /FindPureAtom in Hfind. naive_solver.
  Qed.

End normalforms.