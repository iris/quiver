From quiver.base Require Export classes.
From quiver.argon.abduction Require Export abduct.
From quiver.argon.simplification Require Import normalize elim_existentials cleanup postcond_passes.
From iris.proofmode Require Import string_ident.
From quiver.argon.base Require Import judgments.

(** COQ PROOFMODE TACTICS *)
Ltac reset_yield := idtac.

Definition clear_universals (φ: Prop) : Prop := φ.
Global Typeclasses Opaque clear_universals.

Ltac clear_universals :=
  reset_yield;
  repeat match goal with
  | [H: uni _ |- clear_universals _] => clear H
  end;
  unfold clear_universals at 1.


Definition clear_universals_tc (φ: Prop) : Prop := clear_universals φ.
Existing Class clear_universals_tc.
Global Hint Mode clear_universals_tc - : typeclass_instances.
Global Hint Extern 0 (clear_universals_tc _) =>
  (rewrite {1}/clear_universals_tc; clear_universals; [apply _]) : typeclass_instances.


Global Typeclasses Opaque ctx_remove.

Ltac ctx_remove :=
  match goal with
  | |- ctx_remove ?φ ?ψ => ctx_remove_hyp φ; change ψ
  end.


Definition ctx_remove_tc (φ ψ: Prop): Prop := ctx_remove φ ψ.
Existing Class ctx_remove_tc.
Global Hint Mode ctx_remove_tc - - : typeclass_instances.
Global Hint Extern 0 (ctx_remove_tc _ _) =>
  (rewrite {1}/ctx_remove_tc; ctx_remove; [apply _]) : typeclass_instances.


Definition stop (φ: Prop) : Prop := φ.
Global Typeclasses Opaque stop.

Ltac skip_stop :=
  match goal with
  | |- stop ?φ => change φ
  end.




(* named tactics *)
Definition named_all (s: string) {X: Type} (f: X → Prop) := ∀ x, f x.
Notation "'∀!{' s '}' x , P" := (named_all s (λ x, P))
  (at level 200, x binder, right associativity,
  format "'[hv' '∀!{' s '}' '/ '  x  , '/ '  P ']'") : type_scope.


Definition pose (s: string) {X: Type} (x: X) (φ: X → Prop) := φ x.
Definition pose_proof (s: option string) (φ: Prop) (ψ: Prop) := φ → ψ.
Definition clear (s: string) (φ: Prop) := φ.
Definition lookup_ctx (s: string) {X: Type} (φ: X → Prop) := ∃ x, φ x.

Ltac str_to_name s f :=
  let name := eval simpl in s in
  let var := string_to_ident name in
  f var.


Ltac intro_named_forall :=
  match goal with
  | |- named_all ?s ?f => str_to_name s ltac:(fun var => intros var; inst_evars var)
  end.

Ltac pose_named :=
  lazymatch goal with
  | |- pose ?s ?x ?φ =>
    str_to_name s ltac:(fun var => pose (var := x); change (pose s var φ); unfold pose)
  end.

Ltac pose_proof_named :=
  lazymatch goal with
  | |- pose_proof None ?φ ?ψ => rewrite /pose_proof; intros ?
  | |- pose_proof (Some ?s) ?φ ?ψ =>
    str_to_name s ltac:(fun var => rewrite /pose_proof; intros var)
  end.


Ltac clear_or_subst H := (clear H || subst H).

Ltac clear_named :=
  lazymatch goal with
  | |- clear ?s ?φ =>
    str_to_name s ltac:(fun var => rewrite /clear; clear_or_subst var)
  end.

Ltac lookup_ctx_named :=
  lazymatch goal with
  | |- lookup_ctx ?s ?f =>
    str_to_name s ltac:(fun var => rewrite /lookup_ctx; exists var)
  end.


Ltac pm_tactic :=
  match goal with
  | |- clear_universals _ => clear_universals
  | |- evar_forall _ => evar_forall_intro
  | |- ctx_remove _ _ => ctx_remove
  | |- named_all _ _ => intro_named_forall
  | |- pose _ _ _ => pose_named
  | |- pose_proof _ _ _ => pose_proof_named
  | |- clear _ _ => clear_named
  | |- lookup_ctx _ _ => lookup_ctx_named
  end.


(* QUANTIFIER CHECKS *)
Ltac has_uni X :=
  match goal with
  | [H: uni ?x |- _] =>
    lazymatch X with
    | context [x] => idtac
    end
  end.

Class has_uni {X: Type} (x: X) := {}.
Global Hint Mode has_uni - ! : typeclass_instances.
Global Hint Extern 1 (has_uni ?x) => (has_uni x; split) : typeclass_instances.

Notation no_uni X := (TCUnless (has_uni X)).


(* Automation for abduction *)
(* We do not turn abduction into a type class.
   Instead, we have a type class for executing *a single step*
   of abduction inference. *)
Class AbductStep {Σ: gFunctors} (φ: Prop) (Δ: qenvs Σ) P R :=
  abduct_step: φ → abduct Δ P R.

Global Hint Mode AbductStep - - - ! - : typeclass_instances.

Section basic_abduction_steps.
  Context {Σ: gFunctors}.
  Implicit Types (Δ: qenvs Σ) (φ: Prop).


  Global Instance abduct_step_wand_emp Δ P R:
    AbductStep (abduct Δ P R) Δ (emp -∗ P) R | 2.
  Proof. intros Habd. by apply abduct_wand_emp. Qed.

  Global Instance abduct_step_wand Δ Δ' A `{!Atom A} P R:
    qenvs_add_sep_assertion Δ A Δ' →
    AbductStep (abduct Δ' P R) Δ (A -∗ P) R.
  Proof. intros ??. by eapply abduct_wand_assume. Qed.

  Global Instance abduct_step_wand_sep Δ P1 P2 P3 R:
    AbductStep (abduct Δ (P1 -∗ P2 -∗ P3) R) Δ ((P1 ∗ P2) -∗ P3) R.
  Proof. intros ?; by apply abduct_wand_sep. Qed.

  Global Instance abduct_step_wand_exists Δ X P Q R:
    AbductStep (abduct Δ (∀ x, P x -∗ Q) R) Δ ((∃ x: X, P x) -∗ Q) R.
  Proof. intros ?; by apply abduct_wand_exists. Qed.

  Global Instance abduct_step_wand_assume_pure Δ φ P R:
    AbductStep (abduct Δ (abd_assume ASSUME ⌜φ⌝ P) R) Δ (⌜φ⌝ -∗ P) R.
  Proof. intros ?; by apply abduct_wand_assume_pure. Qed.

  Global Instance abduct_step_sep_assoc Δ P1 P2 P3 R:
    AbductStep (abduct Δ (P1 ∗ (P2 ∗ P3)) R) Δ ((P1 ∗ P2) ∗ P3) R.
  Proof. intros Habd. by eapply abduct_assoc. Qed.

  Global Instance abduct_step_case_left Δ P1 P2 (φ: dProp) (R: iProp Σ):
    Δ ⊨ Solve φ →
    AbductStep (abduct Δ P1 R) Δ (case φ P1 P2) R | 1.
  Proof. rewrite /Solve. intros ??. eapply abduct_case_true; eauto. Qed.

  Global Instance abduct_step_case_right Δ P1 P2 (φ: dProp) (R: iProp Σ):
    Δ ⊨ Solve (¬ φ) →
    AbductStep (abduct Δ P2 R) Δ (case φ P1 P2) R | 2.
  Proof. rewrite /Solve. intros ??. eapply abduct_case_false; eauto. Qed.

  Global Instance abduct_step_case Δ P1 P2 (φ: dProp) (R: iProp Σ):
    no_uni φ →
    AbductStep (abduct Δ (abd_if φ P1 P2) R) Δ (case φ P1 P2) R | 10.
  Proof. intros ??. rewrite /abd_if //. Qed.

  Global Instance abduct_step_case_conj Δ P1 P2 (φ: dProp) (R: iProp Σ):
    has_uni φ →
    AbductStep (abduct Δ ((abd_assume ASSUME ⌜φ⌝ P1) ∧ (abd_assume ASSUME ⌜¬ φ⌝ P2)) R) Δ (case φ P1 P2) R | 20.
  Proof.
    intros ??. by eapply abduct_case_conj_assume.
  Qed.

  Global Instance abduct_step_conj_true_l Δ P R:
    AbductStep (abduct Δ P R) Δ (True ∧ P) R | 1.
  Proof. intros ?. by apply abduct_conj_true_l. Qed.

  Global Instance abduct_step_conj_true_r Δ P R:
    AbductStep (abduct Δ P R) Δ (P ∧ True) R | 1.
  Proof. intros ?. by apply abduct_conj_true_r. Qed.

  Global Instance abduct_step_conj_false_l Δ P R:
    AbductStep (abduct Δ False R) Δ (False ∧ P) R | 1.
  Proof. intros ?. by apply abduct_conj_false_l. Qed.

  Global Instance abduct_step_conj_false_r Δ P R:
    AbductStep (abduct Δ False R) Δ (P ∧ False) R | 1.
  Proof. intros ?. by apply abduct_conj_false_r. Qed.

  Global Instance abduct_step_conj `{!default_conj} Δ (P1 P2 Q1 Q2: iProp Σ):
    AbductStep ((abduct Δ P1 Q1) ∧ (abduct Δ P2 Q2)) Δ (P1 ∧ P2) (Q1 ∧ Q2) | 10.
  Proof. intros ?. by eapply abduct_conj. Qed.

  (* IF *)
  Lemma abduct_abd_if `{!default_if} Δ (φ: dProp) (P1 P2 R1 R2: iProp Σ):
    abduct Δ (abd_assume ASSERT ⌜φ⌝ P1) R1 →
    abduct Δ (abd_assume ASSERT ⌜¬φ⌝ P2) R2 →
    abduct Δ (abd_if φ P1 P2) (case φ R1 R2).
  Proof. rewrite /abd_if. intros. by apply abduct_case_lift. Qed.

  (* IF *)
  Global Instance abduct_step_abd_if `{!default_if} Δ (φ: dProp) (P1 P2 R1 R2: iProp Σ):
    AbductStep (abduct Δ (abd_assume ASSERT ⌜φ⌝ P1) R1 ∧ abduct Δ (abd_assume ASSERT ⌜¬φ⌝ P2) R2)
      Δ (abd_if φ P1 P2) (case φ R1 R2).
  Proof. intros [? ?]. by apply abduct_abd_if. Qed.


  Global Instance abduct_step_find_in_ctx Δ Δ' A P R:
    qenvs_find_atom DEEP Δ A Δ' →
    AbductStep (abduct Δ' P R) Δ (find_in_ctx A P) R | 20.
  Proof.
    rewrite /qenvs_find_atom. intros Hfind Habd. rewrite /abduct Hfind.
    iIntros "[[A Δ] R]".
    rewrite /find_in_ctx. iFrame. iApply Habd. iFrame.
  Qed.

  Lemma abduct_finish_true (Δ : qenvs Σ) R:
    unify R True%I →
    abduct Δ True R.
  Proof. intros ->. eapply abduct_true. Qed.

  Global Instance abduct_step_true (Δ: qenvs Σ) R:
    unify R True%I →
    AbductStep True Δ True R | 1.
  Proof.
    intros ??. by eapply abduct_finish_true.
  Qed.

  Lemma abduct_abort Δ (P: iProp Σ) R:
    unify R (abd_fail "unsupported goal") →
    abduct Δ P R.
  Proof.
    rewrite /unify. intros ->.
    rewrite -(abd_fail_anything _ P). apply abduct_refl.
  Qed.

  Global Instance abduct_step_abort Δ (P: iProp Σ) R:
    unify R (abd_fail "unsupported")%I →
    AbductStep True Δ P R | 100000000.
  Proof. intros ??. by eapply abduct_abort. Qed.

  (* ABDUCTING ATOMS *)
  Global Instance abduct_step_sep_atom Δ A `{!Atom A} (P R: iProp Σ):
    AbductStep (abduct Δ (abduct_atom A P) R) Δ (A ∗ P) R.
  Proof.
    intros ?. by eapply abduct_sep_atom.
  Qed.

  Global Instance abduct_step_abduct_atom_ctx Δ Δ' (A: iProp Σ) (P R : iProp Σ) :
    qenvs_find_atom DEEP Δ A Δ' →
    AbductStep (abduct Δ' P R) Δ (abduct_atom A P) R | 1.
  Proof.
    intros ??. by eapply abduct_atom_ctx.
  Qed.

  Global Instance abduct_step_abduct_atom_missing Δ (A: iProp Σ) (P R : iProp Σ) :
    AbductStep (abduct Δ P R) Δ (abduct_atom A P) (A ∗ R) | 10.
  Proof.
    intros ?. by eapply abduct_atom_missing.
  Qed.

  (* QUANTIFIERS *)
  Global Instance abduct_step_forall_pair Δ X Y P R:
    AbductStep (abduct Δ (∀ x: X, ∀ y: Y, P (x, y)) R) Δ (∀ p: X * Y, P p) R | 2.
  Proof. intros Habd. by apply abduct_forall_pair. Qed.

  Global Instance abduct_step_forall_intros Δ X P R:
    AbductStep (∀ x: X, uni x → abduct Δ (P x) R) Δ (∀ x: X, P x) R | 10.
  Proof. intros Habd. by apply abduct_forall_intros. Qed.

  Global Instance abduct_step_forall_named_intros Δ s X P R:
    AbductStep (∀!{s} x: X, uni x → abduct Δ (P x) R) Δ (∀{s} x: X, P x) R | 10.
  Proof. intros Habd. by eapply abduct_forall_named_intros. Qed.

  Global Instance abduct_step_exists_pair Δ A B (P: A * B → iProp Σ) R:
    AbductStep (abduct Δ (∃ a, ∃ b, P (a, b)) R) Δ (∃ p, P p) R | 2.
  Proof. intros Habd. by apply abduct_exists_pair. Qed.

  Global Instance abduct_step_exists Δ X P R:
    AbductStep (∀! x: X, exi x → abduct Δ (P x) (R x)) Δ (∃ x: X, P x) (∃ x: X, R x) | 10.
  Proof. intros Habd. by eapply abduct_exists. Qed.

  Global Instance abduct_step_exists_named Δ s X P R:
    AbductStep (∀!{s} x: X, exi x → abduct Δ (P x) (R x)) Δ (∃{s} x: X, P x) (∃ x: X, R x) | 10.
  Proof. intros Habd. by eapply abduct_exists_named. Qed.

  Global Instance abduct_step_existsN_zero Δ {X} (P: list X → iProp Σ) R:
    AbductStep (abduct Δ (P nil) R) Δ (existsN 0 P) R.
  Proof. intros ?; by eapply abduct_existsN_zero. Qed.

  Global Instance abduct_step_existsN_succ Δ {X} n (P: list X → iProp Σ) R:
    AbductStep (abduct Δ (∃ x: X, existsN n (λ L, P (x :: L))) R) Δ (existsN (S n) P) R.
  Proof. intros ?; by eapply abduct_existsN_succ. Qed.

  Global Instance abduct_step_existsL_nil A Δ (P: list A → iProp Σ) R:
    AbductStep (abduct Δ (P nil) R)
      Δ (existsL nil P) R.
  Proof. intros ?. by eapply abduct_existsL_nil. Qed.

  Global Instance abduct_step_existsL_cons A s ss Δ P R:
    AbductStep (abduct Δ (∃{s} x: A, existsL ss (λ xr, P (x :: xr))) R)
      Δ (existsL (s :: ss) P) R.
  Proof. intros ?. by eapply abduct_existsL_cons. Qed.

  Global Instance abduct_step_forallL_nil A Δ (P: list A → iProp Σ) R:
    AbductStep (abduct Δ (P nil) R)
      Δ (forallL nil P) R.
  Proof. intros ?. by eapply abduct_forallL_nil. Qed.

  Global Instance abduct_step_forallL_cons A s ss Δ P R:
    AbductStep (abduct Δ (∀{s} x: A, forallL ss (λ xr, P (x :: xr))) R)
      Δ (forallL (s :: ss) P) R.
  Proof. intros ?. by eapply abduct_forallL_cons. Qed.


  (* BIG OPS *)
  Global Instance abduct_step_nfs_cons_pure Δ φ Γ Ω (P R: iProp Σ):
    AbductStep (abduct Δ (abduct_pure φ (nfs Γ Ω P)) R) Δ (nfs (φ :: Γ) Ω P) R.
  Proof. intros ?; by eapply abduct_nfs_cons_pure. Qed.

  Global Instance abduct_step_nfs_cons_atom Δ A Γ Ω (P R: iProp Σ):
    AbductStep (abduct Δ (abduct_atom A (nfs Γ Ω P)) R) Δ (nfs Γ (A :: Ω) P) R.
  Proof. intros ?; by eapply abduct_nfs_cons_atom. Qed.

  Global Instance abduct_step_nfs_empty Δ (P R: iProp Σ):
    AbductStep (abduct Δ P R) Δ (nfs [] [] P) R.
  Proof. intros ?; by eapply abduct_nfs_empty. Qed.

  Global Instance abduct_step_nfw_cons_pure Δ φ Γ Ω (P R: iProp Σ):
    AbductStep (abduct Δ (abd_assume ASSUME ⌜φ⌝ (nfw Γ Ω P)) R) Δ (nfw (φ :: Γ) Ω P) R.
  Proof. intros ?; by eapply abduct_nfw_cons_pure. Qed.

  Global Instance abduct_step_nfw_cons_atom Δ A Γ Ω (P R: iProp Σ):
    AbductStep (abduct Δ (abd_assume ASSUME A (nfw Γ Ω P)) R) Δ (nfw Γ (A :: Ω) P) R.
  Proof. intros ?; by eapply abduct_nfw_cons_atom. Qed.

  Global Instance abduct_step_nfw_empty Δ (P R: iProp Σ):
    AbductStep (abduct Δ P R) Δ (nfw [] [] P) R.
  Proof. intros ?; by eapply abduct_nfw_empty. Qed.

End basic_abduction_steps.



(* a marker for continuations in a loop *)
Section loop_continue.
  Context {Σ: gFunctors}.

  Definition loop_continue (P: iProp Σ): iProp Σ := P.

  Global Instance loop_continue_atom P: Atom (loop_continue P).
  Proof. apply is_atom. Qed.

  Definition loop_separate_loop_continue (P: iProp Σ) (Q R: iProp Σ) : Prop :=
    (P -∗ R) ⊢ Q.

  Lemma loop_separate_loop_continue_loop_continue P Γ Ω Q:
    loop_separate_loop_continue P (nfw Γ (loop_continue P :: Ω) Q) (nfw Γ Ω Q).
  Proof.
    rewrite /loop_separate_loop_continue nfw_cons_atom /loop_continue //.
  Qed.

  Lemma loop_separate_loop_continue_loop_continue_wand P Q:
    loop_separate_loop_continue P ((loop_continue P) -∗ Q) Q.
  Proof.
    rewrite /loop_separate_loop_continue /loop_continue //.
  Qed.

  Lemma loop_separate_loop_continue_nfw_cons P A Γ Ω Q R:
    loop_separate_loop_continue P (nfw Γ Ω Q) R →
    loop_separate_loop_continue P (nfw Γ (A :: Ω) Q) (A -∗ R).
  Proof.
    rewrite /loop_separate_loop_continue nfw_cons_atom.
    intros <-. iIntros "R A P". iApply ("R" with "P A").
  Qed.

  Lemma loop_separate_loop_continue_loop_continue_skip_wand A P Q R:
    loop_separate_loop_continue A Q R →
    loop_separate_loop_continue A (P -∗ Q) (P -∗ R).
  Proof.
    rewrite /loop_separate_loop_continue /loop_continue //.
    iIntros (Hent) "Hw P". iApply Hent. iIntros "A".
    iApply ("Hw" with "A P").
  Qed.

  Lemma loop_separate_loop_continue_true P:
    loop_separate_loop_continue P True True.
  Proof.
    rewrite /loop_separate_loop_continue. by iIntros "_".
  Qed.

  Lemma loop_separate_loop_continue_conj P Q1 Q2 R1 R2:
    loop_separate_loop_continue P Q1 R1 →
    loop_separate_loop_continue P Q2 R2 →
    loop_separate_loop_continue P (Q1 ∧ Q2) (R1 ∧ R2).
  Proof.
    rewrite /loop_separate_loop_continue. intros <- <-.
    iIntros "Hw". iSplit; iIntros "P".
    - iDestruct ("Hw" with "P") as "[$ _]".
    - iDestruct ("Hw" with "P") as "[_ $]".
  Qed.

  Lemma loop_separate_loop_continue_forall P {A} (Φ: A → iProp Σ) R:
    (∀ x, loop_separate_loop_continue P (Φ x) (R x)) →
    loop_separate_loop_continue P (∀ x, Φ x) (∀ x, R x).
  Proof.
    rewrite /loop_separate_loop_continue. intros Hent. iIntros "Hw".
    iIntros (x). iApply Hent. iIntros "P". iApply ("Hw" with "P").
  Qed.

  Lemma loop_separate_loop_continue_exists_unit P (Φ: unit → iProp Σ) R:
    loop_separate_loop_continue P (Φ ()) R →
    loop_separate_loop_continue P (∃ x, Φ x) R.
  Proof.
    rewrite /loop_separate_loop_continue. intros Hent. iIntros "Hw".
    iExists (). iApply Hent. iIntros "P". by iApply "Hw".
  Qed.

End loop_continue.

Global Typeclasses Opaque loop_continue.
Global Typeclasses Opaque loop_separate_loop_continue.


Existing Class loop_separate_loop_continue.
Global Hint Mode loop_separate_loop_continue - - + - : typeclass_instances.

Global Existing Instance loop_separate_loop_continue_true | 1.
Global Existing Instance loop_separate_loop_continue_loop_continue | 1.
Global Existing Instance loop_separate_loop_continue_loop_continue_wand | 1.
Global Existing Instance loop_separate_loop_continue_nfw_cons | 2.
Global Existing Instance loop_separate_loop_continue_loop_continue_wand | 2.
Global Existing Instance loop_separate_loop_continue_conj | 1.
Global Existing Instance loop_separate_loop_continue_forall | 1.
Global Existing Instance loop_separate_loop_continue_exists_unit | 1.

(* SIMPLIFICATION PASSES FOR EVARS *)
Section simpl_evars.
  Context {Σ: gFunctors}.

  Definition simplify_evars (P Q: iProp Σ) : Prop := simplify P Q.

  Lemma simplify_evars_chain P1 P2 P3 P4 P5 P6 P7:
    sequence_classes [
      normalize P1 P2;
      elim_existentials P2 P3;
      cleanup P3 P4;
      normalize P4 P5;
      elim_existentials P5 P6;
      Simpl P6 P7
    ] → simplify_evars P1 P7.
  Proof.
    rewrite /sequence_classes !Forall_cons !Forall_nil /=.
    rewrite /normalize /elim_existentials /cleanup.
    intros [? [? [? [? [? [Hsimpl ?]]]]]]. rewrite -Hsimpl.
    rewrite /simplify_evars. simplify.
  Qed.

End simpl_evars.

Existing Class simplify_evars.
Global Hint Mode simplify_evars - ! - : typeclass_instances.
Global Existing Instance simplify_evars_chain.


(* STRENGTHENING *)
Definition Strengthening {Σ: gFunctors} (F: iProp Σ → iProp Σ) := ∀ P, F P ⊢ P.
Existing Class Strengthening.
Global Hint Mode Strengthening - + : typeclass_instances.


Section proofmode_instructions.
  Context {Σ: gFunctors}.

  Definition abd_simpl {X: Type} (x: X) (T: X → iProp Σ) : iProp Σ := T x.
  Definition abd_cbn {X: Type} (x: X) (T: X → iProp Σ) : iProp Σ := T x.
  Definition abd_yield (Φ: iProp Σ) : iProp Σ := Φ.
  Definition abd_pose (s: string) {X: Type} (x: X) (P: X → iProp Σ) : iProp Σ := P x.
  Definition abd_pose_proof (s: option string) (φ: Prop) (P: iProp Σ) : iProp Σ := ⌜φ⌝ -∗ P.
  Definition abd_clear (s: string) (P: iProp Σ) : iProp Σ := P.
  Definition abd_lookup (s: string) {X: Type} (Φ: X → iProp Σ) : iProp Σ := ∃ x, Φ x.
  Definition abd_stop (P: iProp Σ) : iProp Σ := P.

  (* Φ is a predicate transformer, typically of type (X1 → ... → Xn → iProp Σ) → iProp Σ where A := (X1 → ... → Xn → iProp Σ) *)
  Definition abd_breakpoint {A: Type} (Φ: A → iProp Σ) (P: (A → iProp Σ) → iProp Σ) : iProp Σ :=
    ∃ Ψ, (∀ a, Ψ a -∗ Φ a) ∗ P Ψ.

  Definition abd_evars {A: Type} (Φ: A → iProp Σ) (P: (A → iProp Σ) → iProp Σ) : iProp Σ :=
    ∃ Ψ, □ (∀ a, Ψ a -∗ Φ a) ∗ P Ψ.

  Definition abd_mark_posts {A} (F: iProp Σ → iProp Σ) `{!Strengthening F} (P: A → iProp Σ) (Φ: (A → iProp Σ) → iProp Σ) : iProp Σ :=
    ∃ Q, (∀ a, Q a -∗ P a) ∗ Φ Q.

  (* SIMPLIFICATION *)
  Lemma abduct_abd_simpl Δ {X: Type} (x y: X) (T: X → iProp Σ) R:
    Simpl x y →
    abduct Δ (T y) R →
    abduct Δ (abd_simpl x T) R.
  Proof.
    intros ->. rewrite /abd_simpl //.
  Qed.

  Global Instance abduct_step_abd_simpl Δ {X: Type} (x y: X) (T: X → iProp Σ) R:
    Simpl x y →
    AbductStep (abduct Δ (T y) R) Δ (abd_simpl x T) R | 1.
  Proof.
    intros ??; by eapply abduct_abd_simpl.
  Qed.

  Lemma abduct_abd_cbn Δ {X: Type} (x y: X) (T: X → iProp Σ) R:
    CBN x y →
    abduct Δ (T y) R →
    abduct Δ (abd_cbn x T) R.
  Proof.
    intros ->. rewrite /abd_cbn //.
  Qed.

  Global Instance abduct_step_abd_cbn Δ {X: Type} (x y: X) (T: X → iProp Σ) R:
    CBN x y →
    AbductStep (abduct Δ (T y) R) Δ (abd_cbn x T) R | 1.
  Proof.
    intros ??; by eapply abduct_abd_cbn.
  Qed.

  (* YIELDING/REVERTING *)
  Definition revert (Δ: qenvs Σ) (P R: iProp Σ) := abduct Δ P R.

  Global Instance abduct_step_abd_yield Δ P R:
    AbductStep (revert Δ P R) Δ (abd_yield P) R | 2.
  Proof. rewrite /AbductStep. done. Qed.

  Lemma revert_assumptions Δ Γ Ω (P: iProp Σ):
    qenvs_sep_assertions Δ Ω →
    qenvs_pure_assumptions Δ Γ →
    revert Δ P (nfw Γ Ω P).
  Proof.
    intros Hsep Hpure. by eapply abduct_revert_assumptions.
  Qed.

  Lemma revert_forall {A} (a: A) Δ P R:
    uni a →
    fun_evar (R a) (λ R', revert Δ P R') →
    revert Δ P (∀ a, R a).
  Proof.
    rewrite /fun_evar.
    intros ??; by eapply abduct_revert_forall.
  Qed.

  Lemma abduct_abd_pose Δ {X: Type} s (x: X) Φ R:
    pose s x (λ y, abduct Δ (Φ y) R) →
    abduct Δ (abd_pose s x Φ) R.
  Proof. done. Qed.

  Global Instance abduct_step_abd_pose Δ {X: Type} s (x: X) Φ R:
    AbductStep (pose s x (λ y, abduct Δ (Φ y) R))
      Δ (abd_pose s x Φ) R | 1.
  Proof. intros ?. by eapply abduct_abd_pose. Qed.

  Lemma abduct_abd_pose_proof Δ s φ Φ R:
    pose_proof s φ (abduct Δ Φ R) →
    abduct Δ (abd_pose_proof s φ Φ) R.
  Proof.
    rewrite /pose_proof /abd_pose_proof.
    eapply abduct_wand_pure_assume_hyp.
  Qed.

  Global Instance abduct_step_abd_pose_proof Δ s φ Φ R:
    AbductStep (pose_proof s φ (abduct Δ Φ R))
      Δ (abd_pose_proof s φ Φ) R | 1.
  Proof. intros ?. by eapply abduct_abd_pose_proof. Qed.

  Lemma abduct_abd_clear Δ s Φ R:
    clear s (abduct Δ Φ R) →
    abduct Δ (abd_clear s Φ) R.
  Proof. done. Qed.

  Global Instance abduct_step_abd_clear Δ s Φ R:
    AbductStep (clear s (abduct Δ Φ R))
      Δ (abd_clear s Φ) R | 1.
  Proof. intros ?. by eapply abduct_abd_clear. Qed.

  Lemma abduct_abd_lookup Δ {X: Type} s (Φ: X → iProp Σ) R:
    lookup_ctx s (λ x, abduct Δ (Φ x) R) →
    abduct Δ (abd_lookup s Φ) R.
  Proof.
    rewrite /lookup_ctx. intros [x Hx].
    rewrite /abd_lookup. by eapply abduct_exists_intro.
  Qed.

  Global Instance abduct_step_abd_lookup Δ {X: Type} s (Φ: X → iProp Σ) R:
    AbductStep (lookup_ctx s (λ x, abduct Δ (Φ x) R))
      Δ (abd_lookup s Φ) R | 1.
  Proof. intros ?. by eapply abduct_abd_lookup. Qed.

  Lemma abduct_abd_stop Δ (P: iProp Σ) R:
    stop (abduct Δ P R) →
    abduct Δ (abd_stop P) R.
  Proof.
    rewrite /abd_stop /stop //.
  Qed.

  Global Instance abduct_step_abd_stop Δ P R:
    AbductStep (stop (abduct Δ P R))
      Δ (abd_stop P) R | 1.
  Proof. intros ?. by eapply abduct_abd_stop. Qed.


  (* BREAKPOINT *)
  Lemma abduct_abd_breakpoint {A: Type} Δ Δ1 Δ2 (Φ: A → iProp Σ) F Q R:
    qenvs_move_assumptions_to_assertions Δ Δ1 →
    qenvs_create_persistent_copy Δ Δ2 →
    clear_universals (∀! a, abduct Δ1 (Φ (abd_cont_wrap a)) (Q a)) →
    abduct Δ2 (F Q) R →
    abduct Δ (abd_breakpoint Φ F) R.
  Proof.
    rewrite /qenvs_move_assumptions_to_assertions /qenvs_create_persistent_copy.
    rewrite /clear_universals /evar_forall /abd_breakpoint /abd_cont_wrap.
    intros Hmov Hcopy Habd1 Habd2.
    iIntros "[HΔ HR]". rewrite qenvs_ctx_persistent_copy -Hcopy.
    rewrite (qenvs_ctx_assumptions_to_assertions Δ) -Hmov.
    iDestruct "HΔ" as "[HΔ2 HΔ1]".
    iPoseProof (Habd2 with "[$HΔ2 $HR]") as "HQ".
    iExists Q. iFrame. iIntros (a) "HQ".
    by iPoseProof (Habd1 with "[$HΔ1 $HQ]") as "HΦ".
  Qed.

  Global Instance abduct_step_abd_breakpoint {A: Type} Δ Δ1 Δ2 (Φ: A → iProp Σ) F Q R:
    qenvs_move_assumptions_to_assertions Δ Δ1 →
    qenvs_create_persistent_copy Δ Δ2 →
    AbductStep (clear_universals (∀! a, abduct Δ1 (Φ (abd_cont_wrap a)) (Q a)) ∧ abduct Δ2 (F Q) R) Δ (abd_breakpoint Φ F) R.
  Proof. intros ??[??]. by eapply abduct_abd_breakpoint. Qed.


  (* PRUNING EVARS IN A PREDICATE TRANSFORMER *)
  Lemma abduct_abd_evars {A: Type} Δ (Φ Ψ: A → iProp Σ) P P' R:
    sequence_classes [
      (∀! a, simplify_evars (Φ a) (Ψ a));
      Simpl (P Ψ) P'
    ] →
    abduct Δ P' R →
    abduct Δ (abd_evars Φ P) R.
  Proof.
    rewrite /abd_evars /sequence_classes.
    rewrite !Forall_cons /=. intros [Hent [Hsimpl _]] Habd.
    iIntros "[HΔ HR]". iPoseProof (Habd with "[$HΔ $HR]") as "HQ".
    rewrite -Hsimpl. iExists _. iFrame. iModIntro. iIntros (a) "HΨ".
    eapply ent_simplify in Hent. rewrite Hent //.
  Qed.

  Global Instance abduct_step_abd_evars {A: Type} Δ (Φ Ψ: A → iProp Σ) P P' R:
    sequence_classes [
      (∀! a, simplify_evars (Φ a) (Ψ a));
      Simpl (P Ψ) P'
    ] →
    AbductStep (abduct Δ P' R) Δ (abd_evars Φ P) R.
  Proof.
    intros ??. by eapply abduct_abd_evars.
  Qed.

  (* MARK POST *)
  Lemma abduct_abd_mark_posts Δ {A} F `{Hs: !Strengthening F} (P Q: A → iProp Σ) Φ R:
    (∀! a, mark_postconditions F (P a) (Q a)) →
    abduct Δ (Φ Q) R →
    abduct Δ (abd_mark_posts F P Φ) R.
  Proof.
    rewrite /evar_forall.
    intros Hmark Habd. iIntros "Hctx".
    iDestruct (Habd with "Hctx") as "HΦ".
    rewrite /abd_mark_posts. iExists _. iFrame.
    iIntros (a) "P". feed pose proof (Hmark a) as Hsimpl.
    { intros S. eapply simplify_ent. eapply Hs. }
    eapply ent_simplify in Hsimpl.
    rewrite Hsimpl //.
  Qed.

  Global Instance abduct_step_abd_mark_posts Δ {A} F `{!Strengthening F} (P Q: A → iProp Σ) Φ R:
    (∀! a, mark_postconditions F (P a) (Q a)) →
    AbductStep (abduct Δ (Φ Q) R)
      Δ (abd_mark_posts F P Φ) R.
  Proof. intros ??. by eapply abduct_abd_mark_posts. Qed.

  Global Instance strengthening_abd_yield: Strengthening (abd_yield).
  Proof. rewrite /Strengthening /abd_yield //. Qed.

  Global Instance strengthening_type_precond_yield: Strengthening (type_precond_yield: iProp Σ → iProp Σ).
  Proof. rewrite /Strengthening /type_precond_yield //. Qed.
End proofmode_instructions.


Global Typeclasses Opaque
  abd_pose
  abd_pose_proof
  abd_clear
  abd_lookup
  abd_stop.


Global Typeclasses Opaque
  abd_simpl
  abd_cbn
  abd_yield
  abd_breakpoint
  abd_evars
  abd_mark_posts.



Section judgments.
  Context {Σ: gFunctors}.

  Lemma abduct_fail_lift Δ {X: Type} (m: X) (R: iProp Σ):
    unify R (abd_fail m) →
    abduct Δ (abd_fail m) R.
  Proof. intros ->. apply abduct_refl. Qed.

  Global Instance abduct_step_fail_lift Δ {A: Type} (m: A) (R : iProp Σ):
    unify R (abd_fail m) →
    AbductStep True Δ (abd_fail m) R.
  Proof. intros ??. by eapply abduct_fail_lift. Qed.


  Lemma abduct_abd_cont_wrap_yield_zero (Δ : qenvs Σ) (P: iProp Σ) (R : iProp Σ) :
    is_var P →
    abduct Δ (abd_yield (abd_cont_wrap P)) R →
    abduct Δ (abd_cont_wrap P) R.
  Proof. rewrite /abd_yield //. Qed.

  Lemma abduct_abd_cont_wrap_yield_one (Δ : qenvs Σ) {A} (P: A → iProp Σ) x (R : iProp Σ) :
    is_var P →
    abduct Δ (abd_yield (abd_cont_wrap P x)) R →
    abduct Δ (abd_cont_wrap P x) R.
  Proof. rewrite /abd_yield //. Qed.

  Lemma abduct_abd_cont_wrap_yield_two (Δ : qenvs Σ) {A B} (P: A → B → iProp Σ) x y (R : iProp Σ) :
    is_var P →
    abduct Δ (abd_yield (abd_cont_wrap P x y)) R →
    abduct Δ (abd_cont_wrap P x y) R.
  Proof. rewrite /abd_yield //. Qed.

  Lemma abduct_abd_cont_wrap_yield_three (Δ : qenvs Σ) {A B C} (P: A → B → C → iProp Σ) x y z (R : iProp Σ) :
    is_var P →
    abduct Δ (abd_yield (abd_cont_wrap P x y z)) R →
    abduct Δ (abd_cont_wrap P x y z) R.
  Proof. rewrite /abd_yield //. Qed.

  Lemma abduct_abd_cont_wrap_yield_four (Δ : qenvs Σ) {A B C D} (P: A → B → C → D → iProp Σ) x y z w (R : iProp Σ) :
    is_var P →
    abduct Δ (abd_yield (abd_cont_wrap P x y z w)) R →
    abduct Δ (abd_cont_wrap P x y z w) R.
  Proof. rewrite /abd_yield //. Qed.

  Lemma abduct_abd_cont_wrap_intro_zero (Δ : qenvs Σ) (P: iProp Σ) (R : iProp Σ) :
    abduct Δ P R →
    abduct Δ (abd_cont_wrap P) R.
  Proof.
    rewrite /abd_cont_wrap //.
  Qed.

  Lemma abduct_abd_cont_wrap_intro_one (Δ : qenvs Σ) {A} (P: A → iProp Σ) x (R : iProp Σ) :
    abduct Δ (P x) R →
    abduct Δ (abd_cont_wrap P x) R.
  Proof.
    rewrite /abd_cont_wrap //.
  Qed.

  Lemma abduct_abd_cont_wrap_intro_two (Δ : qenvs Σ) {A B} (P: A → B → iProp Σ) x y (R : iProp Σ) :
    abduct Δ (P x y) R →
    abduct Δ (abd_cont_wrap P x y) R.
  Proof.
    rewrite /abd_cont_wrap //.
  Qed.

  Lemma abduct_abd_cont_wrap_intro_three (Δ : qenvs Σ) {A B C} (P: A → B → C → iProp Σ) x y z (R : iProp Σ) :
    abduct Δ (P x y z) R →
    abduct Δ (abd_cont_wrap P x y z) R.
  Proof.
    rewrite /abd_cont_wrap //.
  Qed.

  Lemma abduct_abd_cont_wrap_intro_four (Δ : qenvs Σ) {A B C D} (P: A → B → C → D → iProp Σ) x y z w (R : iProp Σ) :
    abduct Δ (P x y z w) R →
    abduct Δ (abd_cont_wrap P x y z w) R.
  Proof.
    rewrite /abd_cont_wrap //.
  Qed.

  Global Instance abduct_step_abd_cont_wrap_yield_zero (Δ : qenvs Σ) (P: iProp Σ) (R : iProp Σ) :
    is_var P →
    AbductStep (abduct Δ (abd_yield (abd_cont_wrap P)) R)
      Δ (abd_cont_wrap P) R.
  Proof. intros ??. by eapply  abduct_abd_cont_wrap_yield_zero. Qed.

  Global Instance abduct_step_abd_cont_wrap_intro_zero (Δ : qenvs Σ) (P: iProp Σ) (R : iProp Σ) :
    AbductStep (abduct Δ P R)
      Δ (abd_cont_wrap P) R | 10.
  Proof. intros ?. by eapply  abduct_abd_cont_wrap_intro_zero. Qed.

  Global Instance abduct_step_abd_cont_wrap_yield_one (Δ : qenvs Σ) {A} (P: A → iProp Σ) x (R : iProp Σ) :
    is_var P →
    AbductStep (abduct Δ (abd_yield (abd_cont_wrap P x)) R)
      Δ (abd_cont_wrap P x) R.
  Proof. intros ??. by eapply  abduct_abd_cont_wrap_yield_one. Qed.

  Global Instance abduct_step_abd_cont_wrap_intro_one (Δ : qenvs Σ) {A} (P: A → iProp Σ) x (R : iProp Σ) :
    AbductStep (abduct Δ (P x) R)
      Δ (abd_cont_wrap P x) R | 10.
  Proof. intros ?. by eapply  abduct_abd_cont_wrap_intro_one. Qed.

  Global Instance abduct_step_abd_cont_wrap_yield_two (Δ : qenvs Σ) {A B} (P: A → B → iProp Σ) x y (R : iProp Σ) :
    is_var P →
    AbductStep (abduct Δ (abd_yield (abd_cont_wrap P x y)) R)
      Δ (abd_cont_wrap P x y) R.
  Proof. intros ??. by eapply  abduct_abd_cont_wrap_yield_two. Qed.

  Global Instance abduct_step_abd_cont_wrap_intro_two (Δ : qenvs Σ) {A B} (P: A → B → iProp Σ) x y (R : iProp Σ) :
    AbductStep (abduct Δ (P x y) R)
      Δ (abd_cont_wrap P x y) R | 10.
  Proof. intros ?. by eapply  abduct_abd_cont_wrap_intro_two. Qed.

  Global Instance abduct_step_abd_cont_wrap_yield_three (Δ : qenvs Σ) {A B C} (P: A → B → C → iProp Σ) x y z (R : iProp Σ) :
    is_var P →
    AbductStep (abduct Δ (abd_yield (abd_cont_wrap P x y z)) R)
      Δ (abd_cont_wrap P x y z) R.
  Proof. intros ??. by eapply  abduct_abd_cont_wrap_yield_three. Qed.

  Global Instance abduct_step_abd_cont_wrap_intro_three (Δ : qenvs Σ) {A B C} (P: A → B → C → iProp Σ) x y z (R : iProp Σ) :
    AbductStep (abduct Δ (P x y z) R)
      Δ (abd_cont_wrap P x y z) R | 10.
  Proof. intros ?. by eapply  abduct_abd_cont_wrap_intro_three. Qed.

  Global Instance abduct_step_abd_cont_wrap_yield_four (Δ : qenvs Σ) {A B C D} (P: A → B → C → D → iProp Σ) x y z w (R : iProp Σ) :
    is_var P →
    AbductStep (abduct Δ (abd_yield (abd_cont_wrap P x y z w)) R)
      Δ (abd_cont_wrap P x y z w) R.
  Proof. intros ??. by eapply  abduct_abd_cont_wrap_yield_four. Qed.

  Global Instance abduct_step_abd_cont_wrap_intro_four (Δ : qenvs Σ) {A B C D} (P: A → B → C → D → iProp Σ) x y z w (R : iProp Σ) :
    AbductStep (abduct Δ (P x y z w) R)
      Δ (abd_cont_wrap P x y z w) R | 10.
  Proof. intros ?. by eapply  abduct_abd_cont_wrap_intro_four. Qed.


  (* SEPARATION LOGIC PREDICATES *)
  Lemma abduct_abd_pred_match Δ Δ' {In Out: Type} (p: In → Out → iProp Σ) (i: In) (o: Out) (Φ: Out → iProp Σ) R:
    qenvs_find_atom DEEP Δ (p i o) Δ' →
    abduct Δ' (Φ o) R →
    abduct Δ (abd_pred p i Φ) R.
  Proof.
    iIntros (Hfind Habd) "Hctx". rewrite Hfind.
    iDestruct ("Hctx") as "[[Hp HΔ] HR]".
    iExists o. iFrame. iApply Habd. iFrame.
  Qed.

  Lemma abduct_abd_pred_missing Δ {In Out: Type} (p: In → Out → iProp Σ) (i: In) (Φ: Out → iProp Σ) R:
    abduct Δ (∃ o, p i o ∗ Φ o) R →
    abduct Δ (abd_pred p i Φ) R.
  Proof. rewrite -abd_pred_missing //. Qed.

  Global Instance abduct_step_abd_pred_match Δ Δ' {In Out: Type} (p: In → Out → iProp Σ) (i: In) (o: Out) (Φ: Out → iProp Σ) R:
    qenvs_find_atom DEEP Δ (p i o) Δ' →
    AbductStep (abduct Δ' (Φ o) R)
      Δ (abd_pred p i Φ) R | 1.
  Proof. intros ??. by eapply abduct_abd_pred_match. Qed.

  Global Instance abduct_step_abd_pred_missing Δ {In Out: Type} (p: In → Out → iProp Σ) (i: In) (Φ: Out → iProp Σ) R:
    AbductStep (abduct Δ (∃ o, p i o ∗ Φ o) R)
      Δ (abd_pred p i Φ) R | 1000.
  Proof. intros ?. by eapply abduct_abd_pred_missing. Qed.

  (* EXACT POST *)
  Lemma abduct_abd_exact_post_loop_continue (Δ Δ' : qenvs Σ) (P Q: iProp Σ):
    qenvs_find_atom SHALLOW Δ (loop_continue Q) Δ' →
    abduct Δ (abd_exact_post P) (loop_continue Q -∗ abd_exact_post P).
  Proof.
    intros Hfind. rewrite /abduct. rewrite Hfind. iIntros "([HQ _] & Hw)".
    by iApply "Hw".
  Qed.

  Lemma abduct_abd_exact_post_done (Δ : qenvs Σ) (P: iProp Σ):
    abduct Δ (abd_exact_post P) (abd_exact_post P).
  Proof.
    rewrite /abduct. iIntros "[_ $]".
  Qed.

  Global Instance abduct_step_abd_exact_post_done Δ (P : iProp Σ):
    AbductStep True Δ (abd_exact_post P) (abd_exact_post P) | 2.
  Proof.
    intros ?; by eapply abduct_abd_exact_post_done.
  Qed.

  Global Instance abduct_step_abd_exact_post_loop_continue (Δ Δ' : qenvs Σ) (P Q: iProp Σ):
    qenvs_find_atom SHALLOW Δ (loop_continue Q) Δ' →
    AbductStep True Δ (abd_exact_post P) (loop_continue Q -∗ abd_exact_post P) | 1.
  Proof.
    intros ??; by eapply abduct_abd_exact_post_loop_continue.
  Qed.

  (* REMOVE IDENTS *)
  Lemma abduct_abd_remove_idents_find Δ Δ' x {A} (a: A) (P: iProp Σ) R:
    qenvs_find_pure_atom Δ (have_ident x a) Δ' →
    abduct Δ' (abd_remove_idents P) R →
    abduct Δ  (abd_remove_idents P) R.
  Proof.
    iIntros (Hfind Habd) "Ctx".
    rewrite Hfind. iDestruct "Ctx" as "[[_ HΔ] HR]".
    iApply Habd. iFrame.
  Qed.

  Lemma abduct_abd_remove_idents_intro Δ (P: iProp Σ) R:
    abduct Δ P R →
    abduct Δ (abd_remove_idents P) R.
  Proof. rewrite /abd_remove_idents //. Qed.

  Global Instance abduct_step_abd_remove_idents_find Δ Δ' x {A} (a: A) (P R: iProp Σ):
    qenvs_find_pure_atom Δ (have_ident x a) Δ' →
    AbductStep (abduct Δ' (abd_remove_idents P) R)
      Δ (abd_remove_idents P) R | 1.
  Proof. intros ? ?. by eapply abduct_abd_remove_idents_find. Qed.

  Global Instance abduct_step_abd_remove_idents_intro Δ (P R: iProp Σ):
    AbductStep (abduct Δ P R)
      Δ (abd_remove_idents P) R | 2.
  Proof. intros ?. by eapply abduct_abd_remove_idents_intro. Qed.

  (* ABD_LOOP_CONTINUE *)
  Lemma abduct_abd_loop_continue Δ (P: iProp Σ) Δ' R:
    qenvs_find_atom SHALLOW Δ (loop_continue P) Δ' →
    abduct Δ' True R →
    abduct Δ (abd_loop_continue P) R.
  Proof.
    iIntros (Hfind Habd) "[Δ R]".
    rewrite Hfind. rewrite /loop_continue /abd_loop_continue.
    iDestruct "Δ" as "[$ _]".
  Qed.

  Global Instance abduct_step_abd_loop_continue Δ (P: iProp Σ) Δ' R:
    qenvs_find_atom SHALLOW Δ (loop_continue P) Δ' →
    AbductStep (abduct Δ' True R) Δ (abd_loop_continue P) R.
  Proof. intros ??. by eapply abduct_abd_loop_continue. Qed.


  (* PRUNE ASSUMPTIONS *)
  Lemma abduct_abd_prune_assumptions_intro Δ (P: iProp Σ) R:
    abduct Δ P R →
    abduct Δ (abd_prune_assumptions P) R.
  Proof. done. Qed.
End judgments.


(* PRUNING ASSUMPTIONS *)

(* To deactive the pruning of the assumptions, use the following code:
Ltac abd_prune_assumptions ::=
  notypeclasses refine (abduct_abd_prune_assumptions_intro _ _ _).
*)
Definition PRUNE_ASSUMPTION_MARKER (P : Prop) := P.

Definition simplify_prune_assumption (P : Prop) (Q : option Prop) :=
  P → default True Q.
Global Typeclasses Opaque simplify_prune_assumption.
Existing Class simplify_prune_assumption.

Lemma tac_simplify_prune_assumption P Q (G : Prop) :
  simplify_prune_assumption P Q →
  (if Q is Some Q' then PRUNE_ASSUMPTION_MARKER Q' → G else G) →
  (PRUNE_ASSUMPTION_MARKER P → G).
Proof. rewrite /simplify_prune_assumption. destruct Q; naive_solver. Qed.

Definition prune_pure_assumptions {A} (Q : list Prop) (a : A) : Prop :=
  Forall id Q.

Lemma abduct_abd_prune_assumptions {Σ} Δ P Γ2' R :
  (let '(QE Γ1 Γ2 Ω1 Ω2) := Δ in
   foldr (λ (P Q : Prop), PRUNE_ASSUMPTION_MARKER P → Q)
     (prune_pure_assumptions Γ2' (QE [] [] Ω1 Ω2, P)) Γ2) →
  (let '(QE Γ1 Γ2 Ω1 Ω2) := Δ in abduct (Σ:=Σ) (QE Γ1 Γ2' Ω1 Ω2) P R) →
  abduct (Σ:=Σ) Δ (abd_prune_assumptions P) R.
Proof.
  destruct Δ as [Γ1 Γ2 Ω1 Ω2].
  rewrite foldr_impl_Forall /abd_prune_assumptions/prune_pure_assumptions/revert => HΓ2.
  move => Habd. iIntros "[? ?]". iApply Habd. iFrame.
  by iApply qenvs_ctx_pure_assumption_mono.
Qed.

Lemma tac_prune_pure_asm_start (G : Prop) :
  (Forall id (@nil Prop) → G) → G.
Proof. naive_solver. Qed.
Lemma tac_prune_pure_asm_step Ps (P G : Prop) :
  (Forall id (P :: Ps) → G) → Forall id (Ps) → PRUNE_ASSUMPTION_MARKER P → G.
Proof. naive_solver. Qed.
Lemma tac_prune_pure_asm_end A Ps (a : A) :
  Forall id Ps → prune_pure_assumptions Ps a.
Proof. done. Qed.

Ltac abd_prune_assumptions :=
  notypeclasses refine (abduct_abd_prune_assumptions _ _ _ _ _);
  (* fst and snd are necessary to simplify projections to remove false dependencies *)
  cbn [foldr fst snd];
  (* either simplify or introduce the assumptions *)
  repeat first [
      notypeclasses refine (tac_simplify_prune_assumption _ _ _ _ _); [solve [refine _]| ]; cbv iota
    | intro];
  (* Push all assumptions into the goal that depend on exi variables *)
  repeat lazymatch goal with
       | H : exi ?x |- _ =>
        repeat match goal with
          | H2 : PRUNE_ASSUMPTION_MARKER ?P |- _ => lazymatch P with | context [x] => revert H2 end
          end;
        clear H
       end;
  (* Push all assumptions into the goal that depend on uni variables that appear in the goal *)
  repeat lazymatch goal with
       | H : uni ?x |- context [?x] =>
           repeat match goal with
             | H2 : PRUNE_ASSUMPTION_MARKER ?P |- _ => lazymatch P with | context [x] => revert H2 end
             end;
           clear H
       end;
  repeat lazymatch goal with | H : PRUNE_ASSUMPTION_MARKER ?P |- _ =>
      (* idtac "pruning" P; *)
      clear H end;
  (* finish by collecting all assumptions in the goal into a Forall *)
  refine (tac_prune_pure_asm_start _ _);
  repeat lazymatch goal with | |- Forall _ _ → _ → _ => refine (tac_prune_pure_asm_step _ _ _ _) end;
  refine (tac_prune_pure_asm_end _ _ _).

Global Hint Extern 5 (AbductStep _ _ (abd_prune_assumptions _) _) =>
       (abd_prune_assumptions) : typeclass_instances.





(* REVERTING *)
Global Typeclasses Opaque revert.

Existing Class revert.
Global Hint Mode revert - ! - - : typeclass_instances.


Ltac abd_revert_quantifier :=
  match goal with
  | [H: uni ?a |- ?P] =>
    lazymatch P with
    | context [a] => eapply (revert_forall a _ _ _ H); [clear H; fun_evar]
    | _ => clear a H
    end
  end.

Ltac abd_revert_quantifiers :=
  lazymatch goal with
  | [H: uni ?a |- revert _  _ _] =>
    abd_revert_quantifier; abd_revert_quantifiers
  | [|- revert _ _ _] => idtac
  end.

Ltac abd_yield :=
  match goal with
  | |- revert ?Δ ?P ?R =>
    abd_revert_quantifiers;
    by apply: revert_assumptions
  end.

Global Hint Extern 0 (revert _ _ _) => abd_yield : typeclass_instances.

Ltac abduct_step :=
  idtac; (* This idtac is important, otherwise abduct_step is not
  thunked when passed as an argument to other tactics. *)
  match goal with
  | |- abduct _ _ _ => apply: abduct_step; [idtac]
  | |- ∀ _: _, _ => intros ?
  | |- ∃ _: _, _ => eexists
  | |- _ → _ => intros ?
  | |- _ ∧ _ => split
  | |- True => done
  | |- _ => pm_tactic
  (* in the case of type classes, we trigger inference *)
  | |- _ => apply _
  end.


Ltac abduct := repeat abduct_step.


(* abduction of a single branch, for debugging *)
Ltac abduct_branch :=
  first [(abduct_step; [abduct_branch|idtac..]) | by abduct_step | idtac ].

(* abduction until the abduction procedure would branch *)
Ltac abduct_until_branch :=
  first [abduct_step; [abduct_until_branch] | idtac].
