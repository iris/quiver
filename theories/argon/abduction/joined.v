From quiver.base Require Import classes.
From quiver.argon.simplification Require Export simplify.

Section joined.
  Context {Σ: gFunctors}.


  Definition joined (φ: dProp) (P1 P2 R: iProp Σ) :=
    simplify (case φ P1 P2) R.


  Definition joined_prop (φ: dProp) (P Q: Prop) := case φ P Q.
  Definition then_prop (φ: dProp) (P: Prop) : Prop := φ → P.
  Definition else_prop (φ: dProp) (P: Prop) : Prop := ¬ φ → P.
  Record framed_prop (P : Prop) : Prop :=  { framed_prop_proof: P }.


  Lemma joined_case φ P Q R:
    unify (case φ P Q) R →
    joined φ P Q R.
  Proof.
    intros <-. rewrite /joined. simplify.
  Qed.

  Lemma joined_conjunction φ P Q R:
    unify (P ∧ Q)%I R →
    joined φ P Q R.
  Proof.
    intros <-. rewrite /joined.
    rewrite /case; destruct decide; auto; eapply simplify_ent.
    - iIntros "[$ _]".
    - iIntros "[_ $]".
  Qed.

End joined.

Global Typeclasses Opaque
  joined_prop
  then_prop
  else_prop.


Existing Class joined.
Global Hint Mode joined - ! ! ! - : typeclass_instances.
