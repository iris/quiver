From quiver.argon.simplification Require Import simplify.
From quiver.argon.abduction Require Import abduct proofmode pure joined.
From quiver.argon.base Require Import judgments.

Section cases.
  Context {Σ: gFunctors}.

  (* IF *)
  Definition type_join (φ: dProp) (P1 P2: iProp Σ) : iProp Σ :=
    case φ P1 P2.

  (* NOTE: This version of joining is weaker than it could be for performance reasons.
     The correct sequence would be:
     1. abduct both branches separately _after clearing the universals_
     2. join the results
     3. abduct both branches again inserting back the universals
     Here, we skip the clearing, which avoids two redundant passes over the same formula.
  *)

  Lemma abduct_abd_if `{!fancy_if} (φ: dProp) P1 P2 Q1 Q2 Δ Δcp Δmv (R : iProp Σ):
    qenvs_create_persistent_copy Δ Δcp →
    qenvs_move_assumptions_to_assertions Δcp Δmv →
    (abduct Δ (abd_assume ASSERT ⌜φ⌝ P1) Q1) →
    (abduct Δ (abd_assume ASSERT ⌜¬ φ⌝ P2) Q2) →
    clear_universals (abduct Δmv (abd_evars (λ _, Q1) (λ Q1', abd_evars (λ _, Q2) (λ Q2', type_join φ (Q1' ()) (Q2' ())))) R) →
    abduct Δ (abd_if φ P1 P2) R.
  Proof.
    rewrite /clear_universals /abd_evars /abd_assume /abd_if.
    intros Hcp Hmv Habd1 Habd2 Habd3.
    iIntros "Hctx". rewrite qenvs_ctx_persistent_copy. rewrite -Hcp.
    rewrite qenvs_ctx_assumptions_to_assertions. rewrite -Hmv.
    iDestruct "Hctx" as "([Hmv HΔ] & R)". iDestruct (Habd3 with "[$Hmv $R]") as "Hcont".
    iDestruct "Hcont" as "(%ψ1 & Hw1 & %ψ2 & Hw2 & Hcont)".
    rewrite /type_join /case. destruct decide.
    - iDestruct ("Hw1" with "Hcont") as "Q". iDestruct (Habd1 with "[$HΔ $Q]") as "Hcont".
      iApply "Hcont". done.
    - iDestruct ("Hw2" with "Hcont") as "Q". iDestruct (Habd2 with "[$HΔ $Q]") as "Hcont".
      iApply "Hcont". done.
  Qed.

  Global Instance abduct_step_abd_if `{!fancy_if} (φ: dProp) P1 P2 Q1 Q2 Δ Δcp Δmv (R : iProp Σ) :
    qenvs_create_persistent_copy Δ Δcp →
    qenvs_move_assumptions_to_assertions Δcp Δmv →
    AbductStep
      ((abduct Δ (abd_assume ASSERT ⌜φ⌝ P1) Q1) ∧
      (abduct Δ (abd_assume ASSERT ⌜¬ φ⌝ P2) Q2) ∧
      abduct Δmv (abd_evars (λ _, Q1) (λ Q1', abd_evars (λ _, Q2) (λ Q2', type_join φ (Q1' ()) (Q2' ())))) R)
    Δ (abd_if φ P1 P2) R.
  Proof. intros ?? (?&?&?). by eapply abduct_abd_if. Qed.

  (* NOTE: it looks like this step drops resources,
     but all resources in Δ should be persistent *)
  Lemma abduct_type_join_join Δ φ P1 P2 Q:
    joined φ P1 P2 Q →
    abduct Δ (type_join φ P1 P2) Q.
  Proof.
    intros Hj. rewrite /type_join. eapply ent_simplify in Hj.
    rewrite -Hj //. eapply abduct_refl.
  Qed.

  (* this abduction reverts the assumptions and universals in both branches *)
  Lemma abduct_type_join_no_join Δ φ P1 P2 :
    abduct Δ (type_join φ P1 P2) (case φ P1 P2).
  Proof.
    rewrite /type_join /abd_yield //. eapply abduct_refl.
  Qed.

  Global Instance abduct_step_type_join_join Δ φ P1 P2 Q:
    joined φ P1 P2 Q →
    AbductStep True Δ (type_join φ P1 P2) Q | 1.
  Proof.
    intros ??. by eapply abduct_type_join_join.
  Qed.

  Global Instance abduct_step_type_join_no_join Δ φ P1 P2:
    AbductStep True Δ (type_join φ P1 P2) (case φ P1 P2) | 10.
  Proof.
    intros ?. by eapply abduct_type_join_no_join.
  Qed.

  (* AND *)
  Definition type_conj (b1 b2: bool) (P1 P2: iProp Σ) : iProp Σ := P1 ∧ P2.

  Lemma type_conj_exists_l b1 b2 X (P1: X → iProp Σ) P2:
    (∃ x, type_conj b1 b2 (P1 x) P2) ⊢ type_conj b1 b2 (∃ x, P1 x) P2.
  Proof.
    rewrite /type_conj. rewrite bi.and_exist_r //.
  Qed.

  Lemma abduct_type_conj_exists_l Δ b1 b2 X (P1: X → iProp Σ) P2 R:
    abduct Δ (∃ x, type_conj b1 b2 (P1 x) P2) R →
    abduct Δ (type_conj b1 b2 (∃ x, P1 x) P2) R.
  Proof. rewrite -type_conj_exists_l //. Qed.

  Global Instance abduct_step_type_conj_exists_l Δ b1 b2 X (P1: X → iProp Σ) P2 R:
    AbductStep (abduct Δ (∃ x, type_conj b1 b2 (P1 x) P2) R)
      Δ (type_conj b1 b2 (∃ x, P1 x) P2) R | 1.
  Proof. intros ?. by eapply abduct_type_conj_exists_l. Qed.

  Lemma type_conj_exists_r b1 b2 X (P1: X → iProp Σ) P2:
    (∃ x, type_conj b1 b2 P2 (P1 x)) ⊢ type_conj b1 b2 P2 (∃ x, P1 x).
  Proof.
    rewrite /type_conj. rewrite bi.and_exist_l //.
  Qed.

  Lemma abduct_type_conj_exists_r Δ b1 b2 X (P1: X → iProp Σ) P2 R:
    abduct Δ (∃ x, type_conj b1 b2 P2 (P1 x)) R →
    abduct Δ (type_conj b1 b2 P2 (∃ x, P1 x)) R.
  Proof. rewrite -type_conj_exists_r //. Qed.

  Global Instance abduct_step_type_conj_exists_r Δ b1 b2 X (P1: X → iProp Σ) P2 R:
    AbductStep (abduct Δ (∃ x, type_conj b1 b2 P2 (P1 x)) R)
      Δ (type_conj b1 b2 P2 (∃ x, P1 x)) R | 1.
  Proof. intros ?. by eapply abduct_type_conj_exists_r. Qed.

  Lemma type_conj_pure_l b1 b2 φ P2:
    ⌜φ⌝ ∗ P2 ⊢ type_conj b1 b2 ⌜φ⌝ P2.
  Proof. rewrite /type_conj. iIntros "[$ $]". Qed.

  Lemma abduct_type_conj_pure_l Δ b1 b2 φ P2 R:
    abduct Δ (⌜φ⌝ ∗ P2) R →
    abduct Δ (type_conj b1 b2 ⌜φ⌝ P2) R.
  Proof. rewrite -type_conj_pure_l //. Qed.

  Global Instance abduct_step_type_conj_pure_l Δ b1 b2 φ P2 R:
    AbductStep (abduct Δ (⌜φ⌝ ∗ P2) R)
      Δ (type_conj b1 b2 ⌜φ⌝ P2) R | 1.
  Proof. intros ?. by eapply abduct_type_conj_pure_l. Qed.

  Lemma type_conj_pure_r b1 b2 φ P2:
    ⌜φ⌝ ∗ P2  ⊢ type_conj b1 b2 P2 ⌜φ⌝.
  Proof. rewrite /type_conj. iIntros "[$ $]". Qed.

  Lemma abduct_type_conj_pure_r Δ b1 b2 φ P2 R:
    abduct Δ (⌜φ⌝ ∗ P2) R →
    abduct Δ (type_conj b1 b2 P2 ⌜φ⌝) R.
  Proof. rewrite -type_conj_pure_r //. Qed.

  Global Instance abduct_step_type_conj_pure_r Δ b1 b2 φ P2 R:
    AbductStep (abduct Δ (⌜φ⌝ ∗ P2) R)
      Δ (type_conj b1 b2 P2 ⌜φ⌝) R | 1.
  Proof. intros ?. by eapply abduct_type_conj_pure_r. Qed.

  Lemma type_conj_sep_pure_l b1 b2 φ P1 P2:
    ⌜φ⌝ ∗ type_conj b1 b2 P1 P2 ⊢ type_conj b1 b2 (⌜φ⌝ ∗ P1) P2.
  Proof. rewrite /type_conj. iIntros "[$ $]". Qed.

  Lemma abduct_type_conj_sep_pure_l Δ b1 b2 φ P1 P2 R:
    abduct Δ (⌜φ⌝ ∗ type_conj b1 b2 P1 P2) R →
    abduct Δ (type_conj b1 b2 (⌜φ⌝ ∗ P1) P2) R.
  Proof. rewrite -type_conj_sep_pure_l //. Qed.

  Global Instance abduct_step_type_conj_sep_pure_l Δ b1 b2 φ P1 P2 R:
    AbductStep (abduct Δ (⌜φ⌝ ∗ type_conj b1 b2 P1 P2) R)
      Δ (type_conj b1 b2 (⌜φ⌝ ∗ P1) P2) R | 1.
  Proof. intros ?. by eapply abduct_type_conj_sep_pure_l. Qed.

  Lemma type_conj_sep_pure_r b1 b2 φ P1 P2:
    ⌜φ⌝ ∗ type_conj b1 b2 P1 P2 ⊢ type_conj b1 b2 P1 (⌜φ⌝ ∗ P2).
  Proof. rewrite /type_conj. iIntros "[$ $]". Qed.

  Lemma abduct_type_conj_sep_pure_r Δ b1 b2 φ P1 P2 R:
    abduct Δ (⌜φ⌝ ∗ type_conj b1 b2 P1 P2) R →
    abduct Δ (type_conj b1 b2 P1 (⌜φ⌝ ∗ P2)) R.
  Proof. rewrite -type_conj_sep_pure_r //. Qed.

  Global Instance abduct_step_type_conj_sep_pure_r Δ b1 b2 φ P1 P2 R:
    AbductStep (abduct Δ (⌜φ⌝ ∗ type_conj b1 b2 P1 P2) R)
      Δ (type_conj b1 b2 P1 (⌜φ⌝ ∗ P2)) R | 1.
  Proof. intros ?. by eapply abduct_type_conj_sep_pure_r. Qed.

  Lemma type_conj_sep_assoc_l b1 b2 P1 P2 P3 Q:
    type_conj b1 b2 (P1 ∗ (P2 ∗ P3)) Q ⊢ type_conj b1 b2 ((P1 ∗ P2) ∗ P3) Q.
  Proof.
    rewrite /type_conj. iIntros "HPQ". iSplit; last first.
    { iDestruct "HPQ" as "[_ $]". }
    iDestruct "HPQ" as "[[HP1 [HP2 HP3]] _]". iFrame.
  Qed.

  Lemma abduct_type_conj_sep_assoc_l Δ b1 b2 P1 P2 P3 Q R:
    abduct Δ (type_conj b1 b2 (P1 ∗ (P2 ∗ P3)) Q) R →
    abduct Δ (type_conj b1 b2 ((P1 ∗ P2) ∗ P3) Q) R.
  Proof. rewrite -type_conj_sep_assoc_l //. Qed.

  Global Instance abduct_step_type_conj_sep_assoc_l Δ b1 b2 P1 P2 P3 Q R:
    AbductStep (abduct Δ (type_conj b1 b2 (P1 ∗ (P2 ∗ P3)) Q) R)
      Δ (type_conj b1 b2 ((P1 ∗ P2) ∗ P3) Q) R | 1.
  Proof. intros ?. by eapply abduct_type_conj_sep_assoc_l. Qed.

  Lemma type_conj_sep_assoc_r b1 b2 P1 P2 P3 Q:
    type_conj b1 b2 Q (P1 ∗ (P2 ∗ P3)) ⊢ type_conj b1 b2 Q ((P1 ∗ P2) ∗ P3).
  Proof.
    rewrite /type_conj. iIntros "HPQ". iSplit.
    { iDestruct "HPQ" as "[$ _]". }
    iDestruct "HPQ" as "[_ [HP1 [HP2 HP3]]]". iFrame.
  Qed.

  Lemma abduct_type_conj_sep_assoc_r Δ b1 b2 P1 P2 P3 Q R:
    abduct Δ (type_conj b1 b2 Q (P1 ∗ (P2 ∗ P3))) R →
    abduct Δ (type_conj b1 b2 Q ((P1 ∗ P2) ∗ P3)) R.
  Proof. rewrite -type_conj_sep_assoc_r //. Qed.

  Global Instance abduct_step_type_conj_sep_assoc_r Δ b1 b2 P1 P2 P3 Q R:
    AbductStep (abduct Δ (type_conj b1 b2 Q (P1 ∗ (P2 ∗ P3))) R)
      Δ (type_conj b1 b2 Q ((P1 ∗ P2) ∗ P3)) R | 1.
  Proof. intros ?. by eapply abduct_type_conj_sep_assoc_r. Qed.

  Lemma type_conj_sep_exists_l b1 b2 X (P1: X → iProp Σ) P2 Q:
    type_conj b1 b2 (∃ x, P1 x ∗ P2) Q ⊢ type_conj b1 b2 ((∃ x, P1 x) ∗ P2) Q.
  Proof.
    rewrite /type_conj. iIntros "HPQ". iSplit; last first.
    { iDestruct "HPQ" as "[_ $]". }
    iDestruct "HPQ" as "[(%x & HP1 & HP2) _]". iFrame.
    by iExists x.
  Qed.

  Lemma abduct_type_conj_sep_exists_l Δ b1 b2 X (P1: X → iProp Σ) P2 Q R:
    abduct Δ (type_conj b1 b2 (∃ x, P1 x ∗ P2) Q) R →
    abduct Δ (type_conj b1 b2 ((∃ x, P1 x) ∗ P2) Q) R.
  Proof. rewrite -type_conj_sep_exists_l //. Qed.

  Global Instance abduct_step_type_conj_sep_exists_l Δ b1 b2 X (P1: X → iProp Σ) P2 Q R:
    AbductStep (abduct Δ (type_conj b1 b2 (∃ x, P1 x ∗ P2) Q) R)
      Δ (type_conj b1 b2 ((∃ x, P1 x) ∗ P2) Q) R | 1.
  Proof. intros ?. by eapply abduct_type_conj_sep_exists_l. Qed.

  Lemma type_conj_sep_exists_r b1 b2 X (P1: X → iProp Σ) P2 Q:
    type_conj b1 b2 Q (∃ x, P1 x ∗ P2) ⊢ type_conj b1 b2 Q ((∃ x, P1 x) ∗ P2).
  Proof.
    rewrite /type_conj. iIntros "HPQ". iSplit.
    { iDestruct "HPQ" as "[$ _]". }
    iDestruct "HPQ" as "[_ (%x & HP1 & HP2)]". iFrame.
    by iExists x.
  Qed.

  Lemma abduct_type_conj_sep_exists_r Δ b1 b2 X (P1: X → iProp Σ) P2 Q R:
    abduct Δ (type_conj b1 b2 Q (∃ x, P1 x ∗ P2)) R →
    abduct Δ (type_conj b1 b2 Q ((∃ x, P1 x) ∗ P2)) R.
  Proof. rewrite -type_conj_sep_exists_r //. Qed.

  Global Instance abduct_step_type_conj_sep_exists_r Δ b1 b2 X (P1: X → iProp Σ) P2 Q R:
    AbductStep (abduct Δ (type_conj b1 b2 Q (∃ x, P1 x ∗ P2)) R)
      Δ (type_conj b1 b2 Q ((∃ x, P1 x) ∗ P2)) R | 1.
  Proof. intros ?. by eapply abduct_type_conj_sep_exists_r. Qed.

  Lemma type_conj_sep_atom_l b1 b2 A P Q `{!Atom A}:
    A ∗ type_conj b1 false P (A -∗ Q) ⊢ type_conj b1 b2 (A ∗ P) Q.
  Proof.
    rewrite /type_conj. iIntros "[A PQ]". iSplit.
    - iDestruct "PQ" as "[$ _]". done.
    - iDestruct "PQ" as "[_ PQ]". iApply "PQ". iFrame.
  Qed.

  Lemma abduct_type_conj_sep_atom_l Δ b1 b2 A P Q `{!Atom A} R:
    abduct Δ (A ∗ type_conj b1 false P (A -∗ Q)) R →
    abduct Δ (type_conj b1 b2 (A ∗ P) Q) R.
  Proof. rewrite -type_conj_sep_atom_l //. Qed.

  Global Instance abduct_step_type_conj_sep_atom_l Δ b1 b2 A P Q `{!Atom A} R:
    AbductStep (abduct Δ (A ∗ type_conj b1 false P (A -∗ Q)) R)
      Δ (type_conj b1 b2 (A ∗ P) Q) R | 5.
  Proof. intros ?. by eapply abduct_type_conj_sep_atom_l. Qed.

  Lemma type_conj_sep_atom_r b1 b2 A P Q `{!Atom A}:
    A ∗ type_conj false b2 (A -∗ P) Q ⊢ type_conj b1 b2 P (A ∗ Q).
  Proof.
    rewrite /type_conj. iIntros "[A PQ]". iSplit.
    - iDestruct "PQ" as "[PQ _]". iApply "PQ". iFrame.
    - iFrame. iDestruct "PQ" as "[_ $]".
  Qed.

  Lemma abduct_type_conj_sep_atom_r Δ b1 b2 A P Q `{!Atom A} R:
    abduct Δ (A ∗ type_conj false b2 (A -∗ P) Q) R →
    abduct Δ (type_conj b1 b2 P (A ∗ Q)) R.
  Proof. rewrite -type_conj_sep_atom_r //. Qed.

  Global Instance abduct_step_type_conj_sep_atom_r Δ b1 b2 A P Q `{!Atom A} R:
    AbductStep (abduct Δ (A ∗ type_conj false b2 (A -∗ P) Q) R)
      Δ (type_conj b1 b2 P (A ∗ Q)) R | 5.
  Proof. intros ?. by eapply abduct_type_conj_sep_atom_r. Qed.

  Lemma type_conj_case_l b1 b2 φ P1 P2 Q:
    case φ (type_conj b1 false P1 Q) (type_conj b1 false P2 Q) ⊢ type_conj b1 b2 (case φ P1 P2) Q.
  Proof.
    rewrite /case; destruct decide; rewrite /type_conj //.
  Qed.

  Lemma abduct_type_conj_case_l Δ b1 b2 φ P1 P2 Q R:
    abduct Δ (case φ (type_conj b1 false P1 Q) (type_conj b1 false P2 Q)) R →
    abduct Δ (type_conj b1 b2 (case φ P1 P2) Q) R.
  Proof. rewrite -type_conj_case_l //. Qed.

  Global Instance abduct_step_type_conj_case_l Δ b1 b2 φ P1 P2 Q R:
    AbductStep (abduct Δ (case φ (type_conj b1 false P1 Q) (type_conj b1 false P2 Q)) R)
      Δ (type_conj b1 b2 (case φ P1 P2) Q) R | 2.
  Proof. intros ?. by eapply abduct_type_conj_case_l. Qed.

  Lemma type_conj_case_r b1 b2 φ P1 P2 Q:
    case φ (type_conj false b2 Q P1) (type_conj false b2 Q P2) ⊢ type_conj b1 b2 Q (case φ P1 P2).
  Proof.
    rewrite /case; destruct decide; rewrite /type_conj //.
  Qed.

  Lemma abduct_type_conj_case_r Δ b1 b2 φ P1 P2 Q R:
    abduct Δ (case φ (type_conj false b2 Q P1) (type_conj false b2 Q P2)) R →
    abduct Δ (type_conj b1 b2 Q (case φ P1 P2)) R.
  Proof. rewrite -type_conj_case_r //. Qed.

  Global Instance abduct_step_type_conj_case_r Δ b1 b2 φ P1 P2 Q R:
    AbductStep (abduct Δ (case φ (type_conj false b2 Q P1) (type_conj false b2 Q P2)) R)
      Δ (type_conj b1 b2 Q (case φ P1 P2)) R | 2.
  Proof. intros ?. by eapply abduct_type_conj_case_r. Qed.

  Lemma type_conj_and_l b1 b2 P1 P2 Q:
    type_conj b1 false P1 (type_conj b1 b2 P2 Q) ⊢ type_conj b1 b2 (P1 ∧ P2) Q.
  Proof.
    rewrite /type_conj. iIntros "HPQ". iSplit; first iSplit.
    - iDestruct "HPQ" as "($ & _)".
    - iDestruct "HPQ" as "(_ & $ & _)".
    - iDestruct "HPQ" as "(_ & _ & $)".
  Qed.

  Lemma abduct_type_conj_and_l Δ b1 b2 P1 P2 Q R:
    abduct Δ (type_conj b1 false P1 (type_conj b1 b2 P2 Q)) R →
    abduct Δ (type_conj b1 b2 (P1 ∧ P2) Q) R.
  Proof. rewrite -type_conj_and_l //. Qed.

  Global Instance abduct_step_type_conj_and_l Δ b1 b2 P1 P2 Q R:
    AbductStep (abduct Δ (type_conj b1 false P1 (type_conj b1 b2 P2 Q)) R)
      Δ (type_conj b1 b2 (P1 ∧ P2) Q) R | 2.
  Proof. intros ?. by eapply abduct_type_conj_and_l. Qed.


  Lemma abduct_type_conj_abduct_left Δ Δ1 Δ1' P1 Q1 P2 b2 R:
    qenvs_create_persistent_copy Δ Δ1 →
    qenvs_move_assumptions_to_assertions Δ1 Δ1' →
    (clear_universals (abduct Δ1' P1 Q1)) →
    abduct Δ (type_conj true  b2 Q1 P2) R →
    abduct Δ (type_conj false b2 P1 P2) R.
  Proof.
    rewrite /clear_universals /type_conj. intros HΔ1 HΔ1' Habd1 Habd2.
    iIntros "[HΔ HR]". rewrite qenvs_ctx_persistent_copy.
    rewrite -HΔ1 (qenvs_ctx_assumptions_to_assertions Δ1) -HΔ1'.
    iDestruct "HΔ" as "[HΔ1 HΔ]". iDestruct (Habd2 with "[$HΔ $HR]") as "HPQ".
    iSplit.
    - iDestruct "HPQ" as "[Q1 _]". iApply Habd1. iFrame.
    - iDestruct "HPQ" as "[_ $]".
  Qed.

  Global Instance abduct_step_type_conj_abduct_left Δ Δ1 Δ1' P1 Q1 P2 b2 R:
    qenvs_create_persistent_copy Δ Δ1 →
    qenvs_move_assumptions_to_assertions Δ1 Δ1' →
    AbductStep ((clear_universals (abduct Δ1' P1 Q1)) ∧ abduct Δ (type_conj true  b2 Q1 P2) R)
      Δ (type_conj false b2 P1 P2) R | 20.
  Proof. intros ?? [? ?]. by eapply abduct_type_conj_abduct_left. Qed.

  Lemma abduct_type_conj_abduct_right Δ Δ2 Δ2' P1 P2 Q2 b1 R:
    qenvs_create_persistent_copy Δ Δ2 →
    qenvs_move_assumptions_to_assertions Δ2 Δ2' →
    (clear_universals (abduct Δ2' P2 Q2)) →
    abduct Δ (type_conj b1 true P1 Q2) R →
    abduct Δ (type_conj b1 false P1 P2) R.
  Proof.
    rewrite /clear_universals /type_conj. intros HΔ2 HΔ2' Habd1 Habd2.
    iIntros "[HΔ HR]". rewrite qenvs_ctx_persistent_copy.
    rewrite -HΔ2 (qenvs_ctx_assumptions_to_assertions Δ2) -HΔ2'.
    iDestruct "HΔ" as "[HΔ2 HΔ]". iDestruct (Habd2 with "[$HΔ $HR]") as "HPQ".
    iSplit.
    - iDestruct "HPQ" as "[$ _]".
    - iDestruct "HPQ" as "[_ Q2]". iApply Habd1; iFrame.
  Qed.

  Global Instance abduct_step_type_conj_abduct_right Δ Δ2 Δ2' P1 P2 Q2 b1 R:
    qenvs_create_persistent_copy Δ Δ2 →
    qenvs_move_assumptions_to_assertions Δ2 Δ2' →
    AbductStep ((clear_universals (abduct Δ2' P2 Q2)) ∧ abduct Δ (type_conj b1 true P1 Q2) R)
      Δ (type_conj b1 false P1 P2) R | 21.
  Proof. intros ?? [? ?]. by eapply abduct_type_conj_abduct_right. Qed.


  Lemma abduct_and_type_conj Δ Δpers Ω P1 P2 Q1 Q2 R:
    qenvs_sep_assertions Δ Ω →
    qenvs_create_persistent_copy Δ Δpers →
    Simpl (nfw_lin nil Ω P1) Q1 →
    Simpl (nfw_lin nil Ω P2) Q2 →
    abduct Δpers (type_conj false false Q1 Q2) R →
    abduct Δ (P1 ∧ P2) R.
  Proof.
    rewrite /Simpl; intros HΩ Hpers <- <- Habd.
    iIntros "Hctx". rewrite qenvs_ctx_persistent_copy -Hpers.
    iDestruct ("Hctx") as "[[HΔpers HΔ] R]".
    rewrite HΩ. iDestruct (Habd with "[$HΔpers $R]") as "HP".
    rewrite /type_conj. rewrite -!nfw_nfw_lin.
    iSplit; rewrite /nfw.
    - iDestruct "HP" as "[HP1 _]". iApply ("HP1" with "[//] HΔ").
    - iDestruct "HP" as "[_ HP2]". iApply ("HP2" with "[//] HΔ").
  Qed.

  Global Instance abduct_step_and_type_conj `{!fancy_conj} Δ Δpers Ω P1 P2 Q1 Q2 R:
    qenvs_sep_assertions Δ Ω →
    qenvs_create_persistent_copy Δ Δpers →
    Simpl (nfw_lin nil Ω P1) Q1 →
    Simpl (nfw_lin nil Ω P2) Q2 →
    AbductStep (abduct Δpers (type_conj false false Q1 Q2) R)
      Δ (P1 ∧ P2) R | 1.
  Proof. intros ?????. by eapply abduct_and_type_conj. Qed.

  Lemma abduct_type_conj_id Δ b1 b2 P R :
    abduct Δ P R → abduct Δ (type_conj b1 b2 P P) R.
  Proof. rewrite /type_conj. apply abduct_conj_id. Qed.

  Global Instance abduct_step_type_conj_id Δ b1 b2 P R:
    AbductStep (abduct Δ P R)
      Δ (type_conj b1 b2 P P) R | 1.
  Proof. intros ?. by eapply abduct_type_conj_id. Qed.

  Lemma abduct_type_conj_intro Δ P1 P2 R1 R2:
    abduct Δ P1 R1 →
    abduct Δ P2 R2 →
    abduct Δ (type_conj true true P1 P2) (R1 ∧ R2).
  Proof.
    rewrite /type_conj. intros ??. by apply abduct_conj.
  Qed.

  Global Instance abduct_step_type_conj_intro Δ P1 P2 R1 R2:
    AbductStep (abduct Δ P1 R1 ∧ abduct Δ P2 R2)
      Δ (type_conj true true P1 P2) (R1 ∧ R2) | 40.
  Proof. intros [??]. by eapply abduct_type_conj_intro. Qed.


End cases.

Global Typeclasses Opaque type_join type_conj.
