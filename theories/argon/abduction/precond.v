From quiver.argon.base Require Export precond.
From quiver.argon.abduction Require Import abduct proofmode pure assume.
From quiver.argon.simplification Require Import simplify elim_existentials normalize cleanup.

(* projections *)
Inductive projected (X: Type) : ∀ (Y: Type), Y → Prop :=
| projected_id x : projected X X x
| projected_pair_fst Y Z x: projected X (Y * Z) x → projected X Y (x.1)
| projected_pair_snd Y Z x: projected X (Y * Z) x → projected X Z (x.2)
| projected_sig_fst {Y} (Z: Y → Type) x: projected X (sigT Z) x → projected X Y (projT1 x)
| projected_sig_snd {Y} (Z: Y → Type) x: projected X (sigT Z) x → projected X (Z (projT1 x)) (projT2 x).

Existing Class projected.
Global Hint Mode projected - - ! : typeclass_instances.

Notation projection X Y f := (∀ x: X, projected X Y (f x)).

Ltac projected_apply X z :=
  let T := type of z in
  let U := eval simpl in T in
  match U with
  | prod ?A ?B => eapply (projected_pair_fst X A B z)
  | prod ?A ?B => eapply (projected_pair_snd X A B z)
  | sigT ?Z => eapply (projected_sig_fst X Z z)
  | sigT ?Z => eapply (projected_sig_snd X Z z)
  end.


Ltac projected :=
  match goal with
  | |- projected ?X ?Y ?x =>
    let y := eval simpl in x in
    match y with
    | (projT2 ?z) => projected_apply X z; projected
    | (projT1 ?z) => projected_apply X z; projected
    | (?z.2) => projected_apply X z; projected
    | (?z.1) => projected_apply X z; projected
    | _ => unify X Y; eapply (projected_id X y)
    end
  end.


Global Hint Extern 1 (projected _ _ _) => (projected) : typeclass_instances.



Section type_progress.
  Context {Σ: gFunctors}.

  (* progress matches *)
  Lemma abduct_type_progress_match_guaranteed Δ G B M I (R : iProp Σ):
    abduct Δ G R →
    abduct Δ (type_progress_match GUARANTEED G B M I) R.
  Proof. rewrite -type_progress_match_guaranteed //. Qed.

  Lemma abduct_type_progress_match_blocked Δ G B M I (R : iProp Σ):
    abduct Δ B R →
    abduct Δ (type_progress_match BLOCKED G B M I) R.
  Proof. rewrite -type_progress_match_blocked //. Qed.

  Lemma abduct_type_progress_match_maybe Δ G B M n I (R : iProp Σ):
    abduct Δ (M n) R →
    abduct Δ (type_progress_match (MAYBE n) G B M I) R.
  Proof. rewrite -type_progress_match_maybe //. Qed.

  Lemma abduct_type_progress_match_impossible Δ G B M I (R : iProp Σ):
    abduct Δ I R →
    abduct Δ (type_progress_match IMPOSSIBLE G B M I) R.
  Proof. rewrite -type_progress_match_impossible //. Qed.


  (* proofmode instances *)
  Global Instance abduct_step_type_progress_match_guaranteed Δ G B M I (R : iProp Σ):
    AbductStep (abduct Δ G R)
      Δ (type_progress_match GUARANTEED G B M I) R.
  Proof. intros ?; by eapply abduct_type_progress_match_guaranteed. Qed.

  Global Instance abduct_step_type_progress_match_blocked Δ G B M I (R : iProp Σ):
    AbductStep (abduct Δ B R)
      Δ (type_progress_match BLOCKED G B M I) R.
  Proof. intros ?; by eapply abduct_type_progress_match_blocked. Qed.

  Global Instance abduct_step_type_progress_match_maybe Δ G B M n I (R : iProp Σ):
    AbductStep (abduct Δ (M n) R)
      Δ (type_progress_match (MAYBE n) G B M I) R.
  Proof. intros ?; by eapply abduct_type_progress_match_maybe. Qed.

  Global Instance abduct_step_type_progress_match_impossible Δ G B M I (R : iProp Σ):
    AbductStep (abduct Δ I R)
      Δ (type_progress_match IMPOSSIBLE G B M I) R.
  Proof. intros ?; by eapply abduct_type_progress_match_impossible. Qed.

End type_progress.

Section preconditions.
  Context `{!refinedcG Σ}.

  (* Base Cases *)
  Lemma abduct_type_precond_add_emp Δ X pm (P: X → iProp Σ) E M B C  (R : iProp Σ):
    abduct Δ (type_precond X pm (λ x, P x ∗ emp)%I E M B C) R →
    abduct Δ (type_precond X pm P E M B C) R.
  Proof. rewrite type_precond_add_emp //. Qed.

  Global Instance abduct_step_type_precond_add_emp Δ X pm (P: X → iProp Σ) E M B C (R : iProp Σ):
    TCUnless (unify P (λ _, emp%I)) →
    TCUnless (tc_evar (λ P1, tc_evar (λ P2, unify P (λ x, P1 x ∗ P2 x)%I))) →
    AbductStep (abduct Δ (type_precond X pm (λ x, P x ∗ emp)%I E M B C) R)
      Δ (type_precond X pm P E M B C) R | 10000.
  Proof. intros ???; by eapply abduct_type_precond_add_emp. Qed.

  Lemma abduct_type_precond_emp_done X pm Δ C (R : iProp Σ):
    abduct Δ (∃ x: X, default emp (C x)) R →
    abduct Δ (type_precond X pm (λ _, emp)%I (λ _, nil) (λ _, nil) (λ _, nil) C) R.
  Proof. rewrite -type_precond_emp_done //. Qed.

  Global Instance abduct_step_type_precond_emp_done X pm Δ C (R : iProp Σ):
    AbductStep (abduct Δ (∃ x: X, default emp (C x)) R)
      Δ (type_precond X pm (λ _, emp)%I (λ _, nil) (λ _, nil) (λ _, nil) C) R | 1.
  Proof.
    intros ?; by eapply abduct_type_precond_emp_done.
  Qed.

  Lemma abduct_type_precond_emp_evars Δ X pm E1 E2 M B C P (R : iProp Σ):
    (∀! x, Simpl ([∗] (E1 x :: E2 x ++ (M x).*2 ++ B x ++ [default emp (type_precond_yield <$> (C x))]))%I (P x)) →
    abduct Δ (type_precond_evars X (λ x, P x)%I (λ Q, type_precond unit pm (λ _, Q) (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None))) R →
    abduct Δ (type_precond X pm (λ _, emp)%I (λ x, E1 x :: E2 x) M B C) R.
  Proof.
    intros Hsimpl ?; rewrite -type_precond_emp_evars //.
  Qed.

  Global Instance abduct_step_type_precond_emp_evars Δ X pm E1 E2 M B C P (R : iProp Σ):
    (∀! x, Simpl ([∗] (E1 x :: E2 x ++ (M x).*2 ++ B x ++ [default emp (type_precond_yield <$> (C x))]))%I (P x)) →
    AbductStep (abduct Δ (type_precond_evars X (λ x, P x)%I (λ Q, type_precond unit pm (λ _, Q) (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None))) R)
      Δ (type_precond X pm (λ _, emp)%I (λ x, E1 x :: E2 x) M B C) R | 1.
  Proof.
    intros ??; by eapply abduct_type_precond_emp_evars.
  Qed.

  Lemma abduct_type_precond_emp_maybe Δ X pm M1 M2 (B: X → list (iProp Σ)) C (R : iProp Σ):
    abduct Δ (type_maybe_select X pm (λ x, M1 x :: M2 x) B C) R →
    abduct Δ (type_precond X pm (λ _, emp)%I (λ _, nil) (λ x, M1 x :: M2 x) B C) R.
  Proof. rewrite -type_precond_emp_maybe //. Qed.

  Global Instance abduct_step_type_precond_emp_maybe Δ X pm M1 M2 (B: X → list (iProp Σ)) C (R : iProp Σ):
    AbductStep (abduct Δ (type_maybe_select X pm (λ x, M1 x :: M2 x) B C) R)
      Δ (type_precond X pm (λ _, emp)%I (λ _, nil) (λ x, M1 x :: M2 x) B C) R | 1.
  Proof.
    intros ?; by eapply abduct_type_precond_emp_maybe.
  Qed.

  (* we indicrectly trigger the evars resolution *)
  Lemma abduct_type_precond_emp_lift_exists Δ X Y pm B1 B2 (C: X * Y → option (iProp Σ)) (R : iProp Σ):
    abduct Δ (∃ y: Y, type_precond X pm (λ _, emp)%I (λ _, [emp])%I (λ _, nil) (λ x, B1 (x, y) :: B2 (x, y)) (λ x, C (x, y))) R →
    abduct Δ (type_precond (X * Y) pm (λ _, emp)%I (λ _, nil) (λ _, nil) (λ x, B1 x :: B2 x) C) R.
  Proof.
    rewrite -type_precond_emp_lift_exists //.
  Qed.

  Global Instance abduct_step_type_precond_emp_lift_exists Δ X Y pm B1 B2 (C: X * Y → option (iProp Σ)) (R : iProp Σ):
    AbductStep (abduct Δ (∃ y: Y, type_precond X pm (λ _, emp)%I (λ _, [emp])%I (λ _, nil) (λ x, B1 (x, y) :: B2 (x, y)) (λ x, C (x, y))) R)
      Δ (type_precond (X * Y) pm (λ _, emp)%I (λ _, nil) (λ _, nil) (λ x, B1 x :: B2 x) C) R | 1.
  Proof.
    intros ?; by eapply abduct_type_precond_emp_lift_exists.
  Qed.

  Lemma abduct_type_precond_emp_stuck Δ X pm B1 B2 (C: X → option (iProp Σ)) (R : iProp Σ):
    abduct Δ False R →
    abduct Δ (type_precond X pm (λ _, emp)%I (λ _, nil) (λ _, nil) (λ x, B1 x :: B2 x) C) R.
  Proof. rewrite type_precond_emp_stuck //. Qed.

  Global Instance abduct_step_type_precond_emp_stuck Δ X pm B1 B2 (C: X → option (iProp Σ)) (R : iProp Σ):
    AbductStep (abduct Δ False R)
      Δ (type_precond X pm (λ _, emp)%I (λ _, nil) (λ _, nil) (λ x, B1 x :: B2 x) C) R | 2.
  Proof.
    intros ?; by eapply abduct_type_precond_emp_stuck.
  Qed.

  (* separating conjunctions *)
  Lemma abduct_type_precond_sep_assoc Δ X pm (P Q R: X → iProp Σ) E M B C (R' : iProp Σ):
    abduct Δ (type_precond X pm (λ x, P x ∗ (Q x ∗ R x)) E M B C)%I R' →
    abduct Δ (type_precond X pm (λ x, (P x ∗ Q x) ∗ R x) E M B C)%I R'.
  Proof. rewrite type_precond_sep_assoc //. Qed.

  Global Instance abduct_step_type_precond_sep_assoc Δ X pm (P Q R: X → iProp Σ) E M B C (R' : iProp Σ):
    AbductStep (abduct Δ (type_precond X pm (λ x, P x ∗ (Q x ∗ R x)) E M B C)%I R')
      Δ (type_precond X pm (λ x, (P x ∗ Q x) ∗ R x) E M B C)%I R' | 1.
  Proof.
    intros ?; by eapply abduct_type_precond_sep_assoc.
  Qed.

  Lemma abduct_type_precond_sep_exists_pair Δ X pm (Y: Type) (P: X → Y → iProp Σ) Q E M B C (R : iProp Σ):
    abduct Δ (type_precond (X * Y) pm (λ s, P (s.1) (s.2) ∗ Q (s.1)) (λ s, E (s.1)) (λ s, M (s.1)) (λ s, B (s.1)) (λ s, C (s.1)))%I R →
    abduct Δ (type_precond X pm (λ x, (∃ y : Y, P x y) ∗ Q x) E M B C)%I R.
  Proof. rewrite type_precond_sep_exists_pair //. Qed.

  Global Instance abduct_step_type_precond_sep_exists_pair Δ X pm (Y: Type) (P: X → Y → iProp Σ) Q E M B C (R : iProp Σ):
    AbductStep (abduct Δ (type_precond (X * Y) pm (λ s, P (s.1) (s.2) ∗ Q (s.1)) (λ s, E (s.1)) (λ s, M (s.1)) (λ s, B (s.1)) (λ s, C (s.1)))%I R)
      Δ (type_precond X pm (λ x, (∃ y : Y, P x y) ∗ Q x) E M B C)%I R | 1.
  Proof.
    intros ?; by eapply abduct_type_precond_sep_exists_pair.
  Qed.

  Lemma abduct_type_precond_sep_exists Δ X pm (Y: X → Type) (P: ∀ x, Y x → iProp Σ) Q E M B C (R : iProp Σ):
    abduct Δ (type_precond (sigT Y) pm (λ s, P (projT1 s) (projT2 s) ∗ Q (projT1 s)) (λ s, E (projT1 s)) (λ s, M (projT1 s)) (λ s, B (projT1 s)) (λ s, C (projT1 s)))%I R →
    abduct Δ (type_precond X pm (λ x, (∃ y : Y x, P x y) ∗ Q x) E M B C)%I R.
  Proof. rewrite type_precond_sep_exists //. Qed.

  Global Instance abduct_step_type_precond_sep_exists Δ X pm (Y: X → Type) (P: ∀ x, Y x → iProp Σ) Q E M B C (R : iProp Σ):
    AbductStep (abduct Δ (type_precond (sigT Y) pm (λ s, P (projT1 s) (projT2 s) ∗ Q (projT1 s)) (λ s, E (projT1 s)) (λ s, M (projT1 s)) (λ s, B (projT1 s)) (λ s, C (projT1 s)))%I R)
      Δ (type_precond X pm (λ x, (∃ y : Y x, P x y) ∗ Q x) E M B C)%I R | 2.
  Proof.
    intros ?; by eapply abduct_type_precond_sep_exists.
  Qed.

  Lemma abduct_type_precond_sep_yield Δ X pm (P R: X → iProp Σ) E M B (R' : iProp Σ):
    abduct Δ (type_precond X pm R E M B (λ x, Some (P x)))%I R' →
    abduct Δ (type_precond X pm (λ x, (type_precond_yield (P x)) ∗ R x) E M B (λ _, None))%I R'.
  Proof. rewrite type_precond_sep_continuation //. Qed.

  Global Instance abduct_step_type_precond_sep_yield Δ X pm (P R: X → iProp Σ) E M B (R' : iProp Σ):
    AbductStep (abduct Δ (type_precond X pm R E M B (λ x, Some (P x)))%I R')
      Δ (type_precond X pm (λ x, (type_precond_yield (P x)) ∗ R x) E M B (λ _, None))%I R' | 1.
  Proof.
    intros ?; by eapply abduct_type_precond_sep_yield.
  Qed.

  Lemma abduct_type_precond_sep_emp Δ X pm (P : X → iProp Σ) E M B C R:
    abduct Δ (type_precond X pm P E M B C) R →
    abduct Δ (type_precond X pm (λ x, emp ∗ P x)%I E M B C) R.
  Proof. rewrite type_precond_sep_emp //. Qed.

  Global Instance abduct_step_type_precond_sep_emp Δ X pm (P : X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (type_precond X pm P E M B C) R)
      Δ (type_precond X pm (λ x, emp ∗ P x)%I E M B C) R | 1.
  Proof.
    intros ?; by eapply abduct_type_precond_sep_emp.
  Qed.

  (* pure propositions *)
  (* NOTE: make sure that x or y are projections, otherwise precond matching can diverge *)
  Lemma abduct_type_precond_sep_eq Δ X pm (Y: Type) (x y: X → Y) (P: X → iProp Σ) E M B C R:
    TCOr (projection X Y x) (projection X Y y) →
    abduct Δ (type_precond X pm P (λ z, ⌜x z = y z⌝ :: E z)%I M B C) R →
    abduct Δ (type_precond X pm (λ z, ⌜x z = y z⌝ ∗ P z)%I E M B C) R.
  Proof. rewrite type_precond_pure_evars //. Qed.

  Global Instance abduct_step_type_precond_sep_eq Δ X pm (Y: Type) (x y: X → Y) (P: X → iProp Σ) E M B C R:
    TCOr (projection X Y x) (projection X Y y) →
    AbductStep (abduct Δ (type_precond X pm P (λ z, ⌜x z = y z⌝ :: E z)%I M B C) R)
      Δ (type_precond X pm (λ z, ⌜x z = y z⌝ ∗ P z)%I E M B C) R | 1.
  Proof.
    intros ??; by eapply abduct_type_precond_sep_eq.
  Qed.

  Lemma abduct_type_precond_sep_dprop_iff Δ X pm (x y: X → dProp) (P: X → iProp Σ) E M B C R:
    TCOr (projection X dProp x) (projection X dProp y) →
    abduct Δ (type_precond X pm P (λ z, ⌜dprop_iff (x z) (y z)⌝ :: E z)%I M B C) R →
    abduct Δ (type_precond X pm (λ z, ⌜dprop_iff (x z) (y z)⌝ ∗ P z)%I E M B C) R.
  Proof. rewrite type_precond_pure_evars //. Qed.

  Global Instance abduct_step_type_precond_sep_dprop_iff Δ X pm (x y: X → dProp) (P: X → iProp Σ) E M B C R:
    TCOr (projection X dProp x) (projection X dProp y) →
    AbductStep (abduct Δ (type_precond X pm P (λ z, ⌜dprop_iff (x z) (y z)⌝ :: E z)%I M B C) R)
      Δ (type_precond X pm (λ z, ⌜dprop_iff (x z) (y z)⌝ ∗ P z)%I E M B C) R | 1.
  Proof.
    intros ??; by eapply abduct_type_precond_sep_dprop_iff.
  Qed.

  Lemma abduct_type_precond_pure_conj Δ X pm φ ψ (P: X → iProp Σ) E M B C R:
    abduct Δ (type_precond X pm (λ x, ⌜φ x⌝ ∗ (⌜ψ x⌝ ∗ P x))%I E M B C) R →
    abduct Δ (type_precond X pm (λ x, ⌜φ x ∧ ψ x⌝ ∗ P x)%I E M B C) R.
  Proof. intros ?. rewrite -type_precond_pure_conj //. Qed.

  Global Instance abduct_step_type_precond_pure_conj Δ X pm φ ψ (P: X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (type_precond X pm (λ x, ⌜φ x⌝ ∗ (⌜ψ x⌝ ∗ P x))%I E M B C) R)
      Δ (type_precond X pm (λ x, ⌜φ x ∧ ψ x⌝ ∗ P x)%I E M B C) R | 1.
  Proof. intros ?; by eapply abduct_type_precond_pure_conj. Qed.


  Lemma abduct_precond_true Δ X pm φ P E M B C (R: iProp Σ):
    projection X dProp φ →
    abduct Δ (type_precond X pm (λ x, ⌜φ x = True%DP⌝ ∗ P x)%I E M B C) R →
    abduct Δ (type_precond X pm (λ x, ⌜φ x⌝ ∗ P x) E M B C)%I R.
  Proof.
    rewrite /type_precond. iIntros (_ Habd) "Hctx". iDestruct (Habd with "Hctx") as "(%x & [%Hφ HP] & E & M & B & C)".
    iExists x. iFrame. iPureIntro. rewrite Hφ. done.
  Qed.

  Global Instance abduct_step_precond_true Δ X pm φ P E M B C (R: iProp Σ):
    projection X dProp φ →
    AbductStep (abduct Δ (type_precond X pm (λ x, ⌜φ x = True%DP⌝ ∗ P x)%I E M B C) R)
      Δ (type_precond X pm (λ x, ⌜φ x⌝ ∗ P x) E M B C)%I R | 5.
  Proof. intros ??. by eapply abduct_precond_true. Qed.

  Lemma abduct_precond_false Δ X pm φ P E M B C (R: iProp Σ):
    projection X dProp φ →
    abduct Δ (type_precond X pm (λ x, ⌜φ x = False%DP⌝ ∗ P x)%I E M B C) R →
    abduct Δ (type_precond X pm (λ x, ⌜¬ φ x⌝ ∗ P x) E M B C)%I R.
  Proof.
    rewrite /type_precond. iIntros (_ Habd) "Hctx". iDestruct (Habd with "Hctx") as "(%x & [%Hφ HP] & E & M & B & C)".
    iExists x. iFrame. iPureIntro. rewrite Hφ /=. naive_solver.
  Qed.

  Global Instance abduct_step_precond_false Δ X pm φ P E M B C (R: iProp Σ):
    projection X dProp φ →
    AbductStep (abduct Δ (type_precond X pm (λ x, ⌜φ x = False%DP⌝ ∗ P x)%I E M B C) R)
      Δ (type_precond X pm (λ x, ⌜¬ φ x⌝ ∗ P x) E M B C)%I R | 5.
  Proof. intros ??. by eapply abduct_precond_false. Qed.


  Lemma abduct_type_precond_simplify_pure Δ X pm φ ψ ξ (P: X → iProp Σ) E M B C R:
    Δ ⊨ (∀! x, simplify_pure true (φ x) (ψ x)) →
    (∀! x, Simpl (ψ x) (ξ x)) →
    abduct Δ (type_precond X pm (λ x, ⌜ξ x⌝ ∗ P x)%I E M B C) R →
    abduct Δ (type_precond X pm (λ x, ⌜φ x⌝ ∗ P x)%I E M B C) R.
  Proof.
    rewrite /evar_forall. intros Hif Heq ?.
    apply: abduct_qenvs_entails => {}Hif.
    assert (∀ x, simplify_pure true (φ x) (ξ x)).
    { intros x. rewrite /simplify_pure. rewrite -Heq. apply Hif. }
    rewrite -type_precond_impl //.
  Qed.

  Global Instance abduct_step_type_precond_simplify_pure Δ X pm φ ψ ξ (P: X → iProp Σ) E M B C R:
    Δ ⊨ (∀! x, simplify_pure true (φ x) (ψ x)) →
    (∀! x, Simpl (ψ x) (ξ x)) →
    AbductStep (abduct Δ (type_precond X pm (λ x, ⌜ξ x⌝ ∗ P x)%I E M B C) R)
      Δ (type_precond X pm (λ x, ⌜φ x⌝ ∗ P x)%I E M B C) R | 2.
  Proof. intros ???; by eapply abduct_type_precond_simplify_pure. Qed.


  Lemma abduct_type_precond_sep_pure_lift Δ X pm (φ: Prop) (P: X → iProp Σ) E M B C R:
    abduct Δ (⌜φ⌝ ∗ type_precond X pm P E M B C) R →
    abduct Δ (type_precond X pm (λ x, ⌜φ⌝ ∗ P x)%I E M B C) R.
  Proof. rewrite type_precond_sep_pure_lift //. Qed.

  Global Instance abduct_step_type_precond_sep_pure_lift Δ X pm (φ: Prop) (P: X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (⌜φ⌝ ∗ type_precond X pm P E M B C) R)
      Δ (type_precond X pm (λ x, ⌜φ⌝ ∗ P x)%I E M B C) R | 3.
  Proof.
    intros ?; by eapply abduct_type_precond_sep_pure_lift.
  Qed.

  Lemma abduct_type_precond_forall2_cons_r Δ X pm Y Z (us: list Y) x (y: X → list Z) φ (P: X → iProp Σ) E M B C R:
    abduct Δ (∃ u ur, ⌜us = u :: ur⌝ ∗ type_precond X pm (λ z, ⌜Forall2 φ (u :: ur) (x z :: y z)⌝ ∗ P z)%I E M B C) R →
    abduct Δ (type_precond X pm (λ z, ⌜Forall2 φ (us) (x z :: y z)⌝ ∗ P z)%I E M B C) R.
  Proof. intros ?. rewrite -type_precond_forall2_cons_r //. Qed.

  Global Instance abduct_step_type_precond_forall2_cons_r Δ X pm Y Z (us: list Y) x (y: X → list Z) φ (P: X → iProp Σ) E M B C R:
    exi us →
    AbductStep (abduct Δ (∃ u ur, ⌜us = u :: ur⌝ ∗ type_precond X pm (λ z, ⌜Forall2 φ (u :: ur) (x z :: y z)⌝ ∗ P z)%I E M B C) R)
      Δ (type_precond X pm (λ z, ⌜Forall2 φ (us) (x z :: y z)⌝ ∗ P z)%I E M B C) R | 2.
  Proof. intros ??. by eapply abduct_type_precond_forall2_cons_r. Qed.

  Lemma abduct_type_precond_forall2_cons_l Δ X pm Y Z (us: list Y) x (y: X → list Z) φ (P: X → iProp Σ) E M B C R:
    exi us →
    abduct Δ (∃ u ur, ⌜us = u :: ur⌝ ∗ type_precond X pm (λ z,  ⌜Forall2 φ (x z :: y z) (u :: ur)⌝ ∗ P z)%I E M B C) R →
    abduct Δ (type_precond X pm (λ z, ⌜Forall2 φ (x z :: y z) (us)⌝ ∗ P z)%I E M B C) R.
  Proof. intros ?. rewrite -type_precond_forall2_cons_l //. Qed.

  Global Instance abduct_step_type_precond_forall2_cons_l Δ X pm Y Z (us: list Y) x (y: X → list Z) φ (P: X → iProp Σ) E M B C R:
    exi us →
    AbductStep (abduct Δ (∃ u ur, ⌜us = u :: ur⌝ ∗ type_precond X pm (λ z,  ⌜Forall2 φ (x z :: y z) (u :: ur)⌝ ∗ P z)%I E M B C) R)
      Δ (type_precond X pm (λ z, ⌜Forall2 φ (x z :: y z) (us)⌝ ∗ P z)%I E M B C) R | 2.
  Proof. intros ??. by eapply abduct_type_precond_forall2_cons_l. Qed.

  Lemma abduct_type_precond_forall2_cons Δ X pm Y Z x y (xr: X → list Y) (yr: X → list Z) φ (P: X → iProp Σ) E M B C R:
    abduct Δ (type_precond X pm (λ z, ⌜φ (x z) (y z)⌝ ∗ ⌜Forall2 φ (xr z) (yr z)⌝ ∗ P z)%I E M B C) R →
    abduct Δ (type_precond X pm (λ z, ⌜Forall2 φ (x z :: xr z) (y z :: yr z)⌝ ∗ P z)%I E M B C) R.
  Proof. intros ?. rewrite -type_precond_forall2_cons //. Qed.

  Global Instance abduct_step_type_precond_forall2_cons Δ X pm Y Z x y (xr: X → list Y) (yr: X → list Z) φ (P: X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (type_precond X pm (λ z, ⌜φ (x z) (y z)⌝ ∗ ⌜Forall2 φ (xr z) (yr z)⌝ ∗ P z)%I E M B C) R)
      Δ (type_precond X pm (λ z, ⌜Forall2 φ (x z :: xr z) (y z :: yr z)⌝ ∗ P z)%I E M B C) R | 1.
  Proof. intros ?. by eapply abduct_type_precond_forall2_cons. Qed.

  Lemma abduct_type_precond_forall2_nil_l Δ X pm Y Z (us: X → list Y) φ (P: X → iProp Σ) E M B C R:
    abduct Δ (type_precond X pm (λ z, ⌜us z = nil⌝ ∗ P z)%I E M B C) R →
    abduct Δ (type_precond X pm (λ z, ⌜Forall2 φ (us z) (nil: list Z)⌝ ∗ P z)%I E M B C) R.
  Proof. intros ?. rewrite -type_precond_forall2_nil_l //. Qed.

  Global Instance abduct_step_type_precond_forall2_nil_l Δ X pm Y Z (us: X → list Y) φ (P: X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (type_precond X pm (λ z, ⌜us z = nil⌝ ∗ P z)%I E M B C) R)
      Δ (type_precond X pm (λ z, ⌜Forall2 φ (us z) (nil: list Z)⌝ ∗ P z)%I E M B C) R | 1.
  Proof. intros ?. by eapply abduct_type_precond_forall2_nil_l. Qed.

  Lemma abduct_type_precond_forall2_nil_r Δ X pm Y Z (us: X → list Y) φ (P: X → iProp Σ) E M B C R:
    abduct Δ (type_precond X pm (λ z, ⌜us z = nil⌝ ∗ P z)%I E M B C) R →
    abduct Δ (type_precond X pm (λ z, ⌜Forall2 φ (nil: list Z) (us z)⌝ ∗ P z)%I E M B C) R.
  Proof. intros ?. rewrite -type_precond_forall2_nil_r //. Qed.

  Global Instance abduct_step_type_precond_forall2_nil_r Δ X pm Y Z (us: X → list Y) φ (P: X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (type_precond X pm (λ z, ⌜us z = nil⌝ ∗ P z)%I E M B C) R)
      Δ (type_precond X pm (λ z, ⌜Forall2 φ (nil: list Z) (us z)⌝ ∗ P z)%I E M B C) R | 1.
  Proof. intros ?. by eapply abduct_type_precond_forall2_nil_r. Qed.


  Lemma abduct_type_precond_sep_pure_blocked Δ X pm (φ: X → Prop) (P: X → iProp Σ) E M B C R:
    abduct Δ (type_precond X pm P E M (λ x, ⌜φ x⌝ :: B x)%I C) R →
    abduct Δ (type_precond X pm (λ x, ⌜φ x⌝ ∗ P x)%I E M B C) R.
  Proof. rewrite type_precond_sep_pure_blocked //. Qed.

  Global Instance abduct_step_type_precond_sep_pure_blocked Δ X pm (φ: X → Prop) (P: X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (type_precond X pm P E M (λ x, ⌜φ x⌝ :: B x)%I C) R)
      Δ (type_precond X pm (λ x, ⌜φ x⌝ ∗ P x)%I E M B C) R | 10.
  Proof.
    intros ?; by eapply abduct_type_precond_sep_pure_blocked.
  Qed.

  Lemma abduct_type_precond_sep_predicate Δ X pm Q E M B C {In Out} (p: In → Out → iProp Σ) (i: In) (o: X → Out) R :
    Predicate In Out p →
    abduct Δ (abd_pred p i (λ o', type_precond X pm (λ x, ⌜o' = o x⌝ ∗ Q x)%I E M B C)) R →
    abduct Δ (type_precond X pm (λ x, p i (o x) ∗ Q x)%I E M B C) R.
  Proof.
    intros ? Habd. rewrite -type_precond_sep_predicate //.
  Qed.

  Global Instance abduct_step_type_precond_sep_predicate Δ X pm Q E M B C I O (p: I → O → iProp Σ) (i: I) (o: X → O) R :
    Predicate I O p →
    AbductStep (abduct Δ (abd_pred p i (λ o', type_precond X pm (λ x, ⌜o' = o x⌝ ∗ Q x)%I E M B C)) R)
      Δ (type_precond X pm (λ x, p i (o x) ∗ Q x)%I E M B C) R | 3.
  Proof.
    intros ??; by eapply abduct_type_precond_sep_predicate.
  Qed.

  Lemma abduct_type_precond_sep_predicate_dependent Δ X pm Q E M B C I O (p: I → O → iProp Σ) (i: X → I) (o: X → O) R :
    Predicate I O p →
    abduct Δ (type_precond X pm Q E M (λ x, p (i x) (o x) :: B x) C) R →
    abduct Δ (type_precond X pm (λ x, p (i x) (o x) ∗ Q x)%I E M B C) R.
  Proof.
    intros ??. rewrite -type_precond_sep_predicate_dependent //.
  Qed.

  Global Instance abduct_step_type_precond_sep_predicate_dependent Δ X pm Q E M B C I O (p: I → O → iProp Σ) (i: X → I) (o: X → O) R :
    Predicate I O p →
    AbductStep (abduct Δ (type_precond X pm Q E M (λ x, p (i x) (o x) :: B x) C) R)
      Δ (type_precond X pm (λ x, p (i x) (o x) ∗ Q x)%I E M B C) R | 30.
  Proof.
    intros ??; by eapply abduct_type_precond_sep_predicate_dependent.
  Qed.

  (* NFS *)
  Lemma abduct_type_precond_nfs Δ X pm Γ Ω (P: X → iProp Σ) N Q E M B C R:
    (∀! x, Simpl (nfs_lin (Γ x) (Ω x) (P x)) (N x)) →
    abduct Δ (type_precond X pm (λ x, N x ∗ Q x)%I E M B C) R →
    abduct Δ (type_precond X pm (λ x, nfs (Γ x) (Ω x) (P x) ∗ Q x)%I E M B C) R.
  Proof.
    rewrite /Simpl /evar_forall. intros Hsimpl Habd. rewrite -type_precond_sep_nfs.
    rewrite /type_precond. setoid_rewrite Hsimpl. done.
  Qed.

  Global Instance abduct_step_type_precond_nfs Δ X pm Γ Ω (P: X → iProp Σ) N Q E M B C R:
    (∀! x, Simpl (nfs_lin (Γ x) (Ω x) (P x)) (N x)) →
    AbductStep (abduct Δ (type_precond X pm (λ x, N x ∗ Q x)%I E M B C) R)
      Δ (type_precond X pm (λ x, nfs (Γ x) (Ω x) (P x) ∗ Q x)%I E M B C) R.
  Proof.
    intros ??. by eapply abduct_type_precond_nfs.
  Qed.

  (* EXISTS N *)
  Lemma abduct_type_precond_existsN_zero Δ X pm Y (P: X → list Y → iProp Σ) Q E M B C R :
    abduct Δ (type_precond X pm (λ x, P x nil ∗ Q x) E M B C)%I R →
    abduct Δ (type_precond X pm (λ x, existsN 0%nat (P x) ∗ Q x) E M B C)%I R.
  Proof. rewrite -type_precond_existsN_zero //. Qed.

  Global Instance abduct_step_type_precond_existsN_zero Δ X pm Y (P: X → list Y → iProp Σ) Q E M B C R :
    AbductStep (abduct Δ (type_precond X pm (λ x, P x nil ∗ Q x) E M B C)%I R)
      Δ (type_precond X pm (λ x, existsN 0%nat (P x) ∗ Q x) E M B C)%I R.
  Proof. intros ?. by eapply abduct_type_precond_existsN_zero. Qed.

  Lemma abduct_type_precond_existsN_succ Δ X pm Y n (P: X → list Y → iProp Σ) Q E M B C R :
    abduct Δ (type_precond X pm (λ x, ∃ y, existsN n (λ ys, P x (y :: ys)) ∗ Q x) E M B C)%I R →
    abduct Δ (type_precond X pm (λ x, existsN (S n) (P x) ∗ Q x) E M B C)%I R.
  Proof. rewrite -type_precond_existsN_succ //. Qed.

  Global Instance abduct_step_type_precond_existsN_succ Δ X pm Y n (P: X → list Y → iProp Σ) Q E M B C R :
    AbductStep (abduct Δ (type_precond X pm (λ x, ∃ y, existsN n (λ ys, P x (y :: ys)) ∗ Q x) E M B C)%I R)
      Δ (type_precond X pm (λ x, existsN (S n) (P x) ∗ Q x) E M B C)%I R.
  Proof. intros ?. by eapply abduct_type_precond_existsN_succ. Qed.

  (* SIMPL *)
  Lemma abduct_type_precond_simpl Δ X pm Y (S S': X → Y) Φ (P : X → iProp Σ) E M B C R:
    (∀! x, Simpl (S x) (S' x)) →
    abduct Δ (type_precond X pm (λ x, (Φ x) (S' x) ∗ P x) E M B C)%I R →
    abduct Δ (type_precond X pm (λ x, abd_simpl (S x) (Φ x) ∗ P x) E M B C)%I R.
  Proof.
    rewrite /Simpl /evar_forall. intros Hsimpl Habd.
    rewrite /type_precond. rewrite /type_precond /abd_simpl.
    setoid_rewrite Hsimpl. done.
  Qed.

  Global Instance abduct_step_type_precond_simpl Δ X pm Y (S S': X → Y) Φ (P : X → iProp Σ) E M B C R:
    (∀! x, Simpl (S x) (S' x)) →
    AbductStep (abduct Δ (type_precond X pm (λ x, (Φ x) (S' x) ∗ P x) E M B C)%I R)
      Δ (type_precond X pm (λ x, abd_simpl (S x) (Φ x) ∗ P x) E M B C)%I R.
  Proof. intros ??. by eapply abduct_type_precond_simpl. Qed.

  (* EVARS *)
  Lemma abduct_type_precond_evars_simplify Δ X (P: X → iProp Σ) Φ Q R S T R':
    sequence_classes [
      normalize (∃ x, P x) Q;
      elim_existentials Q R;
      cleanup R S;
      Simpl S T
    ] →
    abduct Δ (Φ T) R' →
    abduct Δ (type_precond_evars X P Φ) R'.
  Proof.
    intros Hent ?. rewrite -type_precond_evars_ent //.
    rewrite /sequence_classes !Forall_cons /Forall_nil /id /Simpl in Hent.
    destruct Hent as (Hnorm%ent_simplify & Helim%ent_simplify & Hclean%ent_simplify & -> & _).
    rewrite Hclean Helim Hnorm //.
  Qed.

  Global Instance abduct_step_type_precond_evars_simplify Δ X (P: X → iProp Σ) Φ Q R S T R' :
    sequence_classes [
      normalize (∃ x, P x) Q;
      elim_existentials Q R;
      cleanup R S;
      Simpl S T
    ] →
    AbductStep (abduct Δ (Φ T) R')
      Δ (type_precond_evars X P Φ) R' | 30.
  Proof.
    intros ??; by eapply abduct_type_precond_evars_simplify.
  Qed.

  (* TYPE PRE *)
  Lemma abduct_type_pre Δ T pm P Φ (R: iProp Σ):
    abduct Δ (
      abd_breakpoint
        (λ Ψ, abd_simpl (∃t, P t ∗ type_precond_yield (Ψ t))%I (λ Q, type_precond unit pm (λ _, Q) (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)))
        (λ P, abd_evars P (λ P', P' Φ))
    ) R →
    abduct Δ (type_pre_post T pm P Φ) R.
  Proof.
    rewrite /abd_simpl /type_precond_yield /abd_evars /type_precond /abd_breakpoint /=.
    rewrite /type_pre_post.
    intros Habd. iIntros "Hctx". iDestruct (Habd with "Hctx") as "HT".
    iDestruct ("HT") as "[%Ψ [HΨ HΦ]]".
    iDestruct ("HΦ") as "[%Ξ [HM HΞ]]".
    iDestruct ("HM" with "HΞ") as "HΨ'".
    by iDestruct ("HΨ" $! Φ with "HΨ'") as "[%_ [HΨ _]]".
  Qed.

  Global Instance abduct_step_type_pre Δ T pm P Φ (R: iProp Σ):
    AbductStep
    (abduct Δ (
        abd_breakpoint
          (λ Ψ, abd_simpl (∃ t, P t ∗ type_precond_yield (Ψ t))%I (λ Q, type_precond unit pm (λ _, Q) (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)))
          (λ P, abd_evars P (λ P', P' Φ))
      ) R) Δ (type_pre_post T pm P Φ) R.
  Proof. intros ?. by apply abduct_type_pre. Qed.

  (* CONJUNCTION AND CASES *)
  Lemma abduct_type_precond_conj_sep_l Δ X pm P (Q : X → iProp Σ) R:
    abduct Δ (type_precond X pm (λ x, P)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None) ∧ type_precond X pm (λ x, Q x ∗ emp)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)) R →
    abduct Δ (type_precond X pm (λ x, (P ∧ Q x) ∗ emp)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)) R.
  Proof. rewrite -type_precond_conj_sep_l //. Qed.

  Global Instance abduct_step_type_precond_conj_sep_l Δ X pm P (Q : X → iProp Σ) R:
    AbductStep (abduct Δ (type_precond X pm (λ x, P)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None) ∧ type_precond X pm (λ x, Q x ∗ emp)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)) R)
      Δ (type_precond X pm (λ x, (P ∧ Q x) ∗ emp)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)) R | 1.
  Proof. intros ?. by eapply abduct_type_precond_conj_sep_l. Qed.

  Lemma abduct_type_precond_conj_sep_r Δ X pm (P : X → iProp Σ) Q R:
    abduct Δ (type_precond X pm (λ x, P x ∗ emp)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None) ∧ type_precond X pm (λ _, Q)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)) R →
    abduct Δ (type_precond X pm (λ x, (P x ∧ Q) ∗ emp)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)) R.
  Proof. rewrite -type_precond_conj_sep_r //. Qed.

  Global Instance abduct_step_type_precond_conj_sep_r Δ X pm (P : X → iProp Σ) Q R:
    AbductStep (abduct Δ (type_precond X pm (λ x, P x ∗ emp)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None) ∧ type_precond X pm (λ _, Q)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)) R)
      Δ (type_precond X pm (λ x, (P x ∧ Q) ∗ emp)%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)) R | 1.
  Proof. intros ?. by eapply abduct_type_precond_conj_sep_r. Qed.

  Lemma abduct_type_precond_conj_blocked Δ X pm (P Q R' : X → iProp Σ) E M B C R:
    abduct Δ (type_precond X pm (λ x, R' x) E M (λ x, (P x ∧ Q x)%I :: B x) C) R →
    abduct Δ (type_precond X pm (λ x, (P x ∧ Q x) ∗ R' x)%I E M B C) R.
  Proof. rewrite -type_precond_conj_blocked //. Qed.

  Global Instance abduct_step_type_precond_conj_blocked Δ X pm (P Q R' : X → iProp Σ) E M B C R:
    AbductStep (abduct Δ (type_precond X pm (λ x, R' x) E M (λ x, (P x ∧ Q x)%I :: B x) C) R)
      Δ (type_precond X pm (λ x, (P x ∧ Q x) ∗ R' x)%I E M B C) R | 10.
  Proof. intros ?. by eapply abduct_type_precond_conj_blocked. Qed.

  Lemma abduct_type_precond_sep_case Δ X pm φ (P Q R: X → iProp Σ) E M B C R':
    abduct Δ (case φ (type_precond X pm (λ x, P x ∗ R x)%I E M B C) (type_precond X pm (λ x, (Q x) ∗ R x)%I E M B C)) R' →
    abduct Δ (type_precond X pm (λ x, case φ (P x) (Q x) ∗ R x) E M B C)%I R'.
  Proof. rewrite -type_precond_sep_case //. Qed.

  Global Instance abduct_step_type_precond_sep_case Δ X pm φ (P Q R: X → iProp Σ) E M B C R':
    AbductStep (abduct Δ (case φ (type_precond X pm (λ x, P x ∗ R x)%I E M B C) (type_precond X pm (λ x, (Q x) ∗ R x)%I E M B C)) R')
      Δ (type_precond X pm (λ x, case φ (P x) (Q x) ∗ R x) E M B C)%I R' | 2.
  Proof. intros ?. by eapply abduct_type_precond_sep_case. Qed.

  Lemma abduct_type_precond_sep_case_blocked Δ X pm φ (P Q R: X → iProp Σ) E M B C R':
    abduct Δ (type_precond X pm R E M (λ x, case (φ x) (P x) (Q x) :: B x) C) R' →
    abduct Δ (type_precond X pm (λ x, case (φ x) (P x) (Q x) ∗ R x) E M B C)%I R'.
  Proof. rewrite -type_precond_sep_case_blocked //. Qed.

  Global Instance abduct_step_type_precond_sep_case_blocked Δ X pm φ (P Q R: X → iProp Σ) E M B C R':
    AbductStep (abduct Δ (type_precond X pm R E M (λ x, case (φ x) (P x) (Q x) :: B x) C) R')
      Δ (type_precond X pm (λ x, case (φ x) (P x) (Q x) ∗ R x) E M B C)%I R' | 10.
  Proof. intros ?. by eapply abduct_type_precond_sep_case_blocked. Qed.


    (* MAYBE SELECT *)
  Lemma abduct_type_maybe_select_first Δ (X: Type) pm (M: X → iProp Σ) n Ms B C R:
    abduct Δ (type_maybe X pm (λ x, M x) (λ Q, type_precond X pm Q (λ _, nil) Ms B C)) R →
    abduct Δ (type_maybe_select X pm (λ x, (n, M x) :: Ms x) B C) R.
  Proof.
    intros. rewrite -type_maybe_select_first //.
  Qed.

  Global Instance abduct_step_type_maybe_select_first Δ (X: Type) pm  (M: X → iProp Σ) n Ms B C R:
    AbductStep (abduct Δ (type_maybe X pm (λ x, M x) (λ Q, type_precond X pm Q (λ _, nil) Ms B C)) R)
      Δ (type_maybe_select X pm (λ x, (n, M x) :: Ms x) B C) R | 1.
  Proof. intros ?. by eapply abduct_type_maybe_select_first. Qed.

  Lemma abduct_type_maybe_select_skip Δ (X: Type) pm (M: X → iProp Σ) n Ms B C R:
    abduct Δ (type_maybe_select X pm Ms (λ x, M x :: B x) C) R →
    abduct Δ (type_maybe_select X pm (λ x, (n, M x) :: Ms x) B C) R.
  Proof.
    intros. rewrite -type_maybe_select_skip //.
  Qed.

  (* NOTE: this instance will never be used, but it could be used in the future for backtracking *)
  Global Instance abduct_step_type_maybe_select_skip Δ (X: Type) pm (M: X → iProp Σ) n Ms B C R:
    AbductStep (abduct Δ (type_maybe_select X pm Ms (λ x, M x :: B x) C) R)
      Δ (type_maybe_select X pm (λ x, (n, M x) :: Ms x) B C) R | 2.
  Proof. intros ?. by eapply abduct_type_maybe_select_skip. Qed.

  Lemma abduct_type_maybe_select_impossible Δ (X: Type) pm B C (R: iProp Σ):
    abduct Δ False R →
    abduct Δ (type_maybe_select X pm (λ _, []) B C) R.
  Proof.
    intros. rewrite -type_maybe_select_impossible //.
  Qed.

  Global Instance abduct_step_type_maybe_select_impossible Δ (X: Type) pm B C (R: iProp Σ):
    AbductStep (abduct Δ False R)
      Δ (type_maybe_select X pm (λ _, []) B C) R | 3.
  Proof. intros ?. by eapply abduct_type_maybe_select_impossible. Qed.

End preconditions.
