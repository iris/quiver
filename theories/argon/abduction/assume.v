From quiver.argon.base Require Export judgments.
From quiver.argon.abduction Require Import abduct proofmode.

Definition contains_universal {X: Type} (x: X) :=
  sealed (∃ y, x = y).
Existing Class contains_universal.
Global Hint Mode contains_universal - ! : typeclass_instances.

Lemma contains_universal_intro {X: Type} (x: X): contains_universal x.
Proof. rewrite /contains_universal sealed_eq. by exists x. Qed.

Ltac contains_universal :=
  match goal with
  | H: uni ?x |- contains_universal ?A =>
    match A with
    | context [x] => exact (contains_universal_intro A)
    end
  end.

Global Hint Extern 2 (contains_universal _) => (contains_universal) : typeclass_instances.

Section assume.

  Context {Σ: gFunctors}.

  Lemma abduct_abd_assume_exists Δ m {A} (P: A → iProp Σ) Q R:
    abduct Δ  (∀ x, abd_assume m (P x) Q) R →
    abduct Δ (abd_assume m (∃ x, P x) Q) R.
  Proof. rewrite -abd_assume_exists //. Qed.

  Lemma abduct_abd_assume_sep Δ m (P1 P2 Q R: iProp Σ) :
    abduct Δ (abd_assume m P1 (abd_assume m P2 Q)) R →
    abduct Δ (abd_assume m (P1 ∗ P2) Q) R.
  Proof. rewrite -abd_assume_sep //. Qed.

  Lemma abduct_abd_assume_big_sep_nil Δ m {X:Type} (P: X → iProp Σ) (Q R: iProp Σ):
    abduct Δ Q R →
    abduct Δ (abd_assume m ([∗ list] x ∈ nil, P x) Q) R.
  Proof. rewrite -abd_assume_drop //. Qed.

  Lemma abduct_abd_assume_big_sep2_nil Δ m {X Y:Type} (P: X → Y → iProp Σ) (Q R: iProp Σ):
    abduct Δ Q R →
    abduct Δ (abd_assume m ([∗ list] x; y ∈ nil; nil, P x y) Q) R.
  Proof. rewrite -abd_assume_drop //. Qed.

  Lemma abduct_abd_assume_big_sep_cons Δ m {X: Type} P' (P: X → iProp Σ) x xr Q R:
    Simpl (P x) P' →
    abduct Δ (abd_assume m P' (abd_assume m ([∗ list] x ∈ xr, P x) Q)) R →
    abduct Δ (abd_assume m ([∗ list] x ∈ x :: xr, P x) Q) R.
  Proof. intros <-. rewrite -abd_assume_sep //. Qed.

  Lemma abduct_abd_assume_big_sep2_cons Δ m {X Y: Type} P' (P: X → Y → iProp Σ) x xr y yr Q R:
    Simpl (P x y) P' →
    abduct Δ (abd_assume m P' (abd_assume m ([∗ list] x; y ∈ xr; yr, P x y) Q)) R →
    abduct Δ (abd_assume m ([∗ list] x; y ∈ x :: xr; y :: yr, P x y) Q) R.
  Proof. intros <-. rewrite -abd_assume_sep //. Qed.

  Lemma abduct_abd_assume_true m Δ (Q R: iProp Σ):
    abduct Δ Q R →
    abduct Δ (abd_assume m True Q) R.
  Proof. rewrite -abd_assume_drop //. Qed.

  Lemma abduct_abd_assume_false Δ m (P: iProp Σ) R:
    unify R True%I →
    abduct Δ (abd_assume m False P) R.
  Proof.
    intros ->. rewrite -abd_assume_false. by apply abduct_true.
  Qed.

  Lemma abduct_abd_assume_emp Δ m (Q R: iProp Σ):
    abduct Δ Q R →
    abduct Δ (abd_assume m emp%I Q) R.
  Proof. rewrite -abd_assume_drop //. Qed.

  Lemma abduct_abd_assume_predicate Δ m I O P i o Q (R: iProp Σ):
    Predicate I O P →
    abduct Δ (P i o -∗ Q) R →
    abduct Δ (abd_assume m (P i o) Q) R.
  Proof. rewrite /abd_assume. done. Qed.

  Lemma abduct_abd_assume_atom Δ m A `{!Atom A} (P: iProp Σ) R:
    abduct Δ (A -∗ P) R →
    abduct Δ (abd_assume m A P) R.
  Proof. rewrite /abd_assume. done. Qed.

  Lemma abduct_abd_assume_pers Δ Δ' m (P Q R: iProp Σ):
    qenvs_add_pers_assertion Δ P Δ' →
    abduct Δ' Q R →
    abduct Δ (abd_assume m (□ P) Q) R.
  Proof.
    rewrite /abd_assume. iIntros (Hadd Habd) "Hctx HP".
    iApply Habd. iFrame. rewrite Hadd. iFrame.
  Qed.

  (* type assume pure *)
  Lemma abduct_abd_assume_context_assume Δ (φ: Prop) (Q R: iProp Σ):
    contains_universal φ →
    abduct Δ (abd_assume ASSUME ⌜φ⌝ Q) R →
    abduct Δ (abd_assume CTX ⌜φ⌝ Q) R.
  Proof. intros ??. done. Qed.

  Global Instance abduct_step_abd_assume_context_assume Δ (φ: Prop) (Q R: iProp Σ):
    contains_universal φ →
    AbductStep (abduct Δ (abd_assume ASSUME ⌜φ⌝ Q) R)
      Δ (abd_assume CTX ⌜φ⌝ Q) R | 4.
  Proof. intros ??. by eapply abduct_abd_assume_context_assume. Qed.

  Lemma abduct_abd_assume_context_assert Δ (φ: Prop) (Q R: iProp Σ):
    abduct Δ (abd_assume ASSERT ⌜φ⌝ Q) R →
    abduct Δ (abd_assume CTX ⌜φ⌝ Q) R.
  Proof. intros ?. done. Qed.

  Global Instance abduct_step_abd_assume_context_assert Δ (φ: Prop) (Q R: iProp Σ):
    AbductStep (abduct Δ (abd_assume ASSERT ⌜φ⌝ Q) R)
      Δ (abd_assume CTX ⌜φ⌝ Q) R | 5.
  Proof. intros ?. by eapply abduct_abd_assume_context_assert. Qed.

  Lemma abduct_abd_assume_recycle Δ φ (Q R: iProp Σ):
    abduct Δ (abd_assume CTX ⌜φ⌝ Q) R →
    abduct Δ (abd_assume RECYCLE ⌜φ⌝ Q) R.
  Proof. intros ?. done. Qed.

  Global Instance abduct_step_abd_assume_recycle Δ φ (Q R: iProp Σ):
    AbductStep (abduct Δ (abd_assume CTX ⌜φ⌝ Q) R)
      Δ (abd_assume RECYCLE ⌜φ⌝ Q) R | 4.
  Proof. intros ?. by eapply abduct_abd_assume_recycle. Qed.

  Lemma abduct_abd_assume_known Δ Δ' m (φ: Prop) (Q R: iProp Σ):
    (* we are only guaranteed that the persistent part stays around, the rest may move *)
    qenvs_create_persistent_copy Δ Δ' →
    Δ' ⊨ ctx_hyp φ →
    abduct Δ Q R →
    abduct Δ (abd_assume m ⌜φ⌝ Q) R.
  Proof. rewrite -abd_assume_drop //. Qed.

  Global Instance abduct_step_abd_assume_known Δ Δ' m (φ: Prop) (Q R: iProp Σ):
    qenvs_create_persistent_copy Δ Δ' →
    Δ' ⊨ ctx_hyp φ →
    AbductStep (abduct Δ Q R) Δ (abd_assume m ⌜φ⌝ Q) R | 10.
  Proof.
    intros ???. by eapply abduct_abd_assume_known.
  Qed.

  Lemma abduct_abd_assume_add_assumption Δ Δ' m (φ: Prop) (Q R: iProp Σ):
    qenvs_add_pure_assumption Δ φ Δ' →
    abduct Δ' (abd_learn_simpl φ Q) R →
    abduct Δ (abd_assume m ⌜φ⌝ Q) R.
  Proof.
    rewrite /qenvs_add_pure_assumption.
    intros Hadd Habd. rewrite /abduct /abd_assume /abd_learn_simpl.
    iIntros "[HΔ HR] #Hφ". iCombine "Hφ HΔ" as "HΔ". rewrite -Hadd.
    iDestruct (Habd with "[$HΔ $HR]") as "HQ". rewrite /abd_learn_simpl.
    by iApply "HQ".
  Qed.

  Lemma abduct_abd_assume_add_assertion Δ Δ' m (φ: Prop) (Q R: iProp Σ):
    qenvs_add_pure_assertion Δ φ Δ' →
    abduct Δ' (abd_learn_simpl φ Q) R →
    abduct Δ (abd_assume m ⌜φ⌝ Q) R.
  Proof.
    rewrite /qenvs_add_pure_assertion.
    intros Hadd Habd. rewrite /abduct /abd_assume /abd_learn_simpl.
    iIntros "[HΔ HR] #Hφ". iCombine "Hφ HΔ" as "HΔ". rewrite -Hadd.
    iDestruct (Habd with "[$HΔ $HR]") as "HQ". rewrite /abd_learn_simpl.
    by iApply "HQ".
  Qed.

  Lemma abduct_abd_assume_assumption Δ Δ' (φ: Prop) (Q R: iProp Σ):
    qenvs_add_pure_assumption Δ φ Δ' →
    abduct Δ' (abd_learn_simpl φ Q) R →
    abduct Δ (abd_assume ASSUME ⌜φ⌝ Q) R.
  Proof. intros ??. by eapply abduct_abd_assume_add_assumption. Qed.

  Global Instance abduct_step_abd_assume_assumption Δ Δ' (φ: Prop) (Q R: iProp Σ):
    qenvs_add_pure_assumption Δ φ Δ' →
    AbductStep (abduct Δ' (abd_learn_simpl φ Q) R) Δ (abd_assume ASSUME ⌜φ⌝ Q) R | 11.
  Proof. intros ??. by eapply abduct_abd_assume_assumption. Qed.

  Lemma abduct_abd_assume_assertion Δ Δ' (φ: Prop) (Q R: iProp Σ):
    qenvs_add_pure_assertion Δ φ Δ' →
    abduct Δ' (abd_learn_simpl φ Q) R →
    abduct Δ (abd_assume ASSERT ⌜φ⌝ Q) R.
  Proof. intros ??. by eapply abduct_abd_assume_add_assertion. Qed.

  Global Instance abduct_step_abd_assume_assertion Δ Δ' (φ: Prop) (Q R: iProp Σ):
    qenvs_add_pure_assertion Δ φ Δ' →
    AbductStep (abduct Δ' (abd_learn_simpl φ Q) R) Δ (abd_assume ASSERT ⌜φ⌝ Q) R | 11.
  Proof. intros ??. by eapply abduct_abd_assume_assertion. Qed.

End assume.



Section assume_instances.
  Context `{!refinedcG Σ}.


  Global Instance abduct_step_assume_exists Δ m {A} (P: A → iProp Σ) Q R:
    AbductStep (abduct Δ (∀ x, abd_assume m (P x) Q) R)
      Δ (abd_assume m (∃ x, P x) Q) R.
  Proof. intros ?. by eapply abduct_abd_assume_exists. Qed.

  Global Instance abduct_step_assume_sep Δ m (P1 P2 Q R: iProp Σ) :
    AbductStep (abduct Δ (abd_assume m P1 (abd_assume m P2 Q)) R)
      Δ (abd_assume m (P1 ∗ P2) Q) R.
  Proof. intros ?. by eapply abduct_abd_assume_sep. Qed.

  Global Instance abduct_step_assume_true Δ m (Q R: iProp Σ):
    AbductStep (abduct Δ Q R)
      Δ (abd_assume m True Q) R | 1.
  Proof. intros ?. by eapply abduct_abd_assume_true. Qed.

  Global Instance abduct_step_abd_assume_false Δ m (P: iProp Σ) R:
    unify R True%I →
    AbductStep True Δ (abd_assume m False P) R | 1.
  Proof. intros ??. by eapply abduct_abd_assume_false. Qed.

  Global Instance abduct_step_assume_emp Δ m (Q R: iProp Σ):
    AbductStep (abduct Δ Q R)
      Δ (abd_assume m emp%I Q) R.
  Proof. intros ?. by eapply abduct_abd_assume_emp. Qed.

  Global Instance abduct_step_abd_assume_pers Δ Δ' m (P Q R: iProp Σ):
    qenvs_add_pers_assertion Δ P Δ' →
    AbductStep (abduct Δ' Q R)
      Δ (abd_assume m (□ P) Q) R | 1.
  Proof. intros ??. by eapply abduct_abd_assume_pers. Qed.

  Global Instance abduct_step_assume_big_sep_nil Δ m {X:Type} (P: X → iProp Σ) (Q R: iProp Σ):
    AbductStep (abduct Δ Q R)
      Δ (abd_assume m ([∗ list] x ∈ nil, P x) Q) R.
  Proof. intros ?. by eapply abduct_abd_assume_big_sep_nil. Qed.

  Global Instance abduct_step_assume_big_sep2_nil Δ m {X Y:Type} (P: X → Y → iProp Σ) (Q R: iProp Σ):
    AbductStep (abduct Δ Q R)
      Δ (abd_assume m ([∗ list] x; y ∈ nil; nil, P x y) Q) R.
  Proof. intros ?. by eapply abduct_abd_assume_big_sep2_nil. Qed.

  Global Instance abduct_step_assume_big_sep_cons Δ m {X: Type} P' (P: X → iProp Σ) x xr Q R:
    Simpl (P x) P' →
    AbductStep (abduct Δ (abd_assume m P' (abd_assume m ([∗ list] x ∈ xr, P x) Q)) R)
      Δ (abd_assume m ([∗ list] x ∈ x :: xr, P x) Q) R.
  Proof. intros ??. by eapply abduct_abd_assume_big_sep_cons. Qed.

  Global Instance abduct_step_assume_big_sep2_cons Δ m {X Y: Type} P' (P: X → Y → iProp Σ) x xr y yr Q R:
    Simpl (P x y) P' →
    AbductStep (abduct Δ (abd_assume m P' (abd_assume m ([∗ list] x; y ∈ xr; yr, P x y) Q)) R)
      Δ (abd_assume m ([∗ list] x; y ∈ x :: xr; y :: yr, P x y) Q) R.
  Proof. intros ??. by eapply abduct_abd_assume_big_sep2_cons. Qed.

  Global Instance abduct_step_abd_assume_predicate Δ m I O P i o Q (R: iProp Σ):
    Predicate I O P →
    AbductStep (abduct Δ (P i o -∗ Q) R) Δ (abd_assume m (P i o) Q) R.
  Proof. intros ??. by eapply abduct_abd_assume_predicate. Qed.

  Global Instance abduct_step_abd_assume_atom Δ m A `{!Atom A} (P: iProp Σ) R:
    AbductStep (abduct Δ (A -∗ P) R) Δ (abd_assume m A P) R | 1000.
  Proof. intros ?. by eapply abduct_abd_assume_atom. Qed.

End assume_instances.


Section learning_pure_assertions.
  Context `{!refinedcG Σ}.

  (* learning pure information
     from case analysis *)
  Definition learn_simpl_prop (φ: Prop) (P Q: iProp Σ) :=
    φ → P ⊢ Q.

  Definition learn_simpl_all (φ: Prop) (Δ Δ': qenvs Σ) :=
    φ → qenvs_ctx_prop Δ ⊢ qenvs_ctx_prop Δ'.

  Lemma learn_simpl_prop_refl φ P:
    learn_simpl_prop φ P P.
  Proof. done. Qed.

  Lemma learn_simpl_all_learn_simpl φ Γ1 Γ2 Ω1 Ω2 Ω2':
    TCForall2 (λ P Q, learn_simpl_prop φ P Q) Ω2 Ω2' →
    learn_simpl_all φ (QE Γ1 Γ2 Ω1 Ω2) (QE Γ1 Γ2 Ω1 Ω2').
  Proof.
    rewrite /learn_simpl_all TCForall2_Forall2.
    rewrite /qenvs_ctx_prop. iIntros (Hall Hφ) "($ & $ & $ & Ω2)".
    iApply (big_sepL_id_mono' with "Ω2").
    eapply Forall2_impl; first done.
    intros x y Hsimpl'. by eapply Hsimpl'.
  Qed.

  Lemma abduct_abd_learn_simpl φ Δ Δ' P R:
    learn_simpl_all φ Δ Δ' →
    abduct Δ' P R →
    abduct Δ (abd_learn_simpl φ P) R .
  Proof.
    rewrite /abd_learn_simpl /qenvs_merge.
    iIntros (Hsimpl Habd) "[Δ R] %Hφ".
    rewrite /learn_simpl_all in Hsimpl. rewrite Hsimpl //.
    iApply Habd. iFrame.
  Qed.

  Global Instance abduct_step_abd_learn_simpl φ Δ Δ' (P R: iProp Σ):
    learn_simpl_all φ Δ Δ' →
    AbductStep (abduct Δ' P R) Δ (abd_learn_simpl φ P) R .
  Proof. intros ??. by eapply abduct_abd_learn_simpl. Qed.

  Lemma learn_simpl_prop_dn φ `{!Decision φ} P Q :
    learn_simpl_prop φ P Q →
    learn_simpl_prop (¬ ¬ φ) P Q.
  Proof.
    rewrite /learn_simpl_prop.
    iIntros (Ha Hb). apply Ha. by eapply dec_stable.
  Qed.

End learning_pure_assertions.


Global Typeclasses Opaque abd_learn_simpl.


Existing Class learn_simpl_prop.
Global Hint Mode learn_simpl_prop - ! ! - : typeclass_instances.

Existing Class learn_simpl_all.
Global Hint Mode learn_simpl_prop - ! ! - : typeclass_instances.

Global Existing Instance learn_simpl_prop_dn | 1.
Global Existing Instance learn_simpl_prop_refl | 10.

Global Existing Instance learn_simpl_all_learn_simpl | 2.
