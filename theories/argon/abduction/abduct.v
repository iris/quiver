From quiver.argon.base Require Export classes judgments precond.
From quiver.argon.simplification Require Export simplify normalize.

(** * Abduction *)


(* contexts for abduction *)
Section abduct_contexts.
  Context {Σ: gFunctors}.

  Inductive qenvs :=
    QE
      (Γ1: list Prop)         (* assertions *)
      (Γ2: list Prop)         (* assumptions *)
      (Ω1: list (iProp Σ))    (* persistent separation logic propositions *)
      (Ω2: list (iProp Σ)).   (* separation logic propositions *)

  Definition qenvs_empty : qenvs := QE [] [] [] [].

  Definition qenvs_ctx_prop (Δ: qenvs) : iProp Σ :=
    let '(QE Γ1 Γ2 Ω1 Ω2) := Δ in l2p Γ1 ∗ l2p Γ2 ∗ (□ [∗] Ω1) ∗ ([∗] Ω2).

  Definition qenvs_entails (Δ: qenvs) (φ: Prop) : Prop :=
    qenvs_ctx_prop Δ ⊢ ⌜φ⌝.

  Definition qenvs_assumptions_to_assertions (Δ: qenvs) : qenvs :=
    match Δ with QE Γ1 Γ2 Ω1 Ω2 => QE (Γ1 ++ Γ2) nil Ω1 Ω2 end.

  Definition qenvs_persistent_copy (Δ: qenvs) : qenvs :=
    match Δ with QE Γ1 Γ2 Ω1 Ω2 => QE Γ1 Γ2 Ω1 nil end.

  Definition qenvs_nonempty (Δ: qenvs) : Prop :=
    match Δ with QE Γ1 Γ2 Ω1 Ω2 => Γ1 ≠ [] ∨ Γ2 ≠ [] ∨ Ω1 ≠ [] ∨ Ω2 ≠ [] end.

  Definition qenvs_merge (Δ1 Δ2 Δ: qenvs) : Prop :=
    qenvs_ctx_prop Δ1 ∗ qenvs_ctx_prop Δ2 ⊣⊢ qenvs_ctx_prop Δ.

  (* basic lemmas about contexts  *)
  Lemma qenvs_ctx_empty : qenvs_ctx_prop (QE nil nil nil nil) ⊣⊢ True.
  Proof.
    rewrite /qenvs_ctx_prop /=. iSplit; done.
  Qed.

  Lemma qenvs_ctx_cons_pure_assertion φ Γ1 Γ2 Ω1 Ω2:
    qenvs_ctx_prop (QE (φ :: Γ1) Γ2 Ω1 Ω2) ⊣⊢ ⌜φ⌝ ∗ qenvs_ctx_prop (QE Γ1 Γ2 Ω1 Ω2).
  Proof.
    rewrite /qenvs_ctx_prop /=. rewrite l2p_cons -bi.sep_assoc //.
  Qed.

  Lemma qenvs_ctx_cons_pure_assumption φ Γ1 Γ2 Ω1 Ω2:
    qenvs_ctx_prop (QE Γ1 (φ :: Γ2) Ω1 Ω2) ⊣⊢ ⌜φ⌝ ∗ qenvs_ctx_prop (QE Γ1 Γ2 Ω1 Ω2).
  Proof.
    rewrite /qenvs_ctx_prop /=. rewrite l2p_cons -bi.sep_assoc //.
    iSplit; iIntros "[H1 [H2 H3]]"; iFrame.
  Qed.

  Lemma qenvs_ctx_cons_sep_assertion P Γ1 Γ2 Ω1 Ω2:
    qenvs_ctx_prop (QE Γ1 Γ2 Ω1 (P :: Ω2)) ⊣⊢ P ∗ qenvs_ctx_prop (QE Γ1 Γ2 Ω1 Ω2).
  Proof.
    rewrite /qenvs_ctx_prop /=.
    iSplit; iIntros "[H1 [H2 [H3 [H4 H5]]]]"; iFrame.
  Qed.

  Lemma qenvs_ctx_cons_pers_assertion P Γ1 Γ2 Ω1 Ω2:
    qenvs_ctx_prop (QE Γ1 Γ2 (P :: Ω1) Ω2) ⊣⊢ □ P ∗ qenvs_ctx_prop (QE Γ1 Γ2 Ω1 Ω2).
  Proof.
    rewrite /qenvs_ctx_prop /=.
    iSplit.
    - iIntros "[H1 [H2 [[H3 H3'] H4]]]". iFrame.
    - iIntros "[#H1 [H2 [H3 [#H4 H5]]]]". iFrame "H1 H4". iFrame.
  Qed.

  Lemma qenvs_ctx_assumptions_to_assertions Δ:
    qenvs_ctx_prop Δ ⊣⊢ qenvs_ctx_prop (qenvs_assumptions_to_assertions Δ).
  Proof.
    destruct Δ as [Γ1 Γ2 Ω1 Ω2]. rewrite /qenvs_ctx_prop /=.
    rewrite l2p_app l2p_nil left_id -bi.sep_assoc //.
  Qed.

  Lemma qenvs_ctx_persistent_copy Δ:
    qenvs_ctx_prop Δ ⊣⊢ qenvs_ctx_prop (qenvs_persistent_copy Δ) ∗ qenvs_ctx_prop Δ.
  Proof.
    destruct Δ as [Γ1 Γ2 Ω1 Ω2]. rewrite /qenvs_ctx_prop /=.
    rewrite right_id -bi.sep_assoc //.
    iSplit.
    - iIntros "[#HΓ1 [#HΓ2 [#HΩ1 HΩ2]]]". iFrame "HΓ1 HΓ2 HΩ1 HΩ2".
    - iIntros "($ & [$ ?] & ? & ? & ? & ?)". iFrame.
  Qed.

  Lemma qenvs_ctx_pure_assumption_mono Γ1 Γ2 Γ2' Ω1 Ω2:
    (Forall id Γ2 → Forall id Γ2') →
    qenvs_ctx_prop (QE Γ1 Γ2 Ω1 Ω2) ⊢ qenvs_ctx_prop (QE Γ1 Γ2' Ω1 Ω2).
  Proof. rewrite /qenvs_ctx_prop /=. iIntros (?) "($&%&$&$)". iPureIntro. naive_solver. Qed.

  (* entailment lemmas *)
  Lemma qenvs_entails_assume_assertion (φ: Prop) Γ1 Γ2 Ω1 Ω2 ψ:
    (∀ _: φ, qenvs_entails (QE Γ1 Γ2 Ω1 Ω2) ψ) → qenvs_entails (QE (φ :: Γ1) Γ2 Ω1 Ω2) ψ.
  Proof.
    rewrite /qenvs_entails. rewrite qenvs_ctx_cons_pure_assertion.
    iIntros (HΔ) "[%Hφ Hctx]". by iApply HΔ.
  Qed.

  Lemma qenvs_entails_assume_assumption (φ: Prop) Γ2 Ω1 Ω2 ψ:
    (∀ _: φ, qenvs_entails (QE nil Γ2 Ω1 Ω2) ψ) → qenvs_entails (QE nil (φ :: Γ2) Ω1 Ω2) ψ.
  Proof.
    rewrite /qenvs_entails. rewrite qenvs_ctx_cons_pure_assumption.
    iIntros (HΔ) "[%Hφ Hctx]". by iApply HΔ.
  Qed.

  Lemma qenvs_entails_destruct_pers A φ ψ Ω1 Ω2:
    once_tc (extract_from_atom A ψ) →
    (ψ → qenvs_entails (QE nil nil Ω1 Ω2) φ) →
    qenvs_entails (QE nil nil (A :: Ω1) Ω2) φ.
  Proof.
    rewrite /qenvs_entails /qenvs_ctx_prop /extract_from_atom /once_tc /=.
    intros ->. iIntros (Hent) "[#HΓ1 [#HΓ2 [#HΩ1 HΩ2]]]".
    iDestruct "HΩ1" as "[%Hψ HΩ1]". iApply Hent; first done.
    by iFrame "#".
  Qed.

  Lemma qenvs_entails_destruct_spatial A φ ψ Ω2:
    once_tc (extract_from_atom A ψ) →
    (ψ → qenvs_entails (QE nil nil nil Ω2) φ) →
    qenvs_entails (QE nil nil nil (A :: Ω2)) φ.
  Proof.
    rewrite /qenvs_entails /qenvs_ctx_prop /extract_from_atom /once_tc /=.
    intros ->. iIntros (Hent) "[#HΓ1 [#HΓ2 [#HΩ1 HΩ2]]]".
    iDestruct "HΩ2" as "[%Hψ HΩ2]". iApply Hent; first done.
    by iFrame "#".
  Qed.

  Lemma qenvs_entails_done (φ: Prop) :
    φ → qenvs_entails (QE [] [] [] []) φ.
  Proof.
    rewrite /qenvs_entails. iIntros (HΔ) "Hctx". done.
  Qed.

  Lemma qenvs_entails_conj_iff Δ (φ: Prop) (ψ: Prop):
    qenvs_entails Δ φ ∧ qenvs_entails Δ ψ ↔
    qenvs_entails Δ (φ ∧ ψ).
  Proof.
    rewrite /qenvs_entails. split.
    - iIntros ([HΔ1 HΔ2]) "Hctx".
      iDestruct (HΔ1 with "Hctx") as "%Hφ".
      iDestruct (HΔ2 with "Hctx") as "%Hψ".
      done.
    - intros Hent; split; rewrite Hent; iIntros "[%Hφ %Hψ]"; done.
  Qed.

  Lemma qenvs_entails_conj Δ (φ: Prop) (ψ: Prop):
    once_tc (qenvs_entails Δ φ) →
    once_tc (qenvs_entails Δ ψ) →
    qenvs_entails Δ (φ ∧ ψ).
  Proof.
    rewrite -qenvs_entails_conj_iff //.
  Qed.

  Lemma qenvs_entails_forall Δ {A: Type} (φ: A → Prop):
    once_tc (∀ x, qenvs_entails Δ (φ x)) →
    qenvs_entails Δ (∀ x, φ x).
  Proof.
    rewrite /qenvs_entails. iIntros (HΔ) "Hctx".
    iApply pure_forall_2. iIntros (a). iApply HΔ. done.
  Qed.

  Lemma qenvs_entails_exists Δ {A: Type} (a: A) (φ: A → Prop):
    once_tc (qenvs_entails Δ (φ a)) →
    qenvs_entails Δ (∃ x, φ x).
  Proof.
    rewrite /qenvs_entails. iIntros (HΔ) "Hctx".
    rewrite HΔ. iDestruct "Hctx" as "%Hφ".
    iPureIntro. eexists _. done.
  Qed.

  Lemma qenvs_entails_apply Δ φ:
    qenvs_entails Δ φ → qenvs_ctx_prop Δ ⊢ ⌜φ⌝.
  Proof. done. Qed.

  Lemma qenvs_entails_prove (Δ: qenvs) (φ : Prop):
    φ → qenvs_entails Δ φ.
  Proof.
    intros Hφ. rewrite /qenvs_entails.
    iIntros "_". by iPureIntro.
  Qed.


  (* context manipulation classes *)
  Definition qenvs_add_pure_assertion Δ φ Δ' : Prop :=
    qenvs_ctx_prop Δ' ⊣⊢ ⌜φ⌝ ∗ qenvs_ctx_prop Δ.

  Lemma qenvs_add_pure_assertion_cons φ Γ1 Γ2 Ω1 Ω2:
    qenvs_add_pure_assertion (QE Γ1 Γ2 Ω1 Ω2) φ (QE (φ :: Γ1) Γ2 Ω1 Ω2).
  Proof.
    rewrite /qenvs_add_pure_assertion.
    rewrite qenvs_ctx_cons_pure_assertion //.
  Qed.

  Definition qenvs_add_pure_assumption Δ φ Δ' : Prop :=
    qenvs_ctx_prop Δ' ⊣⊢ ⌜φ⌝ ∗ qenvs_ctx_prop Δ.

  Lemma qenvs_add_pure_assumption_cons φ Γ1 Γ2 Ω1 Ω2:
    qenvs_add_pure_assumption (QE Γ1 Γ2 Ω1 Ω2) φ (QE Γ1 (φ :: Γ2) Ω1 Ω2).
  Proof.
    rewrite /qenvs_add_pure_assumption.
    rewrite qenvs_ctx_cons_pure_assumption //.
  Qed.

  Definition qenvs_add_sep_assertion Δ P Δ' : Prop :=
    qenvs_ctx_prop Δ' ⊣⊢ P ∗ qenvs_ctx_prop Δ.

  Lemma qenvs_add_sep_assertion_cons P Γ1 Γ2 Ω1 Ω2:
    qenvs_add_sep_assertion (QE Γ1 Γ2 Ω1 Ω2) P (QE Γ1 Γ2  Ω1 (P :: Ω2)).
  Proof.
    rewrite /qenvs_add_sep_assertion.
    rewrite qenvs_ctx_cons_sep_assertion //.
  Qed.

  Definition qenvs_add_pers_assertion Δ P Δ' : Prop :=
    qenvs_ctx_prop Δ' ⊣⊢ □ P ∗ qenvs_ctx_prop Δ.

  Lemma qenvs_add_pers_assertion_cons P Γ1 Γ2 Ω1 Ω2:
    qenvs_add_pers_assertion (QE Γ1 Γ2 Ω1 Ω2) P (QE Γ1 Γ2 (P :: Ω1) Ω2).
  Proof.
    rewrite /qenvs_add_pers_assertion.
    rewrite qenvs_ctx_cons_pers_assertion //.
  Qed.


  Definition qenvs_pure_assertions Δ Γ : Prop :=
    qenvs_ctx_prop Δ ⊣⊢ l2p Γ ∗ qenvs_ctx_prop Δ.

  Lemma qenvs_pure_assertions_inst Γ1 Γ2 Ω1 Ω2:
    qenvs_pure_assertions (QE Γ1 Γ2 Ω1 Ω2) Γ1.
  Proof.
    rewrite /qenvs_pure_assertions /qenvs_ctx_prop.
    iSplit.
    - iIntros "(#H1 & $ & $)". iFrame "H1".
    - iIntros "($ & _ & $ & $)".
  Qed.

  Definition qenvs_pure_assumptions Δ Γ : Prop :=
    qenvs_ctx_prop Δ ⊣⊢ l2p Γ ∗ qenvs_ctx_prop Δ.

  Lemma qenvs_pure_assumptions_inst Γ1 Γ2 Ω1 Ω2:
    qenvs_pure_assumptions (QE Γ1 Γ2 Ω1 Ω2) Γ2.
  Proof.
    rewrite /qenvs_pure_assumptions /qenvs_ctx_prop.
    iSplit.
    - iIntros "($ & #H2 & $)". iFrame "H2".
    - iIntros "($ & $ & _ & $)".
  Qed.

  Definition qenvs_sep_assertions Δ Ω : Prop :=
    qenvs_ctx_prop Δ ⊢ [∗] Ω.

  Lemma qenvs_sep_assertions_inst Γ1 Γ2 Ω1 Ω2:
    qenvs_sep_assertions (QE Γ1 Γ2 Ω1 Ω2) Ω2.
  Proof.
    rewrite /qenvs_sep_assertions /qenvs_ctx_prop.
    iIntros "(_ & _ & ? & $)".
  Qed.

  Definition qenvs_pers_assertions Δ Ω : Prop :=
    qenvs_ctx_prop Δ ⊢ □ ([∗] Ω) ∗ qenvs_ctx_prop Δ.

  Lemma qenvs_pers_assertions_inst Γ1 Γ2 Ω1 Ω2:
    qenvs_pers_assertions (QE Γ1 Γ2 Ω1 Ω2) Ω1.
  Proof.
    rewrite /qenvs_pers_assertions /qenvs_ctx_prop.
    iIntros "($ & $ & #$ & $)".
  Qed.

  Definition qenvs_move_assumptions_to_assertions Δ1 Δ2 :=
    qenvs_ctx_prop Δ2 ⊣⊢ qenvs_ctx_prop (qenvs_assumptions_to_assertions Δ1).

  Lemma qenvs_move_assumptions_to_assertions_inst Γ1 Γ2 Γ Ω1 Ω2:
    Simpl (Γ1 ++ Γ2) Γ →
    qenvs_move_assumptions_to_assertions (QE Γ1 Γ2 Ω1 Ω2) (QE Γ nil Ω1 Ω2).
  Proof.
    intros <-. rewrite /qenvs_move_assumptions_to_assertions /qenvs_assumptions_to_assertions //.
  Qed.

  Definition qenvs_create_persistent_copy Δ1 Δ2 :=
    qenvs_ctx_prop Δ2 ⊣⊢ qenvs_ctx_prop (qenvs_persistent_copy Δ1).

  Lemma qenvs_create_persistent_copy_inst Γ1 Γ2 Ω1 Ω2:
    qenvs_create_persistent_copy (QE Γ1 Γ2 Ω1 Ω2) (QE Γ1 Γ2 Ω1 nil).
  Proof.
    rewrite /qenvs_create_persistent_copy /qenvs_persistent_copy //.
  Qed.

  Global Instance persistent_persistent_copy (Δ: qenvs):
    Persistent (qenvs_ctx_prop (qenvs_persistent_copy Δ)).
  Proof.
    rewrite /qenvs_ctx_prop /qenvs_persistent_copy.
    destruct Δ; apply _.
  Qed.

  Definition qenvs_find_atom (fd: find_depth) Δ A Δ' : Prop :=
    qenvs_ctx_prop Δ ⊣⊢ A ∗ qenvs_ctx_prop Δ'.

  Lemma qenvs_find_atom_inst_sep fd Γ1 Γ2 Ω1 Ω2 Ω2' A:
    FindAtom fd Ω2 A Ω2' →
    qenvs_find_atom fd (QE Γ1 Γ2 Ω1 Ω2) A (QE Γ1 Γ2 Ω1 Ω2').
  Proof.
    rewrite /qenvs_find_atom /qenvs_ctx_prop.
    intros Hfind. rewrite Hfind. iSplit; iIntros "($ & $ & $ & $)".
  Qed.

  Lemma qenvs_find_atom_inst_pers fd Γ1 Γ2 Ω1 Ω2 Ω1' A:
    FindAtom fd Ω1 A Ω1' →
    qenvs_find_atom fd (QE Γ1 Γ2 Ω1 Ω2) A (QE Γ1 Γ2 Ω1 Ω2).
  Proof.
    rewrite /qenvs_find_atom /qenvs_ctx_prop.
    intros Hfind. rewrite Hfind. iSplit.
    - iIntros "($ & $ & #[$ $] & $)".
    - iIntros "(_ & $ & $ & $ & $)".
  Qed.

  Definition qenvs_find_pure_atom Δ φ Δ' : Prop :=
    qenvs_ctx_prop Δ ⊣⊢ ⌜φ⌝ ∗ qenvs_ctx_prop Δ'.

  Lemma qenvs_find_pure_atom_inst_assum Γ1 Γ2 Γ2' Ω1 Ω2 φ:
    FindPureAtom Γ2 φ Γ2' →
    qenvs_find_pure_atom (QE Γ1 Γ2 Ω1 Ω2) φ (QE Γ1 Γ2' Ω1 Ω2).
  Proof.
    rewrite /FindPureAtom /qenvs_find_pure_atom /qenvs_ctx_prop.
    intros Hfind. rewrite /l2p Hfind.
    iSplit.
    - iIntros "($ & ($ & $) & $ & $)".
    - iIntros "(? & $ & ? & $ & $)". iSplit; iFrame.
  Qed.

  Lemma qenvs_find_pure_atom_inst_assert Γ1 Γ1' Γ2 Ω1 Ω2 A:
    FindPureAtom Γ1 A Γ1' →
    qenvs_find_pure_atom (QE Γ1 Γ2 Ω1 Ω2) A (QE Γ1' Γ2 Ω1 Ω2).
  Proof.
    rewrite /FindPureAtom /qenvs_find_pure_atom /qenvs_ctx_prop.
    intros Hfind. rewrite /l2p Hfind. iSplit.
    - iIntros "(($ & $) & $ & $ & $)".
    - iIntros "(? & ? & $ & $ & $)". iSplit; iFrame.
  Qed.

  Lemma qenvs_nonempty_assertion φ Γ1 Γ2 Ω1 Ω2:
    qenvs_nonempty (QE (φ :: Γ1) Γ2 Ω1 Ω2).
  Proof. simpl; auto. Qed.

  Lemma qenvs_nonempty_assumption φ Γ1 Γ2 Ω1 Ω2:
    qenvs_nonempty (QE Γ1 (φ :: Γ2) Ω1 Ω2).
  Proof. simpl; auto. Qed.

  Lemma qenvs_nonempty_sep_assertion P Γ1 Γ2 Ω1 Ω2:
    qenvs_nonempty (QE Γ1 Γ2 Ω1 (P :: Ω2)).
  Proof. simpl; auto. Qed.

  Lemma qenvs_nonempty_pers_assertion P Γ1 Γ2 Ω1 Ω2:
    qenvs_nonempty (QE Γ1 Γ2 (P :: Ω1) Ω2).
  Proof. simpl; auto. Qed.

  Lemma qenvs_merge_inst Γ1 Γ1' Γ1'' Γ2 Γ2' Γ2'' Ω1 Ω1' Ω1'' Ω2 Ω2' Ω2'':
    Simpl (Γ1 ++ Γ1') Γ1'' →
    Simpl (Γ2 ++ Γ2') Γ2'' →
    Simpl (Ω1 ++ Ω1') Ω1'' →
    Simpl (Ω2 ++ Ω2') Ω2'' →
    qenvs_merge (QE Γ1 Γ2 Ω1 Ω2) (QE Γ1' Γ2' Ω1' Ω2') (QE Γ1'' Γ2'' Ω1'' Ω2'').
  Proof.
    intros <- <- <- <-. rewrite /qenvs_merge /qenvs_ctx_prop.
    rewrite !l2p_app !big_sepL_app -!bi.sep_assoc. iSplit.
    - iIntros "(? & ? & #HΩ1 & ? & ? & ? & #HΩ1' & ?)". iFrame "#". iFrame.
    - iIntros "(? & ? & ? & ? & [? ?] & ? & ?)". iFrame.
  Qed.


End abduct_contexts.

Global Arguments qenvs : clear implicits.
Global Arguments qenvs_empty /.
Global Arguments qenvs_ctx_prop : simpl never.

Notation PersistentCtx Δ := (Persistent (qenvs_ctx_prop Δ)).

Existing Class qenvs_entails.
Global Hint Mode qenvs_entails - ! - : typeclass_instances.
Notation "Δ ⊨ φ" := (qenvs_entails Δ φ) (at level 20, φ at next level, right associativity) : type_scope.

Global Existing Instance qenvs_entails_assume_assumption | 2.
Global Existing Instance qenvs_entails_assume_assertion  | 2.
Global Existing Instance qenvs_entails_destruct_pers | 2.
Global Existing Instance qenvs_entails_destruct_spatial  | 2.
Global Existing Instance qenvs_entails_exists            | 3.
Global Existing Instance qenvs_entails_forall            | 3.
Global Existing Instance qenvs_entails_conj              | 3.
Global Existing Instance qenvs_entails_done              | 10.

Global Hint Extern 9 (qenvs_entails (QE nil nil nil nil) _) =>
  (apply qenvs_entails_done; assumption) : typeclass_instances.


(* Classes to manipulate the context without explicit pattern matching.
   Use these classes over explicit pattern matches to avoid having to
   change lemmas whenever the definition of a context changes. *)
Existing Class qenvs_add_pure_assumption.
Global Hint Mode qenvs_add_pure_assumption - ! - - : typeclass_instances.
Global Existing Instance qenvs_add_pure_assumption_cons.

Existing Class qenvs_add_pure_assertion.
Global Hint Mode qenvs_add_pure_assertion - ! - - : typeclass_instances.
Global Existing Instance qenvs_add_pure_assertion_cons.

Existing Class qenvs_add_sep_assertion.
Global Hint Mode qenvs_add_sep_assertion - ! - - : typeclass_instances.
Global Existing Instance qenvs_add_sep_assertion_cons.

Existing Class qenvs_add_pers_assertion.
Global Hint Mode qenvs_add_pers_assertion - ! - - : typeclass_instances.
Global Existing Instance qenvs_add_pers_assertion_cons.

Existing Class qenvs_pure_assumptions.
Global Hint Mode qenvs_pure_assumptions - ! - : typeclass_instances.
Global Existing Instance qenvs_pure_assumptions_inst.

Existing Class qenvs_pure_assertions.
Global Hint Mode qenvs_pure_assertions - ! - : typeclass_instances.
Global Existing Instance qenvs_pure_assertions_inst.

Existing Class qenvs_sep_assertions.
Global Hint Mode qenvs_sep_assertions - ! - : typeclass_instances.
Global Existing Instance qenvs_sep_assertions_inst.

Existing Class qenvs_pers_assertions.
Global Hint Mode qenvs_pers_assertions - ! - : typeclass_instances.
Global Existing Instance qenvs_pers_assertions_inst.

Existing Class qenvs_move_assumptions_to_assertions.
Global Hint Mode qenvs_move_assumptions_to_assertions - ! - : typeclass_instances.
Global Existing Instance qenvs_move_assumptions_to_assertions_inst.

Existing Class qenvs_create_persistent_copy.
Global Hint Mode qenvs_create_persistent_copy - ! - : typeclass_instances.
Global Existing Instance qenvs_create_persistent_copy_inst.

Existing Class qenvs_find_atom.
Global Hint Mode qenvs_find_atom - + ! ! - : typeclass_instances.
Global Existing Instance qenvs_find_atom_inst_sep.
Global Existing Instance qenvs_find_atom_inst_pers.

Existing Class qenvs_find_pure_atom.
Global Hint Mode qenvs_find_pure_atom - ! ! - : typeclass_instances.
Global Existing Instance qenvs_find_pure_atom_inst_assum.
Global Existing Instance qenvs_find_pure_atom_inst_assert.

Existing Class qenvs_nonempty.
Global Hint Mode qenvs_nonempty - ! : typeclass_instances.
Global Existing Instance qenvs_nonempty_assertion.
Global Existing Instance qenvs_nonempty_assumption.
Global Existing Instance qenvs_nonempty_sep_assertion.
Global Existing Instance qenvs_nonempty_pers_assertion.

Existing Class qenvs_merge.
Global Hint Mode qenvs_merge - ! ! - : typeclass_instances.
Global Existing Instance qenvs_merge_inst.


Section abduction.
  Context {Σ: gFunctors}.

  Definition abduct (Δ: qenvs Σ) (P: iProp Σ) (R: iProp Σ):=
    (qenvs_ctx_prop Δ) ∗ R ⊢ P.

  Definition abduct_atom (A: iProp Σ) (P : iProp Σ) : iProp Σ := A ∗ P.

  Global Instance abduct_proper_impl Δ:
    Proper ((⊢) ==> eq ==> impl) (abduct Δ).
  Proof.
    intros P Q Hent R R' <- Habd. iIntros "Hctx".
    iApply Hent. by iApply Habd.
  Qed.

  Global Instance abduct_proper_impl_flip Δ:
    Proper (flip (⊢) ==> eq ==> flip impl) (abduct Δ).
  Proof.
    intros P Q Hent R R' <- Habd. iIntros "Hctx".
    iApply Hent. by iApply Habd.
  Qed.

  Global Instance abduct_proper_iff Δ:
    Proper ((⊣⊢) ==> eq ==> iff) (abduct Δ).
  Proof.
    intros P Q Heq R R' <-. eapply bi.equiv_entails in Heq as [HPQ HQP].
    split; rewrite ?HPQ ?HQP //=.
  Qed.


  Lemma abduct_qenvs_entails Δ φ P R:
    Δ ⊨ φ →
    (φ → abduct Δ P R) →
    abduct Δ P R.
  Proof.
    iIntros (Hent%qenvs_entails_apply Habd) "[Δ R]".
    iDestruct (Hent with "Δ") as "%Hφ".
    iApply Habd; first done.
    by iFrame.
  Qed.

  Lemma abduct_ent (Δ: qenvs Σ) P Q R:
    (P ⊢ Q) →
    abduct Δ P R →
    abduct Δ Q R.
  Proof. by intros ->. Qed.

  (* the strong version gives access to the context *)
  Lemma abduct_ent_strong (Δ: qenvs Σ) P Q C R:
    (P ∧ C ⊢ Q) →
    (qenvs_ctx_prop Δ ⊢ C) →
    abduct Δ P R →
    abduct Δ Q R.
  Proof.
    iIntros (Hent Hctx Habd) "[Δ R]".
    iApply Hent. iSplit.
    - iApply Habd. iFrame.
    - iApply Hctx. iFrame.
  Qed.

  Lemma abduct_ent_sidecond (Δ: qenvs Σ) P Q (φ: Prop) R:
    (φ → (P ⊢ Q)) →
    (Δ ⊨ φ) →
    abduct Δ P R →
    abduct Δ Q R.
  Proof.
    intros ?? Habd. eapply abduct_qenvs_entails; first done.
    intros ?. eapply abduct_ent; last apply Habd. eauto.
  Qed.

  Lemma abduct_refl Δ P:
    abduct Δ P P.
  Proof.
    iIntros "[HΔ $]".
  Qed.

  (** Basic rules for quantifiers. They are *not* used by
    the proofmode. The actual rules of the proofmode are
    a bit more clever. We give them in the section
    quantifiers below. *)
  Lemma abduct_basic_exists A Δ R P:
    (∀ a: A, abduct Δ (P a) (R a) ) →
    (abduct Δ (∃ a: A, P a) (∃ a: A, R a)).
  Proof.
    iIntros (Habd) "[HΔ [%a R]]".
    iExists a. iApply Habd. iFrame.
  Qed.

  Lemma abduct_basic_forall A Δ R P:
    (∀ a: A, abduct Δ (P a) (R a) ) →
    (abduct Δ (∀ a: A, P a) (∀ a: A, R a)).
  Proof.
    iIntros (Habd) "[HΔ HR]". iIntros (a).
    iApply Habd. iFrame. iApply "HR".
  Qed.

  Lemma abduct_exists_intro Δ {X: Type} (x: X) Φ (R : iProp Σ):
    abduct Δ (Φ x) R →
    abduct Δ (∃ x, Φ x) R.
  Proof. iIntros (Habd) "Hctx". iExists x. by iApply Habd. Qed.


  (** ** Magic Wands *)
  Lemma abduct_wand_assume A Δ Δ' R P :
    qenvs_add_sep_assertion Δ A Δ' →
    abduct Δ' P R →
    abduct Δ (A -∗ P) R.
  Proof.
    iIntros (Hadd Habd) "[HΔ HR] A".
    iApply Habd. rewrite Hadd. iFrame.
  Qed.

  Lemma abduct_wand_assume_pure φ Δ R P :
    abduct Δ (abd_assume ASSUME ⌜φ⌝ P) R →
    abduct Δ (⌜φ⌝ -∗ P) R.
  Proof.
    rewrite /abd_assume //.
  Qed.

  Lemma abduct_wand_pure_assume_hyp Δ P (φ: Prop) (R: iProp Σ):
    (φ → abduct Δ P R) →
    abduct Δ (⌜φ⌝ -∗ P) R.
  Proof.
    iIntros (Habd) "Hctx %Hφ".
    by iApply (Habd with "Hctx").
  Qed.

  Lemma abduct_wand_revert Γ1 Γ2 Ω1 Ω2 A P R:
    abduct (QE Γ1 Γ2 Ω1 Ω2) (A -∗ P) R →
    abduct (QE Γ1 Γ2 Ω1 (A :: Ω2)) P R.
  Proof.
    iIntros (Habd) "[(HΓ1 & HΓ2 & HΩ1 & HA & Hctx) HR]".
    iApply (Habd with "[$] HA").
  Qed.

  Lemma abduct_wand_sep Δ P1 P2 P3 R:
    abduct Δ (P1 -∗ P2 -∗ P3) R →
    abduct Δ ((P1 ∗ P2) -∗ P3) R.
  Proof. by rewrite bi.wand_curry. Qed.

  Lemma abduct_wand_exists Δ X P Q R:
    abduct Δ (∀ x, P x -∗ Q) R →
    abduct Δ ((∃ x: X, P x) -∗ Q) R.
  Proof.
    iIntros (Habd) "Hctx [%x HP]". by iApply (Habd with "Hctx HP").
  Qed.

  Lemma abduct_wand_pure (φ: Prop) Δ Δ' R P:
    qenvs_add_pure_assumption Δ φ Δ' →
    abduct Δ' P R →
    abduct Δ (⌜φ⌝ -∗ P) R.
  Proof.
    iIntros (Hadd Habd) "[HΔ R] %H".
    iApply Habd. rewrite Hadd. by iFrame.
  Qed.

  Lemma abduct_wand_emp Δ R P:
    (abduct Δ P R) →
    abduct Δ (emp -∗ P) R.
  Proof.
    iIntros (Habd) "[HΔ R] %H".
    iApply Habd; iFrame.
  Qed.

  Lemma abduct_wand_drop Δ P Q R:
    abduct Δ P R →
    abduct Δ (Q -∗ P) R.
  Proof.
    iIntros (Habd) "[HΔ R] Q".
    iApply Habd; iFrame.
  Qed.

  (** ** Separating Conjunction *)
  Lemma abduct_assoc Δ R (P1 P2 P: iProp Σ):
    abduct Δ (P1 ∗ (P2 ∗ P)) R →
    abduct Δ ((P1 ∗ P2) ∗ P) R.
  Proof.
    rewrite /abduct.
    intros Habd. rewrite -bi.sep_assoc Habd //=.
  Qed.

  Lemma abduct_prove_pure_immediate (φ: Prop) Δ R P:
    φ →
    abduct Δ P R →
    abduct Δ (⌜φ⌝ ∗ P) R.
  Proof.
    iIntros (Hφ Habd) "[HΔ HR]".
    iSplit; first done.
    by iApply Habd; iFrame.
  Qed.

  Lemma abduct_prove_pure (φ: Prop) Δ R P:
    Δ ⊨ φ →
    abduct Δ P R →
    abduct Δ (⌜φ⌝ ∗ P) R.
  Proof.
    intros Hent Habd. apply: abduct_qenvs_entails.
    intros Hφ. by eapply abduct_prove_pure_immediate.
  Qed.

  Lemma abduct_missing (P R A: iProp Σ) Δ:
    abduct Δ P R →
    abduct Δ (A ∗ P) (A ∗ R).
  Proof.
    iIntros (Habd) "[HΔ [$ R]]". by iApply (Habd with "[$]").
  Qed.

  Lemma abduct_assert_pure (φ: Prop) Δ Δ' R P:
    qenvs_add_pure_assertion Δ φ Δ' →
    abduct Δ' P R →
    abduct Δ (⌜φ⌝ ∗ P) (⌜φ⌝ ∗ R).
  Proof.
    iIntros (Hadd Habd) "[HΔ [%Hφ HR]]".
    iSplit; first done.
    iApply Habd. rewrite Hadd. by iFrame.
  Qed.


  (** ** Cases *)
  (**
    We first try to prove one of the two branches of a case (i.e., [φ] or [¬ φ]).
    If that fails, we promote the [case φ] to the abducted precondition [R].
    Promoting the case is not always possible, because [φ] can contain variables
    that are not present in [R] (e.g., the return value of functions called in the body).
    Thus, if all else fails, we resort to promoting a *conjunction*
    [∧] without [φ] to [R]. The AbductionStep rules which do this
    reasoning are in [proofmode.v].
  *)
  Lemma abduct_case_true Δ (φ: dProp) P1 P2 R:
    Δ ⊨ φ →
    abduct Δ P1 R →
    abduct Δ (case φ P1 P2) R.
  Proof.
    intros Hent Habd. apply: abduct_qenvs_entails. intros Hφ.
    rewrite /case; destruct decide; naive_solver.
  Qed.

  Lemma abduct_case_false Δ (φ: dProp) P1 P2 R:
    (Δ ⊨ ¬ φ) →
    abduct Δ P2 R →
    abduct Δ (case φ P1 P2) R.
  Proof.
    intros Hent Habd. apply: abduct_qenvs_entails. intros Hφ.
    rewrite /case; destruct decide; naive_solver.
  Qed.

  Lemma abduct_case_lift Δ (φ: dProp) P1 P2 R1 R2:
    (abduct Δ (⌜φ⌝ -∗ P1) R1) →
    (abduct Δ (⌜¬ φ⌝ -∗ P2) R2) →
    abduct Δ (case φ P1 P2) (case φ R1 R2).
  Proof.
    iIntros (Habd1 Habd2) "Ctx"; rewrite /case; destruct decide.
    - by iApply (Habd1 with "[$]").
    - by iApply (Habd2 with "[$]").
  Qed.


  Lemma abduct_case_conj Δ (φ: dProp) P1 P2 R1 R2:
    (abduct Δ (⌜φ⌝ -∗ P1) R1) →
    (abduct Δ (⌜¬ φ⌝ -∗ P2) R2) →
    abduct Δ (case φ P1 P2) (R1 ∧ R2).
  Proof.
    intros H1 H2. rewrite /case. destruct decide.
    - iIntros "[HΔ [HR _]]". by iApply (H1 with "[$]").
    - iIntros "[HΔ [_ HR]]". by iApply (H2 with "[$]").
  Qed.

  Lemma abduct_case_conj_assume Δ (φ: dProp) P1 P2 R:
    abduct Δ ((⌜φ⌝ -∗ P1) ∧ (⌜¬ φ⌝ -∗ P2)) R →
    abduct Δ (case φ P1 P2) R.
  Proof.
    rewrite /abduct /case. iIntros (Habd) "Hctx".
    iDestruct (Habd with "Hctx") as "HP".
    destruct decide.
    - iDestruct "HP" as "[HP _]". by iApply "HP".
    - iDestruct "HP" as "[_ HP]". by iApply "HP".
  Qed.

  Lemma abduct_conj Δ (P1 P2 Q1 Q2: iProp Σ):
    (abduct Δ P1 Q1) ∧ (abduct Δ P2 Q2) →
    abduct Δ (P1 ∧ P2) (Q1 ∧ Q2).
  Proof.
    intros (Habd1 & Habd2).
    iIntros "[HΔ HQ]". iSplit.
    - iDestruct "HQ" as "[HQ _]". iApply Habd1. iFrame.
    - iDestruct "HQ" as "[_ HQ]". iApply Habd2. iFrame.
  Qed.

  Lemma abduct_conj_id Δ (P Q : iProp Σ):
    abduct Δ P Q →
    abduct Δ (P ∧ P) Q.
  Proof.
    intros Habd.
    iIntros "[HΔ HQ]". iSplit; iApply Habd; iFrame.
  Qed.

  Lemma abduct_conj_true_l Δ P R:
    abduct Δ P R →
    abduct Δ (True ∧ P) R.
  Proof.
    iIntros (Habd) "Ctx". iSplit; first done.
    by iApply Habd.
  Qed.

  Lemma abduct_conj_true_r Δ P R:
    abduct Δ P R →
    abduct Δ (P ∧ True) R.
  Proof.
    iIntros (Habd) "Ctx". iSplit; last done.
    by iApply Habd.
  Qed.

  Lemma abduct_conj_false_l Δ P R:
    abduct Δ False R →
    abduct Δ (False ∧ P) R.
  Proof.
    iIntros (Habd) "Ctx". by iDestruct (Habd with "Ctx") as "%".
  Qed.

  Lemma abduct_conj_false_r Δ P R:
    abduct Δ False R →
    abduct Δ (P ∧ False) R.
  Proof.
    iIntros (Habd) "Ctx". by iDestruct (Habd with "Ctx") as "%".
  Qed.

  (* support for abducting conjunction where the left conjunct is a state goal *)
  Lemma abduct_and_exists {X: Type} Δ (P: X → iProp Σ) (Q R: iProp Σ) :
    abduct Δ (∃ x: X, P x ∧ Q) R →
    abduct Δ ((∃ x: X, P x) ∧ Q) R.
  Proof.
    iIntros (Habd) "[HΔ HR]".
    iDestruct (Habd with "[$HΔ $HR]") as "[%x Hand]".
    iSplit.
    - iExists _. iDestruct "Hand" as "[$ _]".
    - iDestruct "Hand" as "[_ $]".
  Qed.

  Lemma abduct_and_pure Δ φ (P R: iProp Σ) :
    abduct Δ (⌜φ⌝ ∗ P) R →
    abduct Δ (⌜φ⌝ ∧ P) R.
  Proof.
    rewrite /abduct. intros ->.
    iIntros "[$ $]".
  Qed.

  Lemma abduct_and_sep_atom Δ (A P Q R: iProp Σ) :
    abduct Δ (A ∗ (Q ∧ (A -∗ P))) R →
    abduct Δ ((A ∗ Q) ∧ P) R.
  Proof.
    iIntros (Habd) "Hctx".
    iDestruct (Habd with "Hctx") as "[A Hrest]".
    iSplit.
    - by iDestruct "Hrest" as "[$ _]".
    - iDestruct "Hrest" as "[_ Hw]". by iApply "Hw".
  Qed.

  Lemma abduct_and_sep_pure Δ φ (P Q R: iProp Σ) :
    abduct Δ (⌜φ⌝ ∗ (Q ∧ P)) R →
    abduct Δ ((⌜φ⌝ ∗ Q) ∧ P) R.
  Proof.
    iIntros (Habd) "Hctx".
    iDestruct (Habd with "Hctx") as "[A Hrest]".
    iSplit.
    - by iDestruct "Hrest" as "[$ _]".
    - iDestruct "Hrest" as "[_ Hw]". by iApply "Hw".
  Qed.

  Lemma abduct_and_sep_assoc Δ (P1 P2 P3 Q R: iProp Σ) :
    abduct Δ ((P1 ∗ (P2 ∗ P3)) ∧ Q) R →
    abduct Δ (((P1 ∗ P2) ∗ P3) ∧ Q) R.
  Proof. rewrite bi.sep_assoc //. Qed.

  Lemma abduct_and_sep_exists {X} Δ (P: X → iProp Σ) P' Q R :
    abduct Δ ((∃ x, P x ∗ P') ∧ Q) R →
    abduct Δ (((∃ x, P x) ∗ P') ∧ Q) R.
  Proof. rewrite bi.sep_exist_r //. Qed.

  Lemma abduct_and_assoc Δ (P1 P2 Q R: iProp Σ) :
    abduct Δ (P1 ∧ (P2 ∧ Q)) R →
    abduct Δ ((P1 ∧ P2) ∧ Q) R.
  Proof. rewrite bi.and_assoc //. Qed.

  Lemma abduct_and_case Δ (φ: dProp) P1 P2 Q R:
    abduct Δ (case φ (P1 ∧ Q) (P2 ∧ Q))%I R →
    abduct Δ ((case φ P1 P2) ∧ Q) R.
  Proof.
    rewrite /case. destruct decide; done.
  Qed.

  Lemma abduct_and_atom Δ (A P R: iProp Σ) :
    abduct Δ (A ∗ (A -∗ P)) R →
    abduct Δ (A ∧ P) R.
  Proof.
    iIntros (Habd) "Hctx".
    iDestruct (Habd with "Hctx") as "[A Hw]".
    iSplit; first done. by iApply "Hw".
  Qed.

  (* ABDUCT ATOMS *)
  Lemma abduct_sep_atom Δ A `{!Atom A} (P R: iProp Σ):
    abduct Δ (abduct_atom A P) R →
    abduct Δ (A ∗ P) R.
  Proof.
    rewrite /abduct_atom //.
  Qed.

  Lemma abduct_atom_ctx Δ Δ' (A: iProp Σ) (P R : iProp Σ) :
    qenvs_find_atom DEEP Δ A Δ' →
    abduct Δ' P R →
    abduct Δ (abduct_atom A P) R.
  Proof.
    rewrite /abduct /abduct_atom.
    intros Hfind Hent. rewrite Hfind -Hent.
    iIntros "[[$ $] $]".
  Qed.

  Lemma abduct_atom_missing Δ (A: iProp Σ) (P R : iProp Σ) :
    abduct Δ P R →
    abduct Δ (abduct_atom A P) (A ∗ R).
  Proof.
    rewrite /abduct /abduct_atom.
    intros Hent. rewrite -Hent.
    iIntros "($ & $ & $)".
  Qed.

  (* BIG OPS *)
  Lemma abduct_nfs_cons_pure Δ φ Γ Δ' (P R: iProp Σ):
    abduct Δ (⌜φ⌝ ∗ (nfs Γ Δ' P)) R →
    abduct Δ (nfs (φ :: Γ) Δ' P) R.
  Proof. rewrite nfs_cons_pure //. Qed.

  Lemma abduct_nfs_cons_atom Δ A Γ Δ' (P R: iProp Σ):
    abduct Δ (abduct_atom A (nfs Γ Δ' P)) R →
    abduct Δ (nfs Γ (A :: Δ') P) R.
  Proof. rewrite nfs_cons_atom //. Qed.

  Lemma abduct_nfs_empty Δ (P R: iProp Σ):
    abduct Δ P R →
    abduct Δ (nfs [] [] P) R.
  Proof. rewrite nfs_empty //. Qed.

  Lemma abduct_nfw_cons_pure Δ φ Γ Δ' (P R: iProp Σ):
    abduct Δ (⌜φ⌝ -∗ (nfw Γ Δ' P)) R →
    abduct Δ (nfw (φ :: Γ) Δ' P) R.
  Proof. rewrite nfw_cons_pure //. Qed.

  Lemma abduct_nfw_cons_atom Δ A Γ Δ' (P R: iProp Σ):
    abduct Δ (A -∗ (nfw Γ Δ' P)) R →
    abduct Δ (nfw Γ (A :: Δ') P) R.
  Proof. rewrite nfw_cons_atom //. Qed.

  Lemma abduct_nfw_empty Δ (P R: iProp Σ):
    abduct Δ P R →
    abduct Δ (nfw [] [] P) R.
  Proof. rewrite nfw_empty //. Qed.

  Lemma abduct_true (Δ: qenvs Σ):
    abduct Δ True True.
  Proof.
    rewrite /abduct. iIntros "Hctx". done.
  Qed.

  Lemma abduct_false (Δ: qenvs Σ):
    abduct Δ False False.
  Proof.
    rewrite /abduct. iIntros "[Hctx ?]". done.
  Qed.


  (* SIMPLIFICATION *)

  Lemma abduct_simplify Δ (P R1 R2: iProp Σ):
    abduct Δ P R1 →
    simplify R1 R2 →
    abduct Δ P R2.
  Proof.
    rewrite /abduct simplify_eq /simplify_def.
    intros H1 H2. by rewrite H2.
  Qed.

  Lemma simplify_abduct Δ P Q R:
    simplify P Q →
    abduct Δ Q R →
    abduct Δ P R.
  Proof.
    rewrite simplify_eq /simplify_def. by intros ->.
  Qed.



  (* DEBUG LEMMAS *)
  (* for manual use *)
  Lemma debug_assert (φ: Prop) Δ (P: iProp Σ) R:
    (φ → abduct Δ P R) →
    abduct Δ P (⌜φ⌝ ∗ R).
  Proof.
    rewrite /abduct. iIntros (Hent) "[Δ [%Hφ R]]".
    iApply Hent; eauto with iFrame.
  Qed.

  Lemma debug_missing (A: iProp Σ) Δ Δ' (P: iProp Σ) R:
    qenvs_add_sep_assertion Δ A Δ' →
    abduct Δ' P R →
    abduct Δ P (A ∗ R).
  Proof.
    rewrite /abduct. iIntros (Hadd Hent) "[Δ [A R]]".
    iApply Hent; rewrite Hadd. iFrame.
  Qed.

  Lemma debug_exists (X: Type) Δ (P: iProp Σ) R:
    (∀ x, abduct Δ P (R x)) →
    abduct Δ P (∃ x: X, R x).
  Proof.
    rewrite /abduct. iIntros (Hent) "[Δ R]".
    iDestruct "R" as (x) "R".
    iApply Hent; eauto with iFrame.
  Qed.

  Lemma debug_drop_ctx fd A Δ Δ' (P R: iProp Σ):
    qenvs_find_atom fd Δ A Δ' →
    abduct Δ' P R →
    abduct Δ P R.
  Proof.
    rewrite /abduct /qenvs_find_atom.
    iIntros (Hfind Habd) "[HΔ HR]".
    iDestruct (Hfind with "HΔ") as "[_ HΔ']".
    by iApply (Habd with "[$HΔ' $HR]").
  Qed.

  Lemma debug_contradict Δ P (R: iProp Σ):
    Δ ⊨ False →
    unify R True%I →
    abduct Δ P R.
  Proof.
    intros ? ->. eapply abduct_qenvs_entails; done.
  Qed.


  (* QUANTIFIERS *)
  (** Handling quantifiers in precondition inference is a bit tricky.
      Consider the following example:
        foo(){
            int x = f();
            g(x);
        }
      where f : v ◁ᵥ void ∗ ∀ (n: Z) (w: val), w ◁ᵥ int i32 n -∗ Φ w
            g : ∃ n, v ◁ᵥ int i32 n ∗ ∀ (w: val), w ◁ᵥ void -∗ Φ w

      To handle this example, we need to be able to forward the value [x]
      from [f] to [g]. Here, we have two choices:
      1. We lift the quantifiers of [f] anf [g] into the precondition.
         Then we obtain something along the lines of
         v ◁ᵥ void ∗ ∀ (n: Z) (w: val), w ◁ᵥ int i32 n -∗
         ∃ n, w ◁ᵥ int i32 n ∗ ∀ (u: val), u ◁ᵥ void -∗ Φ u


        Unfortunately, this approach will lead to quantifier alternations
        in the precondition/the predicate transformer, which makes it hard
        to understand.

      2. To avoid quantifier alternations (i.e. to allow only a single
         ∃∀-alternation), we can introduce the universal
         quantifier into our proofcontext, but leave the precondition
         unchanged. Now the precondition must apply regardless of
         which value [f] returns. As a consequence, we can no longer
         turn the existential quantifier in the precondition of [g] into
         an existential quantifier in the precondition of [foo]---its
         choice depends on [x], the return value of [f].
  *)

  (* predicates to track which variables come from universal and existential
    quantifiers in the current context *)
  Inductive uni {A: Type} (a: A) : Prop := Uni.
  Inductive exi {A: Type} (a: A) : Prop := Exi.

  (* named versions of the quantifiers *)
  Definition existsNamed {A: Type} (s: string) (P: A → iProp Σ) : iProp Σ := ∃ x, P x.
  Definition forallNamed {A: Type} (s: string) (P: A → iProp Σ) : iProp Σ := ∀ x, P x.

  (* predicate to map between strings and identifiers *)
  Inductive have_ident (s : string) {A: Type} (a: A) : Prop := HaveIdent.

  (** Universal Quantifiers *)
  (** We split the rule [abduct_basic_forall] into two separate rules
      (and always split pairs before we introduce a quantifier).   *)

  (** [R] may not refer to [a] until we introduce a universal
      quantifier over [a] into [R] as well. *)
  Lemma abduct_forall_intros A Δ R P:
    (∀ a: A, uni a → abduct Δ (P a) R) →
    (abduct Δ (∀ a: A, P a) R).
  Proof.
    iIntros (Habd) "[HΔ R]". iIntros (a).
    iApply (Habd a (Uni a)).
    by iFrame.
  Qed.

  Lemma abduct_forall_named_intros A s Δ R P:
    (∀ a: A, uni a → abduct Δ (P a) R) →
    (abduct Δ (forallNamed s (λ a: A, P a)) R).
  Proof.
    rewrite /forallNamed. iIntros (Habd) "[HΔ R]". iIntros (a).
    iApply (Habd a (Uni a)).
    by iFrame.
  Qed.

  (** We can reintroduce a universal into the precondition
      again with this rule. To avoid introducing the quantifier
      into the precondition immediately, we do not add the rule
      to the proofmode directly, but delay it until we want to
      revert the goal. *)
  Lemma abduct_forall_elim A (a: A) Δ R P:
    uni a →
    (abduct Δ P (R a)) →
    abduct Δ P (∀ a, R a).
  Proof.
    rewrite /abduct. iIntros (_ Habd) "[HΔ HR]".
    iSpecialize ("HR" $! a). iApply Habd. iFrame.
  Qed.

  Lemma abduct_forall_pair A B Δ P (R: iProp Σ):
    (abduct Δ (∀ a: A, ∀ b: B, P (a, b)) R) →
    (abduct Δ (∀ p: A * B, P p) R).
  Proof.
    iIntros (Habd) "[HΔ R]". iIntros ([a b]).
    iPoseProof (Habd with "[$HΔ $R]") as "Hall".
    iApply "Hall".
  Qed.


  (** Existential Quantifiers *)
  (** Existential quantifiers are lifted directly into the preconditon.
      If the existential quantifier should be resolved by some kind of existential resolution,
      consider using [abd_evars] (for mere simplification) or [type_precond] (for proper existential instantiation).
  *)
  Lemma abduct_exists A Δ R P:
    (∀ a: A, exi a → abduct Δ (P a) (R a) ) →
    (abduct Δ (∃ a: A, P a) (∃ a: A, R a)).
  Proof.
    iIntros (Habd) "[HΔ [%a R]]".
    iExists a. iApply Habd; first done. iFrame.
  Qed.

  Lemma abduct_exists_named A s Δ R P:
    (∀ a: A, exi a → abduct Δ (P a) (R a)) →
    (abduct Δ (existsNamed s (λ a: A, P a)) (∃ a: A, R a)).
  Proof.
    rewrite /existsNamed. iIntros (Habd) "[HΔ [%a R]]".
    iExists a. iApply Habd; first done. iFrame.
  Qed.

  Lemma abduct_exists_pair Δ A B (P: A * B → iProp Σ) R:
    abduct Δ (∃ a, ∃ b, P (a, b)) R →
    abduct Δ (∃ p, P p) R.
  Proof.
    iIntros (Habd) "Hctx". iDestruct (Habd with "Hctx") as "(%a & %b & HP)".
    iExists (a, b). iFrame.
  Qed.

  (* an existential quantifier over a list of fixed length *)
  Lemma existsN_zero {X} (P: list X → iProp Σ):
    P nil ⊢ existsN 0 P.
  Proof.
    rewrite /existsN. iIntros "P". iExists _. by iFrame.
  Qed.

  Lemma existsN_succ {X} n (P: list X → iProp Σ):
    (∃ x: X, existsN n (λ L, P (x :: L))) ⊢ existsN (S n) P.
  Proof.
    rewrite /existsN.
    iIntros "(%x & %xs & %Hlen & P)".
    iExists _. iFrame. iPureIntro. rewrite /= Hlen //.
  Qed.

  Lemma abduct_existsN_zero Δ {X} (P: list X → iProp Σ) R:
    abduct Δ (P nil) R →
    abduct Δ (existsN 0 P) R.
  Proof. rewrite existsN_zero //. Qed.

  Lemma abduct_existsN_succ Δ {X} n (P: list X → iProp Σ) R:
    abduct Δ (∃ x: X, existsN n (λ L, P (x :: L))) R →
    abduct Δ (existsN (S n) P) R.
  Proof. rewrite existsN_succ //. Qed.


  Definition existsL {A: Type} (s: list string) (P: list A → iProp Σ) : iProp Σ := ∃ x, ⌜length s = length x⌝ ∗ P x.
  Definition forallL {A: Type} (s: list string) (P: list A → iProp Σ) : iProp Σ := ∀ x, ⌜length s = length x⌝ -∗ P x.

  Lemma abduct_existsL_nil A Δ (P: list A → iProp Σ) R:
    (abduct Δ (P nil) R) →
    (abduct Δ (existsL nil P) R).
  Proof.
    rewrite /existsL. iIntros (Habd) "Hctx".
    iDestruct (Habd with "Hctx") as "HP".
    iExists nil. iFrame. by iPureIntro.
  Qed.

  Lemma abduct_existsL_cons A s ss Δ P R:
    (abduct Δ (existsNamed s (λ x: A, existsL ss (λ xr, P (x :: xr)))) R) →
    abduct Δ (existsL (s :: ss) P) R.
  Proof.
    rewrite /existsNamed /existsL. iIntros (Habd) "Hctx".
    iDestruct (Habd with "Hctx") as "(%x & %xr & %Heq & HP)".
    iExists _. iFrame. iPureIntro. simpl. lia.
  Qed.


  Lemma abduct_forallL_nil A Δ (P: list A → iProp Σ) R:
    (abduct Δ (P nil) R) →
    (abduct Δ (forallL nil P) R).
  Proof.
    rewrite /forallL. iIntros (Habd) "Hctx".
    iDestruct (Habd with "Hctx") as "HP".
    iIntros (x Hlen). destruct x; last done. iFrame.
  Qed.


  Lemma abduct_forallL_cons A s ss Δ P R:
    (abduct Δ (forallNamed s (λ x: A, forallL ss (λ xr, P (x :: xr)))) R) →
    abduct Δ (forallL (s :: ss) P) R.
  Proof.
    rewrite /forallNamed /forallL. iIntros (Habd) "Hctx".
    iDestruct (Habd with "Hctx") as "HP". iIntros (x Hlen).
    destruct x as [|x xr]; first done. simpl in Hlen; simplify_eq.
    by iSpecialize ("HP" $! x xr with "[//]").
  Qed.



  (* Eventually, we want to revert the context and all the assumptions we have collected. *)
  (** ** Finish *)
  Lemma abduct_revert_assumptions Δ Γ Ω P:
    qenvs_sep_assertions Δ Ω →
    qenvs_pure_assumptions Δ Γ →
    abduct Δ P (nfw Γ Ω P).
  Proof.
    rewrite /abduct /qenvs_sep_assertions /qenvs_pure_assertions /qenvs_pure_assumptions.
    iIntros (Hsep Hpure) "[HΔ Hpre]".
    rewrite Hpure. iDestruct "HΔ" as "[HΓ HΔ]".
    rewrite Hsep. rewrite /nfw. iApply ("Hpre" with "HΓ HΔ").
  Qed.

  Lemma abduct_revert_forall {A: Type} (a: A) Δ P R:
    uni a →
    abduct Δ P (R a) →
    abduct Δ P (∀ a: A, R a).
  Proof.
    rewrite /remove. eapply abduct_forall_elim.
  Qed.

End abduction.


Existing Class uni.
Global Hint Mode uni - + : typeclass_instances.

Existing Class exi.
Global Hint Mode exi - + : typeclass_instances.

Global Typeclasses Opaque
  abduct_atom.

Global Typeclasses Opaque
  existsNamed forallNamed
  existsN existsL forallL.

(* NAMED QUANTIFIERS NOTATION *)
Notation "'∃{' s '}' x , P" := (existsNamed s (λ x, P))
  (at level 200, x binder, right associativity,
   format "'[hv' '∃{' s '}' '/ '  x  , '/ '  P ']'") : bi_scope.
Notation "'∀{' s '}' x , P" := (forallNamed s (λ x, P))
  (at level 200, x binder, right associativity,
  format "'[hv' '∀{' s '}' '/ '  x  , '/ '  P ']'") : bi_scope.


Inductive learn_mode := Assume | AssumeAndRevert | NoAssume.

Definition abduct_pure {Σ: gFunctors} (φ: Prop) (P: iProp Σ) : iProp Σ := ⌜φ⌝ ∗ P.
Definition abduct_class {Σ: gFunctors} (φ: Prop) (P: iProp Σ) : iProp Σ := ⌜φ⌝ ∗ P.
Global Typeclasses Opaque abduct_pure abduct_class.



(* notation that can be imported for proofs *)
Module AbductNotations.

  Declare Custom Entry context.

  Notation "Ctx '-----------------------------------------------goal' Pre" :=
    (abduct Ctx Pre _)
    (at level 100,
     Ctx custom context,
     Pre at level 200,
     left associativity,
     only printing,
     format "'[' Ctx '-----------------------------------------------goal' '//' Pre ']'") : stdpp_scope.

  Notation "'···············································assert' Γ1 '···············································assume' Γ2 '···············································persistent' Ω1 '···············································spatial' Ω2" :=
    (QE Γ1 Γ2 Ω1 Ω2)
    (in custom context at level 5,
      Γ1 custom context,
      Γ2 custom context,
      Ω1 custom context,
      Ω2 custom context,
      right associativity,
      only printing,
      format "'[' '···············································assert' '//' Γ1 '···············································assume' '//' Γ2 '···············································persistent' '//' Ω1 '···············································spatial' '//' Ω2 ']'").

  Notation "P Γ" := (@cons _ P Γ)
    (in custom context at level 3, P constr, Γ custom context, right associativity, format "P '//' Γ", only printing).

  Notation "" := (@nil _)
    (in custom context at level 3, format "", only printing).


  Notation "'IF' φ 'THEN' X1 'ELSE' X2" := (@case (iProp _) φ X1 X2) (at level 60, φ at next level, format "'[' 'IF'  φ  'THEN' '//'   X1 '//' 'ELSE' '//'   X2 ']'", right associativity).
  Notation "'IF' '*' 'THEN' P1 'ELSE' P2" := (bi_and P1 P2) (at level 60, only printing, format "'[' 'IF'  '*'  'THEN' '//'   P1 '//' 'ELSE' '//'   P2 ']'", right associativity).

  Notation "'REQUIRES' Γ Ω 'IN' P" := (nfs Γ Ω P) (at level 60, Γ at next level, Ω at next level, right associativity, format "'[' 'REQUIRES'  '[' Γ  Ω ']'  'IN'  '//' P ']'", only printing).
  Notation "'ENSURES' Γ Ω 'IN' P"  := (nfw Γ Ω P) (at level 60, Γ at next level, Ω at next level, right associativity, format "'[' 'ENSURES'  '[' Γ  Ω ']'  'IN'  '//' P ']'", only printing).
End AbductNotations.
