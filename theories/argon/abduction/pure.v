From quiver.argon.simplification Require Import cleanup.
From quiver.argon.abduction Require Import abduct proofmode.



Class ReplaceUni (φ ψ: Prop) :=
  replace_uni : φ ↔ ψ.
Global Hint Mode ReplaceUni ! - : typeclass_instances.

Lemma replace_uni_refl φ : ReplaceUni φ φ.
Proof. rewrite /ReplaceUni. reflexivity. Qed.


Section pure_abduction_rules.
  Context {Σ: gFunctors}.
  Implicit Types (Δ: qenvs Σ).

  (* abduct pure *)
  Lemma abduct_pure_prove Δ P R φ:
    Δ ⊨ Solve φ →
    abduct Δ P R →
    abduct Δ (abduct_pure φ P) R.
  Proof.
    rewrite /abduct_pure /Solve.
    eapply abduct_prove_pure.
  Qed.

  Lemma abduct_pure_conj Δ P R  ψ φ:
    abduct Δ (abduct_pure φ (abduct_pure ψ P)) R →
    abduct Δ (abduct_pure (φ ∧ ψ) P) R.
  Proof.
    rewrite /abduct_pure.
    iIntros (Habd) "Hctx".
    iPoseProof (Habd with "Hctx") as "(% & % & HP)".
    iSplit; done.
  Qed.

  Lemma abduct_pure_missing Δ Δ' P R (φ: Prop):
    qenvs_add_pure_assertion Δ φ Δ' →
    abduct Δ' P R →
    abduct Δ (abduct_pure φ P) (⌜φ⌝ ∗ R).
  Proof.
    rewrite /abduct_pure. by eapply abduct_assert_pure.
  Qed.

  Lemma abduct_pure_simplify_pure Δ (φ ψ: Prop) (P: iProp Σ) R:
    Δ ⊨ simplify_pure true φ ψ →
    abduct Δ (abduct_pure ψ P) R →
    abduct Δ (abduct_pure φ P) R.
  Proof.
    rewrite /abduct_pure.
    intros Hent Habd. apply: abduct_qenvs_entails=> Hsimpl.
    iIntros "Hctx". iDestruct (Habd with "Hctx") as "[%Hφ Hx]".
    iFrame. iPureIntro. by eapply Hsimpl.
  Qed.

  Lemma abduct_pure_replace_uni Δ (φ ψ: Prop) (P: iProp Σ) R:
    Δ ⊨ ReplaceUni φ ψ →
    abduct Δ (abduct_pure ψ P) R →
    abduct Δ (abduct_pure φ P) R.
  Proof.
    rewrite /abduct_pure /ReplaceUni.
    intros Hent Habd. apply: abduct_qenvs_entails=> Hsimpl.
    iIntros "Hctx". iDestruct (Habd with "Hctx") as "[%Hφ Hx]".
    iFrame. iPureIntro. rewrite Hsimpl //.
  Qed.

  (* abduct class *)
  Lemma abduct_pure_class Δ φ P R:
    abduct Δ (abduct_pure φ P) R → abduct Δ (abduct_class φ P) R.
  Proof. done. Qed.

  Lemma abduct_class_prove Δ (φ: Prop) P R:
    Δ ⊨ φ →
    abduct Δ P R →
    abduct Δ (abduct_class φ P) R.
  Proof. intros ?; by eapply abduct_pure_prove. Qed.

  (* pure abduction *)
  Global Instance abduct_step_pure_prove φ Δ P R :
    Δ ⊨ Solve φ →
    AbductStep (abduct Δ P R)
      Δ (abduct_pure φ P) R | 2.
  Proof. intros ??. by apply abduct_pure_prove. Qed.

  Global Instance abduct_step_pure_missing (φ: Prop) Δ Δ' P R :
  qenvs_add_pure_assertion Δ φ Δ' →
    AbductStep (abduct Δ' P R)
      Δ (abduct_pure φ P) (⌜φ⌝ ∗ R) | 10.
  Proof. intros ??. by eapply abduct_pure_missing. Qed.

  Global Instance abduct_step_pure_conj φ ψ Δ P R :
    AbductStep (abduct Δ (abduct_pure φ (abduct_pure ψ P)) R) Δ (abduct_pure (φ ∧ ψ) P) R.
  Proof. intros ?. by apply abduct_pure_conj. Qed.

  Global Instance abduct_step_start_pure Δ R φ P:
    AbductStep (abduct Δ (abduct_pure φ P) R) Δ (⌜φ⌝ ∗ P) R.
  Proof. rewrite /abduct_pure. by intros ?. Qed.

  Global Instance abduct_step_pure_simplify_pure Δ (φ ψ: Prop) (P: iProp Σ) R :
    _ → AbductStep _ _ _ _ | 90 := abduct_pure_simplify_pure Δ φ ψ  P R.

  Global Instance abduct_step_pure_replace_uni Δ (φ ψ: Prop) (P: iProp Σ) R :
    _ → AbductStep _ _ _ _ | 100 := abduct_pure_replace_uni Δ φ ψ  P R.

  (* abduct class *)
  Global Instance abduct_step_class_pure Δ R φ P:
    AbductStep (abduct Δ (abduct_pure φ P) R) Δ (abduct_class φ P) R | 20.
  Proof. rewrite /abduct_pure /abduct_class; by intros ?. Qed.

  Global Instance abduct_step_class_pure_inst Δ R (φ: Prop) P:
    Δ ⊨ φ →
    AbductStep (abduct Δ P R) Δ (abduct_class φ P) R | 1.
  Proof. rewrite /abduct_class. intros ??. eapply abduct_step_pure_prove; done. Qed.

End pure_abduction_rules.

(* additional solve instances *)
Section solve_instances.
  Context {Σ: gFunctors}.

  Global Instance qenvs_entails_double_negation (Ω1 Ω2 : list (iProp Σ)) φ :
    once_tc (QE nil nil Ω1 Ω2 ⊨ Solve (φ)) →
    QE nil nil Ω1 Ω2 ⊨ Solve (¬ ¬ φ) | 1.
  Proof.
    rewrite /once_tc/qenvs_entails/Solve => HQ. iIntros "Hx". iDestruct (HQ with "Hx") as %?. by eauto.
  Qed.
End solve_instances.




(* replace universal quantifiers *)
Ltac syn_is_in x X :=
  lazymatch X with context [x] => idtac end.

Ltac no_uni X :=
  match goal with
  | H: uni ?x |- _ => syn_is_in x X; fail 1
  | _ => idtac
  end.

Ltac replace_uni_subst x Heq a b :=
  lazymatch a with context [x] => no_uni b; rewrite Heq; apply replace_uni_refl end.

Ltac replace_uni_core x :=
  clear_maps;
  simpl in *;
  match goal with
  | H: context [x] |- _ =>
    lazymatch type of H with (?l = ?r) =>
      first [
        replace_uni_subst x H l r
      | replace_uni_subst x (symmetry H) r l
      ]
    end
  end.

Ltac replace_uni :=
  lazymatch goal with
  | |- ReplaceUni ?φ ?ψ =>
    lazymatch goal with
    | H: uni ?x |- context [?x] => replace_uni_core x
    end
  end.

Global Hint Extern 1 (ReplaceUni _ _) => replace_uni : typeclass_instances.

