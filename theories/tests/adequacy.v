From quiver.tests.code Require Import adequacy.
From quiver.thorium.logic Require Import adequacy.
From quiver.inference Require Import inferPT.

From iris.program_logic Require Export language. (* must be last to get the correct nsteps *)

Import AbductNotations.
Section spec_inference.
  Context `{!refinedcG Σ}.

  (* Typing inference for [inc]. *)
  Lemma type_inc :
    qenvs_empty ⊨ impl_inc  : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "inc: "; print_loud.
  Defined.

  Definition inc_pt := pt_of (type_inc ).

  Lemma impl_inc_has_type_inc_pt :
    qenvs_empty ⊨ impl_inc  : inc_pt.
  Proof.
    proof_from_well_typed (type_inc ).
  Qed.


  (* Typing inference for [main]. *)
  Lemma type_main (global_inc : loc) :
    QE nil nil [global_inc ◁ᵥ fnT [(it_layout i32)] inc_pt] nil ⊨ impl_main  global_inc : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "main: "; print_loud.
  Defined.

  Definition main_pt := pt_of (type_main  inhabitant).

  Lemma impl_main_has_type_main_pt (global_inc : loc) :
    QE nil nil [global_inc ◁ᵥ fnT [(it_layout i32)] inc_pt] nil ⊨ impl_main  global_inc : main_pt.
  Proof.
    proof_from_well_typed (type_main  global_inc).
  Qed.

End spec_inference.


Section linking.
  Context `{!refinedcG Σ}.

  Context (global_inc : loc).

  (* we link the functions [inc] and [main],
     turning the assumption in the context of [main]
     into merely an assumption about [inc] being allocated.
  *)
  Lemma linking_inc_main:
    (QE nil nil [fntbl_entry global_inc impl_inc] nil)
  ⊨ impl_main global_inc : main_pt.
  Proof.
    (* collect [fntbl_entry] assumptions about the functions *)
    iIntros "#Hctx". rewrite !qenvs_ctx_cons_pers_assertion qenvs_ctx_empty.
    iDestruct "Hctx" as "(#Hinc & _)".

    (* we apply the typing for [main], namely [impl_main_has_type_main_pt],
       leaving us to deal with the assumptions in the context, i.e.,
       [global_inc ◁ᵥ fnT [(it_layout i32)] inc_pt]*)
    iModIntro. iIntros (Φ lsa lsv) "Hpre".
    iApply impl_main_has_type_main_pt; last done.
    rewrite !qenvs_ctx_cons_pers_assertion qenvs_ctx_empty.
    iSplit; last done.

    (* to prove that [global_inc] has the inferred type, we use [impl_inc_has_type_inc_pt]: *)
    iModIntro. iApply (fnT_intro global_inc impl_inc).
    iFrame "Hinc". (* taking care of the function table entry *)
    iApply impl_inc_has_type_inc_pt.
    (* [inc] has an empty context *)
    iModIntro. rewrite qenvs_ctx_empty //.
  Qed.

  Lemma main_linked_function_type (main: loc):
    (fntbl_entry global_inc impl_inc) ∗
    (fntbl_entry main (impl_main global_inc)) ⊢
    main ◁ᵥ fnT [] main_pt.
  Proof.
    iIntros "#[Hinc Hmain]". iApply (fnT_intro _ (impl_main global_inc)).
    iFrame "Hmain". iNext. iApply linking_inc_main.
    rewrite !qenvs_ctx_cons_pers_assertion qenvs_ctx_empty.
    by iFrame "#".
  Qed.


  (* for mutally recursive functions, we can link them using Löb induction,
     similarly for recursive functions  *)
  Lemma linking_mutual (f g: loc) fn_f fn_g pt_f pt_g:
    let f_lys := (fn_f f g).(f_args).*2 in
    let g_lys := (fn_g g f).(f_args).*2 in
    QE nil nil [f ◁ᵥ fnT f_lys pt_f] nil ⊨ fn_g g f : pt_g →
    QE nil nil [g ◁ᵥ fnT g_lys pt_g] nil ⊨ fn_f f g : pt_f →
    QE nil nil [fntbl_entry f (fn_f f g); fntbl_entry g (fn_g g f)] nil ⊨ fn_f f g : pt_f.
  Proof.
    intros f_lys g_lys Hg Hf.
    iIntros "#Hctx". rewrite !qenvs_ctx_cons_pers_assertion qenvs_ctx_empty.
    iDestruct "Hctx" as "(#Hf & #Hg & _)".

    iLöb as "IHf".
    iModIntro. iIntros (Φ lsa lsv) "Hpre".
    iApply Hf; last done.
    rewrite !qenvs_ctx_cons_pers_assertion qenvs_ctx_empty.
    iSplit; last done.
    iModIntro. iApply (fnT_intro g (fn_g g f)).
    iFrame "Hg". iNext. iApply Hg.
    rewrite !qenvs_ctx_cons_pers_assertion qenvs_ctx_empty.
    iSplit; last done. iModIntro.
    iApply (fnT_intro f (fn_f f g)).
    iFrame "Hf". iNext. iFrame "IHf".
  Qed.


End linking.



Section adequacy.

  (* initial addresses of the inc an main function *)
  Context (addr_inc addr_main : addr).

  Let function_addrs := [addr_inc; addr_main].
  Let functions := [impl_inc; impl_main (fn_loc addr_inc)].

  Lemma inc_main_adequate n κs t2 σ2 σ:
    σ = {| st_heap := initial_heap_state; st_fntbl := fn_lists_to_fns function_addrs functions; |} →
    NoDup function_addrs →
    nsteps (Λ := c_lang) n (initial_prog <$> [(fn_loc addr_main)], σ) κs (t2, σ2) →
    ∀ e2, e2 ∈ t2 → not_stuck e2 σ2.
  Proof.
    move => -> HNDfns.

    set Σ : gFunctors := #[quiverΣ].
    apply: (refinedc_adequacy Σ) => //; first apply AllocNewBlock_nil.
    iIntros (Hrc) "_ Hfn".

    have Hin := I.
    repeat (
        move: {Hin} HNDfns => /NoDup_cons[Hin HNDfns];
        iDestruct (fn_lists_to_fns_cons with "Hfn") as "[#? Hfn]" => //
    ). iClear "Hfn".

    iModIntro; simpl. iSplit; last done.
    iExists _. iSplit.
    - iApply main_linked_function_type. by iFrame "#".
    - rewrite /main_pt. done.
  Qed.

End adequacy.


(* Print Assumptions inc_main_adequate. *)