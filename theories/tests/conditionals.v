From quiver.tests.code Require Import conditionals.
From quiver.inference Require Import inferPT.
From quiver.thorium.types Require Import types functions.
From quiver.thorium.typing Require Import garbage_collection controlflow.
Set Default Proof Using "Type".

From quiver.argon.abduction Require Import joined cases.
From quiver.thorium.join Require Import symmetric_join.
From quiver.argon.simplification Require Import normalize.

Import AbductNotations.
Global Remove Hints abduct_step_abort : typeclass_instances.
Set Default Goal Selector "1".


Section branch_idea.
  Context {Σ: gFunctors} `{!refinedcG Σ}.

  Local Existing Instance FancyIf.

  Import AbductNotations.
  Lemma type_symmetric_conditional :
    qenvs_empty ⊨ impl_symmetric_conditional : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test symmetric conditional"; print_loud.
  Qed.

  Lemma type_indirect_conditional :
    qenvs_empty ⊨ impl_indirect_conditional : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test indirect conditional "; print_loud.
  Qed.

  Lemma type_indirect_conditional2 :
    qenvs_empty ⊨ impl_indirect_conditional2 : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test indirect conditional two "; print_loud.
  Qed.

  Lemma type_indirect_conditional_ptr :
    qenvs_empty ⊨ impl_indirect_conditional_ptr : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test indirect conditional ptr "; print_loud.
  Qed.

  Lemma type_indirect_conditional_nested_ptr :
    qenvs_empty ⊨ impl_indirect_conditional_nested_ptr : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test indirect conditional nested ptr "; print_loud.
  Qed.


  Lemma type_conditional_set_zero :
    qenvs_empty ⊨ impl_conditional_set_zero : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test conditional set zero "; print_loud.
  Qed.


  Lemma type_min3_inline :
    qenvs_empty ⊨ impl_min3_inline : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test min3 inline "; print_loud.
  Qed.

  Lemma type_min_inline :
    qenvs_empty ⊨ impl_min_inline : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test min inline "; print_loud.
  Qed.

  Lemma type_assert_positive:
    qenvs_empty ⊨ impl_assert_positive: ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test assert positive "; print_loud.
  Qed.

  Lemma type_assert_all_positive:
    qenvs_empty ⊨ impl_assert_all_positive : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test assert all positive "; print_loud.
  Qed.

  Lemma type_assert_not_null:
    qenvs_empty ⊨ impl_assert_not_null : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test assert not null "; print_loud.
  Qed.

  Lemma type_modify_if_not_null :
    qenvs_empty ⊨ impl_modify_if_not_null : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test modify if not null "; print_loud.
  Qed.

  Lemma type_assert_one_positive:
    qenvs_empty ⊨ impl_assert_one_positive : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test assert one positive "; print_loud.
  Qed.

  Lemma type_assert_all_same_sign:
    qenvs_empty ⊨ impl_assert_all_same_sign : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test assert all same sign "; print_loud.
  Qed.

  (* Remove Hints FancyIf : typeclass_instances. *)
  Lemma type_arith_with_flag_res:
    qenvs_empty ⊨ impl_arith_with_flag_res : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test arith with flag res "; print_loud.
  Qed.

  Lemma type_min :
    qenvs_empty ⊨ impl_min : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test min "; print_loud.
  Defined.

  Definition min_pt := pt_of (type_min).
  Lemma impl_min_has_type_min_pt:
    qenvs_empty ⊨ impl_min : min_pt.
  Proof. proof_from_well_typed (type_min). Qed.


  Lemma type_min3 (min: loc):
    (QE nil nil [min ◁ᵥ fnT [i32: layout; i32: layout] min_pt ] nil) ⊨ impl_min3 min : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "test min3 "; print_loud.
  Qed.

End branch_idea.
