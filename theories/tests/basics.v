From quiver.tests.code Require Import basics.
From quiver.inference Require Import inferPT.
From quiver.thorium.types Require Import types functions.
From caesium Require Import builtins_specs.
From quiver.thorium.types Require Import base_types combinators pointers structs.

Set Default Proof Using "Type".
Import AbductNotations.
Global Remove Hints abduct_step_abort : typeclass_instances.
Set Default Goal Selector "1".
Open Scope ref_type_scope.

Section examples.
  Context `{!refinedcG Σ}.

  Lemma test_simplification x (T: Z → iProp Σ):
    ∃ R, simplify (⌜0 ≤ x⌝ ∗ (∃ y, ⌜(4 * x = 4 * y)%Z⌝ ∗ T y) ) R ∧ print R.
  Proof.
    eexists. split.
    - normalize. cleanup. normalize.
      (* we additionally normalize, because prune_exists expects normalized input *)
      prune_exists. eapply simplify_refl.
    - print_silent.
  Qed.




  Lemma dup_well_typed :
    qenvs_empty ⊨ impl_dup : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "dup : "; print_loud.
  Qed.

  Lemma id_well_typed :
    qenvs_empty ⊨ impl_id : ?.
  Proof.
    pose_hint "pre" (V{{ "x" }} x, ∃? A, x ◁ᵥ A)%I.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "id : "; print_loud.
  Qed.


  (* SIMPLE POINTER MANIPULATION *)
  Lemma swap_well_typed :
    qenvs_empty ⊨ impl_swap : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "swap : "; print_loud.
  Defined.

  Definition swap_pt := pt_of swap_well_typed.
  Lemma impl_swap_has_type_swap_pt :
    qenvs_empty ⊨ impl_swap : swap_pt.
  Proof.
    proof_from_well_typed swap_well_typed.
  Qed.

  Lemma overwrite_well_typed :
    qenvs_empty ⊨ impl_overwrite: ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "overwrite : "; print_loud.
  Qed.

  Lemma deref_well_typed :
    qenvs_empty ⊨ impl_deref : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "deref : "; print_loud.
  Qed.

  Lemma swap2_well_typed :
    qenvs_empty ⊨ impl_swap2 : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "swap2 : "; print_loud.
  Qed.

  (* SIMPLE ARITHMETIC *)
  Lemma times_two_well_typed :
    qenvs_empty ⊨ impl_times_two : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "times two : "; print_loud.
  Qed.

  Lemma arith_well_typed :
    qenvs_empty ⊨ impl_arith : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "arith : "; print_loud.
  Qed.

  (* FUNCTION CALLS *)

  (* FORWARDING *)
  (* We obtain an existentially quantified variable from one function,
     and then we forward it to another function.
     To add a small twist, we tweak the postcondition
     to make forwarding harder. *)
    Definition nat_fun_ft : fn_type := FT _ $
    λ n,
    {|
      fp_atys := [intT i32 n];
      fp_Pa := ⌜n > 0⌝%I;
      fp_rtype := Z;
      fp_fr := λ m, FR (intT i32 m) (⌜0 < m⌝);
    |}.

  Lemma forwarding_well_typed (f g : loc):
    (QE nil nil [f ◁ᵥ functionT nat_fun_ft; g ◁ᵥ functionT nat_fun_ft] nil) ⊨ impl_forwarding f g : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "forwarding : "; print_loud.
  Qed.

  (* USING SWAP *)
  Lemma use_swap_well_typed (swap : loc):
    (QE nil nil [swap ◁ᵥ fnT [void*; void*] swap_pt] nil) ⊨ impl_use_swap swap : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "use swap : "; print_loud.
  Qed.

  (* SIMPLE POINTER CHECKS *)
  Lemma is_none_check_well_typed :
    qenvs_empty ⊨ impl_is_none_check : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "is none check : "; print_loud.
  Qed.

  Lemma is_some_check_well_typed :
    qenvs_empty ⊨ impl_is_some_check : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "is some check : "; print_loud.
  Qed.

  Lemma is_some_well_typed :
    qenvs_empty ⊨ impl_is_some : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "is some : "; print_loud.
  Qed.

  (* ASSIGNMENT IF NOT NULL *)
  Lemma cond_assign_well_typed :
    qenvs_empty ⊨ impl_cond_assign : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "cond assign : "; print_loud.
  Qed.

End examples.
