From lithium Require Import hooks simpl_classes simpl_instances normalize.
From refinedc.typing Require Import naive_simpl.
From quiver.tests.code Require Import simple_loop.
From quiver.thorium.typing Require Import typing assume_type.
From quiver.inference Require Import inferPT.

Set Default Proof Using "Type*".
Set Default Goal Selector "1".
Global Remove Hints abduct_step_abort : typeclass_instances.
Import AbductNotations.


Section loop_invariant_examples.
  Context `{!refinedcG Σ}.


  Lemma type_ten :
    qenvs_empty ⊨ impl_ten : ?.
  Proof.
    pose_hint "__loop_inv_0" (L{{"k"}} k, ∃ n, k ◁ₗ intT i32 n ∗ ⌜n ≤ 10⌝)%I.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "ten: "; print_loud.
  Qed.


  Lemma type_up_to_arg_loc :
    qenvs_empty ⊨ impl_up_to_arg : ?.
  Proof.
    pose_hint "__loop_inv_0" (L{{"k"}} k, L{{"m"}} m, ∃ n n', k ◁ₗ intT i32 n ∗ m ◁ₗ intT i32 n' ∗ ⌜n ≤ n'⌝)%I.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "up_to_arg: "; print_loud.
  Qed.

  Lemma type_up_to_arg :
    qenvs_empty ⊨ impl_up_to_arg : ?.
  Proof.
    pose_hint "pre" (V{{"m"}} m, ∃{{"n"}} n, m ◁ᵥ intT i32 n)%I.
    pose_hint "__loop_inv_0" (L{{"k"}} k, V{{"m"}} m, ∀{{"n"}} n, ∃ n', k ◁ₗ intT i32 n' ∗ m ◁ᵥ intT i32 n ∗ ⌜n' ≤ n⌝)%I.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "up_to_arg: "; print_loud.
  Qed.


  Lemma type_ten_with_frame :
    qenvs_empty ⊨ impl_ten_with_frame : ?.
  Proof.
    pose_hint "__loop_inv_0" (L{{"k"}} k, ∃ n, k ◁ₗ intT i32 n ∗ ⌜n ≤ 10⌝)%I.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "ten_with_frame: "; print_loud.
  Qed.


  Lemma type_plus_ten :
    qenvs_empty ⊨ impl_plus_ten : ?.
  Proof.
    pose_hint "__loop_inv_0" (L{{"k"}} k, ∃ n, k ◁ₗ intT i32 n ∗ ⌜n ≤ 10⌝)%I.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "plus_ten: "; print_loud.
  Qed.


End loop_invariant_examples.
