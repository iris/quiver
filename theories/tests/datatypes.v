From quiver.tests.code Require Import datatypes.

From quiver.thorium.typing Require Import typing garbage_collection.
From quiver.inference Require Import inferPT.
From quiver.thorium.types Require Import types functions structs.
Set Default Proof Using "Type*".

Import AbductNotations.
Set Default Goal Selector "1".
Global Remove Hints abduct_step_abort : typeclass_instances.
Open Scope ref_type_scope.

Global Remove Hints type_progress_default : typeclass_instances.

Section examples.
  Context `{!refinedcG Σ}.


  Lemma type_deref_x :
    qenvs_empty ⊨ impl_deref_x : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - print_silent.
  Qed.

  Lemma type_get_x :
    qenvs_empty ⊨ impl_get_x : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - print_silent.
  Qed.

  Lemma type_set_x :
    qenvs_empty ⊨ impl_set_x : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - print_silent.
  Qed.

  Lemma type_get_foo :
    qenvs_empty ⊨ impl_get_foo : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - print_silent.
  Qed.

  Lemma type_get_next :
    qenvs_empty ⊨ impl_get_next : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - print_silent.
  Qed.


End examples.