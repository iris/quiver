From quiver.tests.code Require Import mkvec.
From quiver.thorium.typing Require Import typing.
From quiver.inference Require Import inferPT.
From quiver.thorium.types Require Import types functions.
From quiver.stdlib Require Import malloc.
Set Printing Depth 1000000.
Set Printing Width 400.
Set Default Proof Using "Type*".


(* Understanding the Quiver automation
-----------------------------------------

To infer a specification for a function `func`, the frontend generates a Lemma of the form:
```
  Lemma type_func  :
    QE nil nil [...] nil ⊨ impl_func : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "func: "; print_loud.
  Defined.
```
where the context contains the specifications of functions that are called inside `func`.
If we have added sketches, the generated code additionally contains a Coq version of the sketches (see, e.g., `examples/src/casestudies/arrays/proofs/array_annotated/generated_proof_mkvec.v`).

The purpose of lemmas like this is to (1) infer a specification and, at the same time, (2) prove that the inferred specification is correct.
(That's why the lemma ends with `Defined`; it consists of a predicate transformer `T` a proof that `func` implements `T`.)

The proof of the Lemma consists of four steps:
1. The tactic `inferF.` sets up the proof environment.
   It results in a goal of the form `abduct Δ P R ` where `R` is an evar (i.e. a unification variable), the precondition to be inferred.
   We will take a closer look at such a goal in an example below.
2. The tactic `abduct.` executes the abduction engine Argon to infer a specification (i.e., to complete `R`).
3. The tactic `simplify_passes.` applies simplification to the resulting predicate transformer.
4. The tactic `idtac "func: "; print_loud.` prints out the result of the inference.

The most interesting of these steps is the abduction engine Argon (i.e., the call to `abduct`).
It works by repeatedly applying abduction rules of the following shape:
```
AbductStep φ Δ P R
```
where `Δ` is the current context, `P` is the current goal, and `R` is the evar-precondition to be inferred.
The argument `φ` is the next goal to prove. It's best to think of `φ` as the premise of an inference rule.
In particular, semantically `AbductStep φ Δ P R ↔ (φ → abduct Δ P R)`.
For example, the rule for `abd_exists`,
```
Global Instance abduct_step_exists Δ X P R:
    AbductStep (∀! x: X, exi x → abduct Δ (P x) (R x)) Δ (∃ x: X, P x) (∃ x: X, R x) | 10.
Proof. intros Habd. by eapply abduct_exists. Qed.
```
says that if we want to deal with `∃ x: X, P x` in the goal, we should unify the precondition with `∃ x: X, R x` (i.e., `R_old := ∃ x, R_new x` for some `R_new`),
and then we should proceed to infer `R_new` by solving `∀! x: X, exi x → abduct Δ (P x) (R x)`.
(Here, `∀!` means `∀` and `exi x` is used to mark `x` as an existentially quantified variable.)

To determine which rule to apply, the abduction engine matches on the goal.
The way this works is that `AbductStep` is a type class, and we use type class inference to find the right instantiation. *)


(* To illustrate how all of these steps work concretely, we consider an example below: the function [mkvec]. *)


Import AbductNotations.
Section inference.
  Context `{!refinedcG Σ}.
  Context `{!lib_malloc Σ}.


  (* To make this example self-contained, we inline the inferred
     specifications for [xmalloc] and [xzalloc]. *)
  Definition xmalloc_pt : pt Σ := λ (ws : list val) (ret : type Σ → iProp Σ),
    (∃ v_n : val, ⌜ws = [v_n]⌝ ∗
    (∃ n : Z, nfs [] [v_n ◁ᵥ (int[size_t] n)] $  (* REQUIRES [] [x ◁ᵥ (int[size_t] x0)] IN *)
    (∀ l : loc, nfw [] [block l n] $             (* ENSURES [] [block x1 x0]  IN *)
                ret (owned l (any[n])))))%I.

  Definition xzalloc_pt : pt Σ := λ (ws : list val) (ret : type Σ → iProp Σ),
    (∃ v_n : val, ⌜ws = [v_n]⌝ ∗
    (∃ n : Z, nfs [] [v_n ◁ᵥ (int[size_t] n)] $  (* REQUIRES [] [x ◁ᵥ (int[size_t] x0)] IN *)
    (∀ l : loc, nfw [] [block l n] $             (* ENSURES [] [block x1 x0]  IN *)
                ret (owned l (zeros[n])))))%I.

  (* We consider the function [mkvec] and infer a bare-bones specification (mem) for it.
     We infer a specification for it in three versions:
       (1) we infer the specification in one go, as is done in the implementation;
       (2) we gradually step through the inference to highlight key points;
       (3) we modify one of the rules of Quiver and see how it affects the inference.
  *)


  (* VERSION ONE: we do a complete inference of the bare-bones [mkvec] spec *)
  Lemma type_mkvec_v1 (global_xmalloc global_xzalloc : loc) :
    QE nil nil [global_xmalloc ◁ᵥ fnT [(it_layout size_t)] xmalloc_pt; global_xzalloc ◁ᵥ fnT [(it_layout size_t)] xzalloc_pt] nil ⊨ impl_mkvec  global_xmalloc global_xzalloc : ?.
  Proof.
    inferF.
    - (* To understand what is going on inside of an abduction, we have AbductNotations that pretty-print the context.
         In this case, we have assumptions for the functions [xmalloc] and [xzalloc].
         Moreover, we have the assumptions
          (1) `vec_var ◁ₗ (any[ly_size void*])` for the local variable `vec`, which is initially uninitialized;
          (2) `s_var ◁ₗ (any[ly_size size_t])` for the local variable `s`, which is initially uninitialized; and
          (3) `n_arg ◁ₗ (value[ly_size i32] n_val)` for the argument variable `n_arg`, which initially stores the argument value `n_val`.
      *)
      abduct.
    - (* After running `abduct`, we have inferred a predicate transformer, which we now simplify. *)
      simplify_passes.
    - (* now we have inferred the simplified predicate, and we print it out. *)
      idtac "mkvec: "; print_loud.
  Defined.

  (* VERSION TWO: We gradually step through [mkvec]. *)
  (* Having seen how the inference proceeds in one go, we now gradually step through the example to understand the inference a little better. *)
  Lemma type_mkvec_v2 (global_xmalloc global_xzalloc : loc) :
    QE nil nil [global_xmalloc ◁ᵥ fnT [(it_layout size_t)] xmalloc_pt; global_xzalloc ◁ᵥ fnT [(it_layout size_t)] xzalloc_pt] nil ⊨ impl_mkvec  global_xmalloc global_xzalloc : ?.
  Proof.
    inferF.
    - (* We can execute _individual_ abduction steps with the tactic [abduct_step].
         Let us execute the first couple of steps. *)
      abduct_step. (* there is no sketch, so we skip [type_annot] *)
      abduct_step. (* we turn the weakest precondition for (Goto #0) into a goal type_goto *)
      abduct_step. (* we jump to the first label in the function, #0, and insert its body into the weakest precondition *)
      abduct_step. (* we break up the outermost assignment *)
      abduct_step. (* we break up the multiplication *)
      abduct_step. (* we type the constant using [type_val] *)
      abduct_step. (* we use a typing rule for integer constants *)
      do 10 abduct_step. (* we proceed further in the abduction *)
      (* at this point, we want to type the value [n_val] *)
      (* it is missing, so we turn the goal into [∃ A, n_val ◁ᵥ A] and proceed *)
      abduct_step.
      do 6 abduct_step.
      (* at this point, we reach the type cast and now need to determine the type of [n_val] *)
      (* we use a rule for type casting, [abduct_step_type_un_op] *)
      abduct_step.
      (* as part of the remaining inference, we will soon add as a constraint that the type of  [n_val] should be an integer. *)
      abduct_branch; abduct_step. (* this command skips several steps of abduction in between *)
      (* now the goal is essentially `∃ n, A `is_ty` int[i32] n` where [A] is the type of [n_val] from above. *)
      (* it is gradually lifted to the precondition with *)
      abduct_step.
      abduct_step.
      abduct_step.
      abduct_step.
      abduct_step.
      idtac.
      (* At this point, the constraint has been added to the precondition. *)
      (* We complete the remainder by calling [abduct]. *)
      abduct.
    - simpl.
      (*  Note the constraint on the type of [n_val]:
            we first have the typing assignment [n_val ◁ᵥ x] for some type [x] and then, additionally,
            the subtyping constraint `x `is_ty` (int[i32] x0)` for some integer [x0].
          Simplification will now remove this indirection. *)
      simplify_passes.
    - (* now we have inferred the simplified predicate, and we print it out. *)
      idtac "mkvec: "; print_loud.
  Defined.



  (* STEP THREE: we modify one of the abduction rules and re-run the inference. *)
  (* Lastly, let us modify the inference a little. As mentioned in the overview,
     the abduction procedure [abduct] applies lemmas of the form [AbductStep φ Δ P R].
     To modify the inference, we can add or remove AbductStep instances.
     To illustrate this point, we remove the general rule for unary operators
     and replace it with our own. *)


  (*  The existing rule for unary operators is a generalization of the rule presented in the appendix:

      Global Instance abduct_step_un_op_force_rule_copy Δ φ op ot v A B R Φ:
        un_op_typing_rule φ op ot A B →
        AbductStep (abduct Δ (abd_assume CTX (v ◁ᵥ A) $ abduct_pure φ (∀ w, Φ w B)) R) Δ (type_un_op_force op ot v A Φ) R | 2.
      Proof. intros ??; by eapply abduct_un_op_force_rule_copy. Qed. *)
  (* We remove the instance to ensure that the abduction engine does not use it. *)
  Remove Hints abduct_step_un_op_force_rule_copy : typeclass_instances.

  (* Instead, we provide a new instance for casting an i32 to a size_t integer: *)
  Global Instance abduct_step_cast_int_sizet Δ v R Φ n:
    AbductStep (abduct Δ (abduct_pure (n ∈ size_t) (∀ w, Φ w (intT size_t n))) R) Δ (type_un_op_force (CastOp (IntOp size_t)) (IntOp i32) v (intT i32 n) Φ) R | 2.
  Proof.
    rewrite /abduct_pure. iIntros (Habd) "Ctx". iDestruct (Habd with "Ctx") as "[%Hsz Hpost]".
    destruct (un_op_typing_rule_cast_int_int i32 size_t n) as [_ Hx]. (* we build on an existing type casting rule *)
    rewrite -Hx //.
  Qed.
  (* This instance adds a different precondition than the standard one: it imposes the precondition that `n ∈ size_t`,
     while technically already `0 ≤ n` would suffice, since nonnegative i32-integers always fit into size_t. *)

  (* We rerun the inference with the modified rule. *)
  Lemma type_mkvec_v3 (global_xmalloc global_xzalloc : loc) :
    QE nil nil [global_xmalloc ◁ᵥ fnT [(it_layout size_t)] xmalloc_pt; global_xzalloc ◁ᵥ fnT [(it_layout size_t)] xzalloc_pt] nil ⊨ impl_mkvec  global_xmalloc global_xzalloc : ?.
  Proof.
    inferF.
    - abduct.
    - simpl.
      (* note the addition of the constraint [x0 ∈ size_t] here. *)
      simplify_passes.
    - (* our modified inference rule leads to a different precondition now *)
      idtac "mkvec: "; print_loud.
  Defined.

  (* We encourage the reader to try other preconditions (without finishing the proof of [abduct_step_cast_int_sizet]).
     For example, one could try out the strictly stronger constraint [n = 0] in [abduct_step_cast_int_sizet]. *)
End inference.