From quiver.tests.code Require Import pred_annotations.
From quiver.inference Require Import inferPT.
From quiver.thorium.types Require Import types functions.
From quiver.argon.simplification Require Import cleanup elim_existentials postcond_passes.
From quiver.thorium.typing Require Import garbage_collection controlflow.
Set Default Proof Using "Type".

(* debug mode *)
Set Default Goal Selector "1".
Global Remove Hints abduct_step_abort : typeclass_instances.
Import AbductNotations.
Global Remove Hints type_progress_default : typeclass_instances.


(** * Exploring annotations for type system states *)
Section experiments.
  Context {Σ: gFunctors} `{!refinedcG Σ}.

  Lemma type_fun :
    qenvs_empty ⊨ impl_fun : ?.
  Proof.
    pose_hint "__hint_1" (L{{ "a" }} a : loc, ∀{{ "p" }} p : loc, ∃? __0, (a ◁ₗ ownT p (intT i32 __0)))%I.
    pose_hint "__hint_0" (L{{ "a" }} a : loc, ∃{{ "p" }} p : loc, ∃? __0, (a ◁ₗ ownT p (intT i32 __0)))%I.
    inferF.
    - abduct.
    - normalize. prune_exists. cleanup. normalize. unfold_type_post. simplify.
    - print_silent.
  Defined.

  Lemma type_fun2 :
    qenvs_empty ⊨ impl_fun : ?.
  Proof.
    (*pose_hint "__hint_1" (L{{ "a" }} a : loc, ∀{{ "p" }} p : loc, ∃? __0, (a ◁ₗ ownT p (intT i32 __0)))%I.*)
    (*pose_hint "__hint_0" (L{{ "a" }} a : loc, ∃{{ "p" }} p : loc, ∃? __0, (a ◁ₗ ownT p (intT i32 __0)))%I.*)
    (*pose_expr_hint "__hint_2" (L[{ "a" }] a : loc, ∃[{ "p" }] p : loc, ∃?e __0, EHintBase (ownT p (intT i32 __0)) (True))%I.*)

    pose_hint "__hint_0" (L{{ "a" }} a : loc, ∃{{ "p" }} p : loc, ∃? __0, (a ◁ₗ ownT p (intT i32 __0)))%I.
    pose_expr_hint "__hint_1" (L[{ "a" }] a : loc, ∀[{ "p" }] p : loc, ∃?e __0, EHintBase (ownT (p) (intT i32 __0)) True)%I.

  (*pose_hint "__hint_0" (L{{ "a" }} a : loc, ∃{{ "p" }} p : loc, ∃? __0, (a ◁ₗ ownT p (intT i32 __0)))%I.*)
  (*pose_expr_hint "__hint_1" (L[{ "a" }] a : loc, ∀[{ "p" }] p : loc, EHintBase (ownT (p) (intT i32 ?)) True)%I.*)

    inferF.
    - abduct.
    - normalize. prune_exists. cleanup. normalize. unfold_type_post.
      simplify.
    - print_silent.
  Defined.

  Lemma type_pick_min_ptr :
    qenvs_empty ⊨ impl_pick_min_ptr : ?.
  Proof.
    inferF.
    - abduct.
    - normalize. prune_exists. cleanup. normalize. unfold_type_post. simpl. simplify.
    - print_silent.
  Defined.

  Lemma type_inc_min_ptr :
    qenvs_empty ⊨ impl_inc_min_ptr : ?.
  Proof.
    inferF.
    - abduct.
    - normalize. prune_exists. cleanup. normalize. unfold_type_post. simpl. simplify.
    - print_silent.
  Defined.

  Lemma type_inc_min_ptr_and_return_other :
    qenvs_empty ⊨ impl_inc_min_ptr_and_return_other : ?.
  Proof.
    inferF.
    - abduct.
    - normalize. prune_exists. cleanup. normalize. unfold_type_post. simpl. simplify.
    - print_silent.
  Defined.



End experiments.
