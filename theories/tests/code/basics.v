From caesium Require Export notation.
From caesium Require Import tactics.
From quiver.base Require Import annotations.
Set Default Proof Using "Type*".

(* Generated from [examples/src/basics.c]. *)
Section code.

  (* Definition of function [swap]. *)
  Definition impl_swap : function := {|
    f_args := [
      ("x", void*);
      ("y", void*)
    ];
    f_local_vars := [
      ("tmp", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "tmp" <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("x")) ;
        !{PtrOp, Na1Ord, false} ("x") <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("y")) ;
        !{PtrOp, Na1Ord, false} ("y") <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} ("tmp") ;
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [overwrite]. *)
  Definition impl_overwrite : function := {|
    f_args := [
      ("x", void*)
    ];
    f_local_vars := [
      ("tmp", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "tmp" <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("x")) ;
        !{PtrOp, Na1Ord, false} ("x") <-{ IntOp i32 } i2v 42 i32 ;
        !{PtrOp, Na1Ord, false} ("x") <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} ("tmp") ;
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [deref]. *)
  Definition impl_deref : function := {|
    f_args := [
      ("x", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        Return (use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("x")))
      ]> $∅
    )%E
  |}.

  (* Definition of function [id]. *)
  Definition impl_id : function := {|
    f_args := [
      ("x", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        Return (use{IntOp i32, Na1Ord, false} ("x"))
      ]> $∅
    )%E
  |}.

  (* Definition of function [swap2]. *)
  Definition impl_swap2 : function := {|
    f_args := [
      ("x", void*);
      ("y", void*)
    ];
    f_local_vars := [
      ("tmp2", it_layout i32);
      ("tmp", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "tmp" <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("x")) ;
        "tmp2" <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("y")) ;
        !{PtrOp, Na1Ord, false} ("x") <-{ IntOp i32 } i2v 42 i32 ;
        !{PtrOp, Na1Ord, false} ("y") <-{ IntOp i32 } i2v 42 i32 ;
        !{PtrOp, Na1Ord, false} ("x") <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} ("tmp2") ;
        !{PtrOp, Na1Ord, false} ("y") <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} ("tmp") ;
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [times_two]. *)
  Definition impl_times_two : function := {|
    f_args := [
      ("x", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        Return ((use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("x"))) +{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("x"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [double_int]. *)
  Definition impl_double_int : function := {|
    f_args := [
      ("x", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        Return ((use{IntOp i32, Na1Ord, false} ("x")) +{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("x")))
      ]> $∅
    )%E
  |}.

  (* Definition of function [dup]. *)
  Definition impl_dup : function := {|
    f_args := [
      ("dst1", void*);
      ("dst2", void*);
      ("src", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        !{PtrOp, Na1Ord, false} ("dst1") <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("src")) ;
        !{PtrOp, Na1Ord, false} ("dst2") <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("src")) ;
        Return (use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("src")))
      ]> $∅
    )%E
  |}.

  (* Definition of function [use_swap]. *)
  Definition impl_use_swap (global_swap : loc): function := {|
    f_args := [
    ];
    f_local_vars := [
      ("x", it_layout i32);
      ("y", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "x" <-{ IntOp i32 } i2v 4 i32 ;
        "y" <-{ IntOp i32 } i2v 5 i32 ;
        expr: (Call (global_swap) [@{expr} &("x") ;
        &("y") ]) ;
        assert{IntOp i32}: (((use{IntOp i32, Na1Ord, false} ("x")) ={IntOp i32, IntOp i32, i32} (i2v 5 i32)) &&{IntOp i32, IntOp i32, i32} ((use{IntOp i32, Na1Ord, false} ("y")) ={IntOp i32, IntOp i32, i32} (i2v 4 i32))) ;
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [arith]. *)
  Definition impl_arith : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", it_layout i32)
    ];
    f_local_vars := [
      ("i", it_layout i32);
      ("c", it_layout i32);
      ("o", it_layout i32);
      ("r", it_layout i32);
      ("m", it_layout i32);
      ("f", it_layout i32);
      ("d", it_layout i32);
      ("l", it_layout i32);
      ("q", it_layout i32);
      ("g", it_layout i32);
      ("j", it_layout i32);
      ("b", it_layout i32);
      ("e", it_layout i32);
      ("p", it_layout i32);
      ("z", it_layout i32);
      ("n", it_layout i32);
      ("a", it_layout i32);
      ("h", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "z" <-{ IntOp i32 }
          (((use{IntOp i32, Na1Ord, false} ("x")) +{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("x"))) ×{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("y"))) /{IntOp i32, IntOp i32} (i2v 2 i32) ;
        "a" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) +{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "b" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) -{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "c" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) ×{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "d" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) /{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "e" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) %{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "f" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) <<{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "g" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) >>{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "h" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) &{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "i" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) |{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "j" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) ^{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "l" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) ={IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "m" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) !={IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "n" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) <{IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "o" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) ≤{IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "p" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) >{IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "q" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("x")) ≥{IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} ("y")) ;
        "r" <-{ IntOp i32 }
          UnOp NegOp (IntOp i32) (use{IntOp i32, Na1Ord, false} ("x")) ;
        Return (i2v 42 i32)
      ]> $∅
    )%E
  |}.

  (* Definition of function [cond_assign]. *)
  Definition impl_cond_assign : function := {|
    f_args := [
      ("x", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        if{IntOp i32, None}: (use{PtrOp, Na1Ord, false} ("x")) !={PtrOp, PtrOp, i32} (NULL)
        then
        Goto "#1"
        else
        Goto "#2"
      ]> $
      <[ "#1" :=
        !{PtrOp, Na1Ord, false} ("x") <-{ IntOp i32 } i2v 42 i32 ;
        Return (VOID)
      ]> $
      <[ "#2" :=
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [is_some_check]. *)
  Definition impl_is_some_check : function := {|
    f_args := [
      ("x", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        Return (UnOp (CastOp $ BoolOp) (IntOp i32) ((use{PtrOp, Na1Ord, false} ("x")) !={PtrOp, PtrOp, i32} (NULL)))
      ]> $∅
    )%E
  |}.

  (* Definition of function [is_none_check]. *)
  Definition impl_is_none_check : function := {|
    f_args := [
      ("x", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        Return (UnOp (CastOp $ BoolOp) (IntOp i32) ((use{PtrOp, Na1Ord, false} ("x")) ={PtrOp, PtrOp, i32} (NULL)))
      ]> $∅
    )%E
  |}.

  (* Definition of function [is_some]. *)
  Definition impl_is_some : function := {|
    f_args := [
      ("x", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        if{IntOp i32, None}: (use{PtrOp, Na1Ord, false} ("x")) ={PtrOp, PtrOp, i32} (NULL)
        then
        Goto "#1"
        else
        Goto "#2"
      ]> $
      <[ "#1" :=
        Return (UnOp (CastOp $ BoolOp) (IntOp i32) (i2v 0 i32))
      ]> $
      <[ "#2" :=
        Return (UnOp (CastOp $ BoolOp) (IntOp i32) (i2v 1 i32))
      ]> $∅
    )%E
  |}.

  (* Definition of function [ptr_eq]. *)
  Definition impl_ptr_eq : function := {|
    f_args := [
      ("x", void*);
      ("y", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        Return (UnOp (CastOp $ BoolOp) (IntOp i32) ((use{PtrOp, Na1Ord, false} ("x")) ={PtrOp, PtrOp, i32} (use{PtrOp, Na1Ord, false} ("y"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [ptr_neq]. *)
  Definition impl_ptr_neq : function := {|
    f_args := [
      ("x", void*);
      ("y", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        Return (UnOp (CastOp $ BoolOp) (IntOp i32) ((use{PtrOp, Na1Ord, false} ("x")) !={PtrOp, PtrOp, i32} (use{PtrOp, Na1Ord, false} ("y"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [forwarding]. *)
  Definition impl_forwarding (global_f global_g : loc): function := {|
    f_args := [
      ("x", it_layout i32)
    ];
    f_local_vars := [
      ("m", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "m" <-{ IntOp i32 }
          Call (global_f) [@{expr} use{IntOp i32, Na1Ord, false} ("x") ] ;
        Return (Call (global_g) [@{expr} use{IntOp i32, Na1Ord, false} ("m") ])
      ]> $∅
    )%E
  |}.
End code.
