From caesium Require Export notation.
From caesium Require Import tactics.
From quiver.base Require Import annotations.
Set Default Proof Using "Type*".

(* Generated from [examples/src/pred_annotations.c]. *)
Section code.

  (* Definition of function [fun]. *)
  Definition impl_fun : function := {|
    f_args := [
      ("a", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        annot: (CtxHintAnnot "__hint_0") ;
        Return (use{PtrOp, Na1Ord, false} ("a"))
      ]> $∅
    )%E
  |}.

  (* Definition of function [pick_min_ptr]. *)
  Definition impl_pick_min_ptr : function := {|
    f_args := [
      ("a", void*);
      ("b", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        if{IntOp i32, None}: (use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("a"))) <{IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("b")))
        then
        Goto "#1"
        else
        Goto "#2"
      ]> $
      <[ "#1" :=
        Return (use{PtrOp, Na1Ord, false} ("a"))
      ]> $
      <[ "#2" :=
        Return (use{PtrOp, Na1Ord, false} ("b"))
      ]> $∅
    )%E
  |}.

  (* Definition of function [inc_min_ptr]. *)
  Definition impl_inc_min_ptr : function := {|
    f_args := [
      ("a", void*);
      ("b", void*)
    ];
    f_local_vars := [
      ("c", void*)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        if{IntOp i32, Some "#1"}: (use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("a"))) <{IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("b")))
        then
        Goto "#2"
        else
        Goto "#3"
      ]> $
      <[ "#1" :=
        !{PtrOp, Na1Ord, false} ("c") <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("c"))) +{IntOp i32, IntOp i32} (i2v 42 i32) ;
        Return (VOID)
      ]> $
      <[ "#2" :=
        "c" <-{ PtrOp } use{PtrOp, Na1Ord, false} ("a") ;
        Goto "#1"
      ]> $
      <[ "#3" :=
        "c" <-{ PtrOp } use{PtrOp, Na1Ord, false} ("b") ;
        Goto "#1"
      ]> $∅
    )%E
  |}.

  (* Definition of function [inc_min_ptr_and_return_other]. *)
  Definition impl_inc_min_ptr_and_return_other : function := {|
    f_args := [
      ("a", void*);
      ("b", void*)
    ];
    f_local_vars := [
      ("c", void*);
      ("d", void*)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        if{IntOp i32, Some "#1"}: (use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("a"))) <{IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("b")))
        then
        Goto "#2"
        else
        Goto "#3"
      ]> $
      <[ "#1" :=
        !{PtrOp, Na1Ord, false} ("c") <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} (!{PtrOp, Na1Ord, false} ("c"))) +{IntOp i32, IntOp i32} (i2v 42 i32) ;
        Return (use{PtrOp, Na1Ord, false} ("d"))
      ]> $
      <[ "#2" :=
        "c" <-{ PtrOp } use{PtrOp, Na1Ord, false} ("a") ;
        "d" <-{ PtrOp } use{PtrOp, Na1Ord, false} ("b") ;
        Goto "#1"
      ]> $
      <[ "#3" :=
        "c" <-{ PtrOp } use{PtrOp, Na1Ord, false} ("b") ;
        "d" <-{ PtrOp } use{PtrOp, Na1Ord, false} ("a") ;
        Goto "#1"
      ]> $∅
    )%E
  |}.

  (* Definition of function [g]. *)
  Definition impl_g : function := {|
    f_args := [
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        Return (i2v 42 i32)
      ]> $∅
    )%E
  |}.

  (* Definition of function [bla]. *)
  Definition impl_bla (global_g : loc): function := {|
    f_args := [
    ];
    f_local_vars := [
      ("x", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "x" <-{ IntOp i32 } Call (global_g) [@{expr}  ] ;
        Return (use{IntOp i32, Na1Ord, false} ("x"))
      ]> $∅
    )%E
  |}.
End code.
