From caesium Require Export notation.
From caesium Require Import tactics.
From quiver.base Require Import annotations.
Set Default Proof Using "Type".

(* Generated from [src/conditionals.c]. *)
Section code.
  Definition file_0 : string := "src/conditionals.c".
  Definition loc_2 : location_info := LocationInfo file_0 6 4 10 5.
  Definition loc_3 : location_info := LocationInfo file_0 6 15 8 5.
  Definition loc_4 : location_info := LocationInfo file_0 7 8 7 17.
  Definition loc_5 : location_info := LocationInfo file_0 7 15 7 16.
  Definition loc_6 : location_info := LocationInfo file_0 7 15 7 16.
  Definition loc_7 : location_info := LocationInfo file_0 8 11 10 5.
  Definition loc_8 : location_info := LocationInfo file_0 9 8 9 17.
  Definition loc_9 : location_info := LocationInfo file_0 9 15 9 16.
  Definition loc_10 : location_info := LocationInfo file_0 9 15 9 16.
  Definition loc_11 : location_info := LocationInfo file_0 6 8 6 13.
  Definition loc_12 : location_info := LocationInfo file_0 6 8 6 9.
  Definition loc_13 : location_info := LocationInfo file_0 6 8 6 9.
  Definition loc_14 : location_info := LocationInfo file_0 6 12 6 13.
  Definition loc_15 : location_info := LocationInfo file_0 6 12 6 13.
  Definition loc_18 : location_info := LocationInfo file_0 15 4 15 31.
  Definition loc_19 : location_info := LocationInfo file_0 15 11 15 30.
  Definition loc_20 : location_info := LocationInfo file_0 15 11 15 14.
  Definition loc_21 : location_info := LocationInfo file_0 15 11 15 14.
  Definition loc_22 : location_info := LocationInfo file_0 15 16 15 26.
  Definition loc_23 : location_info := LocationInfo file_0 15 16 15 19.
  Definition loc_24 : location_info := LocationInfo file_0 15 16 15 19.
  Definition loc_25 : location_info := LocationInfo file_0 15 21 15 22.
  Definition loc_26 : location_info := LocationInfo file_0 15 21 15 22.
  Definition loc_27 : location_info := LocationInfo file_0 15 24 15 25.
  Definition loc_28 : location_info := LocationInfo file_0 15 24 15 25.
  Definition loc_29 : location_info := LocationInfo file_0 15 28 15 29.
  Definition loc_30 : location_info := LocationInfo file_0 15 28 15 29.
  Definition loc_33 : location_info := LocationInfo file_0 19 4 31 5.
  Definition loc_34 : location_info := LocationInfo file_0 19 15 25 5.
  Definition loc_35 : location_info := LocationInfo file_0 20 8 24 9.
  Definition loc_36 : location_info := LocationInfo file_0 20 19 22 9.
  Definition loc_37 : location_info := LocationInfo file_0 21 12 21 21.
  Definition loc_38 : location_info := LocationInfo file_0 21 19 21 20.
  Definition loc_39 : location_info := LocationInfo file_0 21 19 21 20.
  Definition loc_40 : location_info := LocationInfo file_0 22 15 24 9.
  Definition loc_41 : location_info := LocationInfo file_0 23 12 23 21.
  Definition loc_42 : location_info := LocationInfo file_0 23 19 23 20.
  Definition loc_43 : location_info := LocationInfo file_0 23 19 23 20.
  Definition loc_44 : location_info := LocationInfo file_0 20 12 20 17.
  Definition loc_45 : location_info := LocationInfo file_0 20 12 20 13.
  Definition loc_46 : location_info := LocationInfo file_0 20 12 20 13.
  Definition loc_47 : location_info := LocationInfo file_0 20 16 20 17.
  Definition loc_48 : location_info := LocationInfo file_0 20 16 20 17.
  Definition loc_49 : location_info := LocationInfo file_0 25 11 31 5.
  Definition loc_50 : location_info := LocationInfo file_0 26 8 30 9.
  Definition loc_51 : location_info := LocationInfo file_0 26 19 28 9.
  Definition loc_52 : location_info := LocationInfo file_0 27 12 27 21.
  Definition loc_53 : location_info := LocationInfo file_0 27 19 27 20.
  Definition loc_54 : location_info := LocationInfo file_0 27 19 27 20.
  Definition loc_55 : location_info := LocationInfo file_0 28 15 30 9.
  Definition loc_56 : location_info := LocationInfo file_0 29 12 29 21.
  Definition loc_57 : location_info := LocationInfo file_0 29 19 29 20.
  Definition loc_58 : location_info := LocationInfo file_0 29 19 29 20.
  Definition loc_59 : location_info := LocationInfo file_0 26 12 26 17.
  Definition loc_60 : location_info := LocationInfo file_0 26 12 26 13.
  Definition loc_61 : location_info := LocationInfo file_0 26 12 26 13.
  Definition loc_62 : location_info := LocationInfo file_0 26 16 26 17.
  Definition loc_63 : location_info := LocationInfo file_0 26 16 26 17.
  Definition loc_64 : location_info := LocationInfo file_0 19 8 19 13.
  Definition loc_65 : location_info := LocationInfo file_0 19 8 19 9.
  Definition loc_66 : location_info := LocationInfo file_0 19 8 19 9.
  Definition loc_67 : location_info := LocationInfo file_0 19 12 19 13.
  Definition loc_68 : location_info := LocationInfo file_0 19 12 19 13.
  Definition loc_71 : location_info := LocationInfo file_0 35 4 35 19.
  Definition loc_72 : location_info := LocationInfo file_0 36 4 36 11.
  Definition loc_74 : location_info := LocationInfo file_0 35 12 35 17.
  Definition loc_75 : location_info := LocationInfo file_0 35 12 35 13.
  Definition loc_76 : location_info := LocationInfo file_0 35 12 35 13.
  Definition loc_77 : location_info := LocationInfo file_0 35 16 35 17.
  Definition loc_80 : location_info := LocationInfo file_0 41 4 41 27.
  Definition loc_81 : location_info := LocationInfo file_0 41 11 41 26.
  Definition loc_82 : location_info := LocationInfo file_0 41 11 41 18.
  Definition loc_83 : location_info := LocationInfo file_0 41 12 41 13.
  Definition loc_84 : location_info := LocationInfo file_0 41 12 41 13.
  Definition loc_85 : location_info := LocationInfo file_0 41 16 41 17.
  Definition loc_86 : location_info := LocationInfo file_0 41 16 41 17.
  Definition loc_87 : location_info := LocationInfo file_0 41 21 41 22.
  Definition loc_88 : location_info := LocationInfo file_0 41 21 41 22.
  Definition loc_89 : location_info := LocationInfo file_0 41 25 41 26.
  Definition loc_90 : location_info := LocationInfo file_0 41 25 41 26.
  Definition loc_93 : location_info := LocationInfo file_0 45 4 45 37.
  Definition loc_94 : location_info := LocationInfo file_0 46 4 46 11.
  Definition loc_96 : location_info := LocationInfo file_0 45 12 45 35.
  Definition loc_97 : location_info := LocationInfo file_0 45 12 45 26.
  Definition loc_98 : location_info := LocationInfo file_0 45 12 45 17.
  Definition loc_99 : location_info := LocationInfo file_0 45 12 45 13.
  Definition loc_100 : location_info := LocationInfo file_0 45 12 45 13.
  Definition loc_101 : location_info := LocationInfo file_0 45 16 45 17.
  Definition loc_102 : location_info := LocationInfo file_0 45 21 45 26.
  Definition loc_103 : location_info := LocationInfo file_0 45 21 45 22.
  Definition loc_104 : location_info := LocationInfo file_0 45 21 45 22.
  Definition loc_105 : location_info := LocationInfo file_0 45 25 45 26.
  Definition loc_106 : location_info := LocationInfo file_0 45 30 45 35.
  Definition loc_107 : location_info := LocationInfo file_0 45 30 45 31.
  Definition loc_108 : location_info := LocationInfo file_0 45 30 45 31.
  Definition loc_109 : location_info := LocationInfo file_0 45 34 45 35.
  Definition loc_112 : location_info := LocationInfo file_0 50 4 50 37.
  Definition loc_113 : location_info := LocationInfo file_0 51 4 51 11.
  Definition loc_115 : location_info := LocationInfo file_0 50 12 50 35.
  Definition loc_116 : location_info := LocationInfo file_0 50 12 50 26.
  Definition loc_117 : location_info := LocationInfo file_0 50 12 50 17.
  Definition loc_118 : location_info := LocationInfo file_0 50 12 50 13.
  Definition loc_119 : location_info := LocationInfo file_0 50 12 50 13.
  Definition loc_120 : location_info := LocationInfo file_0 50 16 50 17.
  Definition loc_121 : location_info := LocationInfo file_0 50 21 50 26.
  Definition loc_122 : location_info := LocationInfo file_0 50 21 50 22.
  Definition loc_123 : location_info := LocationInfo file_0 50 21 50 22.
  Definition loc_124 : location_info := LocationInfo file_0 50 25 50 26.
  Definition loc_125 : location_info := LocationInfo file_0 50 30 50 35.
  Definition loc_126 : location_info := LocationInfo file_0 50 30 50 31.
  Definition loc_127 : location_info := LocationInfo file_0 50 30 50 31.
  Definition loc_128 : location_info := LocationInfo file_0 50 34 50 35.
  Definition loc_131 : location_info := LocationInfo file_0 55 4 55 68.
  Definition loc_132 : location_info := LocationInfo file_0 56 4 56 11.
  Definition loc_134 : location_info := LocationInfo file_0 55 12 55 66.
  Definition loc_135 : location_info := LocationInfo file_0 55 12 55 37.
  Definition loc_136 : location_info := LocationInfo file_0 55 13 55 27.
  Definition loc_137 : location_info := LocationInfo file_0 55 13 55 18.
  Definition loc_138 : location_info := LocationInfo file_0 55 13 55 14.
  Definition loc_139 : location_info := LocationInfo file_0 55 13 55 14.
  Definition loc_140 : location_info := LocationInfo file_0 55 17 55 18.
  Definition loc_141 : location_info := LocationInfo file_0 55 22 55 27.
  Definition loc_142 : location_info := LocationInfo file_0 55 22 55 23.
  Definition loc_143 : location_info := LocationInfo file_0 55 22 55 23.
  Definition loc_144 : location_info := LocationInfo file_0 55 26 55 27.
  Definition loc_145 : location_info := LocationInfo file_0 55 31 55 36.
  Definition loc_146 : location_info := LocationInfo file_0 55 31 55 32.
  Definition loc_147 : location_info := LocationInfo file_0 55 31 55 32.
  Definition loc_148 : location_info := LocationInfo file_0 55 35 55 36.
  Definition loc_149 : location_info := LocationInfo file_0 55 41 55 66.
  Definition loc_150 : location_info := LocationInfo file_0 55 42 55 56.
  Definition loc_151 : location_info := LocationInfo file_0 55 42 55 47.
  Definition loc_152 : location_info := LocationInfo file_0 55 42 55 43.
  Definition loc_153 : location_info := LocationInfo file_0 55 42 55 43.
  Definition loc_154 : location_info := LocationInfo file_0 55 46 55 47.
  Definition loc_155 : location_info := LocationInfo file_0 55 51 55 56.
  Definition loc_156 : location_info := LocationInfo file_0 55 51 55 52.
  Definition loc_157 : location_info := LocationInfo file_0 55 51 55 52.
  Definition loc_158 : location_info := LocationInfo file_0 55 55 55 56.
  Definition loc_159 : location_info := LocationInfo file_0 55 60 55 65.
  Definition loc_160 : location_info := LocationInfo file_0 55 60 55 61.
  Definition loc_161 : location_info := LocationInfo file_0 55 60 55 61.
  Definition loc_162 : location_info := LocationInfo file_0 55 64 55 65.
  Definition loc_165 : location_info := LocationInfo file_0 60 4 61 21.
  Definition loc_166 : location_info := LocationInfo file_0 62 4 63 21.
  Definition loc_167 : location_info := LocationInfo file_0 64 4 65 21.
  Definition loc_168 : location_info := LocationInfo file_0 66 4 67 21.
  Definition loc_169 : location_info := LocationInfo file_0 69 4 69 13.
  Definition loc_170 : location_info := LocationInfo file_0 69 11 69 12.
  Definition loc_171 : location_info := LocationInfo file_0 67 8 67 21.
  Definition loc_172 : location_info := LocationInfo file_0 67 15 67 20.
  Definition loc_173 : location_info := LocationInfo file_0 67 15 67 16.
  Definition loc_174 : location_info := LocationInfo file_0 67 15 67 16.
  Definition loc_175 : location_info := LocationInfo file_0 67 19 67 20.
  Definition loc_176 : location_info := LocationInfo file_0 67 19 67 20.
  Definition loc_177 : location_info := LocationInfo file_0 66 4 67 21.
  Definition loc_178 : location_info := LocationInfo file_0 66 8 66 17.
  Definition loc_179 : location_info := LocationInfo file_0 66 8 66 12.
  Definition loc_180 : location_info := LocationInfo file_0 66 8 66 12.
  Definition loc_181 : location_info := LocationInfo file_0 66 16 66 17.
  Definition loc_182 : location_info := LocationInfo file_0 65 8 65 21.
  Definition loc_183 : location_info := LocationInfo file_0 65 15 65 20.
  Definition loc_184 : location_info := LocationInfo file_0 65 15 65 16.
  Definition loc_185 : location_info := LocationInfo file_0 65 15 65 16.
  Definition loc_186 : location_info := LocationInfo file_0 65 19 65 20.
  Definition loc_187 : location_info := LocationInfo file_0 65 19 65 20.
  Definition loc_188 : location_info := LocationInfo file_0 64 4 65 21.
  Definition loc_189 : location_info := LocationInfo file_0 64 8 64 17.
  Definition loc_190 : location_info := LocationInfo file_0 64 8 64 12.
  Definition loc_191 : location_info := LocationInfo file_0 64 8 64 12.
  Definition loc_192 : location_info := LocationInfo file_0 64 16 64 17.
  Definition loc_193 : location_info := LocationInfo file_0 63 8 63 21.
  Definition loc_194 : location_info := LocationInfo file_0 63 15 63 20.
  Definition loc_195 : location_info := LocationInfo file_0 63 15 63 16.
  Definition loc_196 : location_info := LocationInfo file_0 63 15 63 16.
  Definition loc_197 : location_info := LocationInfo file_0 63 19 63 20.
  Definition loc_198 : location_info := LocationInfo file_0 63 19 63 20.
  Definition loc_199 : location_info := LocationInfo file_0 62 4 63 21.
  Definition loc_200 : location_info := LocationInfo file_0 62 8 62 17.
  Definition loc_201 : location_info := LocationInfo file_0 62 8 62 12.
  Definition loc_202 : location_info := LocationInfo file_0 62 8 62 12.
  Definition loc_203 : location_info := LocationInfo file_0 62 16 62 17.
  Definition loc_204 : location_info := LocationInfo file_0 61 8 61 21.
  Definition loc_205 : location_info := LocationInfo file_0 61 15 61 20.
  Definition loc_206 : location_info := LocationInfo file_0 61 15 61 16.
  Definition loc_207 : location_info := LocationInfo file_0 61 15 61 16.
  Definition loc_208 : location_info := LocationInfo file_0 61 19 61 20.
  Definition loc_209 : location_info := LocationInfo file_0 61 19 61 20.
  Definition loc_210 : location_info := LocationInfo file_0 60 4 61 21.
  Definition loc_211 : location_info := LocationInfo file_0 60 8 60 17.
  Definition loc_212 : location_info := LocationInfo file_0 60 8 60 12.
  Definition loc_213 : location_info := LocationInfo file_0 60 8 60 12.
  Definition loc_214 : location_info := LocationInfo file_0 60 16 60 17.
  Definition loc_217 : location_info := LocationInfo file_0 74 4 74 16.
  Definition loc_218 : location_info := LocationInfo file_0 76 4 77 20.
  Definition loc_219 : location_info := LocationInfo file_0 78 4 79 20.
  Definition loc_220 : location_info := LocationInfo file_0 80 4 81 20.
  Definition loc_221 : location_info := LocationInfo file_0 82 4 83 20.
  Definition loc_222 : location_info := LocationInfo file_0 85 4 85 15.
  Definition loc_223 : location_info := LocationInfo file_0 85 11 85 14.
  Definition loc_224 : location_info := LocationInfo file_0 85 11 85 14.
  Definition loc_225 : location_info := LocationInfo file_0 83 8 83 20.
  Definition loc_226 : location_info := LocationInfo file_0 83 8 83 11.
  Definition loc_227 : location_info := LocationInfo file_0 83 14 83 19.
  Definition loc_228 : location_info := LocationInfo file_0 83 14 83 15.
  Definition loc_229 : location_info := LocationInfo file_0 83 14 83 15.
  Definition loc_230 : location_info := LocationInfo file_0 83 18 83 19.
  Definition loc_231 : location_info := LocationInfo file_0 83 18 83 19.
  Definition loc_232 : location_info := LocationInfo file_0 82 4 83 20.
  Definition loc_233 : location_info := LocationInfo file_0 82 8 82 17.
  Definition loc_234 : location_info := LocationInfo file_0 82 8 82 12.
  Definition loc_235 : location_info := LocationInfo file_0 82 8 82 12.
  Definition loc_236 : location_info := LocationInfo file_0 82 16 82 17.
  Definition loc_237 : location_info := LocationInfo file_0 81 8 81 20.
  Definition loc_238 : location_info := LocationInfo file_0 81 8 81 11.
  Definition loc_239 : location_info := LocationInfo file_0 81 14 81 19.
  Definition loc_240 : location_info := LocationInfo file_0 81 14 81 15.
  Definition loc_241 : location_info := LocationInfo file_0 81 14 81 15.
  Definition loc_242 : location_info := LocationInfo file_0 81 18 81 19.
  Definition loc_243 : location_info := LocationInfo file_0 81 18 81 19.
  Definition loc_244 : location_info := LocationInfo file_0 80 4 81 20.
  Definition loc_245 : location_info := LocationInfo file_0 80 8 80 17.
  Definition loc_246 : location_info := LocationInfo file_0 80 8 80 12.
  Definition loc_247 : location_info := LocationInfo file_0 80 8 80 12.
  Definition loc_248 : location_info := LocationInfo file_0 80 16 80 17.
  Definition loc_249 : location_info := LocationInfo file_0 79 8 79 20.
  Definition loc_250 : location_info := LocationInfo file_0 79 8 79 11.
  Definition loc_251 : location_info := LocationInfo file_0 79 14 79 19.
  Definition loc_252 : location_info := LocationInfo file_0 79 14 79 15.
  Definition loc_253 : location_info := LocationInfo file_0 79 14 79 15.
  Definition loc_254 : location_info := LocationInfo file_0 79 18 79 19.
  Definition loc_255 : location_info := LocationInfo file_0 79 18 79 19.
  Definition loc_256 : location_info := LocationInfo file_0 78 4 79 20.
  Definition loc_257 : location_info := LocationInfo file_0 78 8 78 17.
  Definition loc_258 : location_info := LocationInfo file_0 78 8 78 12.
  Definition loc_259 : location_info := LocationInfo file_0 78 8 78 12.
  Definition loc_260 : location_info := LocationInfo file_0 78 16 78 17.
  Definition loc_261 : location_info := LocationInfo file_0 77 8 77 20.
  Definition loc_262 : location_info := LocationInfo file_0 77 8 77 11.
  Definition loc_263 : location_info := LocationInfo file_0 77 14 77 19.
  Definition loc_264 : location_info := LocationInfo file_0 77 14 77 15.
  Definition loc_265 : location_info := LocationInfo file_0 77 14 77 15.
  Definition loc_266 : location_info := LocationInfo file_0 77 18 77 19.
  Definition loc_267 : location_info := LocationInfo file_0 77 18 77 19.
  Definition loc_268 : location_info := LocationInfo file_0 76 4 77 20.
  Definition loc_269 : location_info := LocationInfo file_0 76 8 76 17.
  Definition loc_270 : location_info := LocationInfo file_0 76 8 76 12.
  Definition loc_271 : location_info := LocationInfo file_0 76 8 76 12.
  Definition loc_272 : location_info := LocationInfo file_0 76 16 76 17.
  Definition loc_273 : location_info := LocationInfo file_0 74 14 74 15.
  Definition loc_278 : location_info := LocationInfo file_0 91 4 92 20.
  Definition loc_279 : location_info := LocationInfo file_0 93 4 94 20.
  Definition loc_280 : location_info := LocationInfo file_0 95 4 96 20.
  Definition loc_281 : location_info := LocationInfo file_0 97 4 98 20.
  Definition loc_282 : location_info := LocationInfo file_0 100 4 100 15.
  Definition loc_283 : location_info := LocationInfo file_0 100 11 100 14.
  Definition loc_284 : location_info := LocationInfo file_0 100 11 100 14.
  Definition loc_285 : location_info := LocationInfo file_0 98 8 98 20.
  Definition loc_286 : location_info := LocationInfo file_0 98 8 98 11.
  Definition loc_287 : location_info := LocationInfo file_0 98 14 98 19.
  Definition loc_288 : location_info := LocationInfo file_0 98 14 98 15.
  Definition loc_289 : location_info := LocationInfo file_0 98 14 98 15.
  Definition loc_290 : location_info := LocationInfo file_0 98 18 98 19.
  Definition loc_291 : location_info := LocationInfo file_0 98 18 98 19.
  Definition loc_292 : location_info := LocationInfo file_0 97 4 98 20.
  Definition loc_293 : location_info := LocationInfo file_0 97 8 97 17.
  Definition loc_294 : location_info := LocationInfo file_0 97 8 97 12.
  Definition loc_295 : location_info := LocationInfo file_0 97 8 97 12.
  Definition loc_296 : location_info := LocationInfo file_0 97 16 97 17.
  Definition loc_297 : location_info := LocationInfo file_0 96 8 96 20.
  Definition loc_298 : location_info := LocationInfo file_0 96 8 96 11.
  Definition loc_299 : location_info := LocationInfo file_0 96 14 96 19.
  Definition loc_300 : location_info := LocationInfo file_0 96 14 96 15.
  Definition loc_301 : location_info := LocationInfo file_0 96 14 96 15.
  Definition loc_302 : location_info := LocationInfo file_0 96 18 96 19.
  Definition loc_303 : location_info := LocationInfo file_0 96 18 96 19.
  Definition loc_304 : location_info := LocationInfo file_0 95 4 96 20.
  Definition loc_305 : location_info := LocationInfo file_0 95 8 95 17.
  Definition loc_306 : location_info := LocationInfo file_0 95 8 95 12.
  Definition loc_307 : location_info := LocationInfo file_0 95 8 95 12.
  Definition loc_308 : location_info := LocationInfo file_0 95 16 95 17.
  Definition loc_309 : location_info := LocationInfo file_0 94 8 94 20.
  Definition loc_310 : location_info := LocationInfo file_0 94 8 94 11.
  Definition loc_311 : location_info := LocationInfo file_0 94 14 94 19.
  Definition loc_312 : location_info := LocationInfo file_0 94 14 94 15.
  Definition loc_313 : location_info := LocationInfo file_0 94 14 94 15.
  Definition loc_314 : location_info := LocationInfo file_0 94 18 94 19.
  Definition loc_315 : location_info := LocationInfo file_0 94 18 94 19.
  Definition loc_316 : location_info := LocationInfo file_0 93 4 94 20.
  Definition loc_317 : location_info := LocationInfo file_0 93 8 93 17.
  Definition loc_318 : location_info := LocationInfo file_0 93 8 93 12.
  Definition loc_319 : location_info := LocationInfo file_0 93 8 93 12.
  Definition loc_320 : location_info := LocationInfo file_0 93 16 93 17.
  Definition loc_321 : location_info := LocationInfo file_0 92 8 92 20.
  Definition loc_322 : location_info := LocationInfo file_0 92 8 92 11.
  Definition loc_323 : location_info := LocationInfo file_0 92 14 92 19.
  Definition loc_324 : location_info := LocationInfo file_0 92 14 92 15.
  Definition loc_325 : location_info := LocationInfo file_0 92 14 92 15.
  Definition loc_326 : location_info := LocationInfo file_0 92 18 92 19.
  Definition loc_327 : location_info := LocationInfo file_0 92 18 92 19.
  Definition loc_328 : location_info := LocationInfo file_0 91 4 92 20.
  Definition loc_329 : location_info := LocationInfo file_0 91 8 91 17.
  Definition loc_330 : location_info := LocationInfo file_0 91 8 91 12.
  Definition loc_331 : location_info := LocationInfo file_0 91 8 91 12.
  Definition loc_332 : location_info := LocationInfo file_0 91 16 91 17.
  Definition loc_335 : location_info := LocationInfo file_0 106 4 108 5.
  Definition loc_336 : location_info := LocationInfo file_0 110 4 112 5.
  Definition loc_337 : location_info := LocationInfo file_0 110 11 112 5.
  Definition loc_338 : location_info := LocationInfo file_0 111 8 111 15.
  Definition loc_339 : location_info := LocationInfo file_0 111 8 111 10.
  Definition loc_340 : location_info := LocationInfo file_0 111 9 111 10.
  Definition loc_341 : location_info := LocationInfo file_0 111 9 111 10.
  Definition loc_342 : location_info := LocationInfo file_0 111 13 111 14.
  Definition loc_343 : location_info := LocationInfo file_0 110 4 112 5.
  Definition loc_344 : location_info := LocationInfo file_0 110 8 110 9.
  Definition loc_345 : location_info := LocationInfo file_0 110 8 110 9.
  Definition loc_346 : location_info := LocationInfo file_0 106 29 108 5.
  Definition loc_347 : location_info := LocationInfo file_0 107 8 107 15.
  Definition loc_348 : location_info := LocationInfo file_0 107 8 107 10.
  Definition loc_349 : location_info := LocationInfo file_0 107 9 107 10.
  Definition loc_350 : location_info := LocationInfo file_0 107 9 107 10.
  Definition loc_351 : location_info := LocationInfo file_0 107 13 107 14.
  Definition loc_352 : location_info := LocationInfo file_0 106 4 108 5.
  Definition loc_353 : location_info := LocationInfo file_0 106 8 106 27.
  Definition loc_354 : location_info := LocationInfo file_0 106 8 106 9.
  Definition loc_355 : location_info := LocationInfo file_0 106 8 106 9.
  Definition loc_356 : location_info := LocationInfo file_0 106 13 106 27.
  Definition loc_359 : location_info := LocationInfo file_0 117 4 119 5.
  Definition loc_360 : location_info := LocationInfo file_0 121 4 121 13.
  Definition loc_361 : location_info := LocationInfo file_0 121 11 121 12.
  Definition loc_362 : location_info := LocationInfo file_0 121 11 121 12.
  Definition loc_363 : location_info := LocationInfo file_0 117 11 119 5.
  Definition loc_364 : location_info := LocationInfo file_0 118 8 118 14.
  Definition loc_365 : location_info := LocationInfo file_0 118 8 118 9.
  Definition loc_366 : location_info := LocationInfo file_0 118 12 118 13.
  Definition loc_367 : location_info := LocationInfo file_0 117 4 119 5.
  Definition loc_368 : location_info := LocationInfo file_0 117 8 117 9.
  Definition loc_369 : location_info := LocationInfo file_0 117 8 117 9.
  Definition loc_372 : location_info := LocationInfo file_0 125 4 125 10.
  Definition loc_373 : location_info := LocationInfo file_0 126 4 128 5.
  Definition loc_374 : location_info := LocationInfo file_0 129 4 129 13.
  Definition loc_375 : location_info := LocationInfo file_0 129 11 129 12.
  Definition loc_376 : location_info := LocationInfo file_0 129 11 129 12.
  Definition loc_377 : location_info := LocationInfo file_0 126 11 128 5.
  Definition loc_378 : location_info := LocationInfo file_0 127 8 127 14.
  Definition loc_379 : location_info := LocationInfo file_0 127 8 127 9.
  Definition loc_380 : location_info := LocationInfo file_0 127 12 127 13.
  Definition loc_381 : location_info := LocationInfo file_0 126 4 128 5.
  Definition loc_382 : location_info := LocationInfo file_0 126 8 126 9.
  Definition loc_383 : location_info := LocationInfo file_0 126 8 126 9.
  Definition loc_384 : location_info := LocationInfo file_0 125 4 125 5.
  Definition loc_385 : location_info := LocationInfo file_0 125 8 125 9.
  Definition loc_386 : location_info := LocationInfo file_0 125 8 125 9.
  Definition loc_389 : location_info := LocationInfo file_0 133 4 135 5.
  Definition loc_390 : location_info := LocationInfo file_0 133 11 135 5.
  Definition loc_391 : location_info := LocationInfo file_0 134 8 134 15.
  Definition loc_392 : location_info := LocationInfo file_0 134 8 134 10.
  Definition loc_393 : location_info := LocationInfo file_0 134 9 134 10.
  Definition loc_394 : location_info := LocationInfo file_0 134 9 134 10.
  Definition loc_395 : location_info := LocationInfo file_0 134 13 134 14.
  Definition loc_396 : location_info := LocationInfo file_0 133 4 135 5.
  Definition loc_397 : location_info := LocationInfo file_0 133 8 133 9.
  Definition loc_398 : location_info := LocationInfo file_0 133 8 133 9.
  Definition loc_401 : location_info := LocationInfo file_0 139 4 141 5.
  Definition loc_402 : location_info := LocationInfo file_0 139 11 141 5.
  Definition loc_403 : location_info := LocationInfo file_0 140 8 140 16.
  Definition loc_404 : location_info := LocationInfo file_0 140 8 140 11.
  Definition loc_405 : location_info := LocationInfo file_0 140 9 140 11.
  Definition loc_406 : location_info := LocationInfo file_0 140 9 140 11.
  Definition loc_407 : location_info := LocationInfo file_0 140 10 140 11.
  Definition loc_408 : location_info := LocationInfo file_0 140 10 140 11.
  Definition loc_409 : location_info := LocationInfo file_0 140 14 140 15.
  Definition loc_410 : location_info := LocationInfo file_0 139 4 141 5.
  Definition loc_411 : location_info := LocationInfo file_0 139 8 139 9.
  Definition loc_412 : location_info := LocationInfo file_0 139 8 139 9.
  Definition loc_415 : location_info := LocationInfo file_0 145 4 149 5.
  Definition loc_416 : location_info := LocationInfo file_0 145 11 147 5.
  Definition loc_417 : location_info := LocationInfo file_0 146 8 146 15.
  Definition loc_418 : location_info := LocationInfo file_0 146 8 146 10.
  Definition loc_419 : location_info := LocationInfo file_0 146 9 146 10.
  Definition loc_420 : location_info := LocationInfo file_0 146 9 146 10.
  Definition loc_421 : location_info := LocationInfo file_0 146 13 146 14.
  Definition loc_422 : location_info := LocationInfo file_0 147 11 149 5.
  Definition loc_423 : location_info := LocationInfo file_0 148 8 148 15.
  Definition loc_424 : location_info := LocationInfo file_0 148 8 148 10.
  Definition loc_425 : location_info := LocationInfo file_0 148 9 148 10.
  Definition loc_426 : location_info := LocationInfo file_0 148 9 148 10.
  Definition loc_427 : location_info := LocationInfo file_0 148 13 148 14.
  Definition loc_428 : location_info := LocationInfo file_0 145 8 145 9.
  Definition loc_429 : location_info := LocationInfo file_0 145 8 145 9.
  Definition loc_432 : location_info := LocationInfo file_0 155 4 156 15.
  Definition loc_433 : location_info := LocationInfo file_0 158 4 158 11.
  Definition loc_434 : location_info := LocationInfo file_0 158 4 158 6.
  Definition loc_435 : location_info := LocationInfo file_0 158 5 158 6.
  Definition loc_436 : location_info := LocationInfo file_0 158 5 158 6.
  Definition loc_437 : location_info := LocationInfo file_0 158 9 158 10.
  Definition loc_438 : location_info := LocationInfo file_0 156 8 156 15.
  Definition loc_440 : location_info := LocationInfo file_0 155 4 156 15.
  Definition loc_441 : location_info := LocationInfo file_0 155 8 155 27.
  Definition loc_442 : location_info := LocationInfo file_0 155 8 155 9.
  Definition loc_443 : location_info := LocationInfo file_0 155 8 155 9.
  Definition loc_444 : location_info := LocationInfo file_0 155 13 155 27.
  Definition loc_447 : location_info := LocationInfo file_0 162 4 162 33.
  Definition loc_448 : location_info := LocationInfo file_0 163 4 163 11.
  Definition loc_449 : location_info := LocationInfo file_0 163 4 163 6.
  Definition loc_450 : location_info := LocationInfo file_0 163 5 163 6.
  Definition loc_451 : location_info := LocationInfo file_0 163 5 163 6.
  Definition loc_452 : location_info := LocationInfo file_0 163 9 163 10.
  Definition loc_453 : location_info := LocationInfo file_0 162 12 162 31.
  Definition loc_454 : location_info := LocationInfo file_0 162 12 162 13.
  Definition loc_455 : location_info := LocationInfo file_0 162 12 162 13.
  Definition loc_456 : location_info := LocationInfo file_0 162 17 162 31.

  (* Definition of function [min]. *)
  Definition impl_min : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_11 ;
        if{IntOp i32, None}: LocInfoE loc_11 ((LocInfoE loc_12 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_13 ("x")))) <{IntOp i32, IntOp i32, i32} (LocInfoE loc_14 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_15 ("y")))))
        then
        locinfo: loc_4 ;
          Goto "#1"
        else
        locinfo: loc_8 ;
          Goto "#2"
      ]> $
      <[ "#1" :=
        locinfo: loc_4 ;
        Return (LocInfoE loc_5 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_6 ("x"))))
      ]> $
      <[ "#2" :=
        locinfo: loc_8 ;
        Return (LocInfoE loc_9 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_10 ("y"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [min3]. *)
  Definition impl_min3 (global_min : loc): function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", it_layout i32);
      ("z", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_18 ;
        Return (LocInfoE loc_19 (Call (LocInfoE loc_21 (global_min)) [@{expr} LocInfoE loc_22 (Call (LocInfoE loc_24 (global_min)) [@{expr} LocInfoE loc_25 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_26 ("x"))) ;
               LocInfoE loc_27 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_28 ("y"))) ]) ;
               LocInfoE loc_29 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_30 ("z"))) ]))
      ]> $∅
    )%E
  |}.

  (* Definition of function [min3_inline]. *)
  Definition impl_min3_inline : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", it_layout i32);
      ("z", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_64 ;
        if{IntOp i32, None}: LocInfoE loc_64 ((LocInfoE loc_65 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_66 ("x")))) <{IntOp i32, IntOp i32, i32} (LocInfoE loc_67 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_68 ("y")))))
        then
        locinfo: loc_44 ;
          Goto "#1"
        else
        locinfo: loc_59 ;
          Goto "#4"
      ]> $
      <[ "#1" :=
        locinfo: loc_44 ;
        if{IntOp i32, None}: LocInfoE loc_44 ((LocInfoE loc_45 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_46 ("x")))) <{IntOp i32, IntOp i32, i32} (LocInfoE loc_47 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_48 ("z")))))
        then
        locinfo: loc_37 ;
          Goto "#2"
        else
        locinfo: loc_41 ;
          Goto "#3"
      ]> $
      <[ "#2" :=
        locinfo: loc_37 ;
        Return (LocInfoE loc_38 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_39 ("x"))))
      ]> $
      <[ "#3" :=
        locinfo: loc_41 ;
        Return (LocInfoE loc_42 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_43 ("z"))))
      ]> $
      <[ "#4" :=
        locinfo: loc_59 ;
        if{IntOp i32, None}: LocInfoE loc_59 ((LocInfoE loc_60 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_61 ("y")))) <{IntOp i32, IntOp i32, i32} (LocInfoE loc_62 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_63 ("z")))))
        then
        locinfo: loc_52 ;
          Goto "#5"
        else
        locinfo: loc_56 ;
          Goto "#6"
      ]> $
      <[ "#5" :=
        locinfo: loc_52 ;
        Return (LocInfoE loc_53 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_54 ("y"))))
      ]> $
      <[ "#6" :=
        locinfo: loc_56 ;
        Return (LocInfoE loc_57 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_58 ("z"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [assert_positive]. *)
  Definition impl_assert_positive : function := {|
    f_args := [
      ("x", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_71 ;
        assert{IntOp i32}: (LocInfoE loc_74 ((LocInfoE loc_75 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_76 ("x")))) >{IntOp i32, IntOp i32, i32} (LocInfoE loc_77 (i2v 0 i32)))) ;
        locinfo: loc_72 ;
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [min_inline]. *)
  Definition impl_min_inline : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_80 ;
        Return (LocInfoE loc_81 (IfE (IntOp i32)
               (LocInfoE loc_82 ((LocInfoE loc_83 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_84 ("x")))) <{IntOp i32, IntOp i32, i32} (LocInfoE loc_85 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_86 ("y"))))))
               (LocInfoE loc_87 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_88 ("x"))))
               (LocInfoE loc_89 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_90 ("y"))))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [assert_one_positive]. *)
  Definition impl_assert_one_positive : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", it_layout i32);
      ("z", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_93 ;
        assert{IntOp i32}: (LocInfoE loc_96 ((LocInfoE loc_97 ((LocInfoE loc_98 ((LocInfoE loc_99 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_100 ("x")))) >{IntOp i32, IntOp i32, i32} (LocInfoE loc_101 (i2v 0 i32)))) ||{IntOp i32, IntOp i32, i32} (LocInfoE loc_102 ((LocInfoE loc_103 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_104 ("y")))) >{IntOp i32, IntOp i32, i32} (LocInfoE loc_105 (i2v 0 i32)))))) ||{IntOp i32, IntOp i32, i32} (LocInfoE loc_106 ((LocInfoE loc_107 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_108 ("z")))) >{IntOp i32, IntOp i32, i32} (LocInfoE loc_109 (i2v 0 i32)))))) ;
        locinfo: loc_94 ;
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [assert_all_positive]. *)
  Definition impl_assert_all_positive : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", it_layout i32);
      ("z", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_112 ;
        assert{IntOp i32}: (LocInfoE loc_115 ((LocInfoE loc_116 ((LocInfoE loc_117 ((LocInfoE loc_118 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_119 ("x")))) >{IntOp i32, IntOp i32, i32} (LocInfoE loc_120 (i2v 0 i32)))) &&{IntOp i32, IntOp i32, i32} (LocInfoE loc_121 ((LocInfoE loc_122 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_123 ("y")))) >{IntOp i32, IntOp i32, i32} (LocInfoE loc_124 (i2v 0 i32)))))) &&{IntOp i32, IntOp i32, i32} (LocInfoE loc_125 ((LocInfoE loc_126 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_127 ("z")))) >{IntOp i32, IntOp i32, i32} (LocInfoE loc_128 (i2v 0 i32)))))) ;
        locinfo: loc_113 ;
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [assert_all_same_sign]. *)
  Definition impl_assert_all_same_sign : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", it_layout i32);
      ("z", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_131 ;
        assert{IntOp i32}: (LocInfoE loc_134 ((LocInfoE loc_135 ((LocInfoE loc_136 ((LocInfoE loc_137 ((LocInfoE loc_138 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_139 ("x")))) >{IntOp i32, IntOp i32, i32} (LocInfoE loc_140 (i2v 0 i32)))) &&{IntOp i32, IntOp i32, i32} (LocInfoE loc_141 ((LocInfoE loc_142 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_143 ("y")))) >{IntOp i32, IntOp i32, i32} (LocInfoE loc_144 (i2v 0 i32)))))) &&{IntOp i32, IntOp i32, i32} (LocInfoE loc_145 ((LocInfoE loc_146 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_147 ("z")))) >{IntOp i32, IntOp i32, i32} (LocInfoE loc_148 (i2v 0 i32)))))) ||{IntOp i32, IntOp i32, i32} (LocInfoE loc_149 ((LocInfoE loc_150 ((LocInfoE loc_151 ((LocInfoE loc_152 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_153 ("x")))) <{IntOp i32, IntOp i32, i32} (LocInfoE loc_154 (i2v 0 i32)))) &&{IntOp i32, IntOp i32, i32} (LocInfoE loc_155 ((LocInfoE loc_156 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_157 ("y")))) <{IntOp i32, IntOp i32, i32} (LocInfoE loc_158 (i2v 0 i32)))))) &&{IntOp i32, IntOp i32, i32} (LocInfoE loc_159 ((LocInfoE loc_160 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_161 ("z")))) <{IntOp i32, IntOp i32, i32} (LocInfoE loc_162 (i2v 0 i32)))))))) ;
        locinfo: loc_132 ;
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [arith_with_flag]. *)
  Definition impl_arith_with_flag : function := {|
    f_args := [
      ("flag", it_layout i32);
      ("x", it_layout i32);
      ("y", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_211 ;
        if{IntOp i32, Some "#1"}: LocInfoE loc_211 ((LocInfoE loc_212 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_213 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_214 (i2v 0 i32)))
        then
        locinfo: loc_204 ;
          Goto "#11"
        else
        locinfo: loc_200 ;
          Goto "#12"
      ]> $
      <[ "#1" :=
        locinfo: loc_200 ;
        if{IntOp i32, Some "#2"}: LocInfoE loc_200 ((LocInfoE loc_201 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_202 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_203 (i2v 1 i32)))
        then
        locinfo: loc_193 ;
          Goto "#9"
        else
        locinfo: loc_189 ;
          Goto "#10"
      ]> $
      <[ "#10" :=
        locinfo: loc_189 ;
        Goto "#2"
      ]> $
      <[ "#11" :=
        locinfo: loc_204 ;
        Return (LocInfoE loc_205 ((LocInfoE loc_206 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_207 ("x")))) +{IntOp i32, IntOp i32} (LocInfoE loc_208 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_209 ("y"))))))
      ]> $
      <[ "#12" :=
        locinfo: loc_200 ;
        Goto "#1"
      ]> $
      <[ "#2" :=
        locinfo: loc_189 ;
        if{IntOp i32, Some "#3"}: LocInfoE loc_189 ((LocInfoE loc_190 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_191 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_192 (i2v 2 i32)))
        then
        locinfo: loc_182 ;
          Goto "#7"
        else
        locinfo: loc_178 ;
          Goto "#8"
      ]> $
      <[ "#3" :=
        locinfo: loc_178 ;
        if{IntOp i32, Some "#4"}: LocInfoE loc_178 ((LocInfoE loc_179 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_180 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_181 (i2v 3 i32)))
        then
        locinfo: loc_171 ;
          Goto "#5"
        else
        locinfo: loc_169 ;
          Goto "#6"
      ]> $
      <[ "#4" :=
        locinfo: loc_169 ;
        Return (LocInfoE loc_170 (i2v 0 i32))
      ]> $
      <[ "#5" :=
        locinfo: loc_171 ;
        Return (LocInfoE loc_172 ((LocInfoE loc_173 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_174 ("x")))) /{IntOp i32, IntOp i32} (LocInfoE loc_175 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_176 ("y"))))))
      ]> $
      <[ "#6" :=
        locinfo: loc_169 ;
        Goto "#4"
      ]> $
      <[ "#7" :=
        locinfo: loc_182 ;
        Return (LocInfoE loc_183 ((LocInfoE loc_184 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_185 ("x")))) ×{IntOp i32, IntOp i32} (LocInfoE loc_186 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_187 ("y"))))))
      ]> $
      <[ "#8" :=
        locinfo: loc_178 ;
        Goto "#3"
      ]> $
      <[ "#9" :=
        locinfo: loc_193 ;
        Return (LocInfoE loc_194 ((LocInfoE loc_195 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_196 ("x")))) -{IntOp i32, IntOp i32} (LocInfoE loc_197 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_198 ("y"))))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [arith_with_flag_res]. *)
  Definition impl_arith_with_flag_res : function := {|
    f_args := [
      ("flag", it_layout i32);
      ("x", it_layout i32);
      ("y", it_layout i32)
    ];
    f_local_vars := [
      ("res", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "res" <-{ IntOp i32 } LocInfoE loc_273 (i2v 0 i32) ;
        locinfo: loc_269 ;
        if{IntOp i32, Some "#1"}: LocInfoE loc_269 ((LocInfoE loc_270 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_271 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_272 (i2v 0 i32)))
        then
        locinfo: loc_261 ;
          Goto "#11"
        else
        locinfo: loc_257 ;
          Goto "#12"
      ]> $
      <[ "#1" :=
        locinfo: loc_257 ;
        if{IntOp i32, Some "#2"}: LocInfoE loc_257 ((LocInfoE loc_258 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_259 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_260 (i2v 1 i32)))
        then
        locinfo: loc_249 ;
          Goto "#9"
        else
        locinfo: loc_245 ;
          Goto "#10"
      ]> $
      <[ "#10" :=
        locinfo: loc_245 ;
        Goto "#2"
      ]> $
      <[ "#11" :=
        locinfo: loc_261 ;
        LocInfoE loc_262 ("res") <-{ IntOp i32 }
          LocInfoE loc_263 ((LocInfoE loc_264 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_265 ("x")))) +{IntOp i32, IntOp i32} (LocInfoE loc_266 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_267 ("y"))))) ;
        locinfo: loc_257 ;
        Goto "#1"
      ]> $
      <[ "#12" :=
        locinfo: loc_257 ;
        Goto "#1"
      ]> $
      <[ "#2" :=
        locinfo: loc_245 ;
        if{IntOp i32, Some "#3"}: LocInfoE loc_245 ((LocInfoE loc_246 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_247 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_248 (i2v 2 i32)))
        then
        locinfo: loc_237 ;
          Goto "#7"
        else
        locinfo: loc_233 ;
          Goto "#8"
      ]> $
      <[ "#3" :=
        locinfo: loc_233 ;
        if{IntOp i32, Some "#4"}: LocInfoE loc_233 ((LocInfoE loc_234 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_235 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_236 (i2v 3 i32)))
        then
        locinfo: loc_225 ;
          Goto "#5"
        else
        locinfo: loc_222 ;
          Goto "#6"
      ]> $
      <[ "#4" :=
        locinfo: loc_222 ;
        Return (LocInfoE loc_223 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_224 ("res"))))
      ]> $
      <[ "#5" :=
        locinfo: loc_225 ;
        LocInfoE loc_226 ("res") <-{ IntOp i32 }
          LocInfoE loc_227 ((LocInfoE loc_228 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_229 ("x")))) /{IntOp i32, IntOp i32} (LocInfoE loc_230 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_231 ("y"))))) ;
        locinfo: loc_222 ;
        Goto "#4"
      ]> $
      <[ "#6" :=
        locinfo: loc_222 ;
        Goto "#4"
      ]> $
      <[ "#7" :=
        locinfo: loc_237 ;
        LocInfoE loc_238 ("res") <-{ IntOp i32 }
          LocInfoE loc_239 ((LocInfoE loc_240 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_241 ("x")))) ×{IntOp i32, IntOp i32} (LocInfoE loc_242 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_243 ("y"))))) ;
        locinfo: loc_233 ;
        Goto "#3"
      ]> $
      <[ "#8" :=
        locinfo: loc_233 ;
        Goto "#3"
      ]> $
      <[ "#9" :=
        locinfo: loc_249 ;
        LocInfoE loc_250 ("res") <-{ IntOp i32 }
          LocInfoE loc_251 ((LocInfoE loc_252 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_253 ("x")))) -{IntOp i32, IntOp i32} (LocInfoE loc_254 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_255 ("y"))))) ;
        locinfo: loc_245 ;
        Goto "#2"
      ]> $∅
    )%E
  |}.

  (* Definition of function [arith_with_flag_res2]. *)
  Definition impl_arith_with_flag_res2 : function := {|
    f_args := [
      ("flag", it_layout i32);
      ("x", it_layout i32);
      ("y", it_layout i32)
    ];
    f_local_vars := [
      ("res", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_329 ;
        if{IntOp i32, Some "#1"}: LocInfoE loc_329 ((LocInfoE loc_330 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_331 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_332 (i2v 0 i32)))
        then
        locinfo: loc_321 ;
          Goto "#11"
        else
        locinfo: loc_317 ;
          Goto "#12"
      ]> $
      <[ "#1" :=
        locinfo: loc_317 ;
        if{IntOp i32, Some "#2"}: LocInfoE loc_317 ((LocInfoE loc_318 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_319 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_320 (i2v 1 i32)))
        then
        locinfo: loc_309 ;
          Goto "#9"
        else
        locinfo: loc_305 ;
          Goto "#10"
      ]> $
      <[ "#10" :=
        locinfo: loc_305 ;
        Goto "#2"
      ]> $
      <[ "#11" :=
        locinfo: loc_321 ;
        LocInfoE loc_322 ("res") <-{ IntOp i32 }
          LocInfoE loc_323 ((LocInfoE loc_324 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_325 ("x")))) +{IntOp i32, IntOp i32} (LocInfoE loc_326 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_327 ("y"))))) ;
        locinfo: loc_317 ;
        Goto "#1"
      ]> $
      <[ "#12" :=
        locinfo: loc_317 ;
        Goto "#1"
      ]> $
      <[ "#2" :=
        locinfo: loc_305 ;
        if{IntOp i32, Some "#3"}: LocInfoE loc_305 ((LocInfoE loc_306 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_307 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_308 (i2v 2 i32)))
        then
        locinfo: loc_297 ;
          Goto "#7"
        else
        locinfo: loc_293 ;
          Goto "#8"
      ]> $
      <[ "#3" :=
        locinfo: loc_293 ;
        if{IntOp i32, Some "#4"}: LocInfoE loc_293 ((LocInfoE loc_294 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_295 ("flag")))) ={IntOp i32, IntOp i32, i32} (LocInfoE loc_296 (i2v 3 i32)))
        then
        locinfo: loc_285 ;
          Goto "#5"
        else
        locinfo: loc_282 ;
          Goto "#6"
      ]> $
      <[ "#4" :=
        locinfo: loc_282 ;
        Return (LocInfoE loc_283 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_284 ("res"))))
      ]> $
      <[ "#5" :=
        locinfo: loc_285 ;
        LocInfoE loc_286 ("res") <-{ IntOp i32 }
          LocInfoE loc_287 ((LocInfoE loc_288 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_289 ("x")))) /{IntOp i32, IntOp i32} (LocInfoE loc_290 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_291 ("y"))))) ;
        locinfo: loc_282 ;
        Goto "#4"
      ]> $
      <[ "#6" :=
        locinfo: loc_282 ;
        Goto "#4"
      ]> $
      <[ "#7" :=
        locinfo: loc_297 ;
        LocInfoE loc_298 ("res") <-{ IntOp i32 }
          LocInfoE loc_299 ((LocInfoE loc_300 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_301 ("x")))) ×{IntOp i32, IntOp i32} (LocInfoE loc_302 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_303 ("y"))))) ;
        locinfo: loc_293 ;
        Goto "#3"
      ]> $
      <[ "#8" :=
        locinfo: loc_293 ;
        Goto "#3"
      ]> $
      <[ "#9" :=
        locinfo: loc_309 ;
        LocInfoE loc_310 ("res") <-{ IntOp i32 }
          LocInfoE loc_311 ((LocInfoE loc_312 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_313 ("x")))) -{IntOp i32, IntOp i32} (LocInfoE loc_314 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_315 ("y"))))) ;
        locinfo: loc_305 ;
        Goto "#2"
      ]> $∅
    )%E
  |}.

  (* Definition of function [conditional_set_zero]. *)
  Definition impl_conditional_set_zero : function := {|
    f_args := [
      ("x", void*);
      ("y", void*);
      ("z", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_353 ;
        if{IntOp i32, Some "#1"}: LocInfoE loc_353 ((LocInfoE loc_354 (use{PtrOp, Na1Ord, false} (LocInfoE loc_355 ("x")))) !={PtrOp, PtrOp, i32} (LocInfoE loc_356 (NULL)))
        then
        locinfo: loc_347 ;
          Goto "#4"
        else
        locinfo: loc_344 ;
          Goto "#5"
      ]> $
      <[ "#1" :=
        locinfo: loc_344 ;
        if{PtrOp, None}: LocInfoE loc_344 (use{PtrOp, Na1Ord, false} (LocInfoE loc_345 ("y")))
        then
        locinfo: loc_338 ;
          Goto "#2"
        else
        Goto "#3"
      ]> $
      <[ "#2" :=
        locinfo: loc_338 ;
        LocInfoE loc_340 (!{PtrOp, Na1Ord, false} (LocInfoE loc_341 ("y"))) <-{ IntOp i32 }
          LocInfoE loc_342 (i2v 0 i32) ;
        Return (VOID)
      ]> $
      <[ "#3" :=
        Return (VOID)
      ]> $
      <[ "#4" :=
        locinfo: loc_347 ;
        LocInfoE loc_349 (!{PtrOp, Na1Ord, false} (LocInfoE loc_350 ("x"))) <-{ IntOp i32 }
          LocInfoE loc_351 (i2v 0 i32) ;
        locinfo: loc_344 ;
        Goto "#1"
      ]> $
      <[ "#5" :=
        locinfo: loc_344 ;
        Goto "#1"
      ]> $∅
    )%E
  |}.

  (* Definition of function [indirect_conditional]. *)
  Definition impl_indirect_conditional : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_368 ;
        if{IntOp i32, Some "#1"}: LocInfoE loc_368 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_369 ("x")))
        then
        locinfo: loc_364 ;
          Goto "#2"
        else
        locinfo: loc_360 ;
          Goto "#3"
      ]> $
      <[ "#1" :=
        locinfo: loc_360 ;
        Return (LocInfoE loc_361 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_362 ("y"))))
      ]> $
      <[ "#2" :=
        locinfo: loc_364 ;
        LocInfoE loc_365 ("y") <-{ IntOp i32 } LocInfoE loc_366 (i2v 1 i32) ;
        locinfo: loc_360 ;
        Goto "#1"
      ]> $
      <[ "#3" :=
        locinfo: loc_360 ;
        Goto "#1"
      ]> $∅
    )%E
  |}.

  (* Definition of function [indirect_conditional2]. *)
  Definition impl_indirect_conditional2 : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_372 ;
        LocInfoE loc_384 ("y") <-{ IntOp i32 }
          LocInfoE loc_385 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_386 ("y"))) ;
        locinfo: loc_382 ;
        if{IntOp i32, Some "#1"}: LocInfoE loc_382 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_383 ("x")))
        then
        locinfo: loc_378 ;
          Goto "#2"
        else
        locinfo: loc_374 ;
          Goto "#3"
      ]> $
      <[ "#1" :=
        locinfo: loc_374 ;
        Return (LocInfoE loc_375 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_376 ("y"))))
      ]> $
      <[ "#2" :=
        locinfo: loc_378 ;
        LocInfoE loc_379 ("y") <-{ IntOp i32 } LocInfoE loc_380 (i2v 1 i32) ;
        locinfo: loc_374 ;
        Goto "#1"
      ]> $
      <[ "#3" :=
        locinfo: loc_374 ;
        Goto "#1"
      ]> $∅
    )%E
  |}.

  (* Definition of function [indirect_conditional_ptr]. *)
  Definition impl_indirect_conditional_ptr : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_397 ;
        if{IntOp i32, None}: LocInfoE loc_397 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_398 ("x")))
        then
        locinfo: loc_391 ;
          Goto "#1"
        else
        Goto "#2"
      ]> $
      <[ "#1" :=
        locinfo: loc_391 ;
        LocInfoE loc_393 (!{PtrOp, Na1Ord, false} (LocInfoE loc_394 ("y"))) <-{ IntOp i32 }
          LocInfoE loc_395 (i2v 1 i32) ;
        Return (VOID)
      ]> $
      <[ "#2" :=
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [indirect_conditional_nested_ptr]. *)
  Definition impl_indirect_conditional_nested_ptr : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_411 ;
        if{IntOp i32, None}: LocInfoE loc_411 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_412 ("x")))
        then
        locinfo: loc_403 ;
          Goto "#1"
        else
        Goto "#2"
      ]> $
      <[ "#1" :=
        locinfo: loc_403 ;
        LocInfoE loc_405 (!{PtrOp, Na1Ord, false} (LocInfoE loc_407 (!{PtrOp, Na1Ord, false} (LocInfoE loc_408 ("y"))))) <-{ IntOp i32 }
          LocInfoE loc_409 (i2v 1 i32) ;
        Return (VOID)
      ]> $
      <[ "#2" :=
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [symmetric_conditional]. *)
  Definition impl_symmetric_conditional : function := {|
    f_args := [
      ("x", it_layout i32);
      ("y", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_428 ;
        if{IntOp i32, None}: LocInfoE loc_428 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_429 ("x")))
        then
        locinfo: loc_417 ;
          Goto "#1"
        else
        locinfo: loc_423 ;
          Goto "#2"
      ]> $
      <[ "#1" :=
        locinfo: loc_417 ;
        LocInfoE loc_419 (!{PtrOp, Na1Ord, false} (LocInfoE loc_420 ("y"))) <-{ IntOp i32 }
          LocInfoE loc_421 (i2v 1 i32) ;
        Return (VOID)
      ]> $
      <[ "#2" :=
        locinfo: loc_423 ;
        LocInfoE loc_425 (!{PtrOp, Na1Ord, false} (LocInfoE loc_426 ("y"))) <-{ IntOp i32 }
          LocInfoE loc_427 (i2v 0 i32) ;
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [modify_if_not_null]. *)
  Definition impl_modify_if_not_null : function := {|
    f_args := [
      ("x", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_441 ;
        if{IntOp i32, Some "#1"}: LocInfoE loc_441 ((LocInfoE loc_442 (use{PtrOp, Na1Ord, false} (LocInfoE loc_443 ("x")))) ={PtrOp, PtrOp, i32} (LocInfoE loc_444 (NULL)))
        then
        locinfo: loc_438 ;
          Goto "#2"
        else
        locinfo: loc_433 ;
          Goto "#3"
      ]> $
      <[ "#1" :=
        locinfo: loc_433 ;
        LocInfoE loc_435 (!{PtrOp, Na1Ord, false} (LocInfoE loc_436 ("x"))) <-{ IntOp i32 }
          LocInfoE loc_437 (i2v 5 i32) ;
        Return (VOID)
      ]> $
      <[ "#2" :=
        locinfo: loc_438 ;
        Return (VOID)
      ]> $
      <[ "#3" :=
        locinfo: loc_433 ;
        Goto "#1"
      ]> $∅
    )%E
  |}.

  (* Definition of function [assert_not_null]. *)
  Definition impl_assert_not_null : function := {|
    f_args := [
      ("x", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_447 ;
        assert{IntOp i32}: (LocInfoE loc_453 ((LocInfoE loc_454 (use{PtrOp, Na1Ord, false} (LocInfoE loc_455 ("x")))) !={PtrOp, PtrOp, i32} (LocInfoE loc_456 (NULL)))) ;
        locinfo: loc_448 ;
        LocInfoE loc_450 (!{PtrOp, Na1Ord, false} (LocInfoE loc_451 ("x"))) <-{ IntOp i32 }
          LocInfoE loc_452 (i2v 5 i32) ;
        Return (VOID)
      ]> $∅
    )%E
  |}.
End code.
