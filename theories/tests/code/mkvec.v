From caesium Require Export notation.
From caesium Require Import tactics.
From quiver.base Require Import annotations.
Set Default Proof Using "Type*".

(* Generated from [examples/src/casestudies/arrays/array_no_annot.c]. *)
Section code.


  (* Definition of struct [vector]. *)
  Program Definition struct_vector := {|
    sl_members := [
      (Some "data", void*);
      (Some "len", it_layout i32);
      (None, Layout 4%nat 0%nat)
    ];
  |}.
  Solve Obligations with solve_struct_obligations.

  (* Definition of function [mkvec]. *)
  Definition impl_mkvec (global_xmalloc global_xzalloc : loc): function := {|
    f_args := [
      ("n", it_layout i32)
    ];
    f_local_vars := [
      ("s", it_layout size_t);
      ("vec", void*)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "s" <-{ IntOp size_t }
          (i2v (it_layout i32).(ly_size) size_t) ×{IntOp size_t, IntOp size_t} (UnOp (CastOp $ IntOp size_t) (IntOp i32) (use{IntOp i32, Na1Ord, false} ("n"))) ;
        "vec" <-{ PtrOp }
          UnOp (CastOp $ PtrOp) (PtrOp) (Call (global_xmalloc) [@{expr} i2v (layout_of struct_vector).(ly_size) size_t ]) ;
        (!{PtrOp, Na1Ord, false} ("vec")) at{struct_vector} "data" <-{ PtrOp }
          Call (global_xzalloc) [@{expr} use{IntOp size_t, Na1Ord, false} ("s") ] ;
        (!{PtrOp, Na1Ord, false} ("vec")) at{struct_vector} "len" <-{ IntOp i32 }
          use{IntOp i32, Na1Ord, false} ("n") ;
        Return (use{PtrOp, Na1Ord, false} ("vec"))
      ]> $∅
    )%E
  |}.

End code.