From caesium Require Export notation.
From caesium Require Import tactics.
From quiver.base Require Import annotations.
Set Default Proof Using "Type".

(* Generated from [src/datatypes.c]. *)
Section code.
  Definition file_0 : string := "src/datatypes.c".
  Definition loc_2 : location_info := LocationInfo file_0 31 4 31 15.
  Definition loc_3 : location_info := LocationInfo file_0 31 11 31 14.
  Definition loc_4 : location_info := LocationInfo file_0 31 11 31 14.
  Definition loc_5 : location_info := LocationInfo file_0 31 11 31 12.
  Definition loc_8 : location_info := LocationInfo file_0 35 4 35 16.
  Definition loc_9 : location_info := LocationInfo file_0 35 11 35 15.
  Definition loc_10 : location_info := LocationInfo file_0 35 11 35 15.
  Definition loc_11 : location_info := LocationInfo file_0 35 11 35 12.
  Definition loc_12 : location_info := LocationInfo file_0 35 11 35 12.
  Definition loc_15 : location_info := LocationInfo file_0 39 4 39 18.
  Definition loc_16 : location_info := LocationInfo file_0 39 11 39 17.
  Definition loc_17 : location_info := LocationInfo file_0 39 11 39 17.
  Definition loc_18 : location_info := LocationInfo file_0 39 11 39 14.
  Definition loc_19 : location_info := LocationInfo file_0 39 11 39 14.
  Definition loc_22 : location_info := LocationInfo file_0 43 4 43 15.
  Definition loc_23 : location_info := LocationInfo file_0 43 4 43 10.
  Definition loc_24 : location_info := LocationInfo file_0 43 4 43 7.
  Definition loc_25 : location_info := LocationInfo file_0 43 4 43 7.
  Definition loc_26 : location_info := LocationInfo file_0 43 13 43 14.
  Definition loc_27 : location_info := LocationInfo file_0 43 13 43 14.
  Definition loc_30 : location_info := LocationInfo file_0 47 4 47 23.
  Definition loc_31 : location_info := LocationInfo file_0 47 11 47 22.
  Definition loc_32 : location_info := LocationInfo file_0 47 11 47 22.
  Definition loc_33 : location_info := LocationInfo file_0 47 12 47 22.
  Definition loc_34 : location_info := LocationInfo file_0 47 12 47 22.
  Definition loc_35 : location_info := LocationInfo file_0 47 13 47 16.
  Definition loc_36 : location_info := LocationInfo file_0 47 13 47 16.
  Definition loc_39 : location_info := LocationInfo file_0 51 4 51 23.
  Definition loc_40 : location_info := LocationInfo file_0 51 11 51 22.
  Definition loc_41 : location_info := LocationInfo file_0 51 11 51 22.
  Definition loc_42 : location_info := LocationInfo file_0 51 11 51 14.
  Definition loc_43 : location_info := LocationInfo file_0 51 11 51 14.
  Definition loc_46 : location_info := LocationInfo file_0 55 4 55 21.
  Definition loc_47 : location_info := LocationInfo file_0 55 11 55 20.
  Definition loc_48 : location_info := LocationInfo file_0 55 11 55 20.
  Definition loc_49 : location_info := LocationInfo file_0 55 11 55 14.
  Definition loc_50 : location_info := LocationInfo file_0 55 11 55 14.
  Definition loc_53 : location_info := LocationInfo file_0 59 4 59 42.
  Definition loc_54 : location_info := LocationInfo file_0 60 4 60 13.
  Definition loc_55 : location_info := LocationInfo file_0 60 11 60 12.
  Definition loc_56 : location_info := LocationInfo file_0 60 11 60 12.
  Definition loc_58 : location_info := LocationInfo file_0 59 23 59 24.
  Definition loc_59 : location_info := LocationInfo file_0 59 31 59 32.
  Definition loc_60 : location_info := LocationInfo file_0 59 39 59 40.
  Definition loc_65 : location_info := LocationInfo file_0 64 4 64 44.
  Definition loc_66 : location_info := LocationInfo file_0 65 4 65 17.
  Definition loc_67 : location_info := LocationInfo file_0 65 11 65 16.
  Definition loc_68 : location_info := LocationInfo file_0 65 11 65 16.
  Definition loc_69 : location_info := LocationInfo file_0 64 21 64 43.
  Definition loc_70 : location_info := LocationInfo file_0 64 21 64 26.
  Definition loc_71 : location_info := LocationInfo file_0 64 21 64 26.
  Definition loc_72 : location_info := LocationInfo file_0 64 27 64 42.
  Definition loc_77 : location_info := LocationInfo file_0 69 4 69 26.
  Definition loc_78 : location_info := LocationInfo file_0 69 11 69 25.
  Definition loc_79 : location_info := LocationInfo file_0 69 11 69 25.
  Definition loc_80 : location_info := LocationInfo file_0 69 11 69 25.
  Definition loc_81 : location_info := LocationInfo file_0 69 11 69 22.
  Definition loc_82 : location_info := LocationInfo file_0 69 11 69 22.
  Definition loc_83 : location_info := LocationInfo file_0 69 11 69 12.
  Definition loc_84 : location_info := LocationInfo file_0 69 11 69 12.
  Definition loc_85 : location_info := LocationInfo file_0 69 23 69 24.
  Definition loc_86 : location_info := LocationInfo file_0 69 23 69 24.
  Definition loc_89 : location_info := LocationInfo file_0 73 4 73 16.
  Definition loc_90 : location_info := LocationInfo file_0 73 11 73 15.
  Definition loc_91 : location_info := LocationInfo file_0 73 11 73 15.
  Definition loc_92 : location_info := LocationInfo file_0 73 11 73 15.
  Definition loc_93 : location_info := LocationInfo file_0 73 11 73 12.
  Definition loc_94 : location_info := LocationInfo file_0 73 11 73 12.
  Definition loc_95 : location_info := LocationInfo file_0 73 13 73 14.
  Definition loc_96 : location_info := LocationInfo file_0 73 13 73 14.
  Definition loc_99 : location_info := LocationInfo file_0 79 4 79 13.
  Definition loc_100 : location_info := LocationInfo file_0 80 4 80 13.
  Definition loc_101 : location_info := LocationInfo file_0 81 4 81 13.
  Definition loc_102 : location_info := LocationInfo file_0 82 4 82 30.
  Definition loc_103 : location_info := LocationInfo file_0 82 11 82 29.
  Definition loc_104 : location_info := LocationInfo file_0 82 11 82 22.
  Definition loc_105 : location_info := LocationInfo file_0 82 11 82 15.
  Definition loc_106 : location_info := LocationInfo file_0 82 11 82 15.
  Definition loc_107 : location_info := LocationInfo file_0 82 11 82 15.
  Definition loc_108 : location_info := LocationInfo file_0 82 11 82 12.
  Definition loc_109 : location_info := LocationInfo file_0 82 11 82 12.
  Definition loc_110 : location_info := LocationInfo file_0 82 13 82 14.
  Definition loc_111 : location_info := LocationInfo file_0 82 18 82 22.
  Definition loc_112 : location_info := LocationInfo file_0 82 18 82 22.
  Definition loc_113 : location_info := LocationInfo file_0 82 18 82 22.
  Definition loc_114 : location_info := LocationInfo file_0 82 18 82 19.
  Definition loc_115 : location_info := LocationInfo file_0 82 18 82 19.
  Definition loc_116 : location_info := LocationInfo file_0 82 20 82 21.
  Definition loc_117 : location_info := LocationInfo file_0 82 25 82 29.
  Definition loc_118 : location_info := LocationInfo file_0 82 25 82 29.
  Definition loc_119 : location_info := LocationInfo file_0 82 25 82 29.
  Definition loc_120 : location_info := LocationInfo file_0 82 25 82 26.
  Definition loc_121 : location_info := LocationInfo file_0 82 25 82 26.
  Definition loc_122 : location_info := LocationInfo file_0 82 27 82 28.
  Definition loc_123 : location_info := LocationInfo file_0 81 4 81 8.
  Definition loc_124 : location_info := LocationInfo file_0 81 4 81 8.
  Definition loc_125 : location_info := LocationInfo file_0 81 4 81 5.
  Definition loc_126 : location_info := LocationInfo file_0 81 4 81 5.
  Definition loc_127 : location_info := LocationInfo file_0 81 6 81 7.
  Definition loc_128 : location_info := LocationInfo file_0 81 11 81 12.
  Definition loc_129 : location_info := LocationInfo file_0 80 4 80 8.
  Definition loc_130 : location_info := LocationInfo file_0 80 4 80 8.
  Definition loc_131 : location_info := LocationInfo file_0 80 4 80 5.
  Definition loc_132 : location_info := LocationInfo file_0 80 4 80 5.
  Definition loc_133 : location_info := LocationInfo file_0 80 6 80 7.
  Definition loc_134 : location_info := LocationInfo file_0 80 11 80 12.
  Definition loc_135 : location_info := LocationInfo file_0 79 4 79 8.
  Definition loc_136 : location_info := LocationInfo file_0 79 4 79 8.
  Definition loc_137 : location_info := LocationInfo file_0 79 4 79 5.
  Definition loc_138 : location_info := LocationInfo file_0 79 4 79 5.
  Definition loc_139 : location_info := LocationInfo file_0 79 6 79 7.
  Definition loc_140 : location_info := LocationInfo file_0 79 11 79 12.
  Definition loc_143 : location_info := LocationInfo file_0 87 4 87 13.
  Definition loc_144 : location_info := LocationInfo file_0 88 4 88 13.
  Definition loc_145 : location_info := LocationInfo file_0 89 4 89 13.
  Definition loc_146 : location_info := LocationInfo file_0 90 4 90 30.
  Definition loc_147 : location_info := LocationInfo file_0 90 11 90 29.
  Definition loc_148 : location_info := LocationInfo file_0 90 11 90 22.
  Definition loc_149 : location_info := LocationInfo file_0 90 11 90 15.
  Definition loc_150 : location_info := LocationInfo file_0 90 11 90 15.
  Definition loc_151 : location_info := LocationInfo file_0 90 11 90 15.
  Definition loc_152 : location_info := LocationInfo file_0 90 11 90 12.
  Definition loc_153 : location_info := LocationInfo file_0 90 11 90 12.
  Definition loc_154 : location_info := LocationInfo file_0 90 13 90 14.
  Definition loc_155 : location_info := LocationInfo file_0 90 18 90 22.
  Definition loc_156 : location_info := LocationInfo file_0 90 18 90 22.
  Definition loc_157 : location_info := LocationInfo file_0 90 18 90 22.
  Definition loc_158 : location_info := LocationInfo file_0 90 18 90 19.
  Definition loc_159 : location_info := LocationInfo file_0 90 18 90 19.
  Definition loc_160 : location_info := LocationInfo file_0 90 20 90 21.
  Definition loc_161 : location_info := LocationInfo file_0 90 25 90 29.
  Definition loc_162 : location_info := LocationInfo file_0 90 25 90 29.
  Definition loc_163 : location_info := LocationInfo file_0 90 25 90 29.
  Definition loc_164 : location_info := LocationInfo file_0 90 25 90 26.
  Definition loc_165 : location_info := LocationInfo file_0 90 25 90 26.
  Definition loc_166 : location_info := LocationInfo file_0 90 27 90 28.
  Definition loc_167 : location_info := LocationInfo file_0 89 4 89 8.
  Definition loc_168 : location_info := LocationInfo file_0 89 4 89 8.
  Definition loc_169 : location_info := LocationInfo file_0 89 4 89 5.
  Definition loc_170 : location_info := LocationInfo file_0 89 4 89 5.
  Definition loc_171 : location_info := LocationInfo file_0 89 6 89 7.
  Definition loc_172 : location_info := LocationInfo file_0 89 11 89 12.
  Definition loc_173 : location_info := LocationInfo file_0 88 4 88 8.
  Definition loc_174 : location_info := LocationInfo file_0 88 4 88 8.
  Definition loc_175 : location_info := LocationInfo file_0 88 4 88 5.
  Definition loc_176 : location_info := LocationInfo file_0 88 4 88 5.
  Definition loc_177 : location_info := LocationInfo file_0 88 6 88 7.
  Definition loc_178 : location_info := LocationInfo file_0 88 11 88 12.
  Definition loc_179 : location_info := LocationInfo file_0 87 4 87 8.
  Definition loc_180 : location_info := LocationInfo file_0 87 4 87 8.
  Definition loc_181 : location_info := LocationInfo file_0 87 4 87 5.
  Definition loc_182 : location_info := LocationInfo file_0 87 4 87 5.
  Definition loc_183 : location_info := LocationInfo file_0 87 6 87 7.
  Definition loc_184 : location_info := LocationInfo file_0 87 11 87 12.
  Definition loc_187 : location_info := LocationInfo file_0 95 4 95 13.
  Definition loc_188 : location_info := LocationInfo file_0 96 4 96 19.
  Definition loc_189 : location_info := LocationInfo file_0 97 4 97 13.
  Definition loc_190 : location_info := LocationInfo file_0 98 4 98 13.
  Definition loc_191 : location_info := LocationInfo file_0 99 4 99 30.
  Definition loc_192 : location_info := LocationInfo file_0 99 11 99 29.
  Definition loc_193 : location_info := LocationInfo file_0 99 11 99 22.
  Definition loc_194 : location_info := LocationInfo file_0 99 11 99 15.
  Definition loc_195 : location_info := LocationInfo file_0 99 11 99 15.
  Definition loc_196 : location_info := LocationInfo file_0 99 11 99 15.
  Definition loc_197 : location_info := LocationInfo file_0 99 11 99 12.
  Definition loc_198 : location_info := LocationInfo file_0 99 11 99 12.
  Definition loc_199 : location_info := LocationInfo file_0 99 13 99 14.
  Definition loc_200 : location_info := LocationInfo file_0 99 18 99 22.
  Definition loc_201 : location_info := LocationInfo file_0 99 18 99 22.
  Definition loc_202 : location_info := LocationInfo file_0 99 18 99 22.
  Definition loc_203 : location_info := LocationInfo file_0 99 18 99 19.
  Definition loc_204 : location_info := LocationInfo file_0 99 18 99 19.
  Definition loc_205 : location_info := LocationInfo file_0 99 20 99 21.
  Definition loc_206 : location_info := LocationInfo file_0 99 25 99 29.
  Definition loc_207 : location_info := LocationInfo file_0 99 25 99 29.
  Definition loc_208 : location_info := LocationInfo file_0 99 25 99 29.
  Definition loc_209 : location_info := LocationInfo file_0 99 25 99 26.
  Definition loc_210 : location_info := LocationInfo file_0 99 25 99 26.
  Definition loc_211 : location_info := LocationInfo file_0 99 27 99 28.
  Definition loc_212 : location_info := LocationInfo file_0 98 4 98 8.
  Definition loc_213 : location_info := LocationInfo file_0 98 4 98 8.
  Definition loc_214 : location_info := LocationInfo file_0 98 4 98 5.
  Definition loc_215 : location_info := LocationInfo file_0 98 4 98 5.
  Definition loc_216 : location_info := LocationInfo file_0 98 6 98 7.
  Definition loc_217 : location_info := LocationInfo file_0 98 11 98 12.
  Definition loc_218 : location_info := LocationInfo file_0 97 4 97 8.
  Definition loc_219 : location_info := LocationInfo file_0 97 4 97 8.
  Definition loc_220 : location_info := LocationInfo file_0 97 4 97 5.
  Definition loc_221 : location_info := LocationInfo file_0 97 4 97 5.
  Definition loc_222 : location_info := LocationInfo file_0 97 6 97 7.
  Definition loc_223 : location_info := LocationInfo file_0 97 11 97 12.
  Definition loc_224 : location_info := LocationInfo file_0 96 13 96 18.
  Definition loc_225 : location_info := LocationInfo file_0 96 14 96 18.
  Definition loc_226 : location_info := LocationInfo file_0 96 14 96 18.
  Definition loc_227 : location_info := LocationInfo file_0 96 14 96 15.
  Definition loc_228 : location_info := LocationInfo file_0 96 14 96 15.
  Definition loc_229 : location_info := LocationInfo file_0 96 16 96 17.
  Definition loc_232 : location_info := LocationInfo file_0 95 4 95 8.
  Definition loc_233 : location_info := LocationInfo file_0 95 4 95 8.
  Definition loc_234 : location_info := LocationInfo file_0 95 4 95 5.
  Definition loc_235 : location_info := LocationInfo file_0 95 4 95 5.
  Definition loc_236 : location_info := LocationInfo file_0 95 6 95 7.
  Definition loc_237 : location_info := LocationInfo file_0 95 11 95 12.
  Definition loc_240 : location_info := LocationInfo file_0 104 4 104 13.
  Definition loc_241 : location_info := LocationInfo file_0 105 4 105 19.
  Definition loc_242 : location_info := LocationInfo file_0 106 4 106 13.
  Definition loc_243 : location_info := LocationInfo file_0 107 4 107 13.
  Definition loc_244 : location_info := LocationInfo file_0 108 4 108 30.
  Definition loc_245 : location_info := LocationInfo file_0 108 11 108 29.
  Definition loc_246 : location_info := LocationInfo file_0 108 11 108 22.
  Definition loc_247 : location_info := LocationInfo file_0 108 11 108 15.
  Definition loc_248 : location_info := LocationInfo file_0 108 11 108 15.
  Definition loc_249 : location_info := LocationInfo file_0 108 11 108 15.
  Definition loc_250 : location_info := LocationInfo file_0 108 11 108 12.
  Definition loc_251 : location_info := LocationInfo file_0 108 11 108 12.
  Definition loc_252 : location_info := LocationInfo file_0 108 13 108 14.
  Definition loc_253 : location_info := LocationInfo file_0 108 18 108 22.
  Definition loc_254 : location_info := LocationInfo file_0 108 18 108 22.
  Definition loc_255 : location_info := LocationInfo file_0 108 18 108 22.
  Definition loc_256 : location_info := LocationInfo file_0 108 18 108 19.
  Definition loc_257 : location_info := LocationInfo file_0 108 18 108 19.
  Definition loc_258 : location_info := LocationInfo file_0 108 20 108 21.
  Definition loc_259 : location_info := LocationInfo file_0 108 25 108 29.
  Definition loc_260 : location_info := LocationInfo file_0 108 25 108 29.
  Definition loc_261 : location_info := LocationInfo file_0 108 25 108 29.
  Definition loc_262 : location_info := LocationInfo file_0 108 25 108 26.
  Definition loc_263 : location_info := LocationInfo file_0 108 25 108 26.
  Definition loc_264 : location_info := LocationInfo file_0 108 27 108 28.
  Definition loc_265 : location_info := LocationInfo file_0 107 4 107 8.
  Definition loc_266 : location_info := LocationInfo file_0 107 4 107 8.
  Definition loc_267 : location_info := LocationInfo file_0 107 4 107 5.
  Definition loc_268 : location_info := LocationInfo file_0 107 4 107 5.
  Definition loc_269 : location_info := LocationInfo file_0 107 6 107 7.
  Definition loc_270 : location_info := LocationInfo file_0 107 11 107 12.
  Definition loc_271 : location_info := LocationInfo file_0 106 4 106 8.
  Definition loc_272 : location_info := LocationInfo file_0 106 4 106 8.
  Definition loc_273 : location_info := LocationInfo file_0 106 4 106 5.
  Definition loc_274 : location_info := LocationInfo file_0 106 4 106 5.
  Definition loc_275 : location_info := LocationInfo file_0 106 6 106 7.
  Definition loc_276 : location_info := LocationInfo file_0 106 11 106 12.
  Definition loc_277 : location_info := LocationInfo file_0 105 13 105 18.
  Definition loc_278 : location_info := LocationInfo file_0 105 14 105 18.
  Definition loc_279 : location_info := LocationInfo file_0 105 14 105 18.
  Definition loc_280 : location_info := LocationInfo file_0 105 14 105 15.
  Definition loc_281 : location_info := LocationInfo file_0 105 14 105 15.
  Definition loc_282 : location_info := LocationInfo file_0 105 16 105 17.
  Definition loc_285 : location_info := LocationInfo file_0 104 4 104 8.
  Definition loc_286 : location_info := LocationInfo file_0 104 4 104 8.
  Definition loc_287 : location_info := LocationInfo file_0 104 4 104 5.
  Definition loc_288 : location_info := LocationInfo file_0 104 4 104 5.
  Definition loc_289 : location_info := LocationInfo file_0 104 6 104 7.
  Definition loc_290 : location_info := LocationInfo file_0 104 11 104 12.
  Definition loc_293 : location_info := LocationInfo file_0 114 4 114 13.
  Definition loc_294 : location_info := LocationInfo file_0 115 4 115 16.
  Definition loc_295 : location_info := LocationInfo file_0 115 11 115 15.
  Definition loc_296 : location_info := LocationInfo file_0 115 11 115 15.
  Definition loc_297 : location_info := LocationInfo file_0 115 11 115 15.
  Definition loc_298 : location_info := LocationInfo file_0 115 11 115 12.
  Definition loc_299 : location_info := LocationInfo file_0 115 11 115 12.
  Definition loc_300 : location_info := LocationInfo file_0 115 13 115 14.
  Definition loc_301 : location_info := LocationInfo file_0 114 4 114 8.
  Definition loc_302 : location_info := LocationInfo file_0 114 4 114 8.
  Definition loc_303 : location_info := LocationInfo file_0 114 4 114 5.
  Definition loc_304 : location_info := LocationInfo file_0 114 4 114 5.
  Definition loc_305 : location_info := LocationInfo file_0 114 6 114 7.
  Definition loc_306 : location_info := LocationInfo file_0 114 11 114 12.
  Definition loc_307 : location_info := LocationInfo file_0 114 11 114 12.

  (* Definition of struct [triple]. *)
  Program Definition struct_triple := {|
    sl_members := [
      (Some "x", it_layout i32);
      (Some "y", it_layout i32);
      (Some "z", it_layout i32)
    ];
  |}.
  Solve Obligations with solve_struct_obligations.

  (* Definition of struct [mixed]. *)
  Program Definition struct_mixed := {|
    sl_members := [
      (Some "x", it_layout u8);
      (None, Layout 3%nat 0%nat);
      (Some "triple", layout_of struct_triple);
      (Some "next", void*);
      (Some "foo", void*)
    ];
  |}.
  Solve Obligations with solve_struct_obligations.

  (* Definition of struct [buffer]. *)
  Program Definition struct_buffer := {|
    sl_members := [
      (Some "contents", mk_array_layout (it_layout u8) 1024);
      (Some "next", void*)
    ];
  |}.
  Solve Obligations with solve_struct_obligations.

  (* Definition of function [proj_x]. *)
  Definition impl_proj_x : function := {|
    f_args := [
      ("t", layout_of struct_triple)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_2 ;
        Return (LocInfoE loc_3 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_4 ((LocInfoE loc_5 ("t")) at{struct_triple} "x"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [deref_x]. *)
  Definition impl_deref_x : function := {|
    f_args := [
      ("t", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_8 ;
        Return (LocInfoE loc_9 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_10 ((LocInfoE loc_11 (!{PtrOp, Na1Ord, false} (LocInfoE loc_12 ("t")))) at{struct_triple} "x"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [get_x]. *)
  Definition impl_get_x : function := {|
    f_args := [
      ("mix", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_15 ;
        Return (LocInfoE loc_16 (use{IntOp u8, Na1Ord, false} (LocInfoE loc_17 ((LocInfoE loc_18 (!{PtrOp, Na1Ord, false} (LocInfoE loc_19 ("mix")))) at{struct_mixed} "x"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [set_x]. *)
  Definition impl_set_x : function := {|
    f_args := [
      ("mix", void*);
      ("y", it_layout u8)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_22 ;
        LocInfoE loc_23 ((LocInfoE loc_24 (!{PtrOp, Na1Ord, false} (LocInfoE loc_25 ("mix")))) at{struct_mixed} "x") <-{ IntOp u8 }
          LocInfoE loc_26 (use{IntOp u8, Na1Ord, false} (LocInfoE loc_27 ("y"))) ;
        Return (VOID)
      ]> $∅
    )%E
  |}.

  (* Definition of function [get_foo]. *)
  Definition impl_get_foo : function := {|
    f_args := [
      ("mix", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_30 ;
        Return (LocInfoE loc_31 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_33 (!{PtrOp, Na1Ord, false} (LocInfoE loc_34 ((LocInfoE loc_35 (!{PtrOp, Na1Ord, false} (LocInfoE loc_36 ("mix")))) at{struct_mixed} "foo"))))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [get_triple]. *)
  Definition impl_get_triple : function := {|
    f_args := [
      ("mix", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_39 ;
        Return (LocInfoE loc_40 (use{StructOp struct_triple ([ IntOp i32 ; IntOp i32 ; IntOp i32 ]), Na1Ord, false} (LocInfoE loc_41 ((LocInfoE loc_42 (!{PtrOp, Na1Ord, false} (LocInfoE loc_43 ("mix")))) at{struct_mixed} "triple"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [get_next]. *)
  Definition impl_get_next : function := {|
    f_args := [
      ("mix", void*)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_46 ;
        Return (LocInfoE loc_47 (use{PtrOp, Na1Ord, false} (LocInfoE loc_48 ((LocInfoE loc_49 (!{PtrOp, Na1Ord, false} (LocInfoE loc_50 ("mix")))) at{struct_mixed} "next"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [new_triple]. *)
  Definition impl_new_triple : function := {|
    f_args := [
    ];
    f_local_vars := [
      ("t", layout_of struct_triple)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "t" <-{ StructOp struct_triple ([ IntOp i32 ; IntOp i32 ; IntOp i32 ]) }
          StructInit struct_triple [
            ("x", LocInfoE loc_58 (i2v 0 i32) : expr) ;
            ("y", LocInfoE loc_59 (i2v 0 i32) : expr) ;
            ("z", LocInfoE loc_60 (i2v 0 i32) : expr)
          ] ;
        locinfo: loc_54 ;
        Return (LocInfoE loc_55 (use{StructOp struct_triple ([ IntOp i32 ; IntOp i32 ; IntOp i32 ]), Na1Ord, false} (LocInfoE loc_56 ("t"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [alloc_mixed]. *)
  Definition impl_alloc_mixed (global_alloc : loc): function := {|
    f_args := [
    ];
    f_local_vars := [
      ("mixed", void*)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "mixed" <-{ PtrOp }
          LocInfoE loc_69 (UnOp (CastOp $ PtrOp) (PtrOp) (LocInfoE loc_69 (Call (LocInfoE loc_71 (global_alloc)) [@{expr} LocInfoE loc_72 (i2v (void*).(ly_size) size_t) ]))) ;
        locinfo: loc_66 ;
        Return (LocInfoE loc_67 (use{PtrOp, Na1Ord, false} (LocInfoE loc_68 ("mixed"))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [buffer_sub]. *)
  Definition impl_buffer_sub : function := {|
    f_args := [
      ("b", void*);
      ("i", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_77 ;
        Return (LocInfoE loc_78 (use{IntOp u8, Na1Ord, false} (LocInfoE loc_80 ((LocInfoE loc_82 ((LocInfoE loc_83 (!{PtrOp, Na1Ord, false} (LocInfoE loc_84 ("b")))) at{struct_buffer} "contents")) at_offset{it_layout u8, PtrOp, IntOp i32} (LocInfoE loc_85 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_86 ("i"))))))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [array_sub]. *)
  Definition impl_array_sub : function := {|
    f_args := [
      ("a", void*);
      ("i", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_89 ;
        Return (LocInfoE loc_90 (use{IntOp u8, Na1Ord, false} (LocInfoE loc_92 ((LocInfoE loc_93 (!{PtrOp, Na1Ord, false} (LocInfoE loc_94 ("a")))) at_offset{it_layout u8, PtrOp, IntOp i32} (LocInfoE loc_95 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_96 ("i"))))))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [array_local_triple]. *)
  Definition impl_array_local_triple : function := {|
    f_args := [
    ];
    f_local_vars := [
      ("t", mk_array_layout (it_layout i32) 3)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_99 ;
        LocInfoE loc_136 ((LocInfoE loc_138 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_139 (i2v 0 i32))) <-{ IntOp i32 }
          LocInfoE loc_140 (i2v 1 i32) ;
        locinfo: loc_100 ;
        LocInfoE loc_130 ((LocInfoE loc_132 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_133 (i2v 1 i32))) <-{ IntOp i32 }
          LocInfoE loc_134 (i2v 2 i32) ;
        locinfo: loc_101 ;
        LocInfoE loc_124 ((LocInfoE loc_126 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_127 (i2v 2 i32))) <-{ IntOp i32 }
          LocInfoE loc_128 (i2v 3 i32) ;
        locinfo: loc_102 ;
        Return (LocInfoE loc_103 ((LocInfoE loc_104 ((LocInfoE loc_105 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_107 ((LocInfoE loc_109 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_110 (i2v 0 i32)))))) +{IntOp i32, IntOp i32} (LocInfoE loc_111 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_113 ((LocInfoE loc_115 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_116 (i2v 1 i32)))))))) +{IntOp i32, IntOp i32} (LocInfoE loc_117 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_119 ((LocInfoE loc_121 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_122 (i2v 2 i32))))))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [array_local_triple2]. *)
  Definition impl_array_local_triple2 : function := {|
    f_args := [
    ];
    f_local_vars := [
      ("t", mk_array_layout (it_layout i32) 3)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_143 ;
        LocInfoE loc_180 ((LocInfoE loc_182 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_183 (i2v 1 i32))) <-{ IntOp i32 }
          LocInfoE loc_184 (i2v 2 i32) ;
        locinfo: loc_144 ;
        LocInfoE loc_174 ((LocInfoE loc_176 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_177 (i2v 0 i32))) <-{ IntOp i32 }
          LocInfoE loc_178 (i2v 1 i32) ;
        locinfo: loc_145 ;
        LocInfoE loc_168 ((LocInfoE loc_170 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_171 (i2v 2 i32))) <-{ IntOp i32 }
          LocInfoE loc_172 (i2v 3 i32) ;
        locinfo: loc_146 ;
        Return (LocInfoE loc_147 ((LocInfoE loc_148 ((LocInfoE loc_149 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_151 ((LocInfoE loc_153 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_154 (i2v 0 i32)))))) +{IntOp i32, IntOp i32} (LocInfoE loc_155 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_157 ((LocInfoE loc_159 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_160 (i2v 1 i32)))))))) +{IntOp i32, IntOp i32} (LocInfoE loc_161 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_163 ((LocInfoE loc_165 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_166 (i2v 2 i32))))))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [array_local_triple3]. *)
  Definition impl_array_local_triple3 : function := {|
    f_args := [
    ];
    f_local_vars := [
      ("t", mk_array_layout (it_layout i32) 3);
      ("p", void*)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_187 ;
        LocInfoE loc_233 ((LocInfoE loc_235 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_236 (i2v 0 i32))) <-{ IntOp i32 }
          LocInfoE loc_237 (i2v 1 i32) ;
        "p" <-{ PtrOp }
          LocInfoE loc_224 (&(LocInfoE loc_226 ((LocInfoE loc_228 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_229 (i2v 1 i32))))) ;
        locinfo: loc_189 ;
        LocInfoE loc_219 ((LocInfoE loc_220 (!{PtrOp, Na1Ord, false} (LocInfoE loc_221 ("p")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_222 (i2v 0 i32))) <-{ IntOp i32 }
          LocInfoE loc_223 (i2v 2 i32) ;
        locinfo: loc_190 ;
        LocInfoE loc_213 ((LocInfoE loc_214 (!{PtrOp, Na1Ord, false} (LocInfoE loc_215 ("p")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_216 (i2v 1 i32))) <-{ IntOp i32 }
          LocInfoE loc_217 (i2v 3 i32) ;
        locinfo: loc_191 ;
        Return (LocInfoE loc_192 ((LocInfoE loc_193 ((LocInfoE loc_194 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_196 ((LocInfoE loc_198 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_199 (i2v 0 i32)))))) +{IntOp i32, IntOp i32} (LocInfoE loc_200 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_202 ((LocInfoE loc_203 (!{PtrOp, Na1Ord, false} (LocInfoE loc_204 ("p")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_205 (i2v 0 i32)))))))) +{IntOp i32, IntOp i32} (LocInfoE loc_206 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_208 ((LocInfoE loc_209 (!{PtrOp, Na1Ord, false} (LocInfoE loc_210 ("p")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_211 (i2v 1 i32))))))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [array_local_triple4]. *)
  Definition impl_array_local_triple4 : function := {|
    f_args := [
    ];
    f_local_vars := [
      ("t", mk_array_layout (it_layout i32) 3);
      ("p", void*)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_240 ;
        LocInfoE loc_286 ((LocInfoE loc_288 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_289 (i2v 0 i32))) <-{ IntOp i32 }
          LocInfoE loc_290 (i2v 1 i32) ;
        "p" <-{ PtrOp }
          LocInfoE loc_277 (&(LocInfoE loc_279 ((LocInfoE loc_281 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_282 (i2v 1 i32))))) ;
        locinfo: loc_242 ;
        LocInfoE loc_272 ((LocInfoE loc_273 (!{PtrOp, Na1Ord, false} (LocInfoE loc_274 ("p")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_275 (i2v 0 i32))) <-{ IntOp i32 }
          LocInfoE loc_276 (i2v 2 i32) ;
        locinfo: loc_243 ;
        LocInfoE loc_266 ((LocInfoE loc_267 (!{PtrOp, Na1Ord, false} (LocInfoE loc_268 ("p")))) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_269 (i2v 1 i32))) <-{ IntOp i32 }
          LocInfoE loc_270 (i2v 3 i32) ;
        locinfo: loc_244 ;
        Return (LocInfoE loc_245 ((LocInfoE loc_246 ((LocInfoE loc_247 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_249 ((LocInfoE loc_251 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_252 (i2v 0 i32)))))) +{IntOp i32, IntOp i32} (LocInfoE loc_253 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_255 ((LocInfoE loc_257 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_258 (i2v 1 i32)))))))) +{IntOp i32, IntOp i32} (LocInfoE loc_259 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_261 ((LocInfoE loc_263 ("t")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_264 (i2v 2 i32))))))))
      ]> $∅
    )%E
  |}.

  (* Definition of function [array_local_var]. *)
  Definition impl_array_local_var : function := {|
    f_args := [
      ("y", it_layout i32)
    ];
    f_local_vars := [
      ("x", mk_array_layout (it_layout i32) 10)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        locinfo: loc_293 ;
        LocInfoE loc_302 ((LocInfoE loc_304 ("x")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_305 (i2v 1 i32))) <-{ IntOp i32 }
          LocInfoE loc_306 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_307 ("y"))) ;
        locinfo: loc_294 ;
        Return (LocInfoE loc_295 (use{IntOp i32, Na1Ord, false} (LocInfoE loc_297 ((LocInfoE loc_299 ("x")) at_offset{it_layout i32, PtrOp, IntOp i32} (LocInfoE loc_300 (i2v 1 i32))))))
      ]> $∅
    )%E
  |}.
End code.
