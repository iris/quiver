From caesium Require Export notation.
From caesium Require Import tactics.
From quiver.base Require Import annotations.
Set Default Proof Using "Type*".

(* Generated from [examples/src/tests/adequacy.c]. *)
Section code.

  (* Definition of function [inc]. *)
  Definition impl_inc : function := {|
    f_args := [
      ("x", it_layout i32)
    ];
    f_local_vars := [
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        Return ((use{IntOp i32, Na1Ord, false} ("x")) +{IntOp i32, IntOp i32} (i2v 1 i32))
      ]> $∅
    )%E
  |}.

  (* Definition of function [main]. *)
  Definition impl_main (global_inc : loc): function := {|
    f_args := [
    ];
    f_local_vars := [
      ("x", it_layout i32);
      ("y", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "x" <-{ IntOp i32 } i2v 41 i32 ;
        "y" <-{ IntOp i32 }
          Call (global_inc) [@{expr} use{IntOp i32, Na1Ord, false} ("x") ] ;
        Return (use{IntOp i32, Na1Ord, false} ("y"))
      ]> $∅
    )%E
  |}.
End code.
