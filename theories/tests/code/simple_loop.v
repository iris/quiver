From caesium Require Export notation.
From caesium Require Import tactics.
From quiver.base Require Import annotations.
Set Default Proof Using "Type*".

(* Generated from [examples/src/casestudies/simple_loop.c]. *)
Section code.

  (* Definition of struct [div]. *)
  Program Definition struct_div := {|
    sl_members := [
      (Some "quot", it_layout i32);
      (Some "rem", it_layout i32)
    ];
  |}.
  Solve Obligations with solve_struct_obligations.

  (* Definition of struct [ldiv]. *)
  Program Definition struct_ldiv := {|
    sl_members := [
      (Some "quot", it_layout i64);
      (Some "rem", it_layout i64)
    ];
  |}.
  Solve Obligations with solve_struct_obligations.

  (* Definition of struct [lldiv]. *)
  Program Definition struct_lldiv := {|
    sl_members := [
      (Some "quot", it_layout i64);
      (Some "rem", it_layout i64)
    ];
  |}.
  Solve Obligations with solve_struct_obligations.

  (* Definition of struct [list]. *)
  Program Definition struct_list := {|
    sl_members := [
      (Some "head", void*);
      (Some "tail", void*)
    ];
  |}.
  Solve Obligations with solve_struct_obligations.

  (* Definition of function [ten]. *)
  Definition impl_ten : function := {|
    f_args := [
    ];
    f_local_vars := [
      ("k", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "k" <-{ IntOp i32 } i2v 0 i32 ;
        Loop "#1" "#3" "__loop_inv_0"
      ]> $
      <[ "#1" :=
        if{IntOp i32, None}: (use{IntOp i32, Na1Ord, false} ("k")) <{IntOp i32, IntOp i32, i32} (i2v 10 i32)
        then
        Goto "#2"
        else
        Goto "#3"
      ]> $
      <[ "#2" :=
        "k" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("k")) +{IntOp i32, IntOp i32} (i2v 1 i32) ;
        Goto "#1"
      ]> $
      <[ "#3" :=
        assert{IntOp i32}: ((use{IntOp i32, Na1Ord, false} ("k")) ={IntOp i32, IntOp i32, i32} (i2v 10 i32)) ;
        Return (use{IntOp i32, Na1Ord, false} ("k"))
      ]> $∅
    )%E
  |}.

  (* Definition of function [up_to_arg]. *)
  Definition impl_up_to_arg : function := {|
    f_args := [
      ("m", it_layout i32)
    ];
    f_local_vars := [
      ("k", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "k" <-{ IntOp i32 } i2v 0 i32 ;
        Loop "#1" "#3" "__loop_inv_0"
      ]> $
      <[ "#1" :=
        if{IntOp i32, None}: (use{IntOp i32, Na1Ord, false} ("k")) <{IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} ("m"))
        then
        Goto "#2"
        else
        Goto "#3"
      ]> $
      <[ "#2" :=
        "k" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("k")) +{IntOp i32, IntOp i32} (i2v 1 i32) ;
        Goto "#1"
      ]> $
      <[ "#3" :=
        Return (use{IntOp i32, Na1Ord, false} ("k"))
      ]> $∅
    )%E
  |}.

  (* Definition of function [ten_with_frame]. *)
  Definition impl_ten_with_frame : function := {|
    f_args := [
    ];
    f_local_vars := [
      ("frame", it_layout i32);
      ("k", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "k" <-{ IntOp i32 } i2v 0 i32 ;
        "frame" <-{ IntOp i32 } i2v 0 i32 ;
        Loop "#1" "#3" "__loop_inv_0"
      ]> $
      <[ "#1" :=
        if{IntOp i32, None}: (use{IntOp i32, Na1Ord, false} ("k")) <{IntOp i32, IntOp i32, i32} (i2v 10 i32)
        then
        Goto "#2"
        else
        Goto "#3"
      ]> $
      <[ "#2" :=
        "k" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("k")) +{IntOp i32, IntOp i32} (i2v 1 i32) ;
        Goto "#1"
      ]> $
      <[ "#3" :=
        Return ((use{IntOp i32, Na1Ord, false} ("k")) +{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("frame")))
      ]> $∅
    )%E
  |}.

  (* Definition of function [plus_ten]. *)
  Definition impl_plus_ten : function := {|
    f_args := [
      ("a", it_layout i32)
    ];
    f_local_vars := [
      ("k", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "k" <-{ IntOp i32 } i2v 0 i32 ;
        Loop "#1" "#3" "__loop_inv_0"
      ]> $
      <[ "#1" :=
        if{IntOp i32, None}: (use{IntOp i32, Na1Ord, false} ("k")) <{IntOp i32, IntOp i32, i32} (i2v 10 i32)
        then
        Goto "#2"
        else
        Goto "#3"
      ]> $
      <[ "#2" :=
        "k" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("k")) +{IntOp i32, IntOp i32} (i2v 1 i32) ;
        Goto "#1"
      ]> $
      <[ "#3" :=
        assert{IntOp i32}: ((use{IntOp i32, Na1Ord, false} ("a")) <{IntOp i32, IntOp i32, i32} (i2v 200 i32)) ;
        Return ((use{IntOp i32, Na1Ord, false} ("k")) +{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("a")))
      ]> $∅
    )%E
  |}.

  (* Definition of function [reverse]. *)
  Definition impl_reverse : function := {|
    f_args := [
      ("p", void*)
    ];
    f_local_vars := [
      ("w", void*);
      ("t", void*)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "w" <-{ PtrOp } NULL ;
        Loop "#1" "#3" "__loop_inv_0"
      ]> $
      <[ "#1" :=
        if{IntOp i32, None}: (use{PtrOp, Na1Ord, false} ("p")) !={PtrOp, PtrOp, i32} (NULL)
        then
        Goto "#2"
        else
        Goto "#3"
      ]> $
      <[ "#2" :=
        "t" <-{ PtrOp }
          use{PtrOp, Na1Ord, false} ((!{PtrOp, Na1Ord, false} ("p")) at{struct_list} "tail") ;
        (!{PtrOp, Na1Ord, false} ("p")) at{struct_list} "tail" <-{ PtrOp }
          use{PtrOp, Na1Ord, false} ("w") ;
        "w" <-{ PtrOp } use{PtrOp, Na1Ord, false} ("p") ;
        "p" <-{ PtrOp } use{PtrOp, Na1Ord, false} ("t") ;
        Goto "#1"
      ]> $
      <[ "#3" :=
        Return (use{PtrOp, Na1Ord, false} ("w"))
      ]> $∅
    )%E
  |}.

  (* Definition of function [bin_search]. *)
  Definition impl_bin_search : function := {|
    f_args := [
      ("a", void*);
      ("n", it_layout size_t);
      ("x", it_layout i32)
    ];
    f_local_vars := [
      ("r", it_layout size_t);
      ("m", it_layout size_t);
      ("l", it_layout size_t)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "l" <-{ IntOp size_t }
          UnOp (CastOp $ IntOp size_t) (IntOp i32) (i2v 0 i32) ;
        "r" <-{ IntOp size_t } use{IntOp size_t, Na1Ord, false} ("n") ;
        Loop "#1" "#3" "__loop_inv_0"
      ]> $
      <[ "#1" :=
        if{IntOp i32, None}: (use{IntOp size_t, Na1Ord, false} ("l")) <{IntOp size_t, IntOp size_t, i32} (use{IntOp size_t, Na1Ord, false} ("r"))
        then
        Goto "#2"
        else
        Goto "#3"
      ]> $
      <[ "#2" :=
        "m" <-{ IntOp size_t }
          (use{IntOp size_t, Na1Ord, false} ("l")) +{IntOp size_t, IntOp size_t} (((use{IntOp size_t, Na1Ord, false} ("r")) -{IntOp size_t, IntOp size_t} (use{IntOp size_t, Na1Ord, false} ("l"))) /{IntOp size_t, IntOp size_t} (UnOp (CastOp $ IntOp size_t) (IntOp i32) (i2v 2 i32))) ;
        if{IntOp i32, None}: (use{IntOp i32, Na1Ord, false} ((!{PtrOp, Na1Ord, false} ("a")) at_offset{it_layout i32, PtrOp, IntOp size_t} (use{IntOp size_t, Na1Ord, false} ("m")))) <{IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} ("x"))
        then
        Goto "#4"
        else
        Goto "#5"
      ]> $
      <[ "#3" :=
        Return (use{IntOp size_t, Na1Ord, false} ("l"))
      ]> $
      <[ "#4" :=
        "l" <-{ IntOp size_t }
          (use{IntOp size_t, Na1Ord, false} ("m")) +{IntOp size_t, IntOp size_t} (UnOp (CastOp $ IntOp size_t) (IntOp i32) (i2v 1 i32)) ;
        Goto "#1"
      ]> $
      <[ "#5" :=
        "r" <-{ IntOp size_t } use{IntOp size_t, Na1Ord, false} ("m") ;
        Goto "#1"
      ]> $∅
    )%E
  |}.

  (* Definition of function [bin_search_fail]. *)
  Definition impl_bin_search_fail : function := {|
    f_args := [
      ("a", void*);
      ("n", it_layout i32);
      ("x", it_layout i32)
    ];
    f_local_vars := [
      ("r", it_layout i32);
      ("m", it_layout i32);
      ("l", it_layout i32)
    ];
    f_init := "#0";
    f_code := (
      <[ "#0" :=
        "l" <-{ IntOp i32 } i2v 0 i32 ;
        "r" <-{ IntOp i32 } use{IntOp i32, Na1Ord, false} ("n") ;
        Loop "#1" "#3" "__loop_inv_0"
      ]> $
      <[ "#1" :=
        if{IntOp i32, None}: (use{IntOp i32, Na1Ord, false} ("l")) <{IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} ("r"))
        then
        Goto "#2"
        else
        Goto "#3"
      ]> $
      <[ "#2" :=
        "m" <-{ IntOp i32 }
          ((use{IntOp i32, Na1Ord, false} ("l")) +{IntOp i32, IntOp i32} (use{IntOp i32, Na1Ord, false} ("r"))) /{IntOp i32, IntOp i32} (i2v 2 i32) ;
        if{IntOp i32, None}: (use{IntOp i32, Na1Ord, false} ((!{PtrOp, Na1Ord, false} ("a")) at_offset{it_layout i32, PtrOp, IntOp i32} (use{IntOp i32, Na1Ord, false} ("m")))) <{IntOp i32, IntOp i32, i32} (use{IntOp i32, Na1Ord, false} ("x"))
        then
        Goto "#4"
        else
        Goto "#5"
      ]> $
      <[ "#3" :=
        Return (use{IntOp i32, Na1Ord, false} ("l"))
      ]> $
      <[ "#4" :=
        "l" <-{ IntOp i32 }
          (use{IntOp i32, Na1Ord, false} ("m")) +{IntOp i32, IntOp i32} (i2v 1 i32) ;
        Goto "#1"
      ]> $
      <[ "#5" :=
        "r" <-{ IntOp i32 } use{IntOp i32, Na1Ord, false} ("m") ;
        Goto "#1"
      ]> $∅
    )%E
  |}.
End code.
