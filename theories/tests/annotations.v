From quiver.tests.code Require Import basics.
From quiver.inference Require Import inferPT.
From quiver.thorium.types Require Import types functions.
Set Default Proof Using "Type".



Section examples.
  Context `{!refinedcG Σ}.

  (** Examples *)
  Definition hint_example_pre : hint_prop :=
    (L{{"x"}} x, ∃{{"p"}} p, ∃{{"A"}} A, (x ◁ₗ ownT p A))%I.

  Definition hint_example_post  :=
    (∃[{"n"}] n, ∃[{"it"}] it, EHintBase (intT it n) ⌜n > 0⌝)%I.

  Lemma type_deref_no_hint :
    qenvs_empty ⊨ impl_deref : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - print_silent.
  Qed.

  Lemma type_deref_with_hint :
    qenvs_empty ⊨ impl_deref : ?.
  Proof.
    pose_hint "pre" hint_example_pre.
    pose_expr_hint "post_val" hint_example_post.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "annotation example; deref with hint : "; print_loud.
  Qed.

  Definition hint_double_pre : hint_prop :=
    (L{{"x"}} x, ∃{{"n"}} n, (x ◁ₗ intT i32 n))%I.

  Definition hint_double_post :=
    (∀[{"n"}] n, ∃[{"m"}] m, ∃[{"it"}] it, EHintBase (intT it m) ⌜m = (2 * n)%Z⌝)%I.

  Lemma type_double_with_hint:
    qenvs_empty ⊨ impl_double_int : ?.
  Proof.
    pose_hint "pre" hint_double_pre.
    pose_expr_hint "post_val" hint_double_post.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "annotation example; double with hint : "; print_loud.
  Qed.


End examples.