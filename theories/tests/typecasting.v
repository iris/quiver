From quiver.argon.abduction Require Import abduct proofmode pure precond.
From quiver.thorium.typing Require Import assume_type calls syntax.
From quiver.argon.simplification Require Import simplify elim_existentials normalize simplification_instances cleanup.
From quiver.thorium.logic Require Import type_cast calls.

Import AbductNotations.


Section prelim.
  Context `{!refinedcG Σ}.

  (* in the examples, we want to delimit the end of the example with [abd_yield] *)
  Lemma abduct_type_precond_sep_yield Δ X pm (Q R: X → iProp Σ) E M B (R' : iProp Σ):
    abduct Δ (type_precond X pm R E M B (λ x, Some (abd_yield (Q x))))%I R' →
    abduct Δ (type_precond X pm (λ x, (abd_yield (Q x)) ∗ R x) E M B (λ _, None))%I R'.
  Proof. rewrite type_precond_sep_continuation //. Qed.

  Global Instance abduct_step_type_precond_sep_yield Δ pm X (Q R: X → iProp Σ) E M B (R' : iProp Σ):
    AbductStep (abduct Δ (type_precond X pm R E M B (λ x, Some (abd_yield (Q x))))%I R')
      Δ (type_precond X pm (λ x, (abd_yield (Q x)) ∗ R x) E M B (λ _, None))%I R' | 1.
  Proof.
    intros ?; by eapply abduct_type_precond_sep_yield.
  Qed.
End prelim.



Section swap_function.
  Context `{!refinedcG Σ}.

  (* the precondition of a swap function *)
  Definition swap_params : Z * Z * loc * loc → fn_params :=
    λ t,
    {|
      fp_atys := [ownT t.1.2 (intT i32 t.1.1.1); ownT t.2 (intT i32 t.1.1.2)];
      fp_Pa := True%I;
      fp_rtype := unit;
      fp_fr := λ _, FR voidT (t.1.2 ◁ₗ intT i32 t.1.1.2 ∗ t.2 ◁ₗ intT i32 t.1.1.1)%I;
    |}.

  Definition swap_ft := FT _ swap_params.


  Lemma type_precond_of_swap v p q it1 n1 w it2 n2 Φ :
    ∃ R, abduct (QE nil nil nil nil) (type_call_ft [(v, ownT p (intT it1 n1)); (w, ownT q (intT it2 n2))] swap_ft (λ v A, abd_yield (Φ v A))) R.
  Proof.
    eexists _; eapply abduct_simplify.
    - abduct.
    - normalize. prune_exists. cleanup. normalize. simplify.
  Qed.


  (* a contorted version of writing the precondition of swap  *)
  Definition swap_awkward_params : Z * Z * loc * loc → fn_params :=
    λ t,
    {|
      fp_atys := [
        ownT t.1.2 (intT i32 t.1.1.1);
        constraintT (valueT t.2 ptr_size) (t.1.1.1 > t.1.1.2)];
      fp_Pa := t.2 ◁ₗ (intT i32 t.1.1.2) ∗ ⌜t.1.1.1 ≠ t.1.1.2⌝;
      fp_rtype := unit;
      fp_fr := λ _, FR voidT (t.1.2 ◁ₗ intT i32 t.1.1.2 ∗ t.2 ◁ₗ intT i32 t.1.1.1)%I;
    |}.

  Definition swap_awkward_ft := FT _ swap_awkward_params.


  Lemma type_precond_of_swap_awkward v p q it1 n1 w it2 n2 Φ :
    ∃ R, abduct (QE nil nil nil nil) (type_call_ft [(v, ownT p (intT it1 n1)); (w, ownT q (intT it2 n2))] swap_awkward_ft (λ v A, abd_yield (Φ v A))) R.
  Proof.
    eexists _; eapply abduct_simplify.
    - abduct.
    - normalize. prune_exists. cleanup. normalize. simplify.
  Qed.

End swap_function.



(* examples with a custom memory allocator *)
Section memory_management_examples.
  Context `{!refinedcG Σ}.

  Definition block (l: loc) (n: Z) : iProp Σ := False.
  Opaque block.

  Global Instance predicate_block : Predicate loc Z block := {}.


  Definition malloc_ft : fn_type := FT Z (λ n,
    {|
      fp_atys := [intT size_t n];
      fp_Pa := ⌜0 ≤ n⌝%I;
      fp_rtype := dProp;
      fp_fr := λ φ, FR (optional φ (ex p, owned p (any[n]) ∗∗ block p n)%RT) emp;
    |}).

  Definition zmalloc_ft : fn_type := FT Z (λ n,
    {|
      fp_atys := [intT size_t n];
      fp_Pa := ⌜0 ≤ n⌝%I;
      fp_rtype := dProp;
      fp_fr := λ φ, FR (optional φ (ex p, owned p (zerosT n) ∗∗ block p n)%RT) emp;
    |}).

  Definition calloc_ft : fn_type := FT (Z*Z) (λ ns,
    {|
      fp_atys := [int[size_t] ns.1; int[size_t] ns.2];
      fp_Pa := ⌜0 ≤ ns.1⌝%I;
      fp_rtype := dProp;
      fp_fr := λ φ, FR (optional φ (ex p, owned p (zerosT (ns.1 * ns.2)) ∗∗ block p (ns.1 * ns.2))%RT) emp;
    |}).

  Definition free_ft : fn_type := FT (Z * loc * val) (λ t,
    {|
      fp_atys := [owned t.1.2 (value[t.1.1] t.2)];
      fp_Pa := block t.1.2 t.1.1;
      fp_rtype := unit;
      fp_fr := λ _, FR void emp;
    |}).


  (* NOTE: For [free], the issue is not as severe, because the
    [free] function will always go for the maximum length.
    Here is a more complicated example:

    char *buf = malloc(32);    // buf ◁ₗ 32 @ untyped u8
    memcpy(buf, b, 16);        // buf ◁ₗ sliced 32 [(0, 16) ↦ value v; (16, 32) ↦ 16 @ untyped u8]
    memset(buf + 16, 0, 16);   // buf ◁ₗ sliced 32 [(0, 16) ↦ value v; (16, 32) ↦ zeros u8]
  *)
  Lemma type_precond_of_free v p n1 Φ :
    ∃ R, abduct (QE nil nil nil [block p 8]) (type_call_ft [(v, ownT p (intT i64 n1))] free_ft (λ v A, abd_yield (Φ v A))) R.
  Proof.
    eexists _; eapply abduct_simplify.
    - abduct.
    - normalize. prune_exists. cleanup. normalize. simpl.
  Abort.

End memory_management_examples.



(* linked list examples *)
Section linked_list_examples.
  Context `{!refinedcG Σ}.

  (* linked list example *)
  Definition nonempty {X: Type} (x: list X) : dProp :=
    DProp (x ≠ nil).

      (* Definition of struct [list]. *)
  Program Definition struct_list := {|
    sl_members := [
      (Some "head", void*);
      (Some "tail", void*)
    ];
  |}.
  Solve Obligations with solve_struct_obligations.


  (* prelimiary definitions *)
  Definition listR_body {X} (T: rtype Σ X) (list_rec: list X → type Σ) (xs: list X) :=
    optionalT (nonempty xs)
              (existsT (λ p, existsT (λ q, existsT (λ x, existsT (λ xr,
                constraintT (ownT p (structT struct_list [ownT q (x @ T); list_rec xr])) (xs = x :: xr)))))).


  Global Instance listR_body_mono {X} (T: rtype Σ X): TypeMono (listR_body T).
  Proof. solve_proper. Qed.

  Definition listR {X} (T: rtype Σ X) := fixR (listR_body T).



  Remove Hints unfold_refinement_rtype : typeclass_instances.


  Lemma type_precond_of_listR {X} v (T: rtype Σ X) (xs: list X) Φ :
    ∃ R,
      abduct (QE nil nil nil [v ◁ᵥ nullT])
      (type_precond unit RECOVER (λ _, ∃ xs, v ◁ᵥ xs @ listR T ∗ abd_yield (Φ xs))%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)) R.
  Proof.
    eexists _; eapply abduct_simplify.
    - abduct.
    - normalize. prune_exists. cleanup. normalize. simpl.
  Abort.


  Lemma type_precond_of_listR {X} v (T: rtype Σ X) (xs: list X) Φ l v1 v2 A1 A2:
    uni v1 →
    uni v2 →
    exi l →
    exi A1 →
    exi A2 →
    ∃ R,
      abduct (QE nil nil nil [
        v1 ◁ᵥ A1;
        l at{struct_list}ₗ "head" ◁ₗ (value[ly_size void*] v2);
        v2 ◁ᵥ A2;
        v ◁ᵥ owned l
        (struct[struct_list] [(place (l at{struct_list}ₗ "head")); (value[ly_size void*] v1)%RT])
      ])
      (type_precond unit RECOVER (λ _, ∃ xs, v ◁ᵥ xs @ listR T ∗ abd_yield (Φ xs))%I (λ _, nil) (λ _, nil) (λ _, nil) (λ _, None)) R.
  Proof.
    eexists _; eapply abduct_simplify.
    - abduct.
    - simpl. normalize. prune_exists. cleanup. normalize. simplify.
  Qed.

End linked_list_examples.
