# Quiver

To set up Quiver, please follow the instructions in the root folder.
Once everything is set up, case studies can be built using the script `./build.sh`, which works as follows:
```
./build.sh path/to/casestudy/file.c
```
The script will run Quiver on the file `path/to/casestudy/file.c`, outputting the inferred specification to the command line.

## Mapping to the Coq Implementation

### Argon (Section 3)
The rules for Argon are contained in the folder `theories/argon/`.
The folder `argon/base` contains basic judgments, `argon/simplification` contains the simplification machinery, and `argon/abduction` the abduction judgment.

**Coq-level judgments**
- the abduction judgment `Δ * [R] ⊢ G` is the judgment `abduct` in `argon/abduction/abduct.v`.
- the judgment for embedding `E ⊣ G when P`: see the paragraph "Embedding" below
- the judgment for sideconditions `Δ ⊢ P` is the judgment `qenvs_entails` in `argon/abduction/abduct.v`.
- the simplification judgment `P => Q` is the judgment `simplify` in `argon/simplification/simplify.v`.
- the context search `Δ, M = Δ'` is the judgment `qenvs_find_atom` in `argon/abduction/abduct.v`.
- the judgment for existential instantiation `ex(x. G_1 x | S x) ⊣ G_2 when P`: see the paragraph "Instantiating Existential Quantifiers" below

Note: In the Coq implementation, contexts contain *two* lists of pure propositions.
The first one contains pure propositions that belong conceptually to the "precondition", and the second one contains pure propositions that belong conceptually to the "postcondition".

**Goals**
- The embedded goals of Thorium are defined in the `thorium/logic/` folder.
  For example, `lwp`, `rwp`, and `swp` are defined in `thorium/logic/syntax.v`. (In the paper, only a single weakest precondition `wp` for `rwp` in Coq is shown.)
- The goal `ex(G)` is `type_pre` in `argon/base/precond.v`
- The goal `simpl(F){G}` is `abd_evars` in `argon/abduction/proofmode.v`
- The goal `bind(G_1){G_2}` is `abd_breakpoint` in `argon/abduction/proofmode.v`
- The goal `if ... then ... else ...` is written `case` (defined in `base/dprop.v`)

**Proof Search**
The proof search of Argon is the tactic `abduct`, which repeatedly calls `abduct_step` in `argon/abduction/proofmode.v`. The base tactic `abduct_step` triggers typeclass search to apply the next appropriate abduction rule (matching on the goal). Concretely, for the goal `abduct Δ P R`, the rule will search for an instance `AbductStep ?φ Δ P R` and then proceed with `φ` next. Below we list the abduction rules and the `AbductStep` instances to which they correspond.

**Abduction Rules**
The abduction rules in Section 2 and Section 3 map to the Coq code as follows:

| Name | Figure | Coq Name | Location |
|---|---|---|---|
| abd-embed | Fig 4 | `abduct_ent_strong` | `argon/abduction/abduct.v`, see "Embedding" below |
| abd-res-ctx | Fig 4 | `abduct_step_abduct_atom_ctx` | `argon/abduction/proofmode.v` |
| abd-res-missing | Fig 4 | `abduct_step_abduct_atom_missing` | `argon/abduction/proofmode.v` |
| abd-pure-prove | Fig 4 | `abduct_step_pure_prove` | `argon/abduction/pure.v` |
| abd-pure-missing | Fig 4 | `abduct_step_pure_missing` | `argon/abduction/pure.v` |
| abd-wand-res | Fig 4 | `abduct_step_abd_assume_atom`, then `abduct_step_wand` | `argon/abduction/assume.v` and `argon/abduction/proofmode.v` |
| abd-wand-pure | Fig 4 | `abduct_step_abd_assume_assumption` (additionally using `φ` to simplify the context) | `argon/abduction/assume.v` |
| abd-exists | Fig 4 | `abduct_step_exists` | `argon/abduction/proofmode.v` |
| abd-all | Fig 4 | `abduct_step_forall_intros` | `argon/abduction/proofmode.v` |
| abd-end | Fig 4 | `abduct_step_abd_cont_wrap_yield_one`, then `abduct_step_abd_yield`, then the `revert` judgment for reverting | `argon/abduction/proofmode.v` |
| abd-true | Fig 4 | `abduct_step_true` | `argon/abduction/proofmode.v` |
| abd-if-true | Fig 7 | `abduct_step_case_left` | `argon/abduction/proofmode.v` |
| abd-if | Fig 7 | `abduct_step_case`, then `abduct_step_abd_if` | `argon/abduction/proofmode.v` |
| abd-conj | Fig 7 | `abduct_step_conj` | `argon/abduction/proofmode.v` |
| abd-ex | Fig 7 | `abduct_step_type_precond_add_emp`, ...  | see the paragraph "Instantiating Existential Quantifiers" below |
| abd-simpl | Fig 7 | `abduct_step_abd_evars` | `argon/abduction/proofmode.v` |
| abd-bind | Fig 7 | `abduct_step_abd_breakpoint` | `argon/abduction/proofmode.v` |
| abd-fail | Fig 7 | `abduct_step_abort` | `argon/abduction/proofmode.v` |
| simplify | Fig 7 | `simplification_chain`/the tactic `simplify_iter`  | `argon/simplification/simplification.v`|

**Embedding**
We do not use a single, automated proof rule that corresponds to `abd-embed`.
Instead, for each Thorium rule with goal `E`, we provide an instance `AbductStep _ Δ E R`.
Which rule should be applied is resolved during type class search by matching on the goal.
We give several concrete instances below (in [Section Thorium](#thorium-section-4-appendix)).
We define the rules as `AbductStep`-instances directly for performance reasons.
Of course, the rule `abd-embed` is true nonetheless (see `abduct_ent_strong` in `argon/abduction/abduct.v`) where `E ⊣ G when P := G ∧ P ⊢ E`.

**Simplification**
The simplification machinery described in Section 3 is contained in the folder `argon/simplification/`.
Most notably,
1. the file `normalize.v` contains a pass for normalizing a separation logic proposition (`P =>_norm Q` in the paper),
2. the file `elim_existentials.v` contains a pass for eliminating existential quantifiers using trivial instantiations (`P =>_ex Q` in the paper),
3. the files `cleanup.v` and `simplification_instances.v` contain simplification rules for propositions and terms (`P =>_simp Q` in the paper).

**Instantiating Existential Quantifiers**
The existential quantifier instantiation machinery described in Section 3 is contained in the file `argon/abduction/precond.v`.
The main judgment is `type_precond` (see `argon/base/precond.v`) which keeps the existential quantifier in the goal (implicitly) and iterates over the goal.
The judgment `ex(x. G_1 x| S x) ⊣ G_2 when P` is not defined in the Coq code directly.
Instead, we prove its rules directly as `AbductStep`-instances analogous to the treatment of `E ⊣ G when P` (see also the paragraph "Embedding").

The judgment in Coq `type_precond` generalizes the judgment `ex` presented in the paper in two ways:
1. It does not immediately instantiate existential variables but instead puts assertions that lead to instantiations on a stack `E` and continues with the remainder of the goal. Then, while traversing the remaining goal, it may encounter a continuation that will not lead to existential instantiation and store it in the argument `C` (e.g., the postcondition `Φ`).
2. Besides the blocked stack, the judgment carries a "maybe"-stack `M` that leads to an instantiation, but which is not guaranteed to be the right choice (i.e., it is a heuristic).
   The judgment first tries to find other instantiations before looking at the "maybe"-stack.

The rules in Figure 8 can be mapped to the Coq code as follows.

| Name | Coq Name | Location |
|---|---|---|
| ex-exists | `abduct_step_type_precond_sep_exists_pair` | `argon/abduction/precond.v` |
| ex-pure | `abduct_step_type_precond_sep_pure_lift` | `argon/abduction/precond.v` |
| ex-pure-blocked | `abduct_step_type_precond_sep_pure_blocked` | `argon/abduction/precond.v` |
| ex-eq | `abduct_step_type_precond_sep_eq` and `abduct_step_type_precond_evars_simplify`, assertions are first put on the stack `E` | `argon/abduction/precond.v` |
| ex-lift | `abduct_step_type_precond_emp_lift_exists` | `argon/abduction/precond.v` |
| ex-done | `abduct_step_type_precond_emp_done` | `argon/abduction/precond.v` |


**Joining Branches**
Quiver has support for joining or eliminating branches, which can be found in `argon/abduction/cases.v` and `argon/simplification/cleanup.v`.
For conjunctions, it provides support for eliminating conjunctions (e.g., turning `True ∧ P` and `P ∧ P` into `P`).
For conditionals, it provides support for joining the branches by propagating the branch down---if successful.

The conditional joining automation depends on the machinery in `thorium/join/`, which gives rules for joining predicate transformers `symmetric_join.v` (which depends on joining types `join_types.v`).

**Loops**
The rules for loops can be found in `thorium/typing/controlflow.v`. (We list them here, because they are mostly concerned with Argon judgments.)
1. We prove the loop invariant using `type_precond` (see `abduct_type_loop_core`).
2. We add the remainder of the context (see `abduct_type_loop_core_assuming_inv`).
3. We abduct the body of the loop assuming the invariant (see `abduct_type_loop_core_assuming_inv_ctx`).
   This rule ensures that when we want to do another iteration of the loop by jumping to `lb`, we reestablish the invariant instead.
4. We ensure that the abducted predicate transformer does not add any additional assumptions (see `abduct_type_loop_fixp` and the rules for `loop_separate_loop_continue` in `argon/abduction/proofmode.v`).


### Thorium (Section 4; Appendix)

**Judgments and Typing Rules**
The judgments of Thorium are defined in `thorium/logic/syntax.v` (prefixed by `type_`) and additionally in the files `thorium/logic/pointers.v` (for pointers), `thorium/logic/datatypes.v` (for operating on structs and pointer arithmetic), `thorium/logic/arithmetic.v` (for unary and binary operations), `thorium/logic/controlflow.v` (for conditionals), `thorium/logic/calls.v` (for function calls), and `thorium/logic/type_cast.v` (for type conversion).

The typing rules for these judgments are in `thorium/typing/syntax.v`, `thorium/typing/pointers.v`, `thorium/typing/datatypes.v`, `thorium/typing/arithmetic.v`, `thorium/typing/controlflow.v`, `thorium/typing/calls.v`, and `thorium/typing/type_cast.v`.

The typing rules in the appendix map to the Coq code as follows:

| Name | Coq Name | Location |
|---|---|---|
| ty-cast-wp | `preR_unop` (specialized in the appendix to type casts only) | `thorium/typing/syntax.v` |
| ty-cast-sizet | `un_op_typing_rule_cast_int_int_extend_signed_unsigned` (specialized in the appendix, using arbitrary integer sizes here) | `thorium/typing/arithmetic.v` |
| ty-cast-def | `abduct_type_un_op` (specialized in the appendix, using machinery for "default"-rules in Coq) | `thorium/typing/arithmetic.v`  |
| ty-val | `abduct_step_find_val` | `thorium/typing/syntax.v` |
| ty-val-missing | `abduct_step_type_vtr_missing` | `thorium/typing/syntax.v` |
| ty-conv-int | `abduct_step_type_cast_int` | `thorium/typing/type_cast.v` |
| ty-conv-own | `abduct_step_type_cast_own` | `thorium/typing/type_cast.v` |
| ty-call | `abduct_step_type_call_pt_intro` (which additionally takes care of argument value layouts) followed by `abduct_step_type_pred_transform_intro` (which does some additional simplification) | `thorium/typing/calls.v` |
| ex-conv | `abduct_step_type_precond_sep_vtr` followed by `abduct_step_type_precond_xtr_non_dep` (simplified in the paper; see type conversion below) | `thorium/typing/type_cast.v` |
| ex-conv-blocked | `abduct_step_type_precond_sep_vtr` followed by `abduct_step_type_precond_xtr_dep` (simplified in the paper; see type conversion below) | `thorium/typing/type_cast.v` |


**Type Conversion**
The type conversion judgment is `type_cast`, defined in `thorium/logic/type_cast.v`.
Its type conversion rules, including the ones shown in the appendix, are given in `thorium/typing/type_cast.v`.
Some conversion rules involve a `type_recover`-judgment for recycling leftover ownership, which is not shown in the appendix.

Type conversion in the appendix is simplified in two aspects compared to the Coq code:
- In Coq, type conversion can be used for value and, additionally, location type assignments.
- In Coq, type conversion does not always succeed. Instead, besides success---which is called `guaranteed` in Coq---it has the following additional outcomes: `blocked`, meaning we should try other assertions first in `type_precond`; `maybe`, meaning type conversion is possible, but may be too eager and we should try other assertions first; and `impossible`, meaning type conversion should fail (e.g., for attempting to convert null to an owned pointer). The paper shows only the `guaranteed` case.
The different outcomes are handled by a match, `type_progress_match`, in the rule `abduct_step_type_precond_xtr_non_dep`.

**Predicate transformers and joining**
The function call judgment is `type_call`, defined in `thorium/logic/syntax.v`. Its proof rules are given in `thorium/typing/calls.v`. In contrast to the version presented in the paper, this call judgment is designed to evaluate multiple function arguments.
If we have a predicate transformer type for the function argument, we switch to `type_call_predicate_transformer` which will execute **apply** (which in the Coq code is called `type_pred_transform`).

The machinery for joining is defined in the file `thorium/join/symmetric_join.v`.
At a high level, it proceeds in three stages:
1. It first tries to join the precondition-parts of both predicate transformers (using type-specific rules).
2. It propagates information from joining the preconditions (e.g., equalities between integers in both precondition-parts) to the postcondition parts.
3. Lastly, it attempts to join the postcondition parts.

**Semantic Model**
All judgments are directly defined using the connectives of the underlying separation logic Iris (instantiated with Caesium). The definition of types can be found in `thorium/types/types.v` (with the definitions of the basic types given in the other files in the directory) and the model of the Thorium judgments is their definition (e.g., in `thorium/logic/syntax.v` for the weakest preconditions).


## Key Idea: Abductive Deductive Verification (Section 2)

**The language λ_expo.**
To ease the exposition to Quiver, Section 2 uses the toy language λ_expo instead of C (avoiding various complexities orthogonal to abductive deductive verification). The language λ_expo is not formalized in Coq. Its rules are inspired by the C rules that Quiver actually uses.
For example,
- the rule `wp-val` is inspired by the value rule `rwp_val` and `type_val_intro` (in `thorium/logic/syntax.v`)
- the rule `wp-let` in Figure 4 is inspired by the recursively descending rules such as `rwp_binop`, `rwp_unop`, `rwp_if_expr`, ... (in `thorium/logic/syntax.v`)
- the rule `wp-assign` is inspired by `wps_assign_typed` (in `thorium/logic/pointers.v`)
- the rule `wp-assign-def` is inspired by `wps_assign_typed` (in `thorium/logic/pointers.v`) and the treatment of missing locations/values such as `type_loc_missing`/`type_val_missing` (in `thorium/logic/syntax.v`)
- the rule `wp-read` is inspired by `wp_deref_typed` in (in `thorium/logic/pointers.v`)
- the rule `wp-new` has no closely corresponding rule in C, since allocation in C is dealt with via `malloc` (see the `malloc`-spec in `stdlib/malloc.v`)
- the rule `wp-call` is inspired by `rwp_call_pred_transformer` (in `thorium/logic/calls.v`)
- the rule `wp-assert` is inspired by the treatment of sketches, implemented in `theories/inference/flexhints.v`


**Figure 4**
As mentioned above, the weakest precondition rules in Fig 4 have no direct counterpart in Coq, but are inspired by existing Quiver rules.
The abduction rules in Fig 4 directly correspond to Argon abduction rules. See the table in the [Section Argon](#argon-section-3) above.

**Figure 5**

| Name | Coq Name | Location |
|---|---|---|
| ex-ex | simplified form of `abduct_step_type_precond_sep_exists_pair` | `argon/abduction/precond.v` |
| ex-inst | simplified form of `abduct_step_type_precond_sep_eq` and `abduct_step_type_precond_evars_simplify` | `argon/abduction/precond.v` |
| ex-loc | λ_expo rule, inspired by `abduct_step_type_cast_value_val` | `thorium/typing/type_cast.v` |
| ex-pointsto | λ_expo rule, inspired by `abduct_step_type_cast_value_loc` | `thorium/typing/type_cast.v` |
| ex-unfold | λ_expo rule, inspired by `abduct_step_type_cast_refinement_unfold_tgt` | `thorium/typing/type_cast.v` |
| ex-pred | λ_expo rule, inspired by `abduct_step_type_cast_refinement_eq` | `thorium/typing/type_cast.v` |



### Implementation (Section 5)

**Theorem 5.1: All Argon and Thorium rules are sound wrt. the Caesium C semantics.**
This theorem corresponds to all Lemmas in the folders `theories/argon/` and `theories/thorium/` being closed with `Qed.`
That is, we mechanize Thorium and Argon as "shallow embeddings" in the sense that their rules are lemmas in the Coq development and their soundness is the proof of the respective lemma. For example, the lemma
```
Lemma abduct_assoc Δ R (P1 P2 P: iProp Σ):
  abduct Δ (P1 ∗ (P2 ∗ P)) R →
  abduct Δ ((P1 ∗ P2) ∗ P) R.
Proof.
  rewrite /abduct.
  intros Habd. rewrite -bi.sep_assoc Habd //=.
Qed.
```
corresponds to the rule
```
Δ ∗ [R] ⊢ (P1 ∗ (P2 ∗ P))
-------------------------
Δ ∗ [R] ⊢ ((P1 ∗ P2) ∗ P)
```
The proof of the lemma corresponds to a soundness proof of the rule.

We prove the rules sound against the Caesium C semantics, since our typing goals are defined in terms of the Caesium weakest precondition.
For example, when we prove that a Caesium Skip statement can simply be skipped:
```
Lemma swp_skip s Q Φ:
  swp s Q Φ ⊢ swp (SkipS s)%E Q Φ.
Proof.
  rewrite /swp. iIntros "Hs".
  iApply wps_skip.
  iApply fupd_mask_intro; first set_solver.
  iIntros "Hclose". iNext. iMod "Hclose". iModIntro.
  done.
Qed.
```
we prove soundness of the rule
```
swp s Q Φ ⊢ swp (SkipS s)%E Q Φ
```
against Caesium's weakest precondition.


**Corollary 5.2: Assuming the standard library function satisfy their specifications, the specifications inferred by Quiver are sound wrt. the Caesium C semantics.**

This Corollary corresponds to the lemmas generated by Quiver for case studies being closed with `Defined.` and passing the Coq kernel.
That is, the abduction procedure `abduct` assembles the rules we have proven sound in *Theorem 5.1* to a complete inference by repeatedly applying rules.
Since the individual rules are sound, the resulting specifications are also sound.
To be precise, for a function `func`, Quiver produces and solves lemmas of the form
```
  Lemma type_func  :
    QE nil nil [...] nil ⊨ impl_func : ?.
  Proof.
    inferF.
    - abduct.
    - simplify_passes.
    - idtac "func: "; print_loud.
  Defined.
```
where the tactic `abduct` assembles the rules and `[...]` contains assumptions about the functions that are called inside `func`.


**Adequacy**
We give an example of how such an inferred specification can be turned into a proof outside of Iris in the file `theories/tests/adequacy.v`.
As one would expect, we can use previously inferred specifications to resolve dependencies between functions and then obtain a closed result (outside of Iris) in the end.




### Evaluation (Section 6)
All C files are in the folder `examples/src/casestudies/`.
All expected outputs are in the folder `output/casestudies/`.

For the case studies, we generate the Caesium code and the abduction proof files (i.e., the ones that do the specification inference) with the build script `build.sh`.
Before running the script, the "`proofs/*`" folders mentioned below will no yet exist.
In any case, the specifications that should be inferred are contained in `output/` (even before running the script).

| Name | C source | Specifications | Inference Files |
|---|---|---|---|
| Allocators (Memory) | `memory.c` | `output/casestudies/memory/` | `examples/src/casestudies/proofs/memory/` |
| Linked List (Memory) | `list/list_no_annot.c` | `output/casestudies/list/list_no_annot/` | `examples/src/casestudies/list/proofs/list_no_annot/` |
| Linked List (Functional) | `list/list_annotated.c` | `output/casestudies/list/list_annotated/` | `examples/src/casestudies/list/proofs/list_annotated/` |
| Vector (Memory) | `arrays/array_no_annot.c` | `output/casestudies/arrays/array_no_annot/` | `examples/src/casestudies/arrays/proofs/array_no_annot/` |
| Vector (Functional) | `arrays/array_annotated.c` | `output/casestudies/arrays/array_annotated/` | `examples/src/casestudies/arrays/proofs/array_annotated/` |
| Bipbuffer (Length) | `memcached_bidirectional_buffers/bipbuffer_annotated.c` | `output/casestudies/memcached_bidirectional_buffers/bipbuffer_annotated/` | `examples/src/casestudies/memcached_bidirectional_buffers/proofs/bipbuffer_annotated/` |
| OpenSSL Buffer (Memory) | `openssl_buffer/buffer.c` | `output/casestudies/openssl_buffer/buffer/` |  `examples/src/casestudies/openssl_buffer/proofs/buffer/` |
| OpenSSL Buffer (Length) | `openssl_buffer/buffer_shape.c` | `output/casestudies/openssl_buffer/buffer_shape/` | `examples/src/casestudies/openssl_buffer/proofs/buffer_shape/` |
| Binary Search (Functional) | `binary_search.c` | `output/casestudies/binary_search/` | `examples/src/casestudies/proofs/binary_search/` |
| Hashmap (Functional) | `mutable_map.c` | `output/casestudies/mutable_map/` |  `examples/src/casestudies/proofs/mutable_map/` |

