# Case Studies

In the following, we explain the Quiver build script `./build.sh` in more detail ([The Build Script](#the-build-script)), explain schematically how to use it to evaluate case studies ([Schematic Case Study](#schematic-case-study)), and then give concrete instantiations of this schema for evaluating Quiver's case studies ([Concrete Evaluation](#concrete-evaluation)).


## The Build Script
The build script `./build.sh` works as follows:
```
./build.sh [--metrics] [--time] [--no-cache] [--single-core] path/to/casestudy/file.c
```
The script will run Quiver on the file `path/to/casestudy/file.c`, outputting the inferred specification to the command line.
The execution can be controlled by the following options:
- `--metrics` will enable output of additional metrics. These metrics include things like the size of the inferred specification. We explain them in more detail below ([Schematic Case Study](#schematic-case-study)).
- `--single-core` will avoid using multiple cores by passing `-j1` to `dune`.
  For time measurements, this option should always be set (since this is how the ones in the table are produced).
  For other uses, the flag can be omitted.
  However, since the large case studies (i.e., OpenSSL Buffer and Memcached Bipbuffer) are known to consume considerable amounts of RAM, we recommend using only a single core for them, especially when executing inside of a Docker container.  Otherwise, execution can fail due to out of memory issues.
- `--time` will wrap the execution in the Unix utility `time`
- `--no-cache` will ensure that the case study (but not the ones it depends on) are rebuilt from scratch.
  We set this flag for timing measurements to avoid accidental speedups due to caching.

## Schematic Case Study

To compare the inferred specifications with the expectations in `output/`, compute metrics like the instantiation of existentials, measure execution times, count the lines of code, and manually count other metrics, we proceed as follows:

1. *Specs.* First, we execute the case study and compare the inferred specifications with the specifications we expect to infer for this case study (in `output/`).
    ```
    ./build.sh --single-core path/to/casestudy/file.c           # generate specs
    opam exec -- dune runtest path/to/expected/outputs/         # compare against expectation
    ```
    The specifications correspond to the expectation when the second command terminates without showing a diff.

    For the individual case studies, we will replace `path/to/casestudy/file.c` with the C file and `path/to/expected/outputs/` with the folder that contains the expected specifications.
    As mentioned above, for systems with sufficient resources (especially RAM), the argument `--single-core` can be omitted.

2. *Metrics.* Second, we compute the metrics Existentials (∃ in Figure 12), Proven/Simplified (φ in Figure 12), and Size (Specs in Figure 12).
    To do so, we build the case study again with metrics enabled (which will augment the output with information about the execution):
    ```
    ./build.sh --single-core --metrics path/to/casestudy/file.c           # generate specs + metrics
    python3 tools/aggregate_metrics.py                                    # output metrics table
    ```
    The second command should output a table with metrics for the case study.
    For the case study, they should coincide with the table from the paper.

3. *Timing.* Third, we measure the time for executing the case study:
    ```
    ./build.sh --time --single-core --no-cache path/to/casestudy/file.c           # measure time
    ```
    By setting the flag `--no-cache`, we ensure that the case study is rebuilt.
    Note that for case studies that depend on other case studies (e.g., the Vector depends on the Allocators),
    the timing measure is falsified if the reverse dependencies have not been build first. They are built in
    step one (Specs), so please ensure that this step is executed for the current case study before measuring its execution time.

4. *Lines of Code.* Fourth, we compute the lines of C code by running:
    ```
    tools/count_lines.sh path/to/casestudy/file.c
    ```

5. *Manually Counted Metrics* Fifth, we count the remaining columns of the table manually.
    We have added comments in the source files for how we have counted the
    sketch sizes (`SKETCH`), type sizes (`TYPE`), invariant sizes (`INV`), and other annotations (`ANNOT`).
    At the end of each case study, we have summed up these numbers as `TOTAL SKETCH`, `TOTAL TYPE`, `TOTAL INV`, and `TOTAL ANNOT`.

    For the lines of Coq code, we count them using
    ```
    coqwc path/to/extra/proofs.v
    ```
    This is only relevant for the Binary search and Hashmap case studies.



## Concrete Evaluation
Below, we instantiate the sequence of commands for every case study.

### **Allocators (Mem)**
```
# Specs
./build.sh --single-core  examples/src/casestudies/memory.c
opam exec -- dune runtest output/casestudies/memory
```

```
# Metrics
./build.sh --single-core  --metrics examples/src/casestudies/memory.c
python3 tools/aggregate_metrics.py
```

```
# Timing
./build.sh --time --single-core --no-cache examples/src/casestudies/memory.c
```

```
# C Lines
tools/count_lines.sh examples/src/casestudies/memory.c
```
For the allocator case study, there are no annotations to count in the C files/there is no Coq code to count.

### **Linked List (Mem)**
```
# Specs
./build.sh --single-core  examples/src/casestudies/list/list_no_annot.c
opam exec -- dune runtest output/casestudies/list/list_no_annot
```

```
# Metrics
./build.sh --single-core  --metrics examples/src/casestudies/list/list_no_annot.c
python3 tools/aggregate_metrics.py
```

```
# Timing
./build.sh --time --single-core --no-cache examples/src/casestudies/list/list_no_annot.c
```

```
# C Lines
tools/count_lines.sh examples/src/casestudies/list/list_no_annot.c
```

For the Linked List (mem) case study, there are no annotations to count in the C files/there is no Coq code to count.

**Note** The expected line count for this case study is 26.
As indicated in the paper, this case study does not contain the `reverse` function unlike Linked List (Func).

### **Linked List (Func)**
```
# Specs
./build.sh --single-core  examples/src/casestudies/list/list_annotated.c
opam exec -- dune runtest output/casestudies/list/list_annotated
```

```
# Metrics
./build.sh --single-core  --metrics examples/src/casestudies/list/list_annotated.c
python3 tools/aggregate_metrics.py
```

```
# Timing
./build.sh --time --single-core --no-cache examples/src/casestudies/list/list_annotated.c
```

```
# C Lines
tools/count_lines.sh examples/src/casestudies/list/list_annotated.c
```
For the Linked List (Func) case study, the annotations can be counted in the file `examples/src/casestudies/list/list_annotated.c`.
There is no Coq code to count.



### **Vector (Mem)**
```
# Specs
./build.sh --single-core  examples/src/casestudies/arrays/array_no_annot.c
opam exec -- dune runtest output/casestudies/arrays/array_no_annot
```

```
# Metrics
./build.sh --single-core  --metrics examples/src/casestudies/arrays/array_no_annot.c
python3 tools/aggregate_metrics.py
```

```
# Timing
./build.sh --time --single-core --no-cache examples/src/casestudies/arrays/array_no_annot.c
```

```
# C Lines
tools/count_lines.sh examples/src/casestudies/arrays/array_no_annot.c
```

For the Vector (Mem) case study, there are no annotations to count in the C files/there is no Coq code to count.


### **Vector (Func)**
```
# Specs
./build.sh --single-core  examples/src/casestudies/arrays/array_annotated.c
opam exec -- dune runtest output/casestudies/arrays/array_annotated
```

```
# Metrics
./build.sh --single-core  --metrics examples/src/casestudies/arrays/array_annotated.c
python3 tools/aggregate_metrics.py
```

```
# Timing
./build.sh --time --single-core --no-cache examples/src/casestudies/arrays/array_annotated.c
```

```
# C Lines
tools/count_lines.sh examples/src/casestudies/arrays/array_annotated.c
```
For the Vector (Func) case study, the annotations can be counted in the file `examples/src/casestudies/arrays/array_annotated.c`.
There is no Coq code to count.

### **Bipbuffer (Len)**
```
# Specs
./build.sh --single-core  examples/src/casestudies/memcached_bidirectional_buffers/bipbuffer_annotated.c
opam exec -- dune runtest output/casestudies/memcached_bidirectional_buffers/bipbuffer_annotated
```

```
# Metrics
./build.sh --single-core  --metrics examples/src/casestudies/memcached_bidirectional_buffers/bipbuffer_annotated.c
python3 tools/aggregate_metrics.py
```

```
# Timing
./build.sh --time --single-core --no-cache examples/src/casestudies/memcached_bidirectional_buffers/bipbuffer_annotated.c
```

```
# C Lines
tools/count_lines.sh examples/src/casestudies/memcached_bidirectional_buffers/bipbuffer_annotated.c
```
For Bipbuffer (Len) case study, the annotations can be counted in the file `examples/src/casestudies/memcached_bidirectional_buffers/bipbuffer_annotated.c`.
There is no Coq code to count.


### **OpenSSL Buffer (Mem)**
```
# Specs
./build.sh --single-core  examples/src/casestudies/openssl_buffer/buffer.c
opam exec -- dune runtest output/casestudies/openssl_buffer/buffer
```

```
# Metrics
./build.sh --single-core  --metrics examples/src/casestudies/openssl_buffer/buffer.c
python3 tools/aggregate_metrics.py
```

```
# Timing
./build.sh --time --single-core --no-cache examples/src/casestudies/openssl_buffer/buffer.c
```

```
# C Lines
tools/count_lines.sh examples/src/casestudies/openssl_buffer/buffer.c
```

For the OpenSSL Buffer (Mem) case study, there are no annotations to count in the C files/there is no Coq code to count.


### **OpenSSL Buffer (Len)**
```
# Specs
./build.sh --single-core  examples/src/casestudies/openssl_buffer/buffer_shape.c
opam exec -- dune runtest output/casestudies/openssl_buffer/buffer_shape
```

```
# Metrics
./build.sh --single-core  --metrics examples/src/casestudies/openssl_buffer/buffer_shape.c
python3 tools/aggregate_metrics.py
```

```
# Timing
./build.sh --time --single-core --no-cache examples/src/casestudies/openssl_buffer/buffer_shape.c
```

```
# C Lines
tools/count_lines.sh examples/src/casestudies/openssl_buffer/buffer_shape.c
```

For the OpenSSL Buffer (Len) case study, the annotations can be counted in the file `examples/src/casestudies/openssl_buffer/buffer_shape.c`.
There is no Coq code to count.

### **Binary Search (Func)**
```
# Specs
./build.sh --single-core  examples/src/casestudies/binary_search.c
opam exec -- dune runtest output/casestudies/binary_search
```

```
# Metrics
./build.sh --single-core  --metrics examples/src/casestudies/binary_search.c
python3 tools/aggregate_metrics.py
```

```
# Timing
./build.sh --time --single-core --no-cache examples/src/casestudies/binary_search.c
```

```
# C Lines
tools/count_lines.sh examples/src/casestudies/binary_search.c
```

For the Binary Search (Func) case study, the annotations can be counted in the file `examples/src/casestudies/binary_search.c` and
the type is defined in `examples/src/casestudies/extra_proofs/binary_search/type_defs.v`.
The Coq code can be counted with:
```
coqwc examples/src/casestudies/extra_proofs/binary_search/pure_defs.v  # spec: 31 + proof: 18 = 49
```

### **Hashmap (Func)**
```
# Specs
./build.sh --single-core  examples/src/casestudies/mutable_map.c
opam exec -- dune runtest output/casestudies/mutable_map
```

```
# Metrics
./build.sh --single-core  --metrics examples/src/casestudies/mutable_map.c
python3 tools/aggregate_metrics.py
```

```
# Timing
./build.sh --time --single-core --no-cache examples/src/casestudies/mutable_map.c
```

```
# C Lines
tools/count_lines.sh examples/src/casestudies/mutable_map.c
```

For the Hashmap (Func) case study, the annotations can be counted in the file `examples/src/casestudies/mutable_map.c`.
The Coq code can be counted with:
```
coqwc examples/src/casestudies/extra_proofs/mutable_map/definitions.v # spec: 272 + proof: 225 = 497
```
and *additionally* 9 lines of Coq code at the top of `examples/src/casestudies/mutable_map.c`.
