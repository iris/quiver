# Quiver Tutorial


To explain how Quiver can be used to infer specifications, we discuss how to apply it to a new case study:
    a simple range implementation, a variant of the range-datatype in Section 2 implemented in C.
We have prepared a range implementation in the file `examples/src/casestudies/range.c`.
It contains a range data structure
```C
typedef struct range {
    int low;
    int high;
} *range_t;
```
and several operations for working with ranges (e.g., `init_range`, `mk_range`, `size`, etc).
Our goal is to eventually produce specifications where ranges are a data type `(start, end) @ range_t`.

## Step One: The Bare Specification
As a first step, we execute Quiver on the unannotated file:
```
./build.sh examples/src/casestudies/range/range.c
```
The output should be a "memory" specification (i.e., a specification that describes only how the memory is modified by the operations). For example, the output of the function `mk_range` should be:

```
mk_range:
(∀ x : loc, ENSURES [] [block x (ly_size struct_range)] IN
            ret (owned x (struct[struct_range] [value[ly_size i32] a_val; value[ly_size i32] b_val])))%I
```
It says that the function has no preconditions (no REQUIRES) and that in its postcondition, it returns an owned pointer `x` (via `owned x (struct[struct_range] [value[ly_size i32] a_val; value[ly_size i32] b_val])`) to a struct with two fields: the first field stores the value `a_val` of the first argument, the second field stores the value `b_val` of the second argument.
Additionally, the inferred specification returns ownership of the `block`-predicate obtained from allocating the pointer `x`.

**Note.**
As an aside, note that the file already in this format contains two annotations for Quiver:
    (1) the import `#include <quiver.h>` (for Quiver's assertions) and
    (2) the magic comment `//@q::import all_proofs from quiver.examples.src.casestudies.memory` to make the specifications of the memory operations of the Allocators case study available.
We need the latter to use `xmalloc` in the range implementation.
See `docs/ANNOTATIONS.md` for more information on Quvier's annotations.

## Step Two: Defining the Data Type
In this format, reading the specifications takes some effort, since they are still quite low-level. So next, let us introduce a data type for ranges. To do so, we add Quiver annotations to the type declaration:
```
typedef struct
[[q::refined_by("s: Z", "e: Z")]]
[[q::typedef("range_t : ex q, (owned q ...) ∗∗ (block q (ly_size struct_range))")]]
range {
    [[q::field("int[i32] s")]]
    int low;
    [[q::field("int[i32] e")]]
    int high;
} *range_t;
```
In general, annotations are described in the file `docs/ANNOTATIONS.md`.
In this case, the annotation `[[q::refined_by("s: Z", "e: Z")]]` says that we want the argument `X` of the type `X @ range_t` to be two integers `s: Z` and `e: Z`.
The annotation `[[q::typedef("range_t : ex q, (owned q ...) ∗∗ (block q (ly_size struct_range))")]]` says that we want to introduce a new type `range_t`. It is defined as the type that follows after the colon.
That is, in this case, it is an owned pointer `owned q` where the pointer `q` is existentially quantified and we, additionally, keep around ownership of the predicate `block q (ly_size struct_range)`. The pointer `q` points to the contents of the struct. In the type annotation, we indicate this via the three dots `...`. In the first field of the struct should be a 32-bit integer `s` and in the second field should be a 32-bit integer `e` (where `s` and `e` are the arguments of our type).

We can see the type that Quiver produces for its inference internally by running again:
```
./build.sh examples/src/casestudies/range/range.c
```
and opening the file `examples/src/casestudies/range/proofs/range/generated_spec.v`.
In the file, we can see that `range_t` is defined as (the fixpoint of the non-recursive) `range_t_rec`,
which contains the type described above. The dots `...` have been replaced with the type `structT struct_range`.

## Step Three: Sketching Function Specs
We have defined a type in the previous step, but none of the inferred specifications have actually changed. The reason is that we have not yet provided any *sketches* that tell Quiver how to incorporate the type into its inference. To do so, we suggest adding the following to `init_range`:

```
[[q::ensures("^r ◁ᵥ ? @ range_t")]]
void init_range(range_t r, int s, int e) {
    r->low = s;
    r->high = e;
}
```
This annotation tells Quiver that the argument `r` passed to `init_range` (via `^r`, which refers to the value of `r` at the beginning of the function) should be a range at the end of the function. If we now rerun the inference (with `./build.sh examples/src/casestudies/range/range.c`), we obtain the following type for `init_range`:
```
init_range:
(∃ (x : loc) (x0 x1 : Z), REQUIRES [] [r_val ◁ᵥ owned x (struct[struct_range] [place (x at{struct_range}ₗ "low"); place (x at{struct_range}ₗ "high")]); x at{struct_range}ₗ "low" ◁ₗ (any[ly_size i32]); x at{struct_range}ₗ "high" ◁ₗ (any[ly_size i32]); s_val ◁ᵥ (int[i32] x0); e_val ◁ᵥ (int[i32] x1); block x (ly_size struct_range)] IN
                          ENSURES [] [r_val ◁ᵥ (x0, x1) @ range_t] IN
                          ret void)%I
```
The precondition still contains relatively low-level assertions describing the memory layout of `r` initially (i.e., it should be a pointer to a struct `struct_range` where the first and second fields store arbitrary memory of size `ly_size i32`).
However, it additionally contains the assumptions `s_val ◁ᵥ (int[i32] x0)`, `e_val ◁ᵥ (int[i32] x1)`, and `block x (ly_size struct_range)`, which are needed to successfully construct a range at the end of the function as indicated by the sketch.
And indeed, the postcondition says that `r_val ◁ᵥ (x0, x1) @ range_t`, meaning the argument value of `r` is a range at the end of the function---where the first field comes from `s` and the second comes from `e`.

But `init_range` is not the only function that changed. Additionally, the inferred specification of `mk_range` has changed:
```
mk_range:
(∃ x x0 : Z, REQUIRES [] [b_val ◁ᵥ (int[i32] x); a_val ◁ᵥ (int[i32] x0)] IN
             ret ((x0, x) @ range_t))%I
```
Since `mk_range` calls `init_range`, it builds on the specification of `init_range`. In this case,
the specification *now* says that `mk_range` returns a correctly initialized range from the arguments.

We can add similar annotations to the other range functions to infer specifications in terms of `range_t`.
For example, for `get_size`, we recommend adding:
```
[[q::requires("^r ◁ᵥ ? @ range_t")]]
[[q::ensures("^r ◁ᵥ ? @ range_t")]]
int get_size(range_t r) {
    return r->high - r->low;
}
```
which results in the specification:
```
get_size:
(∃ x x0 : Z, REQUIRES [x0 - x ∈ i32] [r_val ◁ᵥ (x, x0) @ range_t] IN
             ENSURES [] [r_val ◁ᵥ (x, x0) @ range_t] IN
             ret (int[i32] x0 - x))%I
```
The annotation `[[q::requires("^r ◁ᵥ ? @ range_t")]]` imposes a precondition constraint: the value of `r` should be a range `? @ range_t` at the beginning of the function.
As we can see in the inferred specification above, this constraint is incorporated during inference.

## Step Four: Adjusting the Definition
There is something interesting about the inferred specification of `get_size`: it has as a precondition that `x0 - x ∈ i32` where `x0` is the end of the range and `x` is the start.
This precondition is necessary to ensure that the difference `r->high - r->low` does not overflow.
The reason is that our data type definition does not actually require the range bounds to be ordered (i.e., `r->low` could be larger than `r->high`).
Thus, as a final step, let us modify the type definition and re-run the inference:
```
typedef struct
[[q::refined_by("s: Z", "e: Z")]]
[[q::typedef("range_t : ex q, (owned q ...) ∗∗ (block q (ly_size struct_range))")]]
[[q::constraints("⌜0 ≤ s ≤ e⌝")]]
range {
    [[q::field("int[i32] s")]]
    int low;
    [[q::field("int[i32] e")]]
    int high;
} *range_t;
```
We add the constraint `0 ≤ s ≤ e` to the data type definition via the annotation `q::constraint` (see `examples/src/casestudies/range/proofs/range/generated_spec.v` for the resulting type definition).
If we now rerun the inference, the specs of all three operations (`init_range`, `mk_range`, and `get_size`) change:
```
mk_range:
(∃ x x0 : Z, REQUIRES [x0 ≤ x; 0 ≤ x0] [b_val ◁ᵥ (int[i32] x); a_val ◁ᵥ (int[i32] x0)] IN
             ret ((x0, x) @ range_t))%I
get_size:
(∃ x x0 : Z, REQUIRES [] [r_val ◁ᵥ (x, x0) @ range_t] IN
             ENSURES [] [r_val ◁ᵥ (x, x0) @ range_t] IN
             ret (int[i32] x0 - x))%I
init_range:
(∃ (x : loc) (x0 x1 : Z), REQUIRES [0 ≤ x0; x0 ≤ x1] [r_val ◁ᵥ owned x (struct[struct_range] [place (x at{struct_range}ₗ "low"); place (x at{struct_range}ₗ "high")]); x at{struct_range}ₗ "low" ◁ₗ (any[ly_size i32]); x at{struct_range}ₗ "high" ◁ₗ (any[ly_size i32]); s_val ◁ᵥ (int[i32] x0); e_val ◁ᵥ (int[i32] x1); block x (ly_size struct_range)] IN
                          ENSURES [] [r_val ◁ᵥ (x0, x1) @ range_t] IN
                          ret void)%I
```
The way they changed is that `mk_range` and `init_range` now have constraints on the integer bounds, since they need to prove that they can construct valid ranges. In exchange, `get_size` no longer needs to impose constraints, since `0 ≤ s ≤ e` ensures that the subtraction `r->high - r->low` cannot overflow.


This last step concludes our tutorial.
We encourage the reader to add sketches to the remaining `range` operations.
(Recall that an overview of Quiver's annotations can be found in `ANNOTATIONS.md`.)

