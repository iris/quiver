Quiver Annotation Syntax and Types
======================================

The Quiver frontend modifies the frontend of [RefinedC](https://plv.mpi-sws.org/refinedc/).
As such, we use similar kinds of annotations. In Quiver, they are:
 - [C2X attributes](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n2335.pdf)
   of the form `[[q::<ident>(<string>, ... ,<string>)]]`,
 - macros defined in [`quiver.h`](include/quiver.h), and
 - special comments of the form `//q::<ident> <payload>`.

The attribute explanation in this file is based on a [similar guide for RefinedC](https://gitlab.mpi-sws.org/iris/refinedc/-/blob/master/ANNOTATIONS.md?ref_type=heads).

# Contents

[[_TOC_]]

# Valid attributes

Quiver attributes are of the form `[[q::<ident>(<string>, ...,<string>)]]`.
They can be placed on certain C constructs (e.g., on functions or on loops).
Attributes of several kinds can be specified, they are distinguished using
the identifier that they carry. Each specific kind of attribute is constrained
as to where it may appear in the source code. For instance, postcondition
attributes may only appear on a function definition or a function declaration.
The following table gives information about every available kind of
attributes, including how many arguments (i.e., strings) it may have,
and what syntactic constructs it can be attached to.

| Identifier     | Attribute Arguments   | Allowed on            | Syntax for the arguments                                    |
|----------------|-----------------------|-----------------------|-------------------------------------------------------------|
| `args`         | One or more           | Functions             | Coq expression for Thorium type                             |
| `constraints`  | One or more           | Structures, Loops     | Coq expression for separation logic assertion               |
| `ensures`      | One or more           | Functions             | Coq expression for separation logic assertion               |
| `exists`       | One or more           | Functions, Loops      | `<ident> ":"` Coq expression for Coq type                   |
| `field`        | Exactly one           | Structure members     | Coq expression for Thorium type                             |
| `parameters`   | One or more           | Functions             | `<ident> ":"` Coq expression for Coq type                   |
| `typedef`      | Exactly one           | Structures            | `<ident> ":"` Coq expression for Thorium type               |
| `refined_by`   | One or more           | Structures            | `<ident> ":"` Coq expression for Coq type                   |
| `requires`     | One or more           | Functions             | Coq expression for separation logic assertion               |
| `returns`      | Exactly one           | Functions             | Coq expression for Thorium type                             |
| `join_if`      | None                  | Functions             |                                                             |
| `join_conj`    | None                  | Functions             |                                                             |
| `exact_post`   | None                  | Functions             |                                                             |
| `external_pt`  | Exactly one           | Functions             | Coq identifier                                              |


# Placement of attributes

As shown in the existing Quiver case studies (in
`examples/src/casestudies/`), attributes on functions are placed
immediately before their definitions and/or declarations. And things
go similarly for most of the attributes, including those on loops and
structure fields. Note that there should be no blank line interleaved
between the attributes themselves, or between the attributes and the
element of C syntax to which they will be attached.

In fact, there is only one kind of annotation for which the attributes
must be given in a somewhat unexpected place: structures. On a
structure, attributes do not precede the declaration. Instead, they
are placed right after the `struct` keyword. An example of this is
given below.
```c
struct
[[q::refined_by("r : Z", "g : Z", "b : Z")]]
color {
  [[q::field("int[u8] r")]]
  uint8_t r;

  [[q::field("int[u8] g")]]
  uint8_t g;

  [[q::field("int[u8] b")]]
  uint8_t b;
};
```

# C Variables in Attributes

Inside attributes, the name of C variables refers to the stack location where they are stored.
For example, in the function:
```c
void foo(int x){
  x = 0;
  q_assert("x ◁ₗ int[i32] 0");
}
```
the variable `x` refers to the stack *location* of `x` inside the assertion `x ◁ₗ int[i32] 0`.
In this case, the assertion means `x` stores the `32`-bit integer `0`.


To refer to the *value* of a function argument at the beginning of a function, we prefix its name with `^`.
For example, in the function:
```c
void bar(int x){
  ...
  q_assert("^x ◁ᵥ int[i32] 0");
}
```
the variable `^x` refers to the *value* of `x` at the beginning of the function inside the assertion `^x ◁ᵥ int[i32] 0`.
In this case, the assertion means the argument `x` of the function was the integer `0` initially.



# Description of the attributes

In the following we describe the syntax and semantics of the
supported attributes.

## `q::args`

This attribute appears on functions only, and requires one or more attribute arguments.
Each attribute argument is a Coq definition of a Thorium type (e.g. `int[u8] n`) and
it specifies the type that is associated with the corresponding
function argument of the function (in order). The attribute can contain sketches.
In particular, it can contain question marks.
To avoid providing anything for a type, it can be an underscore (i.e., "_").

## `q::constraints`

This attribute may appear on structures and on loops, and it must have one or
more attribute arguments. Each argument is a Coq separation logic assertion (i.e., of Coq type `iProp`).
Constraints may not include sketches. On a loop, a constraint is part of the loop invariant and
it must hold at the start of each loop iteration before checking the conditional.

## `q::ensures`

This attribute appears on functions only and requires one or more attribute arguments.
Each argument is a Coq separation logic assertion (i.e., of Coq type `iProp`).
It specifies a post-condition (i.e., the assertion should hold after that the function has returned).
The attribute may contain sketches: it can use question marks to leave holes in the assertion.


## `q::exists`

This attribute may appear on functions and structs. It should carry at least
one attribute argument, and its arguments should all be of the following form.
```
<ident (as variable name)> ":" Coq expression for Coq type
```
For structs, it corresponds to an existentially quantified variable with the given Coq type.
On a function, this variable can appear in post-conditions and on the return
type of the function (see `q::ensures` and `q::returns`). In the predicate transformer
specification format, it corresponds to a universally quantified variable before the postcondition.
In more traditional Hoare-style specifications, it corresponds to an existential quantifier
in the postcondition—hence the name `exists`.


## `q::field`

This attribute only appears on structure members, and it requires exactly one
attribute argument, a Coq expression for a Thorium type (e.g., `int[u8] b`).
It specifies the type that corresponds to the structure member.
Note that a `q::field` attribute must be given for all structure fields that
are involved in the definition of a type.


## `q::parameters`

This attribute can appear either on functions and on structures. It should be
given at least one attribute argument of the following form.
```
<ident (as variable name)> ":" Coq expression for Coq type
```
In the predicate transformer specification format, it corresponds to an
existential quantifier at the beginning of the predicate transformer.
Quiver's inference may decide that this quantifier is not necessary and
remove it by (e.g., a parameter that is constrained to be `0` will be
turned into `0`).

## `q::typedef`

This attribute only appears on structures, and it expects one attribute argument of the
following form.
```
<ident (as pointer type name)> : Coq expression for Thorium type
```
The identifier should correspond to the name defined (using a
`typedef`). When given, this attribute instructs the system to
generate a refinement type corresponding to the type on the right.
The type expression specified inside the
annotation should contain an ellipsis (i.e., a type expression of the
form `...`). In this place, a type generated for the structure (with the
types specified for its fields) will be inserted by Quiver.

## `q::refined_by`

This attribute appears on structures exclusively, and it must be given one or
more attribute arguments of the following form.
```
<ident (as variable name)> : Coq expression for Coq type
```
When attributes are provided on a structure,  a corresponding refinement type
is automatically generated. The idea is that an element of the structure has a
refinement formed of (a tuple of) mathematical (i.e., Coq) values. Each of the
attribute arguments of the `q::refined_by` attribute specify such a value, with a name
and a type. The name is bound in constraints and also in field attributes (see
`q::field`) on the structure (and on nested structures).

## `q::requires`

This attribute appears on functions only, and requires one or more attribute arguments.
Each argument is a Coq separation logic assertion (i.e., of Coq type `iProp`).
It specifies a pre-condition (i.e., the assertion should hold after that the function has returned).
The attribute may contain sketches: it can use question marks to leave holes in the assertion.

## `q::returns`

This attribute appears on functions only,  and it should be given exactly one
attribute argument that is a Coq expression for a Thorium type.
The argument specifies the type corresponding to the value returned
by the function.

## `q::join_if`

This attribute appears on functions only, and it has no attribute arguments.
The attribute enables Quiver's heuristic for joining branches of conditionals.

## `q::join_conj`

This attribute appears on functions only, and it has no attribute arguments.
The attribute enables Quiver's support for distributing conjunctions over conditionals.


## `q::exact_post`

This attribute appears on functions only, and it has no attribute arguments.
The attribute requires Quiver to produce a postcondition exactly as specified in the remaining annotations.
That is, the postcondition in the annotations is not a sketch, it is exact.

## `q::external_pt`

This attribute appears on functions only, and it should be given exactly one
attribute argument that is a Coq identifier for a predicate transformer.
The attribute tells Quiver that the predicate transformer should be used as a
specification by clients of the function.


# Types

Unlike RefinedC, Quiver does not have a fixed frontend syntax for types.
Instead, we use Coq definitions directly and use Coq notations for pretty printing.
We give an overview of the most common types and their notations.

## Integers

The Thorium type for integers is `intT it n` where `it` is the Caesium integer type and `n` is a Coq integer.
We define the notation `int[it] n := intT it n`.

## Booleans

The Thorium type for booleans is `boolT it b` where `it` is the Caesium integer type and `b` is a Coq boolean.
We define the notation `bool[it] b := boolT it b`.
For cases where the boolean `b` is a decidable proposition `φ`, we define `φ @ boolR := boolT it (bool_decide φ)`.

## Any

The Thorium type for arbitrary bytes is `anyT n` where `n` is a Coq integer determining the number of arbitrary bytes.
We define the notation `any[n] := anyT n`.

## Zeros

The Thorium type for zero bytes is `zerosT n` where `n` is a Coq integer determining the number of bytes.
We define the notation `zeros[n] := zerosT n`.

## Value

The Thorium type `valueT v n` says that the value is exactly `v` and it is of size `n` (i.e., it consists of `n` bytes).
We define the notation `value[n] v := valueT v n`.

## Void

The Thorium type `voidT` says that the value is the empty value, corresponding to the C type void (i.e., it is of length zero).
We define the notation `void := voidT`.

## Owned Pointers

The Thorium type `ownT l A` says that its values are the location `l`, which is currently of Thorium type `A` (i.e., it carries the location type assignment `l ◁ₗ A`). We define the notation `owned l A := ownT l A`.

## Null

The Thorium type `nullT` says that its values are `NULL`.
We define the notation `null := nullT`.


## Optionals

The Thorium type `optionalT φ A` carries a decidable proposition `φ` and a Thorium type `A`.
It is `A` if `φ` is true, and `nullT` otherwise.
We define the notation `optional φ A := optionalT φ A`.

## Raw Pointers

The Thorium type `ptrT r len i` carries a location `l`, a Coq integer `len`, and a Coq integer `i`.
It is used for raw pointers that do not carry ownership.
Its values are the pointer `r` offset by `i`.
It remembers that the allocation size of `r` is at least `len`.
We define the notation `ptr[len] r i := ptrT r len i`.

## Pointer Values

The Thorium type `optT v` carries a Caesium value `v`.
It is used for values that are guaranteed to be pointers.
It asserts that the value is `v`.
We define the notation `opt v := optT v`.

## Separating Conjunction

The Thorium type `sepT A P` adds the separation logic constraint `P` to the Thorium type `A`.
We define the notation `A ∗∗ P := sepT A P`.

## Pure Constraints

The Thorium type `constraintT A φ` adds the pure constraint `φ` to the Thorium type `A`.
We define the notation `{ A | φ } := constraintT A φ`.

## Existential Quantification

The Thorium type `existsT (λ x: X, A x)` is used for type-level existential quantification.
We define the notation `ex x, A x := exists (λ x, A x)`.

## Structs

The Thorium type for structs is `structT sl As` where `sl` is the Caesium struct layout and `As` is a list of Thorium types for the fields.
We define the notation `struct[sl] As := structT sl As`.

## Place

The Thorium type `placeT l` says that the location ownership of `l` is currently borrowed.
It can be used, for example, in structures and owned pointers.
For example, `r ◁ₗ structT foo [placeT (r at{foo}ₗ "bar")]` means `r` is a `foo` struct, and the ownership of its `bar` field is currently borrowed,
meaning it resides outside the struct type (e.g., in the form `r at{foo}ₗ "bar" ◁ₗ intT i32 n`).
We define the notation `place l := placeT l`.

## Arrays

The Thorium type `arrayT T sz xs` carries a function `T` from some type `X` to Thorium types, an integer `sz`, and a list `xs` where the elements are of type `X`.
It can be used for arrays where the individual elements are of type `T x` (for the respective `x` in the list `xs`).
The integer `sz` tracks the size of each individual element. (All elements are of the same size.)

## Refinement Types

The Thorium type `x @ T` is used for refinement types where `x` is some Coq term of type `X` and `T` is a function from `X` to Thorium types.
We use this type for custom type definitions generated from annotations on structs.

## Recursive Types

The Thorium type `fixR (λ A x, L A x)` is used for recursive refinement types (where `L` is the body of the type).
We define the notation `μ A x. L A x := fixR (λ A x, L A x)`.

## Functions

The Thorium type `fnT lys T` carries a list of Caesium layouts `lys` and a predicate transformer `T`.
It is used as a type for function pointers. The arguments of the function must be of layout `lys` and the predicate transformer `T` is the specification of the function.

## Slices

The Thorium type `slicesT len As` carries a Coq integer `len` and a list of segments `Ts` where each element is a triple of two integers and a Thorium type.
Like `arrayT`, it is used for sequences of bytes.
Unlike `arrayT`, its segments do not need to be of the same size and not of the same type.
Instead, their length is tracked via the integers in the list `As`.

# Annotations using macros

The macro `q_assert(P);` can be used to specify inline assertions.
Analogous to `[[q::requires]]` and `[[q::ensures]]`, it takes a
separation logic assertion `P`. The assertion can be a sketch (i.e.,
it can contain question marks) and describes (part of) the separation
logic state at that point in the function.

# Annotations using comments

Special comments can be used to import external Coq dependencies as well as
for inlining Coq definitions in the generated code.

## Importing dependencies

To require a Coq module (`From <library> Require Import <modpath>`) in the
generated files, the following annotation can be used.
```c
//@q::import <modpath> from <library>
```


## Context directive

The Coq context (in spec and proof sections) using the following annotation:
```c
//@q::context <context_item> ... <context_item>
```

We use this annotation, for example, to make the memory library available.

## Inlined Coq code

An arbitrary line of Coq code can be inlined in the generated specification
file using the following syntax (for single of multiple lines).
```c
//@q::inlined <code line>

//@q::inlined
//@<code line 1>
//@<code line 2>
//@<code line 3>
//@q::end
```
With `q::inlined`, the code is inserted at the beginning of the main section
of the specification file.
