# Interpreting Quiver's Output

The expected output of Quiver's case studies can be found in the folder `output/`.
We explain how to read Quiver's output and map it to a mathematical predicate transformers.
To keep matters concrete, we consider an example, the memory specification for the function `mkvec`.


**On-Paper Predicate Transformer**
The on-paper predicate transformer for `mkvec` is:

```
∃ (n: Z), v_n ◁ᵥ int[int] n ∗ 0 ≤ n ∗ (∀ l r w, block l sz_vec ∗ block r (sz_int ⋅ n) ∗ w ◁ᵥ own l (struct[vector][own r (zeros(sz_int ⋅ n)); int[int] n]) -∗ Φ w)
```

The precondition part `∃ (n: Z), v_n ◁ᵥ int[int] n ∗ 0 ≤ n` ensures that the argument named n—corresponding to the value `v_n`—is a nonnegative integer `n`.
The postcondition part `∀ l r w, block l sz_vec ∗ block r (sz_int ⋅ n) ∗ w ◁ᵥ own l (struct[vector][own r (zeros(sz_int ⋅ n)); int[int] n])` ensures that the return value `w` is an owned pointer `l` to a struct `vector` with two fields: an owned pointer `r` to `sz_int ⋅ n`-zeros (i.e., `own r (zeros(sz_int ⋅ n))`) and the `int`-integer `n` (i.e., `int[int] n`).


**Inferred Predicate Tranformer**
The predicate transformer that Quiver infers for `mkvec` (see `output/casestudies/arrays/array_no_annot/mkvec.expected`) is:
```
mkvec:
(∃ x : Z, REQUIRES [0 ≤ x] [n_val ◁ᵥ (int[i32] x)] IN
          (∀ x0 x1 : loc, ENSURES [] [block x1 (ly_size struct_vector); block x0 (ly_size i32 * x)] IN
                          ret (owned x1 (struct[struct_vector] [owned x0 (zeros[ly_size i32 * x]); int[i32] x]))))%I
```

The two styles of writing the same specification can be reconciled as follows:
1. Instead of writing out the precondition parts, Quiver uses `REQUIRES` to impose preconditions in its predicate transformers (explained below).
2. Instead of writing out the postcondition parts, Quiver uses `ENSURES` to give postconditions in its predicate transformers (explained below).
3. The postcondition is named `ret` instead of `Φ`.
4. Instead of mentioning the return value `w` explicitly in the predicate transformer, we pass its type `own l ...` directly to the postcondition.
   (That is, the postcondition is on return types, not on the return value itself.)
5. Instead of `v_name` on paper (e.g., `v_n` here), values in Coq are named `name_val` (e.g., `n_val` here).
6. We use Coq notation for various mathematical terms (e.g., `ly_size i32` for `sz_int`).
7. We use Coq notations for the Quiver types. For an explanation of the Quiver types see `docs/ANNOTATIONS.md`.
8. The `%I` is for Coq's notation scopes and can be ignored.

**Pre- and Postconditions**
As we have seen above, to give pre- and postconditions, Quiver uses `REQUIRES` and `ENSURES`.
They are short forms for separating conjunctions/magic wands over multiple elements.
That is, using Iris notation for big separating conjunction, we have:
```
REQUIRES Γ Ω IN P := ([∗ list] φ ∈ Γ, ⌜φ⌝) ∗ ([∗ list] M ∈ Ω, M) ∗ P
ENSURES  Γ Ω IN P := ([∗ list] φ ∈ Γ, ⌜φ⌝) ∗ ([∗ list] M ∈ Ω, M) -∗ P
```
The notations make it easier to distinguish preconditions and postconditions in larger predicate transformers.

**Conditionals and Conjuctions**
Conditionals in predicate transformers are written as `IF φ THEN P1 ELSE P2`.
Conjunctions in predicate transformers are written as `IF * THEN P1 ELSE P2`.