#ifndef REFINEDC_H
#define REFINEDC_H

#define q_annot(e, ...)                                 \
    _Pragma("GCC diagnostic push")                       \
    _Pragma("GCC diagnostic ignored \"-Wunused-value\"") \
    [[q::annot(__VA_ARGS__)]] &(e);                     \
    _Pragma("GCC diagnostic pop")

#define q_annot_expr(e, ...) (0 ? ("q_annot", __VA_ARGS__, (e)) : (e))

#define _q_assert                                         \
    _Pragma("GCC diagnostic push")                        \
    _Pragma("GCC diagnostic ignored \"-Wunused-value\"")  \
    [[q::remove]]                                        \
    0;                                                    \
    _Pragma("GCC diagnostic pop")

#define q_assert(e) [[q::constraints(e)]] _q_assert

#define Q_MACRO_ARG(arg) "ARG", #arg
#define Q_MACRO_EXPR(expr) "EXPR", expr
#define Q_MACRO(name, m, ...) (0 ? ("q_macro", #name, __VA_ARGS__, (m)) : (m))


#endif
